import React, { Component } from "react";
import Table from "ibankComponents/UI/Table";
import Button from "ibankComponents/UI/Button";
import Pagination from "ibankComponents/UI/Pagination";
import Modal from "ibankComponents/UI/Modal";
// import TransactionConfirmation from "ibankComponents/Transactions/Confirmation";
import { ability } from "config/ibankAbility";
import PropTypes from "prop-types";
import Spinner from "ibankComponents/UI/Spinner";
import Select from "ibankComponents/UI/MultiSelect";
import { Row, Col, ControlLabel } from "react-bootstrap";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse";
import { getUserType } from "services/paymentBackendAPI/backendPlatform";
import DateRangePicker from 'react-bootstrap-daterangepicker';
import Dropdown from "ibankComponents/UI/Dropdown";
import DropdownTag from "ibankComponents/UI/Dropdown/AmlTag";
import Input from "ibankComponents/UI/Input";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import moment from "moment";
import { Link } from "react-router-dom";

const userType = getUserType();

let date = (new Date()).setMonth((new Date()).getMonth() - 1);

const matchStatusRefs = {
  "unknown": "Unknown",
  "no_match": "No Match",
  "potential_match": "Potential Match",
  "false_positive": "False Positive",
  "true_positive": "True Positive",
  "Unknown": "unknown",
  "No Match": "no_match",
  "Potential Match": "potential_match",
  "False Positive": "false_positive",
  "True Positive": "true_positive"
}

const riskLevelsRefs = {
  "unknown": "Unknown",
  "low": "Low",
  "medium": "Medium",
  "high": "High",
  "Unknown": "unknown",
  "Low": "low",
  "Medium": "medium",
  "High": "high"
}

const matchStatuses = [
  { name: "Unknown" },
  { name: "No Match" },
  { name: "Potential Match" },
  { name: "False Positive" },
  { name: "True Positive" }
]

const riskLevels = [
  { name: "Unknown" },
  { name: "Low" },
  { name: "Medium" },
  { name: "High" }
]

export default class AbstractComponent extends Component {
  state = {
    statuses: {
      "approve": "Approved",
      "decline": "Declined",
      "cancel": "Cancelled",
      "unapprove": "Unapproved",
      "received": "Received"
    },
    width: 0,
    currentPage: 1,
    pageSize: 10,
    isSearch: false,
    loading: true,
    confirmation: false,
    status: "",
    directionData: {},
    startDate: date,
    endDate: date,
    timeFilter: {},
    selectedMatchStatuses: [
      "All",
      ...matchStatuses.map(i => i.name)
    ],
    selectedMatchStatusesOptions: [],
    matchStatusOptions: [],
    selectedRiskLevels: [
      "All",
      ...riskLevels.map(i => i.name)
    ],
    selectedRiskLevelsOptions: [],
    riskLevelsOptions: [],
    search: {},
    datePlaceholder: "All",
    checked: {},
    attachedTags: [],
    tagsValues: {},
    roinvstgReason: ""
  };

  handleAll = async() => {
    this.setState({
      timeFilter: {},
      currentPage: 1,
      startDate: date,
      endDate: date
    });
    await this.props.get({
      page: this.state.currentPage, 
      items: this.state.pageSize, 
      ...this.props.directionData,
      type: this.props.type
    });
  }

  handleSetAmlDates = (e, p) => {
    const { search } = this.state;
    const created_at_from = moment(p.startDate).format("YYYY-MM-DD");
    const created_at_to = moment(p.endDate).format("YYYY-MM-DD");
    this.setState({
      search: {
        ...search,
        created_at_from,
        created_at_to
      },
      datePlaceholder: `${moment(created_at_from).format("MMM DD")}   - ${moment(created_at_to).format("MMM DD")}`
    });
  }

  handleSetDates = async(e,p) => {
    this.setState({
      timeFilter: {
        fromDate: moment(p.startDate).format("YYYY-MM-DDThh:mm:ss"),
        toDate: moment(p.endDate).format("YYYY-MM-DDThh:mm:ss"),
      },
      currentPage: 1,
      startDate: p.startDate,
      endDate: p.endDate
    });
    await this.props.get({
      page: this.state.currentPage, 
      items: this.state.pageSize, 
      ...this.props.directionData,
      fromDate: moment(p.startDate).format("YYYY-MM-DDThh:mm:ss"),
      toDate: moment(p.endDate).format("YYYY-MM-DDThh:mm:ss"),
      type: this.props.type
    });
  }

  handleLastDay = async() => {    
    this.setState({
      timeFilter: {
        lastDay: "true",
      },
      currentPage: 1
    });
    await this.props.get({
      page: this.state.currentPage, 
      items: this.state.pageSize, 
      ...this.props.directionData,
      lastDay: "true",
      type: this.props.type
    });
  }

  handleLastWeek = async() => {
    this.setState({
      timeFilter: {
        lastWeek: "true",
      },
      currentPage: 1
    });
    await this.props.get({
      page: this.state.currentPage, 
      items: this.state.pageSize, 
      ...this.props.directionData,
      lastWeek: "true",
      type: this.props.type
    });
  }

  handleLastMonth = async() => {
    this.setState({
      timeFilter: {
        lastMonth: "true",
      },
      currentPage: 1
    });
    await this.props.get({
      page: this.state.currentPage, 
      items: this.state.pageSize, 
      ...this.props.directionData,
      lastMonth: "true",
      type: this.props.type
    });
  }

  handleLastYear = async() => {
    this.setState({
      timeFilter: {
        lastYear: "true",
      },
      currentPage: 1
    });
    await this.props.get({
      page: this.state.currentPage, 
      items: this.state.pageSize, 
      ...this.props.directionData,
      lastYear: "true",
      type: this.props.type
    });
  }

  handleCheck = (name) => {
    const checked = this.state.checked[name];
    let { attachedTags } = this.state;
    this.setState({
      checked: {
        ...this.state.checked,
        [name]: !this.state.checked[name]
      }
    })
    if(checked)
      attachedTags = attachedTags.filter(item => item !== name)
    else
      attachedTags.push(name)
    this.setState({ attachedTags })
  }

  handleChangeValue = (name, e) => {
    let { tagsValues } = this.state;
    tagsValues[name] = e.target.value
    this.setState({ tagsValues })
  }

  componentDidMount = async () => {
    this.updateWidth();
    window.addEventListener("resize", this.updateWidth);
    const directionData = this.props.directionData || {};
    this.getMatchStatusOptions(this.state.selectedMatchStatuses)
    this.getRiskLevelsOptions(this.state.selectedRiskLevels)
    if(this.props.type) {
      await this.props.get({
        page: 1, 
        items: this.state.pageSize, 
        ...directionData, 
        type: this.props.type
      });
    }
    else
      await this.props.get({
        page: 1, 
        ...directionData, 
        items: this.state.pageSize, 
      });
    
    if(this.props.getTags)
      await this.props.getTags({})
  };

  componentDidUpdate = async(prevProps) => {
    const directionData = this.props.directionData || {};
    prevProps = {
      ...prevProps,
      directionData: prevProps.directionData || {}
    }
    if(directionData.account !== prevProps.directionData.account) {
      if(this.props.type) {
        await this.props.get({
          page: 1, 
          items: this.state.pageSize, 
          ...directionData, 
          type: this.props.type
        });
      }
      else
        await this.props.get({
          page: 1, 
          ...directionData, 
          items: this.state.pageSize, 
        });
    }
  }

  getMatchStatusOptions = (selectedMatchStatuses) => {
    const matchStatusOptions = [
      ...[
        { name: "All" },
        ...matchStatuses
      ].map(item => {
        item.checked = selectedMatchStatuses.includes(item.name)
        return {
          checked: item.checked,
          // code: item.code,
          value: item.name,
          label: this.getDropdownOption(item)
        }
      })
    ]

    const selectedMatchStatusesOptions = matchStatusOptions.filter(o => o.checked)

    if(selectedMatchStatuses.length > 0 && !selectedMatchStatuses.includes("All")) {
      const { search } = this.state
      let match_status = ""
      match_status = selectedMatchStatuses.map(status => matchStatusRefs[status]).join(",")
      this.setState({
        search: {
          ...search,
          match_status
        }
      })
    }

    this.setState({
      matchStatusOptions,
      selectedMatchStatusesOptions
    })
  }

  getRiskLevelsOptions = (selectedRiskLevels) => {
    const riskLevelsOptions = [
      ...[
        { name: "All" },
        ...riskLevels
      ].map(item => {
        item.checked = selectedRiskLevels.includes(item.name)
        return {
          checked: item.checked,
          // code: item.code,
          value: item.name,
          label: this.getDropdownOption(item)
        }
      })
    ]

    const selectedRiskLevelsOptions = riskLevelsOptions.filter(o => o.checked)

   
    if(selectedRiskLevels.length > 0 && !selectedRiskLevels.includes("All")) {
      const { search } = this.state
      let risk_level = ""

      risk_level = selectedRiskLevels.map(status => riskLevelsRefs[status]).join(",")

      this.setState({
        search: {
          ...search,
          risk_level
        }
      })
    }

    this.setState({
      riskLevelsOptions,
      selectedRiskLevelsOptions
    })
  }

  handleMatchStatusOptionClick = options => {
    const allOption = options.find(item => item.value === "All");
    let selectedMatchStatuses = options.map(item => item.value)
    const isAllInSelected = this.state.selectedMatchStatuses.find(item => item === "All")

    // first click on ALL
    if(allOption && !isAllInSelected)
      selectedMatchStatuses = [
        allOption.value,
        ...matchStatuses.map(item => item.name)
      ]
    // click when ALL is checked
    else if(allOption && isAllInSelected)
      // not all items are checked
      if(options.length < matchStatuses.length + 1)
        selectedMatchStatuses = selectedMatchStatuses.filter(code => code !== "All")
      // all items are checked
      else
        selectedMatchStatuses = selectedMatchStatuses
    // uncheck ALL
    else if(!allOption && isAllInSelected)
      selectedMatchStatuses = []
    else if(!allOption && !isAllInSelected)
      if(options.length === matchStatuses.length)
        selectedMatchStatuses.push("All")

    this.setState({
      selectedMatchStatuses
    })
    this.getMatchStatusOptions(selectedMatchStatuses);
  }

  handleRiskLevelOptionClick = options => {
    const allOption = options.find(item => item.value === "All");
    let selectedRiskLevels = options.map(item => item.value)
    const isAllInSelected = this.state.selectedRiskLevels.find(item => item === "All")

    // first click on ALL
    if(allOption && !isAllInSelected)
      selectedRiskLevels = [
        allOption.value,
        ...riskLevels.map(item => item.name)
      ]
    // click when ALL is checked
    else if(allOption && isAllInSelected)
      // not all items are checked
      if(options.length < matchStatuses.length + 1)
        selectedRiskLevels = selectedRiskLevels.filter(code => code !== "All")
      // all items are checked
      else
        selectedRiskLevels = selectedRiskLevels
    // uncheck ALL
    else if(!allOption && isAllInSelected)
      selectedRiskLevels = []
    else if(!allOption && !isAllInSelected)
      if(options.length === riskLevels.length)
        selectedRiskLevels.push("All")

    this.setState({
      selectedRiskLevels
    })
    this.getRiskLevelsOptions(selectedRiskLevels);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.directionData && prevState.directionData.direction !== nextProps.directionData.direction) {
      if(nextProps.type)
        nextProps.get({
          page: 1, 
          items: prevState.pageSize, 
          ...nextProps.directionData, 
          type: nextProps.type
        });
      else
        nextProps.get({
          page:1, 
          items: prevState.pageSize
        });
      return {
        directionData: nextProps.directionData
      };
    }
    return null;
  }

  updateWidth = () => {
    this.setState({
      width: window.outerWidth
    });
  }

  componentWillUnmount = () => {
    // this.props.reset();
  }

  handleSelectPageSize = async(e) => {
    const directionData = this.props.directionData || {};
    const searchData = {
      ...this.props.searchData,
      ...this.props.directionData
    };

    if (e.name === "All") {
      if (this.state.isSearch)
        await this.props.get({
          page: 1, 
          items: this.props.count, 
          ...searchData, 
          type: this.props.type,
          ...this.state.timeFilter
        });
      else
        await this.props.get({
          page: 1, 
          items: this.props.count, 
          ...directionData, 
          type: this.props.type,
          ...this.state.timeFilter
        });
      this.setState({
        pageSize: String(this.props.count),
        currentPage: 1
      });
    }
    else {
      if (this.state.isSearch)
        await this.props.get({
          page: 1, 
          items: e.name, 
          ...searchData, 
          type: this.props.type,
          ...this.state.timeFilter
        });
      else
        await this.props.get({
          page: 1, 
          items: e.name, 
          ...directionData, 
          type: this.props.type,
          ...this.state.timeFilter
        });
      this.setState({
        pageSize: e.name,
        currentPage: 1
      });
    }
  }

  handlePageChange = async (page) => {
    const directionData = this.props.directionData || {};
    const searchData = {
      ...this.props.searchData,
      ...this.props.directionData
    };

    if (this.state.isSearch)
      await this.props.get({
        page, 
        items: this.state.pageSize, 
        ...searchData, 
        type: this.props.type,
        ...this.state.timeFilter,
        ...this.state.search
      });
    else
      await this.props.get({
        page, 
        items: this.state.pageSize, 
        ...directionData, 
        type: this.props.type,
        ...this.state.timeFilter,
        ...this.state.search
      });
    this.setState({
      currentPage: page
    });
  };

  handleSearch = async () => {
    const searchData = {
      ...this.props.searchData,
      ...this.props.directionData
    };
    this.setState({
      isSearch: true,
      currentPage: 1
    });
    await this.props.get({
      page: 1, 
      items: this.state.pageSize, 
      ...searchData, 
      type: this.props.type,
      ...this.state.timeFilter
    });
  }

  handleReset = async () => {
    const directionData = this.props.directionData || {};
    this.setState({
      isSearch: false,
      currentPage: 1
    });
    await this.props.reset();
    await this.props.get({
      page: 1, 
      items: this.state.pageSize, 
      ...directionData, 
      type: this.props.type,
      ...this.state.timeFilter
    });
  }

  alertModal = async (action, txId, type) => {
    let status = action;
    if(action.toLowerCase() === "unapprove")
      status = "approve";
    const willDo = await swal({
      title: "Are you sure?",
      text: `You will ${status.toLowerCase()} this transaction`,
      icon: "warning",
      buttons: true,
      dangerMode: true
    });
    if(willDo)
      this.handleEditTxStatus(this.state.statuses[action.toLowerCase()], txId, type, false);
  }

  alertCancellationModal = async (action, guid, type, text) => {
    // let status = action;
    // if(action.toLowerCase() === "unapprove")
    //   status = "approve";
    const willDo = await swal({
      title: "Are you sure?",
      text: `You will ${text.toLowerCase()} original transaction`,
      icon: "warning",
      buttons: true,
      dangerMode: true
    });
    if(willDo)
      this.handleEditTxStatus(action, guid, type, true, text === "investigate");
  }

  alertInboxModal = async (action, inboxId) => {
    let status = action;
    if(action.toLowerCase() === "received")
      status = "Imported";
    const willDo = await swal({
      title: "Are you sure?",
      text: `You will ${status.toLowerCase()} this transaction`,
      icon: "warning",
      buttons: true,
      dangerMode: true
    });
    if(willDo)
      this.handleEditInboxStatus( 
        inboxId,
        status,
        "LITAS-MIG" 
      );
  }

  handleEditInboxStatus = async(guid, status, system) => {
    const searchData = {
      ...this.props.searchData,
      ...this.props.directionData
    };
    try {
      // await editInboxStatus({
      //   guid,
      //   status, 
      //   system
      // });
      await this.props.editInboxStatusAction({
        status,
        system: "LITAS-MIG"
      }, guid, searchData, this.state.currentPage, this.state.pageSize);      
      swal(status, {
        icon: "success",
        button: false,
        timer: 2000
      });
    }
    catch(error) {
      const parsedError = parseResponse(error);
      if((error.response && error.response.status !== 409) || !error.response)
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
    }
  }

  handleEditTxStatus = async(status, txId, type, isImported, isInvestigate) => {
    try {
      this.setState({ status });
      const txType = this.props.type === "all" ? type : this.props.type;
      const data = isInvestigate
      ? {
        status,
        system: "LITAS-MIG",
        cxlStsRsnInf: this.state.roinvstgReason,
        cxlStsRsnInfAddtInf: this.state.roinvstgAddtInf,
        isImported
      }
      : {
        status,
        system: "LITAS-MIG",
        isImported
      }
      await this.props.editTxStatus(data, txType, txId, this.props.type, this.props.directionData, this.state.currentPage, this.state.pageSize);      
      swal(status === "Unapproved" ? "Wait to be approve by admin" : status, {
        icon: "success",
        button: false,
        timer: 2000
      });
    }
    catch(error) {
      const parsedError = parseResponse(error);
      if((error.response && error.response.status !== 409) || !error.response)
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
    }
  }

  tableButton = (style, status, text, txId, type) => (
    <Button
      className={`submit-button ${style.toLowerCase()}`}
      onClick={async() => await this.alertModal(status.toLowerCase(), txId, type)}
    >
      {text.charAt(0).toUpperCase() + text.slice(1)}
    </Button>
  )

  tableCancellationButton = (style, status, text, guid, type) => (
    <Button
      className={`submit-button ${style.toLowerCase()}`}
      onClick={async() => await this.alertCancellationModal(status, guid, type, text.toLowerCase())}
      disabled={text === "Investigate" ? this.state.roinvstgValidation : false}
    >
      {text.charAt(0).toUpperCase() + text.slice(1)}
    </Button>
  )

  tableInboxButton = (style, status, text, inboxId) => (
    <Button
      className={`submit-button ${style.toLowerCase()}`}
      onClick={async() => this.alertInboxModal(status, inboxId)}
    >
      {text.charAt(0).toUpperCase() + text.slice(1)}
    </Button>
  )

  roinvstgValidation = () => {
    if(this.state.roinvstgReason === "LEGL")
      return this.state.roinvstgAddtInf
    else
      return this.state.roinvstgReason
  }

  handleReasonSelect = async (option) => {
    await this.setState({
      roinvstgReason: option.guid
    });
    await this.setState({
      roinvstgValidation: this.roinvstgValidation()
    });
  }

  handleAddtInfChange = async (e) => {
    await this.setState({
      [e.target.name]: e.target.value
    });
    await this.setState({
      roinvstgValidation: this.roinvstgValidation()
    });
  }

  getCheckboxLabel = (columns) => {
    if(columns[0].key === "checkedCheckboxTxTemplate") {
      const isChecked = this.props.checkedTemplates.length >= this.props.data.length && this.props.checkedTemplates.length !== 0
      columns[0].label = <><br/><input type="checkbox" style={{ cursor: "pointer", width: "1vw", height: "1vw"  }} checked={isChecked} onChange={() => this.props.changeTemplateCheckbox(isChecked ? [] : this.props.data)}/></>;
      columns[0].content = (item) => <input type="checkbox" style={{ cursor: "pointer", width: "1vw", height: "1vw" }} checked={this.props.checkedTemplates.map(i => i.guid).includes(item.guid)} onChange={() => this.props.changeTemplateCheckbox(item)}/>
    }
    else if(columns[0].key === "checkedCheckboxWaiting") {
      const isChecked = this.props.checkedWaiting.length >= this.props.data.length && this.props.checkedWaiting.length !== 0
      columns[0].label = <><br/><input type="checkbox" style={{ cursor: "pointer", width: "1vw", height: "1vw"  }} checked={isChecked} onChange={() => this.props.changeWaitingCheckbox(isChecked ? [] : this.props.data)}/></>;
      columns[0].content = (item) => <input type="checkbox" style={{ cursor: "pointer", width: "1vw", height: "1vw"  }} checked={this.props.checkedWaiting.map(i => i.guid).includes(item.guid)} onChange={() => this.props.changeWaitingCheckbox(item)}/>
    }
    return columns
  }

  addApprovedOrCancelledColumnContent = (columns) => {
    if(columns[columns.length - 1].key === "editTxStatus") {
      let approveColumn = columns[columns.length - 1];
      const statusColumn = columns.find(column => column.path === "status");
      const typeColumn = columns.find(column => column.path === "type") || { path: "type" };
      approveColumn.content = (item) => (     
        (item[typeColumn.path] = item[typeColumn.path] || this.props.type) &&
        item[statusColumn.path] === "Unapproved" ?
        <>
        <div>
          {this.tableButton("approve", "approve", "approve", item[columns[0].path], item[columns[2].path])}
        </div>
        <div>
          {this.tableButton("cancel", "decline", "decline", item[columns[0].path], item[columns[2].path])}
        </div>
        </>
        : item[statusColumn.path] === "Imported" && item[typeColumn.path] === "FFPCRQST" &&
        <Modal
          header="Decide"
          content={
            <div>
              <div className="decide-ffpcrqst-text">
                Original transaction id: <Link className="link" to={`transactions/ffcctrns/${item.originalTxGuid}`}>
                  {item.originalTxGuid}
                </Link>
                <br/>
                Transaction can be returned or investigated</div>
              <div className="decide-ffpcrqst-wrapper">
                <div className="decide-ffpcrqst-left">
                  {this.tableCancellationButton("submit-button approve", "Imported (Returned)", "Return", item.guid, item[columns[2].path])}
                </div>
                <div className="decide-ffpcrqst-right">
                  <div className="decide-ffpcrqst-right-select">
                    <ControlLabel
                      className="col"
                      style={{ textTransform: "none" }}
                    >
                      Reason
                    </ControlLabel>
                    <Select
                      key="reason"
                      options={[
                        {
                          guid: "AM04",
                          name: "There are insufficient funds on the account"
                        },
                        {
                          guid: "AC04",
                          name: "The account is closed"
                        },
                        {
                          guid: "LEGL",
                          name: "For legal reasons, specifying the reason in clear text"
                        },
                        {
                          guid: "CUST",
                          name: "Refusal by the Creditor to return"
                        },
                        {
                          guid: "NOAS",
                          name: "No answer is received from the Creditor"
                        },
                        {
                          guid: "NOOR",
                          name: "The funds were not received"
                        },
                        {
                          guid: "ARDT",
                          name: "The funds have already been returned"
                        }
                      ]}
                      styles={{
                        // ...selectorStyles,
                        menu: (styles) => {
                          return {
                            ...styles,
                            textAlign: "left !important"
                          };
                        }
                      }}
                      selectedValue={this.state.roinvstgReason}
                      onSelect={this.handleReasonSelect}
                      placeholder="Select reason..."
                    />
                  </div>
                  {this.state.roinvstgReason === "LEGL" &&
                  <div className="decide-ffpcrqst-right-addtinf">
                    <Input
                      label="Additional info"
                      name="roinvstgAddtInf"
                      value={this.state.roinvstgAddtInf}
                      onChange={this.handleAddtInfChange}
                    />
                  </div>}
                  {this.tableCancellationButton("submit-button cancel", "Imported (Investigated)", "Investigate", item.guid, item[columns[2].path])}
                </div>
              </div>
            </div>
          }
          state="decide-ffpcrqst"
          button={
            <Button
              className="submit-button small"
              // style={ { position: "absolute", marginLeft: "0", top:"20px", marginBottom: "-15px" } }
            >
              Decide
            </Button>
          }
        />
      )
      columns[columns.length - 1] = approveColumn;
    }
    else if(columns[columns.length - 1].key === "approve/cancel") {
      let cancelColumn = columns[columns.length - 1];
      const [ statusColumn ] = columns.filter(column => column.label === "Status");
      const [ typeColumn ] = columns.filter(column => column.label === "Type");
      if(userType === "entity") {
        cancelColumn.content = (item) => (
          item[statusColumn.path] === "Unconfirmed" &&
          [ "INTERNAL", "FFCCTRNS" ].includes(item[typeColumn.path]) &&
          <div>
            {this.tableButton("cancel", "cancel", "cancel", item[columns[0].path], item[columns[2].path])}
          </div>
        );
        cancelColumn.label = "Cancel";
      }
      else if(userType === "individual") {
        ability("APPROVE_CANCEL_TRANSACTIONS") 
          ?
          cancelColumn.content = (item) => (
          <>          
          {item[statusColumn.path] === "Unconfirmed" &&
          [ "FFCCTRNS" ].includes(item[typeColumn.path]) &&
          <>
          <div>
            {this.tableButton("approve", "unapprove", "approve", item[columns[0].path], item[columns[2].path])}
          </div>
          <div>
            {this.tableButton("cancel", "cancel", "cancel", item[columns[0].path], item[columns[2].path])}
          </div>          
          </>}
          {item[statusColumn.path] === "Unapproved" &&
            <div>
              {this.tableButton("cancel", "cancel", "cancel", item[columns[0].path], item[columns[2].path])}
            </div>}
          </>
          )
          : columns.pop();
      }
    }
    else if(columns[columns.length - 1].key === "editInboxStatus") {
      let approveColumn = columns[columns.length - 1];
      const [ statusColumn ] = columns.filter(column => column.label === "Status");
      const [ typeColumn ] = columns.filter(column => column.label === "Type");
      approveColumn.content = (item) => (
        item[statusColumn.path] === "Received" &&
        [ /* "FFPCRQST", */ "FFCCTRNS"/* , "PRTRN" */ ].includes(item[typeColumn.path]) &&
        <>
        <div>
          {this.tableInboxButton("approve", "Imported", "approve", item[columns[0].path])}
        </div>
        <div>
          {this.tableInboxButton("cancel", "Import declined", "decline", item[columns[0].path])}
        </div>
        </>
      );
      columns[columns.length - 1] = approveColumn;
    }
    return columns;
  }

  
  getPagedData = () => {
    const {
      pageSize
    } = this.state;
    const { data = [], count } = this.props;
    const pagesCount = Math.ceil(count / pageSize);
    return { pagesCount, data };
  };

  styles = {
    placeholder: (styles) => {
      return {
        ...styles,
        color: "grey",
        fontSize: "calc(7px + 0.5vw) !important"
      };
    },
    menu: (styles) => {
      return {
        ...styles,        
        top: this.state.width > 1000 ? "" : "-145px !important",
        bottom: "100%",
        width: "calc(50px + 10vw)",        
        position: "absolute !important",
        right: "0px !important",
        background: "rgba(0,0,0,0.6)",
        color: "lightgrey !important"
      };
    },
    option: (styles, state) => {
      return {
        ...styles,
        background: state.isSelected ? "grey !important" : "rgba(0,0,0,0.6)",
        color: state.isSelected ? "black !important" : "lightgrey !important"
      };
    },
    control: (styles) => {
      return {
        ...styles,
        width: "calc(50px + 10vw)",        
        position: "absolute !important",
        right: "0px !important",
        background: "transparent",
        color: "white !important",
        border: "1px solid #545761 !important",
        boxShadow: "none !important",
        marginTop: this.state.width > 1000 ? "" : "50px"
      };
    },
    singleValue: (styles) => {
      return {
        ...styles,
        color: "grey !important"
      };
    }
  }

  getDropdownOption = (item) => (
    <>
      <span className="aml-selector-option">
        <span>{item.name}</span>
      </span>
      <span className="aml-selector-option-checkbox">
        <input type="checkbox" checked={item.checked}/>
      </span>
    </>
  )

  

  handleChangeSearch = e => {
    const { search } = this.state;
    this.setState({ 
      search: { 
        ...search,
        [e.target.name]: e.target.value
      } 
    })
  }

  handleApplyFilters = async() => {
    let { search, attachedTags, tagsValues } = this.state;
    Object.keys(search).forEach(key => {
      if(search[key] === "")
        delete search[key]
    })

    attachedTags.forEach(tag => {
      search[tag] = tagsValues[tag] || "undefined"
    })

    await this.props.get({
      page: 1, 
      items: this.state.pageSize, 
      ...this.state.search
    });
  }

  handleResetFilters = async() => {
    const riskLevelsOpts = [
      "All",
      ...riskLevels.map(i => i.name)
    ]
    this.getRiskLevelsOptions(riskLevelsOpts)

    const matchStatusOpts = [
      "All",
      ...matchStatuses.map(i => i.name)
    ]
    this.getMatchStatusOptions(matchStatusOpts)


    this.setState({
      search: {},
      attachedTags: [],
      tagsValues: {},
      checked: {},
      datePlaceholder: "All",
      currentPage: 1
    })

    await this.props.get({
      page: 1, 
      items: this.state.pageSize
    });
  }

  handleConfirm = () => {
    window.location.replace(process.env.PUBLIC_URL + "#/waitingpayments" || "#/waitingpayments")
  }

  handleCreate = () => {
    window.location.replace(process.env.PUBLIC_URL + "#/templatessubmit" || "#/templatessubmit")
  }

  handleDelete = async() => {
    try {
      if(this.props.name === "waiting payments") {
        const guids = this.props.checkedWaiting.map(i => i.guid)
        await this.props.upsertTxsTemplates({
          guids,
          del: true
        })
        swal({
          title: "Payments deleted",
          text: `Status: ${this.props.response.status}`,
          icon: "success"
        });
        this.props.changeWaitingCheckbox([])
      }
      else {
        const guids = this.props.checkedTemplates.map(i => i.guid)
        await this.props.upsertTxsTemplates({
          guids,
          del: true
        })
        swal({
          title: "Templates deleted",
          text: `Status: ${this.props.response.status}`,
          icon: "success"
        });
        this.props.changeTemplateCheckbox([])

      }
      await this.props.get({
        page: 1, 
        ...(this.props.directionData || {}), 
        items: this.state.pageSize, 
      });
    }
    catch(error) {
      const parsedError = parseResponse(error);
      if ((error.response && error.response.status !== 409) || !error.response)
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
    }
  }

  getSumAmount = () => {
    if(this.props.name === "waiting payments" && this.props.checkedWaiting && this.props.checkedWaiting.length > 0) {
      let sum = 0
      this.props.checkedWaiting.forEach(i => {
        sum += parseFloat(i.amount) 
      })
      return sum
    }
    if(this.props.name === "transaction templates" && this.props.checkedTemplates && this.props.checkedTemplates.length > 0) {
      let sum = 0
      this.props.checkedTemplates.forEach(i => {
        sum += parseFloat(i.amount) 
      })
      return sum
    }
    return null
  }

  render() {
    let { tags } = this.props;
    const { currentPage } = this.state;
    const { pagesCount, data } = this.getPagedData();

    const { search, name, type, isSearch } = this.props;

    const isWaiting = this.props.name === "waiting payments" && this.props.checkedWaiting && this.props.checkedWaiting.length > 0
    const isTemplates = this.props.name === "transaction templates" && this.props.checkedTemplates && this.props.checkedTemplates.length > 0

    
    if (this.props.loading) return <Spinner style={{ marginTop: "20%" }} />;
    else {
      const sum = this.getSumAmount()
      let columns = this.addApprovedOrCancelledColumnContent(this.props.columns);
      columns = this.getCheckboxLabel(columns)
      return (
        <div>
          <div className={isSearch ? "show" : "hid"}>
            <Button
              id="search-button"
              className={"btn search-button"}
              onClick={this.handleSearch}
            >
              Search
            </Button>
            <Button
              className={"btn reset-search-button"}
              onClick={this.handleReset}
            >
              Reset
            </Button>
          </div>
          {this.props.amlSearch &&
          <Row className="aml-history-search-wrapper">
            <Row className="aml-history-search-wrapper-inputs">
              <Col md={4} className="search-col">
                <ControlLabel
                  className="col-label"
                  style={{ textTransform: "none" }}
                >
                  Created:
                </ControlLabel>
                <DateRangePicker 
                  className="btn" 
                  startDate={this.state.search.created_at_from ? moment(this.state.search.created_at_from).format("MM/DD/YYYY") : moment(date).format("MM/DD/YYYY")} 
                  endDate={this.state.search.created_at_to ? moment(this.state.search.created_at_to).format("MM/DD/YYYY") : moment(date).format("MM/DD/YYYY")}
                  onApply={this.handleSetAmlDates}
                >
                  <Dropdown
                    amlSearch={true}
                    doNotOpen={true}
                    placeholder={this.state.datePlaceholder}
                    options={[]}
                    selectedOptions={[]}
                    onClick={() => {}}
                  />
                </DateRangePicker>
              </Col>
              <Col md={4} className="search-col">
                <Input
                  label="Search by name:"
                  name="name"
                  value={this.state.search.name}
                  onChange={(e) => this.handleChangeSearch(e)}
                  style={{
                    input: {
                      fontSize: "calc(12px + 0.3vw)"
                    }
                  }}
                />
              </Col>
              <Col md={4} className="search-col">
                <Input
                  label="Search by ref:"
                  name="client_ref"
                  value={this.state.search.client_ref}
                  onChange={this.handleChangeSearch}
                  style={{
                    input: {
                      fontSize: "calc(12px + 0.3vw)"
                    }
                  }}
                />
              </Col>
              <Col md={4} className="search-col">
                <ControlLabel
                  className="col-label"
                  style={{ textTransform: "none" }}
                >
                  Tagged:
                </ControlLabel>
                <DropdownTag
                  amlSearch={true}
                  className="card selector-aml-tag"
                  options={tags || []}
                  checked={this.state.checked || {}}
                  tagsValues={this.state.tagsValues}
                  attachedTags={this.state.attachedTags}
                  // selectedOptions={this.state.selectedMatchStatusesOptions || []}
                  handleCheck={this.handleCheck}
                  handleChangeValue={this.handleChangeValue}
                />
              </Col>
              <Col md={4} className="search-col">
                <ControlLabel
                  className="col-label"
                  style={{ textTransform: "none" }}
                >
                  Match status:
                </ControlLabel>
                <Dropdown
                  amlSearch={true}
                  className="card selector-aml-ms"
                  options={this.state.matchStatusOptions || []}
                  selectedOptions={this.state.selectedMatchStatusesOptions || []}
                  onClick={this.handleMatchStatusOptionClick}
                />
              </Col>
              <Col md={4} className="search-col">
                <ControlLabel
                  className="col-label"
                  style={{ textTransform: "none" }}
                >
                  Risk level:
                </ControlLabel>
                <Dropdown
                  amlSearch={true}
                  className="card selector-aml-rl"
                  options={this.state.riskLevelsOptions || []}
                  selectedOptions={this.state.selectedRiskLevelsOptions || []}
                  onClick={this.handleRiskLevelOptionClick}
                />
              </Col>
            </Row>
            <Row className="aml-history-search-wrapper-buttons">
              <Col md={4} className="search-col">
                <Button
                  className={Object.keys(this.state.search).length + this.state.attachedTags.length === 0 ? "aml-search-filters apply-button disabled" : "aml-search-filters apply-button"}
                  onClick={this.handleApplyFilters}
                >
                  Apply filters
                </Button>
              </Col>
              <Col md={4} className="search-col">
                <Button
                  className="aml-search-filters reset-button"
                  onClick={this.handleResetFilters}
                >
                  Reset filters
                </Button>
              </Col>
            </Row>
          </Row>}
          {!this.props.amlSearch && !this.props.disableDate &&
          <Row>
            <Col md={5}/>
            <Col md={7}>
              {/* <ExportButton
                id="table"
                table={true}
                columns={columns}
                data={this.props.data}
                name={this.props.name}
              >
                export
              </ExportButton> */}
              {this.props.type && 
              <div
                className="date-div"
              >            
              <Button
                className={"btn"}
                onClick={this.handleLastDay}
              >
                Last day
              </Button>
              <Button
                className={"btn"}
                onClick={this.handleLastWeek}
              >
                Last week
              </Button>
              <Button
                className={"btn"}
                onClick={this.handleLastMonth}
              >
                Last month
              </Button>
              <Button
                className={"btn"}
                onClick={this.handleLastYear}
              >
                Last year
              </Button>
              <DateRangePicker 
                className="btn" 
                startDate={moment(this.state.startDate).format("MM/DD/YYYY")} 
                endDate={moment(this.state.endDate).format("MM/DD/YYYY")}
                onApply={this.handleSetDates}
              >
                <Button
                  className={"btn"}
                >
                  Other...
                </Button>
              </DateRangePicker>
              <Button
                className={"btn"}
                onClick={this.handleAll}
              >
                All
              </Button>
              </div>}
            </Col>
          </Row>}
          <Table
            columns={columns}
            search={search}
            data={data}
            name={name}
            transactionType={type}
            disableSearch={this.props.disableSearch}
          />
          
          {isWaiting || isTemplates
            ? 
            <>
              <Col md={6} lg={6} xs={6} style={{ marginTop: "100px" }}>
                <Pagination
                  pagesCount={pagesCount}
                  currentPage={currentPage}
                  onPageChange={this.handlePageChange}
                  pageSize={this.state.pageSize}
                  count={this.props.count}
                />
              </Col>
              {isWaiting 
              ? 
              <>
                <Col md={4} lg={4} xs={4} style={{ marginTop: "17px" }}>
                  <Button className="submit-button delete" onClick={this.handleDelete}>
                    Delete transactions
                  </Button>   
                  <Button className={sum.toFixed(2) <= this.props.balance ? "submit-button confirm" : "disabled submit-button confirm"} onClick={this.handleConfirm}>
                    Confirm payments
                  </Button>
                </Col>
                <div className="templates-all-amount">
                  Total: {sum} EUR
                </div>
              </>
              : 
              <>
                <Col md={4} lg={4} xs={4} style={{ marginTop: "17px" }}>
                  <Button className="submit-button delete" onClick={this.handleDelete}>
                    Delete templates
                  </Button>   
                  <Button className={sum.toFixed(2) <= this.props.balance ? "submit-button confirm" : "disabled submit-button confirm"} onClick={this.handleCreate}>
                    Create transactions
                  </Button>
                </Col>                
                <div className="templates-all-amount">
                  Total: {sum} EUR
                </div>
              </>}
              <Col md={2} lg={2} xs={2} style={{ marginTop: "100px", paddingRight: "0" }}>
                <Select
                  multi={false}
                  name="Items per page"
                  placeholder="Rows per page"
                  options={[ { guid: "1", name: "10" }, { guid: "2", name: "20" }, { guid: "3", name: "50" }, { guid: "4", name: "100" }, { guid: "5", name: "All" } ]}
                  onSelect={this.handleSelectPageSize}
                  isSearchable={false}
                  styles={this.styles}
                />
              </Col>
            </>
            :
            <>
              <Col md={10} lg={10} xs={10}>
                <Pagination
                  pagesCount={pagesCount}
                  currentPage={currentPage}
                  onPageChange={this.handlePageChange}
                  pageSize={this.state.pageSize}
                  count={this.props.count}
                />
              </Col>
              <Col md={2} lg={2} xs={2} style={{ marginTop: "20px", paddingRight: "0" }}>
                <Select
                  multi={false}
                  name="Items per page"
                  placeholder="Rows per page"
                  options={[ { guid: "1", name: "10" }, { guid: "2", name: "20" }, { guid: "3", name: "50" }, { guid: "4", name: "100" }, { guid: "5", name: "All" } ]}
                  onSelect={this.handleSelectPageSize}
                  isSearchable={false}
                  styles={this.styles}
                />
              </Col>
            </>}
          
        </div>
      );
    }
  }
}

AbstractComponent.propTypes = {
  data: PropTypes.array,
  count: PropTypes.number,
  columns: PropTypes.array,
  get: PropTypes.func,
  delete: PropTypes.func,
  search: PropTypes.func,
  reset: PropTypes.func,
  loading: PropTypes.bool,
  searchData: PropTypes.object,
  type: PropTypes.string,
  isSearch: PropTypes.bool,
  name: PropTypes.string,
  directionData: PropTypes.object,
  editTxStatus: PropTypes.func,
  disableSearch: PropTypes.bool
};