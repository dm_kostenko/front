import React, { Component } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import { routes } from "./routes/index.jsx";
// import { getPermissions } from "./services/paymentBackendAPI/backendPlatform";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/animate.min.css";
import "./assets/css/demo.css";
import "./assets/css/pe-icon-7-stroke.css";
import LoginPage from "ibankComponents/LoginPage";
import Logout from "ibankComponents/Logout";
import ForgotPassword from "ibankComponents/ForgotPassword";
import RecoveryPassword from "ibankComponents/RecoveryPassword";
import ExpiredPassword from "ibankComponents/ExpiredPassword";
import ExpiresPassword from "ibankComponents/ExpiresPassword";
import FirstTimeLogin from "ibankComponents/FirstTimeLogin";
import Redirect from "react-router-dom/es/Redirect";
import "assets/css/content.css";
// import "assets/css/new-content.css";
import "./assets/sass/light-bootstrap-dashboard.css?v=1.2.0";
import Loading from "ibankComponents/UI/Spinner";
import auth from "./services/paymentBackendAPI/auth/auth";
import { checkToken } from "services/paymentBackendAPI/backendPlatform";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role: "",
      user: "",
      isTokenPresented: localStorage.getItem("isLoggedIn"),
      isTokenChecked: false
    };
  }

  componentDidMount = async () => {
    document.title = `TBF Finance - ${window.location.hostname}`;
    if (this.state.isTokenPresented) {
      try {
        checkToken();
        await auth.validate();
      }
      catch (error) {
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
      this.setState({
        // role: window.sessionStorage.getItem("Role"),
        // permissions: getPermissions(),
        isTokenChecked: true
      });
    }
  }

  render() {

    if (this.state.isTokenPresented && !this.state.isTokenChecked)
      return <Loading />;

    const LoginRoute = ({ ...rest }) => (
      <Route {...rest} render={() => (
        localStorage.getItem("isLoggedIn") === "false" || !localStorage.getItem("isLoggedIn")
          ? <Route path='/login' component={LoginPage} />
          : <Redirect to='/' />
      )} />
    );

    const ForgotPasswordRoute = ({ component: Component, ...rest }) => (
      <Route {...rest} render={() => (
        <Route path='/forgot' component={ForgotPassword} />
      )} />
    );

    const RecoveryPasswordRoute = ({ component: Component, ...rest }) => (
      <Route {...rest} render={() => (
        <Route path="/recovery_password/:token" component={RecoveryPassword} /> 
      )} />
    );

    const ExpiredPasswordRoute = ({ component: Component, ...rest }) => (
      <Route {...rest} render={() => (
        localStorage.getItem("isLoggedIn") === "true" && localStorage.getItem("expiredPassword") === "true" 
          ? <Route path="/expired_password" component={ExpiredPassword} /> 
          : <Redirect to='/login' />
      )} />
    );

    const ExpiresPasswordRoute = ({ component: Component, ...rest }) => (
      <Route {...rest} render={() => (
        localStorage.getItem("isLoggedIn") === "true" && localStorage.getItem("expiresPassword") === "true" 
          ? <Route path="/expires_password" component={ExpiresPassword} /> 
          : <Redirect to='/login' />
      )} />
    );

    const FirstTimeLoginRoute = ({ component: Component, ...rest }) => (
      <Route {...rest} render={() => (
        localStorage.getItem("isLoggedIn") === "true" && localStorage.getItem("firstTimeLogin") === "true" 
          ? <Route path="/first_time_login" component={FirstTimeLogin} /> 
          : <Redirect to='/login' />
      )} />
    );


    const PrivateRoute = ({ component: Component, ...rest }) => (
      <Route {...rest} render={(props) => (
        localStorage.getItem("isLoggedIn") === "true" && 
        (localStorage.getItem("firstTimeLogin") !== "true" && localStorage.getItem("expiresPassword") !== "true" && localStorage.getItem("expiredPassword") !== "true")
          ? <Component {...props} />
          : <Redirect to='/login' />
      )} />
    );


    let route;
    route = routes.map((prop, key) => {
      return <PrivateRoute to={prop.path} component={prop.component} key={key} />;
    });

    return (
      <HashRouter>
        <Switch>
          {<LoginRoute path='/login' component={LoginPage} />}
          {<ForgotPasswordRoute path='/forgot' component={ForgotPasswordRoute} />}
          {<RecoveryPasswordRoute path='/recovery_password/:token' component={RecoveryPassword} />}
          {<ExpiredPasswordRoute path='/expired_password' component={ExpiredPassword} />}
          {<ExpiresPasswordRoute path='/expires_password' component={ExpiresPassword} />}
          {<FirstTimeLoginRoute path='/first_time_login' component={FirstTimeLogin} />}
          {<PrivateRoute path='/logout' component={Logout} />}
          {route}
        </Switch>
      </HashRouter>
    );
  }
}


export default App;
