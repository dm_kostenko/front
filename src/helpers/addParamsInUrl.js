export const addParamsInUrl = (url, params) => {
  url += "?";
  Object.keys(params).forEach(key => {
    url += `${key.toString()}=${params[key]}&`;
  });
  return url.slice(0, url.length - 1);
};