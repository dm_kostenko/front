module.exports.getBankFromAccountNumber = (accountNumber) => {
  switch(accountNumber.toLowerCase()) {
  case "rusimex": return "rusimex";
  case "tinkoff": return "tinkoff";
  case "gazprombank": return "gazprombank";
  case "sberbank": return "sberbank";
  case "alfabank": return "alfabank";
  default: return "";
  }
};