import Joi from "@hapi/joi";
// import { isValidIBAN } from "helpers/ibanAndBicValidation";


const validate = (data, schema, options = { allowUnknown: true, abortEarly: false }) => {
  let ibanDetails = [];
  // if(schema.ibanFields && schema.ibanFields.length > 0) {
  //   schema.ibanFields.forEach(item => {
  //     if(!isValidIBAN(data[item.field]))
  //       ibanDetails.push({
  //         field: item.field,
  //         message: `"${item.label}" must be a valid IBAN`
  //       });
  //   });
  // }
  const { error: joiError } = Joi.validate(data, schema, options);
  let joiDetails = [];
  if (joiError) {
    joiDetails = joiError.details.map(value => ({ 
      field: value.context.key, 
      message: value.message 
    }));    
  } 
  const details = [ ...joiDetails, ...ibanDetails ];
  if(details.length > 0)
    throw(details);
  else 
    return true;
};

export default validate;