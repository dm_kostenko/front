import { saveAs } from "file-saver";


export const downloadXML = (xml, docId, type) => {
  const file = new File([ unescape(xml) ], `${type.toUpperCase()}-${docId}.xml`, { type: "text/xml;charset=utf-8" });
  saveAs(file);
};