import moment from "moment";
import { getCETDate } from "helpers/target2Timing";

const dict = {
  // RESERVED: {
  //   beautify: "Reserved",
  //   modify: item => ( item.join(" / ") )
  // },
  // CURRENCIES: {
  //   beautify: "Currencies",
  //   modify: item => ( item.join(" / ") )
  // },
  // BALANCES: {
  //   beautify: "Balances",
  //   modify: item => ( item.join(" / ") )
  // },
  // MONTHLY_LIMIT: {
  //   beautify: "Monthly limit"
  // },
  // DAILY_LIMIT: {
  //   beautify: "Daily limit"
  // },
  DETAILS: {
    beautify: "Details",
    modify: (text) => { return unescape(text); }
  },
  AUTH_TYPE: {
    beautify: "Authentication type"
  },
  LOGIN_GUID: {
    beautify: "Login id"
  },
  GUID: {
    beautify: "Id"
  },
  TRANSACTION_GUID: {
    beautify: "Transaction"
  },
  RECEIVER_IBAN: {
    beautify: "Receiver"
  },
  SENDER_IBAN: {
    beautify: "Sender"
  },
  CREATED_BY: {
    beautify: "Created by"
  },
  UPDATED_BY: {
    beautify: "Updated by"
  },  
  UPDATED_AT: {
    beautify: "Updated at",
    modify: (datetime) => { return !datetime ? datetime : moment(getCETDate(new Date(datetime))).format("DD.MM.YYYY"); }
  },
  CREATED_AT: {
    beautify: "Created at",
    modify: (datetime) => { return !datetime ? datetime : moment(getCETDate(new Date(datetime))).format("DD.MM.YYYY"); }
  },
  DATE: {
    beautify: "Rate date",
    modify: (datetime) => { return !datetime ? datetime : moment(getCETDate(new Date(datetime))).format("DD.MM.YYYY"); }
  },
  VALUE: {
    beautify: "Amount"
  },
  MSGID: {
    beautify: "Message id"
  },
  DOCID: {
    beautify: "Document id"
  },
  DATESTYPE: {
    beautify: "Created at",
    modify: (datetime) => { return !datetime ? datetime : moment(getCETDate(new Date(datetime))).format("DD.MM.YYYY HH:mm:ss"); }
  },
  BUSINESSAREA: {
    beautify: "Business area"
  },
  GRPHDRMSGID: {
    beautify: "Group header message id"
  },
  GRPHDRCREDTTM: {
    beautify: "Group header credit time",
    modify: (datetime) => { return !datetime ? datetime : moment(getCETDate(new Date(datetime))).format("DD.MM.YYYY HH:mm:ss"); }
  },
  GRPHDRNBOFTXS: {
    beautify: "Number of transactions"
  },
  GRPHDRTTLINTRBKSTTLMAMT: {
    beautify: "Settlement amount"
  },
  GRPHDRINTRBKSTTLMDT: {
    beautify: "Settlement date",
    modify: (datetime) => { return !datetime ? datetime : moment(getCETDate(new Date(datetime))).format("DD.MM.YYYY"); }
  },
  GRPHDRSTTLMINFSTTLMMTD: {
    beautify: "Settlement method"
  },
  GRPHDRSTTLMINFCLRSYSPRTRY: {
    beautify: "Clearing system"
  },
  GRSTS: {
    beautify: "Group status"
  },
  RSNPRTY: {
    beautify: "Reason property"
  },
  GRPHDRINSTGAGFININSTNIDBIC: {
    beautify: "Instructing Agent BIC (same as Sender)"
  },
  TXSTS: {
    beautify: "Transaction status"
  },
  RSNCD: {
    beautify: "Reason code"
  },
  CDTTRFTXINFPMTIDINSTRID: {
    beautify: "Instruction id"
  },
  CDTTRFTXINFPMTIDENDTOENDID: {
    beautify: "End to End id"
  },
  CDTTRFTXINFPMTIDTXID: {
    beautify: "Transaction id"
  },
  CDTTRFTXINFPMTIDPMTTPINFSVCLVLCD: {
    beautify: "Service level"
  },
  CDTTRFTXINFPMTTPINFLCLINSTRM: {
    beautify: "Local instrument"
  },
  CDTTRFTXINFPMTTPINFCTGYPURP: {
    beautify: "Category purpose"
  },
  CDTTRFTXINFINTRBKSTTLMAMT: {
    beautify: "Amount"
  },
  CDTTRFTXINFCHRGBR: {
    beautify: "Charge bearer"
  },
  CDTTRFTXINFINSTGAGTFININSTNIDBIC: {
    beautify: "Instructing agent BIC"
  },
  CDTTRFTXINFDBTRNM: {
    beautify: "Debtor name"
  },
  CDTTRFTXINFDBTRACCTIDIBAN: {
    beautify: "Debtor IBAN"
  },
  CDTTRFTXINFDBTRAGTFININSTNIDBIC: {
    beautify: "Debtor agent"
  },
  CDTTRFTXINFCDTRAGTFININSTNIDBIC: {
    beautify: "Creditor agent"
  },
  CDTTRFTXINFCDTRNM: {
    beautify: "Creditor name"
  },
  CDTTRFTXINFCDTRACCTIDIBAN: {
    beautify: "Creditor IBAN"
  },
  CDTTRFTXINFPURPCD: {
    beautify: "Purpose of the transfer"
  },
  CDTTRFTXINFRMTINFUSTRD: {
    beautify: "Unstructured remittance information"
  },
  CDTTRFTXINFRMTINFSTRD: {
    beautify: "Structured remittance information"
  },
  CDTTRFTXINFRMTINFSTRDCDTRREFINFTPCDORPRTRYCD: {
    beautify: "Creditor reference code"
  },
  CDTTRFTXINFRMTINFSTRDCDTRREFINFTPISSR: {
    beautify: "Issuer of creditor reference code"
  },
  CDTTRFTXINFRMTINFSTRDCDTRREFINFREF: {
    beautify: "Reference"
  },
  REPORTMSGID: {
    beautify: "Report message id"
  },
  REPORTDTTM: {
    beautify: "Report datetime",
    modify: (datetime) => { return !datetime ? datetime :moment(getCETDate(new Date(datetime))).format("DD.MM.YYYY HH:mm:ss"); }
  },
  ASSGNMTID: {
    beautify: "Assignment id"
  },
  ASSGNMTCREDTTM: {
    beautify: "Assignment credit time"
  },
  CTRLDATANBOFTXS: {
    beautify: "Number of transactions"
  },
  UNDRLYGTXINFORGNLGRPINFORGNLMSGID: {
    beautify: "Original message id"
  },
  UNDRLYGTXINFORGNLGRPINFORGNLMSGNMID: {
    beautify: "Original message type"
  },
  UNDRLYGTXINFORGNLGRPINFORGNLMSGENDTOENDID: {
    beautify: "Original message end to end id"
  },
  UNDRLYGTXINFORGNLGRPINFORGNLTXID: {
    beautify: "Original transaction id"
  },
  UNDRLYGTXINFORGNLINTRBKSTTLMAMT: {
    beautify: "Original transaction bank settlement amount"
  },
  UNDRLYGTXINFORGNLINTRBKSTTLMDT: {
    beautify: "Original transaction settlement date",
    modify: (datetime) => { return moment(getCETDate(new Date(datetime))).format("DD.MM.YYYY"); }
  },
  UNDRLYGTXINFCXLRSNINFORGTRNM: {
    beautify: "Originator name"
  },
  UNDRLYGTXINFCXLRSNINFORGTRIDORGIDBICORBEI: {
    beautify: "Originator BIC (or BEI)"
  },
  UNDRLYGTXINFCXLRSNINFRSNCD: {
    beautify: "Reason code"
  },
  UNDRLYGTXINFCXLRSNINFRSNPRTY: {
    beautify: "Reason property"
  },
  UNDRLYGTXINFCXLRSNINFRSNADDTINF: {
    beautify: "Additional information"
  },
  UNDRLYGTXINFORGNLTXREF: {
    beautify: "Reference"
  },
  UNDRLYGTXINFORGNLTXREFSTTLMINFSTTLMMTD: {
    beautify: "Settlement method"
  },
  UNDRLYGTXINFORGNLTXREFSTTLMINFCLRSYSPRTRY: {
    beautify: "Clearing system"
  },
  UNDRLYGTXINFORGNLTXREFPMTTPINFSVCLVLCD: {
    beautify: "Service level"
  },
  UNDRLYGTXINFORGNLTXREFDBTRNM: {
    beautify: "Original transaction debtor name"
  },
  UNDRLYGTXINFORGNLTXREFDBTRACCTIDIBAN: {
    beautify: "Original transaction debtor IBAN"
  },
  UNDRLYGTXINFORGNLTXREFDBTRAGTFININSTNIDBIC: {
    beautify: "Original transaction debtor BIC"
  },
  UNDRLYGTXINFORGNLTXREFCDTRAGTFININSTNIDBIC: {
    beautify: "Original transaction creditor BIC"
  },
  UNDRLYGTXINFORGNLTXREFCDTRNM: {
    beautify: "Original transaction creditor name"
  },
  UNDRLYGTXINFORGNLTXREFCDTRACCTIDIBAN: {
    beautify: "Original transaction creditor IBAN"
  },
  TXINFRTRID: {
    beautify: "Transaction id"
  },
  TXINFORGNLGRPINFORGNLMSGID: {
    beautify: "Message id"
  },
  TXINFORGNLGRPINFORGNLMSGNMID: {
    beautify: "Message type"
  },
  TXINFORGNLINSTRID: {
    beautify: "Instrument id"
  },
  TXINFORGNLENDTOENDID: {
    beautify: "Original transaction end to end id"
  },
  TXINFORGNLTXID: {
    beautify: "Original transaction id"
  },
  TXINFORGNLINTRBKSTTLMAMT: {
    beautify: "Original transaction settlement amount"
  },
  TXINFRTRDINTRBKSTTLMAMT: {
    beautify: "Settlement amount"
  },
  TXINFRTRDINSTDAMT: {
    beautify: "Amount before charges"
  },
  TXINFCHRGBR: {
    beautify: "Charge bearer"
  },
  TXINFCHRGSINFAMT: {
    beautify: "Charges amount"
  },
  TXINFCHRGSINFPTYFININSTNIDBIC: {
    beautify: "Financial institution BIC"
  },
  TXINFINSTGAGTFININSTNIDBIC: {
    beautify: "Instructing agent BIC"
  },
  TXINFRTRRSNINFORGTRNM: {
    beautify: "Originator name"
  },
  TXINFRTRRSNINFORGTRIDORGIDBICORBEI: {
    beautify: "Originator BIC (or BEI)"
  },
  TXINFRTRRSNINFRSNCD: {
    beautify: "Reason code"
  },
  TXINFRTRRSNINFADDTINF: {
    beautify: "Additional information"
  },
  TXINFORGNLTXREF: {
    beautify: "Reference"
  },
  TXINFORGNLTXREFINTRBKSTTLMDT: {
    beautify: "Settlement date",
    modify: (datetime) => { return !datetime ? datetime : moment(getCETDate(new Date(datetime))).format("DD.MM.YYYY"); }
  },
  TXINFORGNLTXREFSTTLMINFSTTLMMTD: {
    beautify: "Settlement method"
  },
  TXINFORGNLTXREFPMTTPINFSVCLVLCD: {
    beautify: "Service level"
  },
  TXINFORGNLTXREFDBTRNM: {
    beautify: "Debtor name"
  },
  TXINFORGNLTXREFDBTRACCTIDIBAN: {
    beautify: "Debtor IBAN"
  },
  TXINFORGNLTXREFDBTRAGTFININSTNIDBIC: {
    beautify: "Debtor BIC"
  },
  TXINFORGNLTXREFCDTRAGTFININSTNIDBIC: {
    beautify: "Creditor BIC"
  },
  TXINFORGNLTXREFCDTRNM: {
    beautify: "Creditor name"
  },
  TXINFORGNLTXREFCDTRACCTIDIBAN: {
    beautify: "Creditor IBAN"
  }
};

export default dict;