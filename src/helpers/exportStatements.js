/* eslint-disable no-case-declarations */
import jsPDF from "jspdf";
import "jspdf-autotable";
import moment from "moment";

const dict = {
  "ė": "e",
  "ą": "a"
}

const formatText = text => text ? text.split("").map(s => s = dict[s] || s).join("") : ""

export const exportStatements = (
  docType = "pdf", accountIban,
  accountNumber = "no data",
  accountCurrency = "EUR",
  accountType = "settelment",
  initialDate,
  finalDate,
  openingBalance,
  closingBalance,
  data) => {

  initialDate = moment(initialDate).format("DD.MM.YYYY");
  finalDate = moment(finalDate).format("DD.MM.YYYY");
  const date = moment(new Date().toLocaleString('en-US', {
    timeZone: 'Europe/Amsterdam',
    hour12: false
  })).format("DD.MM.YYYY hh:mm:ss");

  let Credit = 0;
  let Debit = 0;

  data.forEach(operation => {
    if (operation.moneyIn != "-") {
      Credit += operation.moneyIn;
    }
    else {
      Debit += operation.moneyOut;
    }
  });

  switch (docType) {
  case "csv":

    let cvsDoc = "Account IBAN," + accountIban + "\n" +
    "Account currency," + accountCurrency + "\n" +
    // "Account type," + accountType + "\n" +
    "Statement generation date," + date + "\n" +
    "Period,from " + initialDate + " to " + finalDate + "\n" +
    "Opening balance," + openingBalance.toFixed(2) + " " + accountCurrency + "\n" +
    "Credit," + Credit.toFixed(2) + " " + accountCurrency + "\n" +
    "Debit," + Debit.toFixed(2) + " " + accountCurrency + "\n" +
    "Closing balance," + closingBalance.toFixed(2) + " " + accountCurrency + "\n" +
    "\n" +
    "Date,Operation,Operation description,Balance" + "\n";
    
    data.forEach(operation => {
      cvsDoc += moment(operation.date).format("DD.MM.YYYY") + ",";
      cvsDoc += operation.description + ",";
      //cvsDoc += operation.paymentReason ? operation.paymentReason + "," : "no descriprion,";
      cvsDoc += "no description,";
      let balanceChange = "no data";
      if (operation.moneyIn != "-") {
        balanceChange = "+" + operation.moneyIn + " " + accountCurrency;
      }
      else {
        balanceChange = "-" + operation.moneyOut + " " + accountCurrency;
      }
      cvsDoc += balanceChange + "\n";
    });

    const element = document.createElement("a");
    const file = new Blob([ cvsDoc ], { type: "text/plain" });
    element.href = URL.createObjectURL(file);
    element.download = accountIban + "_" + date + ".csv";
    document.body.appendChild(element);
    element.click();
    break;

  case "pdf":

    const doc = new jsPDF("p", "pt", "a4");
    const docCenter = doc.internal.pageSize.width / 2;
    const docWidth = doc.internal.pageSize.width;
    const docHeight = doc.internal.pageSize.height;

    const calcTextSizeInUnits = (text) => {
      return doc.getStringUnitWidth(text) * doc.internal.getFontSize();
    };

    const defaultSideOffset = 30;
    
    doc.setFontSize(40);
    doc.setFontStyle("Bold");
    doc.text(defaultSideOffset, 45, "TBF Finance, UAB"); //label
    doc.setFontSize(26);
    doc.text(defaultSideOffset, 100, "Account statement");
    doc.setFontSize(12);
    doc.setFontStyle("normal");
    doc.rect(defaultSideOffset, 110, docCenter - 45, 70); //105
    
    doc.text(35, 130, "Account IBAN");
    //doc.text(35, 146, "Account number");
    doc.text(35, 146, "Account currency");  //162
    // doc.text(35, 162, "Account type");  //178
    doc.text(35, 162, "Statement created");  //194
    // doc.text(35, 178, "date"); //204
    
    doc.text(150, 130, accountIban);
    //doc.text(150, 146, accountNumber);
    doc.text(150, 146, accountCurrency); //162
    // doc.text(150, 162, accountType); //178
    doc.text(150, 162, date); //194
    
    doc.text(docCenter + defaultSideOffset / 2, 120, "Period from " + initialDate + " to " + finalDate);
    doc.text(docCenter + defaultSideOffset / 2, 136, "Opening balance");
    doc.text(docCenter + defaultSideOffset * 2 / 3, 152, "Credit");
    doc.text(docCenter + defaultSideOffset * 2 / 3, 168, "Debit");
    doc.text(docCenter + defaultSideOffset / 2, 184, "Closing balance");
    
    doc.text(docWidth - calcTextSizeInUnits(openingBalance.toFixed(2) + " " + accountCurrency) - defaultSideOffset, 136, openingBalance.toFixed(2) + " " + accountCurrency);
    doc.text(docWidth - calcTextSizeInUnits(closingBalance.toFixed(2) + " " + accountCurrency) - defaultSideOffset, 184, closingBalance.toFixed(2) + " " + accountCurrency);
    
    doc.setLineWidth(1.5);
    doc.line(docCenter + defaultSideOffset / 2, 122, docWidth - defaultSideOffset, 122);
    doc.setLineWidth(0.2);
    doc.line(docCenter + defaultSideOffset / 2, 138, docWidth - defaultSideOffset, 138);
    doc.line(docCenter + defaultSideOffset / 2, 154, docWidth - defaultSideOffset, 154);
    doc.line(docCenter + defaultSideOffset / 2, 170, docWidth - defaultSideOffset, 170);
    
    doc.setFontSize(26);
    doc.text(defaultSideOffset, 240, "Operations");
    doc.setFontStyle("bold");
    doc.setFontSize(12);
    // doc.setFontStyle("normal");

    doc.text(defaultSideOffset, 260, "Date");
    doc.text(80 + defaultSideOffset, 260, "Operation");
    doc.text(170 + defaultSideOffset, 260, "Operation description");
    doc.text(docWidth - calcTextSizeInUnits("Amount") - defaultSideOffset, 260, "Amount");
    
    doc.setFontStyle("normal");
    doc.setLineWidth(2);
    doc.line(defaultSideOffset, 265, docWidth - defaultSideOffset, 265);
    
    doc.text(docWidth - calcTextSizeInUnits(Credit.toFixed(2) + " " + accountCurrency) - defaultSideOffset, 152, Credit.toFixed(2) + " " + accountCurrency);
    doc.text(docWidth - calcTextSizeInUnits("-" + Debit.toFixed(2) + " " + accountCurrency) - defaultSideOffset, 168, "-" + Debit.toFixed(2) + " " + accountCurrency);
    
    let currentOffsetHeight = 280;
    let currentPage = 1;

    data.forEach(operation => {
      doc.text(defaultSideOffset, currentOffsetHeight, moment(operation.date).format("DD.MM.YYYY"));
      doc.text(80 + defaultSideOffset, currentOffsetHeight, operation.description);
      //doc.text(250 + defaultSideOffset, currentOffsetHeight, operation.paymentReason ? operation.paymentReason : "no description");
      doc.text(170 + defaultSideOffset, currentOffsetHeight, formatText(operation.paymentReason));
      let balanceChange = "no data";
      if (operation.moneyIn != "-") {
        balanceChange = "+" + operation.moneyIn.toFixed(2) + " " + accountCurrency;
      }
      else {
        balanceChange = "-" + operation.moneyOut.toFixed(2) + " " + accountCurrency;
      }
      doc.text(docWidth - calcTextSizeInUnits(balanceChange) - defaultSideOffset, currentOffsetHeight, balanceChange);
      if (currentOffsetHeight > docHeight - 80) {
        doc.text(docWidth - calcTextSizeInUnits("Page " + currentPage) - defaultSideOffset, docHeight - 30, "Page " + currentPage);
        doc.addPage();
        currentPage ++;
        doc.text(defaultSideOffset, 45, "Date");
        doc.text(100 + defaultSideOffset, 45, "Operation");
        doc.text(250 + defaultSideOffset, 45, "Operation description");
        doc.text(docWidth - calcTextSizeInUnits("Balance") - defaultSideOffset, 45, "Balance");
        
        doc.setLineWidth(2);
        doc.line(defaultSideOffset, 50, docWidth - defaultSideOffset, 50);

        currentOffsetHeight = 65;
      }
      else {
        currentOffsetHeight +=15;
      }
    });

    if (currentPage > 1) {
      doc.text(docWidth - calcTextSizeInUnits("Page " + currentPage) - defaultSideOffset, docHeight - 30, "Page " + currentPage);
    }

    doc.save(accountIban + "_" + date + ".pdf");
    break;
  }

};