import jsPDF from "jspdf";
import "jspdf-autotable";
import moment from "moment";
import { getCETDate } from "helpers/target2Timing";


const array = [ "delete", "edit", "steps", "params", "headers", "parameters", "request", "response", "message" ];
const timeArray = [ "created_at", "date_time", "apply_from", "apply_to", "createdat", "date" ];

const createHeaders = (columns) => {
  return [[ "#", ...columns.map(col => col.label).filter(label => !array.includes(label.toLowerCase())) ]] ;
};

const createBody = (columns, data) => {
  let body = [];
  data && data.forEach((item, index) => {
    let itemBody = [ index + 1 ];
    columns.forEach(col => {
      const name = col.key || col.path;
      if(!array.includes(name)) {
        let value = item[name];
        if(name.toLowerCase() === "enabled")
          value = value ? "Enabled" : "Disabled";
        else if(timeArray.includes(name.toLowerCase()))
          value = moment(getCETDate(new Date(value))).format("DD.MM.YYYY HH:mm");
        itemBody.push(value);
      }
    });
    body.push(itemBody);
  });
  return body;
};

export const exportTable = (columns, data, label) => {
  columns = columns.filter(col => col);
  const head = createHeaders(columns);
  const body = createBody(columns, data);
  const pdf = new jsPDF("landscape");
  pdf.text(150, 10, label, "center");
  pdf.autoTable({ 
    head, 
    body, 
    theme: "grid",
    marginLeft: 20,
    marginRight: 20,
    styles: {
      halign: "center"
    },
    headStyles: {
      fontSize: label.toLowerCase() === "transactionlogs" ? 7 : 9,
      fillColor: [ 70, 179, 80 ]
    },
    bodyStyles: {
      fontSize: label.toLowerCase() === "transactionlogs" ? 7 : 9,
    }
  });
  pdf.save(`${label.toLowerCase()}.pdf`);
};

const createChartTableData = (columns, data) => {
  if(!columns) {
    const head = [[ "Type", "Success", "Failed" ]];
    const body = data.map(item => { return [ item.type, item.success.toString(), item.failed.toString() ]; });
    return { head, body };
  }
  if(columns[0] === "Success" && columns[1] === "Failed" && columns.length === 2)
    return { 
      head: [ columns ],
      body: [ data[0].data ]
    };
  
  let body = [];
  data[0].data.forEach((item, itemIndex) => {
    let element = [];
    element.push(columns[itemIndex]);
    data.forEach((d, dataIndex) => {
      element.push(data[dataIndex].data[itemIndex].toString());
    });
    body.push(element);
  });

  let head = [];
  if(data[0] && (data[0].label === "transactions") && data.length === 1) {  // Top merchants
    head = [[ "Merchants", "Transactions" ]];
    return { head, body };
  }

  head = [ "   ", ...data.map(item => { return item.label; }) ];
  return { head: [ head ], body };
};

export const exportChartTable = (columns, data, name) => {
  const { head, body } = createChartTableData(columns, data);
  const pdf = new jsPDF("1", "pt");
  pdf.text(name, 300, 25, "center");
  pdf.autoTable({ 
    head, 
    body, 
    theme: "grid",
    styles: {
      halign: "center"
    },
    columnStyles: {
      0: head[0][0] === "   " 
        ? { 
          fontStyle: "bold",
          fillColor: [ 70, 179, 80 ],
          textColor: [ 255, 255, 255 ]
        }
        : {}
    },
    headStyles: {
      fontSize: 15,
      fillColor: [ 70, 179, 80 ]
    },
    bodyStyles: {
      fontSize: 10
    }
  });
  pdf.save(`${name.toLowerCase()}.pdf`);
};

export const exportAml = (obj, filename) => {
  const fileData = JSON.stringify(obj, null, '\t');
  const blob = new Blob([fileData], {type: "text/plain"});
  const url = URL.createObjectURL(blob);
  const link = document.createElement('a');
  link.download = `${filename}.json`;
  link.href = url;
  link.click();
}
