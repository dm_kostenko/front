const calcTime = (date = new Date()) => {
  const offset = date.getTimezoneOffset();
  return new Date(date.getTime() - offset * 60000);
};


const getCETDate = (d = calcTime()) => {
  return new Date(d.toLocaleString('en-US', {
    timeZone: 'Europe/Amsterdam',
    hour12: false
  }).replace(",",""));
}

const isWorkingTime = (d = calcTime()) => {
  const CETDate = getCETDate(d);
  const CETHours = CETDate.getUTCHours();
  return CETHours >= 7 && CETHours < 18;
};

module.exports = { 
  getCETDate,
  getDateTime: () => getCETDate().toISOString().slice(0, 19),
  isWorkingTime, 
  calcTime
};