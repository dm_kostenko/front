// import ability from "config/ability";


// export const isRoute = (route) => {
//   if(route.execute)
//     if(ability.can("EXECUTE", route.alias.toUpperCase()))
//       return route;
//     else 
//       return undefined;
//   if(route.redirect || ability.can("READ", route.alias.toUpperCase()))
//     return route;
//   if(route.views) {
//     const newViews = route.views.filter(childRoute => ability.can("READ", childRoute.alias.toUpperCase()));
//     if(newViews.length)
//       return {
//         ...route,
//         views: newViews
//       };
//   }
// };

import { ability, permissionsDictionary } from "config/ibankAbility";

export const isRoute = (route) => {
  if(!route.redirect && route.name && permissionsDictionary[route.name]) {
    return ability(permissionsDictionary[route.name]) ? route : false;
  }
  return route;
};
