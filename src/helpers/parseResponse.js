export const parseResponse = (error) => {
  if(error.length > 0) {
    let message = "";
    error.forEach(item => {
      message += `${item.message}\n`;
    });
    return {
      error: "Validation error",
      message
    };
  }
  if (!error.response) return {
    error: "Server is not available",
    message: "please try again later"
  };
  if (!error.response.data.details) return {
    error: error.response.data.error,
    message: ""
  };
  else {
    let message = "";
    error.response.data.details.forEach(detail => {
      message += detail.message;
      message += ", ";
    });

    message = message.slice(0, message.length - 2);
    return {
      error: error.response.data.error,
      message: message
    };
  }
};