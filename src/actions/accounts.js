import * as types from "../constants/actionTypes";
import {
  createAccount,
  getAccounts,
  getAccount,
  updateAccount,
  deleteAccount
} from "../services/paymentBackendAPI/management/accounts";

export const getAllAccounts = (page, items, search) => {
  return async dispatch => {
    dispatch({
      type: types.GET_ACCOUNTS
    });
    try {
      const response = await getAccounts(page, items, search);
      return dispatch({
        type: types.GET_ACCOUNTS_SUCCEED,
        data: response.data,
        pageSize: items,
        currentPage: page
      });
    } catch (error) {
      return dispatch({
        type: types.GET_ACCOUNTS_FAILED
      });
    }
  };
};

export const getAccountAction = id => {
  return async dispatch => {
    dispatch({
      type: types.GET_ACCOUNT
    });
    try {
      const response = await getAccount(id);
      return dispatch({
        type: types.GET_ACCOUNT_SUCCEED,
        account: response.data
      });
    } catch (error) {
      return dispatch({
        type: types.GET_ACCOUNT_FAILED
      });
    }
  };
};

export const addAccountAction = (data, page, items, search) => {
  return async dispatch => {
    try {
      await createAccount(data);
      return dispatch(getAllAccounts(page, items, search));
    } catch (error) {
      throw error;
    }
  };
};

export const deleteAccountAction = (data, page, items, search) => {
  return async dispatch => {
    try {
      await deleteAccount(data);
      return dispatch(getAllAccounts(page, items, search));
    } catch (error) {
      throw error;
    }
  };
};

export const updateAccountAction = data => {
  return async dispatch => {
    try {
      const response = await updateAccount(data);
      dispatch({
        type: types.UPDATE_ACCOUNT,
        account: response.data
      });
    } catch (error) {
      throw error;
    }
  };
};