import * as types from "../constants/actionTypes";
import {
  createGroup,
  getGroups,
  getGroup,
  updateGroup,
  deleteGroup,
  getGroupLogins,
  getGroupMerchants,
  addGroupLogin
} from "../services/paymentBackendAPI/management/groups";
import {
  getTopGroups
} from "../services/paymentBackendAPI/reports/entities";

import {
  deleteMerchantFromGroup,
  addMerchantToGroup
} from "../services/paymentBackendAPI/management/merchants";
import { getAmountGroups } from "../services/paymentBackendAPI/reports/entities";
import { getAllMerchants } from "./merchants";

export const getAllGroups = (page, items, search) => {
  return async (dispatch) => {
    dispatch({
      type:types.GET_GROUPS
    });
    try {
      const response = await getGroups(page, items, search);
      return dispatch({
        type: types.GET_GROUPS_SUCCEED,
        data: response.data,
        pageSize: items, 
        currentPage: page
      });
    } catch (error) {
      return dispatch({
        type:types.GET_GROUPS_FAILED
      });
    }
  };
};

export const getGroupAction = (id) => {
  return async (dispatch) => {
    dispatch({
      type:types.GET_GROUP
    });
    try {
      const response = await getGroup(id);
      return dispatch({
        type: types.GET_GROUP_SUCCEED,
        group: response.data
      });
    } catch (error) {
      return dispatch({
        type:types.GET_GROUP_FAILED
      });
    }
  };
};

export const editGroupAction = (data) => {
  return async (dispatch) => {
    try {
      const response = await updateGroup(data);
      return dispatch({
        type: types.EDIT_GROUP,
        group: response.data
      });
    } catch (error) {
      throw (error);
    }
  };
};

export const deleteGroupAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      await deleteGroup(data);
      return dispatch(getAllGroups(page, items, search));
    } catch (error) {
      throw (error);
    }
  };
};

export const addMerchantToGroupAction = (guid, group_guid) => {
  return async () => {
    try {
      await addMerchantToGroup(guid, group_guid);
    } catch(error) {
      throw (error);
    }
  };
};

export const deleteMerchantFromGroupAction = (guid, page, items, groupGuid) => {
  return async (dispatch) => {
    try {
      await deleteMerchantFromGroup(guid);
      return dispatch(getAllMerchants(page, items));
    } catch(error) {
      throw (error);
    }
  };
};


export const getGroupLoginsAction = (id, page, items) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_GROUP_LOGINS
    });
    try {
      const response = await getGroupLogins(id, page, items);
      return dispatch({
        type: types.GET_GROUP_LOGINS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_GROUP_LOGINS_FAILED
      });
      throw (error);
    }
  };
};

export const getGroupMerchantsAction = (id, page, items) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_GROUP_MERCHANTS
    });
    try {
      const response = await getGroupMerchants(id, page, items);
      return dispatch({
        type: types.GET_GROUP_MERCHANTS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_GROUP_MERCHANTS_FAILED
      });
    }
  };
};

export const addGroupAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      const response = await createGroup(data);
      dispatch({
        type: types.ADD_GROUP,
        group: response.data
      });
      dispatch(getAllGroups(page, items, search));
    }
    catch (error) {
      throw (error);
    }
  };
};

export const addGroupLoginAction = (id, data) => {
  return async (dispatch) => {
    try {
      const response = await addGroupLogin(id, data);
      return dispatch({
        type: types.ADD_GROUP_LOGIN,
        groupLogin: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const upsertChangedMerchantAction = (merchant) => {
  return (dispatch) => {
    try {
      return dispatch({
        type: types.UPSERT_GROUP_CHANGED_MERCHANT,
        merchant: merchant
      });
    }
    catch (error) {
      throw (error);
    }   
  };
};

export const getTopGroupsAction = (days, count) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_TOP_GROUPS,
    });
    try {
      const response = await getTopGroups(days, count);
      return dispatch({
        type: types.GET_TOP_GROUPS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_TOP_GROUPS_FAILED,
      });
      throw (error);
    }
  };
};

export const getAmountGroupsForPartner = () => {
  return async (dispatch) => {
    try {
      const response = await getAmountGroups();
      dispatch({
        type: types.GET_AMOUNT_GROUPS_FOR_PARTNER,
        count: response.data[0].count
      });
    }
    catch (error) {
      throw (error);
    }   
  };
};


