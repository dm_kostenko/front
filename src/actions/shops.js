import * as types from "../constants/actionTypes";
import {
  getShops,
  getShop,
  createShop,
  deleteShop,
  updateShop,
  getShopGateways,
  getShopsByGateway,
  upsertGatewayToShop,
  upsertAccountToShop,
  addAccountToShop,
  getAccountsFromShop,
  getShopGatewayProps,
  addShopGatewayProps,
  deleteShopGatewayProps,
  editShopGatewayProps
} from "../services/paymentBackendAPI/management/shops";
import {
  getMerchantShops
} from "../services/paymentBackendAPI/management/merchants";
import {
  getGroupShops
} from "../services/paymentBackendAPI/management/groups";
import {
  getPartnerShops
} from "../services/paymentBackendAPI/management/partners";
import {
  getAmountShops,
  getTopShops
} from "../services/paymentBackendAPI/reports/entities";
import {
  getShopTotals
} from "../services/paymentBackendAPI/reports/transactions";


export const getAllShops = (page, items, search) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_SHOPS
    });
    try {
      const response = await getShops(page, items, search);
      dispatch({
        type: types.GET_SHOPS_SUCCEED,
        data: response.data,
        currentPage: page,
        pageSize: items
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_SHOPS_FAILED
      });
    }
  };
};

export const getMerchantShopsAction = (id, page, items) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_SHOPS
    });
    try {
      const response = await getMerchantShops(id, page, items);
      dispatch({
        type: types.GET_SHOPS_SUCCEED,
        data: response.data,
        currentPage: page,
        pageSize: items
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_SHOPS_FAILED
      });
    }
  };
};


export const getGroupShopsAction = (id, page, items) => {
  return async (dispatch) => {
    try {
      const response = await getGroupShops(id, page, items);
      dispatch({
        type: types.GET_SHOPS,
        data: response.data,
        pageSize: items,
        currentPage: page
      });
    }
    catch (error) {
      throw (error);
    }
  };
};



export const getPartnerShopsAction = (id, page, items) => {
  return async (dispatch) => {
    try {
      const response = await getPartnerShops(id, page, items);
      dispatch({
        type: types.GET_SHOPS,
        data: response.data,
        pageSize: items,
        currentPage: page
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const getShopsByGatewayAction = (id, page, items) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_SHOPS_BY_GATEWAY
    });
    try {
      const response = await getShopsByGateway(id, page, items);
      return dispatch({
        type: types.GET_SHOPS_BY_GATEWAY_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_SHOPS_BY_GATEWAY_FAILED
      });
      throw (error);
    }
  };
};


export const getShopAction = (id) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_SHOP
    });
    try {
      const response = await getShop(id);
      return dispatch({
        type: types.GET_SHOP_SUCCEED,
        shop: response.data
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_SHOP_FAILED
      });
    }
  };
};

export const deleteShopAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      await deleteShop(data);
      return dispatch(getAllShops(page, items, search));
    }
    catch (error) {
      throw (error);
    }
  };
};

export const addShopAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      const response = await createShop(data);
      dispatch({
        type: types.ADD_SHOP,
        shop: response.data
      });
      dispatch(getAllShops(page, items, search));
    }
    catch (error) {
      throw (error);
    }
  };
};

export const editShopAction = (data) => {
  return async (dispatch) => {
    try {
      const response = await updateShop(data);
      dispatch({
        type: types.EDIT_SHOP,
        shop: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const getShopGatewaysAction = (data) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_SHOP_GATEWAYS
    });
    try {
      const response = await getShopGateways(data);
      return dispatch({
        type: types.GET_SHOP_GATEWAYS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_SHOP_GATEWAYS_FAILED
      });
    }
  };
};

export const getShopAccountsAction = (data) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_SHOP_ACCOUNTS
    });
    try {
      const response = await getAccountsFromShop(data);
      return dispatch({
        type: types.GET_SHOP_ACCOUNTS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_SHOP_ACCOUNTS_FAILED
      });
    }
  };
};

export const getAmountShopsForGroup = () => {
  return async (dispatch) => {
    try {
      const response = await getAmountShops();
      dispatch({
        type: types.GET_AMOUNT_SHOPS_FOR_GROUP,
        count: response.data[0].count
      });
    }
    catch (error) {
      throw (error);
    }   
  };
};


export const addShopAccountAction = (id, data) => {
  return async (dispatch) => {
    try {
      const response = await addAccountToShop(id, data);
      dispatch({
        type: types.ADD_SHOP_ACCOUNT,
        shopAccount: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const upsertGatewayToShopAction = (data) => {
  return async (dispatch) => {
    try {
      await upsertGatewayToShop(data.guid, data.gateways);
      dispatch({
        type: types.UPSERT_SHOP_GATEWAYS,
        data: data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const upsertAccountToShopAction = (data) => {
  return async (dispatch) => {
    try {
      await upsertAccountToShop(data.guid, data.accounts);
      dispatch({
        type: types.UPSERT_SHOP_ACCOUNTS,
        data: data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const upsertChangedGatewayAction = (changedGateway) => {
  return (dispatch) => {
    dispatch({
      type: types.UPSERT_SHOP_GATEWAY,
      gateway: changedGateway
    });
  };
};

export const upsertChangedAccountAction = (account) => {
  return (dispatch) => {
    dispatch({
      type: types.UPSERT_SHOP_CHANGED_ACCOUNT,
      account: account
    });
  };
};

export const upsertChangedGatewayDataAction = (changedGatewayData) => {
  return (dispatch) => {
    dispatch({
      type: types.EDIT_SHOP_GATEWAY_DATA,
      changedGatewayData: changedGatewayData
    });
  };
};

export const getAmountShopsForPartner = () => {
  return async (dispatch) => {
    try {
      const response = await getAmountShops();
      dispatch({
        type: types.GET_AMOUNT_SHOPS_FOR_PARTNER,
        count: response.data[0].count
      });
    }
    catch (error) {
      throw (error);
    }   
  };
};

export const getShopGatewayPropsAction = (guid, gatewayGuid) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_SHOP_GATEWAY_PROPS
    });
    try {
      const response = await getShopGatewayProps(guid, gatewayGuid);
      return dispatch({
        type: types.GET_SHOP_GATEWAY_PROPS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_SHOP_GATEWAY_PROPS_FAILED
      });
    }
  };
};

export const addShopGatewayPropsAction = (guid, gatewayGuid, data) => {
  return async (dispatch) => {
    try {
      await addShopGatewayProps(guid, gatewayGuid, data);
      return dispatch(getShopGatewayPropsAction(guid, gatewayGuid));
    }
    catch (error) {
      throw(error);
    }
  };
};

export const editShopGatewayPropsAction = (guid, gatewayGuid, data) => {
  return async (dispatch) => {
    try {
      await editShopGatewayProps(guid, gatewayGuid, data);
      return dispatch(getShopGatewayPropsAction(guid, gatewayGuid));
    }
    catch (error) {
      throw(error);
    }
  };
};

export const deleteShopGatewayPropsAction = (guid, gatewayGuid, data) => {
  return async (dispatch) => {
    try {
      await deleteShopGatewayProps(guid, gatewayGuid, data);
      return dispatch(getShopGatewayPropsAction(guid, gatewayGuid));
    }
    catch (error) {
      throw(error);
    }
  };
};

export const getShopTotalsAction = (page, items, from_date, to_date) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_REPORT_SHOP_TOTALS
    });
    try {
      const response = await getShopTotals(page, items, from_date, to_date);
      return dispatch({
        type: types.GET_REPORT_SHOP_TOTALS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_REPORT_SHOP_TOTALS_FAILED
      });
      throw (error);
    }
  };
};

export const getTopShopsAction = (days, count) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_TOP_SHOPS
    });
    try {
      const response = await getTopShops(days, count);
      return dispatch({
        type: types.GET_TOP_SHOPS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_TOP_SHOPS_FAILED
      });
      throw (error);
    }
  };
};
