import * as types from "../constants/actionTypes";
import {
  createCard,
  getCards,
  getCard,
  updateCard,
  getCardsAccount
} from "../services/paymentBackendAPI/management/cards";

export const getAllCards = (page, items, search) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_CARDS
    });
    try {
      const response = await getCards(page, items, search);
      dispatch({
        type: types.GET_CARDS_SUCCEED,
        data: response.data,
        pageSize: items,
        currentPage: page
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_CARDS_FAILED
      });
    }
  };
};

export const getCardAction = (id) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_CARD
    });
    try {
      const response = await getCard(id);
      return dispatch({
        type: types.GET_CARD_SUCCEED,
        card: response.data
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_CARD_FAILED
      });
    }
  };
};

export const getCardsAccountAction = (number, page, items) => {
  return async (dispatch) => {
    try {
      const response = await getCardsAccount(number, page, items);
      dispatch({
        type: types.GET_CARDS_ACCOUNT,
        data: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const updateCardAction = (data) => {
  return async (dispatch) => {
    try {
      const response = await updateCard(data);
      dispatch({
        type: types.UPDATE_CARD,
        card: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const addCardAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      await createCard(data);
      return dispatch(getAllCards(page, items, search));
    }
    catch (error) {
      throw (error);
    }
  };
};

export const deleteCardAction = (guid, page, items, search) => {
  return async (dispatch) => {
    try {
      await createCard({ guid, delete: true });
      return dispatch(getAllCards(page, items, search));
    }
    catch (error) {
      throw (error);
    }
  };
};