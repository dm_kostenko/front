import * as types from "../constants/actionTypes";

export function showManagerModal(managerModalState) {
  return { type: types.SHOW_MANAGER_MODAL, managerModalState };
}

export function showModal(modalState) {
  return { type: types.SHOW_MODAL, modalState };
}

export function resetModalData() {
  return { type: types.RESET_MODAL_DATA };
}
