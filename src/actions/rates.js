import * as types from "../constants/actionTypes";
import {
  getRates,
  createRate,
  deleteRate,
  getRate,
  getPaymentStatusRate,
  updateRate,
  getOneTimeCharges,
  getPeriodicCharges,
  getConditionalCharges,
  createOneTimeCharge,
  createPeriodicCharge,
  createConditionalCharge
} from "../services/paymentBackendAPI/management/rates";

export const getAllRates = (page, items, search) => {
  return async (dispatch) => {
    dispatch({
      type:types.GET_RATES
    });
    try {
      const response = await getRates(page, items, search);
      return dispatch({
        type: types.GET_RATES_SUCCEED,
        data: response.data,
        pageSize: items,
        currentPage: page
      });
    } catch (error) {
      return dispatch({
        type:types.GET_RATES_FAILED
      });
    }
  };
};

export const getRateAction = (shop_id, gateway_id, currency_id) => {
  return async (dispatch) => {
    dispatch({
      type:types.GET_RATE
    });
    try {
      const response = await getRate(shop_id, gateway_id, currency_id);
      return dispatch({
        type: types.GET_RATE_SUCCEED,
        rate: response.data
      });
    } catch (error) {
      return dispatch({
        type:types.GET_RATE_FAILED
      });
    }
  };
};

export const getPaymentStatusRateAction = (shop_id, gateway_id, currency_id) => {
  return async (dispatch) => {
    dispatch({
      type:types.GET_PAYMENT_STATUS_RATE
    });
    try {
      const response = await getPaymentStatusRate(shop_id, gateway_id, currency_id);
      return dispatch({
        type: types.GET_PAYMENT_STATUS_RATE_SUCCEED,
        rate: response.data
      });
    } catch (error) {
      return dispatch({
        type:types.GET_PAYMENT_STATUS_RATE_FAILED
      });
    }
  };
};

export const addRateAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      await createRate(data);
      return dispatch(getAllRates(page, items, search));
    } catch (error) {
      throw (error);
    }
  };
};

export const getOneTimeChargesAction = (shop_guid,  gateway_guid, currency_guid, page, items) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_ONE_TIME_CHARGES
    });
    try {
      const response = await getOneTimeCharges(shop_guid,  gateway_guid, currency_guid, page, items);
      return dispatch({
        type: types.GET_ONE_TIME_CHARGES_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_ONE_TIME_CHARGES_FAILED
      });
    }
  };
};

export const getPeriodicChargesAction = (shop_guid,  gateway_guid, currency_guid, page, items) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_PERIODIC_CHARGES
    });
    try {
      const response = await getPeriodicCharges(shop_guid,  gateway_guid, currency_guid, page, items);
      return dispatch({
        type: types.GET_PERIODIC_CHARGES_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_PERIODIC_CHARGES_FAILED
      });
    }
  };
};

export const getConditionalChargesAction = (shop_guid,  gateway_guid, currency_guid, page, items) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_CONDITIONAL_CHARGES
    });
    try {
      const response = await getConditionalCharges(shop_guid,  gateway_guid, currency_guid, page, items);
      return dispatch({
        type: types.GET_CONDITIONAL_CHARGES_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_CONDITIONAL_CHARGES_FAILED
      });
    }
  };
};

export const addOneTimeChargeAction = (data) => {
  return async (dispatch) => {
    try {
      await createOneTimeCharge(data);
    } catch (error) {
      throw (error);
    }
  };
};

export const addPeriodicChargeAction = (data) => {
  return async (dispatch) => {
    try {
      await createPeriodicCharge(data);
    } catch (error) {
      throw (error);
    }
  };
};

export const addConditionalChargeAction = (data) => {
  return async (dispatch) => {
    try {
      await createConditionalCharge(data);
    } catch (error) {
      throw (error);
    }
  };
};

export const editRateAction = (data) => {
  return async (dispatch) => {
    try {
      const response = await updateRate(data);
      dispatch({
        type: types.EDIT_RATE,
        rate: response.data
      });
    } catch (error) {
      throw (error);
    }
  };
};

export const deleteRateAction = (shop_guid, gateway_guid, currency_guid, page, items, search) => {
  return async (dispatch) => {
    try {
      await deleteRate(shop_guid, gateway_guid, currency_guid);
      return dispatch(getAllRates(page, items, search));
    } catch (error) {
      throw (error);
    }
  };
};