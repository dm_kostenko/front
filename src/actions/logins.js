import * as types from "../constants/actionTypes";
import { 
  addLoginRoles, 
  getLogins, 
  getFreeLogins,
  getLogin, 
  getLoginUserProfile,
  deleteLogin, 
  createLogin, 
  updateLogin, 
  getLoginRoles 
} from "../services/paymentBackendAPI/management/logins";
import {
  getNewClients
} from "../services/paymentBackendAPI/reports/entities";

export const getAllLogins = (page, items, search) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_LOGINS
    });
    try {
      const response = await getLogins(page, items, search);
      dispatch({
        type: types.GET_LOGINS_SUCCEED,
        data: response.data,
        pageSize: items, 
        currentPage: page
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_LOGINS_FAILED
      });
    }
  };
};

export const getLoginUserProfileAction = (id) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_LOGIN_USER_PROFILE
    });
    try {
      const response = await getLoginUserProfile(id);
      return dispatch({
        type: types.GET_LOGIN_USER_PROFILE_SUCCEED,
        userProfile: response.data
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_LOGIN_USER_PROFILE_FAILED
      });
    }
  };
};

export const getFreeLoginsAction = (entity) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_FREE_LOGINS
    });
    try {
      const response = await getFreeLogins(entity);
      dispatch({
        type: types.GET_FREE_LOGINS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_FREE_LOGINS_FAILED
      });
    }
  };
};

export const getLoginAction = (id) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_LOGIN
    });
    try {
      const response = await getLogin(id);
      return dispatch({
        type: types.GET_LOGIN_SUCCEED,
        login: response.data
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_LOGIN_FAILED
      });
    }
  };
};

export const deleteLoginAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      await deleteLogin(data);
      return dispatch(getAllLogins(page, items, search));
    }
    catch (error) {
      throw (error);
    }
  };
};

export const addLoginAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      const response = await createLogin(data);
      dispatch({
        type: types.ADD_LOGIN,
        login: response.data
      });
      dispatch(getAllLogins(page, items, search));
    }
    catch (error) {
      throw (error);
    }
  };
};

export const addLoginRoleAction = (id, data) => {
  return async (dispatch) => {
    try {
      const response = await addLoginRoles(id, data);
      dispatch({
        type: types.ADD_LOGIN_ROLE,
        loginRole: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};


export const getLoginRolesAction = (id, page, items) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_LOGIN_ROLES
    });
    try {
      const response = await getLoginRoles(id, page, items);
      return dispatch({
        type: types.GET_LOGIN_ROLES_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_LOGIN_ROLES_FAILED
      });
      throw (error);
    }
  };
};

export const editLoginAction = (data) => {
  return async (dispatch) => {
    try {
      const response = await updateLogin(data);
      dispatch({
        type: types.EDIT_LOGIN,
        login: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const getNewLoginsCountAction = (days) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_NEW_LOGINS_COUNT,
    });
    try {
      const response = await getNewClients(days);
      return dispatch({
        type: types.GET_NEW_LOGINS_COUNT_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_NEW_LOGINS_COUNT_FAILED,
      });
      throw (error);
    }
  };
};