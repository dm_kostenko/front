import * as types from "../constants/actionTypes";
import {
  getPartners,
  getPartner,
  getPartnerLogins,
  getPartnerGroups,
  addPartnerLogin,
  updatePartner,
  deletePartner,
  createPartner,
  getPartnerMerchants
} from "../services/paymentBackendAPI/management/partners";
import {
  deleteGroupFromPartner, addGroupToPartner
} from "../services/paymentBackendAPI/management/groups";
import { getAllGroups } from "./groups";
import { getTopPartners } from "../services/paymentBackendAPI/reports/entities";

export const getAllPartners = (page, items, search) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_PARTNERS
    });
    try {
      const response = await getPartners(page, items, search);
      return dispatch({
        type: types.GET_PARTNERS_SUCCEED,
        data: response.data,
        pageSize: items, 
        currentPage: page
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_PARTNERS_FAILED
      });
    }
  };
};

export const getPartnerAction = (id) => {
  return async (dispatch) => {
    dispatch({
      type:types.GET_PARTNER
    });
    try {
      const response = await getPartner(id);
      return dispatch({
        type: types.GET_PARTNER_SUCCEED,
        partner: response.data
      });
    }
    catch (error) {
      return dispatch({
        type:types.GET_PARTNER_FAILED
      });
    }
  };
};

export const editPartnerAction = (data) => {
  return async (dispatch) => {
    try {
      const response = await updatePartner(data);
      dispatch({
        type: types.EDIT_PARTNER,
        partner: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const deletePartnerAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      await deletePartner(data);
      return dispatch(getAllPartners(page, items, search));
    }
    catch (error) {
      throw (error);
    }
  };
};

export const addGroupToPartnerAction = (guid, partner_guid, page, items) => {
  return async (dispatch) => {
    try {
      await addGroupToPartner(guid, partner_guid);
      return dispatch(getAllGroups(page, items));
    } catch(error) {
      throw (error);
    }
  };
};

export const deleteGroupFromPartnerAction = (guid, page, items, partnerGuid) => {
  return async (dispatch) => {
    try {
      await deleteGroupFromPartner(guid);
      return dispatch(getAllGroups(page, items));
    } catch(error) {
      throw (error);
    }
  };
};

export const getPartnerLoginsAction = (id, page, items) => {
  return async (dispatch) => {
    dispatch({
      type:types.GET_PARTNER_LOGINS
    });
    try {
      const response = await getPartnerLogins(id, page, items);
      dispatch({
        type: types.GET_PARTNER_LOGINS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const getPartnerGroupsAction = (id, page, items) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_PARTNER_GROUPS
    });
    try {
      const response = await getPartnerGroups(id, page, items);
      dispatch({
        type: types.GET_PARTNER_GROUPS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_PARTNER_GROUPS_FAILED
      });
    }
  };
};


export const getPartnerMerchantsAction = (id, page, items) => {
  return async (dispatch) => {
    try {
      const response = await getPartnerMerchants(id, page, items);
      dispatch({
        type: types.GET_PARTNER_MERCHANTS,
        data: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};


export const addPartnerAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      const response = await createPartner(data);
      dispatch({
        type: types.ADD_PARTNER,
        data: response.data
      });
      return dispatch(getAllPartners(page, items, search));
    }
    catch (error) {
      throw (error);
    }
  };
};

export const addPartnerLoginAction = (id, data) => {
  return async (dispatch) => {
    try {
      const response = await addPartnerLogin(id, data);
      dispatch({
        type: types.ADD_PARTNER_LOGIN,
        partnerLogin: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const upsertChangedGroupAction = (group) => {
  return (dispatch) => {
    try {
      dispatch({
        type: types.UPSERT_PARTNER_CHANGED_GROUP,
        group: group
      });
    }
    catch (error) {
      throw (error);
    }   
  };
};

export const getTopPartnersAction = (days, count) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_TOP_PARTNERS,
    });
    try {
      const response = await getTopPartners(days, count);
      return dispatch({
        type: types.GET_TOP_PARTNERS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_TOP_PARTNERS_FAILED,
      });
      throw (error);
    }
  };
};