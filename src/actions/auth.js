import * as types from "../constants/actionTypes";
import auth from "../services/paymentBackendAPI/auth/auth";

export const logIn = (data) => {
  return async (dispatch) => {
    try {
      const response = await auth.login(data);
      return dispatch({
        type: types.LOG_IN_SUCCESS,
        data: response.data
      });
    }
    catch(error) {
      return dispatch({
        type: types.LOG_IN_FAILED,
        error: error
      });
    }
  };
};

export const logOut = () => {
  return (dispatch) => {
    return auth.logout()
      .then(() => {
        dispatch({
          type: types.LOG_OUT,
        });
      })
      .catch(error => {
        throw (error);
      });
  };
};