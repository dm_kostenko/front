import * as types from "../constants/actionTypes";
import { getCurrencies, getCurrency, createCurrency, updateCurrency, deleteCurrency } from "../services/paymentBackendAPI/management/currencies";
import { getTransactionsCurrencies, getDailyTransaction } from "services/paymentBackendAPI/reports/transactions";

export const getAllCurrencies = (page, items, search) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_CURRENCIES
    });
    try {
      const res = await getCurrencies(page, items, search);
      return dispatch({
        type: types.GET_CURRENCIES_SUCCEED,
        data: res.data,
        pageSize: items,
        currentPage: page
      });
    } catch (error) {
      return dispatch({
        type: types.GET_CURRENCIES_FAILED
      });
    }
  };
};

export const getCurrencyAction = (id) => {
  return async(dispatch) => {
    dispatch({
      type:types.GET_CURRENCY
    });
    try {
      const res = await getCurrency(id);
      return dispatch({
        type: types.GET_CURRENCY_SUCCEED,
        data: res.data,
      });
    } catch (error) {
      return dispatch({
        type: types.GET_CURRENCY_FAILED
      });
    }
  };
};

export const addCurrencyAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      await createCurrency(data);
      return dispatch(getAllCurrencies(page, items, search));
    } catch (error) {
      throw (error);
    }
  };
};

export const editCurrencyAction = (data) => {
  return async (dispatch) => {
    try {
      const response = await updateCurrency(data);
      dispatch({
        type: types.EDIT_CURRENCY,
        currency: response.data
      });
    } catch (error) {
      throw (error);
    }
  };
};

export const deleteCurrencyAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      await deleteCurrency(data);
      return dispatch(getAllCurrencies(page, items, search));

    } catch (error) {
      throw (error);
    }
  };
};

export const getReportCurrencyAction = (params) => {
  return async dispatch => {
    dispatch({
      type: types.GET_REPORT_CURRENCY,
    });
    try {
      const response = await getTransactionsCurrencies(params);
      return dispatch({
        type: types.GET_REPORT_CURRENCY_SUCCEED,
        data: response.data
      });
    } catch (error) {
      dispatch({
        type: types.GET_REPORT_CURRENCY_FAILED,
      });
      throw error;
    }
  };
};

export const getReportDailyCurrencyAction = () => {
  return async dispatch => {
    dispatch({
      type: types.GET_REPORT_DAILY_CURRENCY,
    });
    try {
      const response = await getDailyTransaction();
      return dispatch({
        type: types.GET_REPORT_DAILY_CURRENCY_SUCCEED,
        data: response.data
      });
    } catch (error) {
      dispatch({
        type: types.GET_REPORT_DAILY_CURRENCY_FAILED,
      });
      throw error;
    }
  };
};