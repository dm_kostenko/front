import * as types from "../constants/actionTypes";
import {
  getUsersLogs, 
  getTransactionsLogsAdmin,
  getTransactionsStepsLogs,
  getMerchantTransactionsLogs,
  getGroupTransactionsLogs,
  getPartnerTransactionsLogs
} from "../services/paymentBackendAPI/reports/logs";
import { getAuthData } from "services/paymentBackendAPI/backendPlatform";

export const getMerchantsLogsAction = (page, items, search) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_USERS_LOGS
    });
    try {
      const response = await getUsersLogs({ 
        userType: "merchant",
        page, 
        items, 
        search
      });
      dispatch({
        type: types.GET_USERS_LOGS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_USERS_LOGS_FAILED
      });
    }
  };
};

export const getGroupsLogsAction = (page, items, search) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_USERS_LOGS
    });
    try {
      const response = await getUsersLogs({ 
        userType: "group",
        page, 
        items, 
        search
      });
      dispatch({
        type: types.GET_USERS_LOGS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_USERS_LOGS_FAILED
      });
    }
  };
};

export const getPartnersLogsAction = (page, items, search) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_USERS_LOGS
    });
    try {
      const response = await getUsersLogs({ 
        userType: "partner",
        page, 
        items, 
        search
      });
      dispatch({
        type: types.GET_USERS_LOGS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_USERS_LOGS_FAILED
      });
    }
  };
};

export const getTransactionsLogsAction = (page, items, search) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_TRANSACTIONS_LOGS
    });
    try {
      const token = getAuthData() ? getAuthData().userPayload : {};
      let response = undefined;
      if (token.merchant)
        response = await getMerchantTransactionsLogs(page, items, search);
      else if (token.group)
        response = await getGroupTransactionsLogs(page, items, search);
      else if (token.partner)
        response = await getPartnerTransactionsLogs(page, items, search);
      else
        response = await getTransactionsLogsAdmin(page, items, search);
      dispatch({
        type: types.GET_TRANSACTIONS_LOGS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_TRANSACTIONS_LOGS_FAILED
      });
    }
  };
};

export const getTransactionsStepsLogsAction = (guid, page, items) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_TRANSACTIONS_STEPS_LOGS
    });
    try {
      const response = await getTransactionsStepsLogs(guid, page, items);
      return dispatch({
        type: types.GET_TRANSACTIONS_STEPS_LOGS_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_TRANSACTIONS_STEPS_LOGS_FAILED
      });
    }
  };
};