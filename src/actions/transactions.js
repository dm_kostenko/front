import * as types from "../constants/actionTypes";
import {
  getTransactionsCurrencies,
  getDailyTransaction,
  getOrders,
  getShopTotals,
  getTotals,
  getTotalProcessed,
  getTotalToClient,
  getTotalToProcessor,
  getTransactionHistory,
  getTransactionTypes,
  getBalance,
  getCurrenciesOfTransactions
} from "../services/paymentBackendAPI/reports/transactions";
import { createTransaction } from "../services/paymentBackendAPI/transactions/transactions";
import {
  getTransactions,
  getTransactionTemplates,
  deleteTransactionTemplate,
  getTransactionTemplate,
  getTransactionSteps,
  getTransactionProcessingSteps,
  getTransactionProcessingParams,
  getTransactionsForGateway
} from "../services/paymentBackendAPI/management/transactions";
import { getStep } from "../services/paymentBackendAPI/management/steps";
// import {addTransactionParam} from "../services/paymentBackendAPI/management/params";
// import {addTransactionStep} from "../services/paymentBackendAPI/management/steps";

export const getAllTransactions = (page, items, search) => {
  return async dispatch => {
    dispatch({
      type: types.GET_TRANSACTIONS
    });
    try {
      const response = await getTransactions(page, items, search);
      dispatch({
        type: types.GET_TRANSACTIONS_SUCCEED,
        data: response.data
      });
    } catch (error) {
      dispatch({
        type: types.GET_TRANSACTIONS_FAILED
      });
    }
  };
};

export const getMerchantTransactionsAction = (page, items, search) => {
  return async dispatch => {
    try {
      const response = await getTransactionHistory(page, items, search);
      dispatch({
        type: types.GET_MERCHANT_TRANSACTIONS,
        data: response.data
      });
    } catch (error) {
      throw error;
    }
  };
};

export const addTransactionAction = (data, shopGuid, shopSecret, shopHashkey) => {
  return async (dispatch) => {
    try {
      const response = await createTransaction(data, shopGuid, shopSecret, shopHashkey);
      dispatch({
        type: types.ADD_TRANSACTION,
        transaction: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const addParamTransaction = data => {
  return async dispatch => {
    try {
      dispatch({
        type: types.ADD_PARAM_TRANSACTION,
        data
      });
    } catch (error) {
      throw error;
    }
  };
};

export const cleanParams = () => {
  return async dispatch => {
    try {
      dispatch({
        type: types.CLEAN_PARAMS
      });
    } catch (error) {
      throw error;
    }
  };
};

export const getTransactionTemplatesAction = (page, items, search) => {
  return async dispatch => {
    dispatch({
      type: types.GET_TRANSACTION_TEMPLATES
    });
    try {
      const response = await getTransactionTemplates(page, items, search);
      dispatch({
        type: types.GET_TRANSACTION_TEMPLATES_SUCCEED,
        data: response.data,
        pageSize: items,
        currentPage: page
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_TRANSACTION_TEMPLATES_FAILED
      });
    }
  };
};

export const deleteTransactionTemplateAction = (data, page, items) => {
  return async (dispatch) => {
    try {
      await deleteTransactionTemplate(data);
      return dispatch(getTransactionTemplatesAction(page, items));
    }
    catch (error) {
      throw (error);
    }
  };
};

export const getTransactionTemplateAction = (id) => {
  return async (dispatch) => {
    try {
      const response = await getTransactionTemplate(id);
      dispatch({
        type: types.GET_TRANSACTION_TEMPLATE,
        template: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const getTransactionStepsAction = (id, page, items, gateway_guid) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_TRANSACTION_STEPS
    });
    try {
      const response = await getTransactionSteps(id, page, items, gateway_guid);
      dispatch({
        type: types.GET_TRANSACTION_STEPS_SUCCEED,
        data: response.data,
        guid: gateway_guid
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_TRANSACTION_STEPS_FAILED
      });
    }
  };
};

export const getStepAction = (id) => {
  return async (dispatch) => {
    try {
      const response = await getStep(id);
      dispatch({
        type: types.GET_STEP,
        step: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};


export const getTransactionProcessingStepsAction = (id, page, items) => {
  return async (dispatch) => {
    try {
      const response = await getTransactionProcessingSteps(id, page, items);
      dispatch({
        type: types.GET_TRANSACTION_PROCESSING_STEPS,
        data: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const getTransactionProcessingParamsAction = (id, page, items) => {
  return async (dispatch) => {
    try {
      const response = await getTransactionProcessingParams(id, page, items);
      dispatch({
        type: types.GET_TRANSACTION_PROCESSING_PARAMS,
        data: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const getTransactionsForGatewayAction = (id) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_TRANSACTIONS_FOR_GATEWAY
    });
    try {
      const response = await getTransactionsForGateway(id);
      dispatch({
        type: types.GET_TRANSACTIONS_FOR_GATEWAY_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_TRANSACTIONS_FOR_GATEWAY_FAILED
      });
    }
  };
};

/***********************( ╯°□°)╯┻┻**************************/


export const getTransactionHistoryAction = (data) => {
  return async dispatch => {
    dispatch({
      type: types.GET_TRANSACTION_HISTORY,
    });
    try {
      const response = await getTransactionHistory(data);
      return dispatch({
        type: types.GET_TRANSACTION_HISTORY_SUCCEED,
        data: response.data
      });
    } catch (error) {
      dispatch({
        type: types.GET_TRANSACTION_HISTORY_FAILED,
      });
      throw error;
    }
  };
};

export const getTransactionsCurrenciesAction = (data) => {
  return async dispatch => {
    dispatch({
      type: types.GET_TRANSACTION_CURRENCIES,
    });
    try {
      const response = await getTransactionsCurrencies(data);
      return dispatch({
        type: types.GET_TRANSACTION_CURRENCIES_SUCCEED,
        data: response.data
      });
    } catch (error) {
      dispatch({
        type: types.GET_TRANSACTION_CURRENCIES_FAILED,
      });
      throw error;
    }
  };
};

export const getCurrenciesOfTransactionsAction = (data) => {
  return async dispatch => {
    dispatch({
      type: types.GET_CURRENCIES_OF_TRANSACTIONS,
    });
    try {
      const response = await getCurrenciesOfTransactions(data);
      return dispatch({
        type: types.GET_CURRENCIES_OF_TRANSACTIONS_SUCCEED,
        data: response.data
      });
    } catch (error) {
      dispatch({
        type: types.GET_CURRENCIES_OF_TRANSACTIONS_FAILED,
      });
      throw error;
    }
  };
};

export const getDailyTransactionAction = (data) => {
  return async dispatch => {
    dispatch({
      type: types.GET_DAILY_TRANSACTION,
    });
    try {
      const response = await getDailyTransaction(data);
      return dispatch({
        type: types.GET_DAILY_TRANSACTION_SUCCEED,
        data: response.data
      });
    } catch (error) {
      dispatch({
        type: types.GET_DAILY_TRANSACTION_FAILED,
      });
      throw error;
    }
  };
};

export const getOrdersAction = (data) => {
  return async dispatch => {
    dispatch({
      type: types.GET_ORDERS,
    });
    try {
      const response = await getOrders(data);
      return dispatch({
        type: types.GET_ORDERS_SUCCEED,
        data: response.data
      });
    } catch (error) {
      dispatch({
        type: types.GET_ORDERS_FAILED,
      });
      throw error;
    }
  };
};

export const getShopTotalsAction = (data) => {
  return async dispatch => {
    dispatch({
      type: types.GET_SHOP_TOTALS,
    });
    try {
      const response = await getShopTotals(data);
      return dispatch({
        type: types.GET_SHOP_TOTALS_SUCCEED,
        data: response.data
      });
    } catch (error) {
      dispatch({
        type: types.GET_SHOP_TOTALS_FAILED,
      });
      throw error;
    }
  };
};

export const getTotalsAction = (data) => {
  return async dispatch => {
    dispatch({
      type: types.GET_TOTALS,
    });
    try {
      const response = await getTotals(data);
      return dispatch({
        type: types.GET_TOTALS_SUCCEED,
        data: response.data
      });
    } catch (error) {
      dispatch({
        type: types.GET_TOTALS_FAILED,
      });
      throw error;
    }
  };
};

export const getTotalProcessedAction = (data) => {
  return async dispatch => {
    if (data.shop_guid) {
      try {
        dispatch({
          type: types.GET_SHOP_TOTAL_PROCESSED,
        });

        const response = await getTotalProcessed(data);
        return dispatch({
          type: types.GET_SHOP_TOTAL_PROCESSED_SUCCEED,
          amount: response.data.amount,
          guid: data.shop_guid
        });
      } catch (error) {
        dispatch({
          type: types.GET_SHOP_TOTAL_PROCESSED_FAILED,
        });
        throw error;
      }
    }
    else {
      try {
        dispatch({
          type: types.GET_TOTAL_PROCESSED,
        });

        const response = await getTotalProcessed(data.days);
        return dispatch({
          type: types.GET_TOTAL_PROCESSED_SUCCEED,
          amount: response.data.amount
        });
      } catch (error) {
        dispatch({
          type: types.GET_TOTAL_PROCESSED_FAILED,
        });
        throw error;
      }
    }
  };
};

export const getTotalToClientAction = (data) => {
  return async dispatch => {
    dispatch({
      type: types.GET_TOTAL_TO_CLIENT,
    });
    try {
      const response = await getTotalToClient(data);
      return dispatch({
        type: types.GET_TOTAL_TO_CLIENT_SUCCEED,
        data: response.data
      });
    } catch (error) {
      dispatch({
        type: types.GET_TOTAL_TO_CLIENT_FAILED,
      });
      throw error;
    }
  };
};

export const getTotalToProcessorAction = (data) => {
  return async dispatch => {
    dispatch({
      type: types.GET_TOTAL_TO_PROCESSOR,
    });
    try {
      const response = await getTotalToProcessor(data);
      return dispatch({
        type: types.GET_TOTAL_TO_PROCESSOR_SUCCEED,
        data: response.data
      });
    } catch (error) {
      dispatch({
        type: types.GET_TOTAL_TO_PROCESSOR_FAILED,
      });
      throw error;
    }
  };
};

export const getTransactionTypesAction = (data) => {
  if (data && data.shop_guid) {
    return async dispatch => {
      dispatch({
        type: types.GET_SHOP_TRANSACTION_TYPE,
      });
      try {
        const response = await getTransactionTypes(data);
        return dispatch({
          type: types.GET_SHOP_TRANSACTION_TYPE_SUCCEED,
          data: response.data
        });
      } catch (error) {
        dispatch({
          type: types.GET_SHOP_TRANSACTION_TYPE_FAILED,
        });
        throw error;
      }
    };
  }
  else {
    return async dispatch => {
      dispatch({
        type: types.GET_TRANSACTION_TYPE,
      });
      try {
        const response = await getTransactionTypes(data);
        return dispatch({
          type: types.GET_TRANSACTION_TYPE_SUCCEED,
          data: response.data
        });
      } catch (error) {
        dispatch({
          type: types.GET_TRANSACTION_TYPE_FAILED,
        });
        throw error;
      }
    };
  }
};

export const getBalanceAction = (data) => {
  return async dispatch => {
    dispatch({
      type: types.GET_BALANCE,
    });
    try {
      const response = await getBalance(data);
      return dispatch({
        type: types.GET_BALANCE_SUCCEED,
        data: response.data
      });
    } catch (error) {
      dispatch({
        type: types.GET_BALANCE_FAILED,
      });
      throw error;
    }
  };
};

