import * as types from "../constants/actionTypes";

export const inverseSearch = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INVERSE_SEARCH,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};


export const reset = () => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.RESET_SEARCH
      });
    }
    catch (error) {
      throw (error);
    }
  };
};


export const searchInMerchantsLogs = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_MERCHANTS_LOGS,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInGroupsLogs = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_GROUPS_LOGS,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInPartnersLogs = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_PARTNERS_LOGS,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInTransactionsLogs = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_TRANSACTIONS_LOGS,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInAccounts = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_ACCOUNTS,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInBlacklist = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_BLACKLIST,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInGlobalBlacklist = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_GLOBAL_BLACKLIST,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInMerchantsBlacklist = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_MERCHANTS_BLACKLIST,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInCards = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_CARDS,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInCurrencies = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_CURRENCIES,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};


export const searchInGateways = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_GATEWAYS,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInGroups = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_GROUPS,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInLogins = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_LOGINS,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInMerchants = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_MERCHANTS,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInPartners = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_PARTNERS,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInPrivileges = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_PRIVILEGES,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInRates = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_RATES,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInRoles = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_ROLES,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInShops = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_SHOPS,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInTransactions = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_TRANSACTIONS,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInTransactionsTemplates = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_TRANSACTIONS_TEMPLATES,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const searchInMerchantTransactions = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.INPUT_SEARCH_IN_MERCHANT_TRANSACTIONS,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const setCurrentSearchData = (data) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: types.SET_CURRENT_SEARCH_DATA,
        data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};