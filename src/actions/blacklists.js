import * as types from "../constants/actionTypes";
import {
  getBlackListItems,
  createBlackListItem,
  getBlackListItem,
  updateBlackListItem,
  deleteBlackListItem,
  createBlackListRule,
  getBlackListGlobal,
  getMerchantsBlackList,
  createItemGlobalBlackList,
  deleteItemGlobalBlackList,
  createItemMerchantsBlackList,
  deleteItemMerchantsBlackList
} from "../services/paymentBackendAPI/management/blacklists";

export const getAllBlackListItems = (page, items, search) => {
  return async dispatch => {
    dispatch({
      type: types.GET_BLACKLIST_ITEMS
    });
    try {
      const response = await getBlackListItems(page, items, search);
      dispatch({
        type: types.GET_BLACKLIST_ITEMS_SUCCEED,
        data: response.data,
        pageSize: items,
        currentPage: page
      });
    } catch (error) {
      dispatch({
        type: types.GET_BLACKLIST_ITEMS_FAILED
      });
    }
  };
};

export const getBlackListItemAction = id => {
  return async dispatch => {
    dispatch({
      type:types.GET_BLACKLIST_ITEM
    });
    try {
      const response = await getBlackListItem(id);
      return dispatch({
        type: types.GET_BLACKLIST_ITEM_SUCCEED,
        blackItem: response.data
      });
    } catch (error) {
      return dispatch({
        type:types.GET_BLACKLIST_ITEM_FAILED
      });
    }
  };
};

export const addBlackListItemAction = (data, page, items, search) => {
  return async dispatch => {
    try {
      await createBlackListItem(data);
      return dispatch(getAllBlackListItems(page, items, search));
    } catch (error) {
      throw error;
    }
  };
};

export const addBlackListRuleAction = data => {
  return async dispatch => {
    try {
      await createBlackListRule(data);
      dispatch({
        type: types.ADD_BLACKLIST_RULE,
        rule: data.value
      });
    } catch (error) {
      throw error;
    }
  };
};

export const deleteBlackListRuleAction = data => {
  return async dispatch => {
    try {
      const response = await createBlackListRule(data);
      dispatch({
        type: types.DELETE_BLACKLIST_RULE,
        rule: response.data.value
      });
    } catch (error) {
      throw error;
    }
  };
};

export const deleteBlackListItemAction = (data, page, items, search) => {
  return async dispatch => {
    try {
      await deleteBlackListItem(data);
      return dispatch(getAllBlackListItems(page, items, search));
    } catch (error) {
      throw error;
    }
  };
};

export const updateBlackListItemAction = data => {
  return async dispatch => {
    try {
      const response = await updateBlackListItem(data);
      dispatch({
        type: types.UPDATE_BLACKLIST_ITEM,
        blackItem: response.data
      });
    } catch (error) {
      throw error;
    }
  };
};

export const getBlackListGlobalAction = (page, items, search) => {
  return async dispatch => {
    dispatch({
      type: types.GET_BLACKLIST_GLOBAL
    });
    try {
      const response = await getBlackListGlobal(page, items, search);
      dispatch({
        type: types.GET_BLACKLIST_GLOBAL_SUCCEED,
        data: response.data,
        pageSize: items,
        currentPage: page
      });
    } catch (error) {
      dispatch({
        type: types.GET_BLACKLIST_GLOBAL_FAILED
      });
    }
  };
};

export const getMerchantsBlackListAction = (page, items, search) => {
  return async dispatch => {
    dispatch({
      type: types.GET_BLACKLIST_MERCHANTS
    });
    try {
      const response = await getMerchantsBlackList(page, items, search);
      dispatch({
        type: types.GET_BLACKLIST_MERCHANTS_SUCCEED,
        data: response.data,
        pageSize: items,
        currentPage: page
      });
    } catch (error) {
      dispatch({
        type: types.GET_BLACKLIST_MERCHANTS_FAILED
      });
    }
  };
};

export const createItemGlobalBlackListAction = (data, page, items, search) => {
  return async dispatch => {
    try {
      await createItemGlobalBlackList(data);
      return dispatch(getBlackListGlobalAction(page, items, search));
    } catch (error) {
      throw error;
    }
  };
};

export const deleteItemGlobalBlackListAction = (data, page, items, search) => {
  return async dispatch => {
    try {
      await deleteItemGlobalBlackList(data);
      return dispatch(getBlackListGlobalAction(page, items, search));
    } catch (error) {
      throw error;
    }
  };
};

export const createItemMerchantsBlackListAction = (data, page, items, search) => {
  return async dispatch => {
    try {
      await createItemMerchantsBlackList(data);
      return dispatch(getMerchantsBlackListAction(page, items, search));
    } catch (error) {
      throw error;
    }
  };
};

export const deleteItemMerchantsBlackListAction = (data, page, items, search) => {
  return async dispatch => {
    try {
      await deleteItemMerchantsBlackList(data.id, data.guid);
      return dispatch(getMerchantsBlackListAction(page, items, search));
    } catch (error) {
      throw error;
    }
  };
};