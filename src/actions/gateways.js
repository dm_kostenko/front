import * as types from "../constants/actionTypes";
import {
  createGateway,
  getGateways,
  getGateway,
  updateGateway,
  deleteGateway,
  getGatewayProps,
  addGatewayProps,
  deleteGatewayProps
} from "../services/paymentBackendAPI/management/gateways";

export const getAllGateways = (page, items, search) => {
  return async (dispatch) => {
    dispatch({
      type:types.GET_GATEWAYS
    });
    try {
      const response = await getGateways(page, items, search);
      return dispatch({
        type: types.GET_GATEWAYS_SUCCEED,
        data: response.data,
        pageSize: items,
        currentPage: page
      });
    } catch (error) {
      return dispatch({
        type:types.GET_GATEWAYS_FAILED
      });
    }
  };
};

export const addGatewayAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      await createGateway(data);
      return dispatch(getAllGateways(page, items, search));
    } catch (error) {
      throw (error);
    }
  };
};

export const getGatewayAction = (data) => {
  return async (dispatch) => {
    dispatch({ type: 
      types.GET_GATEWAY
    });
    try {
      const response = await getGateway(data);
      return dispatch({
        type: types.GET_GATEWAY_SUCCEED,
        gateway: response.data
      });
    } catch (error) {
      dispatch({ type: 
        types.GET_GATEWAY_FAILED
      });
    }
  };
};

export const deleteGatewayAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      await deleteGateway(data);
      return dispatch(getAllGateways(page, items, search));
    } catch (error) {
      throw (error);
    }
  };
};

export const editGatewayAction = (data) => {
  return async (dispatch) => {
    try {
      const response = await updateGateway(data);
      dispatch({
        type: types.EDIT_GATEWAY,
        gateway: response.data
      });
    } catch (error) {
      throw (error);
    }
  };
};

export const getGatewayPropsAction = (guid) => {
  return async (dispatch) => {
    dispatch({
      type:types.GET_GATEWAY_PROPS
    });
    try {
      const response = await getGatewayProps(guid);
      return dispatch({
        type: types.GET_GATEWAY_PROPS_SUCCEED,
        data: response.data.data,
      });
    } catch (error) {
      return dispatch({
        type:types.GET_GATEWAY_PROPS_FAILED
      });
    }
  };
};

export const deleteGatewayPropsAction = (guid, data) => {
  return async (dispatch) => {
    try {
      await deleteGatewayProps(guid, data);
      return dispatch(getGatewayPropsAction(guid));
    } catch (error) {
      throw (error);
    }
  };
};

export const addGatewayPropsAction = (guid, data) => {
  return async (dispatch) => {
    try {
      await addGatewayProps(guid, data);
      return dispatch(getGatewayPropsAction(guid));
    } catch (error) {
      throw (error);
    }
  };
};