import * as types from "../constants/actionTypes";
import {
  addPrivilegeToRole,
  createRole,
  getRoles,
  getRole,
  getPrivilegesFromRole,
  deleteRole,
  upsertPrivilegeToRole,
  updateRole
} from "../services/paymentBackendAPI/management/roles";

export const getAllRoles = (page, items, search, type) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_ROLES
    });
    try {
      const response = await getRoles(page, items, search, type);
      return dispatch({
        type: types.GET_ROLES_SUCCEED,
        data: response.data,
        pageSize: items,
        currentPage: page
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_ROLES_FAILED
      });
    }
  };
};

export const getRoleAction = (id) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_ROLE
    });
    try {
      const response = await getRole(id);
      return dispatch({
        type: types.GET_ROLE_SUCCEED,
        role: response.data
      });
    }
    catch (error) {
      return dispatch({
        type: types.GET_ROLE_FAILED
      });
    }
  };
};

export const getRolePrivilegesAction = (id, page, items) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_ROLE_PRIVILEGES
    });
    try {
      const response = await getPrivilegesFromRole(id, page, items);
      return dispatch({
        type: types.GET_ROLE_PRIVILEGES_SUCCEED,
        data: response.data
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_ROLE_PRIVILEGES_FAILED
      });
      throw (error);
    }
  };
};

export const addRoleAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      const response = await createRole(data);
      dispatch({
        type: types.ADD_ROLE,
        role: response.data
      });
      dispatch(getAllRoles(page, items, search));
    }
    catch (error) {
      throw (error);
    }
  };
};

export const updateRoleAction = (data) => {
  return async (dispatch) => {
    try {
      const response = await updateRole(data);
      dispatch({
        type: types.UPDATE_ROLE,
        role: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const upsertPrivilegeToRoleAction = (data) => {
  return async (dispatch) => {
    try {
      await upsertPrivilegeToRole(data.guid, data.privileges);
      dispatch({
        type: types.UPSERT_ROLE_PRIVILEGES,
        data: data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const addRolePrivilegeAction = (id, data) => {
  return async (dispatch) => {
    try {
      const response = await addPrivilegeToRole(id, data);
      dispatch({
        type: types.ADD_ROLE_PRIVILEGE,
        rolePrivilege: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const deleteRoleAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      await deleteRole(data);
      return dispatch(getAllRoles(page, items, search));
    }
    catch (error) {
      throw (error);
    }
  };
};

export const upsertChangedPrivilegeAction = (privilege) => {
  return (dispatch) => {
    dispatch({
      type: types.UPSERT_ROLE_CHANGED_PRIVILEGE,
      privilege: privilege
    });
  };
};

