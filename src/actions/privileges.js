import * as types from "../constants/actionTypes";
import {
  createPrivilege,
  deletePrivilege,
  getPrivileges,
  updatePrivilege,
  getPrivilege
} from "../services/paymentBackendAPI/management/privileges";

export const getAllPrivileges = (page, items, search) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_PRIVILEGES
    });
    try {
      const response = await getPrivileges(page, items, search);
      dispatch({
        type: types.GET_PRIVILEGES_SUCCEED,
        data: response.data,
        pageSize: items,
        currentPage: page
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_PRIVILEGES_FAILED
      });
    }
  };
};

export const getPrivilegesWithType = (type, page, items, search) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_PRIVILEGES
    });
    try {
      const response = await getPrivileges(page, items, search, type);
      dispatch({
        type: types.GET_PRIVILEGES_SUCCEED,
        data: response.data,
        pageSize: items,
        currentPage: page
      });
    }
    catch (error) {
      dispatch({
        type: types.GET_PRIVILEGES_FAILED
      });
    }
  };
};

export const getPrivilegeAction = (data) => {
  return async (dispatch) => {
    dispatch({
      type:types.GET_PRIVILEGE
    });
    try {
      const response = await getPrivilege(data);
      return dispatch({
        type: types.GET_PRIVILEGE_SUCCEED,
        privilege: response.data
      });
    }
    catch (error) {
      dispatch({
        type:types.GET_PRIVILEGE_FAILED
      });
    }
  };
};

export const editPrivilegeAction = (data) => {
  return async (dispatch) => {
    try {
      const response = await updatePrivilege(data);
      dispatch({
        type: types.EDIT_PRIVILEGE,
        privilege: response.data
      });
    }
    catch (error) {
      throw (error);
    }
  };
};

export const addPrivilegeAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      await createPrivilege(data);
      return dispatch(getAllPrivileges(page, items, search));
    }
    catch (error) {
      throw (error);
    }
  };
};

export const deletePrivilegeAction = (data, page, items, search) => {
  return async (dispatch) => {
    try {
      await deletePrivilege(data);
      return dispatch(getAllPrivileges(page, items, search));
    }
    catch (error) {
      throw (error);
    }
  };
};
