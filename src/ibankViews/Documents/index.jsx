import React, { Component } from "react";
import { ControlLabel } from "react-bootstrap";
import Content from "ibankViews/Content";
import FFCCTRNS  from "ibankComponents/Documents/FFCCTRNS";
import FFPCRQST  from "ibankComponents/Documents/FFPCRQST";
import PRTRN  from "ibankComponents/Documents/PRTRN";
import ROINVSTG  from "ibankComponents/Documents/ROINVSTG";
import FFPSRPRT  from "ibankComponents/Documents/FFPSRPRT";
import Select from "ibankComponents/UI/MultiSelect";
import { getUserType } from "services/paymentBackendAPI/backendPlatform";

const isAdmin = getUserType() === "admin";

class Documents extends Component {

  state = {
    selectedDirection: "All",
    selectedDocType: "Credit transfer",
    docTypesComponents: {
      "All": {
        "Credit transfer": <FFCCTRNS />,
        "Payment status report": <FFPSRPRT />,
        "Payment cancellation": <FFPCRQST />,
        "Payment return": <PRTRN />,
        "Resolution of investigation": <ROINVSTG />
      },
      "Incoming": {
        "Payment status report": <FFPSRPRT directionData={{ sender: "LIABLT2XMSD" }} />,     // change to our BIC (receiver)
        "Payment return": <PRTRN directionData={{ sender: "LIABLT2XMSD" }} />,
      },
      "Outgoing": {
        "Credit transfer": <FFCCTRNS directionData={{ receiver: "LIABLT2XMSD" }} />,   // change to out BIC (sender)
        "Payment cancellation": <FFPCRQST directionData={{ receiver: "LIABLT2XMSD" }} />,
        "Payment return": <PRTRN directionData={{ receiver: "LIABLT2XMSD" }} />,
        "Resolution of investigation": <ROINVSTG directionData={{ receiver: "LIABLT2XMSD" }} />
      }
    }
  }

  handleSelectDocType = (option) => {
    this.setState({
      selectedType: option.name,
      selectedDocType: option.name
    });
  }

  handleSelectDirection = (option) => {
    this.setState({
      selectedDirection: option.name,
      selectedDocType: Object.keys(this.state.docTypesComponents[option.name])[0]
    });
  }


  render() {
    const isDirectionSelected = this.state.selectedDirection.length;    
    let options = [];
    if(isDirectionSelected) {
      options = isAdmin
        ? Object.keys(this.state.docTypesComponents[this.state.selectedDirection]).map(type => {
          return {
            guid: type,
            name: type
          };
        })
        : [
          {
            guid: "Credit transfer",
            name: "Credit transfer"
          }
        ];
    }

    const docTypeSelectorStyles = {
      control: (styles) => {
        return {
          ...styles,
          position: "absolute !important",
          width: "220px !important",          
          marginLeft: "200px",
          opacity: isDirectionSelected ? "1" : "0",
          disabled: isDirectionSelected ? "false" : "true",
          pointerEvents: isDirectionSelected ? "" : "none",
          transition: ".3s all",
          background: "transparent",
          border: "2px solid #545761 !important",
          boxShadow: "none !important"
        };
      },
      option: (styles, state) => {
        return {
          ...styles,
          background: state.isSelected ? "grey !important" : "rgba(0,0,0,0.6)",
          color: state.isSelected ? "white !important" : "lightgrey !important"
        };
      },
      menu: (styles) => {
        return {
          ...styles,
          borderTopLeftRadius: "0",
          borderTopRightRadius: "0",
          marginTop: "40px",
          marginLeft: "200px",
          position: "absolute !important",
          width: "220px !important",          
          background: "rgba(0,0,0,0.6)",
          color: "lightgrey !important"
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          color: "#545761 !important"
        };
      }
    };

    const directionSelectorStyles = {
      control: (styles) => {
        return {
          ...styles,
          position: "absolute !important",
          width: "150px !important",
          background: "transparent",
          color: "#545761 !important",
          border: "2px solid #545761 !important",
          boxShadow: "none !important"
        };
      },
      option: (styles, state) => {
        return {
          ...styles,
          background: state.isSelected ? "grey !important" : "rgba(0,0,0,0.6)",
          color: state.isSelected ? "white !important" : "lightgrey !important"
        };
      },
      menu: (styles) => {
        return {
          ...styles,
          borderTopLeftRadius: "0",
          borderTopRightRadius: "0",
          marginTop: "40px",
          position: "absolute !important",
          width: "150px !important",
          background: "rgba(0,0,0,0.6)",
          color: "lightgrey !important"
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          color: "#545761 !important"
        };
      }
    };


    return (
      <Content>         
        <ControlLabel style={{ position: "absolute", top: "10px", left: "60px" }}>
          Direction
        </ControlLabel>
        <Select
          multi={false}
          defaultValue={this.state.selectedDirection}
          placeholder={"Select direction"}
          onSelect={this.handleSelectDirection}
          options={[
            {
              guid: "All",
              name: "All"
            },
            {
              guid: "Incoming",
              name: "Incoming"
            },
            {
              guid: "Outgoing",
              name: "Outgoing"
            }             
          ]}
          styles={directionSelectorStyles}
        />
        <ControlLabel style={{ position: "absolute", top: "10px", left: "305px" }}>
          Type
        </ControlLabel>
        <Select
          multi={false}
          defaultValue={this.state.selectedDocType}
          onSelect={this.handleSelectDocType}
          options={options}
          styles={docTypeSelectorStyles}
        />
        <div
          style={
            isDirectionSelected 
              ? { opacity: "1", transition: ".3s all" }
              : { opacity: "0", transition: ".3s all", pointerEvents: "none" }
          }
        >
          {isDirectionSelected && 
            this.state.docTypesComponents[this.state.selectedDirection][this.state.selectedDocType]}
        </div>
      </Content>
    );
  }
}

export default Documents;