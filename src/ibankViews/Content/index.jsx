import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { pushLocation } from "ibankActions/history";
import ConfirmationModal from "ibankComponents/UI/ConfirmationModal";
import Modal from "ibankComponents/UI/Modal/Confirmation";
import Confirmation from "ibankComponents/Confirmation";
import AuthTypeConfirmation from "ibankComponents/AuthTypeConfirmation";
import { connect } from "react-redux";
import { showModal } from "ibankActions/modal";
import { getQR } from "ibankActions/auth";
import config from "config";
import { refreshTokens } from "services/paymentBackendAPI/backendPlatform"

class Content extends Component {
  state = {
    loading: true,
    location: ""
  }
  // const { auth_type, type } = getUserData();
  // if(auth_type !== "login-password-otp" && type !== "admin" && window.location.hash !== "#/profile") {
  //   window.location.replace(process.env.PUBLIC_URL+"#/profile");
  //   setTimeout(async() => {             
  //     await this.props.showModal(true, "profile");
  //     await this.props.getQR();
  //   }, 100);
  // }
componentDidMount = () => {
  this.setState({
    loading: false
  });
}

static getDerivedStateFromProps = (nextProps) => {
  const prevPath = nextProps.history[nextProps.history.length - 1];
  if(nextProps.location.pathname !== prevPath)
    nextProps.pushLocation(nextProps.location.pathname)
  return null;
}

handleMouseMove = async() => {
  await refreshTokens()
}

render () {
  const className = this.props.className 
  ? this.props.className
  : this.props.id === "home" 
    ? "content home" 
    : "content";
  let id = this.state.loading ? "hideContent" : "showContent";

  return (
    <div  
      {...this.props}        
      id={id} 
      className={className}
      onMouseMove={this.handleMouseMove}
    > 
      {this.props.children}
      {config.tfa &&
        <>
        <ConfirmationModal
          header="Confirmation"
          content={<Confirmation/>}
          dialogClassName="confirmation-modal"
        />                         
        <Modal
          header="Confirmation"
          content={<AuthTypeConfirmation/>}
          state="profile"
        />  
      </>}
    </div>
  );
}
}

const mapStateToProps = (state) =>({
  history: state.history.history
});

export default withRouter(connect(mapStateToProps, {
  showModal,
  getQR,
  pushLocation
})(Content));
