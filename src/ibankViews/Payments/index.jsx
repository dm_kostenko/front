import React, { Component } from "react";
import { connect } from "react-redux";
import { Tabs, Tab, ControlLabel } from "react-bootstrap";
import Content from "ibankViews/Content";
import TxTemplatesComponent from "ibankComponents/Transactions/Templates";
import WaitingPayments from "ibankComponents/Transactions/WaitingPayments";
import Send from "ibankComponents/Send";
import { getUserType } from "services/paymentBackendAPI/backendPlatform";
import Select from "ibankComponents/UI/MultiSelect";
import config from "config/";
import { getAllAccounts, getBalancesDocs } from "ibankActions/accounts";
import Spinner from "ibankComponents/UI/Spinner";

const isAdmin = getUserType() === "admin";

const getInternalAccount = {
  [config.iban.settlement.external]: config.iban.settlement.internal,
  [config.iban.customer.external]: config.iban.customer.internal,
  "": ""
}

class Payments extends Component {

  state = {
    selectedAccount: "",
    accountOptions: [],
    balance: 0,
    activeTab: 0
  }

  componentDidMount = async() => {
    this.setState({
      loading: true
    })
    if(isAdmin) {
      await this.props.getBalancesDocs("1", { firstTwo: true })
      const [ settlementBalance, customerBalance ] = this.props.balancesDocs
      await this.setState({
        accountOptions: [
          {
            guid: config.iban.settlement.external,
            name: config.iban.settlement.external,
            balance: settlementBalance?.closingbalance / 100
          },
          {
            guid: config.iban.customer.external,
            name: config.iban.customer.external,
            balance: customerBalance?.closingbalance / 100
          }
        ],
        selectedAccount: config.iban.settlement.external,
        balance: settlementBalance?.closingbalance / 100,
        loading: false
      })
    }
    else {
      await this.props.getAllAccounts()
      let accountOptions = []
      this.props.accounts.forEach(account => {
        accountOptions.push({
          guid: account.iban,
          name: account.number,
          balance: account.balances.find(i => i.currency === "EUR").balance
        })
      })
      await this.setState({
        accountOptions,
        selectedAccount: accountOptions[0].guid,
        balance: accountOptions[0].balance,
        loading: false
      })
    }
  }

  handleSelectAccount = (option) => this.setState({
    selectedAccount: option.guid,
    balance: option.balance
  })

  

  render() {
    const selectorStyles = {
      control: (styles) => {
        return {
          ...styles,
          // position: "absolute !important",
          width: "calc(80px + 8vw) !important",
          height: "calc(10px + 2vw) !important",          
          // marginLeft: "calc(75px + 7.5vw)",
          // opacity: isDirectionSelected ? "1" : "0",
          // disabled: isDirectionSelected ? "false" : "true",
          // pointerEvents: isDirectionSelected ? "" : "none",
          transition: ".3s all",
          background: "transparent",
          border: "2px solid #545761 !important",
          boxShadow: "none !important",
          marginTop: "calc(10px + 1vw) !important"
        };
      },
      option: (styles, state) => {
        return {
          ...styles,
          background: state.isSelected ? "grey !important" : "rgba(0,0,0,0.6)",
          color: state.isSelected ? "white !important" : "lightgrey !important",
          fontSize: "calc(10px + 0.3vw)"
        };
      },
      menu: (styles) => {
        return {
          ...styles,
          borderTopLeftRadius: "0",
          borderTopRightRadius: "0",
          marginTop: "0 !important",
          // marginLeft: "calc(75px + 7.5vw)",
          position: "absolute !important",
          width: "calc(80px + 8vw) !important",          
          background: "rgba(0,0,0,0.6)",
          color: "lightgrey !important"
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          color: "#545761 !important",
          fontSize: "calc(10px + 0.3vw)"
        };
      }
    };

    
    return this.state.loading || this.state.selectedAccount === ""
    ? <Spinner/>
    : (
      <Content>
        <Tabs className="payments-tab" defaultActiveKey={0} transition={true} unmountOnExit={true} style={{ marginLeft: "23px" }}>
          <br />
          <Tab eventKey={0} key="New Payment" title="New Payment" unmountOnExit={true} mountOnEnter={true} style={{ position: "inline-block" }}>
            <Send />
          </Tab>
          <br />
          <Tab eventKey={1} key="Templates" title="Templates" unmountOnExit={true} mountOnEnter={true} style={{ position: "inline-block" }}>
            <ControlLabel  style={{ position: "absolute", marginTop: "calc(-20px - 0.5vw)", left: "calc(50px + 4vw)", fontSize: "calc(10px + 0.3vw)" }}>
              Account
            </ControlLabel>
            <Select
              multi={false}
              name={"selector"}
              placeholder={"Select account"}
              onSelect={this.handleSelectAccount}
              defaultValue={this.state.selectedAccount}
              options={this.state.accountOptions}
              styles={selectorStyles}
            />
            <div className="templates-account-balance">
              {`Available amount: ${this.state.balance.toFixed(2)} EUR`}
            </div>
            <TxTemplatesComponent balance={this.state.balance.toFixed(2)} directionData={{ account: getInternalAccount[this.state.selectedAccount] }} />
          </Tab>
          <br />
          <Tab eventKey={2} key="Waiting payments" title="Waiting payments" unmountOnExit={true} mountOnEnter={true} style={{ position: "inline-block" }}>
            <ControlLabel  style={{ position: "absolute", marginTop: "calc(-20px - 0.5vw)", left: "calc(50px + 4vw)", fontSize: "calc(10px + 0.3vw)" }}>
              Account
            </ControlLabel>
            <Select
              multi={false}
              name={"selector"}
              placeholder={"Select account"}
              onSelect={this.handleSelectAccount}
              defaultValue={this.state.selectedAccount}
              options={this.state.accountOptions}
              styles={selectorStyles}
            />
            <div className="templates-account-balance">
              {`Available amount: ${this.state.balance.toFixed(2)} EUR`}
            </div>
            <WaitingPayments balance={this.state.balance.toFixed(2)} directionData={{ direction: "outgoing", status: "Waiting" }} />          
          </Tab>
        </Tabs>
      </Content>
    );
  }
}

const mapStateToProps = (state) => ({
  accounts: state.accounts.accounts,
  balancesDocs: state.accounts.balancesDocs
})

export default connect(mapStateToProps, {
  getAllAccounts,
  getBalancesDocs
})(Payments);