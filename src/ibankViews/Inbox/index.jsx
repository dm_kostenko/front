import React from "react";
import InboxComponent from "ibankComponents/Documents/Inbox";
import Content from "ibankViews/Content";

const Inbox = () => {
  return (
    <Content>
      <InboxComponent/>
    </Content>
  )
};

export default Inbox;