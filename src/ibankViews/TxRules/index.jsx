import React, { Component } from "react";
import Content from "ibankViews/Content";
import TxRulesComponent from "ibankComponents/Transactions/Rules";

class Transfers extends Component {

  render() {
    return(
      <Content>
        <TxRulesComponent />
      </Content>
    );
  }
}

export default Transfers;