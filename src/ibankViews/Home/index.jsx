import React from "react";
import Content from "ibankViews/Content";
import { Row, Col } from "react-bootstrap";
import AccountsCount from "ibankComponents/Dashboard/Admin/AccountsCount";
import UsersCount from "ibankComponents/Dashboard/Admin/UsersCount";
import TransactionsCount from "ibankComponents/Dashboard/Admin/TransactionsCount";
import TransactionsChartPeriodic from "ibankComponents/Transactions/Chart/Line/Periodic";
import RecentTransactions from "ibankComponents/Dashboard/Admin/RecentTransactions";
import TransactionDirectionCount from "ibankComponents/Transactions/Chart/Bar/DirectionCount";
import TransactionDirectionPie from "ibankComponents/Transactions/Chart/Pie/DirectionAmount";
import TransactionTypesPie from "ibankComponents/Transactions/Chart/Pie/TypesAmount";
import TransactionsDashboardTable from "ibankComponents/Transactions/DashboardTable";
import CurrencyCourses from "ibankComponents/Dashboard/CurrencyCourses";

class Home extends React.Component {

  state = {
    width: 0
  }

  componentDidMount = () => {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  }

  updateDimensions = () => {
    this.setState({ width: window.innerWidth });
  }

  render() {
    return (
      this.state.width > 1150 ?
        <Content>
          <Row className="row1">
            <TransactionsCount />
            <AccountsCount />
            <UsersCount />
          </Row>
          <Row className="row1">
            <TransactionDirectionCount />
            <CurrencyCourses />
            <TransactionsDashboardTable type="transactions"/>
          </Row>
          <Row className="row1">
            <TransactionDirectionPie />
            <TransactionTypesPie />
          </Row>         
          <Row>
            <TransactionsChartPeriodic />
          </Row>
        </Content> :
        <Content>
          <Row>
            <TransactionsCount />
          </Row>
          <Row>
            <AccountsCount />
          </Row>
          <Row>
            <UsersCount />
          </Row>
          <Row>
            <TransactionDirectionCount />
          </Row>
          <Row>
            <TransactionDirectionPie />
          </Row>
          <Row>
            <TransactionTypesPie />
          </Row>
          <Row>
            <TransactionsDashboardTable type="transactions"/>
          </Row>
          <Row>
            <TransactionsChartPeriodic />
          </Row>
        </Content>
    );
  }
}

export default Home;