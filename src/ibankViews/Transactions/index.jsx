import React, { Component } from "react";
import { ControlLabel } from "react-bootstrap";
import Content from "ibankViews/Content";
import FFCCTRNS  from "ibankComponents/Transactions/FFCCTRNS";
import FFPCRQST  from "ibankComponents/Transactions/FFPCRQST";
import PRTRN  from "ibankComponents/Transactions/PRTRN";
import ROINVSTG  from "ibankComponents/Transactions/ROINVSTG";
import FFPSRPRT  from "ibankComponents/Transactions/FFPSRPRT";
import Internal from "ibankComponents/Transactions/Internal";
import All from "ibankComponents/Transactions/All";
import Select from "ibankComponents/UI/MultiSelect";
import { getUserType } from "services/paymentBackendAPI/backendPlatform";

const isAdmin = getUserType() === "admin";

class Transactions extends Component {

  state = {
    selectedDirection: "All",
    isTransactionSelector: true,
    selectedTransactionType: "All",
    transactionTypesComponents: {
      "All": {
        "All": <All directionData={{ direction: "all" }} />,
        "Credit transfer": <FFCCTRNS directionData={{ direction: "all" }} />,
        // "Payment status report": <FFPSRPRT directionData={{ direction: "all" }} />,
        "Payment cancellation": <FFPCRQST directionData={{ direction: "all" }} />,
        "Payment return": <PRTRN directionData={{ direction: "all" }} />,
        "Resolution of investigation": <ROINVSTG directionData={{ direction: "all" }} />
      },
      "Incoming": {        
        "All": <All directionData={{ direction: "incoming" }} />,
        "Credit transfer": <FFCCTRNS directionData={{ direction: "incoming" }} />,
        // "Payment status report": <FFPSRPRT directionData={{ direction: "incoming" }} />,
        "Payment cancellation": <FFPCRQST directionData={{ direction: "incoming" }} />,
        "Payment return": <PRTRN directionData={{ direction: "incoming" }} />,
        "Resolution of investigation": <ROINVSTG directionData={{ direction: "incoming" }} />,
      },
      "Outgoing": {
        "All": <All directionData={{ direction: "outgoing" }} />,
        "Credit transfer": <FFCCTRNS directionData={{ direction: "outgoing" }} />,
        "Payment cancellation": <FFPCRQST directionData={{ direction: "outgoing" }} />,
        "Payment return": <PRTRN directionData={{ direction: "outgoing" }} />,
        "Resolution of investigation": <ROINVSTG directionData={{ direction: "outgoing" }} />,
      },
      "Internal": {
        "Internal": <Internal directionData={{ direction: "internal" }} />
      }
    }
  }

  handleSelectTransactionType = (option) => {
    this.setState({
      selectedType: option.name,
      selectedTransactionType: option.name
    });
  }

  handleSelectDirection = (option) => {
    this.setState({
      isTransactionSelector: option.name !== "Internal",
      selectedDirection: option.name,
      selectedTransactionType: option.name !== "Internal" ? Object.keys(this.state.transactionTypesComponents[option.name])[0] : "Internal"
    });
  }


  render() {
    const isDirectionSelected = this.state.selectedDirection.length;   
    let options = [];
    if(isDirectionSelected) {
      options = isAdmin
        ? Object.keys(this.state.transactionTypesComponents[this.state.selectedDirection]).map(type => {
          return {
            guid: type,
            name: type
          };
        })
        : [
          {
            guid: "Credit transfer",
            name: "Credit transfer"
          },
          {
            guid: "Internal",
            name: "Internal"
          },         
          {
            guid: "All",
            name: "All"
          }
        ];
    }

    const transactionTypeSelectorStyles = {
      control: (styles) => {
        return {
          ...styles,
          position: "absolute !important",
          width: "calc(80px + 8vw) !important",
          height: "calc(10px + 2vw) !important",          
          marginLeft: "calc(75px + 7.5vw)",
          opacity: isDirectionSelected ? "1" : "0",
          disabled: isDirectionSelected ? "false" : "true",
          pointerEvents: isDirectionSelected ? "" : "none",
          transition: ".3s all",
          background: "transparent",
          border: "2px solid #545761 !important",
          boxShadow: "none !important",
          marginTop: "calc(0.1vw - 10px) !important"
        };
      },
      option: (styles, state) => {
        return {
          ...styles,
          background: state.isSelected ? "grey !important" : "rgba(0,0,0,0.6)",
          color: state.isSelected ? "white !important" : "lightgrey !important",
          fontSize: "calc(10px + 0.3vw)"
        };
      },
      menu: (styles) => {
        return {
          ...styles,
          borderTopLeftRadius: "0",
          borderTopRightRadius: "0",
          marginTop: "40px",
          marginLeft: "calc(75px + 7.5vw)",
          position: "absolute !important",
          width: "calc(80px + 8vw) !important",          
          background: "rgba(0,0,0,0.6)",
          color: "lightgrey !important"
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          color: "#545761 !important",
          fontSize: "calc(10px + 0.3vw)"
        };
      }
    };

    const directionSelectorStyles = {
      control: (styles) => {
        return {
          ...styles,
          position: "absolute !important",
          width: "calc(60px + 6vw) !important",
          height: "calc(10px + 2vw) !important",   
          background: "transparent",
          color: "#545761 !important",
          border: "2px solid #545761 !important",
          boxShadow: "none !important",
          fontSize: "calc(10px + 0.3vw)",
          marginTop: "calc(0.1vw - 10px) !important"
        };
      },
      option: (styles, state) => {
        return {
          ...styles,
          background: state.isSelected ? "grey !important" : "rgba(0,0,0,0.6)",
          color: state.isSelected ? "white !important" : "lightgrey !important",
          fontSize: "calc(10px + 0.3vw)"
        };
      },
      menu: (styles) => {
        return {
          ...styles,
          borderTopLeftRadius: "0",
          borderTopRightRadius: "0",
          marginTop: "40px",
          position: "absolute !important",
          width: "calc(60px + 6vw) !important",
          background: "rgba(0,0,0,0.6)",
          color: "lightgrey !important"
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          color: "#545761 !important",
          fontSize: "calc(10px + 0.3vw)"
        };
      }
    };


    return (
      <Content> 
        <ControlLabel style={{ position: "absolute", top: "calc(5px - 0.1vw)", left: "calc(20px + 3vw)", fontSize: "calc(10px + 0.3vw)" }}>
          Direction
        </ControlLabel>
        <Select
          multi={false}
          name={"selector"}
          placeholder={"Select direction"}
          onSelect={this.handleSelectDirection}
          defaultValue={this.state.selectedDirection}
          options={[
            {
              guid: "Incoming",
              name: "Incoming"
            },
            {
              guid: "Outgoing",
              name: "Outgoing"
            },    
            {
              guid: "Internal",
              name: "Internal"
            },         
            {
              guid: "All",
              name: "All"
            }            
          ]}
          styles={directionSelectorStyles}
        />
        {this.state.isTransactionSelector &&
        <><ControlLabel style={{ position: "absolute", top: "calc(5px - 0.1vw)", left: "calc(120px + 11vw)", fontSize: "calc(10px + 0.3vw)" }}>
          Type
        </ControlLabel>
        <Select
          multi={false}
          defaultValue={this.state.selectedTransactionType}
          onSelect={this.handleSelectTransactionType}
          options={options}
          styles={transactionTypeSelectorStyles}
        />
        </>}
        <div
          style={
            isDirectionSelected 
              ? { opacity: "1", transition: ".3s all" }
              : { opacity: "0", transition: ".3s all", pointerEvents: "none" }
          }
        >
          {isDirectionSelected && 
            this.state.transactionTypesComponents[this.state.selectedDirection][this.state.selectedTransactionType]}
        </div>
      </Content>
    );
  }
}

export default Transactions;