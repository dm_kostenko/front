import React from "react";
import Content from "ibankViews/Content";
import { Button } from "react-bootstrap";
import AccountsComponent from "ibankComponents/Accounts";
import Modal from "ibankComponents/UI/Modal";
import AccountCreator from "ibankComponents/Accounts/Creator";
import { getUserType } from "services/paymentBackendAPI/backendPlatform";

const isAdmin = getUserType() === "admin";

const Accounts = () => {
  return (
    <Content> 
      {isAdmin &&
      <Modal
        header="Create account"
        content={<AccountCreator />}
        state="accounts"
        button={
          <Button
            className="submit-button small"
            style={ { position: "absolute", marginLeft: "0", top:"20px", marginBottom: "-15px" } }
          >
            Create account
          </Button>
        }
      />}
      <AccountsComponent />
    </Content> 
  );
};

export default Accounts;