import React from "react";
import Content from "ibankViews/Content";
import AmlCheckComponent from "ibankComponents/AML/Check";

const AmlCheck = () => {
  return (
    <Content> 
      <AmlCheckComponent />
    </Content> 
  );
};

export default AmlCheck;