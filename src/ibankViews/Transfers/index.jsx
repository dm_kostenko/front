import React, { Component } from "react";
import Content from "ibankViews/Content";
import TransfersComponent from "ibankComponents/Transfers";
import { isIndividualWithEntities } from "services/paymentBackendAPI/backendPlatform";

const isIndividual = isIndividualWithEntities();

class Transfers extends Component {

  render() {
    return(
      <Content>
        <TransfersComponent directionData={{ entity: "true" }}/>
      </Content>
    );
  }
}

export default Transfers;