import React from "react";
import Content from "ibankViews/Content";
import SendComponent  from "ibankComponents/Send";


const Send = () => {
  return (
    <Content>
      <SendComponent/>
    </Content>
  );
};

export default Send;