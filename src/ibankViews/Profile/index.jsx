import React from "react";
import Content from "ibankViews/Content";
import ProfileComponent  from "ibankComponents/Profile";


const Profile = () => {
  return (
    <Content 
      style={{ display: "flex" }}
    >
      <ProfileComponent/>
    </Content>
  );
};

export default Profile;