import React, { Component } from "react";
import { ControlLabel, Row, Col } from "react-bootstrap";
import Select from "ibankComponents/UI/MultiSelect";
import Button from "ibankComponents/UI/Button";
import Content from "ibankViews/Content";
import AccountsBalancesComponent from "ibankComponents/Accounts/Balances";
import DateRangePicker from 'react-bootstrap-daterangepicker';
import moment from "moment";
import config from "config/";

const accounts = [ 
  ...new Set([
    config.iban.settlement.external,
    config.iban.customer.external
  ]) 
]

class AccountsBalances extends Component {

  state = {
    selectedAccount: accounts[0],
    selectedFilter: "All"
  }

  handleSelectAccount = (option) => {
    this.setState({ 
      selectedAccount: option.guid,
      selectedFilter: "All"
    })
  }

  handleSelectDate = (option) => {
    this.setState({ selectedFilter: option.guid })
  }

  handleSetDates = async (e,p) => {
    this.setState({
      selectedFilter: {
        fromDate: moment(p.startDate).format("YYYY-MM-DDT00:00:00"),
        toDate: moment(p.endDate).format("YYYY-MM-DDT23:59:59"),
      }
    });
  }

  render() {
    const selectorStyles = {
      control: (styles) => {
        return {
          ...styles,
          position: "absolute !important",
          width: "calc(80px + 8vw) !important",
          height: "calc(10px + 2vw) !important",   
          background: "transparent",
          color: "#545761 !important",
          border: "2px solid #545761 !important",
          boxShadow: "none !important",
          fontSize: "calc(10px + 0.3vw)",
          marginTop: "calc(0.1vw - 10px) !important"
        };
      },
      option: (styles, state) => {
        return {
          ...styles,
          background: state.isSelected ? "grey !important" : "rgba(0,0,0,0.6)",
          color: state.isSelected ? "white !important" : "lightgrey !important",
          fontSize: "calc(10px + 0.3vw)"
        };
      },
      menu: (styles) => {
        return {
          ...styles,
          borderTopLeftRadius: "0",
          borderTopRightRadius: "0",
          marginTop: "40px",
          position: "absolute !important",
          width: "calc(80px + 8vw) !important",
          background: "rgba(0,0,0,0.6)",
          color: "lightgrey !important"
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          color: "#545761 !important",
          fontSize: "calc(10px + 0.3vw)"
        };
      }
    };

    const filterSelectorStyles = {
      control: (styles) => {
        return {
          ...styles,
          position: "absolute !important",
          width: "calc(80px + 8vw) !important",
          height: "calc(10px + 2vw) !important",          
          marginLeft: "calc(100px + 10vw)",
          // opacity: isDirectionSelected ? "1" : "0",
          // disabled: isDirectionSelected ? "false" : "true",
          // pointerEvents: isDirectionSelected ? "" : "none",
          transition: ".3s all",
          background: "transparent",
          border: "2px solid #545761 !important",
          boxShadow: "none !important",
          marginTop: "calc(0.1vw - 10px) !important"
        };
      },
      option: (styles, state) => {
        return {
          ...styles,
          background: state.isSelected ? "grey !important" : "rgba(0,0,0,0.6)",
          color: state.isSelected ? "white !important" : "lightgrey !important",
          fontSize: "calc(10px + 0.3vw)"
        };
      },
      menu: (styles) => {
        return {
          ...styles,
          borderTopLeftRadius: "0",
          borderTopRightRadius: "0",
          marginTop: "40px",
          marginLeft: "calc(100px + 10vw)",
          position: "absolute !important",
          width: "calc(80px + 8vw) !important",          
          background: "rgba(0,0,0,0.6)",
          color: "lightgrey !important"
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          color: "#545761 !important",
          fontSize: "calc(10px + 0.3vw)"
        };
      }
    };

    return (
      <Content>
        <ControlLabel style={{ position: "absolute", top: "calc(5px - 0.1vw)", left: "calc(30px + 4vw)", fontSize: "calc(10px + 0.3vw)" }}>
          Accounts
        </ControlLabel>
        <Select
          multi={false}
          name={"selector"}
          placeholder={"Select account"}
          onSelect={this.handleSelectAccount}
          defaultValue={this.state.selectedAccount}
          options={accounts.map(account => ({
            guid: account,
            name: account
          }))}
          styles={selectorStyles}
        />    
        {/* <ControlLabel style={{ position: "absolute", top: "calc(5px - 0.1vw)", left: "calc(140px + 14vw)", fontSize: "calc(10px + 0.3vw)" }}>
          Period
        </ControlLabel> */}
        {/* <Select
          multi={false}
          name={"selector"}
          placeholder={"Select period"}
          onSelect={this.handleSelectDate}
          defaultValue={this.state.selectedFilter}
          options={[
            {
              guid: "Last day",
              name: "Last day"
            },
            {
              guid: "Last week",
              name: "Last week"
            },
            {
              guid: "Last month",
              name: "Last month"
            },    
            {
              guid: "Last year",
              name: "Last year"
            },    
            {
              guid: "All",
              name: "All"
            }
          ]}
          styles={filterSelectorStyles}
        /> */}
        {/* <Row>
          <Col 
            md={2}
            style={{
              marginLeft: "calc(200px + 16vw)",
              marginTop: "calc(0.3vw - 10px)"
            }}
          >
            <DateRangePicker 
              className="btn" 
              startDate={moment(this.state.startDate).format("MM/DD/YYYY")} 
              endDate={moment(this.state.endDate).format("MM/DD/YYYY")}
              onApply={this.handleSetDates}
              showDropdowns
              // containerClass="daterangepicker"
            >
              <Button
                // id="dateButton"
                className="btn"
                style={{
                  // position: "absolute",
                  // marginLeft: "calc(200px + 20vw)",
                //   width: "calc(80px + 8vw) !important",
                //   height: "calc(10px + 2vw) !important"
                }}
              >
                Open calendar
              </Button>
            </DateRangePicker>  
          </Col>
        </Row> */}
        
        <AccountsBalancesComponent 
          accountNumber={this.state.selectedAccount} 
          // date={this.state.selectedFilter}
        />
      </Content> 
    );
  }
};

export default AccountsBalances;