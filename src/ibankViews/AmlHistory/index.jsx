import React from "react";
import Content from "ibankViews/Content";
import AmlHistoryComponent from "ibankComponents/AML/History";

const AmlHistory = () => {
  return (
    <Content> 
      <AmlHistoryComponent />
    </Content> 
  );
};

export default AmlHistory;