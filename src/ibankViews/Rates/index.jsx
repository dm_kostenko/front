import React from "react";
import Content from "ibankViews/Content";
import RatesComponent from "ibankComponents/Rates";
import RateCreator from "ibankComponents/Rates/Creator";
import Modal from "ibankComponents/UI/Modal";
import { Button } from "react-bootstrap";

const Rates = () => {
  return (
    <Content> 
      <Modal
        header="Create rate"
        content={<RateCreator />}
        dialogClassName="modal-wide"
        state="rates-create"
        button={
          <Button
            className="submit-button small"
            style={ { position: "absolute", marginLeft: "0", top:"20px", marginBottom: "-15px" } }
          >
            Create rate
          </Button>
        }
      />
      <RatesComponent />
    </Content> 
  );
};

export default Rates;