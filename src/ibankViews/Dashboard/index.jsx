import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import Content from "ibankViews/Content";
import TransactionsChartPeriodic from "ibankComponents/Transactions/Chart/Line/Periodic";
import TransactionsDashboardTable from "ibankComponents/Transactions/DashboardTable";
import AccountsDashboardTable from "ibankComponents/Accounts/DashboardTable";
import { connect } from "react-redux";
import { showModal } from "ibankActions/modal";
import { getQR } from "ibankActions/auth";
import CurrencyCourses from "ibankComponents/Dashboard/CurrencyCourses";

class Dashboard extends Component {

  state = {
    width: 0
  }

  componentDidMount = () => {    
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  }

  updateDimensions = () => {
    this.setState({ width: window.innerWidth });
  }

  render() {
    return this.state.width > 1000
      ? (
        <Content>
          <Row className="row1">
            <AccountsDashboardTable/>
            <CurrencyCourses/>
            <TransactionsDashboardTable type="transfers"/>
          </Row>
          <Row>
            <TransactionsChartPeriodic />
          </Row>
        </Content> 
      )
      : (
        <Content>   
          <Row>
            <CurrencyCourses/>
          </Row>       
          <Row>
            <TransactionsDashboardTable type="transfers"/>
          </Row>          
          <Row>
            <AccountsDashboardTable/>
          </Row> 
          <Row>
            <TransactionsChartPeriodic />
          </Row>
        </Content>
      );
  }
}

export default connect(null, {
  showModal,
  getQR
})(Dashboard);