import React from "react";
import Content from "ibankViews/Content";
import { Button } from "react-bootstrap";
import UsersComponent  from "ibankComponents/Users";
import UserCreator from "ibankComponents/Users/Creator";
import Modal from "ibankComponents/UI/Modal";


const Users = () => {
  return (
    <Content> 
      <Modal
        header="Create user"
        content={<UserCreator />}
        dialogClassName="modal-wide"
        state="users"
        button={
          <Button
            className="submit-button small"
            style={ { position: "absolute", marginLeft: "0", top:"20px", marginBottom: "-15px" } }
          >
            Create user
          </Button>
        }
      />
      <UsersComponent/>
    </Content>
  );
};

export default Users;