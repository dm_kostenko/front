import React, { Component } from "react";
import {
  Button,
  FormControl,
  Col,
  FormGroup,
  Row
} from "react-bootstrap";
import { connect } from "react-redux";

import { parseResponse } from "helpers/parseResponse";
import { getUserProfile, changePassword } from "ibankActions/users";


class ExpiresPassword extends Component {
  state = {
    oldPassword: "",
    newPassword: "",
    confirmPassword: "",

    showOldPassword: false,
    showNewPassword: false,
    showConfirmPassword: false,

    userGuid: "",

    isPasswordConfirmed: false,
    isPasswordChanged: false,

    errorPassword: ""
  };

  async componentDidMount() {
    await this.props.getUserProfile();
    this.setState({ userGuid: this.props.profile.guid });
  }

  isPasswordConfirmed = () => {
    if (this.state.newPassword === this.state.confirmPassword && this.state.newPassword && this.state.confirmPassword) {
      this.setState({
        isPasswordConfirmed: true
      });
      return true;
    } else {
      this.setState({
        isPasswordConfirmed: false
      });
      return false;
    }
  };

  handleChange = e => {
    const property = e.target.name;
    const value = e.target.value;
    this.setState({
      [property]: value
    });
  };

  handleBack = () => {
    this.props.history.push("/");
    window.location.reload();
  };

  handleShowOldPassword = () => {
    this.setState({
      showOldPassword: !this.state.showOldPassword
    });
  }

  handleShowNewPassword = () => {
    this.setState({
      showNewPassword: !this.state.showNewPassword
    });
  }

  handleShowConfirmPassword = () => {
    this.setState({
      showConfirmPassword: !this.state.showConfirmPassword
    });
  }

  handleSubmit = async e => {
    e.preventDefault();
    if (!this.isPasswordConfirmed()) {
      this.setState({ errorPassword: "Passwords don't match. Try again" });
    } else {
      this.setState({ errorPassword: "" });
      try {
        const { newPassword, userGuid, oldPassword } = this.state;
        await this.props.changePassword({ password: newPassword, guid: userGuid, old_password: oldPassword });
        this.setState({ isPasswordChanged: true });
      } catch (error) {
        const parsedError = parseResponse(error);
        this.setState({
          errorPassword: parsedError.message
        });
      }
    }
  };

  render() {
    const {
      oldPassword,
      newPassword,
      confirmPassword,
      isPasswordChanged,
      errorPassword
    } = this.state;

    return (
      <div className="confirm-email">
        <div className="confirm-form">
          <div className="card" style={{ padding: "20px" , minWidth: "300px" }} >
            {isPasswordChanged ?
              <>
                <p style={{ color: "grey", textAlign: "center" }}>
                  New password set successfully
                </p>
                <div style={{ textAlign: "center" }}>
                  <Button
                    className="btn btn-fill btn-primary"
                    onClick={this.handleBack}
                    style={{ width: "60%" }}
                  >
                    Continue
                  </Button>
                </div>
              </>
              :
              <>
                <p className="header-confirm" style={{ marginBottom: "80px", paddingBottom: "0px" }}>
                  <i className="fas fa-lock" size="3px"></i>
                  <p className="header-confirm header-confirm-warning" style={{ paddingTop: "10px" }}>
                    Your password will expire soon
                  </p>
                </p>

                <form>
       
                  <Row className="rowReg">
                    <Col md={11} sm={10} xs={10}>
                      <FormGroup controlId="oldPassword">
                        <FormControl
                          placeholder="Current password"
                          name="oldPassword"
                          type={this.state.showOldPassword ? "text" : "password"}
                          value={oldPassword}
                          onChange={e => this.handleChange(e)}
                        />
                        <Button className="eye" onClick={this.handleShowOldPassword}>
                          {this.state.showOldPassword
                            ? <span className="fa fa-eye-slash" />
                            : <span className="fa fa-eye" />
                          }
                        </Button>
                      </FormGroup>
                    </Col>
                    {/* <Col md={1} sm={1} xs={1}>
                      <Button className="eye" onClick={this.handleShowOldPassword}>
                        {this.state.showOldPassword
                          ? <span className="fa fa-eye-slash" />
                          : <span className="fa fa-eye" />
                        }
                      </Button>
                    </Col> */}
                  </Row>

                  <Row className="rowReg">
                    <Col md={11} sm={10} xs={10}>
                      <FormGroup controlId="newPassword">
                        <FormControl
                          placeholder="New password"
                          name="newPassword"
                          type={this.state.showNewPassword ? "text" : "password"}
                          value={newPassword}
                          onChange={e => this.handleChange(e)}
                        />
                        <Button className="eye" onClick={this.handleShowNewPassword}>
                          {this.state.showNewPassword
                            ? <span className="fa fa-eye-slash" />
                            : <span className="fa fa-eye" />
                          }
                        </Button>
                      </FormGroup>
                      {errorPassword !== "" && (
                        <p className="validate-error">{errorPassword}</p>
                      )}
                    </Col>
                    {/* <Col md={1} sm={1} xs={1}>
                      <Button className="eye" onClick={this.handleShowNewPassword}>
                        {this.state.showNewPassword
                          ? <span className="fa fa-eye-slash" />
                          : <span className="fa fa-eye" />
                        }
                      </Button>
                    </Col> */}
                  </Row>

                  <Row className="rowReg">
                    <Col md={11} sm={10} xs={10}>
                      <FormGroup controlId="confirmPassword">
                        <FormControl
                          placeholder="Confirm password"
                          name="confirmPassword"
                          type={this.state.showConfirmPassword ? "text" : "password"}
                          value={confirmPassword}
                          onChange={e => this.handleChange(e)}
                        />
                        <Button className="eye" onClick={this.handleShowConfirmPassword}>
                          {this.state.showConfirmPassword
                            ? <span className="fa fa-eye-slash" />
                            : <span className="fa fa-eye" />
                          }
                        </Button>
                      </FormGroup>
                    </Col>
                    {/* <Col md={1} sm={1} xs={1}>
                      <Button className="eye" onClick={this.handleShowConfirmPassword}>
                        {this.state.showConfirmPassword
                          ? <span className="fa fa-eye-slash" />
                          : <span className="fa fa-eye" />
                        }
                      </Button>
                    </Col> */}
                  </Row>

                  <div className="tooltip-text">
                    <span>
                      Password must be at least 8 characters, including a
                      number, a special, and an uppercase letter.
                    </span>
                  </div>
                  <Row>
                    <Col md={6}>
                      <div style={{ textAlign: "center" }}>
                        <Button
                          className="btn btn-fill"
                          onClick={this.handleBack}
                          style={{ width: "60%" }}
                        >
                          Cancel
                        </Button>
                      </div>
                    </Col>
                    <Col md={6}>
                      <div style={{ textAlign: "center" }}>
                        <Button
                          className="btn btn-fill btn-primary"
                          type="submit"
                          onClick={this.handleSubmit}
                          style={{ width: "60%" }}
                        >
                          Continue
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </form>
              </>}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    profile: state.users.profile
  };
};

export default connect(mapStateToProps, {
  getUserProfile,
  changePassword
})(ExpiresPassword);
