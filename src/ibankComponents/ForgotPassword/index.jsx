import React, { Component } from 'react';
import { Row, Col, Button, FormControl, FormGroup } from "react-bootstrap";
import MultiSelect from "ibankComponents/UI/MultiSelect";
import swal from "sweetalert";
import { connect } from "react-redux";

import { parseResponse } from "helpers/parseResponse";
import { getEmailMessageAction } from "ibankActions/auth";
import ReCAPTCHA from "react-google-recaptcha";
import Joi from "@hapi/joi";
const recaptchaRef = React.createRef();

class ForgotPassword extends Component {
  state = {
    fieldName: "email",
    fieldValue: "",
    verified: null,
    validation: false,
    errors: {},
  }

  schema = {
    email: Joi.string()
      .required()
      .email()
      .label("Email"),
    username: Joi.string()
      .required()
      .label("Username"),
    phone: Joi.number()
      .required()
      .min(5)
      .label("Phone"),
  }

  validate = () => {
    const options = { abortEarly: false };
    let name = this.state.fieldName;
    let value = this.state.fieldValue;
    let schema = this.schema[name];
    let { error } = Joi.validate({ [name]: value }, schema, options);

    if (!error) return null;
    const errors = {};
    for (let item of error.details) {
      if (item.path[0]) {
        errors[item.path[0]] = item.message;
      }
    }
    return errors;
  };

  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  formValidation = (name) => {
    if (this.state.errors[name]) return "error";
    else return "success";
  };

  handleChange = (e) => {
    const value = e.target.value;
    const errors = { ...this.state.errors };
    const errorMessage = this.validateProperty({ name: this.state.fieldName, value });
    if (errorMessage) errors[this.state.fieldName] = errorMessage;
    else delete errors[this.state.fieldName];

    this.setState({
      fieldValue: value,
      errors,
      validation: errorMessage ? false : true
    });
  };

  handleBack = () => {
    this.props.history.push('/');
    window.location.reload();
  }

  onActionSelect = (option) => {
    const errors = { ...this.state.errors };
    const errorMessage = this.validateProperty({ name: option.name, value: this.state.fieldValue });
    if (errorMessage) errors[option.name] = errorMessage;
    else delete errors[option.name];

    for (var key in errors) {
      if (key !== option.name) {
        delete errors[key];
      }
    }

    this.setState({
      fieldName: option.name,
      errors,
      validation: errorMessage ? false : true
    });
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    if (this.state.verified) {
      try {
        const { fieldName, fieldValue } = this.state;
        await this.props.getEmailMessageAction({ [fieldName]: fieldValue });
        swal({
          title: `${this.props.status}`,
          icon: "success"
        });
      } catch (error) {
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
        recaptchaRef.current.reset()
      }
    }
  }

  onChange = () => {
    const recaptchaValue = recaptchaRef.current.getValue();
    if (recaptchaValue) {
      this.setState({ verified: true })
    }
    else this.setState({ verified: false })
  }


  render() {
    const actions = [
      {
        guid: 0,
        name: "email"
      },
      {
        guid: 1,
        name: "phone"
      },
      {
        guid: 2,
        name: "username"
      }
    ];
    const { errors } = this.state;
    return (
      <div className="confirm-email">
        <div className="confirm-form">
          <div className="card" style={{ padding: "20px", minWidth: "360px" }}>

            <p className="header-confirm">
              <strong>Forgot password</strong>
            </p>

            {!this.props.status &&
              <>
                <p style={{ color: "grey", textAlign: "center" }}>
                  Enter your user account's verified email, phone or username and we will send you a password reset link.
                </p>
                <form>
                  <Row>
                    <Col className="col-md-4">
                      <FormGroup>
                        <MultiSelect
                          name="Name"
                          options={actions}
                          multi={false}
                          onSelect={this.onActionSelect}
                          defaultValue={this.state.fieldName}
                        />
                      </FormGroup>
                    </Col>
                    <Col className="col-md-8">
                      <FormGroup>
                        <FormControl
                          placeholder="Enter data"
                          name="recoveryData"
                          value={this.state.fieldValue}
                          onChange={(e) => this.handleChange(e)}
                        />
                        {errors.phone && <span className="validate-error">{errors.phone}</span>}
                        {errors.username && <span className="validate-error">{errors.username}</span>}
                        {errors.email && <span className="validate-error">{errors.email}</span>}
                      </FormGroup>
                    </Col>
                  </Row>
                  <ReCAPTCHA
                    ref={recaptchaRef}
                    sitekey="6LcxtOQZAAAAAGqoxZhxI1YzlNvhUXzqg-U7aKHz"
                    onChange={this.onChange}
                  />
                  <div style={{ textAlign: "center", paddingTop: "15px" }}>
                    <Button
                      className={
                        this.state.verified
                          && this.state.validation ?
                          "btn btn-fill btn-primary" : "btn btn-fill"
                      }
                      type="submit"
                      onClick={this.handleSubmit}
                      style={{ width: "60%" }}
                    >
                      Continue
                    </Button>
                  </div>
                </form>
              </>}
            {this.props.status &&
              <>
                <p style={{ color: "grey", textAlign: "center" }}>
                  Check your email for a link to reset your password. If it doesn’t appear within a few minutes, check your spam folder.
                </p>

                <div style={{ textAlign: "center" }}>
                  <Button
                    className="btn btn-fill btn-primary"
                    onClick={this.handleBack}
                    style={{ width: "60%" }}
                  >
                    Return to sign in
                  </Button>
                </div>
              </>}
          </div>
        </div>
      </div>
    );

  }
}


const mapStateToProps = (state) => {
  return {
    status: state.auth.status
  };
};

export default connect(mapStateToProps, {
  getEmailMessageAction
})(ForgotPassword);