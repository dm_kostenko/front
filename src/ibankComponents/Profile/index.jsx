import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, ControlLabel, Button, Form } from "react-bootstrap";
import Card from "ibankComponents/UI/Card";
import InfoLabel from "ibankComponents/UI/InfoLabel";
import { getUserProfile } from "ibankActions/users";
import { getQR, disableTFA } from "ibankActions/auth";
import { showModal } from "ibankActions/modal";
import Modal from "ibankComponents/UI/Modal";
import PropTypes from "prop-types";
// import Select from "ibankComponents/UI/MultiSelect";
// import swal from "sweetalert";
// import { parseResponse } from "helpers/parseResponse";
// import { issueToken, setTokens } from "services/paymentBackendAPI/backendPlatform";
import ChangePassword from "ibankComponents/ChangePassword";

class Profile extends Component {

  state = {
    width: 0,

    IBAN: "123123123ASDS",
    authTypeOption: "",
    auth_type: ""
  }

  componentDidMount = async() => {    
    this.updateWindowStyles();
    window.addEventListener("resize", this.updateWindowStyles);
    await this.props.getUserProfile();
    const authTypeOption = this.props.profile.auth_type === "login-password" ? "Disabled" : "Google authenticator";
    this.setState({ 
      authTypeOption,
      auth_type: this.props.profile.auth_type
    });
  }

  updateWindowStyles = () => {
    this.setState({
      width: window.outerWidth
    });
  }


  // handleSelect = async(option) => {
  //   const { authTypeOption } = this.state;
  //   this.setState({ authTypeOption: option.name });
  //   if(authTypeOption !== option.name) {
  //     try {
  //       switch(option.name) {
  //       case "Google authenticator":
  //         await this.props.showModal(true, "profile");
  //         await this.props.getQR();
  //         break;
  //       case "Disabled":
  //         await this.props.disableTFA();
  //         const response = await issueToken();
  //         setTokens(response.data.accessToken, response.data.refreshToken);
  //         swal({
  //           title: "Two-factor authentication is disabled",
  //           icon: "success",
  //           button:false,
  //           timer: 2000
  //         });
  //         break;
  //       default: break;
  //       }
  //     }
  //     catch(error) {
  //       const parsedError = parseResponse(error);
  //       if((error.response && error.response.status !== 409) || !error.response)        
  //         swal({
  //           title: parsedError.error,
  //           text: parsedError.message,
  //           icon: "error",
  //         });
  //     }

  //   }
  // }

  render() {
    const style = this.state.width > 1000 ? { display: "flex" } : {};
    // const selectorStyles = {
    //   placeholder: (styles) => {
    //     return {
    //       ...styles,
    //       color: "black",
    //       fontSize: "calc(5px + 0.4vw)"
    //     };
    //   },
    //   control: (styles, state) => {
    //     return {
    //       ...styles,
    //       background: "rgba(255,255,255,0.4) !important",
    //       marginBottom: "30px",
    //       minWidth: "50px !important",
    //       marginLeft: "auto",
    //       marginRight: "auto",
    //       width: "100%",
    //       height: this.state.height > 920 ? "calc(0.6vw + 10px) !important" : "20px !important",
    //       transition: ".8s all",
    //       boxShadow: "none !important",
    //       "&:hover": {
    //         background: state.menuIsOpen ? "white !important" : "transparent !important",
    //         transition: ".8s all"
    //       }
    //     };
    //   },
    //   singleValue: (styles) => {
    //     return {
    //       ...styles,
    //       width: "100%",
    //       whiteSpace: "pre-line !important",
    //       marginLeft: "auto",
    //       marginRight: "auto"
    //     };
    //   }
    // };
    return (
      <div style={{ width: "100%" }}>
        <Row>
          <Col style={style}>
            <Card
              style={{
                color: "black",
                // background: "#80dbff",
                // height: "auto",
                marginTop: "20px"
              }}
              header={"Bank info"}
            >
              <InfoLabel
                label="..."
              />
            </Card>
            <Card
              style={{
                color: "black",
                // background: "#80dbff",
                marginTop: "20px"           
              }}
              header={"Personal info"}
            >          
              <InfoLabel
                label="Username:"
                value={this.props.profile.username}
              />
              <InfoLabel
                label="Email:"
                value={this.props.profile.email}
              />
              <InfoLabel
                label="Phone:"
                value={this.props.profile.phone}
              />
              {/* <Row style={{ marginTop: "20%" }}>
                <Col className="col" md={3}/>
                <Col className="col" md={6} style={{ marginTop: "200px !important" }}>
                  <ControlLabel 
                    className="col-label"
                    style={{ fontSize: "calc(5px + 0.5vw) !important" }}
                  >
                Two-factor authentication
                  </ControlLabel>
                  <Select
                    options={[
                      {
                        guid: "Disabled",
                        name: "Disabled"
                      },
                      {
                        guid: "Google authenticator",
                        name: "Google authenticator"
                      }                
                    ]}
                    selectedValue={this.state.authTypeOption}
                    onSelect={this.handleSelect}
                    styles={selectorStyles}
                  />
                </Col>     
                <Col className="col" md={3}/> 
              </Row> */}
              <Row>
                <Col className="col" md={4}/>
                <Col className="col" md={4}>
                  <Modal
                    header="Change password"
                    content={<ChangePassword guid={this.props.profile.guid}/>}
                    state="changePassword"
                    button={
                      <Button
                        className="submit-button change-password"
                      >
                    Change password
                      </Button>
                    }
                  />
                </Col>
                <Col className="col" md={4}/>
              </Row>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profile: state.users.profile,
    loading: state.users.loadingProfile,
    qrCode: state.auth.qrCode
  };
};

export default connect(mapStateToProps, {
  getUserProfile,
  getQR,
  disableTFA,
  showModal
})(Profile);

Profile.propTypes = {
  profile: PropTypes.object,
  loading: PropTypes.bool,
  getUserProfile: PropTypes.func
};