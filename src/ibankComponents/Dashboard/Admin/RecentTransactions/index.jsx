import React from "react";
import { connect } from "react-redux";
import { getTxs } from "ibankActions/transactions";
import PropTypes from "prop-types";
import moment from "moment";
import { getUserType } from "services/paymentBackendAPI/backendPlatform";
import { Link } from "react-router-dom";
import Table from "ibankComponents/UI/Table";
import { ControlLabel } from "react-bootstrap";
import Spinner from "ibankComponents/UI/Spinner";
import { getCETDate } from "helpers/target2Timing";

class RecentTransactions extends React.Component {
  state = {
    lastTransactions: []
  }

  isAdmin = getUserType() === "admin";

  dict = {
    "FFCCTRNS": "Credit transfer",
    "INTERNAL": "Internal",
    "PRTRN": "Payment return",
    "FFPCRQST": "Payment cancellation",
    "ROINVSTG": "Resolution of investigation"
  };

  async componentDidMount() {
    await this.props.getTxs({ type: "all" });
    let lastTransactions = [];
    let length = this.props.transactions.length > 10 ? 10 : this.props.transactions.length;
    for (let i = 0; i < length; i++) {
      lastTransactions[i] = this.props.transactions[i];
    }
    this.setState({ lastTransactions });
  }

  columns = [
    {
      path: "guid",
      label: "ID",
      content: tx => {
        return this.isAdmin
          ? <Link className="link" to={`transactions/${tx.type.toLowerCase()}/${tx.guid}`}>{tx.guid}</Link>
          : tx.guid;
      }
    },
    {
      path: "created_at",
      label: "Timestamp",
      content: (tx) => (moment(getCETDate(new Date(tx.created_at))).format("DD.MM.YYYY HH:mm:ss"))
    },
    {
      path: "type",
      label: "Type",
      content: tx => (
        <div style={{ textTransform: "uppercase" }}>{this.dict[tx.type]}</div>
      )
    },
    {
      path: "status",
      label: "Status"
    },
    {
      path: "value",
      label: "Amount",
      content: tx => (`${tx.value} ${tx.currency}`)
    },
    {
      path: "sender_iban",
      label: "Sender IBAN"
    },
    {
      path: "receiver_iban",
      label: "Receiver IBAN"
    }
  ];

  render() {
    return (
      <div
        className="card"
        style={{
          ...this.props.style,
          textAlign: "center"
        }}
      >
        <ControlLabel
          className="card-header"
        >
          recent transactions
        </ControlLabel>
        <br /><br />
        <ControlLabel
          className="card-body"
        >
          {this.props.loading ? <Spinner />
            : <Table
              columns={this.columns}
              data={this.state.lastTransactions}
              disableSearch
            />}
        </ControlLabel>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    transactions: state.transactions.ALL,
    loading: state.transactions.loadingALL
  };
};

export default connect(mapStateToProps, {
  getTxs
})(RecentTransactions);

RecentTransactions.propTypes = {
  getTxs: PropTypes.func,
  transactions: PropTypes.array,
  loading: PropTypes.bool,
  style: PropTypes.object
};