import React from "react";
import { ControlLabel } from "react-bootstrap";
import { getAllAccounts } from "ibankActions/accounts";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Spinner from "ibankComponents/UI/Spinner";

class AccountsCount extends React.Component {
  state = {}

  async componentDidMount() {
    await this.props.getAllAccounts();
  }

  render() {
    return (
      <div
        className="card"
        style={{
          ...this.props.style,
          textAlign: "center",
          flex: "1"
        }}
      >
        <ControlLabel
          className="card-header"
        >
          Accounts
        </ControlLabel>
        <br /><br />
        <ControlLabel
          className="card-body"
        >
          {this.props.loading ? <Spinner /> :
            <div>
              <span className="dashboard-label-sum">
                {this.props.count}
              </span>
            </div>}
        </ControlLabel>
        <br /><br />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    count: state.accounts.count,
    loading: state.accounts.loading   
  };
};

export default connect(mapStateToProps, {
  getAllAccounts
})(AccountsCount);

AccountsCount.propTypes = {
  count: PropTypes.string,
  loading: PropTypes.bool,
  style: PropTypes.object,
  getAllAccounts: PropTypes.func
};

//count: PropTypes.string