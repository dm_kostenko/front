import React from "react";
import { ControlLabel } from "react-bootstrap";
import { getAllUsers } from "ibankActions/users";
import { connect } from "react-redux";
import Spinner from "ibankComponents/UI/Spinner";

class UsersCount extends React.Component {
  state = {}

  async componentDidMount() {
    await this.props.getAllUsers();
  }

  render() {
    return (
      <div
        className="card"
        style={{
          ...this.props.style,
          textAlign: "center",
          flex: "1"
        }}
      >
        <ControlLabel
          className="card-header"
        >
          Users
        </ControlLabel>
        <br /><br />
        <ControlLabel
          className="card-body"
        >
          {this.props.loading ? <Spinner /> :
            <div>
              <span className="dashboard-label-sum">
                {this.props.count}
              </span>
            </div>}
        </ControlLabel>
        <br /><br />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    count: state.users.count,
    loading: state.users.loading
  };
};

export default connect(mapStateToProps, {
  getAllUsers
})(UsersCount);