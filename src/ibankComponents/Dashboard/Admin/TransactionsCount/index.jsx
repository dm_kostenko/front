import { getTransactionsHistory } from "ibankActions/reports";
import React from "react";
import { ControlLabel } from "react-bootstrap";
import { connect } from "react-redux";
import Spinner from "ibankComponents/UI/Spinner";
import PropTypes from "prop-types";

class TransactionsCount extends React.Component {
  state = {}

  render() {
    return (
      <div
        className="card"
        style={{
          ...this.props.style,
          textAlign: "center",
          flex: "1"
        }}
      >
        <ControlLabel
          className="card-header"
        >
          Transactions
        </ControlLabel>
        <br /><br />
        <ControlLabel
          className="card-body"
        >
          {this.props.loading ? <Spinner /> :
            <div>
              <span className="dashboard-label-sum">
                {this.props.count}
              </span>
            </div>}
        </ControlLabel>
        <br /><br />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    count: state.reports.transactionHistoryCount,
    loading: state.reports.transactionHistoryLoading
  };
};

export default connect(mapStateToProps, {
  getTransactionsHistory
})(TransactionsCount);

TransactionsCount.propTypes = {
  style: PropTypes.object,
  loading: PropTypes.bool,
  getTransactionsHistory: PropTypes.func,
  count: PropTypes.number
};