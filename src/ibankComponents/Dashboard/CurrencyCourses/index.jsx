import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getUserType } from "services/paymentBackendAPI/backendPlatform";
import { getAllCurrenciesRates } from "ibankActions/currencies";
import { ControlLabel, Form, FormControl, Table } from "react-bootstrap";
import Select from "ibankComponents/UI/MultiSelect";

class CurrencyCourses extends React.Component {
  state = {
    course: 0
  }

  async componentDidMount() {
    await this.props.getAllCurrenciesRates();
  }

  currencies = {
    USD: "US dollar",
    RUB: "Russian Ruble",
    JPY: "Japanese yen",
    BGN: "Bulgarian lev",
    CZK: "Czech koruna",
    DKK: "Danish krone",
    GBP: "Pound sterling",
    HUF: "Hungarian forint",
    PLN: "Polish zloty",
    RON: "Romanian leu",
    SEK: "Swedish krona",
    CHF: "Swiss franc",
    ISK: "Icelandic krona",
    NOK: "Norwegian krone",
    HRK: "Croatian kuna",
    TRY: "Turkish lira",
    AUD: "Australian dollar",
    BRL: "Brazilian real",
    CAD: "Canadian dollar",
    CNY: "Chinese yuan renminbi",
    HKD: "Hong Kong dollar",
    IDR: "Indonesian rupiah",
    ILS: "Israeli shekel",
    INR: "Indian rupee",
    KRW: "South Korean won",
    MXN: "Mexican peso",
    MYR: "Malaysian ringgit",
    NZD: "New Zealand dollar",
    PHP: "Philippine peso",
    SGD: "Singapore dollar",
    THB: "Thai baht",
    ZAR: "South African rand"
  };

  render() {
    return (
      <div /* className="card" */ style={{ maxHeight: "60vh", flex: "1", overflowX: "hidden", overflowY: "scroll" }}> 
        <Table className="table table-bordered">
          <tbody>
            <tr>
              <td colSpan="3" style={{ textAlign: "center" }}>
              <ControlLabel
                className="card-header"
              >
                CURRENCIES
              </ControlLabel>
              </td>
            </tr>
            <tr style={{ backgroundColor: "#22ac71", color: "white" }}>
              <th>Code</th>
              <th>Name</th>
              <th>Rate</th>
            </tr>
            {this.props.courses.map((course, index) => {
              if (course.currency_code !== "EUR")
                return <tr key={index}>
                  <td>{course.currency_code}</td>
                  <td>{this.currencies[course.currency_code]}</td>
                  <td>{course.rate}</td>
                </tr>;
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    courses: state.currencies.courses
  };
};

export default connect(mapStateToProps, {
  getAllCurrenciesRates
})(CurrencyCourses);

CurrencyCourses.propTypes = {
  getAllCurrenciesRates: PropTypes.func,
  courses: PropTypes.array,
  style: PropTypes.object
};