import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Col, Form, Row } from "react-bootstrap";
import ReactLoading from "react-loading";
import Input from "ibankComponents/UI/Input";
import Card from "ibankComponents/UI/Card";
import validate from "helpers/validation";
import Joi from "@hapi/joi";
import { showModal } from "ibankActions/modal";
import { parseResponse } from "helpers/parseResponse";
import swal from "sweetalert";
import { changePassword } from "ibankActions/users";


class ChangePassword extends Component {

  state = {
    oldPassword: "",
    password: "",
    password2: ""
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  schema = {
    old_password: Joi.string().min(6).max(20).required().label("Old password"),
    password: Joi.string().min(6).max(20).required().label("New password")
  };


  handleSubmit = async(e) => {
    e.preventDefault();
    try {
      const { oldPassword: old_password, password, password2 } = this.state;
      const request = {
        old_password,
        password,
        guid: this.props.guid
      };
      validate(request, this.schema);
      if(password === password2) {        
        await this.props.changePassword(request);
        swal({
          title: "Password is changed",
          icon: "success",
          button:false,
          timer: 2000
        });
        await this.props.showModal(false, "changePassword");
      }
      else
        swal({
          title: "Validation error",
          text: "New password and confirmation of new password does not match",
          icon: "error"
        });
    }
    catch(error) {
      const parsedError = parseResponse(error);
      if((error.response && error.response.status !== 409) || !error.response)        
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
    }

  }

  render() {
    return (
      <Form autoComplete="off">
        <Row>
          <Col style={{ display: "flex" }}>
            <Card
              header="Info"
              style={{
                // background: "#80dbff"
              }}
            >
              <Row>
                <Col md={2}/>
                <Col className="col" md={8}>
                  <Input
                    label="Old password"
                    type="password"
                    name="oldPassword"
                    value={this.state.oldPassword}
                    onChange={this.handleChange}
                  />
                </Col>
                <Col md={2}/>
              </Row> 
              <Row>
                <Col md={2}/>
                <Col className="col" md={8}>
                  <Input
                    label="New password"
                    type="password"
                    name="password"
                    value={this.state.password}
                    onChange={this.handleChange}
                  />
                </Col>
                <Col md={2}/>
              </Row>  
              <Row>
                <Col md={2}/>
                <Col className="col" md={8}>
                  <Input
                    label="Confirm new password"
                    type="password"
                    name="password2"
                    value={this.state.password2}
                    onChange={this.handleChange}
                  />
                </Col>
                <Col md={2}/>
              </Row>             
            </Card>            
          </Col>          
          <div align="center">
            {this.state.isLoading 
              ? <ReactLoading type='cylon' color='grey' /> 
              : <Button 
                className="submit-button"
                style={{
                  marginTop: "40px"
                }}
                onClick={this.handleSubmit}
              >
                Change
              </Button>}
          </div>
        </Row>
      </Form>
    )
  }
}

export default connect(null, {
  changePassword,
  showModal
})(ChangePassword);