import React, { Component } from "react";
import Content from "ibankViews/Content";
import { getUserDetail, createNewUser as editUser } from "ibankActions/users";
import { connect } from "react-redux";
import Card from "ibankComponents/UI/Card";
import DetailInfoLabel from "ibankComponents/UI/DetailInfoLabel";
import PropTypes from "prop-types";
import Spinner from "ibankComponents/UI/Spinner";
import tagsDictionary from "helpers/fieldsDictionary";
import Graph from "ibankComponents/UI/Graph";
import { Row, Col } from "react-bootstrap";

class UserDetail extends Component {

  state = {
    auth_type: ""
  }

  async componentDidMount() {
    await this.props.getUserDetail(this.props.match.params.id);
    const { auth_type } = this.props.data;
    this.setState({ auth_type });
  }


  render() {
    const unusedKeys = [ "privileges", "loginTree" ];

    if (this.props.loading)
      return <Content>
        <Spinner />
      </Content>;
    return <Content>
      <Row>
        <Col>
          <Card
            style={{
              color: "black",
              minWidth: "200px",
              width: "70%",
              marginLeft: "auto",
              height: "auto",
              marginTop: "20px",
              marginRight: "auto",
              textAlign: "center"
            }}
            detail={true}
            header={this.props.data.username}
          >
            {Object.keys(this.props.data).map(key => {
              const label = tagsDictionary[key.toUpperCase()]
                ? tagsDictionary[key.toUpperCase()].beautify
                : key;
              let value;
              value = tagsDictionary[key.toUpperCase()] && tagsDictionary[key.toUpperCase()].modify
                ? tagsDictionary[key.toUpperCase()].modify(this.props.data[key])
                : this.props.data[key];
              return !unusedKeys.includes(key) &&
                <DetailInfoLabel
                  key={key}
                  label={`${label}:`}
                  value={value === "Invalid date" ? "-" : value || "-"}
                />;
            })}
          </Card>
        </Col>
        <Col>
          {this.props.data.loginTree &&
        <Card
          style={{
            display: "block",
            color: "black",
            // background: "#80dbff",
            minWidth: "400px",
            width: "30%",
            marginLeft: "auto",
            height: "auto",
            marginTop: "20px",
            marginRight: "auto",
            textAlign: "center"
          }}
          header="User hierarchy"
        >
          <Graph data={this.props.data.loginTree} />
        </Card>
          }
        </Col>
      </Row>
    </Content>;
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.users.user,
    loading: state.users.loadingDetail
  };
};

export default connect(mapStateToProps, {
  getUserDetail,
  editUser
})(UserDetail);

UserDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  loading: PropTypes.bool,
  getUserDetail: PropTypes.func,
  editUser: PropTypes.func,
  data: PropTypes.object
};