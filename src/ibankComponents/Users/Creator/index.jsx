import React, { Component } from "react";
import { Button, Col, ControlLabel, Form, FormControl,  Row } from "react-bootstrap";
import Card from "ibankComponents/UI/Card";
import Input from "ibankComponents/UI/Input";
import Checkbox from "ibankComponents/UI/Checkbox";
import { createNewUser, getAllUsers } from "ibankActions/users";
import { showModal } from "ibankActions/modal";
import { getAllPrivileges } from "ibankActions/privileges";
import { generate } from "generate-password";
import { connect } from "react-redux";
import { parseResponse } from "helpers/parseResponse";
import Select from "ibankComponents/UI/MultiSelect";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";
import validate from "helpers/validation";
import Joi from "@hapi/joi";
import Loading from "ibankComponents/UI/Spinner";

class UserCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 0,

      username: "",
      password: "",
      confirmPassword: "",
      showPassword: false,
      isPasswordConfirmed: false,
      email: "",
      personalEmail: "",
      enabled: true,
      phone: "",
      personalPhone: "",
      privileges: {},
      privilegesNames: [],
      firstName: "",
      lastName: "",
      companyName: "",
      note: "",
      type: "",
      founders: [],
      isLocked: false,
      isLoading: false
    };
  }

  schema = {
    username: Joi.string().alphanum().min(3).max(20).required().label("Username"),
    email: Joi.string().email().required().label("Email"),
    type: Joi.string().required().label("Type"),
    password: Joi.string().min(6).max(20).required().label("Password"),
    phone: Joi.string().trim().regex(/^[0-9]{7,11}$/).required().label("Phone"),
    founders: Joi.array().items(Joi.string().guid()).label("Founders"),
    privileges: Joi.array().items(Joi.string().guid()).label("Privileges")
  };


  componentDidMount = async() => {
    this.updateWindowStyles();
    window.addEventListener("resize", this.updateWindowStyles);
    await this.props.getAllUsers();
  }

  updateWindowStyles = () => {
    this.setState({
      width: window.outerWidth
    });
  }

  onChangeHandler = (e) => {
    const property = e.target.name;
    const value = e.target.value;
    this.setState({
      [property]: value
    });
  };

  handleCheckPrivilege = (e) => {
    let { privileges } = this.state;
    privileges[e.target.dataset.attr].status = !privileges[e.target.dataset.attr].status;
    this.setState({ privileges });
  }

  handleExpiresDate = (date) => {
    this.setState({
      expiresAt: date,
    });
  };

  isPasswordConfirmed = () => {
    if (this.state.password === this.state.confirmPassword) {
      this.setState({
        isPasswordConfirmed: true
      });

    }
    else {
      this.setState({
        isPasswordConfirmed: false
      });
    }
  };

  handleEnabledCheckbox = () => {
    this.setState({
      enabled: !(this.state.enabled)
    });
  };

  handleShowPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword
    });
  }

  handleChangeType = async(option) => {
    this.setState({ type: option.name });
    await this.props.getAllPrivileges({ type: option.name });
    let privileges = {};
    this.props.privileges.forEach(privilege => {
      privileges[privilege.name] = {};
      privileges[privilege.name].status = false;
      privileges[privilege.name].guid = privilege.guid;
    });
    this.setState({ privileges });
  }

  handleChangeFounder = (options) => {
    const founders = options.map(option => {
      return option.guid;
    });
    this.setState({ founders });
  }

  getFoundersOptions = () => {
    return this.props.users.filter(user => user.type !== "admin").map(user => {
      return {
        guid: user.guid,
        name: `${user.username} (${user.type})`
      };
    });
  }

  handleGenerate = () => {
    const password = generate({
      length: 10,
      numbers: true
    });
    this.setState({
      password,
      confirmPassword: password
    });
  };

  formValidation = (data) => {
    return data !== "";
  };

  generalValidation = () => {
    return this.formValidation(this.state.username) &&
      this.formValidation(this.state.password) &&
      this.formValidation(this.state.email) &&
      this.formValidation(this.state.phone) &&
      this.formValidation(this.state.type)
  };

  getControlForm = (label, name) => {
    return (
      <Form inline>
        <ControlLabel>{label}:</ControlLabel>
        <FormControl
          name={name}
          type="text"
          value={this.state[name]}
          onChange={(e) => this.onChangeHandler(e)}
        />
      </Form>);
  };

  personalDataValidation = () => {
    if
    (
      this.state.firstName === "" &&
      this.state.lastName === "" &&
      this.state.personalEmail === "" &&
      this.state.personalPhone === "" &&
      this.state.companyName === "" &&
      this.state.note === ""
    ) return "empty";
    else if
    (
      this.state.firstName !== "" &&
      this.state.lastName !== "" &&
      this.state.personalEmail !== "" &&
      this.state.personalPhone !== "" &&
      this.state.companyName !== "" &&
      this.state.note !== ""
    )
      return "ok";
    else return "error";
  }

  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    if (!this.generalValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    }
    else if (this.state.type === "entity" && this.state.founders.length === 0) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, choose founders",
        icon: "warning"
      });
    }
    else {
      try {        
        let { username, password, email, phone, privileges, type, founders } = this.state;
        privileges = Object.keys(privileges).filter(privilege => privileges[privilege].status).map(privilege => { return privileges[privilege].guid; });
        let request = {
          username,
          password,
          email,
          phone,
          privileges,
          type
        };  
        if(founders.length > 0)
          request = {
            ...request,
            founders
          };
        validate(request, this.schema);
        await this.props.createNewUser(this.props.currentPage, this.props.pageSize, request);
        swal({
          title: "User is created",
          icon: "success",
          button:false,
          timer: 2000
        });
        await this.props.showModal(false, "users");
      }
      catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        if((error.response && error.response.status !== 409) || !error.response)        
          swal({
            title: parsedError.error,
            text: parsedError.message,
            icon: "error",
          });
      }
    }
  };

  style={
    default: {
      marginBottom: "30px"
    }
  }


  render() {
    const foundersOptions = this.getFoundersOptions();

    const selectorStyles = {
      control: (styles) => {
        return {
          ...styles,
          marginBottom: "30px !important",
          background: "#dedede",
          height: "40px",
          fontSize: "calc(15px + 0.4vw)"
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          width: "100%",
          textAlign: "center",
          marginLeft: "15px"
        };
      },
      placeholder: (styles) => {
        return {
          ...styles,
          width: "100%",
          textAlign: "center",
          marginLeft: "15px"
        };
      }
    };

    const style = this.state.width > 1000 ? { display: "flex" } : {};
    const rest = this.state.width > 1000 ? {} : { md: 6 };

    return (
      <Form autoComplete="off">
        <Row>
          <Col style={style}>
            <Card
              header="info"
              style={{
                // background: "#80dbff"
              }}
            >
              <Row>
                <Col className="col" {...rest}>
                  <Input
                    label="Username"
                    name="username"
                    value={this.state.username}
                    onChange={this.onChangeHandler}
                    style={this.style}
                  />
                </Col>
                <Col className="col" {...rest}>
                  <Input
                    label="Password"
                    name="password"
                    type={this.state.showPassword ? "text" : "password"}
                    value={this.state.password}
                    onChange={this.onChangeHandler}
                    style={this.style}
                  />
                  <Button
                    // className={this.state.type === "individual" ? "generate-password-button bottom" : "generate-password-button"}
                    className="generate-password-button"
                    type="button" 
                    onClick={this.handleGenerate}
                  >
                    Generate password
                  </Button>
                  {/* <Button 
                    // className={this.state.type === "individual" ? "password-eye-button bottom" : "password-eye-button"}
                    className="password-eye-button"
                    onClick={this.handleShowPassword}
                  >
                    {this.state.showPassword
                      ? <span className="fa fa-eye-slash" />
                      : <span className="fa fa-eye" />
                    }
                  </Button> */}
                </Col>
                <Col className="col" {...rest}>
                  <ControlLabel 
                    className="col-label"
                  >
                    Type
                  </ControlLabel>
                  <Select
                    key="userType"
                    options={[
                      {
                        guid: "1",
                        name: "individual"
                      },
                      {
                        guid: "2",
                        name: "entity"
                      }                      
                    ]}
                    styles={selectorStyles}
                    selectedValue={this.state.type}
                    onSelect={this.handleChangeType}
                    placeholder="Select type..."
                  />
                </Col>
                {this.state.type === "entity" &&
                <Col className="col" {...rest}>
                  <ControlLabel 
                    className="col-label"
                  >
                    Founders
                  </ControlLabel>
                  <Select
                    multi={true}
                    key="founders"
                    options={foundersOptions}
                    styles={selectorStyles}
                    // selectedValue={this.state.founders}
                    onSelect={this.handleChangeFounder}
                    placeholder="Select entity..."
                  />
                </Col>}
                <Col className="col" {...rest}>
                  <Input
                    label="Email"
                    name="email"
                    type="email"
                    value={this.state.email}
                    onChange={this.onChangeHandler}
                    style={this.style}
                  />
                </Col>
                <Col className="col" {...rest}>
                  <Input
                    label="Phone"
                    name="phone"
                    type="number"
                    value={this.state.phone}
                    onChange={this.onChangeHandler}
                    style={this.style}
                  />
                </Col>
              </Row>
            </Card>
            <Card
              header="Privileges"
            >
              <Col>
                {!this.props.privilegesLoading && Object.entries(this.state.privileges).length > 0
                  ? Object.keys(this.state.privileges).map((privilege, i) => (
                    <Col key={i} className="col" md={12}>
                      <Checkbox
                        key={i}
                        label={privilege.split("_").join(" ")}
                        data-attr={privilege}
                        onChange={this.handleCheckPrivilege}
                      />
                    </Col>))
                  : this.props.privilegesLoading ? <Loading/> : <ControlLabel className="card-label">PLEASE SELECT USER TYPE</ControlLabel>}                         
              </Col>
            </Card>            
            {/* <Card
              header="Personal info"
              style={{
                width: "50% !important",
                background: "grey",
                opacity: "0.2",
                pointerEvents: "none"
              }}
            >
              <Col>
                <Col className="col" md={6}>
                  <Input
                    label="Name"
                    name="firstName"
                    value={this.state.firstName}
                    onChange={this.onChangeHandler}
                    style={this.style}
                  />
                </Col>
                <Col className="col" md={6}>
                  <Input
                    label="Surname"
                    name="lastName"
                    value={this.state.lastName}
                    onChange={this.onChangeHandler}
                    style={this.style}
                  />
                </Col>
                <Col className="col" md={6}>
                  <Input
                    label="Email"
                    name="personalEmail"
                    type="email"
                    value={this.state.personalEmail}
                    onChange={this.onChangeHandler}
                    style={this.style}
                  />
                </Col>
                <Col className="col" md={6}>
                  <Input
                    label="Phone"
                    name="personalPhone"
                    type="number"
                    value={this.state.personalPhone}
                    onChange={this.onChangeHandler}
                    style={this.style}
                  />
                </Col>
                <Col className="col" md={12}>
                  <Input
                    label="Note"
                    name="note"
                    value={this.state.note}
                    onChange={this.onChangeHandler}
                    style={this.style}
                  />
                </Col>

              </Col>
            </Card> */}
          </Col>          
          <div align="center">
            {this.state.isLoading 
              ? <ReactLoading type='cylon' color='grey' /> 
              : <Button 
                className="submit-button"
                style={{
                  marginTop: "40px"
                }}
                onClick={this.handleSubmit}
              >
                Create
              </Button>}
          </div>
        </Row>
      </Form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    privileges: state.privileges.privileges,
    privilegesLoading: state.privileges.loading,
    users: state.users.users,
    usersSearch: state.search.usersSearch,
    pageSize: state.users.pageSize,
    currentPage: state.users.currentPage
  };
};

export default connect(mapStateToProps, {
  createNewUser,
  showModal,
  getAllUsers,
  getAllPrivileges
})(UserCreator);

UserCreator.propTypes = {
  showModal: PropTypes.func,
  createNewUser: PropTypes.func,
  usersSearch: PropTypes.object,
  pageSize: PropTypes.number,
  currentPage: PropTypes.number
};