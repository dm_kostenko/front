import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "ibankFactories/Table";
import { getAllUsers } from "ibankActions/users";
import { searchInUsers, reset } from "ibankActions/search";
import { Link } from "react-router-dom";

const columns = [
  {
    path: "guid",
    label: "ID",
    content: user => {
      return <Link className="link" to={`users/${user.guid}`}>{user.guid}</Link>;
    }
  },
  // { 
  //   path: "surname", 
  //   label: "Surname" ,
  // },
  // { 
  //   path: "name", 
  //   label: "Name" 
  // },
  {
    path: "username",
    label: "Username",
    content: user => (
      <div style={{ fontWeight: "bold" }}>{user.username}</div>
    )
  },
  {
    path: "type",
    label: "Type",
    content: user => (
      <div style={{ textTransform: "uppercase" }}>{user.type}</div>
    )
  },
  // {
  //   path: "address",
  //   label: "Address"
  // },
  {
    path: "phone",
    label: "Phone number"
  },
  {
    path: "email",
    label: "Email"
  },
  {
    path: "auth_type",
    label: "Authentication"
  },
  {
    path: "password",
    label: "Password hash"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.users.users,
    count: state.users.count,
    searchData: state.search.usersSearch,
    isSearch: state.search.isSearch,
    loading: state.users.loading,
    name: "users",
    columns
  };
};

export default connect(mapStateToProps, {
  get: getAllUsers,
  search: searchInUsers,
  reset
})(AbstractComponent);