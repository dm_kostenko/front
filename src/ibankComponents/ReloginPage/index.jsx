import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "react-bootstrap";
import { changeLogin } from "ibankActions/auth";
import PropTypes from "prop-types";
import Loading from "ibankComponents/UI/Spinner";
import { showModal } from "ibankActions/modal";
import { getUserData, setTokens } from "services/paymentBackendAPI/backendPlatform";
import { getQR } from "ibankActions/auth";
import config from "config";
import Input from "ibankComponents/UI/Input/Handler"

class ReloginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: "",
      password: "",
      otp: "",
      selectedType: "login-password",
      failedTimes: 0,
      timesToCheck: 3,
      showPassword: false,
      showOtp: false,
      authFail: false,
      error: "",
      note: "",
      isOtp: false,
      auth: false,
      loading: false
    };

    this.handleLoginChange = this.handleLoginChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  componentDidMount = () => {
    this.setState({ login: this.props.login });
  }

    handleShowPassword = () => {
      this.setState({
        showPassword: !this.state.showPassword
      });
    }

    handleShowOtp = () => {
      this.setState({
        showOtp: !this.state.showOtp
      });
    }

    handleLoginChange = (event) => {
      this.setState({
        login: event.target.value,
        authFail: false,
        note: ""
      });
    };

    handlePasswordChange = (event) => {
      this.setState({
        password: event.target.value,
        authFail: false,
        note: ""
      });
    };

    handleOtpChange = (event) => {
      this.setState({
        otp: event.target.value,
        authFail: false,
        note: ""
      });
    };

    handleCloseClick = async(e) => {
      e.preventDefault();
      await this.props.showModal(false, "entity");
    }

    handleSubmit = async(e) => {
      e.preventDefault();
      this.setState({ 
        loading: true,
        note: "",
        error: ""
      });

      let request = { 
        username: this.state.login,
        password: this.state.password,
      };

      if(this.state.isOtp)
        request["otp"] = this.state.otp;

      await this.props.changeLogin(request);
      if(this.props.response && this.props.response.accessToken && this.props.response.refreshToken) {
        const { accessToken, refreshToken } = this.props.response;
        localStorage.setItem("changeAuth", JSON.stringify({ accessToken, refreshToken }));
        const { auth_type, type } = getUserData();
        if(auth_type !== "login-password-otp" && type !== "admin" && config.tfa) {
          setTimeout(async() => {               
            await this.props.getQR();
            await this.props.showModal(false, "entity");
          }, 100);          
          await this.props.showModal(true, "profile");
        }
        else {
          localStorage.removeItem("changeAuth");
          setTokens(accessToken, refreshToken);
          this.setState({ auth: true });
          localStorage.setItem("isLoggedIn", true);
          window.location.replace(process.env.PUBLIC_URL || "/");
        }
      }
      else if(this.props.response && this.props.response.status === "One time password required") {
        this.setState({
          isOtp: true,
          loading: false
        });
      }
      else {
        let state = {
          authFail: true, 
          error: this.props.error, 
          loading: false,
          failedTimes: this.state.failedTimes,
          otp: "",
          note: ""
        };
        if(this.state.isOtp)
          state.failedTimes++;
        if([ "Code is expired", "Requests limit exceeded" ].includes(this.props.error) || state.failedTimes >= this.state.timesToCheck)
          state = {
            ...state,
            isOtp: false,
            failedTimes: 0,
            login: "",
            password: "",
            error: "Requests limit exceeded"
          };
        this.setState({ 
          ...state         
        });
      }
    };


    validateForm() {     
      if(!this.state.isOtp)
        return this.state.login.length > 0 && this.state.password.length > 0;
      else
        return this.state.login.length > 0 && this.state.password.length > 0 && this.state.otp.length > 0;
    }

    handleButtonRender() {
      return this.state.loading 
        ? <button className="button-submit-loading" disabled>
          <Loading color="white" />
        </button> 
        : <button 
          className="button-submit" 
          type="submit" 
          onClick={this.handleSubmit}
          disabled={!this.validateForm()}
        >
            SIGN IN
        </button>;
    }


    render() {
      return (
        // <div className="ReloginPage">          
        <div className="relogin-page">
          <div className="relogin-form" >
            <form className="login-form" autoComplete="off">
              <p className="header-login"/>
              <div>
                {!this.state.isOtp &&
                  <>                  
                  <div 
                    id="login-input"
                    className="login"
                  >
                    <i className="fa fa-user icon" />
                    {/* <input type="text" placeholder="login" onChange={this.handleLoginChange} name="login" value={this.state.login} /> */}
                    <input type="text" value={this.state.login} disabled/>
                  </div>
                  <div
                    id="password-input"
                    className="password-open"
                  >
                    <i 
                      className={"fa fa-lock icon"}
                    />
                    <Input                      
                      type={this.state.showPassword ? "text" : "password"} 
                      placeholder="password" 
                      onChange={this.handlePasswordChange} 
                      name="password"
                      value={this.state.password}
                      disabled={!this.state.selectedType.includes("password") ? "true" : ""}
                    />
                    <Button className="login-page-eye" onClick={this.handleShowPassword}>
                      {this.state.showPassword
                        ? <span className="fa fa-eye-slash"/>
                        : <span className="fa fa-eye"/>
                      }
                    </Button>
                  </div>   
                    </>  }    
                <div 
                  id="otp-input" 
                  className={this.state.isOtp ? "otp-open" : "otp-closed"}
                >
                  <i className="fa fa-key icon" />
                  <Input 
                    type={this.state.showOtp ? "text" : "password"}                        
                    placeholder="verification code" 
                    onChange={this.handleOtpChange} 
                    value={this.state.otp}
                    name="otp" 
                  />
                  <Button 
                    className="login-page-eye-otp" 
                    onClick={this.handleShowOtp}
                  >
                    {this.state.showOtp
                      ? <span className="fa fa-eye-slash"/>
                      : <span className="fa fa-eye"/>}
                  </Button>
                </div>                                            
                <p className="login-page-note">
                  {this.state.note}
                </p>
                { this.handleButtonRender() }                                                    
                {this.state.authFail &&
                    <p className="access-denied">
                      {this.state.error}
                    </p>}  
              </div>
            </form>      
            <div 
              className="close-text"
              onClick={this.handleCloseClick}
            >
              Close
            </div>
          </div>
        </div>
        // </div>
      );
    }

}

const mapStateToProps = (state) => {
  return {
    response: state.auth.changeResponse,
    error: state.auth.changeError,
  };
};

export default connect (mapStateToProps, {
  changeLogin,
  showModal,
  getQR
})(ReloginPage);

ReloginPage.propTypes = {
  response: PropTypes.bool,
  error: PropTypes.string,
  logIn: PropTypes.func,
};