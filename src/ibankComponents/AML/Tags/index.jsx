import React, { Component } from "react";
import Content from "ibankViews/Content";
import { getAllTags, getAllCaseTags, updateTag } from "ibankActions/aml";
import { connect } from "react-redux";
import CardWhite from "ibankComponents/UI/Card/White";
import Spinner from "ibankComponents/UI/Spinner";
import { Row, Tabs, Tab, ControlLabel, Badge, OverlayTrigger, Popover, DropdownButton, MenuItem } from "react-bootstrap";
import { SketchPicker, CompactPicker } from 'react-color';
import AmlTag from "ibankComponents/UI/AmlTag"
import Input from "ibankComponents/UI/Input"
import Radio from "ibankComponents/UI/Radio"
import Modal from "ibankComponents/UI/Modal";
import { parseResponse } from "helpers/parseResponse";
import swal from "sweetalert";


class AmlTags extends Component {

  state = {
    color: '#aedbd2',
    tagName: "",
    tagTypeOption: "name",
    isColorOpen: false,
    loading: false,
  }

  componentDidMount = async () => {
    document.addEventListener('mousedown', this.handleClickOutside);
    await this.props.getAllTags({})
    await this.props.getAllCaseTags({ caseId: "all" })
  }

  handleClickOutside = (e) => {
    if (this.ref && !this.ref.contains(e.target) && !this.colorRef.contains(e.target)) {
      this.setState({ isColorOpen: false });
    }
  }

  handleChange = (e) => this.setState({ [e.target.name]: e.target.value });

  handleChangeColor = (color) => {
    this.setState({ color: color.hex })
  }

  handleCancel = () => this.setState({ color: "#aedbd2", isColorOpen: false })

  handleSelect = () => this.setState({ isColorOpen: false })

  handleClickColor = () => this.setState({ isColorOpen: !this.state.isColorOpen })

  getTags = () => {
    let res = []
    const { tags } = this.props;
    for(let i = 0; i < tags.length; i++) {
      if (i % 3 == 0) res.push([]);
        res[Math.floor(i / 3)].push(tags[i]);
    }
    res.forEach(arr => {
      if(arr.length < 3)
        while(arr.length < 3)
          arr.push(undefined)
    })
    return res
  }

  handleDelete = async(e, name) => {
    e.preventDefault();
    try {
      const caseTag = this.props.caseTags.find(tag => tag.tag_name === name)
      if(caseTag) {
        swal({
          title: "We are sorry",
          text: `We were unable to delete the "${name}" tag. This tag might be used at least one search.`
        });
      }
      else {
        await this.props.updateTag({ 
          name,
          del: true
        })
        swal({
          title: "Status",
          text: "Ok",
          icon: "success",
          button: false,
          timer: 2000
        });
        await this.props.getAllTags({})
        await this.props.getAllCaseTags({ caseId: "all" })
      }      
    }
    catch(error) {
      const parsedError = parseResponse(error);
      if ((error.response && error.response.status !== 409) || !error.response)
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
    }
  }

  matchedValue = (label, text) => (
    text.length > 0
    ? <>
      <OverlayTrigger 
        placement="bottom"
        overlay={
          <Popover>
            {text.map(item => (
              <>
                {item}
                <br/>
              </>
            ))}
          </Popover>
        }>
        <Badge className="badge-aml-matched">  
          {`${label} `}
          <i className="fas fa-info-circle" style={{ fontSize: "calc(4px + 0.5vw)" }} />
        </Badge>
      </OverlayTrigger>
      <br/>
    </>
    : <>
      <Badge className="badge-aml-matched">  
        {label}
      </Badge>
      <br/>
    </>
  )

  handleSubmit = async(e) => {
    e.preventDefault();
    try {
      const { tagName, tagTypeOption, color } = this.state;
      await this.props.updateTag({
        name: tagName,
        type: tagTypeOption,
        color
      })
      swal({
        title: "Status",
        text: "Ok",
        icon: "success"
      });
      this.setState({
        color: '#aedbd2',
        tagName: "",
        tagTypeOption: "name",
        isColorOpen: false
      })
      await this.props.getAllTags({})
    }
    catch(error) {
      const parsedError = parseResponse(error);
      if ((error.response && error.response.status !== 409) || !error.response)
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
    }
  }

  render() {
    const newTags = this.getTags()

    if (this.props.loading)
      return <Content>
        <Spinner />
      </Content>;

    return <Content className="content aml">
      <Row className="aml-tags">
        {/* <Col md={4}> */}
          <CardWhite 
            className="card-left"
            style={{
              paddingRight: "20px",
              // overflowY: "auto"
            }}
          >
            <div className="head">
              Add tag
            </div>
            <Input
              label="Name:"
              name="tagName"
              value={this.state.tagName}
              onChange={this.handleChange}
              style={{
                default: {
                  className: "aml-col"
                }
              }}
            />
            <div className="case-management">
              <div className="display">
                Case Management Display:
              </div>
              <div className="radios">
                <Radio
                  number="1"
                  name="tagTypeOption"
                  option="name"
                  label="Tag Name Only (default)"
                  style={{
                    label: {
                      textTransform: "none"
                    }
                  }}
                  checked={this.state.tagTypeOption === "name"}
                  onChange={this.handleChange}
                />
                <Radio
                  number="2"
                  name="tagTypeOption"
                  option="value"
                  label="Tag Value Only"
                  checked={this.state.tagTypeOption === "value"}
                  onChange={this.handleChange}
                />
                <Radio
                  number="3"
                  name="tagTypeOption"
                  option="name_value"
                  label="Tag Name Combination"
                  checked={this.state.tagTypeOption === "name_value"}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <ControlLabel className="col-label-color" style={{ textTransform: "none" }}>
              Color:
            </ControlLabel>
            <div className="color-wrapper">
              <button
                ref={ref => this.colorRef = ref}
                className="color-button"
                onClick={this.handleClickColor}
                style={{
                  background: this.state.color
                }}
              />
              {this.state.isColorOpen &&
              <div ref={ref => this.ref = ref}>
                <Tabs className="color-picker-tabs" defaultActiveKey="0" transition={false}>
                  <Tab className="color-picker-tab" eventKey="0" title="Wheel" mountOnEnter={false}>
                    <CardWhite className="color">
                      <SketchPicker
                        color={this.state.color}
                        onChange={this.handleChangeColor}
                      />
                      <div>
                        <button 
                          className="cancel-button" 
                          onClick={this.handleCancel}
                        >
                          Cancel
                        </button>
                        <button 
                          className="select-button"
                          onClick={this.handleSelect}
                        >
                          Select
                        </button>
                      </div>
                    </CardWhite>
                  </Tab>
                  <Tab className="color-picker-tab" eventKey="1" title="Palette" mountOnEnter={false}>
                    <CardWhite className="color">
                      <CompactPicker
                        color={this.state.color}
                        onChange={this.handleChangeColor}
                      />
                      <div>
                        <button 
                          className="cancel-button" 
                          onClick={this.handleCancel}
                        >
                          Cancel
                        </button>
                        <button 
                          className="select-button"
                          onClick={this.handleSelect}
                        >
                          Select
                        </button>
                      </div>
                    </CardWhite>
                  </Tab>
                </Tabs>
              </div>}
            </div>
            <hr className="tags-hr" />
            <div className="aml-save-button-wrapper">
              <button className="aml-save-button" onClick={this.handleSubmit}>
                Save
              </button>
            </div>
          </CardWhite>
        {/* </Col>
        <Col md={8}> */}
          <CardWhite 
            className="card-right"
            style={{
              paddingRight: "20px",
              // overflowY: "auto"
            }}
          >
            <div className="head">
              Tags
            </div>
            <div className="tags-rows">
              {newTags.map((tagArr, iArr) => (
                <div className="tags-row">
                  {tagArr.map(tag => (
                    <div className="tags-col">
                      {tag && 
                      <AmlTag
                        tag={tag.name}
                        color={tag.color}
                        element={<DropdownButton 
                            className="aml-tag-button"
                            title={""}
                          >
                          {/* <MenuItem eventKey="edit">
                            <Modal 
                              header="Edit tag"
                              content={} 
                              button={<MenuItem eventKey="edit" onClick={this.handleEdit}>Edit</MenuItem>}
                              state="editTag"
                            />
                          </MenuItem> */}
                          <MenuItem eventKey="del" className="aml-menu-item-delete" onClick={(e) => this.handleDelete(e, tag.name)}>Delete</MenuItem>                          
                        </DropdownButton>}
                        style={{
                          fontSize: "15px",
                          fontWeight: "400",
                          height: "35px",
                          lineHeight: "35px"
                        }}
                      />}
                    </div>
                  ))}
                </div>
              ))}
            </div>
          </CardWhite>
        {/* </Col> */}
      </Row>
    </Content>;
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.aml.tagsLoading,
    tags: state.aml.tags,
    caseTags: state.aml.caseTags
  };
};

export default connect(mapStateToProps, {
  getAllTags,
  updateTag,
  getAllCaseTags
})(AmlTags);
