import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "ibankFactories/Table";
import { getAllAmlHistory, getAllTags } from "ibankActions/aml";
// import { searchInRates, reset } from "ibankActions/search";
import { Link } from "react-router-dom";
import { Badge, DropdownButton, MenuItem, OverlayTrigger, Popover } from "react-bootstrap";
import moment from "moment";
import AmlTag from "ibankComponents/UI/AmlTag"


const columns = [
  {
    path: "created_at",
    label: "Created at",
    content: item => moment(item.created_at).format("MMM DD, YYYY h:mm A")
  },
  {
    path: "search_term",
    label: "Name",
    content: item => (
      <>
        {item.total_hits > 0 
        ? <Link 
          className="link-red"
          style={{ fontWeight: "bold" }}
          to={`aml/${item.id}`}
        >
          {item.search_term} <Badge className="badge-aml">{item.total_hits}</Badge>
        </Link>
        : <Link 
        className="link"
        style={{ fontWeight: "bold" }}
        to={`aml/${item.id}`}
      >
        {item.search_term}
      </Link>}
      {item.tags.length > 2 
      ? item.tags.slice(0, 2).map((tag, i) => (
          <div className="aml-history-tag">
            <AmlTag
              tag={(["name", "name_value"].includes(tag.type) || (tag.type === "value" && !tag.value)) && tag.tag_name}
              value={(["value", "name_value"].includes(tag.type) && tag.value) && tag.value}
              color={tag.color}
              popoverText={["name", "value"].includes(tag.type) && (tag.tag_name || tag.value) && (tag.type === "name" ? tag.value : tag.tag_name)}
              style={{ display: "inline-block", paddingRight: "8px", height: "30px", lineHeight: "30px", fontSize: "13px" }}
            />
            {i === 1 &&
            <DropdownButton 
              className="aml-history-tag-button"
              title=""
            >
              {item.tags.slice(2, item.tags.length).map(_tag => (
                <MenuItem eventKey="del" className="aml-menu-item-history">
                  <AmlTag
                    tag={(["name", "name_value"].includes(_tag.type) || (_tag.type === "value" && !_tag.value)) && _tag.tag_name}
                    value={(["value", "name_value"].includes(_tag.type) && _tag.value) && _tag.value}
                    color={_tag.color}
                    popoverText={["name", "value"].includes(_tag.type) && (_tag.tag_name || _tag.value) && (_tag.type === "name" ? _tag.value : _tag.tag_name)}
                    style={{ display: "inline-block", paddingRight: "8px", height: "30px", lineHeight: "30px" }}
                  />
                </MenuItem> 
              ))}
                                       
            </DropdownButton>}
          </div>))
      : item.tags.map(tag => (
        <div className="aml-history-tag">
          <AmlTag
            tag={(["name", "name_value"].includes(tag.type) || (tag.type === "value" && !tag.value)) && tag.tag_name}
            value={(["value", "name_value"].includes(tag.type) && tag.value) && tag.value}
            color={tag.color}
            popoverText={["name", "value"].includes(tag.type) && (tag.tag_name || tag.value) && (tag.type === "name" ? tag.value : tag.tag_name)}
            style={{ display: "inline-block", paddingRight: "8px", height: "30px", lineHeight: "30px", fontSize: "13px" }}
          />
        </div>))}
    </>
    )
  },
  {
    path: "client_ref",
    label: "Reference"
  },
  {
    path: "match_status",
    label: "Match Status",
    content: item => item.match_status[0].toUpperCase() + item.match_status.slice(1, item.match_status.length).replace("_", " ")
  },
  {
    path: "risk_level",
    label: "Risk Level",
    content: item => item.risk_level[0].toUpperCase() + item.risk_level.slice(1, item.risk_level.length)
  },
  {
    path: "searcher",
    label: "Searcher"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.aml.amlHistory,
    count: state.aml.count,
    searchData: {},
    isSearch: state.search.isSearch,
    disableSearch: true,
    amlSearch: true,
    loading: state.aml.loading,
    name: "amlhistory",
    tags: state.aml.tags,
    columns
  };
};

export default connect(mapStateToProps, {
  get: getAllAmlHistory,
  search: () => {},
  reset: () => {},
  getTags: getAllTags
})(AbstractComponent);