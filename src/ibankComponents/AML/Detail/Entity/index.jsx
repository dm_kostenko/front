import React, { Component } from "react";
import Content from "ibankViews/Content";
import { getAmlDetail, updateAmlSearch, getAllCaseComments, addCommentToCase } from "ibankActions/aml";
import { connect } from "react-redux";
import Card from "ibankComponents/UI/Card";
import DetailInfoLabel from "ibankComponents/UI/DetailInfoLabel";
import AmlLabel from "ibankComponents/UI/AmlLabel";
import PropTypes from "prop-types";
import Spinner from "ibankComponents/UI/Spinner";
import tagsDictionary from "helpers/fieldsDictionary";
import { Tabs, Tab, Col, Badge, OverlayTrigger, Popover } from "react-bootstrap";
import moment from "moment";
import { exportAml } from "helpers/export";
import Modal from "ibankComponents/UI/Modal";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse.js";
import { Link } from "react-router-dom";
import MediaList from "ibankComponents/UI/MediaList";
import MediaItem from "ibankComponents/UI/MediaItem";
import KeyInfoList from "ibankComponents/UI/KeyInfoList";
import KeyInfoLabel from "ibankComponents/UI/KeyInfoLabel";
import ListingList from "ibankComponents/UI/ListingList";
import ListingItem from "ibankComponents/UI/ListingItem";
import CommentItem from "ibankComponents/UI/AmlCommentItem";
import Select from "ibankComponents/UI/Select";



const monitor = {
  classNames: {
    [true]: "fas fa-ban aml-detail stripe-icon stop-monitor",
    [false]: "fas fa-heartbeat aml-detail stripe-icon monitor"
  },
  texts: {
    [true]: "Stop Monitoring Case",
    [false]: "Monitor Case"
  }
}

const compareStatuses = (a, b) => a.status < b.status ? -1 : 1

const toCapitalize = str => str[0].toUpperCase() + str.slice(1, str.length)

const getDob = fields => {
  const dobFields = fields.filter(item => item.tag === "date_of_birth")
  let obj = {}
  dobFields.forEach(item => {
    obj[item.value] = obj[item.value] === undefined
    ? 0
    : obj[item.value] + 1
  })

  let max = 0;
  let key = Object.keys(obj)[0];
  Object.keys(obj).forEach(item => { 
    if(max < obj[item]) { 
      max = obj[item]
      key = item
    }
  });
  return key;
}

const matchStatusRefs = {
  [undefined]: "Unknown",
  "unknown": "Unknown",
  "no_match": "No Match",
  "potential_match": "Potential Match",
  "false_positive": "False Positive",
  "true_positive": "True Positive",
  "Unknown": "unknown",
  "No Match": "no_match",
  "Potential Match": "potential_match",
  "False Positive": "false_positive",
  "True Positive": "true_positive",
}

class AmlDetailEntity extends Component {

  state = {
    wasFound: true,
    hit: {},
    isRightOpen: true,
    isSearchInfo: true,
    matchedValues: "",
    dob: "",
    years: "",
    listing: {
      "sanction": {},
      "warning": {},
      "adverse-media": {},
      "pep": {}
    },
    whitelistOptions: [
      {
        guid: "On",
        name: "On"
      },
      {
        guid: "Off",
        name: "Off"
      }
    ],
    whitelist: "",
    riskOptions: [
      {
        guid: "Unknown",
        name: "Unknown"
      },
      {
        guid: "Low",
        name: "Low"
      },
      {
        guid: "Medium",
        name: "Medium"
      },
      {
        guid: "High",
        name: "High"
      }      
    ],
    riskLevel: "",
    matchStatusOptions: [
      {
        guid: "Unknown",
        name: "Unknown"
      },
      {
        guid: "No Match",
        name: "No Match"
      },
      {
        guid: "Potential Match",
        name: "Potential Match"
      },
      {
        guid: "False Positive",
        name: "False Positive"
      },
      {
        guid: "True Positive",
        name: "True Positive"
      }
    ],
    mainMatchStatus: "",
    assets: [],
    comment: "",
    loading: true
  }

  async componentDidMount() {
    await this.props.getAmlDetail(this.props.match.params.searchId);
    await this.props.getAllCaseComments({ 
      id: this.props.match.params.searchId,
      entityId: this.props.match.params.entityId
    })

    if(this.props.amlDetail.hits.length > 0) {
      const hit = this.props.amlDetail.hits.find(item => item.doc.id === this.props.match.params.entityId)
      const matchedValues = this.getMatchedValues(hit.doc.source_notes)
      const riskLevel = hit.risk_level ? toCapitalize(hit.risk_level) : hit.risk_level
      const mainMatchStatus = matchStatusRefs[hit.match_status]
      const whitelist = hit.is_whitelisted ? "On" : "Off"
      const dob = getDob(hit.doc.fields);
      const fromDate = new Date(dob);
      const toDate = new Date();
  
      const _fromDate = moment([ fromDate.getFullYear(), fromDate.getMonth() ]);
      const _toDate = moment([ toDate.getFullYear(), toDate.getMonth() ]);
      const years = _toDate.diff(_fromDate, "years")

      const listing = this.getListingData(hit.doc.source_notes, hit.doc.fields)

      this.setState({ 
        hit,
        mainMatchStatus,
        riskLevel,
        matchedValues,
        whitelist,
        dob,
        years,
        listing,
        assets: hit.doc.assets,
        loading: false
      })
    }
  }

  getListingData = (sourceNotes, fields) => {
    let listing = {
      "sanction": {},
      "warning": {},
      "adverse-media": {},
      "pep": {}
    }
    Object.keys(listing).forEach(type => {
      const listsOfType = Object.keys(sourceNotes).filter(i => sourceNotes[i].aml_types.includes(type) || sourceNotes[i].aml_types.find(i => i.includes(type)))
      const countListsOfType = listsOfType.length
      if(countListsOfType > 0) {
        let data = [];
        listsOfType.forEach(list => {
          let item = !sourceNotes[list].listing_ended_utc
          ? {
            status: "Currently on",
            name: sourceNotes[list].name,
            url: sourceNotes[list].url,
            listed: `${moment(sourceNotes[list].listing_started_utc).format("DD MMM YYYY")} - Present`
          }
          : {
            status: "Removed from",
            name: sourceNotes[list].name,
            url: sourceNotes[list].url,
            listed: `${moment(sourceNotes[list].listing_started_utc).format("DD MMM YYYY")} - ${moment(sourceNotes[list].listing_ended_utc).format("DD MMM YYYY")}`,
            removed: `${moment(sourceNotes[list].listing_ended_utc).format("DD MMM YYYY")}`
          }

          fields.forEach(field => {
            if(field.source === list)
              item[field.name] = field.value
          })

          data.push(item)

        })

        listing[type] = {
          len: countListsOfType,
          data
        }
      }
    })

    const listingKeys = Object.keys(listing).sort((a, b) => {
      let f = -1;
      listing[a].data && listing[a].data.forEach(item => {
        if(item.listing_ended_utc)
          f = 1
      })
      return f
    })

    let newListing = {};
    listingKeys.forEach(key => { 
      newListing[key] = listing[key]
    })
    Object.keys(newListing).forEach(key => {
      newListing[key].data && (newListing[key].data = newListing[key].data.sort(compareStatuses))
    })

    return newListing;
  }


  matchedValue = (label, text) => (
    text && text.length > 0
    ? <>
      <OverlayTrigger 
        placement="bottom"
        overlay={
          <Popover>
            {text.map(item => (
              <>
                {item}
                <br/>
              </>
            ))}
          </Popover>
        }>
        <Badge className="badge-aml-matched-entity">  
          {`${label} `}
          <i className="fas fa-info-circle" style={{ fontSize: "calc(4px + 0.5vw)" }} />
        </Badge>
      </OverlayTrigger>
      {/* <br/> */}
    </>
    : <>
      <Badge className="badge-aml-matched-entity">  
        {label}
      </Badge>
      {/* <br/> */}
    </>
  )

  stripeIcon = (className, text, action) => (
    <OverlayTrigger 
      placement="left"
      overlay={
        <Popover className="aml-detail popover" >
          {text}
        </Popover>
    }>
      {Array.isArray(className)
      ? <>
        {className.map((item, i) => (
          <i className={className[i]} onClick={action[i] || (() => {})}/>
        ))}
        </>
        
      : <i className={className} onClick={action}/>}
    </OverlayTrigger>
  )

  getMatchedValues = sourceNotes => {
    let arr = []
    const mainTypes = [ "sanction", "warning", "adverse-media", "pep" ]
    Object.keys(sourceNotes).forEach(item => {
      let main = sourceNotes[item].aml_types.filter(type => mainTypes.includes(type))
      const others = sourceNotes[item].aml_types.filter(type => !mainTypes.includes(type))
      if(main.length === 0) {
        for(let i = 0; i < others.length; i++)
          for(let j = 0; j < mainTypes.length; j++)
            if(others[i].includes(mainTypes[j])) {
              main = [ mainTypes[j] ]
              break
            }
      }
      const obj = { 
        main, 
        others
      }
      if(!arr.find(item => JSON.stringify(obj) === JSON.stringify(item)))
        arr.push(obj)
    })
    return [ ...new Set(arr) ].map(item => this.matchedValue(toCapitalize(item.main[0]).split("-").join(" "), item.others.map(item => item.toUpperCase().split("-").join(" "))))
  }

  parseJson = str => {
    try {
      return JSON.stringify(str, null, "\t");
    } catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  handleRiskSelect = async option => {
    if(option.guid !== this.state.riskLevel) {
      this.setState({
        riskLoading: true
      })
      try {
        await this.props.updateAmlSearch({
          id: this.props.match.params.searchId,
          entities: [ this.props.match.params.entityId ],
          risk_level: option.guid.toLowerCase()
        });

        this.setState({
          riskLevel: option.guid,
          riskLoading: false
        })
        swal({
          title: "Case Management has been updated!",
          icon: "success",
          button: false,
          timer: 2000
        });
      }
      catch(error) {
        this.setState({ riskLoading: false })
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
      setTimeout(() => {
        this.setState({ riskLoading: false })
      }, 2000)
    }
  }

  handleWhitelistSelect = async option => {
    if(option.guid !== this.state.whitelist) {
      this.setState({
        whitelistLoading: true
      })
      try {
        await this.props.updateAmlSearch({
          id: this.props.match.params.searchId,
          entities: [ this.props.match.params.entityId ],
          is_whitelisted: option.guid === "On"
        });

        this.setState({
          whitelist: option.guid,
          whitelistLoading: false
        })
        swal({
          title: "Case Management has been updated!",
          icon: "success",
          button: false,
          timer: 2000
        });
      }
      catch(error) {
        this.setState({ whitelistLoading: false })
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
      setTimeout(() => {
        this.setState({ whitelistLoading: false })
      }, 2000)
    }
  }

  handleMatchStatusSelect = async option => {
    if(option.guid !== this.state.mainMatchStatus) {
      this.setState({
        matchStatusLoading: true
      })
      try {
        await this.props.updateAmlSearch({
          id: this.props.match.params.searchId,
          entities: [ this.props.match.params.entityId ],
          match_status: matchStatusRefs[option.guid]
        });

        this.setState({
          mainMatchStatus: option.guid,
          matchStatusLoading: false
        })
        swal({
          title: "Case Management has been updated!",
          icon: "success",
          button: false,
          timer: 2000
        });
      }
      catch(error) {
        this.setState({ matchStatusLoading: false })
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
      setTimeout(() => {
        this.setState({ matchStatusLoading: false })
      }, 2000)
    }
  }

  handleClickInfo = () => {
    let { isSearchInfo, isRightOpen } = this.state;
    isRightOpen = !isSearchInfo || (!isRightOpen && isSearchInfo)
    this.setState({ 
      isRightOpen,
      isSearchInfo: true 
    })
  }

  handleChangeComment = e => this.setState({ comment: e.target.value })

  handleAddComment = async() => {
    const { comment } = this.state
    this.setState({ comment: "" })
    if(comment.trim() !== "") {
      try {
        await this.props.addCommentToCase({
          id: this.props.match.params.searchId,
          entityId: this.props.match.params.entityId,
          text: comment
        })
        swal({
          title: "Comment has been added!",
          icon: "success",
          button: false,
          timer: 2000
        });
        await this.props.getAllCaseComments({ 
          id: this.props.match.params.searchId,
          entityId: this.props.match.params.entityId
        })
      }
      catch(error) {
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
    }
  }

  handleClickComments = () => {
    let { isSearchInfo, isRightOpen } = this.state;
    isRightOpen = isSearchInfo || (!isRightOpen && !isSearchInfo)
    this.setState({ 
      isRightOpen,
      isSearchInfo: false 
    })
  }


  render() {
    const { hit, matchedValues, dob, years, listing, assets } = this.state;

    const searchInfoClassName = this.state.isSearchInfo
    ? "fas fa-info aml-detail stripe-icon info-selected"
    : "fas fa-info aml-detail stripe-icon info"

    const commentsClassName = !this.state.isSearchInfo
    ? "fas fa-comment aml-detail stripe-icon comments-selected"
    : "fas fa-comment aml-detail stripe-icon comments"

    if (this.state.loading)
      return <Content>
        <Spinner />
      </Content>;
    return <Content className="content aml">
      <div className="aml-detail-header-wrapper">
        <div className="aml-detail-title-wrapper">
          <div className="aml-detail-title">
            {hit.doc ? hit.doc.name : ""}
          </div>
          <div className="aml-detail-matched">
            {matchedValues}
          </div>
        </div>
        <div className="aml-detail-header-right">
          <div className="select risk">
            <Select
              label="Risk level"
              options={this.state.riskOptions}
              onSelect={this.handleRiskSelect}
              defaultValue={this.state.riskLevel}
              isLoading={this.state.riskLoading}
              iconClassname="risk-selector"
            />
          </div>
          <div className="select match-status">
            <Select
              label="Match status"
              options={this.state.matchStatusOptions}
              onSelect={this.handleMatchStatusSelect}
              defaultValue={this.state.mainMatchStatus}
              isLoading={this.state.matchStatusLoading}
              iconClassname="match-status-selector"
            />
          </div>
          <div className="select whitelist">
            <Select
              label="Whitelist"
              options={this.state.whitelistOptions}
              onSelect={this.handleWhitelistSelect}
              defaultValue={this.state.whitelist}
              isLoading={this.state.whitelistLoading}
              iconClassname="whitelist-selector"
            />
          </div>
        </div>
      </div>
        <div 
          className="aml-detail cards-row"
        >
          <div 
            className="aml-detail entity-col"
            id={this.state.isRightOpen ? "open" : "closed"}
          >
            <Tabs className="aml-tabs" defaultActiveKey="0" transition={false}>
              <Tab className="aml-tab" eventKey="0" key="0" title="Key Data" mountOnEnter={true}>
                <div className="card media-list">
                  <div className="key-information-header">
                    Key information
                  </div>
                  <KeyInfoList>
                    <KeyInfoLabel
                      label="Full name"
                      value={hit.doc ? hit.doc.name : ""}
                    />
                    <KeyInfoLabel
                      label="Entity type"
                      value={hit.doc ? hit.doc.aka.map(i => i.name).join(", ") : ""}
                    />
                    <KeyInfoLabel
                      label="Countries"
                      value={hit.doc ? hit.doc.fields.find(i => i.tag === "country_names").value : ""}
                    />
                    <KeyInfoLabel
                      label="Date of birth"
                      value={`${dob} (Age: ${years})`}                    
                    />                                       
                  </KeyInfoList>
                </div>
                <div className="card media-list listing-wrapper-main">
                  {Object.keys(listing).filter(i => Object.keys(listing[i]).length).map(source => (
                    <ListingList
                      source={this.matchedValue(toCapitalize(source).split("-").join(" "))}
                      header={`${listing[source].len} ${source.split("-").join(" ")} Listing`}
                    >
                      {listing[source].data.map(item => (
                        <ListingItem
                          header={item.name}
                          url={item.url}
                          status={item.status}
                          data={item}
                        />
                      ))}
                    </ListingList>
                  ))}
                </div>
              </Tab>
              <Tab className="aml-tab" eventKey="1" key="1" title="Adverse Media" mountOnEnter={true}>
                <MediaList>
                  {hit.doc 
                    ? hit.doc.media.sort((a,b) => new Date(b.date) - new Date(a.date)).map(item => (
                    <MediaItem
                      header={item.title}
                      date={moment(item.date).format("DD MMM YYYY")}
                      text={item.snippet}
                      link={item.url}
                    />
                  ))
                  : ""}
                </MediaList>
              </Tab>
              <Tab className="aml-tab" eventKey="2" key="2" title="Documents" mountOnEnter={true}>
                <div className="card media-list">
                  <div className="key-information-header">
                    Media documents
                  </div>
                  <KeyInfoList>
                    <KeyInfoLabel
                      label="Data source"
                      value={assets.map(asset => (
                        <div className="aml-documents">
                          <a href={asset.public_url} target="_blank">
                            <i className="fas fa-download download-asset" href/>
                          </a>
                          <a>{asset.source.split("-").join(" ")}</a>
                        </div>
                      ))}
                    />
                  </KeyInfoList>
                </div>
              </Tab>
            </Tabs>
        </div>
        <div
          className="aml-detail col-stripe"
        >
          <div
            className="aml-detail stripe"
          >
            {this.stripeIcon(searchInfoClassName, "Search Information", this.handleClickInfo)}
            {this.stripeIcon(commentsClassName, "Comments", this.handleClickComments)}
          </div>
        </div>
        {this.state.isRightOpen &&
        (this.state.isSearchInfo 
        ? <div className="aml-search-info wrap">
          <div className="aml-search-info head">
            Search information
          </div>
          <div className="aml-search-info col">
            <AmlLabel
              label="Search term:"
              value={this.props.amlDetail.search_term}
            />
            <AmlLabel
              label="Created at:"
              value={moment(this.props.amlDetail.created_at).format("MMM DD, YYYY hh:mm A")}
            />
            <AmlLabel
              label="Search ref:"
              value={this.props.amlDetail.ref}
            />
            {this.props.amlDetail.client_ref &&
            <AmlLabel
              label="Client ref:"
              value={this.props.amlDetail.client_ref}
            />}
            <AmlLabel
              label="Fuzziness interval:"
              value={this.props.amlDetail.filters.fuzziness*100 + "%"}
            />
            {this.props.amlDetail.filters.birth_year &&
            <AmlLabel
              label="Year of birth:"
              value={this.props.amlDetail.filters.birth_year}
            />}
            <AmlLabel
              label="Entity type:"
              value={this.props.amlDetail.filters.entity_type}
            />
          </div>
        </div>
        : <div className="aml-comments wrap">
          <div className="aml-comments head">
            Comments
          </div>
          <div className="aml-comments col">
            {this.props.caseComments.map(item => (
              <CommentItem
                name={item.username}
                date={item.date}
                text={item.text}
              />
            ))}
          </div>
          <div className="aml-comments-input">
            <textarea 
              placeholder="Type a message to comment on the entity"
              onChange={this.handleChangeComment}
            />
            <button onClick={this.handleAddComment}>
              Add
            </button>
          </div>
        </div>)}
      </div>
    </Content>;
  }
}

const mapStateToProps = (state) => {
  return {
    amlDetail: state.aml.amlDetail,
    loading: state.aml.loadingDetail,
    response: state.aml.updateAmlDetailResponse,
    caseComments: state.aml.caseComments,
    commentResponse: state.aml.addCaseCommentResponse
  };
};

export default connect(mapStateToProps, {
  getAmlDetail,
  updateAmlSearch,
  getAllCaseComments, 
  addCommentToCase
})(AmlDetailEntity);

AmlDetailEntity.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  loadingDetail: PropTypes.bool,
  getAmlDetail: PropTypes.func,
  updateAmlSearch: PropTypes.func,
  amlDetail: PropTypes.object
};