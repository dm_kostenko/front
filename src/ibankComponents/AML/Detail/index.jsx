import React, { Component } from "react";
import Content from "ibankViews/Content";
import { getAmlDetail, addFiles, getAllCaseFiles, getAllTags, getAllCaseTags, attachTagsToCase, updateAmlSearch, getAllCaseComments, addCommentToCase } from "ibankActions/aml";
import { connect } from "react-redux";
import { showModal } from "ibankActions/modal";
import Card from "ibankComponents/UI/Card";
import CardWhite from "ibankComponents/UI/Card/White";
import DetailInfoLabel from "ibankComponents/UI/DetailInfoLabel";
import AmlLabel from "ibankComponents/UI/AmlLabel";
import PropTypes from "prop-types";
import Spinner from "ibankComponents/UI/Spinner";
import tagsDictionary from "helpers/fieldsDictionary";
import { Row, Col, Badge, OverlayTrigger, Popover } from "react-bootstrap";
import moment from "moment";
import { exportAml } from "helpers/export";
import Modal from "ibankComponents/UI/Modal";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse.js";
import { Link } from "react-router-dom";
import Select from "ibankComponents/UI/Select";
import CommentItem from "ibankComponents/UI/AmlCommentItem";
import AmlTag from "ibankComponents/UI/AmlTag"
import Input from "ibankComponents/UI/Input/Handler"

const getDob = fields => {
  const dobFields = fields.filter(item => item.tag === "date_of_birth")
  let obj = {}
  dobFields.forEach(item => {
    obj[item.value] = obj[item.value] === undefined
    ? 0
    : obj[item.value] + 1
  })

  let max = 0;
  let key = Object.keys(obj)[0];
  Object.keys(obj).forEach(item => { 
    if(max < obj[item]) { 
      max = obj[item]
      key = item
    }
  });
  return key;
}

const monitor = {
  classNames: {
    [true]: "fas fa-ban aml-detail stripe-icon stop-monitor",
    [false]: "fas fa-heartbeat aml-detail stripe-icon monitor"
  },
  texts: {
    [true]: "Stop Monitoring Case",
    [false]: "Monitor Case"
  }
}

const toCapitalize = str => str[0].toUpperCase() + str.slice(1, str.length)

const matchStatusRefs = {
  "unknown": "Unknown",
  "no_match": "No Match",
  "potential_match": "Potential Match",
  "false_positive": "False Positive",
  "true_positive": "True Positive",
  "true_positive_approve": "True Positive - Approve",
  "true_positive_reject": "True Positive - Reject",
  "Unknown": "unknown",
  "No Match": "no_match",
  "Potential Match": "potential_match",
  "False Positive": "false_positive",
  "True Positive": "true_positive",
  "True Positive - Approve": "true_positive_approve",
  "True Positive - Reject": "true_positive_reject"
}
class AmlDetail extends Component {

  state = {
    wasFound: true,
    hits: [
      // {
      //   name: "",
      //   matchedValues: "",
      //   countries: "",
      //   dob: "",
      //   years: "",
      //   score: "",
      //   id: ""
      // }
    ],
    is_monitored: false,
    isRightOpen: true,
    monitorLoading: false,
    monitorIconClassName: "",
    monitorText: "",
    isSearchInfo: true,
    riskOptions: [
      {
        guid: "Unknown",
        name: "Unknown"
      },
      {
        guid: "Low",
        name: "Low"
      },
      {
        guid: "Medium",
        name: "Medium"
      },
      {
        guid: "High",
        name: "High"
      }      
    ],
    riskLevel: "",
    matchStatusOptions: [
      {
        guid: "Unknown",
        name: "Unknown"
      },
      {
        guid: "No Match",
        name: "No Match"
      },
      {
        guid: "Potential Match",
        name: "Potential Match"
      },
      {
        guid: "False Positive",
        name: "False Positive"
      },
      {
        guid: "True Positive",
        name: "True Positive"
      },
      {
        guid: "True Positive - Approve",
        name: "True Positive - Approve"
      },
      {
        guid: "True Positive - Reject",
        name: "True Positive - Reject"
      }
    ],
    mainMatchStatus: "",
    comment: "",
    idNameEntityObject: {},
    checked: {},
    attachedTags: [],
    tagsValues: {},
    attachSearch: "",
    filesNames: [],
    loading: true
  }

  componentDidUpdate = (prevProps) => {
    if(!this.props.isOpenUploadModal && prevProps.isOpenUploadModal)
      this.setState({
        filesNames: []
      })
    if(!this.props.isOpenAttachModal && prevProps.isOpenAttachModal)
      this.setState({
        checked: {},
        attachedTags: [],
        tagsValues: {}
      })
  }


  async componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
    await this.props.getAllTags({})
    await this.props.getAllCaseTags({ caseId: this.props.match.params.id })
    await this.props.getAmlDetail(this.props.match.params.id);
    await this.props.getAllCaseComments({ id: this.props.match.params.id })
    let name = "";
    let hits = [];
    let id = "";
    let entityType = "";
    let isWhitelisted = "";
    let matchStatus = "";

    if(this.props.amlDetail.hits.length > 0) {
      const riskLevel = toCapitalize(this.props.amlDetail.risk_level)
      const mainMatchStatus = matchStatusRefs[this.props.amlDetail.match_status]
      this.props.amlDetail.hits.forEach(hit => {
        const matchedValues = this.getMatchedValues(hit.doc.source_notes)
        let countries = "";
        const dob = getDob(hit.doc.fields);
        hit.doc.fields.forEach(item => {
          if(item.name === "Countries")
            countries = item.value
          // else if(item.tag === "date_of_birth" && item.source === "complyadvantage")
          //   dob = item.value
        })
        const fromDate = new Date(dob);
        const toDate = new Date();
    
        const _fromDate = moment([ fromDate.getFullYear(), fromDate.getMonth() ]);
        const _toDate = moment([ toDate.getFullYear(), toDate.getMonth() ]);
        const years = _toDate.diff(_fromDate, "years")
        const score = hit.score.toFixed(2);
        name = hit.doc.name
        id = hit.doc.id
        entityType = hit.doc.entity_type
        isWhitelisted = hit.is_whitelisted
        matchStatus = hit.match_status ? hit.match_status.split("_").join(" ") : false
        hits.push({
          name,
          matchedValues,
          countries,
          dob,
          years,
          score,
          id,
          entityType,
          isWhitelisted,
          matchStatus
        })
      })

      let idNameEntityObject = {}

      hits.forEach(hit => {
        idNameEntityObject[hit.id] = hit.name
      })
      
      const monitorIconClassName = monitor.classNames[this.props.amlDetail.is_monitored];
      const monitorText = monitor.texts[this.props.amlDetail.is_monitored];

      this.setState({ 
        hits,
        is_monitored: this.props.amlDetail.is_monitored,
        monitorIconClassName,
        monitorText,
        riskLevel,
        mainMatchStatus,
        idNameEntityObject,
        loading: false
      })
    }
    else {
      name = this.props.amlDetail.search_term;
      const monitorIconClassName = monitor.classNames[this.props.amlDetail.is_monitored];
      const monitorText = monitor.texts[this.props.amlDetail.is_monitored];

      this.setState({
        wasFound: false,
        hits: [ { name } ],
        monitorIconClassName,
        monitorText,
        loading: false
      })
    }
    await this.props.getAllCaseFiles({ caseId: this.props.match.params.id })
    
  }

  handleDeleteUploadFile = (i) => {
    let { filesNames } = this.state;
    filesNames.splice(i, 1)
    this.setState({ filesNames })
  }

  handleClickOutside = (e) => {
    if (this.ref && !this.ref.contains(e.target) && !this.colorRef.contains(e.target)) {
      this.setState({ isColorOpen: false });
    }
  } 

  handleChange = e => this.setState({ [e.target.name]: e.target.value })


  matchedValue = (label, text) => (
    text.length > 0
    ? <>
      <OverlayTrigger 
        placement="bottom"
        overlay={
          <Popover>
            {text.map(item => (
              <>
                {item}
                <br/>
              </>
            ))}
          </Popover>
        }>
        <Badge className="badge-aml-matched">  
          {`${label} `}
          <i className="fas fa-info-circle" style={{ fontSize: "calc(4px + 0.5vw)" }} />
        </Badge>
      </OverlayTrigger>
      <br/>
    </>
    : <>
      <Badge className="badge-aml-matched">  
        {label}
      </Badge>
      <br/>
    </>
  )

  stripeIcon = (className, text, action) => (
    <OverlayTrigger 
      placement="left"
      overlay={
        <Popover className="aml-detail popover" >
          {text}
        </Popover>
    }>
      {Array.isArray(className)
      ? <>
        {className.map((item, i) => (
          <i className={className[i]} onClick={action[i] || (() => {})}/>
        ))}
        </>
        
      : <i className={className} onClick={action}/>}
    </OverlayTrigger>
  )

  getMatchedValues = sourceNotes => {
    let arr = []
    const mainTypes = [ "sanction", "warning", "adverse-media", "pep" ]
    Object.keys(sourceNotes).forEach(item => {
      let main = sourceNotes[item].aml_types.filter(type => mainTypes.includes(type))
      const others = sourceNotes[item].aml_types.filter(type => !mainTypes.includes(type))
      if(main.length === 0) {
        for(let i = 0; i < others.length; i++)
          for(let j = 0; j < mainTypes.length; j++)
            if(others[i].includes(mainTypes[j])) {
              main = [ mainTypes[j] ]
              break
            }
      }
      const obj = { 
        main, 
        others
      }
      if(!arr.find(item => JSON.stringify(obj) === JSON.stringify(item)))
        arr.push(obj)
    })
    return [ ...new Set(arr) ].map(item => this.matchedValue(toCapitalize(item.main[0]).split("-").join(" "), item.others.map(item => item.toUpperCase().split("-").join(" "))))
  }

  parseJson = str => {
    try {
      return JSON.stringify(str, null, "\t");
    } catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  handleClickInfo = () => {
    let { isSearchInfo, isRightOpen } = this.state;
    isRightOpen = !isSearchInfo || (!isRightOpen && isSearchInfo)
    this.setState({ 
      isRightOpen,
      isSearchInfo: true 
    })
  }

  handleClickComments = () => {
    let { isSearchInfo, isRightOpen } = this.state;
    isRightOpen = isSearchInfo || (!isRightOpen && !isSearchInfo)
    this.setState({ 
      isRightOpen,
      isSearchInfo: false 
    })
  }

  handleClickUpload = (e) => {
    let { filesNames } = this.state;
    if(e.target.files) {
      for(let i = 0; i < e.target.files.length; i++) {
        // files.append("file", e.target.files[i], e.target.files[i].name)
        const s = e.target.files[i].name.split(".")
        filesNames.push({
          name: s.slice(0, s.length - 1)[0],
          format: s[s.length - 1],
          data: e.target.files[i]
        })
      }
      this.setState({ filesNames })
    }
  }

  handleUpload = async(e) => {
    e.preventDefault();
    this.setState({ uploading: true })
    const { filesNames } = this.state;
    const emptyFile = filesNames.find(file => file.name === "")
    if(emptyFile) {
      swal({
        title: "File name error",
        text: "The file name cannot be empty",
        icon: "error",
      });
      this.setState({ uploading: false })
    }
    else {
      let files = new FormData()
      filesNames.forEach(file => {
        files.append("file", file.data, `${file.name}.${file.format}`)
      })
      try {
        await this.props.addFiles({ 
          caseId: this.props.match.params.id, 
          data: files
        })
        await this.props.showModal(false, "uploadFile");
        swal({
          title: "Status",
          text: "Ok",
          icon: "success",
          button: false,
          timer: 2000
        });
        this.setState({ uploading: false })
        await this.props.getAllCaseFiles({ caseId: this.props.match.params.id })
        this.setState({
          filesNames: []
        })
      }
      catch(error) {
        const parsedError = parseResponse(error);
        await this.props.showModal(false, "uploadFile");
        if ((error.response && error.response.status !== 409) || !error.response)
          swal({
            title: parsedError.error,
            text: parsedError.message,
            icon: "error",
          });
        this.setState({
          uploading: false,
          filesNames: []
        })
      }
    }
  }

  handleClickMonitor = async() => {
    this.setState({ monitorLoading: true });
    try {
      const { is_monitored } = this.state;
      await this.props.updateAmlSearch({
        id: this.props.match.params.id,
        is_monitored: !is_monitored 
      });
      const monitorIconClassName = monitor.classNames[!is_monitored];
      const monitorText = monitor.texts[!is_monitored];

      is_monitored 
      ? swal({
        title: "Success",
        text: "Monitor has been stopped",
        icon: "success",
        button: false,
        timer: 2000
      })
      : swal({
        title: "You are now monitoring updates on data from the ComplyAdvantage Database.",
        text: "Updates in your internal lists data remain unmonitored!",
        icon: "success",
        button: false,
        timer: 2000
      });

      this.setState({
        monitorIconClassName,
        monitorText,
        monitorLoading: false,
        is_monitored: !is_monitored
      })
    }
    catch(error) {
      this.setState({ monitorLoading: false })
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
    setTimeout(() => {
      this.setState({ monitorLoading: false })
    }, 2000)
  }

  handleRiskSelect = async option => {
    if(option.guid !== this.state.riskLevel) {
      this.setState({
        riskLoading: true
      })
      try {
        await this.props.updateAmlSearch({
          id: this.props.match.params.id,
          risk_level: option.guid.toLowerCase()
        });

        this.setState({
          riskLevel: option.guid,
          riskLoading: false
        })
        swal({
          title: "Case Management has been updated!",
          icon: "success",
          button: false,
          timer: 2000
        });
      }
      catch(error) {
        this.setState({ riskLoading: false })
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
      setTimeout(() => {
        this.setState({ riskLoading: false })
      }, 2000)
    }
  }

  handleMatchStatusSelect = async option => {
    if(option.guid !== this.state.mainMatchStatus) {
      this.setState({
        matchStatusLoading: true
      })
      try {
        await this.props.updateAmlSearch({
          id: this.props.match.params.id,
          match_status: matchStatusRefs[option.guid]
        });

        this.setState({
          mainMatchStatus: option.guid,
          matchStatusLoading: false
        })
        swal({
          title: "Case Management has been updated!",
          icon: "success",
          button: false,
          timer: 2000
        });
      }
      catch(error) {
        this.setState({ matchStatusLoading: false })
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
      setTimeout(() => {
        this.setState({ matchStatusLoading: false })
      }, 2000)
    }
  }

  handleChangeComment = e => this.setState({ comment: e.target.value })

  handleCheck = (name) => {
    const checked = this.state.checked[name];
    let { attachedTags } = this.state;
    this.setState({
      checked: {
        ...this.state.checked,
        [name]: !this.state.checked[name]
      }
    })
    if(checked)
      attachedTags = attachedTags.filter(item => item !== name)
    else
      attachedTags.push(name)
    this.setState({ attachedTags })
  }

  handleChangeValue = (name, e) => {
    let { tagsValues } = this.state;
    tagsValues[name] = e.target.value
    this.setState({ tagsValues })
  }

  handleAddComment = async() => {
    const { comment } = this.state
    this.setState({ comment: "" })
    if(comment.trim() !== "") {
      try {
        await this.props.addCommentToCase({
          id: this.props.match.params.id,
          text: comment
        })
        swal({
          title: "Comment has been added!",
          icon: "success",
          button: false,
          timer: 2000
        });
        await this.props.getAllCaseComments({ id: this.props.match.params.id })
      }
      catch(error) {
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
    }
  }

  handleDeleteFile = async(e, guid) => {
    e.preventDefault();
    try {
      const data = {
        guid,
        del: true
      }
      await this.props.addFiles({ 
        caseId: this.props.match.params.id, 
        data
      })
      await this.props.showModal(false, "uploadFile");
      swal({
        title: "Status",
        text: "Ok",
        icon: "success",
        button: false,
        timer: 2000
      });
      await this.props.getAllCaseFiles({ caseId: this.props.match.params.id })
    }
    catch(error) {
      const parsedError = parseResponse(error);
      if ((error.response && error.response.status !== 409) || !error.response)
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
    }
  }

  handleDeleteTag = async(e, guid) => {
    e.preventDefault();
    try {
      const data = [{
        guid,
        del: true
      }]
      await this.props.attachTagsToCase({ 
        caseId: this.props.match.params.id,
        data 
      })
      swal({
        title: "Status",
        text: "Ok",
        icon: "success",
        button: false,
        timer: 2000
      });
      await this.props.getAllCaseTags({ caseId: this.props.match.params.id })
    }
    catch(error) {
      const parsedError = parseResponse(error);
      if ((error.response && error.response.status !== 409) || !error.response)
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
    }
  }

  handleAttachTags = async(e) => {
    e.preventDefault();
    try {
      const { tagsValues, attachedTags } = this.state;
      let data = [];

      attachedTags.forEach(tag => {
        data.push({
          name: tag,
          value: tagsValues[tag]
        })
      })

      if(data.length > 0) {
        await this.props.attachTagsToCase({ 
          caseId: this.props.match.params.id,
          data 
        })
        await this.props.showModal(false, "attachTags");
        swal({
          title: "Status",
          text: "Ok",
          icon: "success",
          button: false,
          timer: 2000
        });
        await this.props.getAllCaseTags({ caseId: this.props.match.params.id })
        
        this.setState({
          checked: {},
          attachedTags: [],
          tagsValues: {}
        })
      }
    }
    catch(error) {
      const parsedError = parseResponse(error);
      if ((error.response && error.response.status !== 409) || !error.response)
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      this.setState({
        checked: {},
        attachedTags: [],
        tagsValues: {}
      })
    }
  }

  handleChangeName = (e, i) => {
    let { filesNames } = this.state;
    filesNames[i].name = e.target.value
    this.setState({ filesNames })
  }

  render() {
    const { attachSearch, filesNames } = this.state;

    let { tags, caseTags } = this.props;
    const _tags = tags.filter(tag => tag.name.toLowerCase().includes(attachSearch.toLowerCase()))
    const monitorClassName = this.state.monitorLoading 
    ? "fas fa-cog fa-spin aml-detail stripe-icon spin-icon"
    : this.state.monitorIconClassName;

    const searchInfoClassName = this.state.isSearchInfo
    ? "fas fa-info aml-detail stripe-icon info-selected"
    : "fas fa-info aml-detail stripe-icon info"

    const commentsClassName = !this.state.isSearchInfo
    ? "fas fa-comment aml-detail stripe-icon comments-selected"
    : "fas fa-comment aml-detail stripe-icon comments"

    const uploadClassName = "fas fa-upload aml-detail stripe-icon upload"
    const filesClassName = "fas fa-files-o aml-detail stripe-icon file"

    if (this.state.loading)
      return <Content>
        <Spinner />
      </Content>;

    return <Content className="content aml">
      <div className="aml-detail-header-wrapper">
        <div className="aml-detail-title-wrapper">
          <div className="aml-detail-title">
            Search results for "{this.props.amlDetail.search_term}"
          </div>
          <div className="aml-matches">
            Matches: <a>{`${this.state.wasFound ? this.state.hits.length : 0} `}</a> (sorted by relevance)
          </div>
        </div>
        <div className="aml-detail-header-right">
          {this.state.wasFound &&
          <div className="select risk aml-comments-cert">
            <a href={this.state.wasFound ? `https://bank.tbffinance.com/backend/api/bank/admin/aml/${this.props.match.params.id}/certificate` : ''} target='_blank'>
              <button>
                Download search certificate
              </button>
            </a>
          </div>}
          <div className="select risk">
            <Select
              label="Risk level"
              options={this.state.riskOptions}
              onSelect={this.handleRiskSelect}
              defaultValue={this.state.riskLevel}
              isLoading={this.state.riskLoading}
              iconClassname="risk-selector"
            />
          </div>
          <div className="select match-status">
            <Select
              label="Match status"
              options={this.state.matchStatusOptions}
              onSelect={this.handleMatchStatusSelect}
              defaultValue={this.state.mainMatchStatus}
              isLoading={this.state.matchStatusLoading}
              iconClassname="match-status-selector"
            />
          </div>
        </div>
      </div>
        <div 
          className="aml-detail cards-row"
        >
          <div 
            className="aml-detail cases-col"
            id={this.state.isRightOpen ? "open" : "closed"}
          >
          {this.state.wasFound
            ? this.state.hits.map((hit, i) => (
              <Col md={5}>
                <CardWhite
                  style={{
                    marginTop: "20px"
                  }}
                  entityType={hit.entityType}
                  detail={true}
                  header={<Link
                    className="link"
                    to={`/aml/${this.props.match.params.id}/entity/${hit.id}`}
                  >
                    {hit.name}
                  </Link>}
                > 
                    <AmlLabel
                      label="Matched:"
                      value={hit.matchedValues}
                      half={true}
                    />
                    <AmlLabel
                      label="Countries:"
                      value={hit.countries}
                      half={true}
                    />
                    <AmlLabel
                      label="DOB:"
                      value={`${hit.dob} (Age: ${hit.years} years old)`}
                      half={true}
                    />
                    <AmlLabel
                      label="Is Whitelisted:"
                      value={hit.isWhitelisted
                        ? <i className="fas fa-check" style={{ color: "green" }} />
                        : <i className="fas fa-close" style={{ color: "red" }} />
                      }
                      half={true}
                    />
                    <AmlLabel
                      label="Score:"
                      value={hit.score}
                      half={true}
                    />
                    {hit.matchStatus &&
                    <AmlLabel
                      label="Match Status:"
                      value={toCapitalize(hit.matchStatus)}
                      half={true}
                    />}
                    {/* <i
                      className="fas fa-upload"
                      style={{
                        color: "green",
                        cursor: "pointer",
                        marginLeft: "95%"
                      }}
                      onClick={() => exportAml(this.props.amlDetail.hits[i], `${hit.name}-${this.props.amlDetail.updated_at}`)}
                    /> */}
                    {/* <Modal
                      content={<div 
                          className="card-shop-secret" 
                          style={{ 
                            whiteSpace: "pre-wrap", 
                            padding: "30px",
                            maxWidth: "100%" 
                          }}
                        >
                          {this.parseJson(this.props.amlDetail)}
                        </div>}
                      button={
                        <button
                          className="btn btn-export"
                        >
                          Show response
                        </button>
                      }
                      dialogClassName="modal-wide"
                    /> */}
                    
                </CardWhite>
              </Col>))
            : <Col md={7}>
                <Badge
                  pill 
                  style={{ 
                    // position: "absolute", 
                    // left: "calc(50% - 77px)", 
                    fontSize: "1.2vw",
                    marginTop: "2vw",
                    width: "60%",
                    marginLeft: "60%",
                    marginRight: "50%",
                    paddingTop: "10px",
                    paddingBottom: "10px"
                  }} 
                >
                  0 matches found
              </Badge>
            </Col>}
        </div>
        <div
          className="aml-detail col-stripe"
        >
          <div
            className="aml-detail stripe"
          >
            {this.stripeIcon(searchInfoClassName, "Search Information", this.handleClickInfo)}
            {this.stripeIcon(commentsClassName, "Comments", this.handleClickComments)}
            <Modal
              header="Upload Files"
              state="uploadFile"
              headerClassName="modal-header-aml"
              bodyClassName="modal-body-aml"
              content={<div>
                {this.state.uploading 
                ? <Spinner style={{ height: "100px" }}/>
                : <>
                  <div className="aml-documents">
                    <input type="file" class="form-control" multiple onChange={this.handleClickUpload} />
                  </div>
                  <div style={{ marginTop: "20px" }}>
                    {filesNames.map((fileName, i) => (
                      <div className="aml-input-file">
                        <a className="trash" onClick={() => this.handleDeleteUploadFile(i)}>
                          <i className="fas fa-trash" /* onClick={} *//>
                        </a>
                        <span className="input-span-wrapper">
                          <input 
                            value={this.state.filesNames[i].name} 
                            onChange={(e) => this.handleChangeName(e,i)} 
                            // placeholder="wtf"
                            style={
                              this.state.filesNames[i] 
                              ? {
                                width: ((this.state.filesNames[i].name.length + 1) * 8.1) + "px"
                              }
                              : {}
                            }
                          />
                        </span>
                        <a className="file-format" >.{fileName.format}</a>
                      </div>
                    ))}
                  </div>
                  <div className="aml-save-button-wrapper" style={{ marginTop: "50px" }}>
                    <button className="aml-save-button" onClick={this.handleUpload}>
                      Save
                    </button>
                  </div>
                </>}
              </div>}
              button={this.stripeIcon(uploadClassName, "Upload files", () => {})}
            />
            {this.stripeIcon(monitorClassName, this.state.monitorText, this.handleClickMonitor)}
            <Modal
              header="Files"
              state="files"              
              headerClassName="modal-header-aml"
              bodyClassName="modal-body-aml"
              content={<div>
                {this.props.loadingCaseFiles
                ? <Spinner style={{ height: "100px" }}/>
                : <>
                  {this.props.caseFiles.length > 0 
                    ? this.props.caseFiles.map(file => (
                      <div className="aml-documents-files">
                        <a className="download" download={file.name} href={"data:application/octet-stream;base64,"+file.data} >
                          <i className="fas fa-download download-asset"/>
                        </a>
                        <a className="name">{file.name}</a>
                        <a className="trash">
                          <i className="fas fa-trash" onClick={(e) => this.handleDeleteFile(e, file.guid)}/>
                        </a>
                      </div>))
                    : <div style={{ textAlign: "center", fontSize: "20px", fontWeight: "500" }}>
                      There are no files yet
                    </div> }
                </>}
                </div>}
              button={this.stripeIcon(filesClassName, "Show files", () => {})}
            />
          </div>
        </div>
        {this.state.isRightOpen &&
        (this.state.isSearchInfo 
        ? <div className="aml-search-info wrap">
          <div className="aml-search-info head">
            Search information
          </div>
          <div className="aml-search-info col">
            <AmlLabel
              label="Search term:"
              value={this.props.amlDetail.search_term}
            />
            <AmlLabel
              label="Created at:"
              value={moment(this.props.amlDetail.created_at).format("MMM DD, YYYY hh:mm A")}
            />
            <AmlLabel
              label="Search ref:"
              value={this.props.amlDetail.ref}
            />
            {this.props.amlDetail.client_ref &&
            <AmlLabel
              label="Client ref:"
              value={this.props.amlDetail.client_ref}
            />}
            <AmlLabel
              label="Fuzziness interval:"
              value={this.props.amlDetail.filters.fuzziness*100 + "%"}
            />
            {this.props.amlDetail.filters.birth_year &&
            <AmlLabel
              label="Year of birth:"
              value={this.props.amlDetail.filters.birth_year}
            />}
            <AmlLabel
              label="Entity type:"
              value={this.props.amlDetail.filters.entity_type}
            />
          </div>
          {tags.length > 0 &&
          <>
            <div className="aml-search-info tags-head">
              Tags
            </div>
            <hr className="aml-search-info-hr"/>
            <Modal
              header="Attach Tag"
              state="attachTags"
              headerClassName="modal-header-aml"
              bodyClassName="modal-body-aml"
              content={<div>
                <div className="aml-attach-tag-search-wrapper">
                  <div className="aml-attach-tag-search-icon">
                    <i className="fa fa-search" />
                  </div>
                  <div className="aml-attach-tag-search-input">
                    <Input 
                      placeholder="Search tags..." 
                      onChange={this.handleChange} 
                      name="attachSearch"
                      value={this.state.attachSearch}
                    />
                  </div>
                </div>
                <div className="aml-attach-tag-col">
                  {_tags.map((tag, i) => (
                    <>
                      <div className="aml-attach-tag-row">
                        <div className="aml-attach-tag-checkbox">
                          <input 
                            id={i}
                            type="checkbox" 
                            className="checkbox" 
                            checked={this.state.checked[tag.name]} 
                            onClick={() => this.handleCheck(tag.name)}
                          />
                        </div>
                        <div className="aml-attach-tag-tag">
                          <AmlTag 
                            tag={tag.name}     
                            color={tag.color}    
                            htmlFor={i}
                            style={{
                              cursor: "pointer"
                            }}
                          />
                        </div>
                        <div className="aml-attach-tag-value">
                          <Input 
                            placeholder={!this.state.checked[tag.name] ? "" :"Enter tag value"} 
                            id={!this.state.checked[tag.name] ? "disabled" : ""}
                            disabled={!this.state.checked[tag.name]}
                            onChange={(e) => this.handleChangeValue(tag.name, e)}
                            value={this.state.tagsValues[tag.name]}
                          />
                        </div>
                      </div>
                      {(i !== _tags.length - 1) && <hr className="aml-attach-tag-hr" />}
                    </>))}
                </div>
                <div className="aml-save-button-wrapper" style={{ marginTop: "50px" }}>
                  <button className="aml-save-button" onClick={this.handleAttachTags}>
                    Attach
                  </button>
                </div>
              </div>}
              button={
                <button 
                  className="aml-search-info tags-attach-button"
                >
                  <i className="fas fa-plus" />
                  <a>Attach Tag</a>
                </button>
              }
            />
            <div className="aml-tags-table-header">
              <div className="aml-tags-table-tag">
                Tag
              </div>
              <div className="aml-tags-table-value">
                Value
              </div>
              <div className="aml-tags-table-delete"/>
            </div>
            {caseTags.map(tag => (
              <div className="aml-tags-table-body">
                <div className="aml-tags-table-bodytag">
                  <AmlTag
                    tag={tag.tag_name}
                    color={tags.find(i => i.name === tag.tag_name) ? tags.find(i => i.name === tag.tag_name).color : ""}
                    style={{ height: "30px", lineHeight: "30px", display: "inline-block", paddingRight: "8px" }}
                  />
                </div>
                <div className="aml-tags-table-bodyvalue">
                  {tag.value}
                </div>
                <div className="aml-tags-table-bodydelete">
                  <i className="fas fa-trash" onClick={(e) => this.handleDeleteTag(e, tag.guid)} />
                </div>
              </div>))}
          </>}
        </div>
        : <div className="aml-comments wrap">
          <div className="aml-comments head">
            Comments
          </div>
          <div className="aml-comments col">
            {this.props.caseComments.map(item => (
              <CommentItem
                name={item.username}
                date={item.date}
                entityId={item.entity_id}
                caseName={this.state.idNameEntityObject[item.entity_id]}
                caseId={item.case_id}
                text={item.text}
              />
            ))}
          </div>
          <div className="aml-comments-input">
            <textarea 
              placeholder="Type a message to comment on the whole case"
              onChange={this.handleChangeComment}
            />
            <button 
              onClick={this.handleAddComment}
              className={this.state.comment.trim() === "" && "disabled"}
            >
              Add
            </button>
          </div>
        </div>)}
      </div>
    </Content>;
  }
}

const mapStateToProps = (state) => {
  return {
    tags: state.aml.tags,
    caseTags: state.aml.caseTags,
    amlDetail: state.aml.amlDetail,
    loading: state.aml.loadingDetail/* && state.aml.loadingCaseComments*/,
    loadingCaseFiles: state.aml.loadingCaseFiles,
    response: state.aml.updateAmlDetailResponse,
    caseComments: state.aml.caseComments,
    commentResponse: state.aml.addCaseCommentResponse,
    caseFiles: state.aml.caseFiles,
    addFilesResponse: state.aml.addFilesResponse,
    isOpenUploadModal: state.modal.uploadFile,
    isOpenAttachModal: state.modal.attachTags    
  };
};

export default connect(mapStateToProps, {
  getAmlDetail,
  getAllCaseFiles,
  addFiles,
  updateAmlSearch,
  getAllCaseComments, 
  addCommentToCase,
  getAllTags, 
  getAllCaseTags, 
  attachTagsToCase,
  showModal
})(AmlDetail);

AmlDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  loadingDetail: PropTypes.bool,
  getAmlDetail: PropTypes.func,
  updateAmlSearch: PropTypes.func,
  amlDetail: PropTypes.object
};