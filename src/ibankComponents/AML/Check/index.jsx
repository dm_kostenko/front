import React, { Component } from "react";
import { Row, Col, Button, ControlLabel, FormControl } from "react-bootstrap";
import { searchInAml } from "ibankActions/aml";
import { connect } from "react-redux";
import { countries } from "helpers/countryArray";
import Card from "ibankComponents/UI/Card";
import Input from "ibankComponents/UI/Input";
import Select from "ibankComponents/UI/MultiSelect";
import Radio from "ibankComponents/UI/Radio";
import Dropdown from "ibankComponents/UI/Dropdown";
import Checkbox from "ibankComponents/UI/Checkbox";
import MultiSelect from "ibankComponents/UI/MultiSelect";
import { parseResponse } from "helpers/parseResponse";
import swal from "sweetalert";


const _selectedCountries = [
  "All",
  // ...countries.map(item => item.code)
]

class AmlCheck extends Component {
  state = {
    width: 0,
    selectCount: 0,
    terms: [
      {
        name: "",
        lastName: "",
        reference: "",
        year: "",
        countriesOptions: [],
        selectedCountries: _selectedCountries,
        selectedOptions: [],
        willMonitored: true
      }
    ],
    entityTypes: [
      "Person",
      "Company"
    ],
    entityType: "Person",
    fuzziness: "100% Broad Match",
    fuzzinessOptions: [
      {
        guid: "100",
        name: "100% Broad Match"
      },
      {
        guid: "90",
        name: "90%"
      },
      {
        guid: "80",
        name: "80%"
      },
      {
        guid: "70",
        name: "70%"
      },
      {
        guid: "60",
        name: "60%"
      },
      {
        guid: "50",
        name: "50%"
      },
      {
        guid: "40",
        name: "40%"
      },
      {
        guid: "30",
        name: "30%"
      },
      {
        guid: "20",
        name: "20%"
      },
      {
        guid: "10",
        name: "10%"
      },
      {
        guid: "0",
        name: "0%"
      },
      {
        guid: "Exact",
        name: "Exact Match"
      },    
    ]
  }

  componentDidMount = () => {
    this.updateWidth();
    window.addEventListener("resize", this.updateWidth);
    let { terms } = this.state;
    const { countriesOptions, selectedOptions } = this.getCountriesOptions(terms[0].selectedCountries);
    terms[0].countriesOptions = countriesOptions
    terms[0].selectedOptions = selectedOptions
    this.setState({ terms })
  }

  updateWidth = () => {
    this.setState({
      width: window.outerWidth
    });
  }

  getCountriesOptions = (selectedCountries) => {
    const countriesOptions = [
      ...[
        {
          name: "All",
          code: "All"
        },
        ...countries
      ].map(item => {
        item.checked = selectedCountries.includes(item.code)
        return {
          checked: item.checked,
          code: item.code,
          // value: item.name,
          // label: this.getCountryOptionObject(item)
          guid: item.name,
          name: this.getCountryOptionObject(item)
        }
      })
    ]

    const selectedOptions = countriesOptions.filter(o => o.checked)

    return {
      countriesOptions,
      selectedOptions
    }
  }

  handleChangeMonitored = (e, i) => {
    let { terms } = this.state;
    terms[i].willMonitored = !terms[i].willMonitored
    this.setState({ terms })
  }

  handleAdd = () => {
    let { terms } = this.state;
    const selectedCountries = [
      "All",
      // ...countries.map(item => item.code)
    ]
    const { countriesOptions, selectedOptions } = this.getCountriesOptions(selectedCountries);

    terms.push({
      name: "",
      reference: "",
      year: "",
      countriesOptions,
      selectedOptions,
      selectedCountries,
      willMonitored: true
    })
    this.setState({ terms })
  }

  handleDelete = (e) => {
    let { terms } = this.state;
    const { id } = e.target;
    terms.splice(id, 1);
    this.setState({ terms })
  }

  handleChange = (e, i) => {
    let { terms } = this.state;
    const { name, value } = e.target;
    terms[i][name] = value;
    this.setState({ terms })
  }

  handleRadio = (e) => {
    const { entityType } = this.state;
    if(this.state.entityTypes[e.target.value] !== entityType) {
      let term = {
        name: "",
        lastName: "",
        reference: "",
        year: "",
        countriesOptions: [],
        selectedCountries: _selectedCountries,
        selectedOptions: [],
        willMonitored: true
      }
      const { countriesOptions, selectedOptions } = this.getCountriesOptions(term.selectedCountries);
      term.countriesOptions = countriesOptions
      term.selectedOptions = selectedOptions
      this.setState({
        terms: [ term ]
      })
    }
    this.setState({
      entityType: this.state.entityTypes[e.target.value]
    })
  }

  handleOptionClick = (options, i) => {
    options = options.length > 0 ? options : [{ code: "All" }]
    const allOption = options.find(item => item.code === "All");
    let selectedCountries = options.map(item => item.code)
    const isAllInSelected = this.state.terms[i].selectedCountries.find(item => item === "All")

    // first click on ALL
    if(allOption && !isAllInSelected)
      selectedCountries = [
        allOption.code,
        // ...countries.map(item => item.code)
      ]
    // click when ALL is checked
    else if(allOption && isAllInSelected)
      // not all items are checked
      if(options.length < countries.length + 1) {
        selectedCountries = selectedCountries.filter(code => code !== "All")
        if(selectedCountries.length === 0)
          selectedCountries = [ "All" ]
      }
      // all items are checked
      else
        selectedCountries = selectedCountries
    // uncheck ALL
    else if(!allOption && isAllInSelected)
      selectedCountries = [
        allOption.code
      ]
    else if(!allOption && !isAllInSelected)
      if(options.length === countries.length)
        selectedCountries.push("All")
    let { terms } = this.state;
    terms[i].selectedCountries = selectedCountries
    const { countriesOptions, selectedOptions } = this.getCountriesOptions(selectedCountries);
    terms[i].countriesOptions = countriesOptions
    terms[i].selectedOptions = selectedOptions
    this.setState({ terms })
  }

  getCountryOptionObject = (item) => (
    item.checked 
    ? item.name
    : item.code === "All"
      ? <span className="aml-selector-option">
          <span
            style={{
              fontWeight: "400"
            }}
          >
            All
          </span>
        </span>
      : <span className="aml-selector-option">
          <img src={`https://www.countryflags.io/${item.code.toLowerCase()}/flat/16.png`} />
          <span style={{
            marginLeft: "calc(5px + 0.5vw)",
            fontWeight: "400"
          }}>
            {item.name}
          </span>
        </span>
  )

  handleSelectFuzziness = option => {
    this.setState({ fuzziness: option.name })
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let {
        terms,
        entityType,
        fuzziness
      } = this.state;

      terms.forEach(term => {
        delete term.countriesOptions;
        delete term.selectedOptions;
        if(term.selectedCountries.find(c => c === "All")) 
          delete term.selectedCountries
      })
      const request = {
        terms,
        entityType,
        fuzziness: fuzziness === "Exact Match" ? "exact" : parseFloat(fuzziness.replace(/\D+/g,"")) / 100
      };
      await this.props.searchInAml(request);
      swal({
        title: "Status",
        text: "Ok",
        icon: "success"
      });
      this.setState({
        terms: [
          {
            name: "",
            reference: "",
            year: "",
            countriesOptions: [],
            selectedCountries: [
              "All",
              // ...countries.map(item => item.code)
            ],
            selectedOptions: [],
            willMonitored: true
          }
        ],
      })
      window.location.replace(process.env.PUBLIC_URL + "#/aml");

    }
    catch(error) {
      const parsedError = parseResponse(error);
      if ((error.response && error.response.status !== 409) || !error.response)
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      this.setState({
        terms: [
          {
            name: "",
            reference: "",
            year: "",
            countriesOptions: [],
            selectedCountries: [
              "All",
              // ...countries.map(item => item.code)
            ],
            selectedOptions: [],
            willMonitored: true
          }
        ],
      })
    }
  }

  render() {
    const selectorStyles = {
      placeholder: (styles) => {
        return {
          ...styles,
          color: "black",
          fontSize: "calc(8px + 0.4vw)"
        };
      },
      control: (styles, state) => {
        return {
          ...styles,
          background: "rgba(255,255,255,0.4) !important",
          transition: ".8s all",
          boxShadow: "none !important",
          "&:hover": {
            transition: ".8s all"
          }
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          whiteSpace: "pre-line !important"
        };
      },
      option: (styles) => (
        {
          ...styles,
          textAlign: "left"
        }
      )
    };

    const multiSelectStyles = {
      option: (styles) => ({
        ...styles,
        minHeight: "50px !important",
        // height: "calc(10% - 5vw) !important",
        // position: "absolute",
        left: "calc(5px + 0.5vw)"
      })
    }

    return (
      <>
        <Row className="row">
          <Col md={12}>
            <Card
              header="AML search"
            >
              <Row>
                <Col md={4}>
                  <ControlLabel id="aml-header" className="col-label">
                    Name and details
                  </ControlLabel>
                  <Button 
                    className="submit-button small-aml"
                    onClick={this.handleAdd}
                  >
                    Add new search term
                  </Button>
                </Col>
              </Row>
              {this.state.terms.map((term, i) => (
                this.state.entityType === "Person"
                ? <Row 
                    className="row-search"
                    style={ this.state.width > 1000 
                      ? { display: "flex", flexDirection: "row" }
                      : {}
                    }>
                      <Col md={3} className="aml-check-lastname">
                        <Input
                          label="Last name*"
                          name="lastName"
                          value={this.state.terms[i].lastName}
                          onChange={(e) => this.handleChange(e, i)}
                        />
                      </Col>
                      <Col md={3} className="aml-check-name">
                        <Input
                          label="Name*"
                          name="name"
                          value={this.state.terms[i].name}
                          onChange={(e) => this.handleChange(e, i)}
                        />
                      </Col>
                      {/* <Col md={3}>
                        <Input
                          label="Client reference (optional)"
                          name="reference"
                          value={this.state.terms[i].reference}
                          onChange={(e) => this.handleChange(e, i)}
                        />
                      </Col> */}
                      <Col md={2} className="aml-check-year">
                        <Input
                          label="Year of birth"
                          name="year"
                          value={this.state.terms[i].year}
                          onChange={(e) => this.handleChange(e, i)}
                        />
                      </Col>
                      <Col md={3} className="aml-check-country">
                        <ControlLabel
                          className="col-label"
                          style={{ textTransform: "none" }}
                        >
                          Country
                        </ControlLabel>
                        {/* <Dropdown
                          id={i}
                          options={this.state.terms[i].countriesOptions || []}
                          selectedOptions={this.state.terms[i].selectedOptions || []}
                          onClick={(options) => this.handleOptionClick(options, i)}
                        /> */}
                        <MultiSelect
                          options={this.state.terms[i].countriesOptions || []}
                          multi={true}
                          onSelect={(options) => this.handleOptionClick(options, i)}
                          value={this.state.terms[i].selectedOptions || []}
                          isSearchable={true}
                          styles={multiSelectStyles}
                        />
                      </Col>
                      <Col md={2} className="aml-check-checkbox">
                        <Checkbox
                          // key={i}
                          label="Ongoing monitoring"
                          // data-attr={privilege}
                          onChange={(e) => this.handleChangeMonitored(e, i)}
                          checked={this.state.terms[i].willMonitored || false}
                          containerClassname="aml-checkbox-wrapper"
                          colClassname="col-checkbox-aml"
                          style={{
                            label: {
                              textTransform: "none"
                            }
                          }}
                        />
                      </Col>
                      {this.state.terms.length > 1 &&
                      <Col md={0.5} className="col">
                        <Button 
                          id={i}
                          className="delete-aml" 
                          style={
                            this.state.width > 1000 
                            ? { marginTop: "25px" } 
                            : {} }
                          onClick={this.handleDelete}
                        >
                          <i className="far fa-trash-alt" style={{ color: "white" }}/>
                        </Button>
                      </Col>
                      }
                    </Row>
                  : <Row style={ this.state.width > 1000 
                    ? { display: "flex", flexDirection: "row" }
                    : {}
                  }>
                    <Col md={3}  className="aml-check-name">
                      <Input
                        label="Company name*"
                        name="name"
                        value={this.state.terms[i].name}
                        onChange={(e) => this.handleChange(e, i)}
                      />
                    </Col>
                    <Col md={3}  className="aml-check-year">
                      <Input
                        label="Year of foundation"
                        name="year"
                        value={this.state.terms[i].year}
                        onChange={(e) => this.handleChange(e, i)}
                      />
                    </Col>
                    <Col md={3}  className="aml-check-country">
                      <ControlLabel
                        className="col-label"
                        style={{ textTransform: "none" }}
                      >
                        Country
                      </ControlLabel>
                      <MultiSelect
                        options={this.state.terms[i].countriesOptions || []}
                        multi={true}
                        onSelect={(options) => this.handleOptionClick(options, i)}
                        value={this.state.terms[i].selectedOptions || []}
                        isSearchable={true}
                        styles={multiSelectStyles}
                      />
                    </Col>
                    <Col md={2} className="aml-check-checkbox">
                      <Checkbox
                        // key={i}
                        label="Ongoing monitoring"
                        // data-attr={privilege}
                        onChange={(e) => this.handleChangeMonitored(e, i)}
                        checked={this.state.terms[i].willMonitored || false}
                        containerClassname="aml-checkbox-wrapper"
                        colClassname="col-checkbox-aml"
                        style={{
                          label: {
                            textTransform: "none"
                          }
                        }}
                      />
                    </Col>
                    {this.state.terms.length > 1 &&
                    <Col md={0.5} className="col">
                      <Button 
                        id={i}
                        className="delete-aml" 
                        style={
                          this.state.width > 1000 
                          ? { marginTop: "25px" } 
                          : {} }
                        onClick={this.handleDelete}
                      >
                        <i className="far fa-trash-alt" style={{ color: "white" }}/>
                      </Button>
                    </Col>
                    }
                  </Row>))}
                <hr className="hr-aml"/>
                <Row>
                  <Col md={4}>
                    <ControlLabel id="aml-header" className="col-label">
                      Entity type
                    </ControlLabel>
                  </Col>              
                </Row>
                <Row>
                  {this.state.entityTypes.map((type, id) => (
                    <Col md={12}>
                      <Radio
                        number={100+id}
                        option={id}
                        name="radio"
                        label={type}
                        onChange={this.handleRadio}
                        checked={type === this.state.entityType}
                      />
                    </Col>
                  ))}
                </Row>
                <hr className="hr-aml"/>
                <Row>
                  <Col md={4}>
                    <ControlLabel id="aml-header" className="col-label">
                      Fuzziness
                    </ControlLabel>
                  </Col>              
                </Row>
                <Row>
                  <Col md={2}>
                    <ControlLabel
                      className="col-label"
                      style={{ textTransform: "none" }}
                    >
                      Fuzziness interval
                    </ControlLabel>
                    <Select
                      options={this.state.fuzzinessOptions}
                      styles={selectorStyles}
                      onChange={this.handleSelectFuzziness}
                      defaultValue={this.state.fuzziness}
                    />
                  </Col>
                </Row>
                <Row
                  className="row"
                >
                  <Col md={12}>
                    <Button
                      id="transaction-form-button"
                      onClick={this.handleSubmit}
                    >
                      Search
                    </Button>
                  </Col>
                </Row>
            </Card>
          </Col>
        </Row>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    responses: state.aml.responses,
  };
};

export default connect(mapStateToProps, {
  searchInAml
})(AmlCheck);