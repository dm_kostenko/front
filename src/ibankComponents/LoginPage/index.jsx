import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Button } from "react-bootstrap";
import { logIn } from "ibankActions/auth";
import PropTypes from "prop-types";
import Loading from "ibankComponents/UI/Spinner";
import Modal from "ibankComponents/UI/Modal/Confirmation";
import { getUserData } from "services/paymentBackendAPI/backendPlatform";
import { showModal } from "ibankActions/modal";
import { getQR } from "ibankActions/auth";
import AuthTypeConfirmation from "ibankComponents/AuthTypeConfirmation";
import config from "config";



class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: "",
      password: "",
      otp: "",
      selectedType: "login-password",
      failedTimes: 0,
      timesToCheck: 3,
      showPassword: false,
      showOtp: false,
      authFail: false,
      error: "",
      note: "",
      isOtp: false,
      auth: false,
      loading: false
    };

    this.handleLoginChange = this.handleLoginChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

    handleShowPassword = () => {
      this.setState({
        showPassword: !this.state.showPassword
      });
    }

    handleShowOtp = () => {
      this.setState({
        showOtp: !this.state.showOtp
      });
    }

    handleLoginChange = (event) => {
      this.setState({
        login: event.target.value,
        authFail: false,
        note: ""
      });
    };

    handlePasswordChange = (event) => {
      this.setState({
        password: event.target.value,
        authFail: false,
        note: ""
      });
    };

    handleOtpChange = (event) => {
      this.setState({
        otp: event.target.value,
        authFail: false,
        note: ""
      });
    };

    handleSubmit = async(e) => {
      e.preventDefault();
      this.setState({ 
        loading: true,
        note: "",
        error: ""
      });

      let request = { 
        username: this.state.login,
        password: this.state.password,
      };

      if(this.state.isOtp)
        request["otp"] = this.state.otp;

      await this.props.logIn(request);
      
      console.log('RESPONSE')
      console.log(this.props.response)
      if(this.props.response && this.props.response.accessToken && this.props.response.refreshToken) {
        localStorage.removeItem("firstTimeLogin");
        localStorage.removeItem("expiresPassword");
        localStorage.removeItem("expiredPassword");
        // const { auth_type, type } = getUserData();
        const auth_type = this.props.response.auth_type || "login-password"
        if(auth_type === "login-password-otp"/*  && type !== "admin" */ && config.tfa && this.props.response.status && this.props.response.status.includes("Please, generate and confirm new secret")) {
          setTimeout(async() => {             
            await this.props.showModal(true, "profile");
            await this.props.getQR();
          }, 100);
        }
        else {
          this.setState({ auth: true });
          localStorage.setItem("isLoggedIn", true);
          window.location.replace(process.env.PUBLIC_URL || "/");
        }
      }
      else if(this.props.response && this.props.response.status === "One time password required") {
        this.setState({
          isOtp: true,
          loading: false
        });
      }
      else if (this.props.response && this.props.response.first_time_login && this.props.response.token) {
        this.setState({ auth: true });
        const url = `#/recovery_password/${this.props.response.token}`
        window.location.replace(process.env.PUBLIC_URL + url || url);
      }
      else if (this.props.response && this.props.response.credentials_expire_after && this.props.response.credentials_expired === false) {
        this.setState({ auth: true });
        // localStorage.setItem("expiresPassword", true);
        // window.location.replace(process.env.PUBLIC_URL + "#/expires_password" || "#/expires_password");
        const url = `#/recovery_password/${this.props.response.token}`
        window.location.replace(process.env.PUBLIC_URL + url || url);
      }
      else if (this.props.response && this.props.response.credentials_expired) {
        this.setState({ auth: true });
        // localStorage.setItem("expiredPassword", true);
        // window.location.replace(process.env.PUBLIC_URL + "#/expired_password" || "#/expired_password");
        const url = `#/recovery_password/${this.props.response.token}`
        window.location.replace(process.env.PUBLIC_URL + url || url);
      }
      else {
        let state = {
          authFail: true, 
          error: this.props.error, 
          loading: false,
          failedTimes: this.state.failedTimes,
          otp: "",
          note: ""
        };
        if(this.state.isOtp)
          state.failedTimes++;
        if([ "Code is expired", "Requests limit exceeded" ].includes(this.props.error) || state.failedTimes >= this.state.timesToCheck)
          state = {
            ...state,
            isOtp: false,
            failedTimes: 0,
            login: "",
            password: "",
            error: "Requests limit exceeded"
          };
        this.setState({ 
          ...state         
        });
      }
    };


    validateForm() {     
      if(!this.state.isOtp)
        return this.state.login.length > 0 && this.state.password.length > 0;
      else
        return this.state.login.length > 0 && this.state.password.length > 0 && this.state.otp.length > 0;
    }

    handleButtonRender() {
      return this.state.loading 
        ? <button className="button-submit-loading" disabled>
          <Loading color="white" />
        </button> 
        : <button 
          className="button-submit" 
          type="submit" 
          disabled={!this.validateForm()}
        >
            SIGN IN
        </button>;
    }


    render() {
      return (
        <div className="LoginPage">          
          <div className="login-page">
            <div className="form" >
              <form className="login-form" onSubmit={this.handleSubmit} autoComplete="off">
                <p className="header-login"/>
                <div>
                  {!this.state.isOtp &&
                  <>                  
                  <div 
                    id="login-input"
                    className="login"
                  >
                    <i className="fa fa-user icon" />
                    <input type="text" placeholder="login" onChange={this.handleLoginChange} name="login" value={this.state.login} />
                  </div>
                  <div
                    id="password-input"
                    className="password-open"
                  >
                    <i 
                      className={"fa fa-lock icon"}
                    />
                    <input                      
                      type={this.state.showPassword ? "text" : "password"} 
                      placeholder="password" 
                      onChange={this.handlePasswordChange} 
                      name="password"
                      value={this.state.password}
                      disabled={!this.state.selectedType.includes("password") ? "true" : ""}
                    />
                    <Button className="login-page-eye" onClick={this.handleShowPassword}>
                      {this.state.showPassword
                        ? <span className="fa fa-eye-slash"/>
                        : <span className="fa fa-eye"/>
                      }
                    </Button>
                  </div>   
                    </>  }    
                  <div 
                    id="otp-input" 
                    className={this.state.isOtp ? "otp-open" : "otp-closed"}
                  >
                    <i className="fa fa-key icon" />
                    <input 
                      type={this.state.showOtp ? "text" : "password"}                        
                      placeholder="verification code" 
                      onChange={this.handleOtpChange} 
                      value={this.state.otp}
                      name="otp" 
                    />
                    <Button 
                      className="login-page-eye-otp" 
                      onClick={this.handleShowOtp}
                    >
                      {this.state.showOtp
                        ? <span className="fa fa-eye-slash"/>
                        : <span className="fa fa-eye"/>}
                    </Button>
                  </div>       
                  {!this.state.isOtp && 
                  <div className="forgot-password">
                    <Link className="link" to={"/forgot"} style={{ color:"#989898" }} tabIndex="4">
                      <span>Forgot password?</span>
                    </Link>
                  </div>}                                
                  <p className="login-page-note">
                    {this.state.note}
                  </p>
                  { this.handleButtonRender() }                                                    
                  {this.state.authFail &&
                    <p className="access-denied">
                      {this.state.error}
                    </p>}  
                </div>
              </form>              
              <Modal
                header="Confirmation"
                content={<AuthTypeConfirmation/>}
                state="profile"
              />
            </div>
          </div>
        </div>
      );
    }

}

const mapStateToProps = (state) => {
  return {
    response: state.auth.response,
    error: state.auth.error,
  };
};

export default connect (mapStateToProps, {
  logIn,
  showModal,
  getQR
})(LoginPage);

LoginPage.propTypes = {
  response: PropTypes.bool,
  error: PropTypes.string,
  logIn: PropTypes.func
};