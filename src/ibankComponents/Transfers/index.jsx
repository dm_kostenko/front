import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../ibankFactories/Table";
import { getTxs } from "ibankActions/transactions";
import { editTxStatus, sendRequestForOtp } from "ibankActions/transactions";
import { searchInTxs, reset } from "ibankActions/search";
import { reset as resetResponse } from "ibankActions/confirmation";
import moment from "moment";
import { isIndividualWithEntities, getUserData } from "services/paymentBackendAPI/backendPlatform";
import { getCETDate } from "helpers/target2Timing";

const isIndividual = isIndividualWithEntities();
const data = getUserData();
let username = "";
if(data)
  username = data.username;

const dict = {
  "FFCCTRNS": "External transfer",
  "FFPCRQST": "Payment cancellation",
  "PRTRN": "Payment return",
  "ROINVSTG": "Resolution of investigation",
  "INTERNAL": "Internal transfer"
};

const columns = [
  {
    path: "guid",
    label: "ID"
  },
  { 
    path: "created_at", 
    label: "Timestamp",
    content: transfer => moment(getCETDate(new Date(transfer.created_at))).format("DD.MM.YYYY HH:mm:ss")
  },
  {
    path: "type",
    label: "Type",
    content: transfer => (
      <div>{dict[transfer.type]}</div>
    )
  },
  { 
    path: "status", 
    label: "Status" 
  },
  {
    path: "value",
    label: "Amount",
    content: transfer => transfer.to_amount === null 
      ? ( `${transfer.value.toFixed(2)} ${transfer.currency}` )
      : ( `${transfer.from_amount.toFixed(2)} ${transfer.from_currency_code} -> ${transfer.to_amount.toFixed(2)} ${transfer.to_currency_code}` )
  },  
  {
    path: "commission",
    label: "Fee",
    content: tx => tx.commission > 0 ? `${tx.commission} EUR` : ""
  },  
  {
    path: "sender_iban",
    label: "Sender",
    content: transfer => (
      isIndividual
        ? <div>
          {transfer.sender_iban}
          <br/>
          {transfer.sender_username &&
        <b>
          ({username === transfer.sender_username ? "You" : transfer.sender_username})
        </b>}
        </div>
        : transfer.sender_iban
    )
  },
  {    
    path: "receiver_iban",
    label: "Receiver",
    content: transfer => (
      isIndividual
        ? <div>
          {transfer.receiver_iban}
          <br/>
          {transfer.receiver_username &&
        <b>
          ({username === transfer.receiver_username ? "You" : transfer.receiver_username})
        </b>}
        </div>
        : transfer.receiver_iban
    )
  },
  {
    key: "approve/cancel",
    label: "Approve / Cancel"
  }  
];

const mapStateToProps = (state, props) => {
  return {
    data: state.transactions.ALL,
    count: state.transactions.countALL,
    searchData: state.search.ALLSearch,
    directionData: { ...props.directionData },
    isSearch: state.search.isSearch,
    loading: state.transactions.loadingALL,
    // generateDocResponse: state.documents.docResponse,
    editTxStatusResponse: state.transactions.responseTransactionStatus,
    responseOtp: state.transactions.responseOtp,
    confirmation: state.confirmation.confirmation,
    name: "transactions",
    type: "all",
    columns
  };
};

export default connect(mapStateToProps, {
  get: getTxs,
  search: searchInTxs,
  reset,
  resetResponse,
  editTxStatus,
  sendRequestForOtp
})(AbstractComponent);