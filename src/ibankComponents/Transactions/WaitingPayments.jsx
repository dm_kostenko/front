import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../ibankFactories/Table";
import { getAllTxTemplates, changeWaitingCheckbox, upsertTxsTemplates } from "ibankActions/transactions";
import { editTxStatus } from "ibankActions/transactions";
import { searchInTxs, reset } from "ibankActions/search";
import { Link } from "react-router-dom";
import { getUserType } from "services/paymentBackendAPI/backendPlatform";
import moment from "moment";
import { getCETDate } from "helpers/target2Timing";

const isAdmin = getUserType() === "admin";

const columns = [
  {
    key: "checkedCheckboxWaiting",
    _isCheckbox: true
  },
  // {
  //   path: "cdttrftxinfcdtrnm",
  //   label: "Beneficiary"
  // },
  {
    path: "name",
    label: "Beneficiary's name",
    content: item => <>
      <div>{item.to_name}</div>
      <div>{item.to_account}</div>
    </>
  },
  // {
  //   path: "cdttrftxinfrmtinfstrdcdtrrefinfref",
  //   label: "Payment description"
  // }, 
  {
    path: "description",
    label: "Payment description"
  }, 
  // {
  //   path: "amount",
  //   label: "Amount",
  //   content: item => `${item.cdttrftxinfintrbksttlmamt.toFixed(2)} EUR`
  // }
  {
    path: "amount",
    label: "Amount",
    content: item => `${item.amount.toFixed(2)} EUR`
  }
];

const mapStateToProps = (state, props) => {
  return {
    disableDate: true,
    checkedWaiting: state.transactions.checkedWaiting,
    response: state.transactions.responseUpsertTxsTemplates,
    // data: state.transactions.waitingPayments,
    // count: state.transactions.countWaitingPayments,
    data: state.transactions.txTemplates,
    count: state.transactions.countTxTemplates,
    searchData: {},
    forceRerender: props.forceRerender,
    directionData: { ...props.directionData, once: true },
    isSearch: false,
    // loading: state.transactions.loadingWaitingPayments,
    loading: state.transactions.loadingTxTemplates,
    balance: props.balance,
    // generateDocResponse: state.documents.docResponse,
    // editTxStatusResponse: state.transactions.responseTransactionStatus,
    name: "waiting payments",
    type: "ffcctrns",
    columns
  };
};

export default connect(mapStateToProps, {
  changeWaitingCheckbox,
  upsertTxsTemplates,
  // get: getWaitingPayments,
  get: getAllTxTemplates,
  search: searchInTxs,
  reset,
  editTxStatus
})(AbstractComponent);