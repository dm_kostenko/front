import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "ibankFactories/Table";
import { getAllTxTemplates, changeTemplateCheckbox, upsertTxsTemplates } from "ibankActions/transactions";

const columns = [
  {
    key: "checkedCheckboxTxTemplate",
    _isCheckbox: true
    // key: "guid"
    // content: item => <input type="checkbox" onChange={changeTemplateCheckbox(item.guid)}/>
  },
  {
    path: "name",
    label: "Beneficiary's name",
    content: item => <>
      <div>{item.to_name}</div>
      <div>{item.to_account}</div>
    </>
  },
  {
    path: "description",
    label: "Payment description"
  }, 
  {
    path: "amount",
    label: "Amount",
    content: item => `${item.amount.toFixed(2)} EUR`
  }
];

const mapStateToProps = (state, props) => {
  return {
    checkedTemplates: state.transactions.checkedTemplates,
    response: state.transactions.responseUpsertTxsTemplates,
    data: state.transactions.txTemplates,
    count: state.transactions.countTxTemplates,
    searchData: {},
    forceRerender: props.forceRerender,
    directionData: { ...props.directionData },
    isSearch: false,
    balance: props.balance,
    loading: state.transactions.loadingTxTemplates,
    name: "transaction templates",
    columns
  };
};

export default connect(mapStateToProps, {
  changeTemplateCheckbox,
  upsertTxsTemplates,
  get: getAllTxTemplates,
  search: () => {},
  reset: () => {}
})(AbstractComponent);