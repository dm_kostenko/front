import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Row, Col } from "react-bootstrap";
import Table from "ibankComponents/UI/Table";
import Content from "ibankViews/Content"
import { getTxsFees, createTxs, changeWaitingCheckbox, changeTemplateCheckbox } from "ibankActions/transactions";
import config from "../../../../config/"
import Spinner from "ibankComponents/UI/Spinner"
import { parseResponse } from "helpers/parseResponse";
import swal from "sweetalert";
import { reset } from "ibankActions/confirmation";

class TemplatesSubmit extends Component {

  state = {
    checked: [],
    loading: true,
    totalFee: 0
  }

  

  static getDerivedStateFromProps = async (nextProps) => {
    if (Object.entries(nextProps.responseTransactions).length > 0 && nextProps.confirmation && nextProps.responseTransactions.status) {
      swal({
        title: "Transactions were send",
        text: `Status: ${nextProps.responseTransactions.status}`,
        icon: "success"
      });
      nextProps.reset();
      let clearArrayFunc = () => {};
      switch(nextProps.location.pathname) {
        case "/waitingpayments":
          clearArrayFunc = async () => {
            await nextProps.changeWaitingCheckbox([])
            // delete txs
          }
          break;
        case "/templatessubmit":
          clearArrayFunc = async () => await nextProps.changeTemplateCheckbox([])
          break;
        default:
          break;
      }
      clearArrayFunc()
      window.location.replace(process.env.PUBLIC_URL + "#/transactions" || "#/transactions")
    }
    return null;
  }

  componentDidMount = async() => {
    let checked;
    switch(this.props.location.pathname) {
      case "/waitingpayments":
        checked = this.props.checkedWaiting
        break;
      case "/templatessubmit":
        checked = this.props.checkedTemplates
        break;
      default:
        break;
    }
    if(!checked || checked.length === 0)
      window.location.replace(process.env.PUBLIC_URL + "#/transactions" || "#/transactions")

    await this.props.getTxsFees({
      transactions: checked.map(i => ({
        senderCurrency: i.from_currency_code,
        receiverCurrency: i.to_currency_code,
        amount: i.amount,
        senderAccount: i.from_account,
        receiverAccount: i.to_account,
        senderName: "TBF Finance, UAB",
        senderBIC: config.bic,
        receiverName: i.to_name,
        receiverBIC: i.to_bic
      }))
    })

    const { fees, totalFee } = this.props.responseTransactionsFee
    checked = checked.map((t, i) => ({
      ...t,
      fee: parseFloat(fees[i])
    }))
    this.setState({
      checked,
      totalFee,
      loading: false
    })
  }

  handleConfirm = async() => {
    try {
      const transactions = this.state.checked.map(i => ({
        amount: i.amount,
        amountWithRates: +i.amount + +i.fee,
        debtorName: "TBF Finance, UAB",
        debtorBic: config.bic,
        debtorAccount: i.from_account,
        creditorName: i.to_name,
        creditorAccount: i.to_account,
        creditorBic: i.to_bic,
        remittanceInformation: i.description,
        currency: "EUR" // yet
      }))

      await this.props.createTxs({ transactions })
      swal({
        title: "Transactions were send",
        text: `Status: ${this.props.responseTransactions.status}`,
        icon: "success"
      });
      window.location.replace(process.env.PUBLIC_URL + "#/transactions" || "#/transactions")
    }
    catch(error) {
      const parsedError = parseResponse(error);
      if ((error.response && error.response.status !== 409) || !error.response)
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
    }
  }

  render() {

    const columns = [
      {
        path: "name",
        label: "Beneficiary's name",
        content: item => <>
          <div>{item.name}</div>
          <div>{item.to_account}</div>
        </>
      },
      {
        path: "description",
        label: "Payment description"
      }, 
      {
        path: "amount",
        label: "Amount",
        content: item => `${item.amount.toFixed(2)} EUR`
      },
      {
        path: "fee",
        label: "Fee",
        content: item => `${item.fee.toFixed(2)} EUR`
      }
    ];

    if(this.state.loading)
      return <Spinner/>
    else {
      let sum = 0
      this.state.checked.forEach(i => {
        sum += (parseFloat(i.amount) + parseFloat(i.fee))
      })

      return (
        <Content>
          <Table
            columns={columns}
            data={this.state.checked}
            disableSearch={true}
          />
          <Row>
            <Col md={10} lg={10} xs={10} style={{ marginTop: "17px" }}/>
            <div className="templates-all-amount">
              Total: {sum.toFixed(2)} EUR
            </div>
          </Row>
          <Row>
            <Col md={10} lg={10} xs={10}/>
            {/* <Col md={2} lg={2} xs={2}> */}
              <div className="templates-all-amount">
                <Button className="submit-button confirm2" onClick={this.handleConfirm}>
                  Confirm
                </Button>
              </div>
            {/* </Col> */}
          </Row>
        </Content>
      )
    }
  }
}

const mapStateToProps = (state) => ({
  checkedWaiting: state.transactions.checkedWaiting,
  checkedTemplates: state.transactions.checkedTemplates,
  responseTransactionsFee: state.transactions.responseTransactionsFee,
  responseTransactions: state.transactions.responseTransactions,
  confirmation: state.confirmation.confirmation
})

export default connect(mapStateToProps, {
  getTxsFees,
  createTxs,
  changeWaitingCheckbox,
  changeTemplateCheckbox,
  reset
})(TemplatesSubmit)