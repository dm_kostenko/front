import React from "react";
import { Row } from "react-bootstrap";
import { connect } from "react-redux";
import { BarChart } from "ibankComponents/UI/BarChart";
import Card from "ibankComponents/UI/Card";
import Select from "ibankComponents/UI/MultiSelect";
import PropTypes from "prop-types";
import Spinner from "ibankComponents/UI/Spinner";
import { getTransactionsTypes } from "ibankActions/reports";

class TransactionsChartPeriodic extends React.Component {
  state = {
    selectedType: "FFCCTRNS"
  }

  componentDidMount = async () => {
    await this.props.getTransactionsTypes();
  }

  handleSelect = async (option) => {
    this.setState({
      selectedType: option.name
    });
  }

  render() {
    const selectorStyles = {
      placeholder: (styles) => {
        return {
          ...styles,
          color: "black"
        };
      },
      control: (styles, state) => {
        return {
          ...styles,
          background: "rgba(255,255,255,0.4) !important",
          marginBottom: "30px",
          width: "100%",
          height: "40px !important",
          transition: ".8s all",
          "&:hover": {
            background: state.menuIsOpen ? "white !important" : "rgba(255,255,255,0.4) !important",
            transition: ".8s all"
          }
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          width: "100%",
          marginLeft: "auto",
          marginRight: "auto"
        };
      }
    };
    const options = this.props.data.map(item => {
      return {
        guid: item.type,
        name: item.type
      };
    });
    const selectedOption = this.props.data.filter(item => item.type === this.state.selectedType)[0] || this.props.data.filter(item => item.type === this.state.options[0].name)[0] || "";
    
    let labelsPie = [ selectedOption.type ];
    const incoming = selectedOption.incoming;
    const outgoing = selectedOption.outgoing;
    const dataExport = [ selectedOption ];
    const datasetsPie = [
      {
        label: "Incoming",
        backgroundColor: "#22ac71",
        borderColor: "#22ac71",
        borderWidth: 1,
        hoverBackgroundColor: "#22ac71",
        hoverBorderColor: "#22ac71",
        data: [ incoming ]
      },
      {
        label: "Outgoing",
        backgroundColor: "#23819C",
        borderColor: "#23819C",
        borderWidth: 1,
        hoverBackgroundColor: "#23819C",
        hoverBorderColor: "#23819C",
        data: [ outgoing ]
      }
    ];

    if (this.props.loading)
      return (
        <Card
          header="Incoming/Outgoing"
          style={{
            flex: "1"
          }}
        >
          <Spinner style={{ marginBottom: "5%" }} />
        </Card>
      );

    return (
      <Card
        header="Transactions"
        style={{
          flex: "1"
        }}
      >
        <Row>
          <Select
            value={this.state.selectedType}
            options={options}
            onSelect={this.handleSelect}
            styles={selectorStyles}
          />
        </Row>
        <BarChart
          labels={labelsPie}
          datasets={datasetsPie}
          height={200}
          name={"Transactions types"}
          data={dataExport}
          options={{
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true,
                  callback: (value) => { return value % 1 === 0 ? value : null; }
                }
              }]
            },
            maintainAspectRatio: true,
            responsive: true
          }}
        />
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.reports.transactionTypes,
    loading: state.reports.transactionTypesLoading
  };
};

export default connect(mapStateToProps, {
  getTransactionsTypes
})(TransactionsChartPeriodic);

TransactionsChartPeriodic.propTypes = {
  types: PropTypes.object,
  loading: PropTypes.bool,
  data: PropTypes.array,
  getTransactionsTypes: PropTypes.func,
  transactionTypes: PropTypes.array
};
