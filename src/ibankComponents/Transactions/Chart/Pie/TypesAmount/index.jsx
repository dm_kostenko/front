import React from "react";
import { connect } from "react-redux";
import Card from "ibankComponents/UI/Card";
import { PieChart } from "ibankComponents/UI/PieChart";
import PropTypes from "prop-types";
import { backgroundColors } from "ibankConstants/colors";

class TypesPieChart extends React.Component {

  render() {
    let data = [];
    let labelsPie = [];
    let colors = [];
    this.props.data.map((item, index) => {
      data.push(+item.incoming + +item.outgoing);
      labelsPie.push(item.type);
      colors.push(backgroundColors[index]);
    });
    const datasetsPie = [ {
      data,
      backgroundColor: colors
    } ];
    return (
      <Card
        header="Types amount"
        style={{
          flex: "1"
        }}
      >
        <PieChart
          name="Types amount"
          labels={labelsPie.length !== 0 ? labelsPie : [ "No data" ]}
          datasets={datasetsPie}
        />
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.reports.transactionTypes
  };
};

export default connect(mapStateToProps, {

})(TypesPieChart);

TypesPieChart.propTypes = {
  data: PropTypes.array
};