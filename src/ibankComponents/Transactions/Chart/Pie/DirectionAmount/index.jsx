import React from "react";
import { connect } from "react-redux";
import Card from "ibankComponents/UI/Card";
import { PieChart } from "ibankComponents/UI/PieChart";
import PropTypes from "prop-types";

class DirectionPieChart extends React.Component {

  render() {
    let incoming = 0;
    let outgoing = 0;
    this.props.data.map(item => {
      incoming += +item.incoming;
      outgoing += +item.outgoing;
    });
    const datasetsPie = [ {
      data: [ incoming, outgoing ],
      backgroundColor: [
        "#22ac71",
        "#23819C"
      ]
    } ];
    const labelsPie = [
      "Incoming",
      "Outgoing",
    ];
    return (
      <Card
        header="Direction amount"
        style={{
          flex: "1"
        }}
      >
        <PieChart
          name="Direction amount"
          labels={labelsPie}
          datasets={datasetsPie}
        />
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.reports.transactionTypes
  };
};

export default connect(mapStateToProps, {

})(DirectionPieChart);

DirectionPieChart.propTypes = {
  data: PropTypes.array
};