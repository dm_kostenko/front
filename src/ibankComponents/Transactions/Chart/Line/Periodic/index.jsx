import React from "react";
import { Row, Col } from "react-bootstrap";
import { connect } from "react-redux";
import { LineChart } from "ibankComponents/UI/LineChart";
import Card from "ibankComponents/UI/Card";
import Select from "ibankComponents/UI/MultiSelect";
import { lastNDays } from "services/dateTime/dateTime";
import PropTypes from "prop-types";
import { getTransactionsAmount } from "ibankActions/reports";
import Spinner from "ibankComponents/UI/Spinner";
import moment from "moment";
import { getCETDate } from "helpers/target2Timing";

class TransactionsChartPeriodic extends React.Component {
  state = {
    count: 7,
    countMonths: 12,
    report_type: "day",
    loading: true
  }

  componentDidMount = async() => {
    await this.props.getTransactionsAmount({ 
      report_type: this.state.report_type,
      days: this.state.report_type === "day" ? this.state.count : 365
    });
  }

  handleSelect = async(option) => {
    this.setState({
      report_type: option.guid
    });
    await this.props.getTransactionsAmount({
      report_type: option.guid,
      days: option.guid === "day" ? this.state.count : 365
    });
  }

  render() {
    let labelsPie = [];
    let data = [];
    switch(this.state.report_type) {
    case "day": {
      labelsPie = lastNDays(this.state.count, this.state.report_type);
      data = new Array(this.state.count).fill(0);
      this.props.data.forEach(item => {
        const index = labelsPie.indexOf(moment(getCETDate(new Date(item.created_at))).format("DD.MM"));
        if(index !== -1)
          data[index] = item.count;
      });
      break;
    }
    case "month": {
      labelsPie = lastNDays(this.state.countMonths, this.state.report_type);
      data = new Array(this.state.countMonths).fill(0);
      this.props.data.forEach(item => {
        const index = labelsPie.indexOf(moment(item.created_at).format("MMM"));
        if(index !== -1)
          data[index] = item.count;
      });
      break;
    }
    default: break;
    }
    
    const datasetsPie = [
      {
        label: "Transactions",
        fill: false,
        lineTension: 0.1,
        backgroundColor: "#22ac71",
        borderColor: "#22ac71",
        borderWidth: 4,
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "round",
        pointBorderColor: "#22ac71",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "#22ac71",
        pointHoverBorderColor: "black",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 50,
        data
      }
    ];

    if(this.props.loading)
      return (
        <Card
          header="Amount of transactions"
          style={{
            flex: "2"
          }}
        >
          <Spinner style={{ marginBottom: "5%" }}/>
        </Card>
      );

    return (
      <Card
        header="Amount of transactions"
        style={{
          flex: "2"
        }}
      >
        <Row>
          <Col md={4}/>
          <Col md={4}>
            <Select
              options={[
                {
                  guid: "day",
                  name: "By days"
                },
                {
                  guid: "month",
                  name: "By months"
                }
              ]}
              onSelect={this.handleSelect}
              selectedValue={this.state.report_type}
              styles={{
                option: (state) => ({
                  ...state,
                  textAlign: "center !important",
                  font: "calc(1vw + 5px) Roboto"
                }),
                singleValue: (state) => ({
                  ...state,
                  font: "calc(1vw + 5px) Roboto",
                  marginLeft: "calc(47% - 1vw)",
                  marginRight: "auto"
                })
              }}
            />
          </Col>
          <Col md={4}/>
        </Row>
        <LineChart
          labels={labelsPie}
          datasets={datasetsPie}
          name="Transactions amount"          
          options={{
            scales: {
              yAxes: [ {
                ticks: {
                  beginAtZero: true,
                  callback: (value) => { return value % 1 === 0 ? value : null; }
                }
              } ]
            },
            maintainAspectRatio: true,
            responsive: true
          }}
        />
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.reports.amountOfTransactions,
    count: state.reports.amountOfTransactionsCount,
    loading: state.reports.amountOfTransactionsLoading
  };
};

export default connect(mapStateToProps, {
  getTransactionsAmount
})(TransactionsChartPeriodic);

TransactionsChartPeriodic.propTypes = {
  getTransactionTypesAction: PropTypes.func,
  types: PropTypes.object,
  loading: PropTypes.bool,
  data: PropTypes.array,
  getTransactionsAmount: PropTypes.array
};
