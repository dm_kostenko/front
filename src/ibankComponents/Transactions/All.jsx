import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "ibankFactories/Table";
import { getTxs } from "ibankActions/transactions";
import { editTxStatus } from "ibankActions/transactions";
import { searchInTxs, reset } from "ibankActions/search";
import { Link } from "react-router-dom";
import { getUserType } from "services/paymentBackendAPI/backendPlatform";
import moment from "moment";
import { getCETDate } from "helpers/target2Timing";

const isAdmin = getUserType() === "admin";

const dict = {
  "FFCCTRNS": "Credit transfer",
  "INTERNAL": "Internal",
  "PRTRN": "Payment return",
  "ROINVSTG": "Resolution of investigation",
  "FFPCRQST": "Payment cancellation"
};

const columns = [
  {
    path: "guid",
    label: "ID",
    content: tx => {
      return isAdmin
        ? <Link className="link" to={`transactions/${tx.type.toLowerCase()}/${tx.guid}`}>{tx.guid}</Link>
        : tx.guid;
    }
  },
  { 
    path: "created_at", 
    label: "Timestamp",
    content: (tx) => moment(getCETDate(new Date(tx.created_at))).format("DD.MM.YYYY HH:mm:ss")
  },
  {
    path: "type",
    label: "Type",
    content: tx => (
      <div style={{ textTransform: "uppercase", wordBreak: "keep-all" }}>{dict[tx.type]}</div>
    )
  },
  { 
    path: "status", 
    label: "Status",
    content: tx => <div style={{ wordBreak: "keep-all" }}>{tx.status}</div>
  },
  {
    path: "value",
    label: "Amount",
    content: tx => tx.to_amount === null 
      ? ( `${(+tx.value).toFixed(2)} ${tx.currency}` )
      : ( `${(+tx.from_amount).toFixed(2)} ${tx.from_currency_code} -> ${(+tx.to_amount).toFixed(2)} ${tx.to_currency_code}` )
  },
  {
    path: "commission",
    label: "Fee",
    content: tx => tx.to_amount && tx.rate 
      ? `${tx.commission} ${tx.from_currency_code} (${(tx.commission / tx.rate).toFixed(2)} ${tx.to_currency_code})`
      : `${tx.commission} ${tx.currency}`
  },  
  {
    path: "sender_iban",
    label: "Sender IBAN"
  },
  {    
    path: "receiver_iban",
    label: "Receiver IBAN"
  },
  {
    key: "editTxStatus",
    label: "Approve / Cancel"
  }  
];

const mapStateToProps = (state, props) => {
  return {
    data: state.transactions.ALL,
    count: state.transactions.countALL,
    searchData: state.search.ALLSearch,
    directionData: { ...props.directionData },
    isSearch: state.search.isSearch,
    loading: state.transactions.loadingALL,
    editTxStatusResponse: state.transactions.responseTransactionStatus,
    name: "transactions",
    type: "all",
    columns
  };
};

export default connect(mapStateToProps, {
  get: getTxs,
  search: searchInTxs,
  reset,
  editTxStatus
})(AbstractComponent);