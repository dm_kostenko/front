import React, { Component } from "react";
import Content from "ibankViews/Content";
import { getTx } from "ibankActions/transactions";
import { connect } from "react-redux";
import Card from "ibankComponents/UI/Card";
import DetailInfoLabel from "ibankComponents/UI/DetailInfoLabel";
import PropTypes from "prop-types";
import Spinner from "ibankComponents/UI/Spinner";
import tagsDictionary from "helpers/fieldsDictionary";

class InternalDetail extends Component {

  state = {
    data: {}
  }

  async componentDidMount() {
    await this.props.getTx(this.props.match.params.id, "Internal");   
  }




  render() {
    const unusedKeys = [ 
      "document", 
      "guard_counter", 
      "count", 
      "from_currency_code", 
      "to_currency_code",
      "from_amount", 
      "to_amount",
      "currency",
      "date"
    ];

    if(this.props.loading) 
      return <Content>
        <Spinner/>
      </Content>;
    return <Content>
      <Card
        style={{
          color: "black",
          // background: "#80dbff",          
          marginLeft: "auto",
          marginRight: "auto",
          height: "auto",
          marginTop: "20px",
          textAlign: "center"
        }}
        detail={true}
        header={`TRANSACTION: ${this.props.match.params.id}`}
      >
        {Object.keys(this.props.data).map(key => {
          const label = tagsDictionary[key.toUpperCase()] 
            ? tagsDictionary[key.toUpperCase()].beautify 
            : key;
          const value = tagsDictionary[key.toUpperCase()] && tagsDictionary[key.toUpperCase()].modify 
            ? tagsDictionary[key.toUpperCase()].modify(this.props.data[key])
            : this.props.data[key] 
              ? (key !== "value" 
                ? `${this.props.data[key]}`
                : this.props.data.from_amount 
                  ? `${this.props.data.from_amount.toFixed(2)} ${this.props.data.from_currency_code} -> ${this.props.data.to_amount.toFixed(2)} ${this.props.data.to_currency_code}`
                  : `${this.props.data[key].toFixed(2)} ${this.props.data.currency}`)
              : "";
          return !unusedKeys.includes(key) && 
          <DetailInfoLabel
            key={key}
            label={`${label}:`}
            value={value === "Invalid date" ? "-" : value || "-"}
          />;
        })}
      </Card>
    </Content>;
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.transactions.internalDetail,
    loading: state.transactions.loadingInternalDetail
  };
};

export default connect(mapStateToProps, {
  getTx
})(InternalDetail);

InternalDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  loading: PropTypes.bool,
  getTx: PropTypes.func,
  data: PropTypes.object
};