import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../ibankFactories/Table";
import { getTxs } from "ibankActions/transactions";
import { searchInTxs, reset } from "ibankActions/search";
import { Link } from "react-router-dom";
import { getUserType } from "services/paymentBackendAPI/backendPlatform";
import moment from "moment";
import { getCETDate } from "helpers/target2Timing";

const isAdmin = getUserType() === "admin";

const columns = [
  {
    path: "guid",
    label: "ID",
    content: tx => {
      return isAdmin
        ? <Link className="link" to={`transactions/ffpsrprt/${tx.guid}`}>{tx.guid}</Link>
        : tx.guid;
    }
  },
  { 
    path: "created_at", 
    label: "Timestamp",
    content: (tx) => moment(getCETDate(new Date(tx.created_at))).format("DD.MM.YYYY HH:mm:ss")
  },
  { 
    path: "status", 
    label: "Status" 
  },
  {
    path: "value",
    label: "Amount",
    content: tx => tx.to_amount === null 
      ? ( `${tx.value.toFixed(2)} ${tx.currency}` )
      : ( `${tx.from_amount.toFixed(2)} ${tx.from_currency_code} -> ${tx.to_amount.toFixed(2)} ${tx.to_currency_code}` )
  },
  {
    path: "sender_iban",
    label: "Sender IBAN"
  },
  {    
    path: "receiver_iban",
    label: "Receiver IBAN"
  },
  {
    key: "editTxStatus",
    label: "Approve / Cancel"
  }  
];

const mapStateToProps = (state, props) => {
  return {
    data: state.transactions.FFPSRPRT,
    count: state.transactions.countFFPSRPRT,
    searchData: state.search.txsFFPSRPRTSearch,
    directionData: { ...props.directionData },
    isSearch: state.search.isSearch,
    loading: state.transactions.loadingFFPSRPRT,
    name: "transactions",
    type: "ffpsrprt",
    columns
  };
};

export default connect(mapStateToProps, {
  get: getTxs,
  search: searchInTxs,
  reset
})(AbstractComponent);