import React from "react";
import { connect } from "react-redux";
import Card from "ibankComponents/UI/Card";
import { Button } from "react-bootstrap";
import PropTypes from "prop-types";
import DashboardTable from "ibankComponents/UI/DashboardTable/Transactions";
import { getTransactionsHistory } from "ibankActions/reports";
import Spinner from "ibankComponents/UI/Spinner";

const currencies = {
  USD: "$",
  EUR: "€",
  RUB: "₽"
};

class TransactionsDashboardTable extends React.Component {
  state = {
    loading: true
  }

  componentDidMount = async() => {
    await this.props.getTransactionsHistory();
  }

  render() {
    const header = `Recent ${this.props.type}`;
    if(this.props.loading)
      return (
        <Card
          header={header}
          className="transactions"
          style={{ flex: "1" }}
        >
          <Spinner style={{ marginBottom: "5%" }}/>
        </Card>
      );

    return (
      <Card
        header={header}
        className="transactions"
        style={{ flex: "1" }}

      >
        <DashboardTable
          data={this.props.transactions.slice(0, 5).map(item => (
            {
              from: item.sender_iban,
              to: item.receiver_iban,
              amount: `${item.value} ${currencies[item.currency]}`,
              time: item.created_at
            }
          ))}
          type={this.props.type}
        />
        {this.props.transactions.length > 5 &&
        <div className="show-more">
          <Button
            className="submit-button show-more"
            onClick={() => window.location.replace(process.env.PUBLIC_URL+`#/${this.props.type}`)}
          >
            Show all
          </Button>
        </div>}
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    transactions: state.reports.transactionHistory,
    loading: state.reports.transactionHistoryLoading
  };
};

export default connect(mapStateToProps, {
  getTransactionsHistory
})(TransactionsDashboardTable);

TransactionsDashboardTable.propTypes = {
  getTransactionsHistory: PropTypes.func,
  types: PropTypes.object,
  loading: PropTypes.bool,
  type: PropTypes.string
};