import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "ibankFactories/Table";
import { getAllTxRules } from "ibankActions/transactions";
import Editor from "./Editor";
import Modal from "ibankComponents/UI/Modal";
import { searchInTxRules, reset } from "ibankActions/search";

const columns = [
  {
    path: "direction",
    label: "Direction"
  },
  {
    path: "transaction_type",
    label: "Trasnaction type"
  }, 
  {
    path: "rule_type",
    label: "Rule type"
  },
  {
    path: "role",
    label: "Role"
  },
  {
    path: "success_status",
    label: "Success status"
  },
  {
    path: "failed_status",
    label: "Failed status"
  },
  {
    path: "below",
    label: "Below",
    content: txRule => txRule.below.toFixed(2)
  },
  {
    key: "edit",
    content: rule => (
      <Modal 
        header="Edit rule"
        content={<Editor rule={rule}/>} 
        button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }}/>}
        state="txRules"
      />
    ),
    label: "Edit"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.transactions.txRules,
    count: state.transactions.countTxRules,
    searchData: state.search.txRulesSearch,
    isSearch: state.search.isSearch,
    loading: state.transactions.loadingTxRules,
    name: "transaction rules",
    columns
  };
};

export default connect(mapStateToProps, {
  get: getAllTxRules,
  search: searchInTxRules,
  reset
})(AbstractComponent);