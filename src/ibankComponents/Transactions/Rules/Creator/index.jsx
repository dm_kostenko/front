import React, { Component } from "react";
import { Button, Col, ControlLabel, Form, FormControl, InputGroup,  Row } from "react-bootstrap";
import Card from "ibankComponents/UI/Card";
import Input from "ibankComponents/UI/Input";
import { showModal } from "ibankActions/modal";
import { createTxRule } from "ibankActions/transactions";
import { connect } from "react-redux";
import { parseResponse } from "helpers/parseResponse";
import Select from "ibankComponents/UI/MultiSelect";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";
import Joi from "@hapi/joi";

class TxRuleCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      direction: "",
      transaction_type: "",
      rule_type: "status",
      name: "",
      value: ""
    };
  }

  schema = {
    direction: Joi.equal([ "incoming", "outgoing" ]).required(),
    transaction_type: Joi.equal([ "FFCCTRNS", "INTERNAL" ]).required(),
    rule_type: Joi.equal([ "status" ]).required(),
    name: Joi.string().required(),
    value: Joi.string().required()
  };


  componentDidMount = async() => {
    this.updateWindowStyles();
    window.addEventListener("resize", this.updateWindowStyles);
  }

  updateWindowStyles = () => {
    this.setState({
      width: window.outerWidth
    });
  }

  optionsDirection = [
    { guid: "1", name: "incoming" },
    { guid: "2", name: "outgoing" }
  ]

  optionsTxType = [
    { guid: "1", name: "FFCCTRNS" }
  ]

  optionsName = [
    { guid: "1", name: "success_individual_status" },
    { guid: "2", name: "failed_individual_status" },
    { guid: "3", name: "success_entity_status" },
    { guid: "4", name: "failed_entity_status" },
    { guid: "5", name: "below" },
    { guid: "6", name: "above" }
  ]

  onChangeHandler = (e) => {
    const property = e.target.name;
    const value = e.target.value;
    this.setState({
      [property]: value
    });
  };

  handleChangeDirection = option => {
    this.setState({ direction: option.name, value: "" });
  }

  handleChangeTxType = option => {
    this.setState({ transaction_type: option.name });
  }

  handleChangeName = option => {
    this.setState({ name: option.name });
  }

  handleChangeValue = option => {
    this.setState({ value: option.name });
  }

  generalValidation = () => {
    return this.formValidation(this.state.direction) &&
    this.formValidation(this.state.transaction_type) &&
    this.formValidation(this.state.rule_type) &&
    this.formValidation(this.state.name) &&
    this.formValidation(this.state.value);
  }

  formValidation = (data) => {
    return data !== "";
  };

  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    const request = {
      direction: this.state.direction,
      transaction_type: this.state.transaction_type,
      rule_type: this.state.rule_type,
      name: this.state.name,
      value: this.state.value
    };
    if (!this.generalValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    }
    else {
      try {        
        await this.props.createTxRule(this.props.currentPage, this.props.pageSize, request);
        this.setState({ isLoading: false });
        swal({
          title: "Rule is created",
          icon: "success",
          button:false,
          timer: 2000
        });
        await this.props.showModal(false, "txRules");
      }
      catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        if((error.response && error.response.status !== 409) || !error.response)        
          swal({
            title: parsedError.error,
            text: parsedError.message,
            icon: "error",
          });
      }
    }
  };

  style={
    default: {
      marginBottom: "30px"
    }
  }


  render() {
    const selectorStyles = {
      control: (styles) => {
        return {
          ...styles,
          marginBottom: "30px !important",
          background: "#dedede",
          height: "40px"
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          width: "100%",
          textAlign: "center",
          marginLeft: "15px"
        };
      },
      placeholder: (styles) => {
        return {
          ...styles,
          width: "100%",
          textAlign: "center",
          marginLeft: "15px"
        };
      }
    };

    const optionsValue = this.state.direction === "outgoing" 
      ? [
        { guid: "1", name: "Approved" },
        { guid: "2", name: "Unapproved" }
      ] 
      : [
        { guid: "1", name: "Received" },
        { guid: "2", name: "Imported" },
        { guid: "3", name: "Import declined" }
      ];

    const style = this.state.width > 1000 ? { display: "flex" } : {};
    const rest = this.state.width > 1000 ? {} : { md: 6 };

    return (
      <Form autoComplete="off">
        <Row>
          <Col style={style}>
            <Card
              header="info"
              style={{
                // background: "#80dbff"
              }}
            >
              <Row>
                <Col className="col" {...rest}>
                  <ControlLabel 
                    className="col-label"
                  >
                    Direction
                  </ControlLabel>
                  <Select
                    key="direction"
                    options={this.optionsDirection}
                    styles={selectorStyles}
                    selectedValue={this.state.direction}
                    onSelect={this.handleChangeDirection}
                    placeholder="Select..."
                  />
                </Col>
                <Col className="col" {...rest}>
                  <ControlLabel 
                    className="col-label"
                  >
                    Transaction type
                  </ControlLabel>
                  <Select
                    key="transaction_type"
                    options={this.optionsTxType}
                    styles={selectorStyles}
                    selectedValue={this.state.transaction_type}
                    onSelect={this.handleChangeTxType}
                    placeholder="Select type..."
                  />
                </Col>
                <Col className="col" {...rest}>
                  <Input
                    label="Rule type"
                    name="rule_type"
                    value={this.state.rule_type}
                    onChange={this.onChangeHandler}
                    style={this.style}
                    disabled
                  />
                </Col>
                <Col className="col" {...rest}>
                  <ControlLabel 
                    className="col-label"
                  >
                    Name
                  </ControlLabel>
                  <Select
                    key="name"
                    options={this.optionsName}
                    styles={selectorStyles}
                    selectedValue={this.state.name}
                    onSelect={this.handleChangeName}
                    placeholder="Select name..."
                  />
                </Col>
                {this.state.name === "below" || this.state.name === "above"
                  ? <Col className="col" {...rest}>
                    <InputGroup>
                      <FormControl
                        label="Value"
                        name="value"
                        type="number"
                        value={this.state.value}
                        onChange={this.onChangeHandler}
                        style={this.style}
                      />
                      <span className="input-group-addon" style={{ color: "black", backgroundColor: "#dedede" }}>€</span>
                    </InputGroup>
                  </Col>
                  : <Col className="col" {...rest}>
                    <ControlLabel 
                      className="col-label"
                    >
                      Value
                    </ControlLabel>
                    <Select
                      key="value"
                      options={optionsValue}
                      styles={selectorStyles}
                      selectedValue={this.state.value}
                      onSelect={this.handleChangeValue}
                      placeholder="Select value..."
                    />
                  </Col>}
              </Row>
            </Card>
          </Col>          
          <div align="center">
            {this.state.isLoading 
              ? <ReactLoading type='cylon' color='grey' /> 
              : <Button 
                className="submit-button"
                style={{
                  marginTop: "40px"
                }}
                onClick={this.handleSubmit}
              >
                Edit
              </Button>}
          </div>
        </Row>
      </Form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pageSize: state.transactions.pageSize,
    currentPage: state.transactions.currentPage
  };
};

export default connect(mapStateToProps, {
  createTxRule,
  showModal
})(TxRuleCreator);

TxRuleCreator.propTypes = {
  createTxRule: PropTypes.func,
  rule: PropTypes.object,
  showModal: PropTypes.func,
  currentPage: PropTypes.string,
  pageSize: PropTypes.string
};