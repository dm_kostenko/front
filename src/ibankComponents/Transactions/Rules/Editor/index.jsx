import React, { Component } from "react";
import { Button, Col, ControlLabel, Form, Row } from "react-bootstrap";
import Card from "ibankComponents/UI/Card";
import Input from "ibankComponents/UI/Input";
import { showModal } from "ibankActions/modal";
import { createTxRule } from "ibankActions/transactions";
import Select from "ibankComponents/UI/MultiSelect";
import { connect } from "react-redux";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";
import Joi from "@hapi/joi";

class TxRuleEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      direction: "",
      transaction_type: "",
      rule_type: "",
      role: "",
      success_status: "",
      failed_status: "",
      below: "",
      options_success_status: [],
      options_failed_status: []
    };
  }

  schema = {
    direction: Joi.equal([ "incoming", "outgoing" ]).required(),
    transaction_type: Joi.equal([ "FFCCTRNS" ]).required(),
    rule_type: Joi.equal([ "status" ]).required(),
    role: Joi.equal([ "all", "individual", "entity" ]).required(),
    success_status: Joi.string().required(),
    failed_status: Joi.string().required(),
    below: Joi.number().required()
  };


  componentDidMount = async() => {
    this.updateWindowStyles();
    window.addEventListener("resize", this.updateWindowStyles);
    let options_success_status = [];
    let options_failed_status = [];
    switch(this.props.rule.role) {
    case "entity": {
      options_success_status = [
        { guid: "1", name: "Unconfirmed" }, 
        { guid: "2", name: "Unapproved" }, 
        { guid: "3", name: "Approved" }
      ];
      options_failed_status = [
        { guid: "1", name: "Unconfirmed" }, 
        { guid: "2", name: "Unapproved" }
      ];
      break;
    }
    case "individual": {
      options_success_status = [
        { guid: "1", name: "Unapproved" }, 
        { guid: "2", name: "Approved" }
      ];
      options_failed_status = [
        { guid: "1", name: "Unapproved" }, 
        { guid: "2", name: "Approved" }
      ];
      break;
    }
    case "all": {
      options_success_status = [
        { guid: "1", name: "Received" }, 
        { guid: "2", name: "Imported" }
      ];
      options_failed_status = [
        { guid: "1", name: "Import declined" }, 
        { guid: "2", name: "Received" },
        { guid: "3", name: "Imported" }
      ];
      break;
    }
    default: break;
    }
    const endIndexForSlice = options_success_status.map(item => {return item.name;}).indexOf(this.props.rule.success_status) + 1;
    this.setState({
      direction: this.props.rule.direction,
      transaction_type: this.props.rule.transaction_type,
      rule_type: this.props.rule.rule_type,
      role: this.props.rule.role,
      success_status: this.props.rule.success_status,
      failed_status: this.props.rule.failed_status,
      below: (+this.props.rule.below).toFixed(2),
      options_success_status,
      options_failed_status: options_failed_status.slice(0, endIndexForSlice),
      initial_options_success_status: options_success_status,
      initial_options_failed_status: options_failed_status
    });
  }

  updateWindowStyles = () => {
    this.setState({
      width: window.outerWidth
    });
  }

  onChangeHandler = (e) => {
    const property = e.target.name;
    let value = e.target.value;
    if (property === "below") {
      value = value.replace(".", "");
      let start = value.substr(0, value.length - 2);
      let middle = ".";
      let finish = value.substr(value.length - 2, value.length);
      value = start + middle + finish;
      switch (value.length) {
      case 2: {
        value = value[value.length - 1] + ".00";
        break;
      }
      case 3: {
        value = "0" + value;
        break;
      }
      default: {
        if (value[0] === "0") {
          value = value.substr(1, value.length - 1);
        }
        break;
      }
      }
    }
    this.setState({
      [property]: value
    });
  };

  generalValidation = () => {
    return this.formValidation(this.state.direction) &&
    this.formValidation(this.state.transaction_type) &&
    this.formValidation(this.state.rule_type) &&
    this.formValidation(this.state.name) &&
    this.formValidation(this.state.value);
  }

  formValidation = (data) => {
    return data !== "";
  };

  handleChangeSuccessStatus = option => {
    const options_failed_status = this.state.initial_options_failed_status.slice(0, +option.guid);
    this.setState({
      success_status: option.name,
      options_failed_status,
      failed_status: ""
    });
  }

  handleChangeFailedStatus = option => {
    this.setState({
      failed_status: option.name
    });
  }

  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    const request = {
      direction: this.state.direction,
      transaction_type: this.state.transaction_type,
      rule_type: this.state.rule_type,
      role: this.state.role,
      success_status: this.state.success_status,
      failed_status: this.state.failed_status,
      below: this.state.below
    };
    if (!this.generalValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    }
    else {
      try {        
        await this.props.createTxRule(this.props.currentPage, this.props.pageSize, request);
        this.setState({ isLoading: false });
        swal({
          title: "Rule is updated",
          icon: "success",
          button:false,
          timer: 2000
        });
        await this.props.showModal(false, "txRules");
      }
      catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        if((error.response && error.response.status !== 409) || !error.response)        
          swal({
            title: parsedError.error,
            text: parsedError.message,
            icon: "error",
          });
      }
    }
  };

  style={
    default: {
      marginBottom: "30px"
    }
  }


  render() {
    const selectorStyles = {
      control: (styles) => {
        return {
          ...styles,
          marginBottom: "30px !important",
          background: "#dedede",
          height: "40px",
          fontSize: "calc(15px + 0.4vw)"
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          width: "100%",
          textAlign: "center",
          marginLeft: "15px"
        };
      },
      placeholder: (styles) => {
        return {
          ...styles,
          width: "100%",
          textAlign: "center",
          marginLeft: "15px"
        };
      }
    };

    const style = this.state.width > 1000 ? { display: "flex", backgroundColor: "rgba(255, 255, 255, 0.4) !important"} : {backgroundColor: "rgba(255, 255, 255, 0.4) !important"};
    const rest = this.state.width > 1000 ? {} : { md: 6 };

    return (
      <Form autoComplete="off">
        <Row>
          <Col style={style}>
            <Card
              header="info"
              style={{
                // background: "#80dbff"
              }}
            >
              <Row>
                <Col className="col" {...rest}>
                  <Input
                    label="Direction"
                    name="direction"
                    value={this.state.direction}
                    onChange={this.onChangeHandler}
                    style={{ ...this.style, input: { backgroundColor: "rgba(255, 255, 255, 0.4)" } }}
                    disabled
                  />
                </Col>
                <Col className="col" {...rest}>
                  <Input
                    label="Transaction type"
                    name="transaction_type"
                    value={this.state.transaction_type}
                    onChange={this.onChangeHandler}
                    style={{ ...this.style, input: { backgroundColor: "rgba(255, 255, 255, 0.4)" } }}
                    disabled
                  />
                </Col>
                <Col className="col" {...rest}>
                  <Input
                    label="Rule type"
                    name="rule_type"
                    value={this.state.rule_type}
                    onChange={this.onChangeHandler}
                    style={{ ...this.style, input: { backgroundColor: "rgba(255, 255, 255, 0.4)" } }}
                    disabled
                  />
                </Col>
                <Col className="col" {...rest}>
                  <Input
                    label="Role"
                    name="role"
                    value={this.state.role}
                    onChange={this.onChangeHandler}
                    style={{ ...this.style, input: { backgroundColor: "rgba(255, 255, 255, 0.4)" } }}
                    disabled
                  />
                </Col>
                <Col className="col" {...rest}>
                  <ControlLabel 
                    className="col-label"
                  >
                    Success status
                  </ControlLabel>
                  <Select
                    key="success_status"
                    options={this.state.options_success_status}
                    styles={selectorStyles}
                    selectedValue={this.state.success_status}
                    onSelect={this.handleChangeSuccessStatus}
                    placeholder="Select..."
                  />
                </Col>
                <Col className="col" {...rest}>
                  <ControlLabel 
                    className="col-label"
                  >
                    Failed status
                  </ControlLabel>
                  <Select
                    key="failed_status"
                    options={this.state.options_failed_status}
                    styles={selectorStyles}
                    selectedValue={this.state.failed_status}
                    onSelect={this.handleChangeFailedStatus}
                    placeholder="Select..."
                  />
                </Col>
                <Col className="col" {...rest}>
                  <Input
                    label="Below"
                    name="below"
                    type="number"
                    value={this.state.below}
                    onChange={this.onChangeHandler}
                    style={this.style}
                  />
                </Col>
              </Row>
            </Card>
          </Col>          
          <div align="center">
            {this.state.isLoading 
              ? <ReactLoading type='cylon' color='grey' /> 
              : <Button 
                className="submit-button"
                style={{
                  marginTop: "40px"
                }}
                onClick={this.handleSubmit}
              >
                Edit
              </Button>}
          </div>
        </Row>
      </Form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pageSize: state.transactions.pageSize,
    currentPage: state.transactions.currentPage
  };
};

export default connect(mapStateToProps, {
  createTxRule,
  showModal
})(TxRuleEditor);

TxRuleEditor.propTypes = {
  createTxRule: PropTypes.func,
  rule: PropTypes.object,
  showModal: PropTypes.func,
  currentPage: PropTypes.string,
  pageSize: PropTypes.string
};