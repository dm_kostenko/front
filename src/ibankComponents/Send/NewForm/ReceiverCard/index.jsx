import React, { Component } from "react";
import { getBankFromAccountNumber } from "helpers/getBankFromAccountNumber";
// import alfabank from "assets/img/cardLogo/alfabank.png";
// import gazprombank from "assets/img/cardLogo/gazprombank.png";
import tbf from "assets/img/logo/logo.png";
// import sberbank from "assets/img/cardLogo/sberbank.png";
// import tinkoff from "assets/img/cardLogo/tinkoff.png";
import Select from "ibankComponents/UI/MultiSelect";
import Input from "ibankComponents/UI/Input/Handler"

const logo = {
  // alfabank,
  // gazprombank,
  tbf,
  // sberbank,
  // tinkoff
};

class ReceiverCard extends Component {

  state = {
    width: 0,
    isInput: false,
    accountNumber: "",
    accountText: "",
    name: ""
  }

  componentDidMount = () => {
    this.updateWindowStyles();
    window.addEventListener("resize", this.updateWindowStyles);
  }

  updateWindowStyles = () => {
    this.setState({
      width: window.innerWidth
    });
  }

  handleBackClick = (e) => {
    e.preventDefault();
    this.setState({
      accountNumber: ""
    });
    this.props.changeIsInput(false);
  }

  getBankName = () => {
    return "tbf";
  }

  render() { 
    const styles = {
      dropdownIndicator: (styles) => ({ 
        ...styles,
        marginRight: "-moz-calc(10% - 2vw) !important",
        "svg": {
          width: "2vw !important",
          height: "2vw !important",
          maxHeight: "25px !important"
        }
      }),
      indicatorSeparator: () => ({ display: "none" }),
      placeholder: (styles) => ({
        ...styles,
        font: this.state.width > 1000 ? "calc(0.8vw + 8px) Roboto" : "12px Roboto",
        marginLeft: this.state.width > 1000 ? "5vw !important" : "calc(1vw + 40px)",
        opacity: "0.5 !important",
        color: "#797d8a !important"
      }),
      control: (styles) => ({
        ...styles,                
        maxWidth: "500px !important",   
        maxHeight: "63px !important",
        minWidth: "200px !important",
        minHeight: "25px !important",
        height: "3vw",
        border: "none !important",
        borderRadius: "0 !important",
        boxShadow: "none !important",
        opacity: this.props.isInput ? "0" : "1",
        pointerEvents: this.props.isInput ? "none !important" : "",
        userSelect: this.props.isInput ? "none !important" : ""
      }),
      menu: (styles) => ({
        ...styles,
        marginTop: "2px",          
        borderTopLeftRadius: "0",
        borderTopRightRadius: "0"
      }),
      option: (styles, state) => ({
        ...styles,
        paddingLeft: this.state.width > 1000 
          ? (state.data.other 
            ? "8vw !important" 
            : "3vw !important") 
          : (state.data.other 
            ? "calc(1vw + 50px)" 
            : "1vw"),
        textAlign: "left !important",
        borderTop: state.data.other || state.data.firstEntity ? "1px solid #545761" : ""
      }),
      singleValue: (styles) => ({
        ...styles,
        lineHeight: "3vw",
        paddingLeft: this.state.width > 1000 ? "3vw !important" : "calc(1vw + 55px)",
      })
    };
    // const bankName = getBankFromAccountNumber(this.state.accountNumber);
    const bankName = this.getBankName();
    return (
      <div className={bankName === "" ? "card send receiver" : `card send ${bankName}`}>
        <div className="bank-info" autoComplete="off">
          <div 
            id={bankName === "" ? "hide" : "show"}
            className={`logo ${bankName}`} 
            style={{ 
              backgroundImage: `url(${logo[bankName]})`
            }}
          />
        </div>
        <div id="strip" className="strip">
          <Select
            options={this.props.accountOptions}
            styles={styles}
            placeholder="Receiver account"
            onSelect={this.props.onChangeAccount}
            selectedValue={this.props.accountNumberGuid}
          />
          <Input 
            ref={(input) => { this.accountInput = input; }}
            placeholder="Receiver account"
            name="receiverAccountNumber"
            value={this.props.accountNumber === null ? "" : this.props.accountNumber}
            onChange={this.props.onChange}
            style={
              !this.props.isInput
                ? {
                  opacity: "0",
                  pointerEvents: "none",
                  userSelect: "none"
                }
                : {}
            }
          />
          <i
            className="fas fa-arrow-circle-left"
            style={
              !this.props.isInput || this.props.accountOptions.length < 2
                ? {
                  opacity: "0",
                  pointerEvents: "none",
                  userSelect: "none"
                }
                : {}
            }
            onClick={this.handleBackClick}
          />
          <span className="bar"/>
        </div>       
        <div className="name">
          {this.props.username}
        </div>
      </div>
    );
  }
}

export default ReceiverCard;