import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Button, ControlLabel } from "react-bootstrap";
import ReceiverCard from "./ReceiverCard";
import SenderCard from "./SenderCard";
import { createTx, getInternalTxsTemplates, createInternalTxTemplate } from "ibankActions/transactions";
import { parseResponse } from "helpers/parseResponse";
import { reset } from "ibankActions/confirmation";
import validate from "helpers/validation";
import Joi from "@hapi/joi";
import swal from "sweetalert";
import Select from "ibankComponents/UI/MultiSelect";
import Input from "ibankComponents/UI/Input";
import { getUserData, isIndividualWithEntities } from "services/paymentBackendAPI/backendPlatform";
import { getAllUsers as getOwnLogins } from "ibankActions/users";
import { getAllAccounts } from "ibankActions/accounts";
import InputHandler from "ibankComponents/UI/Input/Handler"

const isIndividual = isIndividualWithEntities();
const userData = getUserData();

class SendForm extends Component {

  state = {
    isInput: false,
    receiverAccountNumber: null,
    receiverName: "",
    receiverCurrencies: [],
    senderAccountNumber: null,
    sender_currency: "",
    receiver_currency: "",
    amount: "",
    accountOptions: [],
    allReceiverAccountOptions: [],
    receiverAccountOptions: [],
    ownName: "",
    width: 0,
    selectedTemplate: "",
    transactionsTemplates: [],
    templateName: ""
  }

  schema = {
    sender_number: Joi.string().length(11).required().label("Your account"),
    receiver_number: Joi.string().length(11).required().label("Receiver account"),
    amount: Joi.number().min(0.01).max(999999999.99).required().label("Amount"),
  }

  static getDerivedStateFromProps = async(nextProps) => {
    if (Object.entries(nextProps.response).length > 0 && nextProps.confirmation) {
      swal({
        title: "Transaction was send",
        text: `ID: ${nextProps.response.guid}`,
        icon: "success"
      });
      nextProps.reset();
      await nextProps.getInternalTxsTemplates({})
      return {
        isInput: false,
        receiverAccountNumber: null,
        receiverName: "",
        receiverCurrencies: [],
        senderAccountNumber: null,
        sender_currency: "",
        receiver_currency: "",
        amount: "",
        templateName: ""
      };
    }
    return null;
  }

  componentDidMount = async () => {
    this.updateWindowStyles();
    window.addEventListener("resize", this.updateWindowStyles);
    await this.props.getOwnLogins();
    await this.props.getInternalTxsTemplates({})
    const [ownLogin] = this.props.users.filter(user => user.guid === userData.loginGuid);
    let accountOptions = this.props.accounts.map(account => {
      return account.balances.map(item => {
        const balance = (item.balance - item.reserved).toFixed(2);
        return {
          guid: account.number + "|" + item.currency,
          currency: item.currency,
          balance,
          name: <div style={{ font: this.state.width > 1000 ? "1vw Roboto" : "calc(5px + 0.5vw) Roboto" }}><b style={{ font: this.state.width > 1000 ? "1.2vw Roboto" : "calc(8px + 0.5vw) Roboto" }}>{account.number}</b> ({balance} {item.currency})</div>,
        };
      });
    });
    let accounts = this.props.accounts;
    let allReceiverAccountOptions = accounts.map(account => {
      return account.balances.map(item => {
        const balance = (item.balance - item.reserved).toFixed(2);
        return {
          guid: account.number + "|" + item.currency,
          balance,
          currency: item.currency,
          name: <div style={{ font: "calc(0.5vw + 5px) Roboto" }}><b style={{ font: "calc(0.8vw + 8px)  Roboto" }}>{account.number}</b> ({balance} {item.currency})</div>,
        };
      });
    });
    if (isIndividual) {
      allReceiverAccountOptions = [
        ...allReceiverAccountOptions,
        ...this.props.entitiesAccounts.map((account, index) => {
          return account.balances.map((item, i) => {
            const balance = Math.round(item.balance) - Math.round(item.reserved)
            return {
              guid: account.number + "|" + item.currency,
              entity: true,
              firstEntity: index === 0 && i === 0,
              currency: item.currency,
              balance,
              name: <div style={{ font: "calc(0.5vw + 5px) Roboto" }}>
                <b style={{ font: "calc(0.8vw + 8px)  Roboto" }}>
                  {account.number}
                </b>
                <b>
                  {` ${account.entity_name || account.login} `}
                </b>
                ({balance} {item.currency})
              </div>,
            };
          });
        })
      ];
    }
    accountOptions = accountOptions.flat();
    allReceiverAccountOptions = allReceiverAccountOptions.flat();
    allReceiverAccountOptions.push({
      guid: "other",
      other: true,
      name: <div style={{ font: "calc(0.8vw + 8px) Roboto", textTransform: "uppercase", textAlign: "left" }}><b>other</b></div>
    });
    this.setState({
      accountOptions,
      allReceiverAccountOptions,
      receiverAccountOptions: allReceiverAccountOptions,
      isInput: allReceiverAccountOptions.length > 1 ? this.state.isInput : true,
      ownName: ownLogin.username,
      transactionsTemplates: this.props.transactionsTemplates,
      templateName: ""
    });
  }

  updateWindowStyles = () => {
    this.setState({
      width: window.innerWidth
    });
  }

  receiverAccountChange = (option) => {
    if (option.other) {
      this.setState({
        isInput: true,
        receiverAccountNumber: "",
        receiverAccountText: "",
        receiverName: ""
      });
      this.receiverComponent.accountInput.focus();
    }
    else {
      const accounts = [
        ...this.props.accounts,
        ...this.props.entitiesAccounts
      ];
      const [account] = accounts.filter(account => account.number === option.guid.split("|")[0]);
      const [user] = this.props.users.filter(user => user.guid === account.login_guid || user.guid === account.entity_guid);
      this.setState({
        receiverAccountNumber: option.guid.split("|")[0],
        receiverAccountText: option.name,
        receiver_currency: option.currency,
        receiverName: user.username,
        selectedTemplate: ""
      });
    }
  }

  senderAccountChange = (option) => {
    const { allReceiverAccountOptions } = this.state;
    let receiverAccountOptions = allReceiverAccountOptions.filter(receiverOption => option.guid !== receiverOption.guid);
    let flag = true;
    receiverAccountOptions.forEach((option, index) => {
      if (option.entity && index > 0 && flag) {
        option.firstEntity = true;
        flag = false;
      }
    });
    this.setState({
      senderAccountNumber: option.guid.split("|")[0],
      senderAccountText: option.name,
      sender_currency: option.currency,
      receiverAccountNumber: null,
      receiverAccountOptions,
      isInput: receiverAccountOptions.length > 1 ? this.state.isInput : true,
      selectedTemplate: ""
    });
  }

  handleChange = async (e) => {
    const property = e.target.name;
    let value = e.target.value;
    if (property === "amount") {
      value = value.replace(".", "");
      let start = value.substr(0, value.length - 2);
      let middle = ".";
      let finish = value.substr(value.length - 2, value.length);
      value = start + middle + finish;
      switch (value.length) {
        case 2: {
          value = value[value.length - 1] + ".00";
          break;
        }
        case 3: {
          value = "0" + value;
          break;
        }
        default: {
          if (value[0] === "0") {
            value = value.substr(1, value.length - 1);
          }
          break;
        }
      }
    }
    const request = {
      sender_number: this.state.senderAccountNumber,
      receiver_number: this.state.receiverAccountNumber,
      sender_currency: this.state.sender_currency,
      receiver_currency: this.state.receiver_currency || this.state.sender_currency,
      value
    };
    this.setState({
      [property]: value,
      selectedTemplate: ""
    });
    if (validate(request, this.schema)) {
      await this.props.createTx(request, "Internal");
      this.setState({
        commission: this.props.response.commission
      })
    }
  }

  handleTemplateSelect = async(option) => {
    const template = this.state.transactionsTemplates.find(template => template.name === option.name);
    
    await this.setState({
      isInput: !this.state.receiverAccountOptions.find(item => item.guid.split("|")[0] === template.to_account),
      selectedTemplate: option.name
    });

    await this.setState({
      senderAccountNumber: template.from_account,
      sender_currency: template.from_currency_code,
      receiverAccountNumber: template.to_account,
      receiver_currency: template.to_currency_code,
      amount: template.amount
    })

    await this.props.createTx({
      sender_number: this.state.senderAccountNumber,
      receiver_number: this.state.receiverAccountNumber,
      sender_currency: this.state.sender_currency,
      receiver_currency: this.state.receiver_currency || this.state.sender_currency,
      value: this.state.amount
    }, "Internal");
    this.setState({
      commission: this.props.response.commission
    });
  }

  changeIsInput = (value) => {
    this.setState({ isInput: value });
  }

  handleAddTxTemplate = async(e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    const isExists = this.state.transactionsTemplates.find(item => item.name === this.state.templateName)
    if(!this.state.templateName) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter template name",
        icon: "warning"
      });
    }
    else if(isExists) {
      this.setState({ isLoading: false });
      swal({
        title: "Validation error",
        text: `Transaction template with name '${this.state.templateName}' already exists`,
        icon: "warning"
      });
    }
    else {
      try {
        await this.props.createInternalTxTemplate({
          name: this.state.templateName,
          from_account: this.state.senderAccountNumber,
          to_account: this.state.receiverAccountNumber,
          from_currency: this.state.sender_currency,
          to_currency: this.state.receiver_currency || this.state.sender_currency,
          amount: this.state.amount
        })
        await this.props.getInternalTxsTemplates({})
        swal({
          title: "Template was created",
          icon: "success"
        });
        this.setState({
          confirmation: false,
          transactionsTemplates: this.props.transactionsTemplates,
        });
      }
      catch(error) {
        const parsedError = parseResponse(error);
          if ((error.response && error.response.status !== 409) || !error.response)
            swal({
              title: parsedError.error,
              text: parsedError.message,
              icon: "error",
            });
      }
    }
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const request = {
        sender_number: this.state.senderAccountNumber,
        receiver_number: this.state.receiverAccountNumber,
        sender_currency: this.state.sender_currency,
        receiver_currency: this.state.receiver_currency || this.state.sender_currency,
        value: this.state.amount,
        amountWithRates: +this.state.amount + +this.state.commission
      };
      validate(request, this.schema);
      await this.props.createTx(request, "Internal");
      swal({
        title: "Transaction was send",
        text: `ID: ${this.props.response.guid}`,
        icon: "success"
      });
      await this.props.getAllAccounts();
      let accountOptions = this.props.accounts.map(account => {
        return account.balances.map(item => {
          const balance = (item.balance - item.reserved).toFixed(2);
          return {
            guid: account.number + "|" + item.currency,
            currency: item.currency,
            balance,
            name: <div style={{ font: this.state.width > 1000 ? "1vw Roboto" : "calc(5px + 0.5vw) Roboto" }}><b style={{ font: this.state.width > 1000 ? "1.2vw Roboto" : "calc(8px + 0.5vw) Roboto" }}>{account.number}</b> ({balance} {item.currency})</div>,
          };
        });
      });
      let accounts = this.props.accounts;
      let allReceiverAccountOptions = accounts.map(account => {
        return account.balances.map(item => {
          const balance = (item.balance - item.reserved).toFixed(2);
          return {
            guid: account.number + "|" + item.currency,
            balance,
            currency: item.currency,
            name: <div style={{ font: "calc(0.5vw + 5px) Roboto" }}><b style={{ font: "calc(0.8vw + 8px)  Roboto" }}>{account.number}</b> ({balance} {item.currency})</div>,
          };
        });
      });
      if (isIndividual) {
        allReceiverAccountOptions = [
          ...allReceiverAccountOptions,
          ...this.props.entitiesAccounts.map((account, index) => {
            return account.balances.map((item, i) => {
              const balance = Math.round(item.balance) - Math.round(item.reserved)
              return {
                guid: account.number + "|" + item.currency,
                entity: true,
                firstEntity: index === 0 && i === 0,
                currency: item.currency,
                balance,
                name: <div style={{ font: "calc(0.5vw + 5px) Roboto" }}>
                  <b style={{ font: "calc(0.8vw + 8px)  Roboto" }}>
                    {account.number}
                  </b>
                  <b>
                    {` ${account.entity_name || account.login} `}
                  </b>
                  ({balance} {item.currency})
                </div>,
              };
            });
          })
        ];
      }
      accountOptions = accountOptions.flat();
      allReceiverAccountOptions = allReceiverAccountOptions.flat();
      allReceiverAccountOptions.push({
        guid: "other",
        other: true,
        name: <div style={{ font: "calc(0.8vw + 8px) Roboto", textTransform: "uppercase", textAlign: "left" }}><b>other</b></div>
      });
      this.setState({
        isInput: false,
        receiverAccountNumber: "",
        receiverName: "",
        senderAccountNumber: "",
        amount: "",
        accountOptions,
        allReceiverAccountOptions
      });
    }
    catch (error) {
      const parsedError = parseResponse(error);
      if ((error.response && error.response.status !== 409) || !error.response)
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      const request = {
        sender_number: this.state.senderAccountNumber,
        receiver_number: this.state.receiverAccountNumber,
        sender_currency: this.state.sender_currency,
        receiver_currency: this.state.receiver_currency || this.state.sender_currency,
        value: this.state.amount
      };
      if (validate(request, this.schema)) {
        await this.props.createTx(request, "Internal");
        this.setState({
          commission: this.props.response.commission
        })
      }
    }
  }

  getTemplatesOptions = () => {
    return this.state.transactionsTemplates.map(template => {
      return {
        guid: template.name + template.login_guid,
        name: template.name
      }
    })
  }

  render() {

    const templatesOptions = this.getTemplatesOptions();

    return this.state.width > 1000
      ? (
        <>
          <Row className="row">
            <Col className="col" md={2} lg={2} sm={2}/>
            <Col className="col" md={3} lg={3} sm={3}>
              <ControlLabel
                className="col-label"
              >
                Templates
              </ControlLabel>
              <Select
                // key="template"
                options={templatesOptions}
                // styles={selectorStyles}
                selectedValue={this.state.selectedTemplate}
                onSelect={this.handleTemplateSelect}
                placeholder="Select template"
              />
            </Col>
            <Col className="col" md={2} lg={2} sm={2}/>
            <Col 
              className="col" 
              id={!this.state.selectedTemplate ? "showTemplateButton" : "hideTemplateButton"}
              md={2} lg={2} sm={2}
            >
              <Input
                label="Template name"
                name="templateName"
                value={this.state.templateName}
                onChange={this.handleChange}
              />
              <Button
                className="submit-button next"
                onClick={this.handleAddTxTemplate}
              >
                Add template
              </Button>
            </Col>
            <Col className="col" md={3} lg={3} sm={3}/>
          </Row>
          <Row id="send">
            <Col md={1} id="send" />
            <Col md={4} id="send">
              <SenderCard
                accountOptions={this.state.accountOptions}
                accountNumber={this.state.senderAccountNumber}
                accountNumberGuid={this.state.senderAccountNumber === null ? null : this.state.senderAccountNumber + "|" + this.state.sender_currency}
                onChangeAccount={this.senderAccountChange}
                name={this.state.ownName}
              />
            </Col>
            <Col md={2} id="send">
              <i className="fas fa-arrow-right send-arrow-icon" />
              <InputHandler
                className="input-amount"
                name="amount"
                value={this.state.amount}
                type="number"
                placeholder="Amount"
                onChange={this.handleChange}
              />
              <span id="input-amount" className="bar" />
              {
                this.state.sender_currency &&
                  this.state.receiver_currency &&
                  this.state.sender_currency !== this.state.receiver_currency
                  ?
                  <span
                    style={{
                      position: "relative",
                      top: "7vw",
                      font: "1vw Roboto",
                      marginTop: "30px",
                      color: "red"
                    }}
                  >
                    Your funds will be converted at the bank rate.
                  </span>
                  : null
              }
              <div>
                <span
                  style={{
                    position: "relative",
                    top: "8vw",
                    font: "1vw Roboto",
                    marginTop: "30px"
                  }}
                >
                  {this.state.commission ? `Commission: ${this.state.commission} ${this.state.sender_currency}` : ""}
                </span>
              </div>

              <Button
                className="submit-button send"
                onClick={this.handleSubmit}
              >
                Send
              </Button>
            </Col>
            <Col md={4} id="send">
              <ReceiverCard
                ref={(component) => { this.receiverComponent = component; }}
                isInput={this.state.isInput}
                accountNumber={this.state.receiverAccountNumber}
                accountNumberGuid={this.state.receiverAccountNumber === null ? null : this.state.receiverAccountNumber + "|" + this.state.receiver_currency}
                name={this.state.receiverName}
                changeIsInput={this.changeIsInput}
                onChangeAccount={this.receiverAccountChange}
                onChange={this.handleChange}
                username={this.state.receiverName}
                accountOptions={this.state.receiverAccountOptions}
              />
            </Col>
            <Col md={1} id="send" />
          </Row>
        </>
      )
      : (
        <>
          <Row id="send">
            <Col id="send" sm={4} />
            <Col id="send" sm={4}>
              <SenderCard
                accountOptions={this.state.accountOptions}
                accountNumber={this.state.senderAccountNumber}
                accountNumberGuid={this.state.senderAccountNumber === null ? null : this.state.senderAccountNumber + "|" + this.state.currency}
                onChangeAccount={this.senderAccountChange}
                name={this.state.ownName}
              />
            </Col>
            <Col id="send" sm={4} />
          </Row>
          <Row id="send-arrow">
            <Col id="send" sm={4} />
            <Col id="send" sm={4}>
              <i className="fas fa-arrow-down send-arrow-icon" />
              <InputHandler
                className="input-amount"
                name="amount"
                value={this.state.amount}
                placeholder="Amount"
                onChange={this.handleChange}
              />
              <span id="input-amount" className="bar" />
            </Col>
            <Col id="send" sm={4} />
          </Row>
          <Row id="send">
            <Col id="send" sm={4} />
            <Col id="send" sm={4}>
              <ReceiverCard
                ref={(component) => { this.receiverComponent = component; }}
                isInput={this.state.isInput}
                accountNumber={this.state.receiverAccountNumber}
                name={this.state.receiverName}
                changeIsInput={this.changeIsInput}
                onChangeAccount={this.receiverAccountChange}
                onChange={this.handleChange}
                username={this.state.receiverName}
                accountOptions={this.state.receiverAccountOptions}
              />

              <Button
                className="submit-button send"
                onClick={this.handleSubmit}
              >
                Send
            </Button>
            </Col>

            <Col id="send" sm={4} />
          </Row>
        </>
      );
  }
}

const mapStateToProps = state => ({
  accounts: state.accounts.accounts,
  users: state.users.users,
  entitiesAccounts: state.accounts.txAccounts,
  response: state.transactions.responseTransaction,
  confirmation: state.confirmation.confirmation,
  transactionsTemplates: state.transactions.internalTemplates
});

export default connect(mapStateToProps, {
  createTx,
  getOwnLogins,
  getAllAccounts,
  getInternalTxsTemplates,
  createInternalTxTemplate,
  reset
})(SendForm);