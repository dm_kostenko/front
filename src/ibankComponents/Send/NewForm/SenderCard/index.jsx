import React, { Component } from "react";
import Select from "ibankComponents/UI/MultiSelect";
import tbf from "assets/img/logo/logo.png";

class SenderCard extends Component {

  state = {
    accountText: "",
    accountGuid: "",
    width: 0
  }

  componentDidMount = () => {
    this.updateWindowStyles();
    window.addEventListener("resize", this.updateWindowStyles);
  }

  updateWindowStyles = () => {
    this.setState({
      width: window.innerWidth
    });
  }

  render() { 
    const styles = {
      dropdownIndicator: (styles) => ({ 
        ...styles,
        marginRight: "-moz-calc(10% - 2vw) !important",
        "svg": {
          width: "2vw !important",
          height: "2vw !important",
          maxHeight: "25px !important"
        }
      }),
      indicatorSeparator: () => ({ display: "none" }),
      placeholder: (styles) => ({
        ...styles,
        font: this.state.width > 1000 ? "calc(0.8vw + 8px) Roboto" : "12px Roboto",
        marginLeft: this.state.width > 1000 ? "6vw !important" : "calc(1vw + 55px)",
        opacity: "0.5 !important",
        color: "#797d8a !important"
      }),
      control: (styles) => ({
        ...styles,                
        maxWidth: "500px !important",   
        maxHeight: "63px !important",
        minWidth: "200px !important",
        minHeight: "25px !important",
        height: "3vw",
        border: "none !important",
        borderRadius: "0 !important",
        boxShadow: "none !important",
      }),
      menu: (styles) => ({
        ...styles,
        marginTop: "2px",          
        borderTopLeftRadius: "0",
        borderTopRightRadius: "0"
      }),
      option: (styles, state) => ({
        ...styles,
        paddingLeft: this.state.width > 1000 ? "4vw !important" : "calc(1vw + 30px)",
        textAlign: "left !important",        
        opacity: state.data.balance == "0.00" ? "0.3 !important" : "1",
        pointerEvents: state.data.balance == "0.00" ? "none !important" : "",
        userSelect: state.data.balance == "0.00" ? "none !important" : ""   
      }),
      singleValue: (styles) => ({
        ...styles,
        lineHeight: "3vw",
        paddingLeft: this.state.width > 1000 ? "4vw !important" : "calc(1vw + 30px)",
      })
    };
    return (
      <div className="card send tbf">
        <div className="bank-info">
          <div 
            className="logo tbf" 
            style={{ 
              backgroundImage: `url(${tbf})`
            }}
          />
        </div>
        <div className="strip">
          <Select
            options={this.props.accountOptions}
            styles={styles}
            placeholder="Your account"
            selectedValue={this.props.accountNumberGuid}
            onSelect={this.props.onChangeAccount}
          />
        </div>
        <div className="name">
          {this.props.name}
        </div>
      </div>
    );
  }
}

export default SenderCard;