import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "react-bootstrap";
import { createTx, upsertTxsTemplates } from "ibankActions/transactions";
import { getAllAccounts } from "ibankActions/accounts";
import config from "config/index"
import validate from "helpers/validation";
import Joi from "@hapi/joi";
import { reset } from "ibankActions/confirmation";
import { BICRegExp, IBANRegExp } from "helpers/ibanAndBicValidation";
import { parseResponse } from "helpers/parseResponse";
import swal from "sweetalert";
import Spinner from "ibankComponents/UI/Spinner"


class DetailedCreditTransfer extends Component {
  state = {
    systemOptions: [
      {
        guid: "LITAS-MIG",
        name: "LITAS-MIG"
      },
      {
        guid: "LITAS-RLS",
        name: "LITAS-RLS"
      }
    ],
    accountOptions: [
      {
        guid: config.iban.settlement.internal,
        // name: `${config.iban.settlement.internal} (corporate)`
        name: "Corporate"
      },
      {
        guid: config.iban.customer.internal,
        // name: `${config.iban.customer.internal} (customer)`
        name: "Customer"
      }
    ],
    account: config.iban.settlement.internal,
    system: "LITAS-MIG",
    showBicAndName: "hideData",
    error: "",
    next: this.props.next || false,
    validation: false,
    addToTemplates: false
  }

  schema = {
    amount: Joi.number().min(0.01).max(999999999.99).required().label("Amount"),
    // creditorName: Joi.string().required().label("Receiver name"),       // add name validator
    // creditorBic: Joi.string().required().regex(BICRegExp).label("Receiver BIC"),
    creditorAccount: Joi.string().required().regex(IBANRegExp).label("Receiver IBAN"),
    ClrSysPrtry: Joi.equal("LITAS-MIG", "LITAS-RLS").required(),
    remittanceInformation: Joi.string().required().label("Description"),
    // ibanFields: [{               // IBAN validation
    //   field: "creditorAccount",
    //   label: "Receiver account"
    // }]
  }

  static getDerivedStateFromProps = async (nextProps) => {
    if (Object.entries(nextProps.response).length > 0 && nextProps.confirmation) {
      swal({
        title: "Transaction was send",
        text: `ID: ${nextProps.response.guid}`,
        icon: "success"
      });
      nextProps.reset();
      return {
        system: "LITAS-MIG",
        amount: 0,
        showBicAndName: "hideData",
        creditorBIC: "",
        creditorName: "",
        creditorIBAN: "",
        remittanceInformation: "",
        next: false,
        validation: false
      };
    }
    // return this.state;
  }

  componentDidMount = async() => {
    // await this.props.getBalancesDocs("1", { firstTwo: true })
    await this.props.getAllAccounts()
    const settlement = this.props.accounts.find(a => a.iban === config.iban.settlement.internal)
    const customer = this.props.accounts.find(a => a.iban === config.iban.customer.internal)
    // const settlementBalance = settlement?.balances?.find(b => b.currency === "EUR")
    // const customerBalance = customer?.balances?.find(b => b.currency === "EUR")

    this.setState({
      accountOptions: [
        {
          guid: config.iban.settlement.internal,
          // name: `${config.iban.settlement.internal} (corporate)`
          name: `Corporate`
        },
        {
          guid: config.iban.customer.internal,
          // name: `${config.iban.customer.internal} (customer)`
          name: `Customer`
        }
      ]
    })
    this.structure[2].children[0].options = [
      {
        guid: config.iban.settlement.internal,
        // name: `${config.iban.settlement.internal} (corporate)`
        name: `Corporate`
      },
      {
        guid: config.iban.customer.internal,
        // name: `${config.iban.customer.internal} (customer)`
        name: `Customer`
      }
    ]
  }

  structure = [
    {
      title: "",
      children: [
        {
          title: "Clearing system",
          name: "system",
          select: true,
          options: "systemOptions"
        },
      ]
    },
    {
      title: "Pay from",
      children: [
        {
          title: "Account",
          name: "account",
          select: true,
          options: "accountOptions"
        }
      ]
    },
    {
      title: "Beneficiary",
      children: [
        {
          title: "Account (IBAN)",
          name: "creditorIBAN"
        },
        {
          title: "BIC",
          name: "creditorBIC",
          id: this.state.showBicAndName
        },
        {
          title: "Name",
          name: "creditorName",
          id: this.state.showBicAndName
        },
      ]
    },
    {
      title: "",
      children: [
      
        {
          title: "Amount",
          name: "amount"
        },
        {
          title: "Currency",
          default: "EUR",
          disabled: true
        },
        {
          title: "Payment description",
          name: "remittanceInformation",
        }
      ]
    },
    {
      title: "",
      children: [
        {
          title: "Add to my templates",
          name: "addToTemplates",
          checkbox: true,
          id: "submitCheckbox"
        }
      ]
    }
  ]

  nextStructure = [
    {
      title: "",
      children: [
        {
          title: "Clearing system",
          name: "system",
        },
      ]
    },
    {
      title: "Pay from",
      children: [
        {
          title: "Account",
          name: "account",
        }
      ]
    },
    {
      title: "Beneficiary",
      children: [
        {
          title: "Account (IBAN)",
          name: "creditorIBAN"
        },
        {
          title: "BIC",
          name: "creditorBIC",
        },
        {
          title: "Name",
          name: "creditorName",
        },
      ]
    },
    {
      title: "",
      children: [
        {
          title: "Amount",
          name: "amount"
        },
        {
          title: "Currency",
          default: "EUR",
          disabled: true
        },
        {
          title: "Payment description",
          name: "remittanceInformation",
        }
      ]
    }
  ]

  validate = async () => {
    const {
      system,
      amount,
      // creditorBIC,
      // creditorName,
      creditorIBAN,
      remittanceInformation
    } = this.state;

    const validation = !!system &&
      !!amount &&
      // !!creditorBIC &&
      // !!creditorName &&
      !!creditorIBAN &&
      !!remittanceInformation;
    await this.setState({ validation, error: "" })
  }

  handleChange = async (e, checked) => {
    if(checked)
      await this.setState({ [e.target.name]: !this.state[e.target.name] })
    else
      await this.setState({ [e.target.name]: e.target.value })
    await this.validate();
  }

  handleSelect = async (e, name) => {
    await this.setState({ [name]: e.target.value })
    await this.validate();
  }

  handleSave = async() => {
    const {
      account,
      system,
      addToTemplates,
      amount,
      creditorBIC,
      creditorName,
      creditorIBAN,
      remittanceInformation
    } = this.state;

    await this.props.upsertTxsTemplates({
      name: creditorName, 
      from_account: account, 
      to_account: creditorIBAN, 
      from_currency: "EUR",
      to_currency: "EUR",
      to_name: creditorName,
      to_bic: creditorBIC, 
      currency_code: "EUR", 
      amount: amount, 
      description: remittanceInformation,
      once: 'true'
    })
    if(this.props.response.empty) {
      this.setState({
        showBicAndName: "showData",
        error: "Please enter beneficiary BIC and Name"
      })
    }
    else if(this.props.responseUpsertTxsTemplates.guid) {
      swal({
        title: "Transaction was saved",
        text: `ID: ${this.props.responseUpsertTxsTemplates.guid}`,
        icon: "success"
      });
      if(addToTemplates)
        await this.props.upsertTxsTemplates({
          name: creditorName, 
          from_account: account, 
          to_account: creditorIBAN, 
          from_currency: "EUR",
          to_currency: "EUR",
          to_name: creditorName,
          to_bic: creditorBIC, 
          currency_code: "EUR", 
          amount: amount, 
          description: remittanceInformation,
          once: 'false'
        })
      this.setState({
        system: "LITAS-MIG",
        amount: "",
        showBicAndName: "hideData",
        creditorBIC: "",
        creditorName: "",
        creditorIBAN: "",
        remittanceInformation: "",
        validation: false
      })
    }
  }


  handleSubmit = async(e) => {
    const {
      account,
      system,
      addToTemplates,
      amount,
      creditorBIC,
      creditorName,
      creditorIBAN,
      remittanceInformation
    } = this.state;

    try {
      let request = {
        amount,
        amountWithRates: amount,
        debtorName: "TBF Finance, UAB",
        debtorBic: config.bic,
        debtorAccount: account,
        creditorName,
        creditorAccount: creditorIBAN,
        creditorBic: creditorBIC,
        ClrSysPrtry: system,
        remittanceInformation,
        currency: "EUR",
      }
      if(this.state.next) {
        if(addToTemplates)
          await this.props.upsertTxsTemplates({
            name: creditorName, 
            from_account: account, 
            to_account: creditorIBAN, 
            from_currency: "EUR",
            to_currency: "EUR",
            to_name: creditorName,
            to_bic: creditorBIC, 
            currency_code: "EUR", 
            amount: amount, 
            description: remittanceInformation,
            once: 'false'
          })
        request.isNext = true
      }
      validate(request, this.schema)
      await this.props.createTx(request, "FFCCTRNS");
      if(this.props.response.empty) {
        this.setState({
          showBicAndName: "showData",
          error: "Please enter beneficiary BIC and Name"
        })
      }
      else if(this.props.response.next) {
        let newState = { next: true }
        if(this.props.response.creditorName)
          newState.creditorName = this.props.response.creditorName
        this.setState(newState)
      }
      else if(this.props.response.guid) {
        swal({
          title: "Transaction was send",
          text: `ID: ${this.props.response.guid}`,
          icon: "success"
        });
        this.setState({
          system: "LITAS-MIG",
          amount: "",
          showBicAndName: "hideData",
          creditorBIC: "",
          creditorName: "",
          creditorIBAN: "",
          remittanceInformation: "",
          validation: false
        })
      }
    }
    catch(error) {
      const parsedError = parseResponse(error);
      if ((error.response && error.response.status !== 409) || !error.response) {
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
        this.setState({
          system: "LITAS-MIG",
          amount: "",
          showBicAndName: "hideData",
          creditorBIC: "",
          creditorName: "",
          creditorIBAN: "",
          remittanceInformation: "",
          validation: false
        })
      }
    }
  }

  generateForm = el => {
    if(Array.isArray(el))
      return el.map(i => this.generateForm(i))
    if(el.children)
      return (
        <>
          <div className="detail-credit-transfer-header">
            {el.title}
          </div>
          {el.children.map(i => this.generateForm(i))}
        </>
      )
    return (
      <div className="detail-credit-transfer-row">
        <div               
          id={el.checkbox ? "" : el.id ? this.state.showBicAndName : ""}
          className={el.bold ? `detail-credit-transfer-label bold` : "detail-credit-transfer-label"}
        >
          {el.title}
        </div>
        <div className={el.checkbox ? "detail-credit-transfer-value _checkbox" : el.bold ? `detail-credit-transfer-value bold` : "detail-credit-transfer-value"}>
          {el.select
            ? <select onChange={(e) => this.handleSelect(e, el.name)}>
              {this.state[el.options].map(o => 
                <option 
                  value={o.guid} 
                  selected={this.state[el.name] === o.guid}
                >
                  {o.name}
                </option>)
              }
            </select>
            : <input 
              id={el.checkbox ? el.id : el.id ? this.state.showBicAndName : ""}
              name={el.name}
              type={el.checkbox ? "checkbox" : el.name === "amount" ? "number" : "text"}
              onChange={(e) => this.handleChange(e, el.checkbox)}
              value={el.default || this.state[el.name]}
              checked={el.checkbox ? this.state[el.name] : ""}
              disabled={el.disabled}
            />}
        </div>
      </div>
    )
  }

  generateNextForm = () => {
    return (
      <>
        <div className="detail-credit-transfer-row-next">
          <div               
            className="detail-credit-transfer-label bolder"
          >
            Date
          </div>
          <div className="detail-credit-transfer-value">
            {new Date().toISOString().slice(0, 10)}
          </div>
        </div>
        <div className="detail-credit-transfer-row-next">
          <div               
            className="detail-credit-transfer-label bolder"
          >
            Beneficiary
          </div>
          <div className="detail-credit-transfer-value">
            {this.state.creditorName}
          </div>
        </div>
        <div className="detail-credit-transfer-row-next">
          <div               
            className="detail-credit-transfer-label bolder"
          >
            Beneficiary account
          </div>
          <div className="detail-credit-transfer-value">
            {this.state.creditorIBAN}
          </div>
        </div>
        <div className="detail-credit-transfer-row-next">
          <div               
            className="detail-credit-transfer-label bolder"
          >
            Remitter account
          </div>
          <div className="detail-credit-transfer-value">
            {this.state.account}
          </div>
        </div>
        <div className="detail-credit-transfer-row-next">
          <div               
            className="detail-credit-transfer-label bolder"
          >
            Amount
          </div>
          <div className="detail-credit-transfer-value">
            {parseFloat(this.state.amount).toFixed(2)} EUR
          </div>
        </div>
        <div className="detail-credit-transfer-row-next">
          <div               
            className="detail-credit-transfer-label bolder"
          >
            Description
          </div>
          <div className="detail-credit-transfer-value">
            {this.state.remittanceInformation}
          </div>
        </div>
      </>
    )
  }

  render() {
    return this.props.loading 
    ? <Spinner/> 
    : this.state.next 
      ?
        (
          <div>
            <div className="detail-credit-transfer">
              {this.generateNextForm()}
              <div 
                className="detail-credit-transfer-button"
              >
                <Button className="submit-button" onClick={this.handleSubmit}>
                  Confirm
                </Button>
              </div>
            </div>
          </div>
        )
      : 
        (
          <div>
            <div className="detail-credit-transfer">
              {this.generateForm(this.structure)}
              <div className="error">{this.state.error}</div>
              <div 
                className="detail-credit-transfer-button"
              >
                <Button className="submit-button save-for-later" onClick={this.handleSave}>
                  Save for later
                </Button>
                <Button className="submit-button" onClick={this.handleSubmit}>
                  Continue
                </Button>
              </div>
            </div>
          </div>
        )
  }
}

const mapStateToProps = (state) => {
  return {
    responseUpsertTxsTemplates: state.transactions.responseUpsertTxsTemplates,
    response: state.transactions.responseTransaction,
    loading: state.accounts.loadingBalancesDocs,
    accounts: state.accounts.accounts
  };
};

export default connect(mapStateToProps, {
  createTx,
  getAllAccounts,
  upsertTxsTemplates,
  reset
})(DetailedCreditTransfer);