import React, { Component } from "react";
import { Row, Col, Button, ControlLabel } from "react-bootstrap";
import { connect } from "react-redux";
import Card from "../UI/Card";
import Input from "../UI/Input";
import Spinner from "../UI/Spinner";
import { getAllUsers as getOwnLogins } from "ibankActions/users";
import { createTx, getAllTxs, getExternalTxsTemplates, createExternalTxTemplate } from "ibankActions/transactions";
import { getAllAccounts, getTxAccounts } from "ibankActions/accounts";
import { getUserData, getUserLoginsData } from "../../services/paymentBackendAPI/backendPlatform";
import { parseResponse } from "helpers/parseResponse";
import { reset } from "ibankActions/confirmation";
import Select from "ibankComponents/UI/MultiSelect";
import PropTypes from "prop-types";
import swal from "sweetalert";
import { getUserType } from "services/paymentBackendAPI/backendPlatform";
import moment from "moment";
import validate from "helpers/validation";
import Joi from "@hapi/joi";
import { BICRegExp, IBANRegExp } from "helpers/ibanAndBicValidation";
import NewForm from "./NewForm";
import DetailedCreditTransfer from "./Detailed/CreditTransfer";
import config from "config/index"
import { getCETDate } from "helpers/target2Timing";

const userType = getUserType();
const isAdmin = userType === "admin";
const userData = getUserData();

class Send extends Component {
  state = {
    height: 0,

    txRef: {
      "Credit transfer": "FFCCTRNS",
      "Payment cancellation": "FFPCRQST",
      "Payment return": "PRTRN",
      "Resolution of investigation": "ROINVSTG",
      "Internal transaction": "Internal",
      "Internal": "Internal",
      "External": "FFCCTRNS",
      "Detailed credit transfer": "Detailed credit transfer"
    },
    entitySelector: userType === "individual",
    transactions: [],
    accounts: [],
    currencyOptions: [],
    internalCurrencyOptions: [],
    transactionsTemplates: [],
    templateName: "",
    currencies: {
      USD: "$",
      EUR: "€",
      RUB: "₽"
    },
    selectedTxType: isAdmin ? "Detailed credit transfer" : "External",
    selectedTemplate: "",

    // FFCCTRNS
    IBAN: "",
    accountNumber: "",
    currency: null,
    amount: "0.00",
    creditorName: "Test name",
    creditorAccount: "AT802011129214844493",
    creditorBIC: "LIABLT2XMSD",
    remittance: "",
    commission: "",
    //FFPCRQST
    transactionId: null,
    clrSys: "LITAS-MIG",
    originatorClient: "TBF Finance, UAB",
    reasonInfo: "DUPL",
    additionalInformation: "",
    clearingSystems: [
      {
        guid: "LITAS-MIG",
        name: "LITAS-MIG"
      }
    ],

    //PRTRN
    reasonForReturn: "",    // allowed values: AC01, AC04, AC06, AG01, AG02, AM05, BE04, FOCR, MD07, MS02, MS03, RC01, RR01, RR02, RR03, RR04
    chrgsInfAmt: "",         // Charges amount Fee for the operation. Can only be completed if TxInf/RtrRsnInf/Rsn/Cd=FOCR
    rtrRsnInfAddtInf: "",   // if reason: FOCR

    //ROINVSTG
    reasonForNegativeAnswer: "",
    cxlStsRsnInfAddtInf: "",

    //Internal
    senderNumber: "",
    senderIBAN: "",
    receiverNumber: "",
    receiverIBAN: "",
    internalCurrency: null,
    value: "0.00",
    belonging: "",

    validation: false,
  }


  schemas = {
    FFCCTRNS: {
      amount: Joi.number().min(0.01).max(999999999.99).required().label("Amount"),
      creditorName: Joi.string().required().label("Receiver name"),       // add name validator
      creditorBic: Joi.string().required().regex(BICRegExp).label("Receiver BIC"),
      creditorAccount: Joi.string().required().regex(IBANRegExp).label("Receiver IBAN"),
      remittanceInformation: Joi.string().required().label("Description"),
      // ibanFields: [{               // IBAN validation
      //   field: "creditorAccount",
      //   label: "Receiver account"
      // }]
      // add currency field
    },
    FFPCRQST: {
      CxlRsnInf: Joi.string().required().label("Reason info"),
      CxlRsnInfAddtInf: Joi.any().when("CxlRsnInf", { is: Joi.valid("FRAD"), then: Joi.string().required().label("Additional info"), otherwise: Joi.any() }),
    },
    PRTRN: {},
    ROINVSTG: {},
    Internal: {
      value: Joi.number().min(0).max(1000000).required().label("Amount")
    }
  }

  static getDerivedStateFromProps = async (nextProps) => {
    if (Object.entries(nextProps.response).length > 0 && nextProps.confirmation && nextProps.response.guid) {
      swal({
        title: "Transaction was send",
        text: `ID: ${nextProps.response.guid}`,
        icon: "success"
      });
      nextProps.reset();
      await nextProps.getAllAccounts();
      return {
        IBAN: "",
        accountNumber: null,
        currency: null,
        currencyOptions: [],
        amount: "",
        creditorName: "Test name",
        creditorAccount: "AT802011129214844493",
        remittance: "",
        transactionId: null,
        reasonForReturn: null,
        chrgsInfAmt: "",
        rtrRsnInfAddtInf: "",
        internalCurrency: null,
        senderNumber: null,
        receiverNumber: null,
        value: "",
        validation: false,
        confirmation: false,
        templateName: "",
        selectedTemplate: "",
        next: false
      };
    }
    return null;
  }



  componentDidMount = async () => {
    this.updateWindowStyles();
    window.addEventListener("resize", this.updateWindowStyles);
    await this.props.getExternalTxsTemplates({});
    await this.props.getAllTxs("ffcctrns");
    await this.props.getAllAccounts();
    await this.props.getExternalTxsTemplates({});
    await this.props.getTxAccounts(userType === "individual" ? { entities: true } : {});
    if (this.state.entitySelector && userData.logins.length > 1)
      await this.props.getOwnLogins();
    this.setState({
      userData,
      transactions: this.props.transactions,
      accounts: this.props.accounts,
      IBAN: "",
      accountNumber: "",
      currency: null,
      amount: "0.00",
      creditorName: "Test name",
      creditorAccount: "AT802011129214844493",
      remittance: "",
      transactionId: null,
      reasonForReturn: "",
      chrgsInfAmt: "",
      rtrRsnInfAddtInf: "",
      validation: false,
      confirmation: false,
      transactionsTemplates: this.props.transactionsTemplates,
      templateName: "",
      selectedTemplate: ""
    });
  }

  updateWindowStyles = () => {
    this.setState({
      height: window.outerHeight
    });
  }

  handleAddTxTemplate = async(e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    const isExists = this.state.transactionsTemplates.find(item => item.name === this.state.templateName)
    if(!this.state.templateName) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter template name",
        icon: "warning"
      });
    }
    else if(isExists) {
      this.setState({ isLoading: false });
      swal({
        title: "Validation error",
        text: `Transaction template with name '${this.state.templateName}' already exists`,
        icon: "warning"
      });
    }
    else {
      try {
        await this.props.createExternalTxTemplate({
          name: this.state.templateName,
          from_account: this.state.accountNumber,
          to_account: this.state.creditorAccount,
          to_bic: this.state.creditorBIC,
          currency_code: this.state.currency,
          amount: this.state.amount,
          description: this.state.remittance
        })
        await this.props.getExternalTxsTemplates({})
        swal({
          title: "Template was created",
          icon: "success"
        });
        this.setState({
          confirmation: false,
          transactionsTemplates: this.props.transactionsTemplates,
        });
      }
      catch(error) {
        const parsedError = parseResponse(error);
          if ((error.response && error.response.status !== 409) || !error.response)
            swal({
              title: parsedError.error,
              text: parsedError.message,
              icon: "error",
            });
      }
    }
  }

  handleTxTypeSelect = async (option) => {
    await this.setState({
      isLoading: true,
      selectedTxType: option.name,
    })
    await this.props.getExternalTxsTemplates({});
    await this.props.getAllAccounts();
    await this.props.getTxAccounts(userType === "individual" ? { entities: true } : {});
    await this.props.getAllTxs("ffcctrns");
    const accounts = this.getAccounts();
    if (this.state.entitySelector && userData.logins.length > 1)
      await this.props.getOwnLogins();
    await this.setState({
      accounts,
      IBAN: "",
      accountNumber: null,
      currency: null,
      currencyOptions: [],
      amount: "",
      creditorName: "Test name",
      creditorAccount: "AT802011129214844493",
      remittance: "",
      transactionId: null,
      reasonForReturn: null,
      chrgsInfAmt: "",
      rtrRsnInfAddtInf: "",
      internalCurrency: null,
      senderNumber: null,
      receiverNumber: null,
      value: "",
      validation: false,
      confirmation: false,
      transactionsTemplates: this.props.transactionsTemplates,
      templateName: "",
      selectedTemplate: "",
      isLoading: false
    });

  }
  handleClrSysSelect = async (option) => {
    await this.setState({
      clrSys: option.name
    });
    await this.setState({
      validation: this.validation()
    });
  }
  handleCurrencySelect = async (option) => {
    await this.setState({
      currency: option.guid
    });
    await this.setState({
      validation: this.validation(),
      selectedTemplate: ""
    });
    if (this.state.txRef[this.state.selectedTxType] === "FFCCTRNS" && this.state.validation) {
      await this.props.createTx({
        amount: this.state.amount,
        debtorName: this.state.originatorClient,
        debtorBic: config.bic,
        debtorAccount: this.state.IBAN,
        creditorName: this.state.creditorName,
        creditorAccount: this.state.creditorAccount,
        creditorBic: this.state.creditorBIC,
        remittanceInformation: this.state.remittance,
        currency: this.state.currency
      }, this.state.txRef[this.state.selectedTxType]);
      this.setState({
        commission: this.props.response.commission
      });
    }
  }

  handleInternalCurrencySelect = async (option) => {
    await this.setState({
      internalCurrency: option.guid
    });
    await this.setState({
      validation: this.validation()
    });
  }

  handleAccountSelect = async (option) => {
    const currencyOptions = await this.getAccountCurrencies(option.guid);
    let IBAN = "";
    this.props.accounts.forEach(account => {
      if (account.number === option.guid)
        IBAN = account.iban;
    });
    await this.setState({
      accountNumber: option.guid,
      currencyOptions,
      IBAN,
      currency: ""
    });
    await this.setState({
      validation: this.validation(),
      selectedTemplate: ""
    });
    if (this.state.txRef[this.state.selectedTxType] === "FFCCTRNS" && this.state.validation) {
      await this.props.createTx({
        amount: this.state.amount,
        debtorName: this.state.originatorClient,
        debtorBic: config.bic,
        debtorAccount: this.state.IBAN,
        creditorName: this.state.creditorName,
        creditorAccount: this.state.creditorAccount,
        creditorBic: this.state.creditorBIC,
        remittanceInformation: this.state.remittance,
        currency: this.state.currency
      }, this.state.txRef[this.state.selectedTxType]);
      this.setState({
        commission: this.props.response.commission
      });
    }
  }

  handleSenderAccountSelect = async (option) => {
    let IBAN = "";
    this.props.accounts.forEach(account => {
      if (account.number === option.guid)
        IBAN = account.iban;
    });
    // if(this.validate(this.state.receiverNumber)) {
    //   const receiverCurrencyOptions = await this.getAccountCurrencies(this.state.receiverNumber);
    //   internalCurrencyOptions = internalCurrencyOptions.filter(option => receiverCurrencyOptions.some(receiverCurrency => receiverCurrency.guid === option.guid));
    //   this.setState({ internalCurrencyOptions });
    // }
    await this.setState({
      senderNumber: option.guid,
      receiverNumber: null,
      senderIBAN: IBAN,
      internalCurrencyOptions: [],
      internalCurrency: null
    });
    await this.setState({
      validation: this.validation()
    });
  };

  handleReceiverAccountSelect = async (option) => {
    let internalCurrencyOptions = await this.getAccountCurrencies(option.guid);
    let IBAN = "";
    this.props.accounts.forEach(account => {
      if (account.number === option.guid)
        IBAN = account.iban;
    });
    if (this.validate(this.state.senderNumber)) {
      const senderCurrencyOptions = await this.getAccountCurrencies(this.state.senderNumber);
      internalCurrencyOptions = senderCurrencyOptions.filter(senderCurrency => internalCurrencyOptions.some(option => senderCurrency.guid === option.guid));
      this.setState({ internalCurrencyOptions });
    }
    await this.setState({
      receiverNumber: option.guid,
      receiverIBAN: IBAN
    });
    await this.setState({
      validation: this.validation()
    });
  }

  handleTxSelect = async (option) => {
    await this.setState({
      transactionId: option.guid
    });
    await this.setState({
      validation: this.validation()
    });
  }

  handleReasonCancellationSelect = async (option) => {
    await this.setState({
      reasonInfo: option.guid
    });
    await this.setState({
      validation: this.validation()
    });
  }

  handleReasonSelect = async (option) => {
    await this.setState({
      reasonForReturn: option.guid
    });
    await this.setState({
      validation: this.validation()
    });
  }

  handleReasonNegativeAnswerSelect = async (option) => {
    await this.setState({
      reasonForNegativeAnswer: option.guid
    });
    await this.setState({
      validation: this.validation()
    });
  }

  handleBelongingSelect = (option) => {
    this.setState({
      belonging: option.guid
    });
  }

  getAccountCurrencies = (accountNumber) => {
    let [account] = this.props.accounts.filter(account => account.number === accountNumber);
    if (!account)
      [account] = this.props.entitiesAccounts.filter(account => account.number === accountNumber);
    return account.balances.map(item => {
      const balance = (item.balance - item.reserved).toFixed(2);
      return {
        guid: item.currency,
        balance,
        name: <div style={{ textAlign: "left", fontSize: "calc(8px + 0.3vw)" }}><b>{item.currency}</b> ({balance})</div>
      };
    });
  }


  validate = (value) => {
    return value && value !== "";
  }


  validation = () => {
    switch (this.state.txRef[this.state.selectedTxType]) {
      case "FFCCTRNS":
        return this.validate(this.state.accountNumber) &&
          this.validate(this.state.currency) &&
          this.validate(this.state.amount) &&
          this.validate(this.state.creditorName) &&
          this.validate(this.state.creditorAccount) &&
          this.validate(this.state.creditorBIC) &&
          this.validate(this.state.remittance);
      case "FFPCRQST":
        return this.validate(this.state.transactionId) &&
          this.validate(this.state.clrSys) &&
          this.validate(this.state.reasonInfo) &&
          (this.state.reasonInfo === "FRAD" ? this.validate(this.state.additionalInformation) : true);
      case "PRTRN":
        if (this.state.reasonForReturn === "FOCR")
          return this.validate(this.state.transactionId) &&
            this.validate(this.state.originatorClient) &&
            this.validate(this.state.reasonForReturn) &&
            this.validate(this.state.chrgsInfAmt) &&
            this.validate(this.state.rtrRsnInfAddtInf);
        return this.validate(this.state.transactionId) &&
          this.validate(this.state.originatorClient) &&
          this.validate(this.state.reasonForReturn);
      case "ROINVSTG":
        if (this.state.reasonForNegativeAnswer === "LEGL")
          return this.validate(this.state.transactionId) &&
            this.validate(this.state.reasonForNegativeAnswer) &&
            this.validate(this.state.cxlStsRsnInfAddtInf);
        return this.validate(this.state.transactionId) &&
          this.validate(this.state.reasonForNegativeAnswer);
      case "Internal":
        return this.validate(this.state.senderNumber) &&
          this.validate(this.state.receiverNumber) &&
          this.validate(this.state.internalCurrency) &&
          this.validate(this.state.value) &&
          parseFloat(this.state.value) > 0 &&
          this.state.senderNumber !== this.state.receiverNumber;
      default: return false;
    }
  }

  getAccounts = () => {
    return this.props.accounts.map(account => {
      return {
        guid: account.number,
        name: <div style={{ textAlign: "left", fontSize: "calc(8px + 0.3vw)" }}><b>{account.number}</b> ({account.type})</div>
      };
    });
  }

  getReceiverAccounts = () => {
    let accounts = [];
    if (isAdmin) {
      accounts = this.props.accounts.map(account => {
        return {
          guid: account.number,
          name: <div style={{ textAlign: "left", fontSize: "calc(8px + 0.3vw)" }}><b>{account.number}</b> ({account.type})</div>
        };
      });
    }
    else if (!["Own", ""].includes(this.state.belonging) && this.state.entitySelector && userData.logins.length > 1)    // individual with entities select entity
      accounts = this.props.entitiesAccounts.filter(account => account.entity_guid === this.state.belonging).map(account => {
        return {
          guid: account.number,
          name: <div style={{ textAlign: "left", fontSize: "calc(8px + 0.3vw)" }}><b>{account.number}</b> ({account.type})</div>
        };
      });
    else if (this.state.belonging === "Own" || (this.state.belonging === "" && !(this.state.entitySelector && userData.logins.length > 1)))
      accounts = this.props.accounts.map(account => {
        return {
          guid: account.number,
          name: <div style={{ textAlign: "left", fontSize: "calc(8px + 0.3vw)" }}><b>{account.number}</b> ({account.type})</div>
        };
      });
    return accounts;
  }

  getCancellationTransactions = () => {
    const availableStatuses = ["Sent", "Accepted", "Settled"];
    return this.props.transactions.filter(tx => availableStatuses.includes(tx.status)).map(tx => {
      return {
        guid: tx.msgid,
        name: <div style={{ fontSize: "calc(8px + 0.3vw)" }}>{tx.msgid} ({tx.value} {tx.currency}: {moment(getCETDate(new Date(tx.created_at))).format("DD.MM.YYYY HH:mm:ss")})</div>
      };
    });
  }

  getTemplatesOptions = () => {
    return this.state.transactionsTemplates.map(template => {
      return {
        guid: template.name + template.login_guid,
        name: template.name
      }
    })
  }

  getReturningTransactions = () => {
    return this.props.transactions.filter(tx => tx.status === "Imported").map(tx => {
      return {
        guid: tx.msgid,
        name: <div style={{ fontSize: "calc(8px + 0.3vw)" }}>{tx.msgid} ({tx.value} {tx.currency}: {moment(getCETDate(new Date(tx.created_at))).format("DD.MM.YYYY HH:mm:ss")})</div>
      };
    });
  }

  getROINVSTGTransactions = () => {
    const availableStatuses = ["Imported", "Import errored"];
    return this.props.transactions.filter(tx => availableStatuses.includes(tx.status)).map(tx => {
      return {
        guid: tx.msgid,
        name: <div style={{ fontSize: "calc(8px + 0.3vw)" }}>{tx.msgid} ({tx.value} {tx.currency}: {moment(getCETDate(new Date(tx.created_at))).format("DD.MM.YYYY HH:mm:ss")})</div>
      };
    })
  }

  getReasonsForRoinvstg = () => {
    return [
      {
        guid: "AM04",
        name: "There are insufficient funds on the account"
      },
      {
        guid: "AC04",
        name: "The account is closed"
      },
      {
        guid: "LEGL",
        name: "For legal reasons, specifying the reason in clear text"
      },
      {
        guid: "CUST",
        name: "Refusal by the Creditor to return"
      },
      {
        guid: "NOAS",
        name: "No answer is received from the Creditor"
      },
      {
        guid: "NOOR",
        name: "The funds were not received"
      },
      {
        guid: "ARDT",
        name: "The funds have already been returned"
      }
    ];
  }

  getReasonsForReturn = () => {
    return [
      {
        guid: "AC01",
        name: "Incorrect Account Number or the Account Number does not exist"
      },
      {
        guid: "AC04",
        name: "The account is closed"
      },
      {
        guid: "AC06",
        name: "The account is blocked, the reason is not specified"
      },
      {
        guid: "AG01",
        name: "For this type of account credit transfers are forbidden (e.g. a time deposit account)"
      },
      {
        guid: "AG02",
        name: "Invalid transaction code or invalid file format"
      },
      {
        guid: "AM05",
        name: "Duplication"
      },
      {
        guid: "BE04",
        name: "Missing Creditor Address"
      },
      {
        guid: "FOCR",
        name: "Following Cancellation Request"
      },
      {
        guid: "MD07",
        name: "End Customer Deceased"
      },
      {
        guid: "MS02",
        name: "Not Specified Reason Customer Generated"
      },
      {
        guid: "MS03",
        name: "Not Specified Reason Agent Generated"
      },
      {
        guid: "RC01",
        name: "BIC is incorrect"
      },
      {
        guid: "RR01",
        name: "Regulatory reason. The Debtor account or identification data is not available"
      },
      {
        guid: "RR02",
        name: "Regulatory reason. There is no full name or address of the debtor"
      },
      {
        guid: "RR03",
        name: "Regulatory reason. There is no full name or address of the creditor"
      },
      {
        guid: "RR04",
        name: "Regulatory reason"
      }
    ];
  }

  getReasonsForCancellation = () => {
    return [
      {
        guid: "DUPL",
        name: "Duplication"
      },
      {
        guid: "TECH",
        name: "Was incorrect for technical reasons"
      },
      {
        guid: "FRAD",
        name: "Was submitted by fraudulence"
      }
    ];
  }

  getBelongingOptions = () => {
    const { loginGuid } = userData;
    let belongingOptions = [{ guid: "Own", name: <div style={{ fontSize: "calc(8px + 0.3vw)" }}><b>Own</b></div> }];
    if (this.props.ownLogins) {
      belongingOptions = [
        ...belongingOptions,
        ...this.props.ownLogins.filter(item => item.guid !== loginGuid).map(item => {
          return {
            guid: item.guid,
            name: <div style={{ fontSize: "calc(8px + 0.3vw)" }}><b>{item.username}</b></div>
          };
        })
      ];
      return belongingOptions;
    }
  };

  handleChange = async (e) => {
    const names = ["amount", "value"];
    if (names.includes(e.target.name)) {
      const property = e.target.name;
      let value = e.target.value;
      value = value.replace(".", "");
      let start = value.substr(0, value.length - 2);
      let middle = ".";
      let finish = value.substr(value.length - 2, value.length);
      value = start + middle + finish;
      switch (value.length) {
      case 2: {
        value = value[value.length - 1] + ".00";
        break;
      }
      case 3: {
        value = "0" + value;
        break;
      }
      default: {
        if (value[0] === "0") {
          value = value.substr(1, value.length - 1);
        }
        break;
      }
      }
      this.setState({
        [property]: value
      });
    }
    else {
      await this.setState({
        [e.target.name]: e.target.value
      });
    }
    await this.setState({
      validation: this.validation(),
      selectedTemplate: ""
    });
    if (this.state.txRef[this.state.selectedTxType] === "FFCCTRNS" && this.state.validation) {
      await this.props.createTx({
        amount: this.state.amount,
        debtorName: this.state.originatorClient,
        debtorBic: config.bic,
        debtorAccount: this.state.IBAN,
        creditorName: this.state.creditorName,
        creditorAccount: this.state.creditorAccount,
        creditorBic: this.state.creditorBIC,
        remittanceInformation: this.state.remittance,
        currency: this.state.currency
      }, this.state.txRef[this.state.selectedTxType]);
      this.setState({
        commission: this.props.response.commission
      });
    }
  }

  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    if (!this.state.validation) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    }
    else {
      try {
        let request;
        switch (this.state.txRef[this.state.selectedTxType]) {
          case "FFCCTRNS":
            request = {
              amount: this.state.amount,
              amountWithRates: +this.state.amount + +this.state.commission,
              debtorName: this.state.originatorClient,
              debtorBic: config.bic,
              debtorAccount: this.state.IBAN,
              creditorName: this.state.creditorName,
              creditorAccount: this.state.creditorAccount,
              creditorBic: this.state.creditorBIC,
              remittanceInformation: this.state.remittance,
              currency: this.state.currency
            };
            break;
          case "FFPCRQST":
            request = {
              OrgnlTxId: this.state.transactionId,
              CxlRsnInf: this.state.reasonInfo,
              ClrSysPrtry: this.state.clrSys,
              originatorClient: this.state.originatorClient
            };
            if (this.state.reasonInfo === "FRAD")
              request["CxlRsnInfAddtInf"] = this.state.additionalInformation;
            break;
          case "PRTRN":
            const { transactionId, originatorClient, reasonForReturn, chrgsInfAmt, rtrRsnInfAddtInf } = this.state;
            if (this.state.reasonForReturn === "FOCR")
              request = {
                originalTxId: transactionId,
                originatorClient,
                reasonForReturn,
                chrgsInfAmt,
                rtrRsnInfAddtInf
              };
            else
              request = {
                originalTxId: transactionId,
                originatorClient,
                reasonForReturn
              };
            break;
          case "ROINVSTG":
            const { reasonForNegativeAnswer, cxlStsRsnInfAddtInf } = this.state;
            if (this.state.reasonForNegativeAnswer === "LEGL")
              request = {
                OrgnlTxId: this.state.transactionId,
                originatorClient: this.state.originatorClient,
                CxlStsRsnInf: reasonForNegativeAnswer,
                CxlStsRsnInfAddtInf: cxlStsRsnInfAddtInf
              };
            else
              request = {
                OrgnlTxId: this.state.transactionId,
                originatorClient: this.state.originatorClient,
                CxlStsRsnInf: reasonForNegativeAnswer
              };
            break;
          case "Internal":
            request = {
              sender_number: this.state.senderNumber,
              receiver_number: this.state.receiverNumber,
              currency: this.state.internalCurrency,
              value: this.state.value,
              amountWithRates: +this.state.amount + +this.state.commission
            };
            break;
          default: throw ("Error in tx type");
        }
        const txType = this.state.txRef[this.state.selectedTxType];
        validate(request, this.schemas[txType]);
        await this.props.createTx(request, txType);
        swal({
          title: "Transaction was send",
          text: `ID: ${this.props.response.guid}`,
          icon: "success"
        });
        await this.props.getAllAccounts();
        this.setState({
          IBAN: "",
          accountNumber: null,
          currency: null,
          currencyOptions: [],
          amount: "",
          creditorName: "Test name",
          creditorAccount: "AT802011129214844493",
          remittance: "",
          transactionId: null,
          reasonForReturn: null,
          chrgsInfAmt: "",
          rtrRsnInfAddtInf: "",
          internalCurrency: null,
          senderNumber: null,
          receiverNumber: null,
          value: "",
          validation: false,
          commission: "",
          confirmation: false,
          selectedTemplate: "",
          templateName: ""
        });
      }
      catch (error) {
        this.setState({
          isLoading: false
        });
        const parsedError = parseResponse(error);
        if ((error.response && error.response.status !== 409) || !error.response)
          swal({
            title: parsedError.error,
            text: parsedError.message,
            icon: "error",
          });
        if(this.state.txRef[this.state.selectedTxType] === "FFCCTRNS" && this.state.validation) {
          await this.props.createTx({
            amount: this.state.amount,
            debtorName: this.state.originatorClient,
            debtorBic: config.bic,
            debtorAccount: this.state.IBAN,
            creditorName: this.state.creditorName,
            creditorAccount: this.state.creditorAccount,
            creditorBic: this.state.creditorBIC,
            remittanceInformation: this.state.remittance,
            currency: this.state.currency
          }, this.state.txRef[this.state.selectedTxType]);
          
          this.setState({
            commission: this.props.response.commission
          });
        }
      }
    }
  }

  handleTemplateSelect = async(option) => {
    await this.setState({
      selectedTemplate: option.name
    });
    const template = this.state.transactionsTemplates.find(template => template.name === option.name);
    const currencyOptions = await this.getAccountCurrencies(template.from_account);
    await this.setState({
      accountNumber: template.from_account,
      currencyOptions
    })
    const account = this.props.accounts.find(account => account.number === template.from_account)
    await this.setState({
      amount: template.amount.toFixed(2),
      creditorAccount: template.to_account,
      creditorBIC: template.to_bic,
      currency: template.currency_code,
      remittance: template.description,
      IBAN: account.iban
    })

    await this.props.createTx({
      amount: this.state.amount,
      debtorName: this.state.originatorClient,
      debtorBic: config.bic,
      debtorAccount: this.state.IBAN,
      creditorName: this.state.creditorName,
      creditorAccount: this.state.creditorAccount,
      creditorBic: this.state.creditorBIC,
      remittanceInformation: this.state.remittance,
      currency: this.state.currency
    }, this.state.txRef[this.state.selectedTxType]);
    this.setState({
      commission: this.props.response.commission,
      validation: this.validation()
    });
  }

  render() {
    const selectorStyles = {
      placeholder: (styles) => {
        return {
          ...styles,
          color: "black",
          fontSize: "calc(8px + 0.4vw)"
        };
      },
      control: (styles, state) => {
        return {
          ...styles,
          background: "rgba(255,255,255,0.4) !important",
          marginBottom: "30px",
          width: "100%",
          height: this.state.height > 920 ? "calc(1vw + 10px) !important" : "50px !important",
          transition: ".8s all",
          boxShadow: "none !important",
          "&:hover": {
            background: state.menuIsOpen ? "white !important" : "transparent !important",
            transition: ".8s all"
          }
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          width: "100%",
          whiteSpace: "pre-line !important",
          marginLeft: "auto",
          marginRight: "auto"
        };
      }
    };

    const belongingSelectorStyles = {
      ...selectorStyles,
      option: (styles, state) => {
        return {
          ...styles,
          borderBottom: state.data.guid === "Own" ? "1px solid #545761" : ""
        };
      }
    };

    const currencySelectorStyles = {
      ...selectorStyles,
      option: (styles, state) => {
        return {
          ...styles,
          opacity: state.data.balance == "0.00" ? "0.3 !important" : "1",
          pointerEvents: state.data.balance == "0.00" ? "none !important" : "",
          userSelect: state.data.balance == "0.00" ? "none !important" : ""
        };
      }
    };

    const reasonSelectorStyles = {
      ...selectorStyles,
      menu: (styles) => {
        return {
          ...styles,
          textAlign: "left !important"
        };
      }
    };

    const accountsOptions = this.getAccounts();
    const templatesOptions = this.getTemplatesOptions();
    const receiverOptions = this.getReceiverAccounts().filter(item => item.guid !== this.state.senderNumber);
    const belongingOptions = this.getBelongingOptions();
    const cancellationTransactionsOptions = this.getCancellationTransactions();
    const returningTransactionsOptions = this.getReturningTransactions();
    const roinvstgTransactionsOptions = this.getROINVSTGTransactions();
    const reasonsForReturnOptions = this.getReasonsForReturn();
    const reasonsForRoinvstgOptions = this.getReasonsForRoinvstg();
    const reasonsForCancellationOptions = this.getReasonsForCancellation();
    let inputs = "";
    switch (this.state.txRef[this.state.selectedTxType]) {
      case "FFCCTRNS":
        inputs = <>
        <Row className="row">
          <Col className="col" md={4} lg={4} sm={4}>
            <ControlLabel
              className="col-label"
              style={{ textTransform: "none" }}
            >
              Templates
            </ControlLabel>
            <Select
              key="template"
              options={templatesOptions}
              styles={selectorStyles}
              selectedValue={this.state.selectedTemplate}
              onSelect={this.handleTemplateSelect}
              placeholder="Select template"
            />
          </Col>   
          <Col className="col" md={4} lg={4} sm={4} />       
          <Col 
            className="col"
            id={this.state.validation && !this.state.selectedTemplate ? "showTemplateButton" : "hideTemplateButton"}
            md={4} lg={4} sm={4}
          >
            <Input
              label="Template name"
              name="templateName"
              value={this.state.templateName}
              onChange={this.handleChange}
            />
            <Button
              className="submit-button next"
              onClick={this.handleAddTxTemplate}
            >
              Add template
            </Button>
          </Col>
        </Row>
        <hr className="hr" />
        <Row className="row">
          {/* <Col className="col" md={4}>
          <Input
            className="input-disabled"
            placeholder={this.state.IBAN}
            label="Your iban"
            disabled
          />
        </Col>             */}
          <Col className="col" md={4} lg={4} sm={4}>
            <ControlLabel
              className="col-label"
              style={{ textTransform: "none" }}
            >
              Your account
          </ControlLabel>
            <Select
              key="account"
              options={accountsOptions}
              styles={selectorStyles}
              selectedValue={this.state.accountNumber}
              onSelect={this.handleAccountSelect}
              placeholder="Select account..."
            />
          </Col>
          <Col className="col" md={3} lg={3} sm={3}>
            <ControlLabel
              className="col-label"
              style={{ textTransform: "none" }}
            >
              Currency
          </ControlLabel>
            <Select
              options={this.state.currencyOptions}
              styles={currencySelectorStyles}
              onSelect={this.handleCurrencySelect}
              selectedValue={this.state.currency}
            />
          </Col>
          <Col className="col" md={3} lg={3} sm={3}>
            <Input
              label="Amount"
              type="number"
              name="amount"
              inputLabel={this.state.accountNumber ? this.state.currencies[this.state.currency] : ""}
              note={["EUR", null].includes(this.state.currency) ? "" : "Your funds will be converted into euros at the bank rate"}
              value={this.state.amount}
              onChange={this.handleChange}
            />
          </Col>
        </Row>
          <Row className="row">
            <Col className="col" md={4} lg={4} sm={4}>
              <Input
                label="Receiver name"
                name="creditorName"
                value={this.state.creditorName}
                onChange={this.handleChange}
              />
            </Col>
            <Col className="col" md={4} lg={4} sm={4}>
              <Input
                label="Receiver account"
                name="creditorAccount"
                value={this.state.creditorAccount}
                onChange={this.handleChange}
              />
            </Col>
            <Col className="col" md={4} lg={4} sm={4}>
              <Input
                label="Receiver BIC"
                name="creditorBIC"
                value={this.state.creditorBIC}
                onChange={this.handleChange}
              />
            </Col>
          </Row>
          <Row className="row">
            <Col className="col" md={12} lg={12} sm={12}>
              <Input
                label="Description"
                name="remittance"
                value={this.state.remittance}
                onChange={this.handleChange}
                style={{
                  input: {
                    textAlign: "left"
                  }
                }}
              />
            </Col>
          </Row>
          <Row className="row">
            <Col className="col" md={12} lg={12} sm={12}>
              <Input
                label="Commission"
                name="commission"
                value={this.state.commission ? `${this.state.commission} ${this.state.currency}` : ""}
                onChange={this.handleChange}
                disabled
              />
            </Col>
          </Row>
        </>;
      break;

    case "FFPCRQST":
      inputs = <><Row className="row">
        <Col className="col" md={7}>
          <ControlLabel
            className="col-label"
            style={{ textTransform: "none" }}
          >
            Transaction ID
          </ControlLabel>
          <Select
            options={cancellationTransactionsOptions}
            styles={selectorStyles}
            onSelect={this.handleTxSelect}
            selectedValue={this.state.transactionId}
            placeholder="Select transaction..."
          />
        </Col>
        <Col className="col" md={5}>
          <ControlLabel
            className="col-label"
            style={{ textTransform: "none" }}
          >
            Clearing system
          </ControlLabel>
          <Select
            defaultValue={this.state.clrSys}
            options={this.state.clearingSystems}
            styles={selectorStyles}
            onSelect={this.handleClrSysSelect}
          />
        </Col>
      </Row>
          <Row className="row">
            <Col className="col" md={2} />
            <Col className="col" md={4}>
              <ControlLabel
                className="col-label"
                style={{ textTransform: "none" }}
              >
                Reason info
              </ControlLabel>
              <Select
                selectedValue={this.state.reasonInfo}
                options={reasonsForCancellationOptions}
                styles={selectorStyles}
                onSelect={this.handleReasonCancellationSelect}
              />
            </Col>
            {this.state.reasonInfo == "FRAD" &&
              <Col className="col" md={4}>
                <Input
                  label="Additional info"
                  name="additionalInformation"
                  value={this.state.additionalInformation}
                  onChange={this.handleChange}
                />
              </Col>}
          </Row>
        </>;
      break;
    case "PRTRN": {
      inputs = <><Row className="row">
        <Col className="col" md={7}>
          <ControlLabel
            className="col-label"
            style={{ textTransform: "none" }}
          >
              Transaction ID
          </ControlLabel>
          <Select
            options={returningTransactionsOptions}
            styles={selectorStyles}
            onSelect={this.handleTxSelect}
            placeholder="Select transaction..."
            selectedValue={this.state.transactionId}
          />
        </Col>
        <Col className="col" md={5}>
          <ControlLabel
            className="col-label"
            style={{ textTransform: "none" }}
          >
            Reason for return
          </ControlLabel>
          <Select
            key="reason"
            options={reasonsForReturnOptions}
            styles={reasonSelectorStyles}
            selectedValue={this.state.reasonForReturn}
            onSelect={this.handleReasonSelect}
            placeholder="Select reason..."
          />
        </Col>
      </Row>
          {this.state.reasonForReturn === "FOCR" &&
            <><Row className="row">
              <Col className="col" md={4}>
                <Input
                  label="Charges amount fee"
                  type="number"
                  name="chrgsInfAmt"
                  value={this.state.chrgsInfAmt}
                  onChange={this.handleChange}
                />
              </Col>
              <Col className="col" md={4}>
                <Input
                  label="Additional info"
                  name="rtrRsnInfAddtInf"
                  value={this.state.rtrRsnInfAddtInf}
                  onChange={this.handleChange}
                />
              </Col>
            </Row></>}
        </>;
      break;
    }
    case "ROINVSTG": {
      inputs = <>
        <Row className="row">
          <Col className="col" md={7}>
            <ControlLabel
              className="col-label"
              style={{ textTransform: "none" }}
            >
              Transaction ID
            </ControlLabel>
            <Select
              options={roinvstgTransactionsOptions}
              styles={selectorStyles}
              onSelect={this.handleTxSelect}
              placeholder="Select transaction..."
              selectedValue={this.state.transactionId}
            />
          </Col>
          <Col className="col" md={5}>
            <ControlLabel
              className="col-label"
              style={{ textTransform: "none" }}
            >
              Reason for the negative answer
            </ControlLabel>
            <Select
              key="reason"
              options={reasonsForRoinvstgOptions}
              styles={reasonSelectorStyles}
              selectedValue={this.state.reasonForNegativeAnswer}
              onSelect={this.handleReasonNegativeAnswerSelect}
              placeholder="Select reason..."
            />
          </Col>
        </Row>
          {this.state.reasonForNegativeAnswer === "LEGL" &&
            <Row className="row">
              <Col md={3}></Col>
              <Col className="col" md={6}>
                <Input
                  label="Additional info"
                  name="cxlStsRsnInfAddtInf"
                  value={this.state.cxlStsRsnInfAddtInf}
                  onChange={this.handleChange}
                />
              </Col>
              <Col md={3}></Col>
            </Row>}
        </>;
      break;
    }
    // case "Internal": {
    //   inputs = <><Row className="row">
    //     <Col className="col" md={3}>
    //       <ControlLabel
    //         className="col-label"
    //         style={{ textTransform: "none" }}
    //       >
    //         From account
    //       </ControlLabel>
    //       <Select
    //         key="sender"
    //         options={accountsOptions}
    //         styles={selectorStyles}
    //         onSelect={this.handleSenderAccountSelect}
    //         selectedValue={this.state.senderNumber}
    //         placeholder="Select account..."
    //       />
    //     </Col>
    //     {this.state.entitySelector && userData.logins.length > 1 &&
    //         <Col className="col" md={4}>
    //           <ControlLabel
    //             className="col-label"
    //             style={{ textTransform: "none" }}
    //           >
    //             To
    //           </ControlLabel>
    //           <Select
    //             key="belonging"
    //             options={belongingOptions}
    //             styles={belongingSelectorStyles}
    //             onSelect={this.handleBelongingSelect}
    //             selectedValue={this.state.belonging}
    //             placeholder="Select belonging..."
    //           />
    //         </Col>}
    //     <Col className="col" md={4}>
    //       <ControlLabel
    //         className="col-label"
    //         style={{ textTransform: "none" }}
    //       >
    //         To account
    //       </ControlLabel>
    //       <Select
    //         key="receiver"
    //         options={receiverOptions}
    //         styles={selectorStyles}
    //         onSelect={this.handleReceiverAccountSelect}
    //         selectedValue={this.state.receiverNumber}
    //         placeholder="Select account..."
    //       />
    //     </Col>
    //     {this.state.entitySelector && userData.logins.length > 1 &&
    //       <Col className="col" md={3} />}
    //     <Col className="col" md={2}>
    //       <ControlLabel
    //         className="col-label"
    //         style={{ textTransform: "none" }}
    //       >
    //         Currency
    //       </ControlLabel>
    //       <Select
    //         options={this.state.internalCurrencyOptions}
    //         styles={currencySelectorStyles}
    //         onSelect={this.handleInternalCurrencySelect}
    //         selectedValue={this.state.internalCurrency}
    //       />
    //     </Col>
    //     <Col className="col" md={3}>
    //       <Input
    //         label="Amount"
    //         type="number"
    //         name="value"
    //         inputLabel={this.state.currencies[this.state.internalCurrency]}
    //         note={[ "EUR", null ].includes(this.state.internalCurrency) ? "" : "Your funds will be converted into euros at the bank rate"}
    //         value={this.state.value}
    //         onChange={this.handleChange}
    //       />
    //     </Col>
    //   </Row>
    //     </>;
    //   break;
    // }
      default: break;
    }

    return (
      <>
        <Row className="row">
          <Col md={4} />
          <Col className="col" md={4}>
            <ControlLabel
              className="col-label"
              style={{ textTransform: "none" }}
            >
              {isAdmin ? "Transaction type" : "Transfer type"}
            </ControlLabel>
            <Select
              options={isAdmin ? [
                {
                  guid: "Detailed credit transfer",
                  name: "Detailed credit transfer"
                },
                // {
                //   guid: "FFCCTRNS",
                //   name: "Credit transfer"
                // },
                {
                  guid: "FFPCRQST",
                  name: "Payment cancellation"
                },
                {
                  guid: "PRTRN",
                  name: "Payment return"
                },
                {
                  guid: "ROINVSTG",
                  name: "Resolution of investigation"
                },
                // {
                //   guid: "Internal",
                //   name: "Internal transaction"
                // },
              ] : [
                  {
                    guid: "FFCCTRNS",
                    name: "External"
                  },
                  {
                    guid: "Internal",
                    name: "Internal"
                  }
                ]
              }
              defaultValue={this.state.selectedTxType}
              styles={selectorStyles}
              onSelect={this.handleTxTypeSelect}
            />
          </Col>
          <Col md={4} />
        </Row>
        {this.state.isLoading
          ? <Spinner/>
          : this.state.txRef[this.state.selectedTxType] === "Internal"
            ? <NewForm />
            : this.state.txRef[this.state.selectedTxType] === "Detailed credit transfer"
              ? <DetailedCreditTransfer/>
              : <Card
                style={{
                  color: "black",
                  width: this.state.height > 920 ? "70%" : "90%",
                  height: "100vh !important",
                  marginLeft: this.state.height > 920 ? "15%" : "5%",
                  marginTop: "20px"
                }}
                header={isAdmin ? "Send transaction" : "Send transfer"}
              >
                {inputs}
                <Row
                  id={this.state.validation ? "showButton" : "hideButton"}
                  className="row"
                >
                  <Col md={12}>
                    <Button
                      id="transaction-form-button"
                      onClick={this.handleSubmit}
                    >
                      Send
                    </Button>
                  </Col>
                </Row>
              </Card>}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    accounts: state.accounts.accounts,
    ownLogins: state.users.users,
    entitiesAccounts: state.accounts.txAccounts,
    response: state.transactions.responseTransaction,
    transactions: state.transactions.FFCCTRNS,
    confirmation: state.confirmation.confirmation,
    transactionsTemplates: state.transactions.externalTemplates
  };
};

export default connect(mapStateToProps, {
  createTx,
  getAllTxs,
  getAllAccounts,
  getTxAccounts,
  reset,
  getOwnLogins,
  getExternalTxsTemplates,
  createExternalTxTemplate
})(Send);

Send.propTypes = {
  accounts: PropTypes.array,
  response: PropTypes.object,
  createTx: PropTypes.func,
  getAllAccounts: PropTypes.func,
  getAllTxs: PropTypes.func,
  getExternalTxsTemplates: PropTypes.func,
  createExternalTxTemplate: PropTypes.func
};