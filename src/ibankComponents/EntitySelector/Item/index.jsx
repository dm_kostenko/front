import React from "react";

const EntitySelectorItem = (props) => {
  return (
    <div className="entity-selector-item">
      <img className="entity-selector-item img" src={props.img}/>
      <a className="entity-selector-item text">
        {props.text}
      </a>
    </div>
  );
};

export default EntitySelectorItem;