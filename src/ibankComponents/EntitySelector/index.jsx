import React, { Component } from "react";
import { connect } from "react-redux";
import { changeLogin } from "ibankActions/auth";
import { getAllUsers as getOwnLogins } from "ibankActions/users";
import Select from "ibankComponents/UI/MultiSelect";
import logo from "assets/img/default-logo.png";
import EntitySelectorItem from "./Item";
import ReloginModal from "ibankComponents/UI/Modal/Relogin";
import ReloginPage from "ibankComponents/ReloginPage";
import { showModal } from "ibankActions/modal";

class EntitySelector extends Component {

  state = {
    width: 0,

    loginsOptions: [],
    defaultValue: "",
    login: ""
  }

  componentDidMount = async() => {
    this.updateWindowStyles();
    window.addEventListener("resize", this.updateWindowStyles);
    if(this.props.data) {
      await this.props.getOwnLogins();
      const loginsOptions = this.props.ownLogins.map(login => {
        return {
          guid: login.guid,
          text: login.username,
          name: <EntitySelectorItem img={logo} text={login.username} />
        };
      });
      const [ defaultValue ] = loginsOptions.filter(option => option.guid === this.props.data.currentLogin)
      this.setState({ loginsOptions, defaultValue: defaultValue ? defaultValue.name : "" });
    }
  }

  updateWindowStyles = () => {
    this.setState({
      width: window.outerWidth
    });
  }

  handleSelect = async (option) => {
    this.setState({ login: option.text });
    await this.props.showModal(true, "entity");
  }

  render() {
    return (
      <div className="entity-selector">
        <Select
          options={this.state.loginsOptions}
          isSearchable={false}
          multi={false}
          placeholder=" "
          onSelect={this.handleSelect}
          defaultValue={this.state.defaultValue || null}
          styles={{
            placeholder: (styles) => {
              return {
                ...styles,
                color: "grey"
              };
            },
            control: (styles) => {
              return {
                ...styles,
                background: "rgba(255,255,255,0.4) !important",
                marginBottom: "30px",
                width: this.state.width > 1000 ? "250px" : "150px",
                height: this.state.width > 1000 ? "50px !important" : "10px !important",
                maxHeight: this.state.width > 1000 ? "50px !important" : "10px !important",
                padding: "0px !important",
                transition: ".8s all",
                boxShadow: "none !important"
              };
            },
            singleValue: (styles) => {
              return {
                ...styles,
                width: "100%",
                marginLeft: "auto",
                marginRight: "auto"
              };
            },
            menu: (styles) => {
              return {
                ...styles,
                textAlign: "left !important",
                cursor: "default !important"
              };
            },
            option: (styles, state) => {
              return {
                ...styles,
                opacity: state.value === this.props.data.currentLogin ? "0.3" : "1",
                userSelect: state.value === this.props.data.currentLogin ? "none !important" : "",
                pointerEvents: state.value === this.props.data.currentLogin ? "none !important" : "",
                background: state.value === this.props.data.currentLogin ? "transparent !important" : "",
                cursor: "default !important"
              };
            },
          }}
        />
        <ReloginModal
          header=""
          dialogClassName="modal-wide relogin"
          content={<ReloginPage login={this.state.login} />}
          state="entity"
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ownLogins: state.users.users,
    response: state.auth.changeResponse,
    error: state.auth.changeError
  };
};

export default connect(mapStateToProps, {
  changeLogin,
  getOwnLogins,
  showModal
})(EntitySelector);
