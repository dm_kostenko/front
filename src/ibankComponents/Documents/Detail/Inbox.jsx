import React, { Component } from "react";
import Content from "ibankViews/Content";
import { getInboxDoc } from "ibankActions/documents";
import { connect } from "react-redux";
import { downloadXML } from "helpers/downloadXML";
import Card from "ibankComponents/UI/Card";
import Button from "ibankComponents/UI/Button";
import DetailInfoLabel from "ibankComponents/UI/DetailInfoLabel";
import PropTypes from "prop-types";
import Spinner from "ibankComponents/UI/Spinner";
import tagsDictionary from "helpers/fieldsDictionary";

class InboxDetail extends Component {

  state = {
    data: {}
  }

  async componentDidMount() {
    await this.props.getInboxDoc(this.props.match.params.id);   
  }


  render() {
    const unusedKeys = [ "count", "guid", "guard_counter" ];

    if(this.props.loading) 
      return <Content>
        <Spinner/>
      </Content>;
    return <Content>
      <Card
        style={{
          color: "black",
          // background: "#80dbff",
          width: "1000px",
          marginLeft: "calc(50% - 500px)",
          height: "auto",
          marginTop: "20px",
          marginRight: "10px",
          textAlign: "center"
        }}
        detail={true}
        header={`Inbox document: ${this.props.match.params.id}`}
      >
        {Object.keys(this.props.data).filter(key => key !== "data").map(key => {
          const label = tagsDictionary[key.toUpperCase()] 
            ? tagsDictionary[key.toUpperCase()].beautify 
            : key;
          const value = tagsDictionary[key.toUpperCase()] && tagsDictionary[key.toUpperCase()].modify 
            ? tagsDictionary[key.toUpperCase()].modify(this.props.data[key])
            : this.props.data[key];
          return !unusedKeys.includes(key) && 
            <DetailInfoLabel
              key={key}
              label={`${label}:`}
              value={value || "-"}
            />;
        })}
        {/* {this.props.data.status === "Prepared" &&
        <Button
          className="submit-button small"
          onClick={() => downloadXML(this.props.match.params.id, "ffcctrns")}
          style={{
            margin: "50px 0 20px 0"
          }}
        >
          Save in XML
        </Button>} */}
      </Card>
    </Content>;
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.documents.inboxDetail,
    loading: state.documents.loadingInboxDetail
  };
};

export default connect(mapStateToProps, {
  getInboxDoc
})(InboxDetail);

InboxDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  loading: PropTypes.bool,
  getInboxDetail: PropTypes.func,
  data: PropTypes.object
};