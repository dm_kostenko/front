import React, { Component } from "react";
import { getTxDoc } from "ibankActions/documents";
import { connect } from "react-redux";
import { downloadXML } from "helpers/downloadXML";
import Card from "ibankComponents/UI/Card";
import Button from "ibankComponents/UI/Button";
import DetailInfoLabel from "ibankComponents/UI/DetailInfoLabel";
import PropTypes from "prop-types";
import Spinner from "ibankComponents/UI/Spinner";
import tagsDictionary from "helpers/fieldsDictionary";

class FFCCTRNSDocumentDetail extends Component {

  state = {
    data: {}
  }

  async componentDidMount() {
    await this.props.getTxDoc(this.props.match.params.id, "FFCCTRNS");   
  }


  render() {
    if(this.props.loading) 
      return <Spinner/>;
    return <div className="content">
      <Card
        style={{
          color: "black",
          // background: "#80dbff",
          width: "1000px",
          marginLeft: "calc(50% - 500px)",
          height: "auto",
          marginTop: "20px",
          marginRight: "10px",
          textAlign: "center"
        }}
        detail={true}
        header={`DOCUMENT: ${this.props.match.params.id}`}
      >
        {Object.keys(this.props.data).map(key => {
          const label = tagsDictionary[key.toUpperCase()] 
            ? tagsDictionary[key.toUpperCase()].beautify 
            : key;
          const value = tagsDictionary[key.toUpperCase()] && tagsDictionary[key.toUpperCase()].modify 
            ? tagsDictionary[key.toUpperCase()].modify(this.props.data[key])
            : this.props.data[key];
          return <DetailInfoLabel
            key={key}
            label={`${label}:`}
            value={value}
          />;
        })}
        {this.props.data.status === "Prepared" &&
        <Button
          className="submit-button small"
          onClick={() => downloadXML(this.props.match.params.id, "roinvstg")}
          style={{
            margin: "50px 0 20px 0"
          }}
        >
          Save in XML
        </Button>}
      </Card>
    </div>;
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.documents.DocFFCCTRNS,
    loading: state.documents.loadingDocFFCCTRNS
  };
};

export default connect(mapStateToProps, {
  getTxDoc
})(FFCCTRNSDocumentDetail);

FFCCTRNSDocumentDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  loading: PropTypes.bool,
  getTxDoc: PropTypes.func,
  data: PropTypes.object
};