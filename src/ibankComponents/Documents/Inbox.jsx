import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { downloadXML } from "helpers/downloadXML";
import AbstractComponent from "ibankFactories/Table";
import { getInboxDocs } from "ibankActions/documents";
import { searchInInbox } from "ibankActions/search";
import { reset } from "ibankActions/search";
import moment from "moment";
import { editInboxStatusAction } from "ibankActions/documents";

const columns = [
  {
    label: "ID",
    path: "guid",
    content: doc => (
      <Link className="link" to={`documents/inbox/${doc.guid}`}>{doc.guid}</Link>
    )
  },
  { 
    label: "Timestamp",
    content: doc => { 
      return moment(doc.created_at).utcOffset(7).format("DD.MM.YYYY HH:mm:ss");
    }
  },
  {
    path: "type",
    label: "Type"
  },
  {
    path: "status",
    label: "Status"
  },
  {
    label: "Details",
    path: "details",
    content: doc => (
      unescape(doc.details)
    )
  },
  {
    label: "Download",
    content: doc => (
      <i 
        className="fas fa-file-download" 
        style={{ cursor: "pointer", color: "green" }}
        onClick={() => downloadXML(doc.data, doc.guid, "inbox")} 
      />
    )
  },
  {
    key: "editInboxStatus",
    label: "Approve / Cancel"
  } 
];

const mapStateToProps = (state) => {
  return {
    data: state.documents.inbox,
    count: state.documents.countInbox,
    searchData: state.search.inboxSearch,
    isSearch: state.search.isSearch,
    loading: state.documents.loadingInbox,
    name: "inbox",
    columns
  };
};

export default connect(mapStateToProps, {
  get: getInboxDocs,
  search: searchInInbox,
  reset,
  editInboxStatusAction
})(AbstractComponent);