import React from "react";
import { connect } from "react-redux";
import { downloadXML } from "helpers/downloadXML";
import AbstractComponent from "ibankFactories/Table";
import { getTxsDocs } from "ibankActions/documents";
import { searchInTxsDocs, reset } from "ibankActions/search";
import { Link } from "react-router-dom";
import moment from "moment";


const columns = [
  {
    label: "ID",
    content: doc => {
      return <Link className="link" to={`documents/ffcctrns/${doc.docid}`} >{doc.docid}</Link>;
    }
  },
  { 
    label: "Timestamp",
    content: doc => { 
      return moment(doc.datestype).utcOffset(7).format("DD.MM.YYYY HH:mm:ss");
    }
  },
  {
    path: "status",
    label: "Status"
  },
  {
    path: "sender",
    label: "Sender"
  },
  {
    path: "receiver",
    label: "Receiver"
  },
  { 
    path: "grphdrnboftxs", 
    label: "Number of txs" 
  },
  {
    label: "Download",
    content: doc => {
      return doc.status === "Prepared"
        ? <i 
          className="fas fa-file-download" 
          style={{ cursor: "pointer", color: "green" }}
          onClick={() => downloadXML(doc.docid, "ffcctrns")} 
        />
        : <i 
          className="fas fa-file-download" 
          style={{ color: "grey", opacity: "0.5" }}
        />;
    }
  }
];

const mapStateToProps = (state, props) => {
  return {
    data: state.documents.DocsFFCCTRNS,
    count: state.documents.countDocsFFCCTRNS,
    searchData: state.search.txsDocsFFCCTRNSSearch,
    directionData: { ...props.directionData },
    isSearch: state.search.isSearch,
    loading: state.documents.loadingDocsFFCCTRNS,
    name: "transactionsDocs",
    type: "ffcctrns",
    columns
  };
};

export default connect(mapStateToProps, {
  get: getTxsDocs,
  search: searchInTxsDocs,
  reset
})(AbstractComponent);