import React, { Component } from "react";
import { Button, Col, ControlLabel, Form, Row } from "react-bootstrap";
import Card from "ibankComponents/UI/Card";
import Input from "ibankComponents/UI/Input";
import { showModal } from "ibankActions/modal";
import Select from "ibankComponents/UI/MultiSelect";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";
import { createNewRate, getAllRateRules, getAllRateRuleParams } from "ibankActions/rates";
import { getAllCurrencies } from "ibankActions/currencies";
import Spinner from "ibankComponents/UI/Spinner";

const dict = {
  "currency_exchange": "Currency exchange",
  "outgoing_FFCCTRNS": "Outgoing transfer",
  "incoming_FFCCTRNS": "Incoming transfer",
  "system": "System",
  "recurring_payment": "Recurring payment",
  "receiverBIC": "Receiver BIC",
  "senderBIC": "Sender BIC",
  "receiverName": "Receiver name",
  "senderName": "Sender name",
  "senderAccount": "Sender IBAN",
  "receiverAccount": "Receiver IBAN",
  "amount": "Amount"
}

class RateCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      description: "",
      rule: {},
      ruleTypeOptions: {},
      currencyExchangeFrom: [],
      currencyExchangeTo: [],
      types: {},
      changedParams: {},
      loading: true,
      separately: true
    };
  }

  componentDidMount = async () => {
    this.updateWindowStyles();
    window.addEventListener("resize", this.updateWindowStyles);
    const rate_guid = "d8c5b253-d583-d14a-20bf-249609336ee9";
    await this.props.getAllRateRules({ rate_guid });
    let rule = {};
    let ruleTypeOptions = {};
    let types = {};
    this.props.rate_rules.forEach(item => {
      if(!types[item.type])
        types[item.type] = [ item.rule_type ]
      else {
        types[item.type].push(item.rule_type)
      }
    });
    let currencyExchangeFrom = [];
    let currencyExchangeTo = [];
    if(types["currency_exchange"]) {
      types["currency_exchange"].forEach(item => {
        const arr = item.split("_");
        currencyExchangeFrom.push(arr[0])
        currencyExchangeTo.push(arr[1])
      })
      currencyExchangeFrom = [ ...new Set(currencyExchangeFrom) ]
      currencyExchangeTo = [ ...new Set(currencyExchangeTo) ]
    }

    this.setState({
      // name: this.props.rate.name,
      // description: this.props.rate.description,
      ruleTypeOptions,
      rule,
      currencyExchangeFrom,
      currencyExchangeTo,
      types,
      loading: false
    });
    await this.props.getAllCurrencies();
  }

  updateWindowStyles = () => {
    this.setState({
      width: window.outerWidth
    });
  }

  onChangeHandler = (e) => {
    const property = e.target.name;
    let value = e.target.value;
    if (property === "name" || property === "description") {
      this.setState({
        [property]: value
      });
    }
    else {
      const rule_type = this.state.type === "currency_exchange"
        ? `${this.state.from_currency}_${this.state.to_currency}`
        : this.state.rule_type

      let [ first, second ] = property.split("__");
      let { rule, changedParams } = this.state;
      rule[`${this.state.type}__${rule_type}`][first][second] = value;

      if(changedParams[`${this.state.type}__${rule_type}`]) {
        if(changedParams[`${this.state.type}__${rule_type}`][first])
          changedParams[`${this.state.type}__${rule_type}`][first][second] = value;
        else
          changedParams[`${this.state.type}__${rule_type}`][first] = {
            ...rule[`${this.state.type}__${rule_type}`][first],
            [second]: value
          }        
      }
      else {
        changedParams[`${this.state.type}__${rule_type}`] = {
          [first]: {
            ...rule[`${this.state.type}__${rule_type}`][first],
            [second]: value
          }
        }
      }

      this.setState({
        changedParams,
        rule 
      });
    }
  };

  onChangeRateHandler = (e) => {
    const [ type, _value ] = e.target.name.split("__");
    if(_value.includes("percent") && !/(^[0-9]{0,2} %{0,1}[0-9]{0,1}$)|(^[0-9]{1,2}\,[0-9]{2} %{0,1}[0-9]{0,1}$)/.test(e.target.value))
      return
    if (!_value.includes("percent") || (e.target.value.substr(-2) !== "%%" && e.target.value.indexOf("%") === e.target.value.length - 1) || (e.target.value[e.target.value.length - 1] !== "%")) {

      let value = _value.includes("percent") 
        ? e.target.value[e.target.value.length - 1] !== "%"
          ? e.target.value.substring(0, e.target.value.length - 3) + e.target.value[e.target.value.length - 1]
          : e.target.value.substring(0, e.target.value.length - 2) 
        : e.target.value;
      value = value.trim()
      value = value.replace(",", "")
      let { rule, changedParams } = this.state;
      const rule_type = this.state.type === "currency_exchange"
        ? `${this.state.from_currency}_${this.state.to_currency}`
        : this.state.rule_type

      rule[`${this.state.type}__${rule_type}`][type][_value] = _value.includes("percent") 
        ? value.length <= 2
          ? value.split(",").join("")
          : `${value.split(",").join("").slice(-4, -2)},${value.split(",").join("").slice(-2)}`
        : value;
      console.log(changedParams)
      if(changedParams[`${this.state.type}__${rule_type}`]) {
        if(changedParams[`${this.state.type}__${rule_type}`][type])
          changedParams[`${this.state.type}__${rule_type}`][type][_value] = rule[`${this.state.type}__${rule_type}`][type][_value];
        else
          changedParams[`${this.state.type}__${rule_type}`][type] = {
            ...rule[`${this.state.type}__${rule_type}`][type],
            [_value]: rule[`${this.state.type}__${rule_type}`][type][_value]
          }        
      }
      else {
        changedParams[`${this.state.type}__${rule_type}`] = {
          [type]: {
            ...rule[`${this.state.type}__${rule_type}`][type],
            [_value]: rule[`${this.state.type}__${rule_type}`][type][_value]
          }
        }
      }

      this.setState({ 
        changedParams,
        rule
      });
    }  
  };

  generalValidation = () => {
    return this.formValidation(this.state.name) &&
      this.formValidation(this.state.description);
  }

  formValidation = (data) => {
    return data !== "";
  };

  handleSubmit = async (e) => {
    const { changedParams, name, description } = this.state;
    this.setState({ isLoading: true });
    e.preventDefault();
    if (!this.generalValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    }
    else {
      try {
        let rules = [];
        Object.keys(changedParams).forEach(item => {
          const [ type, ruleType ] = item.split("__");
          if(type === "system") {
            Object.keys(changedParams[item]).forEach(param => {
              rules.push(
                {
                  type,
                  ruleType,
                  i: param,
                  rate_rule_guid: changedParams[item][param].rate_rule_guid,
                  name: "rate",
                  value_type: "number",
                  value: changedParams[item][param].number_rate.toString()
                },
                {
                  type,
                  ruleType,
                  i: param,
                  rate_rule_guid: changedParams[item][param].rate_rule_guid,
                  name: "interval",
                  value_type: "string",
                  value: changedParams[item][param].value
                }                
              )
            })
          }
          else {
            Object.keys(changedParams[item]).forEach(param => {
              rules.push(
                {
                  type,
                  ruleType,
                  i: param,
                  // rate_guid: this.props.rate.guid,
                  rate_rule_guid: changedParams[item][param].rate_rule_guid,
                  name: "rate",
                  value_type: "number",
                  value: changedParams[item][param].number_rate.toString(),
                  all: ruleType === "ALL_ALL"
                },
                {
                  type,
                  ruleType,
                  i: param,
                  // rate_guid: this.props.rate.guid,
                  rate_rule_guid: changedParams[item][param].rate_rule_guid,
                  name: "rate",
                  value_type: "percent",
                  value: changedParams[item][param].percent_rate.toString().replace(",", "."),
                  all: ruleType === "ALL_ALL"
                },
                {
                  type,
                  ruleType,
                  i: param,
                  // rate_guid: this.props.rate.guid,
                  rate_rule_guid: changedParams[item][param].rate_rule_guid,
                  name: param,
                  value_type: changedParams[item][param].value_type,
                  value: changedParams[item][param].value.toString(),
                  all: ruleType === "ALL_ALL"
                }
              )
            })
          }
        })
        await this.props.createNewRate(this.props.currentPage, this.props.pageSize, {
          name,
          description,
          rules
        });
        this.setState({ isLoading: false });
        swal({
          title: "Rate is updated!",
          icon: "success",
          button: false,
          timer: 2000
        });
        await this.props.showModal(false, "rates-create");
      } catch (err) {
        throw(err)
      }
    }
  };

  handleChangeType = option => {
    this.setState({
      type: option.guid,
      typeName: option.name,
      rule_type: "",
      from_currency: "",
      to_currency: ""
    });
  }

  refreshRule = (isAll) => {
    let { changedParams, rule } = this.state;
    const rule_type = this.state.type === "currency_exchange"
      ? `${this.state.from_currency}_${this.state.to_currency}`
      : this.state.rule_type

    this.props.rate_rule_params.forEach(item => {
      if(changedParams[`${this.state.type}__${rule_type}`] && changedParams[`${this.state.type}__${rule_type}`][item.compare_action])
        rule[`${this.state.type}__${rule_type}`] = {
          ...rule[`${this.state.type}__${rule_type}`],
          [item.compare_action]: changedParams[`${this.state.type}__${rule_type}`][item.compare_action]
        }
      else
        rule[`${this.state.type}__${rule_type}`] = {
          ...rule[`${this.state.type}__${rule_type}`],
          [item.compare_action]: {
            value: item.compare_value,
            number_rate: item.number_rate,
            percent_rate: item.percent_rate,
            rate_rule_guid: item.rate_rule_guid,
            value_type: item.compare_value_type
          }
        }
    })

    if(isAll) {
      changedParams[`${this.state.type}__${rule_type}`] = rule[`${this.state.type}__${rule_type}`]
    }

    this.setState({ 
      rule,
      changedParams
    })
  }

  handleChangeRuleType = async option => {
    
    this.setState({
      rule_type: option.name
    });

    await this.props.getAllRateRuleParams({ 
      rate_guid: "d8c5b253-d583-d14a-20bf-249609336ee9", 
      rule_type: option.guid,
      type: this.state.type
    });

    this.refreshRule();
  }

  handleChangeInterval = option => {
    let { rule, changedParams } = this.state;
    
    if(changedParams[`${this.state.type}__${this.state.rule_type}`] && changedParams[`${this.state.type}__${this.state.rule_type}`].interval) {
      changedParams[`${this.state.type}__${this.state.rule_type}`].interval.value = option.name;
    }
    else {
      changedParams[`${this.state.type}__${this.state.rule_type}`] = {
        ...rule[`${this.state.type}__${this.state.rule_type}`],
        interval: {
          ...rule[`${this.state.type}__${this.state.rule_type}`].interval,
          value: option.name
        }
      }
    }
    rule[`${this.state.type}__${this.state.rule_type}`].interval.value = option.name;

    this.setState({ 
      rule,
      changedParams
    });
  }

  handleChangeFromCurrency = option => {
    this.setState({
      from_currency: option.name
    });
  }

  handleChangeToCurrency = async option => {
    this.setState({
      to_currency: option.name
    });
    
    await this.props.getAllRateRuleParams({ 
      rate_guid: "d8c5b253-d583-d14a-20bf-249609336ee9", 
      rule_type: `${this.state.from_currency}_${option.name}`,
      type: this.state.type
    });

    this.refreshRule();

  }

  handleCheck = async() => {
    let { changedParams } = this.state;

    Object.keys(changedParams).forEach(key => {
      if(key.includes("currency_exchange"))
        delete changedParams[key]
    })

    this.setState({ 
      from_currency: "ALL",
      to_currency: "ALL",
      changedParams,
      separately: false
    })

    await this.props.getAllRateRuleParams({
      rate_guid: "d8c5b253-d583-d14a-20bf-249609336ee9", 
      rule_type: "EUR_USD",
      type: this.state.type
    })

    this.refreshRule(true);

  }

  handleCheckS = () => {
    let { changedParams } = this.state;

    Object.keys(changedParams).forEach(key => {
      if(key.includes("currency_exchange"))
        delete changedParams[key]
    })

    this.setState({ 
      changedParams,
      separately: true,
      from_currency: "",
      to_currency: ""
    })
  }

  styleInput = {
    input: {
      width: "calc(80px + 2vw)",
      margin: "auto"
    }
  }

  styleInputMask = {
    input: {
      minWidth: "calc(120px + 3vw)",
      margin: "auto"
    }
  }

  render() {

    let fromCurrencyOptions = [];
    let toCurrencyOptions = [];
    this.state.currencyExchangeFrom.forEach((item, index) => {
      fromCurrencyOptions.push({
        guid: index.toString(),
        name: item
      });
    });
    if (this.state.from_currency) {
      this.state.currencyExchangeTo.filter(item => item !== this.state.from_currency).forEach((item, index) => {
        toCurrencyOptions.push({
          guid: index.toString(),
          name: item
        });
      });
    }

    let typesOptions = Object.keys(this.state.types).map(item => {
      return {
        guid: item,
        name: dict[item]
      };
    });

    const selectorStyles = {
      control: (styles) => {
        return {
          ...styles,
          marginBottom: "30px !important",
          background: "#dedede",
          height: "calc(25px + 1vw)",
          fontSize: "calc(15px + 0.4vw)"
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          width: "100%",
          textAlign: "center",
          marginLeft: "15px"
        };
      },
      placeholder: (styles) => {
        return {
          ...styles,
          width: "100%",
          textAlign: "center",
          marginLeft: "15px"
        };
      }
    };

    const style = this.state.width > 1000 ? { display: "flex", backgroundColor: "rgba(255, 255, 255, 0.4) !important" } : { backgroundColor: "rgba(255, 255, 255, 0.4) !important" };

    const rulesInputs = () => {
      const rule = this.state.rule[
        `${this.state.type}__${this.state.type === "currency_exchange" 
          ? `${this.state.from_currency}_${this.state.to_currency}`
          : this.state.rule_type}`
        ] 
        || {}
      return (
        this.props.loadingParams/*  && this.state.separately */
        ? <Spinner/>
        : this.state.rule_type === "amount" || this.state.type === "currency_exchange"
          ? <>
            {rule.above &&
              <Row style={{ display: "flex", alignItems: "center" }}>
                <Col className="col" md={4}>
                  <Input
                    label="Above (amount)"
                    name="above__value"
                    value={rule.above.value}
                    onChange={this.onChangeHandler}
                    style={this.styleInput}
                  />
                </Col>
                <Col className="col" md={4}>
                  <Input
                    label="Commission (EUR)"
                    name="above__number_rate"
                    type="number"
                    value={rule.above.number_rate}
                    onChange={this.onChangeRateHandler}
                    style={this.styleInput}
                  />
                </Col>
                <Col className="col" md={4}>
                  <Input
                    label="Commission (percent)"
                    name="above__percent_rate"
                    value={`${rule.above.percent_rate} %`}
                    onChange={this.onChangeRateHandler}
                    style={this.styleInput}
                  />
                </Col>
              </Row>}
            {rule.below &&
              <Row style={{ display: "flex", alignItems: "center" }}>
                <Col className="col" md={4}>
                  <Input
                    label="Below (amount)"
                    name="below__value"
                    type="number"
                    value={rule.below.value}
                    onChange={this.onChangeHandler}
                    style={this.styleInput}
                  />
                </Col>
                <Col className="col" md={4}>
                  <Input
                    label="Commission (EUR)"
                    name="below__number_rate"
                    type="number"
                    value={rule.below.number_rate}
                    onChange={this.onChangeRateHandler}
                    style={this.styleInput}
                  />
                </Col>
                <Col className="col" md={4}>
                  <Input
                    label="Commission (percent)"
                    name="below__percent_rate"
                    value={`${rule.below.percent_rate} %`}
                    onChange={this.onChangeRateHandler}
                    style={this.styleInput}
                  />
                </Col>
              </Row>}
            {rule.other &&
              <Row style={{ display: "flex", alignItems: "center" }}>
                <Col className="col" md={4}>
                  <ControlLabel
                    className="col-label"
                  >
                    Other
                  </ControlLabel>
                </Col>
                <Col className="col" md={4}>
                  <Input
                    label="Commission (EUR)"
                    name="other__number_rate"
                    type="number"
                    value={rule.other.number_rate}
                    onChange={this.onChangeRateHandler}
                    style={this.styleInput}
                  />
                </Col>
                <Col className="col" md={4}>
                  <Input
                    label="Commission (percent)"
                    name="other__percent_rate"
                    value={`${rule.other.percent_rate} %`}
                    onChange={this.onChangeRateHandler}
                    style={this.styleInput}
                  />
                </Col>
              </Row>}
          </>
          : this.state.type === "system"
            ? <>
              {rule.interval &&
                <Row>
                  <Col className="col" md={6}>
                    <Input
                      label="Commission (EUR)"
                      name="interval__number_rate"
                      type="number"
                      value={rule.interval.number_rate}
                      onChange={this.onChangeRateHandler}
                      style={this.styleInput}
                    />
                  </Col>
                  <Col className="col" md={6}>
                    <ControlLabel
                      className="col-label"
                    >
                      Interval
                    </ControlLabel>
                    <Select
                      key="interval"
                      options={[ { guid: "1", name: "month" }, { guid: "2", name: "year" } ]}
                      styles={selectorStyles}
                      selectedValue={rule.interval.value}
                      onSelect={this.handleChangeInterval}
                      placeholder="Select..."
                    />
                  </Col>
                </Row>}
            </>
            : <>
              {rule.equals &&
                <Row style={{ display: "flex", alignItems: "center" }}>
                  <Col className="col" md={4}>
                    <Input
                      label="Equals"
                      name="equals__value"
                      // type="number"
                      value={rule.equals.value}
                      onChange={this.onChangeHandler}
                      style={this.styleInputMask}
                    />
                  </Col>
                  <Col className="col" md={4}>
                    <Input
                      label="Commission (EUR)"
                      name="equals__number_rate"
                      type="number"
                      value={rule.equals.number_rate}
                      onChange={this.onChangeRateHandler}
                      style={this.styleInput}
                    />
                  </Col>
                  <Col className="col" md={4}>
                    <Input
                      label="Commission (percent)"
                      name="equals__percent_rate"
                      value={`${rule.equals.percent_rate} %`}
                      onChange={this.onChangeRateHandler}
                      style={this.styleInput}
                    />
                  </Col>
                </Row>
                }
              {rule.mask &&
                <Row style={{ display: "flex", alignItems: "center" }}>
                  <Col className="col" md={4}>
                    <Input
                      label="Mask"
                      name="mask__value"
                      value={rule.mask.value}
                      onChange={this.onChangeHandler}
                      style={this.styleInputMask}
                    />
                  </Col>
                  <Col className="col" md={4}>
                    <Input
                      label="Commission (EUR)"
                      name="mask__number_rate"
                      type="number"
                      value={rule.mask.number_rate}
                      onChange={this.onChangeRateHandler}
                      style={this.styleInput}
                    />
                  </Col>
                  <Col className="col" md={4}>
                    <Input
                      label="Commission (percent)"
                      name="mask__percent_rate"
                      value={`${rule.mask.percent_rate} %`}
                      onChange={this.onChangeRateHandler}
                      style={this.styleInput}
                    />
                  </Col>
                </Row>}
              {rule.other &&
                <Row style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
                  <Col className="col" md={4}>
                    <ControlLabel
                      className="col-label"
                    >
                      Other
                    </ControlLabel>
                  </Col>
                  <Col className="col" md={4}>
                    <Input
                      label="Commission (EUR)"
                      name="other__number_rate"
                      type="number"
                      value={rule.other.number_rate}
                      onChange={this.onChangeRateHandler}
                      style={this.styleInput}
                    />
                  </Col>
                  <Col className="col" md={4}>
                    <Input
                      label="Commission (percent)"
                      name="other__percent_rate"
                      value={`${rule.other.percent_rate} %`}
                      onChange={this.onChangeRateHandler}
                      style={this.styleInput}
                    />
                  </Col>
                </Row>}
            </>
      );
    };

    let ruleTypeOptions = [];
    if (this.state.types[this.state.type]) {
      [ ... new Set(this.state.types[this.state.type]) ].forEach((ruleType, index) => {
        ruleTypeOptions.push({
          guid: ruleType,
          name: dict[ruleType]
        });
      });
    }

    return (
      <Form autoComplete="off">
        <Row>
          <Col style={style}>
            <Card
              header="Info"
              style={{
                // background: "#80dbff"
              }}
            >
              {this.state.loading 
              ? <Spinner/>
              : <Row>
                <Col className="col">
                  <Input
                    label="Name*"
                    name="name"
                    value={this.state.name}
                    onChange={this.onChangeHandler}
                  />
                </Col>
                <Col className="col">
                  <Input
                    label="Description*"
                    name="description"
                    value={this.state.description}
                    onChange={this.onChangeHandler}
                  />
                </Col>
                <Col className="col">
                  <ControlLabel
                    className="col-label"
                  >
                    Type
                  </ControlLabel>
                  <Select
                    key="type"
                    options={typesOptions}
                    styles={selectorStyles}
                    selectedValue={this.state.type}
                    onSelect={this.handleChangeType}
                    placeholder="Select..."
                  />
                </Col>
                {this.state.type === "currency_exchange"
                  ? <>
                    <Col className="col" md={6}>
                      <ControlLabel
                        className="col-label"
                      >
                        From currency
                      </ControlLabel>
                      <Select
                        key="from_currency"
                        options={fromCurrencyOptions}
                        styles={selectorStyles}
                        selectedValue={this.state.separately ? this.state.from_currency : ""}
                        onSelect={this.handleChangeFromCurrency}
                        placeholder={this.state.separately ? "Select..." : "All"}
                        isSearchable
                        disabled={!this.state.separately}
                      />
                    </Col>
                    <Col className="col" md={6}>
                      <ControlLabel
                        className="col-label"
                      >
                        To currency
                      </ControlLabel>
                      <Select
                        key="to_currency"
                        options={toCurrencyOptions}
                        styles={selectorStyles}
                        selectedValue={this.state.separately ? this.state.to_currency : ""}
                        onSelect={this.handleChangeToCurrency}
                        placeholder={this.state.separately ? "Select..." : "All"}
                        isSearchable
                        disabled={!this.state.separately}
                      />
                    </Col>
                    <Col md={3}/>
                    <Col className="col-currency" md={3}>
                      <ControlLabel
                        className="col-label"
                      >
                        Fill in separately
                      </ControlLabel>
                      <input 
                        type="checkbox" 
                        checked={this.state.separately} 
                        onChange={this.handleCheckS}
                      />
                    </Col>
                    <Col className="col-currency" md={3}>
                      <ControlLabel
                        className="col-label"
                      >
                        Fill in equally
                      </ControlLabel>
                      <input 
                        type="checkbox" 
                        checked={!this.state.separately} 
                        onChange={this.handleCheck}                      
                      />
                    </Col>
                    <Col md={3}/>
                  </>
                  : <Col className="col">
                    <ControlLabel
                      className="col-label"
                    >
                      Rule type
                    </ControlLabel>
                    <Select
                      key="rule_type"
                      options={ruleTypeOptions}
                      styles={selectorStyles}
                      selectedValue={this.state.rule_type}
                      onSelect={this.handleChangeRuleType}
                      placeholder="Select..."
                    />
                  </Col>}
              </Row>}
            </Card>
            {this.state.type && (this.state.rule_type || ((this.state.from_currency && this.state.to_currency) || !this.state.separately)) &&
              <Card
                header="Rules"
                style={{
                  // background: "#80dbff"
                }}
              >
                {rulesInputs()}
              </Card>}
          </Col>
          <div align="center">
            {this.state.isLoading
              ? <ReactLoading type='cylon' color='grey' />
              : <Button
                className="submit-button"
                style={{
                  marginTop: "40px"
                }}
                onClick={this.handleSubmit}
              >
                Create
              </Button>}
          </div>
        </Row>
      </Form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pageSize: state.rates.pageSize,
    currentPage: state.rates.currentPage,
    currencies: state.currencies.currencies,
    rate_rules: state.rates.rate_rules,
    rate_rule_params: state.rates.rate_rule_params,
    loadingParams: state.rates.loadingParams
  };
};

export default connect(mapStateToProps, {
  createNewRate,
  showModal,
  getAllCurrencies,
  getAllRateRules,
  getAllRateRuleParams,
})(RateCreator);

RateCreator.propTypes = {
  createNewRate: PropTypes.func,
  rule: PropTypes.object,
  showModal: PropTypes.func,
  currentPage: PropTypes.string,
  pageSize: PropTypes.string,
  currencies: PropTypes.array,
  getAllCurrencies: PropTypes.func,
  getAllRateRules: PropTypes.func,
  rate_rules: PropTypes.array,
  rate_rule_params: PropTypes.array,
  getAllRateRuleParams: PropTypes.func,
  rate: PropTypes.object
};