import React from "react";
import Content from "ibankViews/Content";
import { getAllRateRules, getAllRateRuleParams } from "ibankActions/rates";
import { Col, Row, ControlLabel, Table } from "react-bootstrap";
import Card from "ibankComponents/UI/Card";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Select from "ibankComponents/UI/MultiSelect";
import { getAllCurrencies } from "ibankActions/currencies";

class RateDetail extends React.Component {
  state = {
    rule: {},
    type: "",
    rule_type: "",
    from_currency: "",
    to_currency: "",
    currencyExchangeFromCurrencies: [],
    types: {}
  }

  async componentDidMount() {
    await this.props.getAllRateRules({ rate_guid: this.props.match.params.id });
    let rule = {};
    let currencyExchangeFromCurrencies = [];
    let types = {};
    this.props.rate_rules.map(async item => {
      // await this.props.getAllRateRuleParams({ rate_guid: this.props.match.params.id, rule_guid: item.guid });
      if (item.type === "currency_exchange" && !currencyExchangeFromCurrencies.includes(item.rule_type.split("_")[0])) {
        currencyExchangeFromCurrencies.push(item.rule_type.split("_")[0]);
        // this.setState({
        //   currencyExchangeFromCurrencies
        // });
      }
      if (types[item.type]) {
        if (!types[item.type].includes(item.rule_type))
          types[item.type].push(item.rule_type);
      }
      else {
        types[item.type] = [ item.rule_type ];
      }
      // this.setState({ types });
      const _params = rule[`${item.type}__${item.rule_type}`] ? rule[`${item.type}__${item.rule_type}`].params : [];
      console.log(_params)
      rule[`${item.type}__${item.rule_type}`] = {
        params: [           
          ..._params,
          {
            ...item.params[0]
          }
        ]
      };
      
      // console.log(rule.currency_exchange__USD_BGN)
      // this.setState({ rule });
    });
    this.setState({
      types,
      rule,
      currencyExchangeFromCurrencies
    })
    await this.props.getAllCurrencies();
  }

  handleChangeFromCurrency = option => {
    this.setState({
      from_currency: option.name
    });
  }

  handleChangeToCurrency = option => {
    this.setState({
      to_currency: option.name
    });
  }

  handleChangeType = option => {
    this.setState({
      type: option.name,
      rule_type: "",
      from_currency: "",
      to_currency: ""
    });
  }

  handleChangeRuleType = option => {
    this.setState({
      rule_type: option.name
    });
  }

  renderTable = rule_type => {
    console.log(this.state.rule[`${this.state.type}__${rule_type}`].params)
    if (rule_type === "recurring_payment") {
      return <Table className="table">
        <tbody>
          <tr>
            <td colSpan="3" style={{ textAlign: "center" }}><h3>Rule {rule_type}:</h3></td>
          </tr>
          {this.state.rule[`${this.state.type}__${rule_type}`].params.map((param, param_index) => {
            return <tr key={param_index}>
              <td id="rate"><ControlLabel style={{ color: "black" }}>{param.name}:</ControlLabel></td>
              <td id="rate">{param.value}</td>
            </tr>;
          })}
        </tbody>
      </Table>;
    }
    return <Table className="table">
      <tbody>
        <tr>
          <td colSpan="3" style={{ textAlign: "center" }}><h3>Rule {rule_type}:</h3></td>
        </tr>
        <tr style={{ backgroundColor: "#e0e0e0" }}>
          <th></th>
          <th>Value</th>
          <th>Rate</th>
        </tr>
        {this.state.rule[`${this.state.type}__${rule_type}`] && this.state.rule[`${this.state.type}__${rule_type}`].params.map((param, param_index) => {
          if (param.name !== "rate")
            return <tr key={param_index}>
              <td style={{ backgroundColor: "#e0e0e0" }}><ControlLabel style={{ color: "black" }}>{param.compare_action}</ControlLabel></td>
              <td>{param.compare_value}</td>
              <td>{`${param.number_rate} + ${param.percent_rate}%`}</td>
            </tr>;
        })}
      </tbody>
    </Table>;
  }

  render() {
    let fromCurrencyOptions = [];
    let toCurrencyOptions = [];
    this.state.currencyExchangeFromCurrencies.map((item, index) => {
      fromCurrencyOptions.push({
        guid: index.toString(),
        name: item
      });
    });
    if (this.state.from_currency) {
      this.props.currencies.filter(filterItem => filterItem.name !== this.state.from_currency).map((item, index) => {
        if (this.state.rule[`${this.state.type}__${this.state.from_currency}_${item.name}`])
          toCurrencyOptions.push({
            guid: index.toString(),
            name: item.name
          });
      });
    }

    let typesOptions = Object.keys(this.state.types).map((item, index) => {
      return {
        guid: index.toString(),
        name: item
      };
    });

    const rule_type = this.state.type === "currency_exchange" ? `${this.state.from_currency}_${this.state.to_currency}` : this.state.rule_type;

    let ruleTypeOptions = [];
    let ruleTypes = [];
    switch (this.state.type) {
    case "outgoing_FFCCTRNS": {
      ruleTypes = [
        "amount",
        "paymentTransactionId",
        "paymentInstructionId",
        "paymentEndToEndId",
        "currency",
        "senderAccount",
        "receiverAccount",
        "serviceLevel",
        "chargeBearer",
        "senderName",
        "senderBIC",
        "receiverName",
        "receiverBIC",
        "remittanceInformation"
      ];
      ruleTypes.map((ruleType, index) => {
        if (this.state.rule[`${this.state.type}__${ruleType}`]) {
          ruleTypeOptions.push({
            guid: index.toString(),
            name: ruleType
          });
        }
      });
      break;
    }
    case "Internal": {
      ruleTypes = [
        "senderCurrency",
        "receiverCurrency",
        "senderNumber",
        "receiverNumber",
        "amount"
      ];
      ruleTypes.map((ruleType, index) => {
        if (this.state.rule[`${this.state.type}__${ruleType}`]) {
          ruleTypeOptions.push({
            guid: index.toString(),
            name: ruleType
          });
        }
      });
      break;
    }
    case "system": {
      ruleTypes = [
        "recurring_payment"
      ];
      ruleTypes.map((ruleType, index) => {
        if (this.state.rule[`${this.state.type}__${ruleType}`]) {
          ruleTypeOptions.push({
            guid: index.toString(),
            name: ruleType
          });
        }
      });
      break;
    }
    default: {
      break;
    }
    }
    const selectorStyles = {
      control: (styles) => {
        return {
          ...styles,
          marginBottom: "30px !important",
          background: "#dedede",
          height: "40px",
          fontSize: "calc(15px + 0.4vw)"
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          width: "100%",
          textAlign: "center",
          marginLeft: "15px"
        };
      },
      placeholder: (styles) => {
        return {
          ...styles,
          width: "100%",
          textAlign: "center",
          marginLeft: "15px"
        };
      }
    };
    return (
      <Content>
        <Row>
          <Col className="col" md={3}></Col>
          <Col className="col" md={6}>
            <Card
              header={`${this.props.match.params.name} Rules`}
              style={{
                // background: "#80dbff"
              }}
            >
              <Col className="col">
                <ControlLabel
                  className="col-label"
                >
                  Type
                </ControlLabel>
                <Select
                  key="type"
                  options={typesOptions}
                  styles={selectorStyles}
                  selectedValue={this.state.type}
                  onSelect={this.handleChangeType}
                  placeholder="Select..."
                />
              </Col>
              {this.state.type === "currency_exchange" ? <Row><Col className="col" md={6}>
                <ControlLabel
                  className="col-label"
                >
                  From currency
                </ControlLabel>
                <Select
                  key="from_currency"
                  options={fromCurrencyOptions}
                  styles={selectorStyles}
                  selectedValue={this.state.from_currency}
                  onSelect={this.handleChangeFromCurrency}
                  placeholder="Select..."
                  isSearchable
                />
              </Col>
              <Col className="col" md={6}>
                <ControlLabel
                  className="col-label"
                >
                    To currency
                </ControlLabel>
                <Select
                  key="to_currency"
                  options={toCurrencyOptions}
                  styles={selectorStyles}
                  selectedValue={this.state.to_currency}
                  onSelect={this.handleChangeToCurrency}
                  placeholder="Select..."
                  isSearchable
                />
              </Col>
              </Row>
                : <Col className="col">
                  <ControlLabel
                    className="col-label"
                  >
                    Rule type
                  </ControlLabel>
                  <Select
                    key="rule_type"
                    options={ruleTypeOptions}
                    styles={selectorStyles}
                    selectedValue={rule_type}
                    onSelect={this.handleChangeRuleType}
                    placeholder="Select..."
                  />
                </Col>}
              {this.state.rule[`${this.state.type}__${rule_type}`] && this.renderTable(rule_type)}
            </Card>
          </Col>
          <Col className="col" md={3}></Col>
        </Row>
      </Content>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    rate_rules: state.rates.rate_rules,
    rate_rule_params: state.rates.rate_rule_params,
    currencies: state.currencies.currencies
  };
};

export default connect(mapStateToProps, {
  getAllRateRules,
  getAllRateRuleParams,
  getAllCurrencies
})(RateDetail);

RateDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string
    })
  }),
  rate_rule_params: PropTypes.array,
  rate_rules: PropTypes.array,
  getAllRateRules: PropTypes.func,
  getAllRateRuleParams: PropTypes.func,
  getAllCurrencies: PropTypes.func,
  currencies: PropTypes.array
};