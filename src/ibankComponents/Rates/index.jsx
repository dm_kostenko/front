import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "ibankFactories/Table";
import { getAllRates } from "ibankActions/rates";
import { searchInRates, reset } from "ibankActions/search";
import { Link } from "react-router-dom";
import Modal from "ibankComponents/UI/Modal";
import Editor from "./Editor";

const columns = [
  {
    path: "guid",
    label: "ID",
    // content: rate => {
    //   return <Link className="link" to={`rates/${rate.guid}/${rate.name}`}>{rate.guid}</Link>;
    // }
  },
  {
    path: "name",
    label: "Name"
  },
  {
    path: "description",
    label: "Description"
  },
  {
    key: "edit",
    content: rate => (
      <Modal
        header="Edit rate"
        content={<Editor rate={rate} />}
        button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }} />}
        state="rates"
        dialogClassName="modal-wide"
      />
    ),
    label: "Edit"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.rates.rates,
    count: state.rates.count,
    searchData: state.search.ratesSearch,
    isSearch: state.search.isSearch,
    loading: state.rates.loading,
    name: "rates",
    columns
  };
};

export default connect(mapStateToProps, {
  get: getAllRates,
  search: searchInRates,
  reset
})(AbstractComponent);