import React, { Component } from "react";
import {
  Button,
  FormControl,
  FormGroup,
  Row,
  Col
} from "react-bootstrap";
import swal from "sweetalert";
import { connect } from "react-redux";

import { parseResponse } from "helpers/parseResponse";
import { setNewPasswordAction, checkRecoveryTokenAction } from "ibankActions/auth";
import Spinner from "ibankComponents/UI/Spinner";
import Content from "ibankViews/Content";

class RecoveryPassword extends Component {
  state = {
    newPassword: "",
    confirmPassword: "",
    showNewPassword: false,
    showConfirmPassword: false,
    token: "",

    statusToken: "",

    isTokenTrue: false,
    isTokenBroken: false,
    isPasswordConfirmed: false,

    errorPassword: ""
  };

  async componentDidMount() {
    const token = this.props.match.params.token;
    this.setState({ token });

    await this.props.checkRecoveryTokenAction({ token });
    this.setState({ statusToken: this.props.statusToken });

    if (this.state.statusToken === "true") {
      this.setState({ isTokenTrue: true });
    } else if (this.state.statusToken === "Token does not exist") {
      this.setState({ isTokenBroken: true });
    }
  }

  handleShowNewPassword = () => {
    this.setState({ showNewPassword: !this.state.showNewPassword })
  }

  handleShowConfirmPassword = () => {
    this.setState({ showConfirmPassword: !this.state.showConfirmPassword })
  }

  isPasswordConfirmed = () => {
    if (this.state.newPassword === this.state.confirmPassword) {
      this.setState({
        isPasswordConfirmed: true
      });
      return true;
    } else {
      this.setState({
        isPasswordConfirmed: false
      });
      return false;
    }
  };

  handleChange = e => {
    const property = e.target.name;
    const value = e.target.value;
    this.setState({
      [property]: value
    });
  };

  handleBack = () => {
    this.props.history.push("/");

    window.location.reload();
  };

  handleSubmit = async e => {
    e.preventDefault();
    if (!this.isPasswordConfirmed()) {
      this.setState({ errorPassword: "Passwords don't match. Try again" });
    } 
    else {
      this.setState({ errorPassword: "" });
      try {
        const { newPassword, token } = this.state;
        await this.props.setNewPasswordAction({ password: newPassword, token });
        swal({
          title: this.props.statusPassword,
          icon: "success"
        });
      } 
      catch (error) {
        const parsedError = parseResponse(error);
        this.setState({
          errorPassword: parsedError.message || parsedError.error
        });
      }
    }
  };

  render() {
    const {
      newPassword,
      confirmPassword,
      statusToken,
      isTokenTrue,
      isTokenBroken,
      errorPassword
    } = this.state;
    const { statusPassword } = this.props;

    if (statusToken === "")
      return (
        <Content>
          <Spinner />
        </Content>
      );
    else
      return (
        <div className="confirm-email">
          <div className="confirm-form">
            <div className="card" style={{ padding: "20px" }}>
              <p className="header-confirm">
                <strong>Recovery password</strong>
              </p>
              {statusPassword === "Password changed" ? (
                <>
                  <p style={{ color: "grey", textAlign: "center" }}>
                    New password set successfully
                  </p>
                  <div style={{ textAlign: "center" }}>
                    <Button
                      className="btn btn-fill btn-primary"
                      onClick={this.handleBack}
                      style={{ width: "60%" }}
                    >
                      Return to sign in
                    </Button>
                  </div>
                </>
              ) : isTokenTrue ? (
                <form>
                  <Row className="rowReg">
                    <Col md={11} sm={10} xs={10}>
                  <FormGroup controlId="newEmail">
                    <FormControl
                      placeholder="New password"
                      name="newPassword"
                      value={newPassword}
                      onChange={e => this.handleChange(e)}
                      type={this.state.showNewPassword ? "text" : "password"}
                    />
                    <Button className="eye" onClick={this.handleShowNewPassword}>
                      {this.state.showNewPassword
                        ? <span className="fa fa-eye-slash" />
                        : <span className="fa fa-eye" />
                      }
                    </Button>
                    {errorPassword !== "" && (
                      <p className="validate-error">{errorPassword}</p>
                    )}
                  </FormGroup>
                  </Col>
                  </Row>
                  <Row className="rowReg">
                  <Col md={11} sm={10} xs={10}>
                  <FormGroup controlId="confirmEmail">
                    <FormControl
                      placeholder="Confirm password"
                      name="confirmPassword"
                      value={confirmPassword}
                      onChange={e => this.handleChange(e)}
                      type={this.state.showConfirmPassword ? "text" : "password"}
                    />
                    <Button className="eye" onClick={this.handleShowConfirmPassword}>
                      {this.state.showConfirmPassword
                        ? <span className="fa fa-eye-slash" />
                        : <span className="fa fa-eye" />
                      }
                    </Button>
                  </FormGroup>
                  </Col>
                  </Row>
                  <div style={{ marginBottom: "10px", textAlign: "center" }}>
                    <span style={{ color: "grey" }}>
                      Password must be at least 8 characters, including a
                      number, a special, and an uppercase letter.
                    </span>
                  </div>

                  <div style={{ textAlign: "center" }}>
                    <Button
                      className="btn btn-fill btn-primary"
                      type="submit"
                      onClick={this.handleSubmit}
                      style={{ width: "60%" }}
                    >
                      Continue
                    </Button>
                  </div>
                </form>
              ) : isTokenBroken ? (
                <p style={{ color: "grey", textAlign: "center" }}>
                  It looks like you clicked on an invalid password reset link.
                  Please try again.
                </p>
              ) : (
                <p style={{ color: "grey", textAlign: "center" }}>
                  The token for your account has expired. Please change your
                  password at your earliest convenience.
                </p>
              )}
            </div>
          </div>
        </div>
      );
  }
}

const mapStateToProps = state => {
  return {
    statusToken: state.auth.statusToken,
    statusPassword: state.auth.statusPassword
  };
};

export default connect(mapStateToProps, {
  setNewPasswordAction,
  checkRecoveryTokenAction
})(RecoveryPassword);
