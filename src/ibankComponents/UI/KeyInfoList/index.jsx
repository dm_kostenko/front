import React from "react";
import PropTypes from "prop-types";

const KeyInfoList = ({ children }) => {
  return (
    <div className="key-information-wrapper">
      <div className="key-information labels">
        {children}
      </div>
    </div>
  );
};

KeyInfoList.propTypes = {
  children: PropTypes.array
};

export default KeyInfoList;