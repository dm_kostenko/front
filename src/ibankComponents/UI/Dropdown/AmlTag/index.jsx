import React, { Component } from 'react';
import Button from '@atlaskit/button';
import Select, { defaultTheme, components } from 'react-select';
import AmlTag from "ibankComponents/UI/AmlTag"
import Input from "ibankComponents/UI/Input/Handler"

const { colors } = defaultTheme;

const selectStyles = {
  placeholder: (styles) => {
    return {
      ...styles,
      color: "black",
      fontSize: "calc(8px + 0.4vw)"
    };
  },
  control: (styles, state) => {
    return {
      ...styles,
      background: "rgba(255,255,255,0.4) !important",
      // marginBottom: "30px !important",
      // width: "100%",
      // height: this.state.height > 920 ? "calc(3vw + 9px) !important" : "50px !important",
      transition: ".8s all",
      boxShadow: "none !important",
      cursor: "text",
      "&:hover": {
        // background: state.menuIsOpen ? "white !important" : "transparent !important",
        transition: ".8s all"
      }
    };
  },
  singleValue: (styles) => {
    return {
      ...styles,
      // width: "100%",
      whiteSpace: "pre-line !important",
      // marginLeft: "auto",
      // marginRight: "auto"
    }
  },
  menu: (styles) => {
    return {
      ...styles,
      background: "transparent !important"
    }
  },
  option: (styles, { isSelected, isFocused }) => {
    return {
      ...styles,
      fontSize: "calc(10px + 0.3vw)",
      cursor: "pointer",
      backgroundColor: isSelected
        ? isFocused
          ? "transparent !important"
          : "transparent !important"
        : isFocused
          ? "transparent !important"
          : styles.backgroundColor,
      color: "black",
      textDecoration: isFocused
        ? "underline !important"
        : "none",
      ":active": {
        backgroundColor: isFocused
          ? "#aedbd2"
          : styles.backgroundColor
      }
    }
  }
};


export default class PopoutExample extends Component {
  state = { 
    isOpen: false, 
    selected: 0,
    countries: [],
    attachSearch: ""
  };


  componentDidMount = () => {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  handleClickOutside = (e) => {
    if (this.ref && !this.ref.contains(e.target)) {
      this.setState({ isOpen: false });
    }
  }

  toggleOpen = () => {
    this.setState(state => ({ isOpen: !state.isOpen }));
  };

  handleChange = e => this.setState({ [e.target.name]: e.target.value })

  render() {
    const { isOpen, attachSearch } = this.state;
    const length = Object.keys(this.props.checked).filter(i => this.props.checked[i]).length
    const options = this.props.options.filter(tag => tag.name.toLowerCase().includes(attachSearch.toLowerCase()))
    return (
      <div ref={ref => this.ref = ref}>
      <Dropdown
        
        isOpen={this.props.doNotOpen ? false : isOpen}
        onClose={this.toggleOpen}
        target={
          <Button
            iconAfter={<ChevronDown isOpen={isOpen} />}
            onClick={this.toggleOpen}
            isSelected={isOpen}
            className={this.props.amlSearch ? "dropdown-button-aml" : "dropdown-button"}
          >
            {this.props.placeholder || `Selected: ${length}`}
          </Button>
        }
      >
        <div className={this.props.className || "card selector"}>
          <div>
            <div className="aml-attach-tag-search-wrapper">
              <div className="aml-attach-tag-search-icon">
                <i className="fa fa-search" />
              </div>
              <div className="aml-attach-tag-search-input">
                <Input 
                  placeholder="Search tags..." 
                  onChange={this.handleChange} 
                  name="attachSearch"
                  value={this.state.attachSearch}
                />
              </div>
            </div>
            <div className="aml-attach-tag-col">
              {options.map((tag, i) => (
                <>
                  <div className="aml-attach-tag-row">
                    <div className="aml-attach-tag-checkbox-2">
                      <input 
                        id={i}
                        type="checkbox" 
                        className="checkbox" 
                        checked={this.props.checked[tag.name]} 
                        onClick={() => this.props.handleCheck(tag.name)}
                      />
                    </div>
                    <div className="aml-attach-tag-tag-2">
                      <AmlTag 
                        tag={tag.name}     
                        color={tag.color}
                        htmlFor={i}
                        style={{
                          cursor: "pointer"
                        }}
                      />
                    </div>
                    <div className="aml-attach-tag-value">
                      <Input 
                        placeholder={!this.props.checked[tag.name] ? "" :"Enter tag value"} 
                        id={!this.props.checked[tag.name] ? "disabled" : ""}
                        disabled={!this.props.checked[tag.name]}
                        onChange={(e) => this.props.handleChangeValue(tag.name, e)}
                        value={this.props.tagsValues[tag.name]}
                      />
                    </div>
                  </div>
                  {(i !== this.props.options.length - 1) && <hr className="aml-attach-tag-hr" />}
                </>))}
            </div>
          </div>
        </div>
      </Dropdown>
      </div>
    );
  }
}

// styled components

const Menu = props => {
  const shadow = 'hsla(218, 50%, 10%, 0.1)';
  return (
    <div
      css={{
        backgroundColor: 'white',
        borderRadius: 4,
        boxShadow: `transparent !important`,
        marginTop: 8,
        position: 'absolute',
        zIndex: 2,
      }}
      {...props}
    />
  );
};
const Blanket = props => (
  <div
    css={{
      bottom: 0,
      left: 0,
      top: 0,
      right: 0,
      position: 'fixed',
      zIndex: 1,
    }}
    {...props}
  />
);
const Dropdown = ({ children, isOpen, target, onClose }) => (
  <div css={{ position: 'relative' }}>
    {target}
    {isOpen ? <Menu>{children}</Menu> : null}
    {isOpen ? <Blanket onClick={onClose} /> : null}
  </div>
);
const Svg = p => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    focusable="false"
    role="presentation"
    {...p}
  />
);
const DropdownIndicator = () => (
  <div css={{ color: colors.neutral20, height: 24, width: 32 }}>
    <Svg>
      <path
        d="M16.436 15.085l3.94 4.01a1 1 0 0 1-1.425 1.402l-3.938-4.006a7.5 7.5 0 1 1 1.423-1.406zM10.5 16a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11z"
        fill="currentColor"
        fillRule="evenodd"
      />
    </Svg>
  </div>
);
const ChevronDown = ({ isOpen }) => (
  <i className={isOpen ? "fas fa-chevron-up" : "fas fa-chevron-down"} style={{ fontSize: "calc(7px + 0.2vw)" }}/>
  // <Svg style={{ marginRight: -6 }}>
  //   <path
  //     d="M8.292 10.293a1.009 1.009 0 0 0 0 1.419l2.939 2.965c.218.215.5.322.779.322s.556-.107.769-.322l2.93-2.955a1.01 1.01 0 0 0 0-1.419.987.987 0 0 0-1.406 0l-2.298 2.317-2.307-2.327a.99.99 0 0 0-1.406 0z"
  //     fill="currentColor"
  //     fillRule="evenodd"
  //   />
  // </Svg>
);