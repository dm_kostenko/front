import React from "react";
import PropTypes from "prop-types";

const ListingList = ({ source, header, children }) => {
  return (
    <div className="listing-wrapper">
      <div className="listing source">
        {source}
      </div>
      <div className="listing header-labels-wrapper">
        <div className="listing-header">
          {header}
        </div>
        {children}
      </div>
    </div>
  );
};

ListingList.propTypes = {
  source: PropTypes.string,
  header: PropTypes.string,
  children: PropTypes.array
};

export default ListingList;