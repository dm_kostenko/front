import React, { Component } from "react";
import PropTypes from "prop-types";
import numeral from "numeral";

class DashboardLabel extends Component {

  state = {
    timeout: 6000,
    currentIndex: 0,
    isShow: false
  }


  changeItem = () => {
    setInterval(async () => {
      await this.setState({ currentIndex: (this.state.currentIndex + 1) % this.props.data.length })
    }, this.state.timeout);
  }

  componentDidMount = () => {
    if(this.props.animate) {
      this.changeItem();
    }
  }

  render() {
    if(!this.props.animate)
      return (
        <div>
          <span className="dashboard-label-sum">
            {`${this.props.data.sum || this.props.data[0].sum}  `}
            <span id="dashboard-label-currency">
              {this.props.data.currency || this.props.data[0].currency}
            </span>
          </span>
        </div>
      );
    const { currentIndex } = this.state;
    return (
      <div>
        {this.props.data.map((item, i) => {    
          const [ beforeDot, afterDot ] = item.sum.split(".");
          return (
            <span 
              key={i}
              id={i === currentIndex ? "show" : "hide"}
              className="dashboard-label-sum"
            >
              {afterDot 
                ? `${numeral(beforeDot).format("0,0")}.${afterDot} `
                : `${numeral(beforeDot).format("0,0")} `
              }
              <span id="dashboard-label-currency">
                {item.currency}
              </span>
            </span>);
        })}
      </div>
    );
  }
}

export default DashboardLabel;

DashboardLabel.propTypes = {
  sum: PropTypes.string,
  currency: PropTypes.string
};
