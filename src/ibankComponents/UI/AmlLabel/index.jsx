import React from "react";
import {
  Row,
  Col,
  ControlLabel
} from "react-bootstrap";
import PropTypes from "prop-types";

const AmlLabel = ({ label, value, half }) => {
  let left = 5;
  let right = 7;
  if(half) {
    left = 6;
    right = 6;
  }
  return (
    <Row className="row-aml">
      <Col className="col-info-label-aml" md={left} xs={left} lg={left}>
        <ControlLabel 
          htmlFor={label} 
          className="info-label"
        >
          {label}
        </ControlLabel>
      </Col>
      <Col className="col-info-value" md={right} xs={right} lg={right}>
        <ControlLabel 
          htmlFor={value} 
          className="info-value"
        >
          {value}
        </ControlLabel>
      </Col>
    </Row>
  );
};

export default AmlLabel;

AmlLabel.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  half: PropTypes.bool
};
