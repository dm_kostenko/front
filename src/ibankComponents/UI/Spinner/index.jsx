import ReactLoading from "react-loading";
import React from "react";


const Spinner = (props) => {
  return ( 
    <div className={props.className || "loading"} style={props.style || {}}>
      <ReactLoading type="spokes" color={props.color || "grey"} />
    </div>
  );
};
 
export default Spinner;
