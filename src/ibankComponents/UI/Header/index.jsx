import React, { Component } from "react";
import { Navbar } from "react-bootstrap";
import HeaderLinks from "./HeaderLinks.jsx";
import { navRoutes } from "routes/dashboard.jsx";
import PropTypes from "prop-types";

class Header extends Component {
  constructor(props) {
    super(props);
    this.mobileSidebarToggle = this.mobileSidebarToggle.bind(this);
    this.state = {
      sidebarExists: false
    };
  }
  mobileSidebarToggle(e) {
    if (this.state.sidebarExists === false) {
      this.setState({
        sidebarExists: true
      });
    }
    e.preventDefault();
    document.documentElement.classList.toggle("nav-open");
    const node = document.createElement("div");
    node.id = "bodyClick";
    node.onclick = function () {
      this.parentElement.removeChild(this);
      document.documentElement.classList.toggle("nav-open");
    };
    // document.body.appendChild(node);
  }
  getBrand() {
    let name;
    navRoutes.map((prop) => {
      if (prop.collapse) {
        prop.views.forEach((prop) => {
          if (prop.path === this.props.location.pathname) {
            name = prop.name;
          }
        });
      } else {
        if (prop.redirect) {
          if (prop.path === this.props.location.pathname) {
            name = prop.name;
          }
        } else {
          if (prop.path === this.props.location.pathname) {
            name = prop.name;
          }
        }
      }
      return null;
    });
    return name;
  }
  render() {
    return (
      <div id="header" className="nav-bar">
        {/* <Navbar.Header> */}
        {/* <Navbar.Brand>
            <span style={{ textTransform: "uppercase" }}>{this.getBrand()}</span>
          </Navbar.Brand> */}
        {/* <Navbar.Toggle onClick={this.mobileSidebarToggle} /> */}
        {/* </Navbar.Header> */}
        {/* <Navbar.Header> */}
        <HeaderLinks />
        {/* </Navbar.Header> */}
      </div>
    );
  }
}

export default Header;

Header.propTypes = {
  location: PropTypes.object,
};
