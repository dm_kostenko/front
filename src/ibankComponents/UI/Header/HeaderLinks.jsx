import React, { Component } from "react";
import { NavItem, Nav, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { popLocation } from "ibankActions/history";
import { getToken, flushTokens, getUserLoginsData } from "services/paymentBackendAPI/backendPlatform";
import Spinner from "ibankComponents/UI/Spinner";
import EntitySelector from "ibankComponents/EntitySelector";
import { ability } from "config/ibankAbility";
import { withRouter } from "react-router-dom";

const loginsData = getUserLoginsData();
class HeaderLinks extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tokenTtl: 0,
    };
  }

  async componentDidMount() {
    setInterval(() => {
      const token = getToken();
      if (token && token.exp) {
        let exp = new Date((token.exp - Math.round(+new Date() / 1000)) * 1000);
        if (exp.getMinutes() === 0 & exp.getSeconds() === 0) {
          flushTokens();
        }
        this.setState({ tokenTtl: exp });        
      }   
    }
    , 1000);
  }

  handleBack = () => {
    console.log(this.props.pathHistory)
    const path = this.props.pathHistory[this.props.pathHistory.length - 2];
    this.props.popLocation();
    this.props.history.push(path);
  }

  getTokenTime = () => {
    return (
      <span
        style={
          this.state.tokenTtl.getMinutes() < 1 
            ? {
              color: "red",
              fontWeight: "bold"
            }
            : {}
        }
      >
        {this.state.tokenTtl.getMinutes()}:{(this.state.tokenTtl.getSeconds() < 10 ? "0" : "") + this.state.tokenTtl.getSeconds()}
      </span>
    );
  }

  render() {
    return (
      <div>
        <Nav pullLeft>
          {this.props.pathHistory.length > 1 &&
          <NavItem>
            <Button
              onClick={this.handleBack}
              className="back-button"
            >
              <i
                className="fas fa-arrow-left icon"
                style={{ color: "black" }}
              />
            </Button>
          </NavItem>}
        </Nav>
        <Nav id="header-right" pullRight>    
          <NavItem>
            {loginsData && ability("RELOGIN") &&
            <EntitySelector
              data={loginsData}
            />}
          </NavItem>
          <NavItem id="session-time" eventKey="disabled" disabled>             
            {this.state.tokenTtl instanceof Date
              ? <>
                {"Session time: "}
                {this.getTokenTime()}
                </>              
              : <Spinner className="session-loading" color="white" />}
          </NavItem>
          <NavItem
            id="logout-item"
            eventKey={3}
            className="nav-link"
            onClick={() => window.location.replace(process.env.PUBLIC_URL+"#/logout")}
          >
            <a id="logout-button">
              <i className="fas fa-sign-out-alt logout-icon"/>
            </a>
          </NavItem>
        </Nav>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  pathHistory: state.history.history
});

export default withRouter(connect(mapStateToProps, {
  popLocation
})(HeaderLinks));
