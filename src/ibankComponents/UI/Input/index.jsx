import React from "react";
import {
  Col,
  ControlLabel,
  FormControl,
  FormGroup,
  Alert
} from "react-bootstrap";
import PropTypes from "prop-types";
import { refreshTokens } from "services/paymentBackendAPI/backendPlatform"

const changeHandler = async (e, cb) => {
  cb(e)
  await refreshTokens()
}

const Input = ({ name, label, error, style, inputLabel, note, placeholder, onChange, ...rest }) => {
  style = style || {};
  return (
    <Col className={(style.default && style.default.className) ? style.default.className : "col"} style={{ ...style.default }}>
      <ControlLabel 
        htmlFor={name} 
        className="col-label"
        style={{ ...style.label, textTransform: "none" }}
      >
        {label}
      </ControlLabel>
      <FormGroup>
        {inputLabel &&
        <a
          className="input-label"
        >{inputLabel}</a>}
        {note !== "" &&
        <a
          className="input-note"
        >{note}</a>}
        <FormControl
          {...rest}
          name={name}
          id={name}
          placeholder={placeholder}
          onChange={(e) => changeHandler(e, onChange)}
          style={{              
            textAlign: "center", 
            fontSize: "calc(15px + 0.4vw)",       
            height: "calc(25px + 1vw)",    
            ...style.input
          }}
        />
        {error && <Alert>{error}</Alert>}
      </FormGroup>
    </Col>
  );
};

export default Input;

Input.propTypes = {
  name: PropTypes.node,
  inputLabel: PropTypes.string,
  label: PropTypes.string,
  error: PropTypes.string,
  style: PropTypes.object,
  note: PropTypes.string
};
