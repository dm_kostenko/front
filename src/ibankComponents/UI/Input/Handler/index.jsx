import React from "react";
import { refreshTokens } from "services/paymentBackendAPI/backendPlatform"

const changeHandler = async (e, cb) => {
  cb(e)
  await refreshTokens()
}

const Input = ({ onChange, ...rest }) => (
  <input
    onChange={(e) => changeHandler(e, onChange)}
    {...rest}
  />
)

export default Input;