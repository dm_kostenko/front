import React, { Component } from "react";
import * as d3 from "d3";
import dagreD3 from "dagre-d3";

class Graph extends Component {

  state = { 
    g: ""
  }
    
componentDidMount = () => {
  var g;
  if(this.props.data) {
    const entities = this.props.data.map(item => item.entity_username);
    const founders = this.props.data.map(item => item.founder_username);
    const all = [ ...new Set([ ...entities, ...founders ]) ];

    const [ rootFounder ] = all.filter(item => {
      return !this.props.data.find(_item => item === _item.entity_username)
    });

    // Create the input graph
    g = new dagreD3.graphlib.Graph()
      .setGraph({})
      .setDefaultEdgeLabel(function() { return {}; });

    // Here we"re setting nodeclass, which is used by our custom drawNodes function
    // below.

    all.forEach(item => {
      g.setNode(item,  { 
        label: item,
        shape: item === rootFounder ? 'rect' : 'ellipse',
        style: 'stroke: black; fill:white; stroke-width: 1px;',
        labelStyle: "font: 300 14px 'Helvetica Neue', Helvetica;fill: black;"
      });      
    });

    g.nodes().forEach(function(v) {
      var node = g.node(v);
      // Round the corners of the nodes
      node.rx = node.ry = 5;
    });

    // Set up edges, no special attributes.

    this.props.data.forEach(item => {
      g.setEdge(item.founder_username, item.entity_username, {
        curve: d3.curveBasis,
        style: 'stroke: gray; fill:none; stroke-width: 1px;',
        arrowhead: "undirected",
        arrowheadStyle: 'fill: gray',
      });
    })
    
    // inner = svg.select('g');

    // // Create the renderer
    var render = new dagreD3.render();
    var svg = d3.select(this.nodeTree);

    render(d3.select(this.nodeTreeGroup), g);

    svg.attr("height", g.graph().height + 40);

  }
}

render() {
  return (
    <svg 
      id="nodeTree"
      ref={(ref) => { this.nodeTree = ref; }}
      // height="500"
    >
      <g
        ref={(r) => { this.nodeTreeGroup = r; }}
      />
    </svg>
  )
}
};

export default Graph;