import React from "react";
import { connect } from "react-redux";
import { inverseSidebar } from "actions/sidebar";
import PropTypes from "prop-types";


const SidebarHideButton = ({ isHide, inverseSidebar }) => {
  return (
    <div
      id="sidebar-hide-button-div"
      className={isHide ? "slideOut" : "slideIn"}
      onClick={async () => await inverseSidebar()}
    >
      <i
        id="sidebar-hide-button"
        className={isHide ? "fas fa-angle-double-right slideOut" : "fas fa-angle-double-left slideIn"}
      />
      <span
        id="sidebar-hide-text"
        className={isHide ? "slideOut" : "slideIn"}
      >Collapse sidebar</span>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    isHide: state.sidebar.isHide
  };
};

export default connect(mapStateToProps, {
  inverseSidebar
})(SidebarHideButton);

SidebarHideButton.propTypes = {
  isHide: PropTypes.bool,
  inverseSidebar: PropTypes.func
};