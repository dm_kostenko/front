import React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";


const SidebarHeader = ({ isHide }) => {
  return (
    <NavLink to="/">
      <div className="logo"/>
    </NavLink>
  );
};

const mapStateToProps = (state) => {
  return {
    isHide: state.sidebar.isHide
  };
};

export default connect(mapStateToProps, null)(SidebarHeader);

SidebarHeader.propTypes = {
  isHide: PropTypes.bool
};