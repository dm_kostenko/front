import React, { Component } from "react";
import { connect } from "react-redux";
import { hideSidebar } from "ibankActions/sidebar";
import SidebarWrapper from "./Wrapper";
import SidebarHeader from "./Header";
import SidebarHideButton from "./HideButton";
import PropTypes from "prop-types";


import { navRoutes } from "routes/dashboard.jsx";

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: window.innerWidth,
      collapse: []
    };
  }

  updateDimensions = () => {
    // this.setState({ width: window.innerWidth });
    // if(window.innerWidth <= 991) {
    //   this.props.hideSidebar();
    // }
  }

  componentDidMount() { 
    // this.updateDimensions();
    // window.addEventListener("resize", this.updateDimensions.bind(this));
  }

  render() {
    return (
      <>
      <div
        id="sidebar"
        className={this.props.isHide ? "slideOut" : "slideIn"}
      >
        <SidebarHeader/>
        <SidebarWrapper routes={navRoutes} />     
      </div>                
      {/* <SidebarHideButton/>    */}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isHide: state.sidebar.isHide
  };
};

export default connect(mapStateToProps, {
  hideSidebar
})(Sidebar);


Sidebar.propTypes = {
  hideSidebar: PropTypes.func,
  isHide: PropTypes.bool
};
