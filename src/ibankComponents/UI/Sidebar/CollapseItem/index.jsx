import React from "react";
import { connect } from "react-redux";
import { Button, Collapse } from "react-bootstrap";
import { onCollapseItemClick } from "actions/sidebar";
import SidebarCollapseItemChild from "./Child";
import PropTypes from "prop-types";


const SidebarCollapseItem = ({ item, key, isHide, onCollapseItemClick, collapseItemState }) => {

  return (
    <li
      key={key}
    >
      <Button
        id="sidebar-active"
        title={isHide ? item.name : ""}
        className={isHide ? "slideOut" : "slideIn"}
        onClick={() => onCollapseItemClick(item.state)}
        aria-controls={item.state}
        aria-expanded={item.path === collapseItemState}
        style={{ transition: ".4s linear" }}
      >
        <i 
          id="sidebar-icon"
          className={isHide ? `${item.icon} slideOut` : `${item.icon} slideIn`} 
        />
        <div id="sidebar-text-wrapper">
          <p 
            id="sidebar-text"
            className={isHide ? "slideOut" : "slideIn"}
          >
            {item.name}
          </p>                        
        </div>
        <b 
          id="sidebar-caret"
          className={isHide ? "caret slideOut" : "caret slideIn"}
        ></b>
      </Button>
      <Collapse in={item.state === collapseItemState}>
        <ul
          className="nav"
          id={item.state}
          style={{ marginTop: "-5px" }}
        >
          {item.views.map((child, key) => {
            return (
              <SidebarCollapseItemChild 
                key={key}
                child={child}
              />
            );
          })}
        </ul>
      </Collapse>
    </li>
  );
};

const mapStateToProps = (state) => {
  return {
    isHide: state.sidebar.isHide,
    collapseItemState: state.sidebar.collapseItemState
  };
};
                
export default connect(mapStateToProps, {
  onCollapseItemClick
})(SidebarCollapseItem);

SidebarCollapseItem.propTypes = {
  item: PropTypes.object,
  key: PropTypes.string,
  isHide: PropTypes.bool,
  onCollapseItemClick: PropTypes.func, 
  collapseItemState: PropTypes.string
};