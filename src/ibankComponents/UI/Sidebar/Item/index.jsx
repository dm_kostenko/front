import React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";


const activePath = (path, location) => {
  return location.includes(path) ? "active" : "";
};


const SidebarItem = ({ item, key, isHide, location }) => {

  return (
    <li
      className={activePath(item.path, location)}
      key={key}
      style={{ marginBottom: "2px" }}
    >
      <NavLink
        title={isHide ? item.name : ""}
        to={item.path}
        id="sidebar-active"
        className={isHide ? "nav-link slideOut" : "nav-link slideIn"} 
        activeClassName="active"
        style={{ height: "50px", width: isHide ? "50px" : "" }}
      >
        <p 
          id="sidebar-text" 
          className={isHide ? "slideOut" : "slideIn"}
        >
          {item.name}
        </p>
      </NavLink>
    </li>
  );
};

const mapStateToProps = (state) => {
  return {
    isHide: state.sidebar.isHide,
    location: window.location.hash
  };
};
                
export default connect(mapStateToProps, null)(SidebarItem);

SidebarItem.propTypes = {
  item: PropTypes.object,
  key: PropTypes.string,
  isHide: PropTypes.bool,
  location: PropTypes.string
};