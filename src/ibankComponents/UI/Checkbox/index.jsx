import React from "react";
import {
  Col,
  ControlLabel,
  FormGroup,
  Alert
} from "react-bootstrap";
import PropTypes from "prop-types";

const Checkbox = ({ name, label, error, colClassname, containerClassname, style, ...rest }) => {
  style = style || {};
  return (
    <Col className={colClassname || "col-checkbox"} style={{ ...style.default }}>
      <ControlLabel 
        htmlFor={name} 
        className="col-checkbox-label"
        style={{ ...style.label }}
      >
        {label}
      </ControlLabel>
      {containerClassname 
      ? <div 
        className={containerClassname}
      >
        <input
          {...rest}
          type="checkbox"  
          className="checkbox"        
        />
      </div>
      : <FormGroup
        className="checkbox-form"
      >
        <input
          {...rest}
          type="checkbox"  
          className="checkbox"        
        />
        {error && <Alert>{error}</Alert>}
      </FormGroup>}
    </Col>
  );
};

export default Checkbox;

Checkbox.propTypes = {
  name: PropTypes.node,
  inputLabel: PropTypes.string,
  label: PropTypes.string,
  error: PropTypes.string,
  style: PropTypes.object
};
