import React from "react";
import { ControlLabel } from "react-bootstrap";
import PropTypes from "prop-types";

const Card = (props) => {
  return (
    <div 
      className={`card ${props.className || ""}`}
      style={{
        ...props.style
      }}
    >
      <ControlLabel
        className={props.detail ? "card-header detail" : `card-header ${props.className}`}
      >
        {props.header}        
      </ControlLabel>
      {!props.withoutTitle &&
      <>
        <hr className="hr" />
        <br/>
      </>}
      {props.children}
    </div>
  );
};

Card.propTypes = {
  style: PropTypes.object,
  detail: PropTypes.bool,
  children: PropTypes.array,
  header: PropTypes.string
};

export default Card;