import React from "react";
import { ControlLabel } from "react-bootstrap";
import PropTypes from "prop-types";

const CardGrid = (props) => {
  return (
    <div 
      className="card"
      style={{
        ...props.style,
        textAlign: "center"
      }}
    >
      <ControlLabel
        className="card-header"
      >
        {props.header}        
      </ControlLabel>
      <br/><br/>
      <ControlLabel
        className="card-body"
      >
        {props.body}
      </ControlLabel>
      <br/><br/>
    </div>
  );
};

CardGrid.propTypes = {
  style: PropTypes.object,
  detail: PropTypes.bool,
  body: PropTypes.string,
  header: PropTypes.string
};

export default CardGrid;