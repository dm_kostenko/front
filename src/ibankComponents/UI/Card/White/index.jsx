import React from "react";
import { ControlLabel } from "react-bootstrap";
import PropTypes from "prop-types";

const CardWhite = (props) => {
  return (
    <div 
      className={`card-white ${props.className || ""}`}
      style={{
        ...props.style
      }}
    > 
      {props.entityType &&
      <div className="entity-type-wrapper">
        <a className="entity-type">
          {props.entityType}
        </a>
      </div>}
      {props.header &&
      <ControlLabel
        className={props.detail ? "card-header-white detail" : `card-header-white ${props.className}`}
      >
        {props.header}        
      </ControlLabel>}
      {!props.withoutTitle && <br/>}
      {props.children}
    </div>
  );
};

CardWhite.propTypes = {
  style: PropTypes.object,
  detail: PropTypes.bool,
  children: PropTypes.array,
  header: PropTypes.string,
  entityType: PropTypes.string
};

export default CardWhite;