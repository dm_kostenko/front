import React from "react";
import PropTypes from "prop-types";

const KeyInfoLabel = ({ label, value }) => {
  return (
    <div className="key-information-label">
      <div className="key-information key">
        {label}
      </div>
      <div className="key-information value">
        {value}
      </div>
    </div>
  );
};

export default KeyInfoLabel;

KeyInfoLabel.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string
};
