import React from "react";
import {
  Row,
  Col,
  ControlLabel
} from "react-bootstrap";
import PropTypes from "prop-types";
import CardWhite from "../Card/White"
import moment from "moment";
import { Link } from "react-router-dom";

const getFirstLetters = name => name.split(" ").map(i => i[0]).join("")

const Comment = ({ name, date, text, entityId, caseName, caseId }) => {
  return (
    <Row className="row-aml-comments">
      <CardWhite className="comments">
        <Col className="col-comments-avatar" md={2} xs={2} lg={2}>
          <CardWhite className="comments-avatar" withoutTitle>
            <div>{getFirstLetters(name)}</div>
          </CardWhite>
        </Col>
        <Col className="col-comments-text" md={10} xs={10} lg={10}>
          <div className="comment-name">
            {name}
          </div>
          <div className="comment-date">
            {moment(date).format("MMM DD, YYYY hh:mm A")}
          </div>
          {entityId && caseName && caseId &&
          <div classname="comment-entity">
            <Link
              className="comment-entity-link"
              to={`/aml/${caseId}/entity/${entityId}`}
            >
              {`${caseName} (${entityId})`}
            </Link>
          </div>}
          <div className="comment-text">
            {text}
          </div>
        </Col>
      </CardWhite>
    </Row>
  );
};

export default Comment;

Comment.propTypes = {
  name: PropTypes.string,
  date: PropTypes.string,
  text: PropTypes.string
};
