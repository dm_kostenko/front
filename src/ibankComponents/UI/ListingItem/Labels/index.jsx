import React from "react";
import PropTypes from "prop-types";

const ListingItemLabels = ({ data }) => {
  return (
    <div className="listing labels">
      {Object.keys(data).map(key => (
        <div className="listing-label">
        <div className="listing key">
          {key}
        </div>
        <div className="listing value">
          {data[key]}
        </div>
      </div>
      ))}          
    </div>
  );
};

export default ListingItemLabels;

ListingItemLabels.propTypes = {
  source: PropTypes.string,
  header: PropTypes.string,
  data: PropTypes.object,
  children: PropTypes.array
};
