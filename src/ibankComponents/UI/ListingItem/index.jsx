import React from "react";
import PropTypes from "prop-types";

const ListingItem = ({ header, status, url, data }) => {
  return (
    <div className="listing labels-wrapper">
      <div className="listing-status">
        {status}
      </div>
      <div className="listing labels-header">
        <a href={url}>{header}</a>
      </div>
      <div className="listing listed-time">
        <i className="far fa-clock"/>
        {` Listed `}<a>{data.listed}</a> 
        {data.removed && 
        <>
          <i className="far fa-clock"/>
          {` Removed `} <a>{data.removed}</a> 
        </>}
      </div>
      <div className="listing labels">
        {Object.keys(data).filter(i => ![ "name", "url", "status", "listed", "removed" ].includes(i.toLowerCase())).map(key => (
          <div className="listing-label">
            <div className="listing key">
              {key}
            </div>
            <div className="listing value">
              {data[key]}
            </div>
          </div>))}          
      </div>
    </div>
  );
};

export default ListingItem;

ListingItem.propTypes = {
  header: PropTypes.string,
};
