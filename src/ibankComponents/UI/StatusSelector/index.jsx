import React, { Component } from "react";
import { connect } from "react-redux";
import { setCurrentSearchData } from "ibankActions/search";
import Select from "ibankComponents/UI/MultiSelect";
import PropTypes from "prop-types";

class StatusSelector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ""
    };
  }

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.reset) {
      return {
        value: ""
      };
    }     
  }

  parseValue = (key, value) => {
    if(value === "Success") {
      if(key === "enabled")
        return 1;
      return value;
    }
    else {
      if(key === "enabled")
        return 0;
      return value;
    }          
  }

  handleSelect = (option) => {
    this.setState({
      name: option.name
    });
    if(this.props.transactionType)
      this.props.search({
        [this.props.value]: option.name
      }, this.props.transactionType);
    else
      this.props.search({
        [this.props.value]: option.name
      });
    this.props.setCurrentSearchData({
      [this.props.value]: option.name
    });
  }

  types = {
    inbox: [
      {
        guid: "0",
        name: "Imported"
      },
      {
        guid: "1",
        name: "Errored"
      },
      {
        guid: "2",
        name: "Received"
      }
    ],
    // transactionsDocs: [
    //   {
    //     guid: "0",
    //     name: "prepared"
    //   },
    //   {
    //     guid: "1",
    //     name: "imported"
    //   }
    // ],
    transactions: [
      {
        guid: "0",
        name: "Unconfirmed"
      },
      {
        guid: "1",
        name: "Unapproved"
      },
      {
        guid: "2",
        name: "Cancelled"
      },
      {
        guid: "3",
        name: "Approved"
      },
      {
        guid: "4",
        name: "Declined"
      },
      {
        guid: "5",
        name: "Sent"
      },
      {
        guid: "6",
        name: "Processed"
      }      
    ],
    accounts: [
      {
        guid: "0",
        name: "active"
      },
      {
        guid: "1",
        name: "blocked"
      }
    ],
    transfers: [
      {
        guid: "0",
        name: "Success"
      },
      {
        guid: "1",
        name: "Failed"
      }
    ]
  }

  render() {
    return (
      <div
        id="search-wrapper"
        className={this.props.isSearch ? "searchOpen" : "searchClosed"}
      >
        <Select
          multi={false}
          // placeholder={`SELECT ${this.props.type.toUpperCase()} STATUS`}
          options={this.types[this.props.type]}
          onSelect={this.handleSelect}
          isSearchable={false}
          styles={{
            placeholder: (styles) => {
              return {
                ...styles,
                color: "black",
                fontSize: "14px"
              };
            },
            menu: (styles) => {
              return {
                ...styles,
                color: "black !important",
                fontSize: "14px !important",
                textTransform: "capitalize"
              };
            },
            control: (styles) => {
              return {
                ...styles,
                minHeight: "0px !important",
                height: "32px !important",
                marginBottom: "5px !important",
                boxShadow: "inset 0 0 5px rgba(0,0,0,0.1), inset 0 1px 2px rgba(0,0,0,0.3)",
                borderRadius: "5px",
                border: "none",
                background: "#d8d8d8",
                font: "15px Tahoma, Arial sans-serif",
                textAlign: "center !important",
                paddingLeft: "10%"
              };
            },
            singleValue: (styles) => {
              return {
                ...styles,
                width: "100%",
                marginLeft: "auto",
                marginRight: "auto",
                fontSize: "14px",
                textTransform: "capitalize"
              };
            }
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    reset: state.search.reset,
    isSearch: state.search.isSearch
  };
};

export default connect(mapStateToProps, {
  setCurrentSearchData
})(StatusSelector);

StatusSelector.propTypes = {
  value: PropTypes.string,
  reset: PropTypes.bool,
  search: PropTypes.func,
  setCurrentSearchData: PropTypes.func,
  type: PropTypes.string,
  isSearch: PropTypes.bool,
  transactionType: PropTypes.string
};