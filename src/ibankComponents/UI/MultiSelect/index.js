import React from "react";
import Select from "react-select";
import makeAnimated from "react-select/lib/animated";
import PropTypes from "prop-types";

const MultiSelect = (props) => {

  const convertOptions = (options) => {
    return options.map(item => {
      item.label = item.name;
      item.value = item.guid;
      return item;
    });
  };

  convertOptions(props.options);

  if(props.defaultValue) {
    return (
      <div>
        <Select
          isMulti={props.multi}
          selectOption={props.options[0]}
          components={props.components}
          options={props.options}
          className={props.name}          
          classNamePrefix="select"
          value={props.options.filter(option => option.name === props.defaultValue)}
          onChange = {props.onSelect}
          styles = {props.styles}
          isSearchable={props.isSearchable || false}
          placeholder={props.placeholder || "Select..."}
          isDisabled={props.disabled}
          {...props}
        />
      </div>
    );
  }
  else if(props.hasOwnProperty("selectedValue")) {
    return (
      <div>
        <Select
          isMulti={props.multi}
          selectOption={props.options[0]}
          components={props.components}
          options={props.options}
          className={props.name}          
          classNamePrefix="select"
          value={props.selectedValue === null ? props.selectedValue : props.options.filter(option => option.name === props.selectedValue || option.guid === props.selectedValue)}
          onChange = {props.onSelect}
          styles = {props.styles}
          isSearchable={props.isSearchable || false}
          placeholder={props.placeholder || "Select..."}
          isDisabled={props.disabled}
          {...props}
        />
      </div>
    );
  }
  else
    return (
      <div>
        <Select
          isMulti={props.multi}
          selectOption={props.options[0]}
          components={props.components}
          options={props.options}
          className={props.name}
          classNamePrefix="select"
          onChange = {props.onSelect}
          styles = {props.styles}
          isSearchable = {props.isSearchable || false}
          placeholder={props.placeholder || "Select..."}
          isDisabled={props.disabled}
          {...props}
        />
      </div>
    );
};
 
export default MultiSelect;


MultiSelect.propTypes = {
  multi: PropTypes.bool,
  options: PropTypes.array,
  name: PropTypes.string,
  onSelect: PropTypes.func,
};