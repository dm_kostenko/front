import React from "react";
import { Bar } from "react-chartjs-2";
import PropTypes from "prop-types";
import { ExportButton } from "components/ExportButton";

export const BarChart = (props) => {
  const data = {
    labels: props.labels,
    datasets: props.datasets
  };

  
  return (
    <>
    <Bar 
      data={data} 
      height={props.height}
      options={props.options}
    />
    {/* <ExportButton
      data={props.data}
      name={props.name}
    >
      export
    </ExportButton> */}
    </>
  );
};

BarChart.propTypes = {
  name: PropTypes.string,
  labels: PropTypes.array,
  datasets: PropTypes.array,
  height: PropTypes.number,
  options: PropTypes.array,
  data: PropTypes.object,
};
