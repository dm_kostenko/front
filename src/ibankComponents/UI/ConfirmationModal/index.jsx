import React from "react";
import { Modal } from "react-bootstrap";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { showConfirmationModal } from "ibankActions/modal";

class ConfirmationModal extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      modalIsOpen: false
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    // if (nextProps.modalState !== prevState.modalIsOpen && !nextProps.modalState) {
    //   return {
    //     modalIsOpen: nextProps.modalState
    //   };
    // }
    if(nextProps.modalState !== prevState.modalIsOpen) {
      return {
        modalIsOpen: nextProps.modalState
      };
    }

    return null;
  }

  handleOpen = async () => {
    await this.props.showConfirmationModal(true);
    this.setState({
      modalIsOpen: true
    });
  }


  handleClose = async () => {
    await this.props.showConfirmationModal(false);
  }

  render() {
    return (
      <React.Fragment>
        <div>
          <Modal show={this.state.modalIsOpen} onHide={this.handleClose} dialogClassName={this.props.dialogClassName ? this.props.dialogClassName : ""}>
            <Modal.Header className="modal-header" closeButton>
              <Modal.Title>{this.props.header}</Modal.Title>
            </Modal.Header>
            <Modal.Body className="modal-body">{this.props.content}</Modal.Body>
          </Modal>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  modalState: state.modal.confirmModalState
});

export default connect(mapStateToProps, {
  showConfirmationModal
})(ConfirmationModal);

ConfirmationModal.propTypes = {
  content: PropTypes.node,
  header: PropTypes.string,
  button: PropTypes.node,
  showModal: PropTypes.func,
  resetModalData: PropTypes.func,
  modalState: PropTypes.string,
  dialogClassName: PropTypes.string,
};