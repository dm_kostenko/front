import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import PropTypes from "prop-types";
import { setCurrentSearchData } from "ibankActions/search";
import Input from "ibankComponents/UI/Input/Handler"

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ""
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount = () => {
    this.setState({
      value: this.props.currentSearchData[this.props.value]
    });
  }

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.reset) {
      return {
        value: ""
      };
    }     
  }

  handleChange = (event) => {
    this.setState({ value: event.target.value });
    if(this.props.transactionType)
      this.props.search({
        [this.props.value]: event.target.value
      }, this.props.transactionType);
    else
      this.props.search({
        [this.props.value]: event.target.value
      });
    this.props.setCurrentSearchData({
      [this.props.value]: event.target.value
    });
  };

  handleTimeChange = (event) => {
    const value = event.target.value;
    this.setState({ value });
    this.props.search({
      [this.props.value]: moment(value).format("YYYY-MM-DDTHH:mm:ss")
    });
  }

  render() {
    const onChange = this.props.type ? this.handleTimeChange : this.handleChange;
    return (
      <div 
        id="search-wrapper"
        className={this.props.isSearch ? "searchOpen search" : "searchClosed search"}
      >
        <Input 
          className="input"
          type={this.props.type ? this.props.type : "search"} 
          style={{ textAlign: "center", height: this.props.type ? "30px" : "" }}
          value={this.state.value} 
          onChange={onChange} 
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    reset: state.search.reset,
    isSearch: state.search.isSearch,
    currentSearchData: state.search.currentSearchData
  };
};

export default connect(mapStateToProps, {
  setCurrentSearchData
})(Search);

Search.propTypes = {
  search: PropTypes.func,
  value: PropTypes.string,
  transactionType: PropTypes.string,
  reset: PropTypes.bool,
  type:  PropTypes.string,
  isSearch: PropTypes.bool,
  setCurrentSearchData: PropTypes.func,
  currentSearchData: PropTypes.object
};