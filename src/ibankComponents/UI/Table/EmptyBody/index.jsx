import React, { Component } from "react";
import { Badge } from "react-bootstrap";


class TableEmptyBody extends Component {
  render() {
    return (
      <div
        style={{
          width: "100%",
          height: "60px",
          textAlign: "center",
          paddingTop: "13%",
          paddingBottom: "13%",
          marginTop: "0",
          borderTopLeftRadius: "0",
          borderTopRightRadius: "0"
        }}
      >
        <Badge 
          pill 
          className="empty-table-badge"
        >
          There is no data
        </Badge>
      </div>
    );
  }
}

export default TableEmptyBody;