import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import PropTypes from "prop-types";

class TableBody extends Component {

  renderCell = (item, column) => {
    if (column.content) { return column.content(item); }
    return _.get(item, column.path);
  };

  createKey = (item, column) => {
    return item.guid + (column.path || column.key);
  };

  render() {
    const { data, columns } = this.props;
    return (
      <tbody key={window.location} style={{ textAlign: "center" }}>
        {data.map(item => (
          <tr 
            key={item.guid}  
          >
            {columns.map(column => (
              <td 
                id={column._isCheckbox ? "slim-col" : ""}
                key={this.createKey(item, column)}
              >
                {this.renderCell(item, column)}
              </td>
            ))}
          </tr>))}
      </tbody>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isSearch: state.search.isSearch
  };
};

export default connect(mapStateToProps, null)(TableBody);

TableBody.propTypes = {
  data: PropTypes.array,
  columns: PropTypes.array,
};