import React, { Component } from "react";
import DashboardTableItem from "./Item";

class DashboardTable extends Component {

  render() {
    return (
      <div className="dashboard-table-accounts">
        {this.props.data.length > 0
          ? this.props.data.map((item, key) => (
            <DashboardTableItem
              key={key}
              number={item.number}
              balances={item.balances}
              type={item.type}
            />
          ))
          : <div className="empty">There are no accounts yet</div>}
      </div>
    );
  }
}

export default DashboardTable;