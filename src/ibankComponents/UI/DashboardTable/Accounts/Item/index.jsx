import React, { Component } from "react";
import { Link } from "react-router-dom";
import numeral from "numeral";

const currencies = {
  USD: "USD",
  EUR: "EUR",
  RUB: "RUB",
  JPY: "JPY",
  BGN: "BGN",
  CZK: "CZK",
  DKK: "DKK",
  GBP: "GBP",
  HUF: "HUF",
  PLN: "PLN",
  RON: "RON",
  SEK: "SEK",
  CHF: "CHF",
  ISK: "ISK",
  NOK: "NOK",
  HRK: "HRK",
  TRY: "TRY",
  AUD: "AUD",
  BLR: "BRL",
  CAD: "CAD",
  CNY: "CNY",
  HKD: "HKD",
  IDR: "IDR",
  ILS: "ILS",
  INR: "INR",
  KRW: "KRW",
  MXN: "MXN",
  MYR: "MYR",
  NZD: "NZD",
  PHP: "PHP",
  SGD: "SGD",
  THB: "THB",
  ZAR: "ZAR"
};

class DashboardTableItem extends Component {

  render() {
    return (
      <div className="dashboard-table-accounts-item">
        <a className="number">
          <Link className="link" style={{ marginRight: "0" }} to={`accounts/${this.props.number}`}>{this.props.number}</Link>
        </a>
        <a className="type">
          {this.props.type}
        </a>
        <a className="balances">
          <ul style={{ listStyle: "none" }}>
            {this.props.balances.map((item, key) => (
              <li key={key}>
                <div className="balance">
                  <b>{`${item.balance.toFixed(2)} ${currencies[item.currency]}`}</b>
                </div>
              </li>
            ))}
          </ul>
        </a>
      </div>
    );
  }
}

export default DashboardTableItem;