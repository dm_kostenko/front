import React, { Component } from "react";
import DashboardTableItem from "./Item";

class DashboardTable extends Component {

  render() {
    return (
      <div className="dashboard-table">
        {this.props.data.length > 0
          ? this.props.data.map((item, key) => (
            <DashboardTableItem
              key={key}
              from={item.from}
              to={item.to}
              amount={item.amount}
              time={item.time}
            />
          ))
          : <div className="empty">There are no {this.props.type} yet</div>}
      </div>
    );
  }
}

export default DashboardTable;