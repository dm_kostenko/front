import React, { Component } from "react";
import moment from "moment";

class DashboardTableItem extends Component {

  render() {
    return (
      <div className="dashboard-table-item">
        <div className="from-to">
          {`${this.props.from}   `}
          <i className="fas fa-long-arrow-alt-right green" />
          {`   ${this.props.to}`}
        </div>
        <div className="amount">{this.props.amount}</div>
        <div className="time">{moment(this.props.time).format("DD MMM YYYY")}</div>
      </div>
    );
  }
}

export default DashboardTableItem;