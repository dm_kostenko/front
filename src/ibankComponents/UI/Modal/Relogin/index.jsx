import React from "react";
import { Modal } from "react-bootstrap";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { showModal, resetModalData } from "ibankActions/modal";

class ModalRelogin extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      modalIsOpen: false,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    // if (nextProps.modalState !== prevState.modalIsOpen && !nextProps.modalState) {
    //   return {
    //     modalIsOpen: nextProps.modalState
    //   };
    // }
    if(nextProps.modalState !== prevState.modalIsOpen) {
      return {
        modalIsOpen: nextProps.modalState
      };
    }

    return null;
  }

  handleOpen = async () => {
    await this.props.showModal(true, this.props.state);
    this.setState({
      modalIsOpen: true
    });
  }


  handleClose = async () => {
    await this.props.showModal(false, this.props.state);
    await this.props.resetModalData();
  }

  render() {
    return (
      <React.Fragment>
        <div>
          <Modal style={{ width: "1000px !important", background: "transparent" }} show={this.state.modalIsOpen} onHide={this.handleClose} dialogClassName={this.props.dialogClassName ? this.props.dialogClassName : ""}>
            <Modal.Header className={this.props.dialogClassName === "modal-wide relogin" ? "modal-header relogin" : "modal-header"} closeButton={this.props.dialogClassName === "modal-wide relogin"} closeButton>
              <Modal.Title>{this.props.header}</Modal.Title>
            </Modal.Header>
            <Modal.Body className={this.props.dialogClassName === "modal-wide relogin" ? "modal-body relogin" : "modal-body"}>{this.props.content}</Modal.Body>
          </Modal>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => ({
  modalState: state.modal[props.state]
});

export default connect(mapStateToProps, {
  showModal,
  resetModalData
})(ModalRelogin);

ModalRelogin.propTypes = {
  content: PropTypes.node,
  header: PropTypes.string,
  button: PropTypes.node,
  showModal: PropTypes.func,
  resetModalData: PropTypes.func,
  modalState: PropTypes.string,
  dialogClassName: PropTypes.string,
};