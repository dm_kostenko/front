import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const MediaItem = (props) => {
  return (
    <div className="media-item">
      <div className="media-icon-wrapper">
        <i className="far fa-newspaper media-icon" />
      </div>
      <div className="media-info">
        <div className="media-header">
          <a href={props.link}>{props.header}</a>
        </div>
        <div className="media-date">
          Published
          <a>{` ${props.date}`}</a>
        </div>
        <div className="media-text">
          {props.text}
        </div>
      </div>
    </div>
  );
};

MediaItem.propTypes = {
  children: PropTypes.array
};

export default MediaItem;