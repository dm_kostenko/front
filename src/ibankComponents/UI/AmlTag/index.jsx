import React from "react";
import PropTypes from "prop-types";
import { Badge, DropdownButton, MenuItem, OverlayTrigger, Popover } from "react-bootstrap";

const AmlTag = ({ tag, value, color, element, style, popoverText, ...rest }) => {
  return (
    <label className="aml-tag" style={{ borderColor: color, ...style }} {...rest}>
      {popoverText 
        ? <OverlayTrigger 
          placement="top"
          overlay={<Popover>{popoverText}</Popover>}
        >
          <a>{value 
            ? tag 
              ? `${tag}: ${value}` 
              : value
            : tag}</a>
        </OverlayTrigger>
        : <a>{value 
          ? tag 
            ? `${tag}: ${value}` 
            : value
          : tag}</a>
      }
      
      {element}
    </label>
  )
}

export default AmlTag;