import React from "react";
import {
  Col,
  ControlLabel,
  FormControl,
  FormGroup,
  Alert
} from "react-bootstrap";
import Select from "../MultiSelect";
import PropTypes from "prop-types";

const SelectForm = ({ name, options, onSelect, iconClassname, isLoading, defaultValue, label, error, style, inputLabel, note, ...rest }) => {
  style = style || {};
  return (
    <Col className="col" style={{ ...style.default }}>
      <ControlLabel 
        htmlFor={name} 
        className="col-label"
        style={{ ...style.label, textTransform: "none" }}
      >
        {label}
      </ControlLabel>
      <Select
        multi={false}
        onSelect={onSelect}
        options={options}
        defaultValue={defaultValue}
        styles={{
          singleValue: styles => ({
            ...styles,
            opacity: isLoading ? "0.6" : "1"
          }),
          option: styles => ({
            ...styles,
            textAlign: "left"
          })
        }}
      />
      {isLoading && <i className={`fas fa-cog fa-spin ${iconClassname}`}/>}
    </Col>
  );
};

export default SelectForm;

SelectForm.propTypes = {
  name: PropTypes.node,
  inputLabel: PropTypes.string,
  label: PropTypes.string,
  error: PropTypes.string,
  style: PropTypes.object,
  note: PropTypes.string
};
