import React from "react";
import _ from "lodash";
import PropTypes from "prop-types";

const Pagination = ({ pagesCount, currentPage, onPageChange, pageSize, count }) => {
  pagesCount = Math.floor(pagesCount);
  const from = currentPage * pageSize - pageSize + 1;
  const to = pageSize * currentPage < count ? pageSize * currentPage : count;
  if (pagesCount > 1) {
    let remainder, startLink, finishLink;
    const buttonsCount = 10;
    remainder = currentPage % buttonsCount === 0 ? Math.floor(currentPage / buttonsCount) - 1 : Math.floor(currentPage / buttonsCount);
    startLink = buttonsCount * remainder + 1;
    finishLink = startLink + buttonsCount > pagesCount ? pagesCount + 1 : buttonsCount * remainder + buttonsCount + 1;
    let pages = _.range(startLink, finishLink);
    return (
      <div>
        <nav>
          <ul className="pagination" style={{ cursor: "pointer" }}>
            <li
              key={"Previous"}
              className={currentPage === 1 ? "page-item disabled" : "page-item"}
            >
              <a className={"page-link"} onClick={currentPage === 1 ? null : () => onPageChange(currentPage - 1)}>
                {"<"}
              </a>
            </li>
            {
              currentPage > buttonsCount ?
                (
                  <li
                    key={1}
                    className={"page-item"}
                  >
                    <a className={"page-link"} onClick={currentPage === 1 ? null : () => onPageChange(1)}>
                      {1}
                    </a>
                  </li>) : null
            }
            {
              currentPage > buttonsCount ?
                (
                  <li
                    key={".."}
                    className={"page-item"}
                  >
                    <a className={"page-link"} onClick={() => onPageChange(currentPage % buttonsCount !== 0 ? (Math.floor(currentPage / buttonsCount) - 1) * buttonsCount + 1 : currentPage - 2 * buttonsCount + 1)}>
                      ...
                    </a>
                  </li>) : null
            }
            {pages.map(page => (
              <li
                key={page}
                className={page === currentPage ? "page-item active" : "page-item"}
              >
                <a className={"page-link"} onClick={page === currentPage ? null : () => onPageChange(page)}>
                  {page}
                </a>
              </li>
            ))}
            
            {
              (Math.floor(pagesCount / buttonsCount) !== Math.floor(currentPage / buttonsCount) 
              || currentPage === Math.floor(pagesCount / buttonsCount) * buttonsCount) 
              && (finishLink - pagesCount !== 1) && (finishLink !== pagesCount) ?
                (
                  <li
                    key={"..."}
                    className={"page-item"}
                  >
                    <a className={"page-link"} onClick={currentPage + buttonsCount > pagesCount ? () => onPageChange(Math.floor(pagesCount / buttonsCount) * buttonsCount + 1) : () => onPageChange(currentPage % 10 !== 0 ? (Math.floor(currentPage / buttonsCount) + 1) * buttonsCount + 1 : currentPage + 1)}>
                      ...
                    </a>
                  </li>) : null
            }
            {
              (Math.floor(pagesCount / buttonsCount) !== Math.floor(currentPage / buttonsCount) 
              || currentPage === Math.floor(pagesCount / buttonsCount) * buttonsCount) 
              && (finishLink - pagesCount !== 1) ?
                (
                  <li
                    key={pagesCount}
                    className={"page-item"}
                  >
                    <a className={"page-link"} onClick={pagesCount === currentPage ? null : () => onPageChange(pagesCount)}>
                      {pagesCount}
                    </a>
                  </li>) : null
            }

            <li
              key={"Next"}
              className={currentPage === pagesCount ? "page-item disabled" : "page-item"}
            >
              <a className={"page-link"} onClick={currentPage === pagesCount ? null : () => onPageChange(currentPage + 1)}>
                {">"}
              </a>
            </li>
          </ul>
        </nav>
        {from != to ? <label>Shown {from}-{to} from {count}</label> : <label>Shown {from} element from {count}</label>}
      </div>
    );
  }
  else if (count > 10)
    return (from != to ? <label>Shown {from}-{to} from {count}</label> : <label>Shown {from} element from {count}</label>);
  else
    return null;
};

export default Pagination;

Pagination.propTypes = {
  pagesCount: PropTypes.number,
  itemsCount: PropTypes.number,
  pageSize: PropTypes.number,
  currentPage: PropTypes.number,
  onPageChange: PropTypes.func
};


