import React from "react";
import { Pie } from "react-chartjs-2";
import PropTypes from "prop-types";
import { ExportButton } from "components/ExportButton";

export const PieChart = (props) => {

  const data = {
    labels: props.labels,
    datasets: props.datasets 
  };
  const firstLabel = props.labels ? props.labels[0] : "";
  return (
    <>
    <Pie
      data={data}
      height={200}
      options={{ maintainAspectRation: false }}
    />
    {!firstLabel.includes("There are no") &&
    <ExportButton
      labels={props.labels}
      datasets={props.datasets}
      name={props.name}
    >
      export
    </ExportButton>}
    </>
  );
};

PieChart.propTypes = {
  name: PropTypes.string,
  options: PropTypes.array,
  height: PropTypes.number,
  labels: PropTypes.array,
  datasets: PropTypes.array,
};
