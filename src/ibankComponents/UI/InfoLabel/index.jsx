import React from "react";
import {
  Row,
  Col,
  ControlLabel
} from "react-bootstrap";
import PropTypes from "prop-types";

const InfoLabel = ({ label, value }) => {
  return (
    <Row className="row">
      <Col className="col-info-label" md={6} xs={6} lg={6}>
        <ControlLabel 
          htmlFor={label} 
          className="info-label"
        >
          {label}
        </ControlLabel>
      </Col>
      <Col className="col-info-value" md={6} xs={6} lg={6}>
        <ControlLabel 
          htmlFor={value} 
          className="info-value"
        >
          {value}
        </ControlLabel>
      </Col>
    </Row>
  );
};

export default InfoLabel;

InfoLabel.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string
};
