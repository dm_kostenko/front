import React from "react";
import { exportChartTable, exportTable } from "helpers/export";
import PropTypes from "prop-types";


export const ExportButton = (props) => {
  const download = () => {
    if(props.table)
      exportTable(props.columns, props.data, props.name.toUpperCase());
    else if(props.data)
      exportChartTable(null, props.data, props.name);
    else
      exportChartTable(props.labels, props.datasets, props.name);
  };

  return (
    <button
      id={props.id}
      key={props.name}
      className="btn btn-export"
      onClick={download}
    >
      export
    </button>
  );
};

ExportButton.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  columns: PropTypes.array,
  data: PropTypes.array,
  datasets: PropTypes.array,
  labels: PropTypes.array,
  table: PropTypes.bool
};