import React, { Component } from "react";
import PropTypes from "prop-types";
import numeral from "numeral";

class HeaderLabel extends Component {

  state = {
    timeout: 6000,
    currentIndex: 0,
    isShow: false
  }


  changeItem = () => {
    setInterval(async () => {
      await this.setState({ currentIndex: (this.state.currentIndex + 1) % this.props.data.length })
    }, this.state.timeout);
  }

  componentDidMount = () => {
    if(this.props.animate) {
      this.changeItem();
    }
  }

  render() {
    if(!this.props.animate)
      return (
        <div className={this.props.className || "header-label"}>
          <span className="header-label-sum">
            {`${this.props.data.sum || this.props.data[0].sum}  `}
            <span id="header-label-currency">
              {this.props.data.currency || this.props.data[0].currency}
            </span>
          </span>
          <br/>
          <span id="header-label-label">
            {this.props.label}
          </span>
        </div>
      );
    const { currentIndex } = this.state;
    return (
      <div className={this.props.className || "header-label"}>
        {this.props.data.map((item, i) => {    
          const [ beforeDot, afterDot ] = item.sum.split(".");
          return (
            <span 
              key={i}
              id={i === currentIndex ? "show" : "hide"}
              className="header-label-sum"
            >
              {afterDot 
                ? `${numeral(beforeDot).format("0,0")}.${afterDot} `
                : `${numeral(beforeDot).format("0,0")} `
              }
              <span id="header-label-currency">
                {item.currency}
              </span>
            </span>)})}
        <br/>
        <span id="header-label-label">
          {this.props.label}
        </span>
      </div>
    );
  }
}

export default HeaderLabel;

HeaderLabel.propTypes = {
  sum: PropTypes.string,
  label: PropTypes.string,
  currency: PropTypes.string
};
