import React from "react";
import {
  Row,
  Col,
  ControlLabel
} from "react-bootstrap";
import PropTypes from "prop-types";

const DetailInfoLabel = ({ label, value, cols }) => {
  if(cols > 1 && label.length > 2 && value.length > 1 && (cols === label.length - 1 && label.length - 1 === value.length))
    return (
      <Row className="row-detail-a">        
        <Col className="main-col-info-label-detail" md={12} xs={12} lg={12}>
          <ControlLabel 
            htmlFor={label[0]} 
            className="main-label"
          >
            {label[0]}
          </ControlLabel>
        </Col>
        <Col md={6} xs={6} lg={6}>
          <ControlLabel 
            htmlFor={label[1]} 
            className="info-label-detail-cols"
          >
            {`${label[1]}:`}
          </ControlLabel>
          <ControlLabel 
            htmlFor={value[0]} 
            className="info-value-detail-cols"
          >
            {value[0]}
          </ControlLabel>
        </Col>
        <Col md={6} xs={6} lg={6}>
          <ControlLabel 
            htmlFor={label[2]} 
            className="info-label-detail-cols"
          >
            {`${label[2]}:`}
          </ControlLabel>
          <ControlLabel 
            htmlFor={value[1]} 
            className="info-value-detail-cols"
          >
            {value[1]}
          </ControlLabel>
        </Col> 
      </Row>
    );
  return (
    <Row className="row-detail">
      <Col className="col-info-label-detail" md={6} xs={6} lg={6}>
        <ControlLabel 
          htmlFor={label} 
          className="info-label-detail"
        >
          {label}
        </ControlLabel>
      </Col>
      <Col className="col-info-value-detail" md={6} xs={6} lg={6}>
        <ControlLabel 
          htmlFor={value} 
          className="info-value-detail"
        >
          {value}
        </ControlLabel>
      </Col>
    </Row>
  );
};

export default DetailInfoLabel;

DetailInfoLabel.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string
};
