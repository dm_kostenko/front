import React, { Component } from "react";
import PropTypes from "prop-types";

class Radio extends Component {
  render() {
    const { number, label, option, name, style = {}, ...rest } = this.props;
    return (
      <div className="radio">
        <input id={number} name={name} type="radio" value={option} style={style.input} {...rest} />
        <label htmlFor={number} style={{ textTransform: "capitalize", color: "#545761", fontSize: "calc(5px + 0.5vw)", ...style.label }}>{label}</label>
      </div>
    );
  }
}

export default Radio;

Radio.propTypes = {
  number: PropTypes.string,
  name: PropTypes.string,
  label: PropTypes.string,
  option: PropTypes.string,
};