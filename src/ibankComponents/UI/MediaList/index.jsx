import React from "react";
import PropTypes from "prop-types";

const MediaList = (props) => {
  return (
    <div className="card media-list">
      {props.children.length && props.children.length > 1 
        ? props.children.map((child, i) => (
          <>
            {child}
            {(i !== props.children.length - 1) && <hr className="media-hr" />}
          </>
        ))
        : props.children
      }
    </div>
  );
};

MediaList.propTypes = {
  children: PropTypes.array
};

export default MediaList;