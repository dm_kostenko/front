import React, { Component } from "react";
import { connect } from "react-redux";
import { logOut } from "ibankActions/auth";
import PropTypes from "prop-types";
import Spinner from "ibankComponents/UI/Spinner";

class LogOut extends Component {
  componentDidMount = async() => {
    await this.props.logOut();
  }

  render() {
    return (
      <div className="LoginPage">
        <div className="login-page">
          <Spinner color="white" style={{ position: "absolute", top: "15%", transform: "skew(0deg, 10deg)" }} />
        </div>
      </div>
    );
  }
}
export default connect(null, {
  logOut
})(LogOut);

LogOut.propTypes = {
  logOut: PropTypes.func,
};