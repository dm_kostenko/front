import React, { Component } from "react";
import { Button, Col, Form, Row, ControlLabel } from "react-bootstrap";
import Card from "ibankComponents/UI/Card";
import { sendOTP } from "ibankActions/auth";
import { showModal } from "ibankActions/modal";
import Input from "ibankComponents/UI/Input";
import { connect } from "react-redux";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";
import QRCode from "qrcode.react";
import { issueToken, setTokens } from "services/paymentBackendAPI/backendPlatform";
import validate from "helpers/validation";
import Joi from "@hapi/joi";




class AuthTypeConfirmation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: ""
    };
  }

  schema = {
    otp: Joi.string().regex(/^[0-9]+$/).length(6).required()
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      enter: true
    });
  };

  formValidation = (data) => {
    return data !== "";
  };

  generalValidation = () => {
    return this.formValidation(this.state.otp);
  };


  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    if (!this.generalValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in field",
        icon: "warning"
      });
    }
    else {
      const { otp } = this.state;
      try {      
        validate({ otp }, this.schema);
        await this.props.sendOTP({ otp });        
        swal("Code is approved", {
          icon: "success",
          button: false,
          timer: 2000
        });
        setTimeout(async() => {
          const response = await issueToken();
          setTokens(response.data.accessToken, response.data.refreshToken);
          await this.props.showModal(false, "profile");
          localStorage.setItem("isLoggedIn", true);
          window.location.replace(process.env.PUBLIC_URL || "/");
        }, 500);        
      }
      catch (error) {
        this.setState({ 
          isLoading: false,          
          otp: ""
        });
        const parsedError = parseResponse(error);
        if((error.response && error.response.status !== 409) || !error.response)        
          swal({
            title: parsedError.error,
            text: parsedError.message,
            icon: "error",
          });
      }
    }
  };

  style={
    default: {
      marginBottom: "30px"
    }
  }


  render() {
    let href = false;
    if(navigator.userAgent.includes("iPhone") || navigator.userAgent.includes("iPod") || navigator.userAgent.includes("iPad"))
      href = "https://apps.apple.com/app/google-authenticator/id388497605";
    if(navigator.userAgent.includes("Android"))
      href = "https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2";   
    if(!this.props.qrCode)
      return (
        <Form autoComplete="off">
          <Row>
            <Col style={{ display: "flex" }}>
              <Card
                header="Confirmation"
                style={{
                  // background: "#80dbff"
                }}
              >
                <Row>
                  <Col md={12}>
                    <div align="center">
                      <ReactLoading type='cylon' color='grey' />
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col md={12}/>
                </Row>
              </Card>            
            </Col>
          </Row>
        </Form>
      );
    return (
      <Form autoComplete="off" onSubmit={this.handleSubmit}>
        <Row>
          <Col style={{ display: "flex" }}>
            <Card
              header="Confirmation"
              style={{
                // background: "#80dbff"
              }}
            >
              <Row>
                <Col className="col" md={12}>
                  <ControlLabel 
                    className="col-label confirm"
                  >
                    Scan the QR code or enter the secret key (under QR code) in the {href 
                      ? <a style={{ color: "blue" }} href={href}>Google authenticator app</a>
                      : "Google authenticator app"}
                    {/* <br/> */}
                    {!href && 
                    <>
                    {" ("}
                    <a style={{ color: "blue" }} href={"https://apps.apple.com/app/google-authenticator/id388497605"}>iOS</a>, 
                    <a style={{ color: "blue" }} href={"https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2"}> Android</a>)
                    {/* <br/>                     */}
                    </>}
                    {", then enter the specified code below"}
                  </ControlLabel>
                </Col>
              </Row>
              <Row>
                <Col className="col" md={12}>
                  <QRCode 
                    value={this.props.qrCode} 
                  />
                </Col>
              </Row>                            
              <Row>
                <Col className="col" md={12}>
                  <ControlLabel className="col-label salt">
                    {this.props.salt.replace(/([A-Z0123456789]{4})/g, "$1 ").replace(/(^\s+|\s+$)/,"")}
                  </ControlLabel>
                </Col>
              </Row>
              <br/>
              <Row>
                <Col className="col" md={3}/>
                <Col className="col" md={6}>
                  <Input
                    label="Code"
                    name="otp"
                    type="password"
                    value={this.state.otp}
                    onChange={this.handleChange}
                    style={this.style}
                  />
                </Col>
                <Col className="col" md={3}/>
              </Row>
            </Card>            
          </Col>          
          <div align="center">
            {this.state.isLoading 
              ? <ReactLoading type='cylon' color='grey' /> 
              : <Button 
                className="submit-button"
                style={{
                  marginTop: "40px"
                }}
                type="submit"
              >
                Confirm
              </Button>}
          </div>
        </Row>
      </Form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    otpResponse: state.auth.otpResponse,
    qrCode: state.auth.qrCode,
    salt: state.auth.salt
  };
};

export default connect(mapStateToProps, {
  sendOTP,
  showModal
})(AuthTypeConfirmation);

AuthTypeConfirmation.propTypes = {
  editTxStatus: PropTypes.func,
  showModal: PropTypes.func,
};