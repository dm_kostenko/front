import React, { Component } from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import Card from "ibankComponents/UI/Card";
import { editTxStatus } from "ibankActions/transactions";
import { addOtpAndSend, reset as resetResponse } from "ibankActions/confirmation";
import { showConfirmationModal } from "ibankActions/modal";
import Input from "ibankComponents/UI/Input";
import { connect } from "react-redux";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";
import validate from "helpers/validation";
import Joi from "@hapi/joi";
class Confirmation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: "",
      enter: false
    };
  }

  schema = {
    otp: Joi.string().regex(/^[0-9]+$/).length(6).required()
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      enter: true
    });
  };

  formValidation = (data) => {
    return data !== "";
  };

  generalValidation = () => {
    return this.formValidation(this.state.otp);
  };


  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    if (!this.generalValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in field",
        icon: "warning"
      });
    }
    else {
      const { otp } = this.state;
      try {    
        validate({ otp }, this.schema);  
        await this.props.addOtpAndSend(otp);
        await this.props.showConfirmationModal(false);
        if(this.props.confirmation && this.props.swalText) {
          swal(this.props.swalText, {
            icon: "success",
            button: false,
            timer: 2000
          });
          this.props.resetResponse();
        }
      }
      catch (error) {
        this.setState({ 
          isLoading: false,
          otp: ""
        });
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
    }
  };

  style={
    default: {
      marginBottom: "30px"
    }
  }


  render() {
    return (
      <Form autoComplete="off" onSubmit={this.handleSubmit}>
        <Row>
          <Col style={{ display: "flex" }}>
            <Card
              header="Confirmation"
              style={{
                // background: "#80dbff"
              }}
            >
              <Row>
                <Col md={3}/>
                <Col className="col" md={6}>
                  <Input
                    label="Code"
                    name="otp"
                    type="password"
                    value={this.state.otp}
                    onChange={this.handleChange}
                    style={this.style}
                  />
                </Col>
                <Col md={3}/>
              </Row>
              <Row>
                <Col md={12}>
                  <div align="center" className="confirmation">
                    {this.state.enter ? "" : this.props.responseOtp.message}
                  </div>
                </Col>
              </Row>
            </Card>            
          </Col>          
          <div align="center">
            {this.state.isLoading 
              ? <ReactLoading type='cylon' color='grey' /> 
              : <Button 
                className="submit-button"
                style={{
                  marginTop: "40px"
                }}
                type="submit"
              >
                Send
              </Button>}
          </div>
        </Row>
      </Form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    confirmation: state.confirmation.confirmation,
    swalText: state.confirmation.swalText,
    responseOtp: state.transactions.responseOtp
  };
};

export default connect(mapStateToProps, {
  editTxStatus,
  showConfirmationModal,
  addOtpAndSend,
  resetResponse
})(Confirmation);

Confirmation.propTypes = {
  editTxStatus: PropTypes.func,
  showConfirmationModal: PropTypes.func,
};