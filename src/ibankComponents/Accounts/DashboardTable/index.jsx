import React from "react";
import { connect } from "react-redux";
import { Button } from "react-bootstrap";
import Card from "ibankComponents/UI/Card";
import PropTypes from "prop-types";
import DashboardTable from "ibankComponents/UI/DashboardTable/Accounts";
import { getAllAccounts } from "ibankActions/accounts";
import Spinner from "ibankComponents/UI/Spinner";

class AccountsDashboardTable extends React.Component {
  state = {
    loading: true
  }

  componentDidMount = async() => {
    await this.props.getAllAccounts();
  }

  render() {
    if(this.props.loading)
      return (
        <Card
          header="Accounts"
          className="transactions"
          style={{ flex: "2" }}
        >
          <Spinner style={{ marginBottom: "5%" }}/>
        </Card>
      );
    return (
      <Card
        header="Accounts"
        className="transactions"
        style={{ flex: "2" }}
      >
        <DashboardTable
          data={this.props.accounts.slice(0, 5).map(account => (
            {
              number: account.number,
              balances: account.balances,              
              type: account.type
            }
          ))}
        />
        {this.props.accounts.length > 5 && 
        <div className="show-more">
          <Button
            className="submit-button show-more"
            onClick={() => window.location.replace(process.env.PUBLIC_URL+"#/accounts")}
          >
            Show all
          </Button>
        </div>}
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    accounts: state.accounts.accounts,
    loading: state.accounts.loading
  };
};

export default connect(mapStateToProps, {
  getAllAccounts
})(AccountsDashboardTable);

AccountsDashboardTable.propTypes = {
  types: PropTypes.object,
  loading: PropTypes.bool
};