import React, { Component } from "react";
import { connect } from "react-redux";
import { getBalancesDocs } from "ibankActions/accounts";
import Table from "ibankComponents/UI/Table";
import { Row, Col } from "react-bootstrap";
import { downloadXML } from "helpers/downloadXML";
import Spinner from "ibankComponents/UI/Spinner";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import moment from "moment";

const equal = item => item.openingBalance === item.closingBalance

const parseDiv = (item, value) => (
  equal(item)
    ? value
    : <div style={{ fontWeight: "bold" }}>{value}</div>
)

const getClassName = item => {
  return item.openingBalance === item.closingBalance
    ? ""
    : "balance"
}

class AccountsBalances extends Component {

  state = {
    data: [],
    width: 0
  }

  columns = [
    {
      path: "date",
      label: "Date",
      content: item => parseDiv(item, moment(item.date).format("DD.MM.YYYY")),
      className: item => item.openingBalance === item.closingBalance
          ? ""
          : "balance"
    },
    {
      path: "openingBalance",
      label: "Opening balance",
      content: item => parseDiv(item, item.openingBalance + " EUR"),
      // className: item => getClassName(item)
    },
    {
      path: "closingBalance",
      label: "Closing balance",
      content: item => parseDiv(item, item.closingBalance + " EUR"),
      // className: item => getClassName(item)
    },
    {
      label: "Download",
      content: doc => (
        <i 
          className="fas fa-file-download" 
          style={{ cursor: "pointer", color: "green" }}
          onClick={() => downloadXML(doc.data, doc.date, "BALANCE")} 
        />
      ),
      // className: item => getClassName(item)
    }
  ]

  componentDidMount = async() => {
    this.updateWidth();
    window.addEventListener("resize", this.updateWidth);
    await this.props.getBalancesDocs(this.props.accountNumber, {});
    // this.parseDate()
  }

  updateWidth = () => {
    this.setState({
      width: window.outerWidth
    });
  }

  componentDidUpdate = async(prevProps) => {
    // let obj = {};
    // if(prevProps.date !== this.props.date) {
    //   switch(this.props.date) {
    //     case "Last day":
    //       obj = {
    //         lastDay: "true"
    //       }
    //       break;
    //     case "Last week":
    //       obj = {
    //         lastWeek: "true"
    //       }
    //       break;
    //     case "Last month":
    //       obj = {
    //         lastMonth: "true"
    //       }
    //       break;
    //     case "Last year":
    //       obj = {
    //         lastYear: "true"
    //       }
    //       break;
    //     case "All":
    //       obj = {};
    //       break;
    //     default: break;        
    //   }
    //   if(this.props.date instanceof Object)
    //     obj = this.props.date;
    //   await this.props.getStatements(this.props.accountNumber, obj);
    //   this.parseDate()
    // }
    if(this.props.accountNumber !== prevProps.accountNumber) {      
      await this.props.getBalancesDocs(this.props.accountNumber, {});
    }
  }

  // parseDate = () => {    

  //   const { balances, statements } = this.props.statements;
  //   let data = [];
  //   if(Object.keys(balances).length !== 0 && statements.length !== 0) {
  //     data = [
  //       {
  //         _flag: true,
  //         date: balances.startDate,
  //         description: "Start balance",
  //         moneyIn: "-",
  //         moneyOut: "-",
  //         balance: balances.openingBalance
  //       }
  //     ];

  //     if(statements.length > 0) {
  //       let firstEl = {
  //         date: statements[0].date,
  //         moneyOut: "-",
  //         moneyIn: "-"
  //       }
  //       const isOut = statements[0].accountiban === statements[0].debtoriban;
  //       if(isOut) {
  //         firstEl.moneyOut = statements[0].amount;
  //         firstEl.balance = parseFloat(balances.openingBalance) - parseFloat(firstEl.moneyOut);
  //       }
  //       else {
  //         firstEl.moneyIn = statements[0].amount;
  //         firstEl.balance = parseFloat(balances.openingBalance) + parseFloat(firstEl.moneyIn);
  //       }
        
  //       firstEl.description = statements[0].creditoriban === statements[0].accountiban
  //       ? statements[0].debit 
  //         ? "Debit payment"
  //         : "Incoming transaction"
  //       : "Outgoing transaction"
  //       data.push(firstEl)      
  //     }

  //     for(let i = 1; i < statements.length; i++) {
  //       let el = {
  //         date: statements[i].date,
  //         moneyOut: "-",
  //         moneyIn: "-"
  //       }
  //       const isOut = statements[i].accountiban === statements[i].debtoriban;
  //       if(isOut) {
  //         el.moneyOut = statements[i].amount;
  //         el.balance = parseFloat(data[i].balance) - parseFloat(statements[i].amount);
  //       }
  //       else {
  //         el.moneyIn = statements[i].amount;
  //         el.balance = parseFloat(data[i].balance) + parseFloat(statements[i].amount);
  //       }
  //       el.description = statements[i].creditoriban === statements[i].accountiban
  //       ? statements[i].debit 
  //         ? "Debit payment"
  //         : "Incoming transaction"
  //       : "Outgoing transaction"
  //       data.push(el)  
  //     }

  //     data.push({
  //       _flag: true,
  //       date: balances.endDate,
  //       description: "End balance",
  //       moneyIn: "-",
  //       moneyOut: "-",
  //       balance: balances.closingBalance
  //       // balance: data[data.length - 1].balance
  //     })
  //   }
  //   this.setState({ data })
  // }

  // getPaysDocs = async(item) => {
  //   await this.props.getPaysDocs({ date: item.date.slice(0,10) })
  //   this.props.paysDocs.forEach(doc => {
  //     downloadXML(doc.data, doc.date, doc.type)
  //   })
  // }


  render() {
    if (this.props.loading) return <Spinner />;
    else return (
      <div>
        <Row>
          <Col md={5}/>
        </Row>
        <Table
          columns={this.columns}
          data={this.props.balancesDocs}
          name="statements"
          disableSearch={true}
        />
        <Col md={10} lg={10} xs={10}>
          {/* <Pagination
            pagesCount={pagesCount}
            currentPage={currentPage}
            onPageChange={this.handlePageChange}
            pageSize={this.state.pageSize}
            count={this.props.count}
          /> */}
        </Col>
        <Col md={2} lg={2} xs={2} style={{ marginTop: "20px", paddingRight: "0" }}>
          {/* <Select
            multi={false}
            name="Items per page"
            placeholder="Rows per page"
            options={[ { guid: "1", name: "10" }, { guid: "2", name: "20" }, { guid: "3", name: "50" }, { guid: "4", name: "100" }, { guid: "5", name: "All" } ]}
            onSelect={this.handleSelectPageSize}
            isSearchable={false}
            styles={this.styles}
          /> */}
        </Col>
      </div>)
  }

}

const mapStateToProps = (state) => {
  return {
    loading: state.accounts.loadingBalancesDocs,
    balancesDocs: state.accounts.balancesDocs
  }
}


export default connect(mapStateToProps, {
  getBalancesDocs
})(AccountsBalances);
