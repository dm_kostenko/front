import React from "react";
import { Button, Row, Col } from "react-bootstrap";
import { connect } from "react-redux";
import AbstractComponent from "ibankFactories/Table";
import { getAllAccounts } from "ibankActions/accounts";
import { searchInAccounts, reset } from "ibankActions/search";
import { Link } from "react-router-dom";
import Modal from "ibankComponents/UI/Modal";
import AccountCreatorByCurrency from "ibankComponents/Accounts/Creator/ByCurrency";
import { getUserType } from "services/paymentBackendAPI/backendPlatform";

const isAdmin = getUserType() === "admin";

const columns = [
  {
    path: "number",
    label: "Number",
    content: account => (
      <Link className="link" style={{ fontWeight: "bold" }} to={`accounts/${account.number}`}>{account.number}</Link>)
  },
  // {
  //   path: "currencies",
  //   label: "Currencies",
  //   content: account => ( account.currencies.join(" / ") )
  // },
  {
    path: "type",
    label: "Type",
    content: account => (
      <div style={{ textTransform: "capitalize" }}>{account.type}</div>
    )
  },
  isAdmin &&
  { 
    path: "login_guid", 
    label: "User",
    content: account => {
      return <Link className="link" to={`users/${account.login_guid}`}>{account.login_guid}</Link>;
    }
  },
  // !isAdmin && {
  //   path: "balance",
  //   label: "Balance",
  //   content: account => (
  //     account.balances.map((item, i) => (
  //       <div key={i}>
  //         <Col md={6} style={{ fontWeight: "bold", textAlign: "right" }}>
  //           {item.currency}:
  //         </Col> 
  //         <Col md={6} style={{ textAlign: "left" }}>
  //           {parseInt(item.balance) - parseInt(item.reserved)}
  //         </Col>
  //       </div>
  //     ))
  //   )
  // },
  // !isAdmin && {
  //   path: "reserved",
  //   label: "Reserved",
  //   content: account => ( 
  //     account.balances.map((item, i) => (
  //       <div key={i}>
  //         <Col md={6} style={{ fontWeight: "bold", textAlign: "right" }}>
  //           {item.currency}:
  //         </Col> 
  //         <Col md={6} style={{ textAlign: "left" }}>
  //           {item.reserved}
  //         </Col>
  //       </div>
  //     ))
  //   )
  // },
  {
    path: "iban",
    label: "IBAN",
    content: account => (
      <div>{account.iban}</div>
    )
  },
  isAdmin &&
  {
    label: "Add currency",
    content: account => {
      return <Modal
        header="Add currency"
        content={<AccountCreatorByCurrency accountNumber={account.number} currency={account.currency} />}
        dialogClassName="modal-default"
        button={
          <Button
            className="submit-button add-currency"
            // style={ { position: "absolute", marginLeft: "0", top:"20px", marginBottom: "-15px" } }
          >
            Add
          </Button>
        }
      />;
    }
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.accounts.accounts,
    count: state.accounts.count,
    searchData: state.search.accountSearch,
    isSearch: state.search.isSearch,
    loading: state.accounts.loading,
    name: "accounts",
    columns
  };
};

export default connect(mapStateToProps, {
  get: getAllAccounts,
  search: searchInAccounts,
  reset
})(AbstractComponent);