import React, { Component } from "react";
import { connect } from "react-redux";
import { getAllAccounts, getStatements, getPaysDocs, getXmlStatements } from "ibankActions/accounts";
import Table from "ibankComponents/UI/Table";
import { Row, Col } from "react-bootstrap";
import { downloadXML } from "helpers/downloadXML";
import Button from "ibankComponents/UI/Button";
import { exportStatements } from "helpers/exportStatements";
import Spinner from "ibankComponents/UI/Spinner";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import moment from "moment";
import config from "config"

let date = (new Date()).setMonth((new Date()).getMonth() - 1);

class AccountsStatements extends Component {

  state = {
    data: [],
    width: 0
  }

  columns = [
    {
      path: "instrid",
      label: "Transaction ID"
    },
    {
      path: "date",
      label: "Date",
      content: item => <div style={{ fontWeight: item._flag ? "bold" : "" }}>{moment.utc(item.date).format(item._flag ? "DD.MM.YYYY" : "DD.MM.YYYY - hh:mm:ss")}</div>
    },
    {
      path: "description",
      label: "Description",
      content: item => <div style={{ fontWeight: item._flag ? "bold" : "" }}>
        {item.description.toLowerCase().includes("debit")
          ? <>
              {`${item.description} `}
              <i 
                className="fas fa-file-download" 
                style={{ cursor: "pointer", color: "green" }}
                onClick={async() => await this.getPaysDocs(item)} 
              />
            </>
          : item.description
        }
      </div>
    },
    {
      path: "paymentReason",
      label: "Reason"
    },
    {
      path: "moneyOut",
      label: "Money out",
      content: item => item.moneyOut !== "-" ? <div style={{ color: "red" }}>-{parseFloat(item.moneyOut).toFixed(2)} EUR</div>: "-"
    },  
    {
      path: "moneyIn",
      label: "Money in",
      content: item => item.moneyIn !== "-" ? <div style={{ color: "green" }}>+{parseFloat(item.moneyIn).toFixed(2)} EUR</div> : "-"
    },     
    {
      path: "balance",
      label: "Balance",
      content: item => <div style={{ fontWeight: item._flag ? "bold" : "" }}>{parseFloat(item.balance).toFixed(2)} EUR</div>
    } 
  ]

  componentDidMount = async() => {
    this.updateWidth();
    window.addEventListener("resize", this.updateWidth);
    await this.props.getStatements(this.props.accountNumber, {});
    this.parseDate()
  }

  updateWidth = () => {
    this.setState({
      width: window.outerWidth
    });
  }

  componentDidUpdate = async(prevProps) => {
    let obj = {};
    if(prevProps.date !== this.props.date) {
      switch(this.props.date) {
        case "Last day":
          obj = {
            lastDay: "true"
          }
          break;
        case "Last week":
          obj = {
            lastWeek: "true"
          }
          break;
        case "Last month":
          obj = {
            lastMonth: "true"
          }
          break;
        case "Last year":
          obj = {
            lastYear: "true"
          }
          break;
        case "All":
          obj = {};
          break;
        default: break;        
      }
      if(this.props.date instanceof Object)
        obj = this.props.date;
      await this.props.getStatements(this.props.accountNumber, obj);
      this.parseDate()
    }
    else if(this.props.accountNumber !== prevProps.accountNumber) {      
      await this.props.getStatements(this.props.accountNumber, obj);
      this.parseDate()
    }
  }

  parseDate = () => {    

    let { balances, statements } = this.props.statements;
    let data = [];

    statements = statements.map(i => (
      {
        ...i,
        _date: (i.transactiondatetime || i.date).slice(0, 19)
      }
    )).sort((a, b) => {
      if(a._date == b._date) {
        let left = (a.pmtinfid !== 'none' && a.pmtinfid) || a.transactionid || a.instrid || a.msgid
        let right = (b.pmtinfid !== 'none' && b.pmtinfid) || b.transactionid || b.instrid || b.msgid
        return left < right ? -1 : 1
      }
      else
        return (new Date(a._date) - new Date(b._date))
    })

    if(Object.keys(balances).length !== 0 && statements.length !== 0) {
      data = [
        {
          _flag: true,
          date: balances.startDate,
          description: "Start balance",
          paymentReason: "-",
          moneyIn: "-",
          moneyOut: "-",
          balance: balances.openingBalance,
          instrid: "-"
        }
      ];

      if(statements.length > 0) {
        let firstEl = {
          date: statements[0].transactiondatetime || statements[0].date,
          moneyOut: "-",
          moneyIn: "-",
          instrid: (statements[0].pmtinfid !== 'none' && statements[0].pmtinfid) || statements[0].transactionid || statements[0].instrid || statements[0].msgid
        }
        const isOut = statements[0].creditdebitindex !== "CRDT";
        if(isOut) {
          firstEl.moneyOut = statements[0].amount;
          firstEl.balance = parseFloat(balances.openingBalance) - parseFloat(firstEl.moneyOut);
        }
        else {
          firstEl.moneyIn = statements[0].amount;
          firstEl.balance = parseFloat(balances.openingBalance) + parseFloat(firstEl.moneyIn);
        }

        firstEl.paymentReason = statements[0].remittanceinformationpaymentreason;
        
        firstEl.description = statements[0].debit 
          ? "Debit payment"
          : statements[0].creditdebitindex === "CRDT"
            ? "Incoming"
            : "Outgoing"
        data.push(firstEl)      
      }

      for(let i = 1; i < statements.length; i++) {
        let el = {
          date: statements[i].transactiondatetime || statements[i].date,
          moneyOut: "-",
          moneyIn: "-",
          instrid: (statements[i].pmtinfid !== 'none' && statements[i].pmtinfid) || statements[i].transactionid || statements[i].instrid || statements[i].msgid
        }
        const isOut = statements[i].creditdebitindex !== "CRDT";
        if(isOut) {
          el.moneyOut = statements[i].amount;
          el.balance = parseFloat(data[i].balance) - parseFloat(statements[i].amount);
        }
        else {
          el.moneyIn = statements[i].amount;
          el.balance = parseFloat(data[i].balance) + parseFloat(statements[i].amount);
        }

        el.paymentReason = statements[i].remittanceinformationpaymentreason;

        el.description = statements[i].debit
          ? "Debit payment"
          : statements[i].creditdebitindex === "CRDT"
            ? "Incoming"
            : "Outgoing"
        data.push(el)  
      }

      data.push({
        _flag: true,
        date: balances.endDate,
        //date: data[data.length - 1].date,
        description: "End balance",
        moneyIn: "-",
        moneyOut: "-",
        paymentReason: "-",
        // balance: balances.closingBalance
        balance: data[data.length - 1].balance,
        instrid: "-"
      })
    }
    this.setState({ data })
  }

  getPaysDocs = async(item) => {
    await this.props.getPaysDocs({ date: item.date.slice(0,10) })
    this.props.paysDocs.forEach(doc => {
      downloadXML(doc.data, doc.date, doc.type)
    })
  }

  firstDayInPreviousMonth = (yourDate = new Date()) => {
      return new Date(yourDate.getFullYear(), yourDate.getMonth() - 1, 1);
  }

  lastDayInPreviousMonth = (yourDate = new Date()) => {
      return new Date(yourDate.getFullYear(), yourDate.getMonth(), 0);
  }

  handleButtonExportPDFClick = async() => {
    //await this.props.getAllAccounts({iban: this.props.accountNumber});
    exportStatements(
      "pdf",
      this.props.accountNumber, //account iban
      undefined, // account number
      undefined, //currency
      undefined, // account type
      this.state.data[0] ? this.state.data[0].date : this.firstDayInPreviousMonth(),
      this.state.data[this.state.data.length - 1] ? this.state.data[this.state.data.length - 1].date : this.lastDayInPreviousMonth(),
      this.state.data[0] ? this.state.data[0].balance : 0,
      this.state.data[this.state.data.length - 1] ? this.state.data[this.state.data.length - 1].balance : 0,
      this.state.data.slice(1, this.state.data.length - 1)
    );
  };
  handleButtonExportCSVClick = async() => {
    //await this.props.getAllAccounts({iban: this.props.accountNumber});
    exportStatements(
      "csv",
      this.props.accountNumber, //account iban
      undefined, // account number
      undefined, //currency
      undefined, // account type
      this.state.data[0] ? this.state.data[0].date : this.firstDayInPreviousMonth(),
      this.state.data[this.state.data.length - 1] ? this.state.data[this.state.data.length - 1].date : this.lastDayInPreviousMonth(),
      this.state.data[0] ? this.state.data[0].balance : 0,
      this.state.data[this.state.data.length - 1] ? this.state.data[this.state.data.length - 1].balance : 0,
      this.state.data.slice(1, this.state.data.length - 1)
    );
  };

  handleButtonExportXMLClick = async() => {
    let obj = {};
    switch(this.props.date) {
      case "Last day":
        obj = {
          lastDay: "true"
        }
        break;
      case "Last week":
        obj = {
          lastWeek: "true"
        }
        break;
      case "Last month":
        obj = {
          lastMonth: "true"
        }
        break;
      case "Last year":
        obj = {
          lastYear: "true"
        }
        break;
      case "All":
        obj = {};
        break;
      default: break;        
    }
    if(this.props.date instanceof Object)
      obj = this.props.date;
    await this.props.getXmlStatements(this.props.accountNumber, obj);
    if(this.props.xmlStatement)
      downloadXML(this.props.xmlStatement, this.props.accountNumber, "STATEMENT")
  }


  render() {
    if (this.props.loading) return <Spinner />;
    else return (
      <div>
        <Row>
          <Col md={5}/>
        </Row>
        <Table
          columns={this.columns}
          data={this.state.data}
          name="statements"
          disableSearch={true}
        />
        <Col md={10} lg={10} xs={10}>
          {/* <Pagination
            pagesCount={pagesCount}
            currentPage={currentPage}
            onPageChange={this.handlePageChange}
            pageSize={this.state.pageSize}
            count={this.props.count}
          /> */}
        </Col>
        <Col md={2} lg={2} xs={2} style={{ marginTop: "20px", paddingRight: "0" }}>
          {/* <Select
            multi={false}
            name="Items per page"
            placeholder="Rows per page"
            options={[ { guid: "1", name: "10" }, { guid: "2", name: "20" }, { guid: "3", name: "50" }, { guid: "4", name: "100" }, { guid: "5", name: "All" } ]}
            onSelect={this.handleSelectPageSize}
            isSearchable={false}
            styles={this.styles}
          /> */}
        </Col>
        <Button
          className="btn"
          onClick={this.handleButtonExportPDFClick}
        >
          Export as PDF
        </Button>
        <Button
          className="btn"
          style={{
            marginLeft: "20px"
          }}
          onClick={this.handleButtonExportCSVClick}
        >
          Export as CSV
        </Button>
        <Button
          className="btn"
          style={{
            marginLeft: "20px"
          }}
          onClick={this.handleButtonExportXMLClick}
        >
          Export as XML
        </Button>
      </div>);
  }

}

const mapStateToProps = (state) => {
  return {
    account: state.accounts,
    statements: state.accounts.statements,
    loading: state.accounts.loadingStatements,
    paysDocs: state.accounts.paysDocs,
    xmlStatement: state.accounts.xmlStatement
  }
}


export default connect(mapStateToProps, {
  getAllAccounts,
  getStatements,
  getPaysDocs,
  getXmlStatements
})(AccountsStatements);
