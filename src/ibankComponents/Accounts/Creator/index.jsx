import React, { Component } from "react";
import { Button, Col, Form, Row, ControlLabel } from "react-bootstrap";
import Card from "ibankComponents/UI/Card";
import Input from "ibankComponents/UI/Input";
import { createNewAccount } from "ibankActions/accounts";
import { getAllUsers } from "ibankActions/users";
import { getAllCurrencies } from "ibankActions/currencies";
import { showModal } from "ibankActions/modal";
import Select from "ibankComponents/UI/MultiSelect";
import { connect } from "react-redux";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";
import validate from "helpers/validation";
import Joi from "@hapi/joi";
import { isTemplateElement } from "@babel/types";

class AccountCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login_guid: "",
      currency: "",
      monthly_limit: "",
      daily_limit: "",
      type: ""
    };
  }

  schema = {
    login_guid: Joi.string().required().label("User"),
    type: Joi.string().required().label("Type"),
    currency: Joi.string().required().label("Currency"),
    daily_limit: Joi.number().min(0).max(1000000).label("Daily limit"),
    monthly_limit: Joi.number().min(0).max(1000000).label("Monthly limit")
  };


  componentDidMount = async() => {
    await this.props.getAllUsers();
    await this.props.getAllCurrencies();
  }

  getUsersOptions = () => {
    return this.props.users.filter(user => user.type !== "admin").map(user => {
      return {
        guid: user.guid,
        name: user.username
      };
    });
  }

  getTypesOptions = () => {
    return [
      {
        guid: "1",
        name: "card"
      },
      {
        guid: "2",
        name: "settlement"
      }
    ]
  }

  handleChange = (e) => {
    const property = e.target.name;
    let value = e.target.value;
    const values = [ "daily_limit", "monthly_limit" ];
    if (values.includes(property)) {
      value = value.replace(".", "");
      let start = value.substr(0, value.length - 2);
      let middle = ".";
      let finish = value.substr(value.length - 2, value.length);
      value = start + middle + finish;
      switch (value.length) {
      case 2: {
        value = value[value.length - 1] + ".00";
        break;
      }
      case 3: {
        value = "0" + value;
        break;
      }
      default: {
        if (value[0] === "0") {
          value = value.substr(1, value.length - 1);
        }
        break;
      }
      }
    }
    this.setState({
      [property]: value
    });
  };

  handleSelectUser = (option) => {
    this.setState({
      login_guid: option.guid
    });
  }

  handleSelectCurrency = (option) => {
    this.setState({
      currency: option.name
    });
  }

  handleSelectType = (option) => {
    this.setState({
      type: option.name
    });
  }

  handleLimitOpen = () => {
    this.setState({
      isLimits: !this.state.isLimits
    });
  }

  formValidation = (data) => {
    return data !== "";
  };

  generalValidation = () => {
    return this.formValidation(this.state.login_guid) &&
      this.formValidation(this.state.currency)
  };

  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    if (!this.generalValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    }
    else {
      const { login_guid, currency, monthly_limit, daily_limit, type } = this.state;
      try {
        let request = {
          login_guid,
          currency, 
          type
        };
        if(monthly_limit !== "" && monthly_limit !== "0.00")
          request = {
            ...request,
            monthly_limit
          };
        if(daily_limit !== "" && daily_limit !== "0.00")
          request = {
            ...request,
            daily_limit
          };        
        validate(request, this.schema);
        await this.props.createNewAccount(this.props.currentPage, this.props.pageSize, request);
        swal({
          title: "Account is created",
          icon: "success",
          button:false,
          timer: 2000
        });
        await this.props.showModal(false, "accounts");
      }
      catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        if((error.response && error.response.status !== 409) || !error.response)        
          swal({
            title: parsedError.error,
            text: parsedError.message,
            icon: "error",
          });
      }
    }
  };

  style={
    default: {
      marginBottom: "30px"
    }
  }


  render() {
    const selectorStyles = {
      placeholder: (styles) => {
        return {
          ...styles,
          color: "black"
        };
      },
      control: (styles, state) => {
        return {
          ...styles,
          background: "rgba(255,255,255,0.4) !important",
          marginBottom: "30px",
          width: "100%",
          height: "40px !important",
          transition: ".8s all",
          "&:hover": {
            background: state.menuIsOpen ? "white !important" : "rgba(255,255,255,0.4) !important",
            transition: ".8s all"
          }
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          width: "100%",
          marginLeft: "auto",
          marginRight: "auto"
        };
      }
    };
    const usersOptions = this.getUsersOptions();
    const currenciesOptions = this.props.currencies.map(currency => {
      return { guid: currency.code, name: currency.name };
    });
    const typesOptions = this.getTypesOptions();
    return (
      <Form autoComplete="off">
        <Row>
          <Col style={{ display: "flex" }}>
            <Card
              header="info"
              style={{
                // // background: "#80dbff"
              }}
            >
              <Row>
                <Col className="col" md={6}>
                  <ControlLabel 
                    className="col-label"
                  >
                    User *
                  </ControlLabel>
                  <Select
                    isMulti={false}
                    options={usersOptions}
                    onSelect={this.handleSelectUser}
                    placeholder="Select user"
                    styles={selectorStyles}
                  />
                </Col>
                <Col className="col" md={6}>
                  <ControlLabel 
                    className="col-label"
                  >
                    Type *
                  </ControlLabel>
                  <Select
                    isMulti={false}
                    options={typesOptions}
                    onSelect={this.handleSelectType}
                    placeholder="Select type"
                    styles={selectorStyles}
                  />
                </Col>
              </Row>
              <Row>
                <Col className="col" md={3}/>                  
                <Col className="col" md={6}>
                  <ControlLabel 
                    className="col-label"
                  >
                    Currency *
                  </ControlLabel>
                  <Select
                    isMulti={false}
                    options={currenciesOptions}
                    onSelect={this.handleSelectCurrency}
                    placeholder="Select currency"
                    styles={selectorStyles}
                  />
                </Col>
                <Col className="col" md={3}/>                  
              </Row>
              <div align="center">
                <Col md={12}>
                  {!this.state.isLimits &&
                  <Button
                    className="submit-button next"
                    onClick={this.handleLimitOpen}
                  >
                    Set account limits
                  </Button>}
                </Col>
              </div>
              {this.state.isLimits &&
              <Row>
                <Col md={6}>
                  <Input
                    label="Daily limit"
                    name="daily_limit"
                    type="number"
                    value={this.state.daily_limit}
                    onChange={this.handleChange}
                  />
                </Col>
                <Col md={6}>
                  <Input
                    label="Monthly limit"
                    name="monthly_limit"
                    type="number"
                    value={this.state.monthly_limit}
                    onChange={this.handleChange}
                  />
                </Col>
              </Row>}
            </Card>            
          </Col>          
          <div align="center">
            {this.state.isLoading 
              ? <ReactLoading type='cylon' color='grey' /> 
              : <Button 
                className="submit-button"
                style={{
                  marginTop: "40px"
                }}
                onClick={this.handleSubmit}
              >
                Create
              </Button>}
          </div>
        </Row>
      </Form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    accountsSearch: state.search.accountsSearch,
    users: state.users.users,
    currencies: state.currencies.currencies,
    currentPage: state.accounts.currentPage,
    pageSize: state.accounts.pageSize
  };
};

export default connect(mapStateToProps, {
  createNewAccount,
  getAllUsers,
  getAllCurrencies,
  showModal,
})(AccountCreator);

AccountCreator.propTypes = {
  users: PropTypes.array,
  getAllCurrencies: PropTypes.func,
  currencies: PropTypes.array,
  showModal: PropTypes.func,
  createNewAccount: PropTypes.func,
  accountsSearch: PropTypes.object,
  getAllUsers: PropTypes.func
};