import React, { Component } from "react";
import { Button, Col, Form, Row, ControlLabel } from "react-bootstrap";
import Card from "ibankComponents/UI/Card";
import { addAccountCurrency, getAccountDetail } from "ibankActions/accounts";
import { getAllCurrencies } from "ibankActions/currencies";
import { showModal } from "ibankActions/modal";
import Input from "ibankComponents/UI/Input";
import Select from "ibankComponents/UI/MultiSelect";
import { connect } from "react-redux";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";
import validate from "helpers/validation";
import Joi from "@hapi/joi";


class AccountCreatorByCurrency extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currency: "",
      currencies: [],
      monthly_limit: "",
      daily_limit: ""
    };
  }

  schema = {
    currency: Joi.string().required().label("Currency"),
    daily_limit: Joi.number().min(0).max(1000000).label("Daily limit"),
    monthly_limit: Joi.number().min(0).max(1000000).label("Monthly limit")
  };


  componentDidMount = async() => {
    await this.props.getAccountDetail(this.props.accountNumber);
    await this.props.getAllCurrencies();
    const currencies = this.getCurrenciesOptions();
    this.setState({ currencies });
  }

  getCurrenciesOptions = () => {
    if(this.props.account) {
      let currencies = this.props.currencies.map(currency => {
        return { guid: currency.code, name: currency.name };
      });
      this.props.account.balances.forEach(item => {
        currencies = currencies.filter(cur => cur.name !== item.currency);
      });
      return currencies;
    }
    return [];
  }

  handleChange = (e) => {
    const property = e.target.name;
    let value = e.target.value;
    const values = [ "daily_limit", "monthly_limit" ];
    if (values.includes(property)) {
      value = value.replace(".", "");
      let start = value.substr(0, value.length - 2);
      let middle = ".";
      let finish = value.substr(value.length - 2, value.length);
      value = start + middle + finish;
      switch (value.length) {
      case 2: {
        value = value[value.length - 1] + ".00";
        break;
      }
      case 3: {
        value = "0" + value;
        break;
      }
      default: {
        if (value[0] === "0") {
          value = value.substr(1, value.length - 1);
        }
        break;
      }
      }
    }
    this.setState({
      [property]: value
    });
  };

  handleSelectCurrency = (option) => {
    this.setState({
      currency: option.name
    });
  }

  formValidation = (data) => {
    return data !== "";
  };

  generalValidation = () => {
    return this.formValidation(this.state.currency)
  };

  handleLimitOpen = () => {
    this.setState({
      isLimits: !this.state.isLimits
    });
  }

  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    if (!this.generalValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    }
    else {
      const { currency, monthly_limit, daily_limit } = this.state;
      try {      
        let request = { currency };
        if(this.formValidation(monthly_limit) && monthly_limit !== "0.00")
          request = {
            ...request,
            monthly_limit
          };
        if(this.formValidation(daily_limit) && daily_limit !== "0.00")
          request = {
            ...request,
            daily_limit
          };
        validate(request, this.schema);
        await this.props.addAccountCurrency(this.props.accountNumber, request);
        swal({
          title: "Currency is added",
          icon: "success",
          button:false,
          timer: 2000
        });
        await this.props.showModal(false, "accounts");
      }
      catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        if((error.response && error.response.status !== 409) || !error.response)        
          swal({
            title: parsedError.error,
            text: parsedError.message,
            icon: "error",
          });
      }
    }
  };

  style={
    default: {
      marginBottom: "30px"
    }
  }


  render() {
    const selectorStyles = {
      placeholder: (styles) => {
        return {
          ...styles,
          color: "black"
        };
      },
      control: (styles, state) => {
        return {
          ...styles,
          background: "rgba(255,255,255,0.4) !important",
          marginBottom: "30px",
          width: "100%",
          height: "40px !important",
          transition: ".8s all",
          "&:hover": {
            background: state.menuIsOpen ? "white !important" : "rgba(255,255,255,0.4) !important",
            transition: ".8s all"
          }
        };
      },
      singleValue: (styles) => {
        return {
          ...styles,
          width: "100%",
          marginLeft: "auto",
          marginRight: "auto"
        };
      }
    };
    if(this.state.currencies.length === 0)
      return (
        <Form autoComplete="off">
          <Row>
            <Col style={{ display: "flex" }}>
              <Card
                header="Add currency"
                style={{
                  // // background: "#80dbff"
                }}
              >
                <Row>
                  <Col md={3}/>
                  <Col className="col" md={6}>
                    <ControlLabel 
                      className="col-label"
                    >
                      There are no free currencies
                    </ControlLabel>
                  </Col>
                  <Col md={3}/>
                </Row>
              </Card>            
            </Col>
          </Row>
        </Form>
      );
    return (
      <Form autoComplete="off">
        <Row>
          <Col style={{ display: "flex" }}>
            <Card
              header="Add currency"
              style={{
                // // background: "#80dbff"
              }}
            >
              <Row>
                <Col md={3}/>
                <Col className="col" md={6}>
                  <ControlLabel 
                    className="col-label"
                  >
                    Currency
                  </ControlLabel>
                  <Select
                    isMulti={false}
                    options={this.state.currencies}
                    onSelect={this.handleSelectCurrency}
                    placeholder="Select currency"
                    styles={selectorStyles}
                  />
                </Col>
                <Col md={3}/>
              </Row>
              <div align="center">
                <Col md={12}>
                  {!this.state.isLimits &&
                  <Button
                    className="submit-button next"
                    onClick={this.handleLimitOpen}
                  >
                    Set account limits
                  </Button>}
                </Col>
              </div>
              {this.state.isLimits &&
              <Row>
                <Col md={6}>
                  <Input
                    label="Daily limit"
                    name="daily_limit"
                    type="number"
                    value={this.state.daily_limit}
                    onChange={this.handleChange}
                  />
                </Col>
                <Col md={6}>
                  <Input
                    label="Monthly limit"
                    name="monthly_limit"
                    type="number"
                    value={this.state.monthly_limit}
                    onChange={this.handleChange}
                  />
                </Col>
              </Row>}
            </Card>            
          </Col>          
          <div align="center">
            {this.state.isLoading 
              ? <ReactLoading type='cylon' color='grey' /> 
              : <Button 
                className="submit-button"
                style={{
                  marginTop: "40px"
                }}
                onClick={this.handleSubmit}
              >
                Add
              </Button>}
          </div>
        </Row>
      </Form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    account: state.accounts.account,
    currencies: state.currencies.currencies
  };
};

export default connect(mapStateToProps, {
  addAccountCurrency,
  getAccountDetail,
  getAllCurrencies,
  showModal
})(AccountCreatorByCurrency);

AccountCreatorByCurrency.propTypes = {
  getAccountDetail: PropTypes.func,
  getAllCurrencies: PropTypes.func,
  showModal: PropTypes.func,
  currencies: PropTypes.string,
  addAccountCurrency: PropTypes.func
};