import React, { Component } from "react";
import { Col, Row, Button } from "react-bootstrap";
import Content from "ibankViews/Content";
import { getAccountDetail } from "ibankActions/accounts";
import { connect } from "react-redux";
import Card from "ibankComponents/UI/Card";
import DetailInfoLabel from "ibankComponents/UI/DetailInfoLabel";
import PropTypes from "prop-types";
import Spinner from "ibankComponents/UI/Spinner";
import tagsDictionary from "helpers/fieldsDictionary";
import { getUserType } from "services/paymentBackendAPI/backendPlatform";

class AccountDetail extends Component {

  state = {      
    width: 0,
    data: {}
  }

  async componentDidMount() {    
    this.updateWindowStyles();
    window.addEventListener("resize", this.updateWindowStyles);
    await this.props.getAccountDetail(this.props.match.params.id);   
  }

  updateWindowStyles = () => {
    this.setState({
      width: window.outerWidth
    });
  }

  render() {
    let unusedKeys = [ "count", "balances", "number" ];
    if(getUserType() !== "admin")
      unusedKeys.push("login_guid");
    const style = this.state.width > 1000 ? { display: "flex" } : {};
    if(this.props.loading) 
      return <Content>
        <Spinner/>
      </Content>;
    return <Content>
      <Row>
        <Col style={style}>
          <Card
            style={{
              color: "black",
              // width: "35%",
              // marginLeft: "calc(50% - 500px)",
              height: "auto",
              marginTop: "20px",
              marginRight: "10px",
              textAlign: "center"
            }}
            detail={true}
            header={`Account № ${this.props.data.number}`}
          >
            {Object.keys(this.props.data).map(key => {
              const label = tagsDictionary[key.toUpperCase()] 
                ? tagsDictionary[key.toUpperCase()].beautify 
                : key;
              const value = tagsDictionary[key.toUpperCase()] && tagsDictionary[key.toUpperCase()].modify 
                ? tagsDictionary[key.toUpperCase()].modify(this.props.data[key])
                : this.props.data[key];
              return !unusedKeys.includes(key) &&
          <DetailInfoLabel
            key={key}
            label={`${label}:`}
            value={value || "-"}
          />;
            })}
          </Card>
          <Card
            style={{
              color: "black",
              // width: "35%",
              // marginLeft: "calc(50% - 500px)",
              height: "auto",
              marginTop: "20px",
              marginRight: "10px",
              textAlign: "center"
            }}
            detail={true}
            header="Currencies"
          >
            {this.props.data.balances
              ? this.props.data.balances.map((item, i) => (
                <DetailInfoLabel
                  key={i}
                  cols={2}
                  label={[ item.currency, "balance", "reserved" ]}
                  value={[ (item.balance - item.reserved).toFixed(2), item.reserved.toFixed(2) ]}
                />
              ))
              : null}
          </Card>
        </Col>
      </Row>
      
    </Content>;
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.accounts.account,
    loading: state.accounts.loadingDetail
  };
};

export default connect(mapStateToProps, {
  getAccountDetail
})(AccountDetail);

AccountDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  loading: PropTypes.bool,
  getAccountDetail: PropTypes.func,
  data: PropTypes.object
};