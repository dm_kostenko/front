import React from "react";
import Title from "./Title";
import MessagesList from "./MessagesList";
import SendMessageForm from "./SendMessageForm";
import { ChatManager, TokenProvider } from '@pusher/chatkit-client';
import { getUserProfile } from "ibankActions/users";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Chatkit from "@pusher/chatkit-server";

class Support extends React.Component {

  state = {
    messages: []
  }

  componentDidMount = async () => {
    await this.props.getUserProfile();   
    const userId = this.props.profile.username;
    const chatManager = await new ChatManager({
      instanceLocator: "v1:us1:d8006921-4bf7-4fea-a853-9f69781973b7",
      userId,
      tokenProvider: new TokenProvider({
        url: "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/d8006921-4bf7-4fea-a853-9f69781973b7/token"
      })
    });
    await chatManager
      .connect()
      .then(currentUser => {
        this.setState({ currentUser: currentUser })
        return currentUser.subscribeToRoom({
          roomId: "7c660edb-b745-41fa-96ad-de2877782202",
          messageLimit: 100,
          hooks: {
            onMessage: message => {
              this.setState({
                messages: [...this.state.messages, message],
              });
            },
          }
        })
      })
      .then(currentRoom => {
        this.setState({
          currentRoom,
          users: currentRoom.userIds
        })
      })
      .catch(error => console.log(error))
  }

  // sendMessage = (text) => {
  //   this.setState({
  //     messages: [
  //       ...this.state.messages,
  //       {
  //         senderId: "me",
  //         text
  //       }
  //     ]
  //   })
  // }

  sendMessage = async (text) => {
    await this.state.currentUser.sendMessage({
      text,
      roomId: this.state.currentRoom.id
    }).catch(error => console.error('error', error));
  }

  onDelete = async (e) => {
    const messageId = e.target.id;
    const chatkit = new Chatkit({
      instanceLocator: "v1:us1:d8006921-4bf7-4fea-a853-9f69781973b7",
      key: "952ef450-4849-42eb-a680-2c55e1bbd596:uDs3x1NcL4S8WrWKwQxiW46KPgcHjzxcEf8CAlgkSJg=",
    });
    await chatkit.deleteMessage({
      roomId: this.state.currentRoom.id,
      messageId
    }).then(() => {
      const messages = this.state.messages.filter(message => message.id != messageId);
      this.setState({
        messages
      });
    })
      .catch(error => console.log(error));
  }

  render() {
    return (
      <div className="app">
        <Title />
        <MessagesList messages={this.state.messages} onDelete={this.onDelete}/>
        <SendMessageForm sendMessage={this.sendMessage} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profile: state.users.profile,
    loading: state.users.loadingProfile
  };
};

export default connect(mapStateToProps, {
  getUserProfile
})(Support);

Support.propTypes = {
  getUserProfile: PropTypes.func
};