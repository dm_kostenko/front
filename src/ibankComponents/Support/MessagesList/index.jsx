import React from "react";
import {
  Col,
  ControlLabel,
  FormControl,
  FormGroup,
  Alert
} from "react-bootstrap";
import { getUserProfile } from "ibankActions/users";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import moment from "moment";

class MessagesList extends React.Component {

  async componentDidMount() {
    await this.props.getUserProfile();
  }

  render() {
    return (
      <ul className="message-list">
        {
          this.props.messages.map((message, index) => {
            const className = this.props.profile.username === message.senderId ? "my-message" : "message";
            return <li key={index} className={className}>
              <div>{`${message.senderId}, ${moment(message.createdAt).utcOffset(7).format("MMMM Do, h:mm a")}`}</div>
              <div style={{ margin: "20px", width: "10px" }}>{message.text}</div>
              {this.props.profile.username === message.senderId && <i
                id={message.id} o
                onClick={this.props.onDelete}
                className="far fa-trash-alt"
                style={{ cursor: "pointer", color: "red" }}
              />}
            </li>;
          })
        }
      </ul>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profile: state.users.profile,
    loading: state.users.loadingProfile
  };
};

export default connect(mapStateToProps, {
  getUserProfile
})(MessagesList);

MessagesList.propTypes = {
  getUserProfile: PropTypes.func,
  messages: PropTypes.array
};
