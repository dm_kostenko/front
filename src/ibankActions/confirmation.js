import * as types from "ibankConstants/actionTypes";
import axios from "axios";

export const saveConfig = (config) => {
  return { type: types.SAVE_AXIOS_CONFIG, payload: config };
};

export const addOtpToConfig = (otp) => ({ type: types.ADD_OTP_TO_CONFIG, payload: otp });

export const addOtpAndSend = (otp) => async(dispatch, getState)  => {
  try {
    dispatch(addOtpToConfig(otp));
    const { config } = getState().confirmation;
    const response = await axios.request(config);
    dispatch({ 
      type: types.CONFIRMATION_SUCCEED,
      payload: true
    });
    const { dispatchArray } = getState().confirmation;
    dispatchArray.forEach(item => {
      switch(item.type) {
      case "action":
        dispatch({
          type: item.data,
          payload: response.data
        });
        break;
      case "function":
        dispatch(item.data);
        break;
      case "text":
        dispatch({
          type: types.SAVE_SWAL_TEXT,
          payload: item.data
        });
        break;
      default:
        break;
      }
    });
    dispatch({
      type: types.CLEAR_DISPATCH_ARRAY
    });
  }
  catch(error) {
    throw(error);
  }
};

export const reset = () => ({ type: types.RESET });