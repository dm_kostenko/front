import * as types from "ibankConstants/actionTypes";

export const pushLocation = (location) => {
  return dispatch => {
    dispatch({
      type: types.PUSH_LOCATION,
      payload: location
    });
  };
};

export const popLocation = () => {
  return dispatch => {
    dispatch({
      type: types.POP_LOCATION
    });
  };
};