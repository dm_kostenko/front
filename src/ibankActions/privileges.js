import * as types from "ibankConstants/actionTypes";
import {
  getPrivileges
} from "services/paymentBackendAPI/ibankCommon/privileges";


export const getAllPrivileges = ({ page, items, search, ...rest }) => async(dispatch) => {
  dispatch({
    type: types.GET_PRIVILEGES
  });
  try {
    const response = await getPrivileges({ page, items, search, ...rest });
    dispatch({
      type: types.GET_PRIVILEGES_SUCCEED,
      payload: response.data,
      pageSize: items, 
      currentPage: page
    });
  } catch (error) {
    dispatch({
      type: types.GET_PRIVILEGES_FAILED
    });
  }
};