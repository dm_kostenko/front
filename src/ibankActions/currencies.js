import * as types from "ibankConstants/actionTypes";
import {
  getCurrencies,
  getCurrenciesRates
} from "services/paymentBackendAPI/ibankCommon/currencies";


export const getAllCurrencies = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_CURRENCIES
  });
  try {
    const response = await getCurrencies(args);
    dispatch({
      type: types.GET_CURRENCIES_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_CURRENCIES_FAILED
    });
  }
};

export const getAllCurrenciesRates  = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_CURRENCIES_RATES
  });
  try {
    const response = await getCurrenciesRates(args);
    dispatch({
      type: types.GET_CURRENCIES_RATES_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_CURRENCIES_RATES_FAILED
    });
  }
};