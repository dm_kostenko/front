import * as types from "ibankConstants/actionTypes";
import {
  getAccounts,
  getAccount,
  createAccount,
  addCurrencyToAccount,
  getAccountsStatements,
  getAccountsStatementsInXml,
  getAccountsPaysDocs,
  getAccountsBalances
} from "services/paymentBackendAPI/ibankCommon/accounts";


export const getAllAccounts = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_ACCOUNTS
  });
  try {
    const response = await getAccounts(args);
    dispatch({
      type: types.GET_ACCOUNTS_SUCCEED,
      payload: response.data,
      pageSize: args && args.items, 
      currentPage: args && args.page
    });
  } catch (error) {
    dispatch({
      type: types.GET_ACCOUNTS_FAILED
    });
  }
};

export const getTxAccounts = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_TX_ACCOUNTS
  });
  try {
    const response = await getAccounts(args);
    dispatch({
      type: types.GET_TX_ACCOUNTS_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_TX_ACCOUNTS_FAILED
    });
  }
};

export const getStatements = (accountNumber, args) => async(dispatch) => {
  dispatch({
    type: types.GET_ACCOUNTS_STATEMENTS
  });
  try {
    const response = await getAccountsStatements(accountNumber, args);
    dispatch({
      type: types.GET_ACCOUNTS_STATEMENTS_SUCCEED,
      payload: response.data,
      pageSize: args && args.items, 
      currentPage: args && args.page
    });
  } catch (error) {
    dispatch({
      type: types.GET_ACCOUNTS_STATEMENTS_FAILED
    });
  }
};

export const getXmlStatements = (accountNumber, args) => async(dispatch) => {
  dispatch({
    type: types.GET_ACCOUNTS_STATEMENTS_IN_XML
  });
  try {
    const response = await getAccountsStatementsInXml(accountNumber, args);
    dispatch({
      type: types.GET_ACCOUNTS_STATEMENTS_IN_XML_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_ACCOUNTS_STATEMENTS_IN_XML_FAILED
    });
  }
};

export const getPaysDocs = args => async(dispatch) => {
  dispatch({
    type: types.GET_ACCOUNTS_PAYS_DOCS
  });
  try {
    const response = await getAccountsPaysDocs(args);
    dispatch({
      type: types.GET_ACCOUNTS_PAYS_DOCS_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_ACCOUNTS_PAYS_DOCS_FAILED
    });
  }
};

export const getBalancesDocs = (number, args) => async(dispatch) => {
  dispatch({
    type: types.GET_ACCOUNTS_BALANCES_DOCS
  });
  try {
    const response = await getAccountsBalances(number, args);
    dispatch({
      type: types.GET_ACCOUNTS_BALANCES_DOCS_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_ACCOUNTS_BALANCES_DOCS_FAILED
    });
  }
};

export const getAccountDetail = (guid) => async(dispatch) => {
  dispatch({
    type: types.GET_ACCOUNT
  });
  try {
    const response = await getAccount(guid);
    dispatch({
      type: types.GET_ACCOUNT_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_ACCOUNT_FAILED
    });
  }
};

export const createNewAccount = (page, items, data) => async(dispatch) => {
  try {
    const response = await createAccount(data);
    dispatch({
      type: types.CREATE_ACCOUNT,
      payload: response.data
    });
    dispatch(getAllAccounts({ page, items }));
  } catch (error) {
    throw(error);
  }
};


export const addAccountCurrency = (number, data) => async(dispatch) => {
  try {
    const response = await addCurrencyToAccount(number, data);
    dispatch({
      type: types.ADD_CURRENCY_TO_ACCOUNT,
      payload: response.data
    });
    dispatch(getAllAccounts({ page: 1, items: 10 }));       // add page, items
  } catch (error) {
    throw(error);
  }
};