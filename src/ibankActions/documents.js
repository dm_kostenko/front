import * as types from "ibankConstants/actionTypes";
import {
  getInbox,
  getInboxDetail,
  editInboxStatus
} from "services/paymentBackendAPI/ibankCommon/documents";


export const getInboxDocs = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_INBOX
  });
  try {
    const response = await getInbox(args);
    dispatch({
      type: types.GET_INBOX_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_INBOX_FAILED
    });
  }
};

export const getInboxDoc = (guid) => async(dispatch) => {
  dispatch({
    type: types.GET_INBOX_DETAIL
  });
  try {
    const response = await getInboxDetail(guid);
    dispatch({
      type: types.GET_INBOX_DETAIL_SUCCEED,
      payload: response
    });
  } catch (error) {
    dispatch({
      type: types.GET_INBOX_DETAIL_FAILED
    });
  }
};

export const editInboxStatusAction = (data, guid, directionData, page, items) => async(dispatch) => {
  try {
    await editInboxStatus(data, guid);
    dispatch(getInboxDocs({
      page, 
      items, 
      ...directionData
    }));
  } catch (error) {
    throw(error);
  }
};

// export const getTxsDocs = (page, items, search, type) => {
//   return async dispatch => {
//     dispatch({
//       type: types[`GET_${type.toUpperCase()}_DOCS`]
//     });
//     try {
//       const response = await getTransactionsDocs(page, items, search, type.toLowerCase());
//       dispatch({
//         type: types[`GET_${type.toUpperCase()}_DOCS_SUCCEED`],
//         payload: response.data
//       });
//     } catch (error) {
//       dispatch({
//         type: types[`GET_${type.toUpperCase()}_DOCS_FAILED`]
//       });
//     }
//   };
// };

// export const getTxDoc = (guid, type) => {
//   return async dispatch => {
//     dispatch({
//       type: types[`GET_${type.toUpperCase()}_DOC`]
//     });
//     try {
//       const response = await getTransactionDoc(guid, type.toLowerCase());
//       dispatch({
//         type: types[`GET_${type.toUpperCase()}_DOC_SUCCEED`],
//         payload: response
//       });
//     } catch (error) {
//       dispatch({
//         type: types[`GET_${type.toUpperCase()}_DOC_FAILED`]
//       });
//     }
//   };
// };

// export const generateNewDoc = (data, type) => {
//   return async dispatch => {
//     try {
//       const response = await generateDoc(data, type);
//       dispatch({
//         type: types.GENERATE_DOC,
//         payload: response.data
//       });
//       dispatch(getTxs(1, 10, {}, type.toLowerCase()));
//     }
//     catch(error) {
//       throw(error);
//     }
//   }
// }