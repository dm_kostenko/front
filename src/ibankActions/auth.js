import * as types from "../ibankConstants/actionTypes";
import auth from "../services/paymentBackendAPI/auth/auth";

export const logIn = (data) => async(dispatch) => {
  try {
    const response = await auth.login(data);
    return dispatch({
      type: types.LOG_IN_SUCCESS,
      data: response.data
    });
  }
  catch(error) {
    return dispatch({
      type: types.LOG_IN_FAILED,
      error: error
    });
  }
};

export const logOut = () => (dispatch) => {
  return auth.logout()
    .then(() => {
      dispatch({
        type: types.LOG_OUT,
      });
    })
    .catch(error => {
      throw (error);
    });
};

export const changeLogin = (data) => async(dispatch) => {
  try {
    const response = await auth.change(data);
    return dispatch({
      type: types.CHANGE_LOGIN_SUCCESS,
      data: response.data
    });
  }
  catch(error) {
    return dispatch({
      type: types.CHANGE_LOGIN_FAILED,
      error: error
    });
  }
};

export const getQR = () => async(dispatch) => {
  try {
    const response = await auth.getQRCode();
    return dispatch({
      type: types.GET_QR_CODE,
      payload: response.data
    });
  }
  catch(error) {
    throw (error);
  }
};

export const sendOTP = (data) => async(dispatch) => {
  try {
    const response = await auth.sendOtp(data);
    return dispatch({
      type: types.SEND_OTP,
      payload: response.data
    });
  }
  catch(error) {
    throw (error);
  }
};

export const disableTFA = () => async(dispatch) => {
  try {
    const response = await auth.sendOtp({ disable: true });
    return dispatch({
      type: types.DISABLE_TFA,
      payload: response.data
    });
  }
  catch(error) {
    throw (error);
  }
};

export const getEmailMessageAction = (data) => async (dispatch) => {
  try {
    const response = await auth.getEmailMessage(data);
    dispatch({
      type: types.GET_EMAIL_MESSAGE,
      payload: response.data.status
    });
  }
  catch (error) {
    throw (error);
  }
};

export const checkRecoveryTokenAction = data => async (dispatch) => {
  try {
    const response = await auth.checkRecoveryToken(data);
    dispatch({
      type: types.CHECK_RECOVERY_TOKEN,
      payload: response.data.status
    });
  }
  catch (error) {
    throw (error);
  }
};


export const setNewPasswordAction = data => async (dispatch) => {
  try {
    const response = await auth.setNewPassword(data);
    dispatch({
      type: types.SET_NEW_PASSWORD,
      payload: response.data.status
    });
  }
  catch (error) {
    throw (error);
  }
};