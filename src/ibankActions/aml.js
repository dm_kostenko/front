import * as types from "ibankConstants/actionTypes";
import {
  getAmlHistory,
  getAmlHistoryDetail,
  checkInAml,
  updateAmlDetail,
  getCaseComments,
  addCaseComment,
  getTags,
  upsertTag,
  getCaseTags,
  attachCaseTags,
  getCaseFiles,
  addCaseFiles,
  getAmlCertificate
} from "services/paymentBackendAPI/ibankCommon/aml";


export const getAmlCert = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_AML_CERTIFICATE
  });
  try {
    const response = await getAmlCertificate(args);
    dispatch({
      type: types.GET_AML_CERTIFICATE_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_AML_CERTIFICATE_FAILED
    });
  }
};

export const getAllAmlHistory = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_AML_HISTORY
  });
  try {
    const response = await getAmlHistory(args);
    dispatch({
      type: types.GET_AML_HISTORY_SUCCEED,
      payload: response.data,
      pageSize: args && args.items, 
      currentPage: args && args.page
    });
  } catch (error) {
    dispatch({
      type: types.GET_AML_HISTORY_FAILED
    });
  }
};

export const getAllTags = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_TAGS
  });
  try {
    const response = await getTags(args);
    dispatch({
      type: types.GET_TAGS_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_TAGS_FAILED
    });
  }
};

export const getAllCaseFiles = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_FILES
  });
  try {
    const response = await getCaseFiles(args);
    dispatch({
      type: types.GET_FILES_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_FILES_FAILED
    });
  }
};

export const addFiles = (data) => async(dispatch) => {
  try {
    const response = await addCaseFiles(data);
    dispatch({
      type: types.ADD_FILES,
      payload: response.data
    });
  } catch (error) {
    throw(error);
  }
};

export const updateTag = (data) => async(dispatch) => {
  try {
    const response = await upsertTag(data);
    dispatch({
      type: types.UPDATE_TAG,
      payload: response.data
    });
  } catch (error) {
    throw(error);
  }
};

export const getAllCaseTags = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_CASE_TAGS
  });
  try {
    const response = await getCaseTags(args);
    dispatch({
      type: types.GET_CASE_TAGS_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_CASE_TAGS_FAILED
    });
  }
};

export const attachTagsToCase = (data) => async(dispatch) => {
  try {
    const response = await attachCaseTags(data);
    dispatch({
      type: types.ATTACH_TAGS,
      payload: response.data
    });
  } catch (error) {
    throw(error);
  }
};

export const getAmlDetail = id => async(dispatch) => {
  dispatch({
    type: types.GET_AML_DETAIL
  });
  try {
    const response = await getAmlHistoryDetail(id);
    dispatch({
      type: types.GET_AML_DETAIL_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_AML_DETAIL_FAILED
    });
  }
};

export const searchInAml = (data) => async(dispatch) => {
  try {
    const response = await checkInAml(data);
    dispatch({
      type: types.SEARCH_IN_AML,
      payload: response.data
    });
  } catch (error) {
    throw(error);
  }
};

export const updateAmlSearch = (data) => async(dispatch) => {
  try {
    const response = await updateAmlDetail(data);
    dispatch({
      type: types.UPDATE_AML_DETAIL,
      payload: response.data
    });
  } catch (error) {
    throw(error);
  }
};

export const getAllCaseComments = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_AML_CASE_COMMENTS
  });
  try {
    const response = await getCaseComments(args);
    dispatch({
      type: types.GET_AML_CASE_COMMENTS_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_AML_CASE_COMMENTS_FAILED
    });
  }
};

export const addCommentToCase = (data) => async(dispatch) => {
  try {
    const response = await addCaseComment(data);
    dispatch({
      type: types.ADD_AML_CASE_COMMENT,
      payload: response.data
    });
  } catch (error) {
    throw(error);
  }
};