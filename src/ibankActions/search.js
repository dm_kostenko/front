import * as types from "ibankConstants/actionTypes";

export const inverseSearch = (data) => (dispatch) => {
  try {
    dispatch({
      type: types.INVERSE_SEARCH,
      data
    });
  }
  catch (error) {
    throw (error);
  }
};


export const reset = () => (dispatch) => {
  try {
    dispatch({
      type: types.RESET_SEARCH
    });
  }
  catch (error) {
    throw (error);
  }
};

export const searchInInbox = (data) => (dispatch) => {
  try {
    dispatch({
      type: types.INPUT_SEARCH_IN_INBOX,
      data
    });
  }
  catch (error) {
    throw (error);
  }
};

export const searchInUsers = (data) => (dispatch) => {
  try {
    dispatch({
      type: types.INPUT_SEARCH_IN_USERS,
      data
    });
  }
  catch (error) {
    throw (error);
  }
};

export const searchInRates = (data) => (dispatch) => {
  try {
    dispatch({
      type: types.INPUT_SEARCH_IN_RATES,
      data
    });
  }
  catch (error) {
    throw (error);
  }
};

export const searchInAccounts = (data) => (dispatch) => {
  try {
    dispatch({
      type: types.INPUT_SEARCH_IN_ACCOUNTS,
      data
    });
  }
  catch (error) {
    throw (error);
  }
};

export const searchInTxs = (data, transactionType) => (dispatch) => {
  try {
    dispatch({
      type: types[`INPUT_SEARCH_IN_TRANSACTIONS_${transactionType.toUpperCase()}`],
      data
    });
  }
  catch (error) {
    throw (error);
  }
};

export const searchInTxRules = (data) => (dispatch) => {
  try {
    dispatch({
      type: types.INPUT_SEARCH_IN_TX_RULES,
      data
    });
  }
  catch (error) {
    throw (error);
  }
};

export const setCurrentSearchData = (data) => (dispatch) => {
  try {
    dispatch({
      type: types.SET_CURRENT_SEARCH_DATA,
      data
    });
  }
  catch (error) {
    throw (error);
  }
};