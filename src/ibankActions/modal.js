import * as types from "ibankConstants/actionTypes";

// export function showManagerModal(managerModalState) {
//   return { type: types.SHOW_MANAGER_MODAL, managerModalState };
// }

export const showModal = (modalState, name) => ({ type: types.SHOW_MODAL, payload: { modalState, name } });

export const resetModalData = () => ({ type: types.RESET_MODAL_DATA });

export const showConfirmationModal = (modalState) => ({ type: types.SHOW_CONFIRMATION_MODAL, modalState });