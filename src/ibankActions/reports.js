import * as types from "ibankConstants/actionTypes";
import {
  getTransactionHistory,
  getAmountOfTransactions,
  getTransactionTypes
} from "services/paymentBackendAPI/ibankCommon/reports";

export const getTransactionsHistory = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_TRANSACTION_HISTORY
  });
  try {
    const response = await getTransactionHistory(args);
    dispatch({
      type: types.GET_TRANSACTION_HISTORY_SUCCEED,
      payload: response.data,
    });
  } catch (error) {
    dispatch({
      type: types.GET_TRANSACTION_HISTORY_FAILED
    });
  }
};

export const getTransactionsAmount = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_AMOUNT_OF_TRANSACTIONS
  });
  try {
    const response = await getAmountOfTransactions(args);
    dispatch({
      type: types.GET_AMOUNT_OF_TRANSACTIONS_SUCCEED,
      payload: response.data,
    });
  } catch (error) {
    dispatch({
      type: types.GET_AMOUNT_OF_TRANSACTIONS_FAILED
    });
  }
};

export const getTransactionsTypes = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_TRANSACTION_TYPES
  });
  try {
    const response = await getTransactionTypes(args);
    dispatch({
      type: types.GET_TRANSACTION_TYPES_SUCCEED,
      payload: response.data,
    });
  } catch (error) {
    dispatch({
      type: types.GET_TRANSACTION_TYPES_FAILED
    });
  }
};
