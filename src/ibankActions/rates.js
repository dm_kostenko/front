import * as types from "ibankConstants/actionTypes";
import {
  getRates,
  getRateRules,
  getRateRuleParams,
  createRate,
  changeRateRuleParams
} from "services/paymentBackendAPI/ibankCommon/rates";


export const getAllRates = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_RATES
  });
  try {
    const response = await getRates(args);
    dispatch({
      type: types.GET_RATES_SUCCEED,
      payload: response.data,
      pageSize: args && args.items, 
      currentPage: args && args.page
    });
  } catch (error) {
    dispatch({
      type: types.GET_RATES_FAILED
    });
  }
};

export const getAllRateRules = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_RATE_RULES
  });
  try {
    const response = await getRateRules(args);
    dispatch({
      type: types.GET_RATE_RULES_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_RATE_RULES_FAILED
    });
  }
};

export const getAllRateRuleParams = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_RATE_RULE_PARAMS
  });
  try {
    const response = await getRateRuleParams(args);
    dispatch({
      type: types.GET_RATE_RULE_PARAMS_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_RATE_RULE_PARAMS_FAILED
    });
  }
};

export const createNewRate = (page, items, data) => async(dispatch) => {
  try {
    const response = await createRate(data);
    dispatch({
      type: types.CREATE_RATE,
      payload: response.data
    });
    dispatch(getAllRates({ page, items }));
  } catch (error) {
    throw(error);
  }
};

export const changeParams = data => async(dispatch) => {
  try {
    const response = await changeRateRuleParams(data);
    dispatch({
      type: types.CHANGE_RATE_RULE_PARAMS,
      payload: response.data
    });
  } catch (error) {
    throw(error);
  }
};