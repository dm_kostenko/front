import * as types from "../ibankConstants/actionTypes";
import {
  getWaitPayments,
  getTransactions,
  getAllTransactions,
  getTransaction,
  createTransaction,
  createTransactions,
  createTransactionRule,
  editTransactionStatus,
  requestOtp,
  getTransactionRules,
  getTransactionsTemplates,
  upsertTransactionsTemplates,
  getTransactionsFees,
  getExternalTransactionsTemplates,
  createExternalTransactionTemplate,
  getInternalTransactionsTemplates,
  createInternalTransactionTemplate
} from "services/paymentBackendAPI/ibankCommon/transactions";

export const getTxs = (args) => async(dispatch) => {
  dispatch({
    type: types[`GET_${args && args.type.toUpperCase()}`]
  });
  try {
    let response = await getTransactions(args);
    dispatch({
      type: types[`GET_${args && args.type.toUpperCase()}_SUCCEED`],
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types[`GET_${args && args.type.toUpperCase()}_FAILED`]
    });
  }
};

export const getWaitingPayments = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_WAITING_PAYMENTS
  });
  try {
    let response = await getWaitPayments(args);
    dispatch({
      type: types.GET_WAITING_PAYMENTS_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_WAITING_PAYMENTS_FAILED
    });
  }
};


export const getAllTxs = (type) => async(dispatch) => {
  dispatch({
    type: types[`GET_${type.toUpperCase()}`]
  });
  try {
    let response;
    type === "all"
      ? response = await getAllTransactions()
      : response = await getAllTransactions(type.toUpperCase());
    dispatch({
      type: types[`GET_${type.toUpperCase()}_SUCCEED`],
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types[`GET_${type.toUpperCase()}_FAILED`]
    });
  }
};

export const getTx = (guid, type) => async(dispatch) => {
  dispatch({
    type: types[`GET_${type.toUpperCase()}_DETAIL`]
  });
  try {
    const response = await getTransaction(guid, type.toLowerCase());
    dispatch({
      type: types[`GET_${type.toUpperCase()}_DETAIL_SUCCEED`],
      payload: response
    });
  } catch (error) {
    dispatch({
      type: types[`GET_${type.toUpperCase()}_DETAIL_FAILED`]
    });
  }
};

export const getAllTxRules = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_ALL_TX_RULES
  });
  try {
    let response = await getTransactionRules(args);
    dispatch({
      type: types.GET_ALL_TX_RULES_SUCCEED,
      payload: response.data,
      pageSize: args && args.items, 
      currentPage: args && args.page
    });
  } catch (error) {
    dispatch({
      type: types.GET_ALL_TX_RULES_FAILED
    });
  }
};

export const createTxRule = (page, items, data) => async(dispatch) => {
  try {
    const response = await createTransactionRule(data);
    dispatch({
      type: types.CREATE_TRANSACTION_RULE,
      payload: response.data
    });    
    dispatch( getAllTxRules( { page, items } ) );
  } catch (error) {
    throw(error);
  }
};

export const createTx = (data, type) => async(dispatch) => {
  try {
    dispatch({
      type: types.CREATE_REQUEST,
      payload: [          
        {
          type: "action",
          data: types.CREATE_TRANSACTION
        }
      ]
    });
    const response = await createTransaction(data, type.toLowerCase());
    dispatch({
      type: types.CREATE_TRANSACTION,
      payload: response.data
    });      
    dispatch({
      type: types.CLEAR_DISPATCH_ARRAY
    });
  } catch (error) {
    throw(error);
  }
};

export const createTxs = (data) => async(dispatch) => {
  try {
    dispatch({
      type: types.CREATE_REQUEST,
      payload: [          
        {
          type: "action",
          data: types.CREATE_TRANSACTIONS
        }
      ]
    });
    const response = await createTransactions(data);
    dispatch({
      type: types.CREATE_TRANSACTIONS,
      payload: response.data
    });      
    dispatch({
      type: types.CLEAR_DISPATCH_ARRAY
    });
  } catch (error) {
    throw(error);
  }
};


export const editTxStatus = (data, type, guid, tableType, directionData, page, items) => async(dispatch) => {
  try {      
    dispatch({
      type: types.CREATE_REQUEST,
      payload: [
        {
          type: "action",
          data: types.EDIT_TRANSACTION_STATUS
        },
        {
          type: "function",
          data: getTxs({
            page, 
            items, 
            ...directionData, 
            type: tableType.toLowerCase()
          })
        },
        {
          type: "text",
          data: `${data.status === "Unapproved" 
            ? "Wait to be approved by admin"
            : data.status}`
        }
      ]
    });
    const response = await editTransactionStatus(data, type.toLowerCase(), guid);
    dispatch({
      type: types.EDIT_TRANSACTION_STATUS,
      payload: response.data
    });
    dispatch(getTxs({
      page, 
      items, 
      ...directionData, 
      type: tableType.toLowerCase()
    }));      
    dispatch({
      type: types.CLEAR_DISPATCH_ARRAY
    });
  } catch (error) {
    throw(error);
  }
};

export const sendRequestForOtp = (guid) => async(dispatch) => {
  try {
    const response = await requestOtp(guid);
    dispatch({
      type: types.REQUEST_OTP,
      payload: response.data
    });
  } catch (error) {
    throw(error);
  }
};


export const getAllTxTemplates = (params) => async(dispatch) => {
  dispatch({
    type: types.GET_TXS_TEMPLATES
  });
  try {
    let response = await getTransactionsTemplates(params);
    dispatch({
      type: types.GET_TXS_TEMPLATES_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_TXS_TEMPLATES_FAILED
    });
  }
};

export const upsertTxsTemplates = (data) => async(dispatch) => {
  try {
    let response = await upsertTransactionsTemplates(data);
    dispatch({
      type: types.UPSERT_TXS_TEMPLATES,
      payload: response.data
    });
  } catch (error) {
    throw(error)
  }
};

export const getExternalTxsTemplates = (params) => async(dispatch) => {
  dispatch({
    type: types.GET_EXTERNAL_TXS_TEMPLATES
  });
  try {
    let response = await getExternalTransactionsTemplates(params);
    dispatch({
      type: types.GET_EXTERNAL_TXS_TEMPLATES_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_EXTERNAL_TXS_TEMPLATES_FAILED
    });
  }
};

export const createExternalTxTemplate = (data) => async(dispatch) => {
  try {
    let response = await createExternalTransactionTemplate(data);
    dispatch({
      type: types.CREATE_EXTERNAL_TX_TEMPLATE,
      payload: response.data
    });
  } catch (error) {
    throw(error)
  }
};

export const getInternalTxsTemplates = (params) => async(dispatch) => {
  dispatch({
    type: types.GET_INTERNAL_TXS_TEMPLATES
  });
  try {
    let response = await getInternalTransactionsTemplates(params);
    dispatch({
      type: types.GET_INTERNAL_TXS_TEMPLATES_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_INTERNAL_TXS_TEMPLATES_FAILED
    });
  }
};

export const createInternalTxTemplate = (data) => async(dispatch) => {
  try {
    let response = await createInternalTransactionTemplate(data);
    dispatch({
      type: types.CREATE_INTERNAL_TX_TEMPLATE,
      payload: response.data
    });
  } catch (error) {
    throw(error)
  }
};

export const getTxsFees = (data) => async(dispatch) => {
  try {
    let response = await getTransactionsFees(data);
    dispatch({
      type: types.GET_TXS_FEES,
      payload: response.data
    });
  } catch (error) {
    throw(error)
  }
};

export const changeTemplateCheckbox = (template) => dispatch => dispatch({ type: types.CHANGE_TEMPLATE_CHECKBOX, payload: template })

export const changeWaitingCheckbox = (waitingTx) => dispatch => dispatch({ type: types.CHANGE_WAITING_CHECKBOX, payload: waitingTx })