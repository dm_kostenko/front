import * as types from "ibankConstants/actionTypes";
import {
  getUsers,
  getEntities,
  getUser,
  getUserInfo,
  upsertUser
} from "services/paymentBackendAPI/ibankCommon/users";


export const getAllUsers = (args) => async(dispatch) => {
  dispatch({
    type: types.GET_USERS
  });
  try {
    const response = await getUsers(args);
    dispatch({
      type: types.GET_USERS_SUCCEED,
      payload: response.data,
      pageSize: args && args.items, 
      currentPage: args && args.page
    });
  } catch (error) {
    dispatch({
      type: types.GET_USERS_FAILED
    });
  }
};

export const getAllEntities = () => async(dispatch) => {
  dispatch({
    type: types.GET_ENTITIES
  });
  try {
    const response = await getEntities();
    dispatch({
      type: types.GET_ENTITIES_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_ENTITIES_FAILED
    });
  }
};

export const getUserDetail = (guid) => async(dispatch) => {
  dispatch({
    type: types.GET_USER
  });
  try {
    const response = await getUser(guid);
    dispatch({
      type: types.GET_USER_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_USER_FAILED
    });
  }
};

export const getUserProfile = () => async(dispatch) => {
  dispatch({
    type: types.GET_USER_INFO
  });
  try {
    const response = await getUserInfo();
    dispatch({
      type: types.GET_USER_INFO_SUCCEED,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: types.GET_USER_INFO_FAILED
    });
  }
};

export const createNewUser = (page, items, data) => async(dispatch) => {
  try {
    const response = await upsertUser(data);
    dispatch({
      type: types.CREATE_USER,
      payload: response.data
    });
    dispatch(getAllUsers(page, items, {}));
  } catch (error) {
    throw(error);
  }
};

export const changePassword = (data) => async(dispatch) => {
  try {
    dispatch({
      type: types.CREATE_REQUEST,
      payload: [
        {
          type: "action",
          data: types.CHANGE_PASSWORD
        },
        {
          type: "text",
          data: "Password is changed"
        }
      ]
    });
    const response = await upsertUser(data);
    dispatch({
      type: types.CHANGE_PASSWORD,
      payload: response.data
    });
  } catch (error) {
    throw(error);
  }
};