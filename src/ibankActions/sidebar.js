import * as types from "ibankConstants/actionTypes";


export const hideSidebar = () => (dispatch) => {
  dispatch({
    type: types.HIDE_SIDEBAR
  });
};

export const inverseSidebar = () => (dispatch) => {
  dispatch({
    type: types.INVERSE_SIDEBAR
  });
};

export const onCollapseItemClick = (state) => (dispatch) => {
  dispatch({
    type: types.COLLAPSE_ITEM_CLICK,
    state
  });
};