import * as types from "../constants/actionTypes";

const initialState = {
  accountsList: [],
  account: {
    guid: "",
    currency_guid: "",
    currency_name: "",
    number: "",
    balance: "",
    created_at: "",
    created_by: "",
    updated_at: "",
    updated_by: ""
  }
};

export default function accounts(state = initialState, action) {
  switch (action.type) {
  case types.GET_ACCOUNTS:
    return {
      ...state,
      accountsLoading: true,
      error: false
    };
  case types.GET_ACCOUNTS_SUCCEED:
    return {
      ...state,
      accountsList: action.data.data,
      count: action.data.count,
      currentPage: action.currentPage,
      pageSize: action.pageSize,
      accountsLoading: false,
      error: false
    };
  case types.GET_ACCOUNTS_FAILED:
    return {
      ...state,
      accountsLoading: false,
      error: true
    };
  case types.GET_ACCOUNT:
    return {
      ...state,
      accountLoading: true,
      error: false
    };
  case types.GET_ACCOUNT_SUCCEED:
    return {
      ...state,
      account: action.account,
      accountLoading: false,
      error: false
    };
  case types.GET_ACCOUNT_FAILED:
    return {
      ...state,
      accountLoading: false,
      error: true
    };
  case types.UPDATE_ACCOUNT: {
    let newAccountsList = state.accountsList;
    newAccountsList = newAccountsList.map(account => {
      if (account.guid === action.account.guid)
        return action.account;
      else
        return account;
    });
    return { ...state, accountsList: newAccountsList };
  }
  case types.ADD_ACCOUNT:
    return { ...state };
  case types.DELETE_ACCOUNT:
    let newAccountsList = state.accountsList.filter(account => account.guid !== action.account.guid);
    return { ...state, accountsList: newAccountsList };
  default:
    return state;
  }
}