import * as types from "../constants/actionTypes";

const initialState = {
  shopsList: [],
  count: -1,
  shopGuid: "",
  shopGateways: [],
  gatewayShops: [],
  shopAccounts: [],
  gatewayProps: [],
  reportSpops: [],
  topShops: [],
  shopLoading: false,
  shopsLoading: false,
  gatewayPropsLoading: false,
  shop: {
    guid: "",
    merchant_guid: "",
    merchant_name: "",
    name: "",
    url: "",
    email: "",
    phone: "",
    enabled: false,
    note: "",
    secret: "",
    hash_key: "No hash-key",
    enable_checkout: false,
    checkout_method: "",
    antiFraudMonitor: false,
    antiFraudMonitorValue: "",
    created_at: "",
    created_by: "",
    updated_at: "",
    updated_by: ""
  }
};

export default function shops(state = initialState, action) {
  switch (action.type) {
  case types.GET_SHOPS:
    return {
      ...state,
      shopsLoading: true,
      error: false
    };
  case types.GET_SHOPS_SUCCEED:
    return {
      ...state,
      shopsList: action.data.data,
      count: action.data.count,
      pageSize: action.pageSize,
      currentPage: action.currentPage,
      shopsLoading: false,
      error: false,
      shopGateways: []
    };
  case types.GET_SHOPS_FAILED:
    return {
      ...state,
      shopsLoading: false,
      error: true
    };
  case types.GET_SHOP:
    return {
      ...state,
      shopLoading: true,
      error: false
    };
  case types.GET_SHOP_SUCCEED:
    return {
      ...state,
      shop: action.shop,
      shopLoading: false,
      error: false
    };
  case types.GET_SHOP_FAILED:
    return {
      ...state,
      shopLoading: false,
      error: true
    };
  case types.DELETE_SHOP:{
    let newShopsList = state.shopsList.filter(shop => shop.guid !== action.shop.guid);
    return { ...state, shopsList: newShopsList };
  }
  case types.ADD_SHOP:
    return { ...state, shopGuid: action.shop.guid };
  case types.EDIT_SHOP:{
    let newShopsList = state.shopsList || [];
    newShopsList = newShopsList.map(shop => {
      if (shop.guid === action.shop.guid)
        return action.shop;
      else
        return shop;
    });
    if (newShopsList.length !== 0)
      return { ...state, shopsList: newShopsList };
    return state;
  }
  case types.GET_SHOP_GATEWAYS:
    return {
      ...state,
      shopGatewaysLoading: true,
      error: false
    };
  case types.GET_SHOP_GATEWAYS_SUCCEED:
    return {
      ...state,
      shopGateways: action.data.data,
      gatewaysCount: action.data.count,
      shopGatewaysLoading: false,
      error: false
    };
  case types.GET_SHOP_GATEWAYS_FAILED:
    return {
      ...state,
      shopGatewaysLoading: false,
      error: true
    };
  case types.UPSERT_SHOP_GATEWAY:{
    let shopGateways = state.shopGateways || [];
    let changedGateways = state.changedGateways;
    if (typeof (changedGateways) === "undefined")
      changedGateways = [];
    if (action.gateway.delete) {
      shopGateways = shopGateways.filter(gateway => gateway.guid !== action.gateway.guid);
      const newChangedGateways = changedGateways.filter(gateway => gateway.guid !== action.gateway.guid);
      if (newChangedGateways.length === changedGateways.length)
        changedGateways = [ ...changedGateways, action.gateway ];
      else
        changedGateways = newChangedGateways;
    }
    else {
      shopGateways = shopGateways.filter(gateway => !(gateway.guid === action.gateway.guid && gateway.delete));
      changedGateways = changedGateways.filter(gateway => !(gateway.guid === action.gateway.guid && gateway.delete));
      shopGateways = [ ...shopGateways, action.gateway ];
      changedGateways = [ ...changedGateways, action.gateway ] ;
    }
    return { ...state, shopGateways, changedGateways };
  }
  case types.GET_SHOPS_BY_GATEWAY:
    return { 
      ...state, 
      gatewayShopsLoading: true, 
    };
  case types.GET_SHOPS_BY_GATEWAY_SUCCEED:
    return { 
      ...state, 
      gatewayShops: action.data.data, 
      shopsCount: action.data.count,
      gatewayShopsLoading: false 
    };
  case types.GET_SHOPS_BY_GATEWAY_FAILED:
    return { 
      ...state, 
      gatewayShopsLoading: false, 
    };
  case types.GET_SHOPS_BY_MERCHANT:
    return { ...state, shopsList: action.data.data, count: action.data.count };
  case types.GET_SHOPS_BY_GROUP:
    return { ...state, shopsList: action.data };
  case types.GET_SHOPS_BY_PARTNER:
    return { ...state, shopsList: action.data };
  case types.GET_AMOUNT_SHOPS_FOR_GROUP:
    return { ...state, count: action.count };
  case types.EDIT_SHOP_GATEWAY_DATA:{
    let shopGateways = state.shopGateways || [];
    let changedGateways = state.changedGateways;
    if (typeof (changedGateways) === "undefined" || changedGateways.length === 0) {
      changedGateways = [ action.changedGatewayData ];
    }
    else {
      changedGateways = changedGateways.map(gateway => {
        if (gateway.guid === action.changedGatewayData.guid)
          return action.changedGatewayData;
        else
          return gateway;
      });
    }
    shopGateways = shopGateways.map(gateway => {
      if (gateway.guid === action.changedGatewayData.guid)
        return action.changedGatewayData;
      else
        return gateway;
    });
    return { ...state, shopGateways, changedGateways };
  }
  case types.UPSERT_SHOP_GATEWAYS:
    return { ...state, changedGateways: [], shopGateways: [] };

  case types.ADD_SHOP_ACCOUNT:
    return { ...state, shopAccounts: [ action.shopAccount ] };
  case types.UPSERT_SHOP_ACCOUNTS:{
    let changedAccounts = action.data.accounts;
    let shopsList = state.shopsList;
    shopsList.forEach(shop => {
      if (shop.guid === action.data.shopGuid) {
        changedAccounts = changedAccounts.map(account => {
          if (account.delete)
            shop.shopAccounts = shop.shopAccounts.filter(shopAccount => shopAccount.guid !== account.guid);
          else {
            let f = 0;
            shop.shopAccounts.forEach(shopAccount => {
              if (shopAccount.guid === account.guid) {
                shopAccount = account;
                f = 1;
              }
            });
            if (f === 0)
              shop.shopAccounts = [ ...shop.shopAccounts, shop ];
          }
          return null;
        });
      }
    });
    return { ...state, shopsList, changedAccounts: [], shopAccounts: [] };
  }
  case types.GET_SHOP_ACCOUNTS:
    return {
      ...state,
      shopAccountsLoading: true,
      error: false
    };
  case types.GET_SHOP_ACCOUNTS_SUCCEED:
    return {
      ...state,
      shopAccounts: action.data.data,
      accountCount: action.data.count,
      shopAccountsLoading: false,
      error: false
    };
  case types.GET_SHOP_ACCOUNTS_FAILED:
    return {
      ...state,
      shopAccountsLoading: false,
      error: true
    };
  case types.UPSERT_SHOP_CHANGED_ACCOUNT:{
    let shopAccounts = state.shopAccounts || [];
    let changedAccounts = state.changedAccounts;
    if (typeof (changedAccounts) === "undefined")
      changedAccounts = [];
    if (action.account.delete) {
      shopAccounts = shopAccounts.filter(account => account.guid !== action.account.guid);
      const newChangedAccounts = changedAccounts.filter(account => account.guid !== action.account.guid);
      if (newChangedAccounts.length === changedAccounts.length)
        changedAccounts = [ ...changedAccounts, action.account ];
      else
        changedAccounts = newChangedAccounts;
    }
    else {
      shopAccounts = shopAccounts.filter(account => !(account.guid === action.account.guid && account.delete));
      changedAccounts = changedAccounts.filter(account => !(account.guid === action.account.guid && account.delete));
      shopAccounts = [ ...shopAccounts, action.account ];
      changedAccounts = [ ...changedAccounts, action.account ];
    }
    return { ...state, shopAccounts, changedAccounts };
  }
  case types.GET_AMOUNT_SHOPS_FOR_PARTNER:
    return { ...state, count: action.count };
  case types.GET_SHOP_GATEWAY_PROPS:
    return { 
      ...state, 
      gatewayPropsLoading: true 
    };
  case types.GET_SHOP_GATEWAY_PROPS_SUCCEED:
    return { 
      ...state, 
      gatewayPropsLoading: false,
      gatewayProps: action.data.data 
    };
  case types.GET_SHOP_GATEWAY_PROPS_FAILED:
    return { 
      ...state, 
      gatewayPropsLoading: false 
    };
  case types.RESET_MODAL_DATA:
    return { 
      ...state,
      changedGateways: []
    };
  case types.GET_REPORT_SHOP_TOTALS:
    return {
      ...state,
      reportShopsLoading: true,
      error: false
    };
  case types.GET_REPORT_SHOP_TOTALS_SUCCEED:
    return {
      ...state,
      reportSpops: action.data.data,
      reportsCount: action.data.count,
      reportShopsLoading: false,
      error: false
    };
  case types.GET_TOP_SHOPS:
    return { 
      ...state, 
      topShopsLoading: true 
    };
  case types.GET_TOP_SHOPS_SUCCEED:
    return { 
      ...state, 
      topShops: action.data,
      topShopsLoading: false 
    };
  case types.GET_TOP_SHOPS_FAILED:
    return { 
      ...state, 
      topShopsLoading: false
    };
  default:
    return state;
  }
}
