import * as types from "../constants/actionTypes";

const initialState = {
  cardsList: [],
  accountCards: [],
  card: {
    guid: "",
    type: "",
    account_guid: "",
    account_number: "",
    card_number: "",
    currency_guid: "",
    currency_name: "",
    name_on_card: "",
    expired: "",
    expires_at: "",
    balance: "",
    created_at: "",
    created_by: "",
    updated_at: "",
    updated_by: ""
  }
};

export default function cards(state = initialState, action) {
  switch (action.type) {
  case types.ADD_CARD:
    return { ...state, card_guid: action.card.guid };
  case types.UPDATE_CARD:
    let cardsList = state.cardsList;
    cardsList = cardsList.map(card => {
      if (card.guid === action.card.guid)
        return action.card;
      else
        return card;
    });
    cardsList = [ ...cardsList ];
    return { ...state, cardsList };
  case types.GET_CARDS:
    return { 
      ...state, 
      cardsLoading: true,
      error: false, 
    };
  case types.GET_CARDS_SUCCEED:
    return { 
      ...state, 
      cardsList: action.data.data, 
      count: action.data.count, 
      pageSize: action.pageSize, 
      currentPage: action.currentPage,
      cardsLoading: false,
      error: false,  
    };
  case types.GET_CARDS_FAILED:
    return { 
      ...state, 
      cardsLoading: false,
      error: true,  
    };
  case types.GET_CARD:
    return {
      ...state,
      cardLoading: true,
      error: false
    };
  case types.GET_CARD_SUCCEED:
    return {
      ...state,
      card: action.card,
      cardLoading: false,
      error: false
    };
  case types.GET_CARD_FAILED:
    return {
      ...state,
      cardLoading: false,
      error: true
    };
  case types.GET_CARDS_ACCOUNT:
    return { ...state, accountCards: action.data.data, accountCardsCount: action.data.count };
  case types.DELETE_CARD:
    let newCardsList = state.cardsList.filter(card => card.guid !== action.card.guid);
    return { ...state, cardsList: newCardsList };
  default:
    return state;
  }
}