import * as types from "../constants/actionTypes";

const initialState = {
  paramsList: []
};

export default function steps(state = initialState, action) {
  switch (action.type) {
  case types.GET_PARAMS_FOR_STEP:
    return {
      ...state,
      loading: true,
      error: false,
    };
  case types.GET_PARAMS_FOR_STEP_SUCCEED: {
    return {
      ...state,
      paramsList: action.data.data,
      loading: false,
      error: false,
    };
  }
  case types.GET_PARAMS_FOR_STEP_FAILED:
    return {
      ...state,
      loading: false,
      error: true,
    };
  default:
    return state;
  }
}