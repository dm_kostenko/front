import * as types from "../constants/actionTypes";

const initialState = {
  ratesList: [],
  rate: {
    currency_guid: "",
    currency_name: "",
    gateway_guid: "",
    gateway_name: "",
    hold_rate: "",
    hold_period: "",
    apply_from: "",
    apply_to: "",
    created_at: "",
    created_by: "",
    updated_at: "",
    updated_by: ""
  },
  oneTimeCharges: {},
  periodicCharges: {},
  conditionalCharges: {},
  ratePaymentStatus: []
};

export default function rates(state = initialState, action) {
  switch (action.type) {
  case types.GET_RATES:
    return {
      ...state,
      ratesLoading: true,
      error: false
    };
  case types.GET_RATES_SUCCEED:
    return {
      ...state,
      ratesList: action.data.data,
      count: action.data.count,
      currentPage: action.currentPage,
      pageSize: action.pageSize,
      ratesLoading: false,
      error: false
    };
  case types.GET_RATES_FAILED:
    return {
      ...state,
      ratesLoading: false,
      error: true
    };
  case types.GET_RATE:
    return {
      ...state,
      rateLoading: true,
      error: false
    };
  case types.GET_RATE_SUCCEED:
    return {
      ...state,
      rate: action.rate,
      rateLoading: false,
      error: false
    };
  case types.GET_RATE_FAILED:
    return {
      ...state,
      rateLoading: false,
      error: true
    };
  case types.GET_PAYMENT_STATUS_RATE:
    return {
      ...state,
      ratePaymentLoading: true,
      error: false
    };
  case types.GET_PAYMENT_STATUS_RATE_SUCCEED:
    return {
      ...state,
      ratePaymentStatus: action.rate,
      ratePaymentLoading: false,
      error: false
    };
  case types.GET_PAYMENT_STATUS_RATE_FAILED:
    return {
      ...state,
      ratePaymentLoading: false,
      error: true
    };
  case types.EDIT_RATE: {
    let newRatesList = state.ratesList;
    newRatesList = newRatesList.map(rate => {
      if (rate.guid === action.rate.guid)
        return action.rate;
      else
        return rate;
    });
    return { ...state, ratesList: newRatesList };
  }
  case types.ADD_RATE:
    return { ...state };
  case types.GET_ONE_TIME_CHARGES:
    return {
      ...state,
      oneTimeLoading: true,
      error: false
    };
  case types.GET_ONE_TIME_CHARGES_SUCCEED:
    return {
      ...state,
      oneTimeCharges: action.data,
      oneTimeLoading: false,
      error: false
    };
  case types.GET_ONE_TIME_CHARGES_FAILED:
    return {
      ...state,
      oneTimeLoading: false,
      error: true
    };
  case types.GET_PERIODIC_CHARGES:
    return {
      ...state,
      periodicLoading: true,
      error: false
    };
  case types.GET_PERIODIC_CHARGES_SUCCEED:
    return {
      ...state,
      periodicCharges: action.data,
      periodicLoading: false,
      error: false
    };
  case types.GET_PERIODIC_CHARGES_FAILED:
    return {
      ...state,
      periodicLoading: false,
      error: true
    };
  case types.GET_CONDITIONAL_CHARGES:
    return {
      ...state,
      conditionalLoading: true,
      error: false
    };
  case types.GET_CONDITIONAL_CHARGES_SUCCEED:
    return {
      ...state,
      conditionalCharges: action.data,
      conditionalLoading: false,
      error: false
    };
  case types.GET_CONDITIONAL_CHARGES_FAILED:
    return {
      ...state,
      conditionalLoading: false,
      error: true
    };
  case types.ADD_ONE_TIME_CHARGE:
    return { ...state };
  case types.ADD_PERIODIC_CHARGE:
    return { ...state };
  case types.ADD_CONDITIONAL_CHARGE:
    return { ...state };
  case types.DELETE_RATE:
    let newRatesList = state.ratesList.filter(rate => rate.shop_guid !== action.rate.shop_guid && rate.gateway_guid !== action.rate.gateway_guid && rate.currency_guid !== action.rate.currency_guid);
    return { ...state, ratesList: newRatesList };
  default:
    return state;
  }
}