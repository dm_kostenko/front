import {
  LOG_IN_SUCCESS,
  LOG_IN_FAILED,
  LOG_OUT
} from "../constants/actionTypes";

export default function auth(state = [], action) {
  switch(action.type) {
  case LOG_IN_SUCCESS:
    return { 
      ...state, 
      response: action.data,
      error: ""
    };
  case LOG_IN_FAILED:
    const errorMessage = action.error.response ? action.error.response.data.error : "Server error";
    return { 
      ...state, 
      error: errorMessage,
      response: ""
    };
  case LOG_OUT:
    return state;
  default:
    return state;
  }
}