import * as types from "../constants/actionTypes";

export default function logs(state = [], action) {
  switch (action.type) {
  case types.GET_USERS_LOGS:
    return {
      ...state,
      usersLogsLoading: true,
      error:false
    };
  case types.GET_USERS_LOGS_SUCCEED:
    return {
      ...state,
      usersLogs: action.data.data,
      usersLogsCount: action.data.count,
      usersLogsLoading: false,
      error:false
    };
  case types.GET_USERS_LOGS_FAILED:
    return {
      ...state,
      usersLogsLoading: false,
      error: true
    };
  case types.GET_TRANSACTIONS_LOGS:
    return {
      ...state,
      transactionsLogsLoading: true,
      error: false
    };
  case types.GET_TRANSACTIONS_LOGS_SUCCEED:
    return {
      ...state,
      transactionsLogs: action.data.data,
      transactionsLogsCount: action.data.count,
      transactionsLogsLoading: false,
      error: false
    };
  case types.GET_TRANSACTIONS_LOGS_FAILED:
    return {
      ...state,
      transactionsLogsLoading: false,
      error: true
    };
  case types.GET_TRANSACTIONS_STEPS_LOGS:
    return {
      ...state,
      loading: true,
      error: false
    };
  case types.GET_TRANSACTIONS_STEPS_LOGS_SUCCEED:
    return {
      ...state,
      transactionsStepsLogs: action.data.data,
      transactionsStepsLogsCount: action.data.count,
      loading: false,
      error: false
    };
  case types.GET_TRANSACTIONS_STEPS_LOGS_FAILED:
    return {
      ...state,
      loading: false,
      error: true
    };
  default:
    return state;
  }
}