import { combineReducers } from "redux";
import auth from "./auth";
import logins from "./logins";
import merchants from "./merchants";
import roles from "./roles";
import shops from "./shops";
import gateways from "./gateways";
import groups from "./groups";
import partners from "./partners";
import privileges from "./privileges";
import rates from "./rates";
import cards from "./cards";
import currencies from "./currencies";
import modal from "./modal";
import accounts from "./accounts";
import blacklist from "./blacklists";
import transactions from "./transactions";
import logs from "./logs";
import search from "./search";
import sidebar from "./sidebar";
import useraccounts from "./useraccounts";
import steps from "./steps";

const rootReducer = combineReducers({
  auth,
  logins,
  merchants,
  roles,
  gateways,
  groups,
  partners,
  privileges,
  rates,
  currencies,
  shops,
  modal,
  accounts,
  transactions,
  cards,
  blacklist,
  logs,
  search,
  sidebar,
  useraccounts,
  steps
});

export default rootReducer;