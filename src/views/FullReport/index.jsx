import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import PieChartCardsTypes from "components/PieChart/CardTypes";
import PieChartCurrencies from "components/PieChart/Currencies";
import PieChartTransactionsStatuses from "components/PieChart/TransactionsStatuses";
import PieChartTransactionsTypes from "components/PieChart/TransactionsTypes";
import Map from "components/Report/Full/Geographic";
import LineChartTransactionsCurrencies from "components/LineChart/FullDayTransactionsCurrencies";
import TotalsTable from "./TotalsTable";
import DetailedTotalsTable from "./DetailedTotalsTable";

export default class FullReport extends Component {
  render() {
    return (
      <div className="content">
        <div className="card">
          <LineChartTransactionsCurrencies />
          <Row>
            <Col md={3} sm={3} xs={3}>
              <div className="card-large">
                <div className="header">Transactions status</div>
                <PieChartTransactionsStatuses />
              </div>
            </Col>
            <Col md={3} sm={3} xs={3}>
              <div className="card-large">
                <div className="header">Card type</div>
                <PieChartCardsTypes />
              </div>
            </Col>
            <Col md={3} sm={3} xs={3}>
              <div className="card-large">
                <div className="header">Currency</div>
                <PieChartCurrencies />
              </div>
            </Col>
            <Col md={3} sm={3} xs={3}>
              <div className="card-large">
                <div className="header">Transactions type</div>
                <PieChartTransactionsTypes />
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <TotalsTable />
            </Col>
          </Row>
          <Row>
            <Col>
              <DetailedTotalsTable />
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="card-map">
                <div className="header">Geographic</div>
                <Map/>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}