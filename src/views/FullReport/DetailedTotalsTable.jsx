import React, { Component } from "react";
import Table from "components/UI/Table";

export default class TotalsTable extends Component {
  render() {
    const columns = [
      { path: "card_type", label: "Card type" },
      { path: "transaction_type", label: "Transaction type" },
      { path: "status", label: "Status" },
      { path: "sum", label: "Sum" },
      { path: "count", label: "Count" }
    ];
    const data = [ { card_type:0, transaction_type:0, status:0, sum:0, count:0 } ];
    return (
      <div>
        <Table
          columns={columns}
          data={data}
          disableSearch={true}
        />        
      </div>
    );
  }
}