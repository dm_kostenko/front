import React, { Component } from "react";
import Table from "components/UI/Table";

export default class TotalsTable extends Component {
  render() {
    const columns = [
      { path: "currency", label: "Currency" },
      { path: "status", label: "Status" },
      { path: "authorization", label: "Authorization" },
      { path: "payment", label: "Payment" },
      { path: "visa", label: "VISA" },
      { path: "mastercard", label: "MasterCard" },
      { path: "refund", label: "Refund" },
      { path: "chargeback", label: "Chargeback" }
    ];
    const data = [ { currency:"USD", status:0, authorization:0, payment:0, visa:0, mastercard:0, refund:0, chargeback: 0 } ];
    return (
      <div>
        <Table
          columns={columns}
          data={data}
          disableSearch={true}
        />        
      </div>
    );
  }
}