import React from "react";
import TransactionMerchantHistoryMain from "components/Transaction/MerchantHistory/Main";

const TransactionMerchantHistory = () => {
  return (
    <div className="content">
      <TransactionMerchantHistoryMain />
    </div>
  );
};

export default TransactionMerchantHistory;