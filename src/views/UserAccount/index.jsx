import React from "react";
import UserAccountMain from "components/UserAccount/Main";

const UserAccount = () => {
  return (
    <div className="content">
      <UserAccountMain />
    </div>
  );
};

export default UserAccount;