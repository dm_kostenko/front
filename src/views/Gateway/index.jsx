import React from "react";
import GatewayMain from "components/Gateway/Main";
import Modal from "components/UI/Modal";
import Can from "components/Can";
import GatewayCreator from "components/Gateway/Creator";

const Gateway = () => {
  return (
    <div className="content">
      <Can do="EXECUTE" on="GATEWAYS">
        <Modal
          header="Create gateway"
          content={<GatewayCreator />}
          button={
            <button
              type="button"
              className="btn btn-primary"
              style={ { marginBottom:"-15px", marginTop:"-15px" } }
            >
            Create gateway
            </button>
          }
        />
      </Can>
      <GatewayMain />
    </div>
  );
};

export default Gateway;