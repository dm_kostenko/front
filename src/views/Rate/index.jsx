import React from "react";
import RateMain from "components/Rate/Main";
import Modal from "components/UI/Modal";
import Can from "components/Can";
import RateCreator from "components/Rate/Creator";

const Rate = () => {
  return (
    <div className="content">
      <Can do="EXECUTE" on="RATES">
        <Modal
          header="Create rate"
          content={<RateCreator />}
          button={
            <button
              type="button"
              className="btn btn-primary"
              style={ { marginBottom:"-15px", marginTop:"-15px" } }
            >
            Create rate
            </button>
          }
          dialogClassName={"rate-modal"}
        />
      </Can>
      <RateMain />
    </div>
  );
};

export default Rate;