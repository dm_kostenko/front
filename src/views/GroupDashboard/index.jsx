import React, { Component } from "react";
import { connect } from "react-redux";
import LineChartTransactionsAmount from "components/LineChart/TransactionsAmount";
import LineChartTransactionsCurrencies from "components/LineChart/TransactionsCurrencies";
import HorizontalBarChartTopMerchants from "components/HorizontalBarChart/TopMerchants";
import HorizontalBarChartTopShops from "components/HorizontalBarChart/TopShops";
import TotalProcessed from "components/TotalProcessed";
import { Row, Col, Tab, Tabs } from "react-bootstrap";
import { getAmountShopsForGroup } from "../../actions/shops";
import { getTransactionHistoryAction } from "../../actions/transactions";
import { getAmountMerchantsForGroupAction } from "../../actions/merchants";
import PropTypes from "prop-types";

class GroupDashboard extends Component {

  componentDidMount = async () => {
    await this.props.getAmountShopsForGroup();
    await this.props.getAmountMerchantsForGroupAction();
    await this.props.getTransactionHistoryAction({ days: 7 });
  }

  render() {
    return (
      <React.Fragment>
        <div className="content">
          <Row>
            <Col md={3} sm={12}>
              <div className="card-large">
                <div className="header">Amount of merchants</div>
                <div className="card-large-content">
                  {this.props.amountMerchants}
                </div>
              </div>
            </Col>

            <Col md={6} sm={12}>
              <div className="card-large">
                <LineChartTransactionsAmount />
              </div>
            </Col>

            <Col md={3} sm={12}>
              <div className="card-large">
                <div className="header">Amount of shops</div>
                <div className="card-large-content">
                  {this.props.shopsAmount}
                </div>
              </div>
            </Col>
          </Row>

          <Row>
            <Col md={3} sm={12}>
              <div className="card-large">
                <TotalProcessed classname={"card-large-content"}/>
              </div>
            </Col>

            <Col md={6} sm={12}>
              <div className="card-large">
                <LineChartTransactionsCurrencies />
              </div>
            </Col>

            <Col md={3} sm={12}>
              <div className="card-large">
                <div className="header">Total transactions</div>
                <div className="card-large-content">
                  {this.props.transactionsAmount}
                </div>
              </div>
            </Col>
          </Row>
          <Tabs defaultActiveKey="topMerchants">
          <Tab eventKey="topMerchants" title="Top merchants">
          <Row>
            <Col md={12}>
              <div className="card">
                <HorizontalBarChartTopMerchants />
              </div>
            </Col>
          </Row>
          </Tab>
          <Tab eventKey="topShops" title="Top shops">
          <Row>
            <Col md={12}>
              <div className="card">
                <HorizontalBarChartTopShops />
              </div>
            </Col>
          </Row>
          </Tab>
          </Tabs>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    shopsAmount: state.shops.count,
    transactionsAmount: state.transactions.transactionHistoryCount,
    amountMerchants: state.merchants.amountMerchants
  };
};

export default connect(mapStateToProps, { 
  getAmountShopsForGroup,
  getTransactionHistoryAction,
  getAmountMerchantsForGroupAction
})(GroupDashboard);

GroupDashboard.propTypes = {
  amountMerchants: PropTypes.number,
  getAmountShopsForGroup: PropTypes.func,
  getTransactionHistoryAction: PropTypes.func,
  getAmountMerchantsForGroupAction: PropTypes.func,
  shopsAmount:  PropTypes.array,
  transactionsAmount: PropTypes.number,
};

