import React from "react";
import GroupLogsMain from "components/Log/Group/Main";

const GroupsLogs = () => {
  return (
    <div className="content">
      <GroupLogsMain />
    </div>
  );
};

export default GroupsLogs;