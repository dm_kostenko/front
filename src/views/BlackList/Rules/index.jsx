import React from "react";
import BlackListMain from "components/BlackList/Rules/Main";
import Modal from "components/UI/Modal";
import Can from "components/Can";
import BlacklistCreator from "components/BlackList/Rules/Creator";

const BlackList = () => {
  return (
    <div className="content">
      <Can do="EXECUTE" on="BLACKLISTS">
        <Modal
          header="Create rule"
          content={<BlacklistCreator />}
          button={
            <button
              type="button"
              className="btn btn-primary"
              style={ { marginBottom:"-15px", marginTop:"-15px" } }
            >
            Create rule
            </button>
          }
        />
      </Can>
      <BlackListMain />
    </div>
  );
};

export default BlackList;