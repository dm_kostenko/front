import React from "react";
import GlobalBlackListMain from "components/BlackList/Global/Main";
import Modal from "components/UI/Modal";
import Can from "components/Can";
import GlobalBlacklistCreator from "components/BlackList/Global/Creator";

const GlobalBlackList = () => {
  return (
    <div className="content">
      <Can do="EXECUTE" on="GLOBALBLACKLIST">
        <Modal
          header="Create global item"
          content={<GlobalBlacklistCreator />}
          button={
            <button
              type="button"
              className="btn btn-primary"
              style={ { marginBottom:"-15px", marginTop:"-15px" } }
            >
            Create item
            </button>
          }
        />
      </Can>
      <GlobalBlackListMain />
    </div>
  );
};

export default GlobalBlackList;