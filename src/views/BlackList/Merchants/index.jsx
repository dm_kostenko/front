import React from "react";
import MerchantsBlackListMain from "components/BlackList/Merchants/Main";
import Modal from "components/UI/Modal";
import Can from "components/Can";
import MerchantsBlacklistCreator from "components/BlackList/Merchants/Creator";

const MerchantsBlackList = () => {
  return (
    <div className="content">
      <Can do="EXECUTE" on="MERCHANTSBLACKLIST">
        <Modal
          header="Create item"
          content={<MerchantsBlacklistCreator />}
          button={
            <button
              type="button"
              className="btn btn-primary"
              style={ { marginBottom:"-15px", marginTop:"-15px" } }
            >
            Create item
            </button>
          }
        />
      </Can>
      <MerchantsBlackListMain />
    </div>
  );
};

export default MerchantsBlackList;