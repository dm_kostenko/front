import React from "react";
import ShopMain from "components/Shop/Main";
import Modal from "components/UI/Modal";
import Can from "components/Can";
import ShopCreator from "components/Shop/Creator";

const Shop = () => {
  return (
    <div className="content">
      <Can do="EXECUTE" on="SHOPS">
        <Modal
          header="Create shop"
          content={<ShopCreator />}
          button={
            <button
              type="button"
              className="btn btn-primary"
              style={ { marginBottom:"-15px", marginTop:"-15px" } }
            >
            Create shop
            </button>
          }
        />
      </Can>
      <ShopMain />
    </div>
  );
};

export default Shop;