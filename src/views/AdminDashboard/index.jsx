import React from "react";
import { Row, Col } from "react-bootstrap";
import BarChartTransactionTypes from "components/BarChart/TransactionTypes";
import PieChartTransactionsStatusesCount from "components/PieChart/TransactionsStatusesCount";
import LineChartTransactionsAmount from "components/LineChart/TransactionsAmount";
import LineChartTransactionsCurrencies from "components/LineChart/TransactionsCurrencies";
import NewLoginsCount from "components/NewLoginsCount";
import TotalProcessed from "components/TotalProcessed";
import HorizontalBarChartTopMerchants from "components/HorizontalBarChart/TopMerchants";

export default class AdminDashboard extends React.Component {

  render() {

    return (
      <div className="content">
        <Row>
          <Col md={3}>
            <div className="card-large">
              <PieChartTransactionsStatusesCount />
            </div>
          </Col>
          <Col md={6}>
            <div className="card-large">
              <LineChartTransactionsAmount />
            </div>
          </Col>
          <Col md={3}>
            <div className="card-large">
              <NewLoginsCount/>
            </div>
          </Col>
        </Row>
        <Row>
          <Col md={3}>
            <div className="card-large">
              <BarChartTransactionTypes />
            </div>
          </Col>
          <Col md={6}>
            <div className="card-large">
              <LineChartTransactionsCurrencies />
            </div>
          </Col>
          <Col md={3}>
            <div className="card-large">
              <TotalProcessed classname={"card-large-content"}/>
            </div>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <div className="card-large">
              <HorizontalBarChartTopMerchants />
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}