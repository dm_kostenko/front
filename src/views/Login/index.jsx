import React from "react";
import LoginMain from "components/Login/Main";
import Modal from "components/UI/Modal";
import Can from "components/Can";
import LoginCreator from "components/Login/Creator";

const Login = () => {
  return (
    <div className="content"> 
      <Can do="EXECUTE" on="LOGINS">
        <Modal
          header="Create login"
          content={<LoginCreator />}
          button={
            <button
              type="button"
              className="btn btn-primary"
              style={ { marginBottom:"-15px", marginTop:"-15px" } }
            >
            Create login
            </button>
          }
        />
      </Can> 
      <LoginMain />
    </div>
  );
};

export default Login;