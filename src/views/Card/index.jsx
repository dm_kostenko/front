import React from "react";
import CardMain from "components/Card/Main";
import Modal from "components/UI/Modal";
import Can from "components/Can";
import CardCreator from "components/Card/Creator";

const Card = () => {
  return (
    <div className="content">
      <Can do="EXECUTE" on="CARDS">
        <Modal
          header="Create card"
          content={<CardCreator />}
          button={
            <button
              type="button"
              className="btn btn-primary"
              style={ { marginBottom:"-15px", marginTop:"-15px" } }
            >
            Create card
            </button>
          }
        />
      </Can>
      <CardMain />
    </div>
  );
};

export default Card;