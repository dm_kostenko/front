import React from "react";
import TransactionFormMain from "components/Transaction/Form/Main";

const TransactionForm = () => {
  return (
    <div className="content">
      <TransactionFormMain />
    </div>
  );
};

export default TransactionForm;