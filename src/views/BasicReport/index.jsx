import React from "react";
import ShopTotalsMain from "components/Report/Basic/ShopTotals";
import TotalsMain from "components/Report/Basic/Totals";
import { Panel, PanelGroup, Row, Col } from "react-bootstrap";
import CurrencyMain from "components/Report/Basic/Currency";
import TransactionTypeMain from "components/Report/Basic/TransactionType";
import PassabilityMain from "components/Report/Basic/Passability";
import OrdersMain from "components/Report/Basic/Orders";
import PieChartCardsTypes from "components/PieChart/CardTypes";
import TransactionsMain from "components/Report/Basic/TransactionTypeHistory";
import CustomSelect from "components/UI/MultiSelect";
//import DatePicker from "react-datepicker";
import CustomButton from "components/UI/Button";
import { getAllMerchants } from "actions/merchants";
import { getAllShops, getMerchantShopsAction } from "actions/shops";
import PropTypes from "prop-types";
import moment from "moment";
import { connect } from "react-redux";
//import "react-datepicker/dist/react-datepicker.css";

class ShopTotals extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      merchants: [],
      fromDate: null,
      toDate: null,
      selectedMerchant: null,
      selectedShop: null,
      data: {
        merchant_guid: null,
        shop_guid: null,
        from_date: null,
        to_date: null,
      },
      shops: []
    };
  }

  async componentDidMount() {
    await this.props.getAllMerchants();
    await this.props.getAllShops();
    this.setState({
      merchants: [...this.props.merchants, { guid: "1", name: "All merchants", label: "All merchants", value: "1" }]
    });
  }

  handleTimeChange = (event) => {
    let value = event.target.value;
    value = moment(value).format("YYYY-MM-DDTHH:mm:ss");
    this.setState({ [event.target.name]: value });
  }

  handleSelectMerchant = async option => {
    if (option.guid !== "" && option.guid !== "1") {
      await this.props.getMerchantShopsAction(option.guid);
      this.setState({
        shops: [...this.props.shops, { guid: "1", name: "All shops", label: "All shops", value: "1" }]
      });
    } else {
      await this.props.getAllShops();
      this.setState({
        shops: [...this.props.shops, { guid: "1", name: "All shops", label: "All shops", value: "1" }]
      });
    }
    this.setState({
      selectedMerchant: option.guid
    });
  };


  handleSelectShop = (option) => {
    this.setState({
      selectedShop: option.guid
    });
  };

  handleClick = () => {
    let from_date = (this.state.fromDate === "Invalid date" || this.state.fromDate === "") ? null : this.state.fromDate;
    let to_date = (this.state.toDate === "Invalid date" || this.state.toDate === "") ? null : this.state.toDate;
    let merchant_guid = (this.state.selectedMerchant === "" || this.state.selectedMerchant == 1) ? null : this.state.selectedMerchant;
    let shop_guid = (this.state.selectedShop === "" || this.state.selectedShop == 1) ? null : this.state.selectedShop;
    this.setState({
      data: {
        merchant_guid,
        shop_guid,
        from_date,
        to_date
      }
    });
  }

  render() {
    return (
      <div className="content">
        <div className="card">
          <Row style={{ marginLeft: "15px" }} >
            <Col md={3} sm={3} xs={3}>
              <CustomSelect
                multi={false}
                name="selectedMerchant"
                options={this.state.merchants}
                value="All merchants"
                defaultValue={{ label: "All merchants", value: "1" }}
                onSelect={this.handleSelectMerchant}
              />
            </Col>
            <Col md={3} sm={3} xs={3}>
              <CustomSelect
                multi={false}
                name="selectedShop"
                options={this.state.shops}
                value="All shops"
                defaultValue={{ label: "All shops", value: "1" }}
                onSelect={this.handleSelectShop}
              />
            </Col>
            <Col md={2} sm={2} xs={2}>
              <label style={{ marginRight: "5px" }}>from</label>
              <input
                type="date"
                className="datetimepicker"
                name="fromDate"
                onChange={this.handleTimeChange} >
              </input>
            </Col>
            <Col md={2} sm={2} xs={2}>
              <label style={{ marginRight: "5px" }}>to</label>
              <input
                type="date"
                className="datetimepicker"
                name="toDate"
                onChange={this.handleTimeChange} >
              </input>
            </Col>
            <Col md={2} sm={2} xs={2}>
              <CustomButton className="btn" onClick={this.handleClick}>Find</CustomButton>
            </Col>
          </Row>
        </div>
        <div className="card-large">
          <Row>
            {/* <Col md={1} sm={1} xs={1} /> */}
            <Col md={12} sm={12} xs={12}>
              <TransactionsMain data={this.state.data} />
            </Col>
            {/* <Col md={1} sm={1} xs={1} /> */}
          </Row>
        </div>
        <Row>
          <Col md={3} sm={3} xs={3}>
            <div className="card-large">
              <div className="header">Passability</div>
              <PassabilityMain params={this.state.data} />
            </div>
          </Col>
          <Col md={3} sm={3} xs={3}>
            <div className="card-large">
              <div className="header">Currency</div>
              <CurrencyMain params={this.state.data} />
            </div>
          </Col>
          <Col md={3} sm={3} xs={3}>
            <div className="card-large">
              <div className="header">Transactions Type</div>
              <TransactionTypeMain params={this.state.data} />
            </div>
          </Col>
          <Col md={3} sm={3} xs={3}>
            <div className="card-large">
              <div className="header">Card type</div>
              <PieChartCardsTypes />
            </div>
          </Col>
        </Row>
        <Row>
          <Col md={12} sm={12} xs={12}>
            <PanelGroup
              id="accordion-controlled"
              defaultActiveKey="3"
            >
              <Panel eventKey="1" >
                <Panel.Heading>
                  <Panel.Title toggle>Totals<b className="caret"></b></Panel.Title>
                </Panel.Heading>
                <Panel.Body collapsible><TotalsMain merchants={this.state.merchants} /></Panel.Body>
              </Panel>
              <Panel eventKey="2">
                <Panel.Heading>
                  <Panel.Title toggle>Shop Totals<b className="caret"></b></Panel.Title>
                </Panel.Heading>
                <Panel.Body collapsible><ShopTotalsMain /></Panel.Body>
              </Panel>
              <Panel eventKey="3">
                <Panel.Heading>
                  <Panel.Title toggle>Orders <b className="caret"></b></Panel.Title>
                </Panel.Heading>
                <Panel.Body collapsible><OrdersMain /></Panel.Body>
              </Panel>
            </PanelGroup>
          </Col>
        </Row>
      </div>
    );
  }
}




const mapStateToProps = (state) => {
  return {
    merchants: state.merchants.merchantsList,
    shops: state.shops.shopsList
  };
};

export default connect(mapStateToProps, {
  getAllMerchants,
  getMerchantShopsAction,
  getAllShops
})(ShopTotals);

ShopTotals.propTypes = {
  merchants: PropTypes.array,
  shops: PropTypes.array,
  getAllMerchants: PropTypes.func,
  getAllShops: PropTypes.func,
  getMerchantShopsAction: PropTypes.func
};