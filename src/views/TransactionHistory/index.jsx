import React from "react";
import TransactionHistoryMain from "components/Transaction/History/Main";

const TransactionHistory = () => {
  return (
    <div className="content">
      <TransactionHistoryMain />
    </div>
  );
};

export default TransactionHistory;