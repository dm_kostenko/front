import React from "react";
import MerchantLogsMain from "components/Log/Merchant/Main";

const MerchantsLogs = () => {
  return (
    <div className="content">
      <MerchantLogsMain />
    </div>
  );
};

export default MerchantsLogs;