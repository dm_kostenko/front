import React from "react";
import CurrencyMain from "components/Currency/Main";
import Modal from "components/UI/Modal";
import Can from "components/Can";
import CurrencyCreator from "components/Currency/Creator";

const Currency = () => {
  return (
    <div className="content">
      <Can do="EXECUTE" on="CURRENCIES">
        <Modal
          header="Create currency"
          content={<CurrencyCreator />}
          button={
            <button
              type="button"
              className="btn btn-primary"
              style={ { marginBottom:"-15px", marginTop:"-15px" } }
            >
            Create currency
            </button>
          }
        />
      </Can>
      <CurrencyMain />
    </div>
  );
};

export default Currency;