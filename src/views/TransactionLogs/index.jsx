import React from "react";
import TransactionLogsMain from "components/Log/Transaction/Main";

const TransactionsLogs = () => {
  return (
    <div className="content">
      <TransactionLogsMain />
    </div>
  );
};

export default TransactionsLogs;