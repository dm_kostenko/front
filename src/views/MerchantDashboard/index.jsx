import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Tabs, Tab } from "react-bootstrap";
import PieChartTransactionsStatusesCount from "components/PieChart/TransactionsStatusesCount";
import PieChartShopTransactionsStatusesCount from "components/PieChart/ShopTransactionsStatusesCount";
import LineChartTransactionsAmount from "components/LineChart/TransactionsAmount";
import LineChartShopTransactionsAmount from "components/LineChart/ShopTransactionsAmount";
import TotalProcessed from "components/TotalProcessed";
import ShopTotalProcessed from "components/ShopTotalProcessed";
import { getAllShops } from "../../actions/shops";
import PropTypes from "prop-types";

class MerchantDashboard extends Component {

  componentDidMount = async () => {
    await this.props.getAllShops();
  }

  render() {
    return (
      <div className="content">
        <Row>
          <h2 style={{ margin: "10px 0 5px 20px" }}>Total</h2>
        </Row>
        <Row>
          <Col md={3} sm={12}>
            <div className="card-large">
              <PieChartTransactionsStatusesCount />
            </div>
          </Col>
          <Col md={6} sm={12}>
            <div className="card-large">
              <LineChartTransactionsAmount />
            </div>
          </Col>
          <Col md={3} sm={12}>
            <div className="card-small">
              <TotalProcessed classname={"card-small-content"}/>
            </div>
            <div className="card-small"
              style={{
                pointerEvents: "none",
                opacity: "0.5",
                background: "#CCC"
              }}>
              <div className="header">Holds</div>
              <div className="card-small-content">
                $100
              </div>
            </div>
          </Col>
        </Row>
        {this.props.count !== 0 ?
          <React.Fragment>
            <Row>
              <h2 style={{ margin: "10px 0 5px 20px" }}>Shops</h2>
            </Row>
            <Row>
              <Tabs defaultActiveKey={0} transition={false} style={{ marginLeft: "23px" }}>
                <br />
                {this.props.shops.map((shop, index) => {
                  return <Tab eventKey={index} key={shop.name} title={shop.name} mountOnEnter={true} style={{ position: "inline-block" }}>
                    <Col md={3} sm={12} style={{ paddingLeft: "0", paddingRight: "30px" }}>
                      <div className="card-large" style={{ marginLeft: "0" }}>
                        <div className="header">Transactions statuses</div>
                        <PieChartShopTransactionsStatusesCount shopGuid={shop.guid} />
                      </div>
                    </Col>
                    <Col md={6} sm={12} style={{ paddingLeft: "0", paddingRight: "20px" }}>
                      <div className="card-large" >
                        <div className="header">Amount of transactions</div>
                        <LineChartShopTransactionsAmount shopGuid={shop.guid} />
                      </div>
                    </Col>
                    <Col md={3} sm={12} style={{ paddingLeft: "10px" }}>
                      <div className="card-small">
                        <ShopTotalProcessed classname={"card-small-content"} shopGuid={shop.guid}/>
                      </div>
                      <div className="card-small"
                        style={{
                          pointerEvents: "none",
                          opacity: "0.5",
                          background: "#CCC"
                        }}>
                        <div className="header">Holds</div>
                        <div className="card-small-content">
                          $100
                        </div>
                      </div>
                    </Col>
                  </Tab>;
                })}
              </Tabs>
            </Row>
          </React.Fragment>
          : <Row>
            <h2 style={{ margin: "10px 0 5px 20px" }}>There are no shops</h2>
          </Row>}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    shops: state.shops.shopsList,
    count: state.shops.count
  };
};

export default connect(mapStateToProps, {
  getAllShops
})(MerchantDashboard);

MerchantDashboard.propTypes = {
  count: PropTypes.number,
  shops: PropTypes.array,
  getAllShops: PropTypes.func,
};