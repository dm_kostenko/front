import React from "react";
import GroupMain from "components/Group/Main";
import Modal from "components/UI/Modal";
import Can from "components/Can";
import GroupCreator from "components/Group/Creator";

const Group = () => {
  return (
    <div className="content">
      <Can do="EXECUTE" on="GROUPS">
        <Modal
          button={
            <button
              type="button"
              className="btn btn-primary"
              style={{ marginBottom: "-15px", marginTop: "-15px" }}
            >
              Create group
            </button>
          }
          content={<GroupCreator />}
          header="Create group"
        />
      </Can>
      <GroupMain />
    </div>
  );
};

export default Group;