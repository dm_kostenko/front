import React from "react";
import TransactionTemplateMain from "components/Transaction/Template/Main";

const TransactionTemplate = () => {
  return (
    <div className="content">
      <TransactionTemplateMain />
    </div>
  );
};

export default TransactionTemplate;