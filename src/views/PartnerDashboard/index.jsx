import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Tab, Tabs } from "react-bootstrap";
import { getAmountGroupsForPartner } from "../../actions/groups";
import { getAmountMerchantsForPartner } from "../../actions/merchants";
import { getAmountShopsForPartner } from "../../actions/shops";
import { getTransactionHistoryAction } from "../../actions/transactions";
import LineChartTransactionsAmount from "components/LineChart/TransactionsAmount";
import LineChartTransactionsCurrencies from "components/LineChart/TransactionsCurrencies";
import HorizontalBarChartTopMerchants from "components/HorizontalBarChart/TopMerchants";
import HorizontalBarChartTopGroups from "components/HorizontalBarChart/TopGroups";
import HorizontalBarChartTopShops from "components/HorizontalBarChart/TopShops";
import TotalProcessed from "components/TotalProcessed";
import PropTypes from "prop-types";

class PartnerDashboard extends Component {

  componentDidMount = () => {
    this.props.getAmountGroupsForPartner();
    this.props.getAmountMerchantsForPartner();
    this.props.getAmountShopsForPartner();
    this.props.getTransactionHistoryAction( { days: 7 });
  }

  render() {
    return (
      <React.Fragment>
        <div className="content">
          <Row>
            <Col md={4}>
              <div className="card" style={{ textAlign: "center" }}>
                <div className="header">Amount of groups</div>
                <div className="content">
                  <p style={{ fontSize: "3em" }}>{this.props.groupsAmount}</p>
                </div>
              </div>
            </Col>
            <Col md={4}>
              <div className="card" style={{ textAlign: "center" }}>
                <div className="header">Amount of merchants</div>
                <div className="content">
                  <p style={{ fontSize: "3em" }}>{this.props.merchantsAmount}</p>
                </div>
              </div>
            </Col>
            <Col md={4}>
              <div className="card" style={{ textAlign: "center" }}>
                <div className="header">Amount of shops</div>
                <div className="content">
                  <p style={{ fontSize: "3em" }}>{this.props.shopsAmount}</p>
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <div className="card"
                style={{
                  textAlign: "center"
                }}>
                <TotalProcessed classname={"card-small-content"}/>
              </div>
            </Col>
            <Col md={6}>
              <div className="card" style={{ textAlign: "center" }}>
                <div className="header">Total transactions</div>
                <div className="content">
                  <p style={{ fontSize: "3em" }}>{this.props.transactionsAmount}</p>
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <div className="card-large">
                <LineChartTransactionsAmount />
              </div>
            </Col>
            <Col md={6}>
              <div className="card-large">
                <LineChartTransactionsCurrencies/>
              </div>
            </Col>
          </Row>
          <Tabs defaultActiveKey="topMerchants">
          <Tab eventKey="topMerchants" title="Top merchants">
            <Row>
              <Col md={12}>
                <div className="card">
                  <div className="content">
                    <HorizontalBarChartTopMerchants />
                  </div>
                </div>
              </Col>
            </Row>
          </Tab>
          <Tab eventKey="topGroups" title="Top groups">
          <Row>
            <Col md={12}>
              <div className="card">
                <div className="content">
                  <HorizontalBarChartTopGroups />
                </div>
              </div>
            </Col>
          </Row>
          </Tab>
          <Tab eventKey="topShops" title="Top shops">
          <Row>
            <Col md={12}>
              <div className="card">
                <div className="content">
                  <HorizontalBarChartTopShops />
                </div>
              </div>
            </Col>
          </Row>
          </Tab>
          </Tabs>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    merchantsAmount: state.merchants.count,
    shopsAmount: state.shops.count,
    groupsAmount: state.groups.count,
    transactionsAmount: state.transactions.transactionHistoryCount
  };
};

export default connect(mapStateToProps, {
  getAmountGroupsForPartner,
  getAmountMerchantsForPartner,
  getAmountShopsForPartner,
  getTransactionHistoryAction
})(PartnerDashboard);

PartnerDashboard.propTypes = {
  merchantsCount: PropTypes.array,
  shopsCount: PropTypes.array,
  groupsCount: PropTypes.array,
  getCountShopsForPartner: PropTypes.func,
  getCountMerchantsForPartner: PropTypes.func,
  getCountGroupsForPartner: PropTypes.func,
  transactionsAmount: PropTypes.number,
  merchantsAmount: PropTypes.number,
  shopsAmount: PropTypes.number,
  groupsAmount: PropTypes.number,

  getAmountGroupsForPartner: PropTypes.func,
  getAmountMerchantsForPartner: PropTypes.func,
  getAmountShopsForPartner: PropTypes.func,
  getTransactionHistoryAction: PropTypes.func,
};