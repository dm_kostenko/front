import React from "react";
import AccountMain from "components/Account/Main";
import Modal from "components/UI/Modal";
import Can from "components/Can";
import AccountCreator from "components/Account/Creator";

const Account = () => {
  return (
    <div className="content">
      <Can do="EXECUTE" on="ACCOUNTS">
        <Modal
          header="Create account"
          content={<AccountCreator />}
          button={
            <button
              type="button"
              className="btn btn-primary"
              style={ { marginBottom:"-15px", marginTop:"-15px" } }
            >
            Create account
            </button>
          }
        />
      </Can>
      <AccountMain />
    </div>
  );
};

export default Account;