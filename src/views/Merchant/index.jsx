import React from "react";
import MerchantMain from "components/Merchant/Main";
import Modal from "components/UI/Modal";
import Can from "components/Can";
import MerchantCreator from "components/Merchant/Creator";

const Merchant = () => {
  return (
    <div className="content">
      <Can do="EXECUTE" on="MERCHANTS">
        <Modal
          button={
            <button
              type="button"
              className="btn btn-primary"
              style={{ marginBottom: "-15px", marginTop: "-15px" }}
            >
              Create merchant
            </button>
          }
          content={<MerchantCreator />}
          header="Create merchant"
        />
      </Can>
      <MerchantMain />
    </div>
  );
};

export default Merchant;