import React from "react";
import PartnerLogsMain from "components/Log/Partner/Main";

const PartnersLogs = () => {
  return (
    <div className="content">
      <PartnerLogsMain />
    </div>
  );
};

export default PartnersLogs;