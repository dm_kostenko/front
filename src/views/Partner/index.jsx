import React from "react";
import PartnerMain from "components/Partner/Main";
import Modal from "components/UI/Modal";
import Can from "components/Can";
import PartnerCreator from "components/Partner/Creator";

const Partner = () => {
  return (
    <div className="content">
      <Can do="EXECUTE" on="PARTNERS">
        <Modal
          button={
            <button
              type="button"
              className="btn btn-primary"
              style={{ marginBottom: "-15px", marginTop: "-15px" }}
            >
              Create partner
            </button>
          }
          content={<PartnerCreator />}
          header="Create partner"
        />
      </Can>
      <PartnerMain />
    </div>
  );
};

export default Partner;