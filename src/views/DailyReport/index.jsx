import React, { Component } from "react";
import { Row, Col, Tab, Tabs } from "react-bootstrap";
import CustomSelect from "components/UI/MultiSelect";
import CustomButton from "components/UI/Button";
import HorizontalBarChartTopMerchantsDaily from "components/Report/Daily/TopMerchantsDaily";
import HorizontalBarChartTopGroups from "components/HorizontalBarChart/TopGroups";
import HorizontalBarChartTopPartners from "components/HorizontalBarChart/TopPartners";
import HorizontalBarChartTopShops from "components/HorizontalBarChart/TopShops";
import DailyCardsTypes from "components/Report/Daily/CardTypes";
import DailyCurrencies from "components/Report/Daily/Currencies";
import DailyTransactionsStatuses from "components/Report/Daily/TransactionsStatuses";
import DailyTransactionsTypes from "components/Report/Daily/TransactionsTypes";
import DailyTransactionsHistory from "components/Report/Daily/TransactionsHistory";
import { getAllMerchants } from "actions/merchants";
import { getAllShops, getMerchantShopsAction } from "actions/shops";
import { getTransactionHistoryAction, getTotalProcessedAction } from "actions/transactions";
import { connect } from "react-redux";
import PropTypes from "prop-types";

class DailyReport extends Component {
  state = {
    merchants: [],
    shops: [],
    selectedMerchant: null,
    selectedShop: null,
    data: {
      merchant_guid: null,
      shop_guid: null
    }
  };

  async componentDidMount() {
    await this.props.getAllMerchants();
    await this.props.getAllShops();
    await this.props.getTransactionHistoryAction({ days: 1 });
    await this.props.getTotalProcessedAction({ days: 1 });
    this.setState({
      shops: [...this.props.shops, { guid: "1", name: "All shops", label: "All shops", value: "1" }],
      merchants: [...this.props.merchants, { guid: "1", name: "All merchants", label: "All merchants", value: "1" }]
    });
  }

  handleSelectMerchant = async option => {
    if (option.guid !== "" && option.guid !== "1") {
      await this.props.getMerchantShopsAction(option.guid);
    } else {
      this.props.getAllShops();
    }
    this.setState({
      selectedMerchant: option.guid
    });
  };

  handleSelectShop = option => {
    this.setState({
      selectedShop: option.guid
    });
  };

  handleClick = () => {
    let merchant_guid =
      (this.state.selectedMerchant === "" || this.state.selectedMerchant == 1) ? null : this.state.selectedMerchant;
    let shop_guid =
      (this.state.selectedShop === "" || this.state.selectedShop == 1) ? null : this.state.selectedShop;
    this.setState({
      data: {
        merchant_guid,
        shop_guid
      }
    });
  };

  render() {
    const shopsForSelect = [ ...this.props.shops, { guid: "1", name: "All shops", label: "All shops", value: "1" } ];
    return (
      <div className="content">
        <div className="card">
          <Row style={{ marginLeft: "15px" }}>
            <Col md={3} sm={3} xs={3}>
              <CustomSelect
                multi={false}
                name="selectedMerchant"
                options={this.state.merchants}
                value="All merchants"
                defaultValue={{ label: "All merchants", value: "1" }}
                onSelect={this.handleSelectMerchant}
              />
            </Col>
            <Col md={3} sm={3} xs={3}>
              <CustomSelect
                multi={false}
                name="selectedShop"
                options={shopsForSelect}
                value="All shops"
                defaultValue={{ label: "All shops", value: "1" }}
                onSelect={this.handleSelectShop}
              />
            </Col>

            <Col md={3} sm={3} xs={3} >
              <CustomButton className="btn" onClick={this.handleClick} style={{marginRight:"90%"}}>Find</CustomButton>
            </Col>
          </Row>
        </div>
        <div className="card-large">
          <DailyTransactionsHistory data={this.state.data} />
        </div>
        <Row>
          <Col md={3} sm={3} xs={3}>
            <div className="card-large">
              <div className="header">Passability</div>
              <DailyTransactionsStatuses />
            </div>
          </Col>
          <Col md={3} sm={3} xs={3}>
            <div className="card-large">
              <div className="header">Currency</div>
              <DailyCurrencies params={this.state.data} />
            </div>
          </Col>
          <Col md={3} sm={3} xs={3}>
            <div className="card-large">
              <div className="header">Transactions type</div>
              <DailyTransactionsTypes />
            </div>
          </Col>
          <Col md={3} sm={3} xs={3}>
            <div className="card-large">
              <div className="header">Card type</div>
              <DailyCardsTypes />
            </div>
          </Col>
        </Row>
        <Tabs defaultActiveKey="topMerchants" style={{ marginLeft:"9px" }}>
          <Tab eventKey="topMerchants" title="Top merchants" >
            <Row>
              <Col md={12} sm={12} xs={12}>
                <div className="card-large" style={{ marginLeft: "1px" }}>
                  <div className="header">Top 10 merchants</div>
                  <HorizontalBarChartTopMerchantsDaily />
                </div>
              </Col>
            </Row>
          </Tab>
          <Tab eventKey="topGroups" title="Top groups">
            <Row>
              <Col md={12}>
                <div className="card" style={{ marginLeft: "1px" }}>
                  <div className="content">
                    <HorizontalBarChartTopGroups />
                  </div>
                </div>
              </Col>
            </Row>
          </Tab>
          <Tab eventKey="topPartners" title="Top partners">
            <Row>
              <Col md={12}>
                <div className="card" style={{ marginLeft: "1px" }}>
                  <div className="content">
                    <HorizontalBarChartTopPartners />
                  </div>
                </div>
              </Col>
            </Row>
          </Tab>
          <Tab eventKey="topShops" title="Top shops">
            <Row>
              <Col md={12}>
                <div className="card" style={{ marginLeft: "1px" }}>
                  <div className="content">
                    <HorizontalBarChartTopShops />
                  </div>
                </div>
              </Col>
            </Row>
          </Tab>
        </Tabs>
        <Row>
          <Col md={3} sm={3} xs={3} />
          <Col md={3} sm={3} xs={3}>
            <div
              className="card-large">
              <div className="header">Count transactions</div>
              <div className="card-large-content">
                {this.props.transactionsAmount}
              </div>
            </div>
          </Col>
          <Col md={3} sm={3} xs={3}>
            <div
              className="card-large">
              <div className="header">Amount of money processed</div>
              <div className="card-large-content">$ {this.props.totalProcessed ? this.props.totalProcessed : 0}</div>
            </div>
          </Col>
          <Col md={3} sm={3} xs={3} />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    merchants: state.merchants.merchantsList,
    transactionsAmount: state.transactions.transactionHistoryCount,
    totalProcessed: state.transactions.totalProcessed,
    shops: state.shops.shopsList
  };
};

export default connect(mapStateToProps, {
  getAllMerchants,
  getAllShops,
  getMerchantShopsAction,
  getTransactionHistoryAction,
  getTotalProcessedAction
})(DailyReport);

DailyReport.propTypes = {
  getAllMerchants: PropTypes.func,
  getTotalProcessedAction: PropTypes.func,
  getTransactionHistoryAction: PropTypes.func,
  shops: PropTypes.array,
  getAllShops: PropTypes.func,
  merchants: PropTypes.array,
  transactionsAmount: PropTypes.number,
  totalProcessed: PropTypes.number,
  getMerchantShopsAction: PropTypes.func
};
