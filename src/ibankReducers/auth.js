import {
  LOG_IN_SUCCESS,
  LOG_IN_FAILED,
  CHANGE_LOGIN_SUCCESS,
  CHANGE_LOGIN_FAILED,
  LOG_OUT,
  GET_QR_CODE,
  SEND_OTP,
  DISABLE_TFA,
  GET_EMAIL_MESSAGE,
  SET_NEW_PASSWORD,
  CHECK_RECOVERY_TOKEN
} from "../ibankConstants/actionTypes";

const initialState = {
  response: {},
  error: "",

  changeResponse: {},
  changeError: "",

  otpResponse: {},

  qrCode: "",
  salt: "",

  disableTFAResponse: {},

  logOut: false,

  status: "",
  statusPassword: "",
  statusToken: ""
};

export default function auth(state = initialState, action) {
  switch(action.type) {
  case LOG_IN_SUCCESS:
    return { 
      ...state, 
      response: action.data,
      error: ""
    };
  case LOG_IN_FAILED:
    let errorMessage = action.error.response ? action.error.response.data.error : "Server error";
    return { 
      ...state, 
      error: errorMessage,
      response: ""
    };
  case CHANGE_LOGIN_SUCCESS:
    return { 
      ...state, 
      changeResponse: action.data,
      changeError: ""
    };
  case CHANGE_LOGIN_FAILED:
    errorMessage = action.error.response ? action.error.response.data.error : "Server error";
    return { 
      ...state, 
      changeError: errorMessage,
      changeResponse: ""
    };
  case LOG_OUT:
    return {
      ...state,
      logOut: true
    };
  case GET_QR_CODE:
    return {
      ...state,
      qrCode: action.payload.otpAuth,
      salt: action.payload.otpSalt
    };
  case SEND_OTP:
    return {
      ...state,
      otpResponse: action.payload
    };
  case DISABLE_TFA:
    return {
      ...state,
      disableTFAResponse: action.payload
    };     
  case GET_EMAIL_MESSAGE:
    return {
      ...state,
      status: action.payload
    } 
  case SET_NEW_PASSWORD:
    return {
      ...state,
      statusPassword: action.payload
    }
  case CHECK_RECOVERY_TOKEN:
    return {
      ...state,
      statusToken: action.payload
    }
  default:
    return state;
  }
}