import {
  GET_USERS,
  GET_USERS_FAILED,
  GET_USERS_SUCCEED,
  GET_ENTITIES,
  GET_ENTITIES_FAILED,
  GET_ENTITIES_SUCCEED,
  GET_USER,
  GET_USER_FAILED,
  GET_USER_SUCCEED,
  GET_USER_INFO,
  GET_USER_INFO_FAILED,
  GET_USER_INFO_SUCCEED,
  CREATE_USER,
  CHANGE_PASSWORD
} from "ibankConstants/actionTypes";

const initialState = {
  users: [],
  count: 0,
  loading: true,
  error: false,
  pageSize: 10,
  currentPage: 1,

  entities: [],
  entitiesLoading: true,
  entitiesError: false,

  user: {},
  loadingDetail: true,
  errorDetail: false,

  profile: {},
  loadingProfile: true,
  errorProfile: false
};

export default function users(state = initialState, action) {
  switch (action.type) {
  case GET_USERS:
    return {
      ...state,
      loading: true,
      error: false
    };
  case GET_USERS_SUCCEED: {
    return {
      ...state,
      users: action.payload.data,
      count: action.payload.count,
      pageSize: action.pageSize,
      currentPage: action.currentPage,
      loading: false,
      error: false
    };
  }
  case GET_USERS_FAILED:
    return {
      ...state,
      loading: false,
      error: true
    };
  case GET_ENTITIES:
    return {
      ...state,
      entitiesLoading: true,
      entitiesError: false
    };
  case GET_ENTITIES_SUCCEED: {
    return {
      ...state,
      entities: action.payload.data,
      entitiesLoading: false,
      entitiesError: false
    };
  }
  case GET_ENTITIES_FAILED:
    return {
      ...state,
      entitiesLoading: false,
      entitiesError: true
    };
  case GET_USER:
    return {
      ...state,
      loadingDetail: true,
      errorDetail: false
    };
  case GET_USER_SUCCEED: {
    return {
      ...state,
      user: action.payload,
      loadingDetail: false,
      errorDetail: false
    };
  }
  case GET_USER_FAILED:
    return {
      ...state,
      loadingProfile: false,
      errorProfile: true
    };
  case GET_USER_INFO:
    return {
      ...state,
      loadingProfile: true,
      errorProfile: false
    };
  case GET_USER_INFO_SUCCEED: {
    return {
      ...state,
      profile: action.payload,
      loadingProfile: false,
      errorProfile: false
    };
  }
  case GET_USER_INFO_FAILED:
    return {
      ...state,
      loadingDetail: false,
      errorDetail: true
    };
  case CREATE_USER:
    return {
      ...state,
      responseUser: action.payload
    };  
  case CHANGE_PASSWORD:
    return {
      ...state,
      responseChangePassword: action.payload
    };
  default:
    return state;
  }
}