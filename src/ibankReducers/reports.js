import {
  GET_TRANSACTION_HISTORY,
  GET_TRANSACTION_HISTORY_SUCCEED,
  GET_TRANSACTION_HISTORY_FAILED,
  GET_AMOUNT_OF_TRANSACTIONS,
  GET_AMOUNT_OF_TRANSACTIONS_SUCCEED,
  GET_AMOUNT_OF_TRANSACTIONS_FAILED,
  GET_TRANSACTION_TYPES,
  GET_TRANSACTION_TYPES_SUCCEED,
  GET_TRANSACTION_TYPES_FAILED
} from "ibankConstants/actionTypes";

const initialState = {
  transactionHistory: [],
  transactionHistoryCount: 0,
  transactionHistoryLoading: false,
  transactionHistoryError: false,

  amountOfTransactions: [],
  amountOfTransactionsCount: 0,
  amountOfTransactionsLoading: false,
  amountOfTransactionsError: false,
  
  transactionTypes: [],
  transactionTypesLoading: false,
  transactionTypesError: false

};

export default function reports(state = initialState, action) {
  switch(action.type) {
  case GET_TRANSACTION_HISTORY:
    return {
      ...state,
      transactionHistoryLoading: true,
      transactionHistoryError: false
    };
  case GET_TRANSACTION_HISTORY_SUCCEED: {
    return {
      ...state,
      transactionHistory: action.payload.data,
      transactionHistoryCount: action.payload.count,
      transactionHistoryLoading: false,
      transactionHistoryError: false
    };
  }
  case GET_TRANSACTION_HISTORY_FAILED:
    return {
      ...state,
      transactionHistoryLoading: false,
      transactionHistoryError: true
    };
  case GET_AMOUNT_OF_TRANSACTIONS:
    return {
      ...state,
      amountOfTransactionsLoading: true,
      amountOfTransactionsError: false
    };
  case GET_AMOUNT_OF_TRANSACTIONS_SUCCEED: {
    return {
      ...state,
      amountOfTransactions: action.payload.data,
      amountOfTransactionsCount: action.payload.count,
      amountOfTransactionsLoading: false,
      amountOfTransactionsError: false
    };
  }
  case GET_AMOUNT_OF_TRANSACTIONS_FAILED:
    return {
      ...state,
      amountOfTransactionsLoading: false,
      amountOfTransactionsError: true
    };
  case GET_TRANSACTION_TYPES:
    return {
      ...state,
      transactionTypesLoading: true,
      transactionTypesError: false
    };
  case GET_TRANSACTION_TYPES_SUCCEED: {
    return {
      ...state,
      transactionTypes: action.payload.report,
      transactionTypesLoading: false,
      transactionTypesError: false
    };
  }
  case GET_TRANSACTION_TYPES_FAILED:
    return {
      ...state,
      transactionTypesLoading: false,
      transactionTypesError: true
    };
  default: return state;
  }
}
