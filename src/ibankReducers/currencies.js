import {
  GET_CURRENCIES,
  GET_CURRENCIES_FAILED,
  GET_CURRENCIES_SUCCEED,
  GET_CURRENCIES_RATES,
  GET_CURRENCIES_RATES_SUCCEED,
  GET_CURRENCIES_RATES_FAILED
} from "ibankConstants/actionTypes";

const initialState = {
  currencies: [],
  loading: false,
  error: false,
  courses: [],
  courseLoading: false,
  courseError: false
};

export default function currencies(state = initialState, action) {
  switch (action.type) {
  case GET_CURRENCIES:
    return {
      ...state,
      loading: true,
      error: false
    };
  case GET_CURRENCIES_SUCCEED: {
    return {
      ...state,
      currencies: action.payload.data,
      count: action.payload.count,
      loading: false,
      error: false
    };
  }
  case GET_CURRENCIES_FAILED:
    return {
      ...state,
      loading: false,
      error: true
    };
  case GET_CURRENCIES_RATES:
    return {
      ...state,
      loading: true,
      error: false
    };
  case GET_CURRENCIES_RATES_SUCCEED: {
    return {
      ...state,
      courses: action.payload.data,
      courseLoading: false,
      courseError: false
    };
  }
  case GET_CURRENCIES_RATES_FAILED:
    return {
      ...state,
      courseLoading: false,
      courseError: true
    };
  default:
    return state;
  }
}