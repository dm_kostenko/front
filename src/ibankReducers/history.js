import {
  PUSH_LOCATION,
  POP_LOCATION
} from "ibankConstants/actionTypes";

const initialState = {
  history: [],

};

export default function history(state = initialState, action) {
  switch(action.type) {
  case PUSH_LOCATION:
    let { history } = state;
    history.push(action.payload);
    return {
      ...state,
      history
    };
  case POP_LOCATION:
    history = state.history;
    history.pop();
    return {
      ...state,
      history
    };
  default: return state;
  }
}