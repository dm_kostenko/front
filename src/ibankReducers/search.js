import {
  INVERSE_SEARCH,
  RESET_SEARCH,
  INPUT_SEARCH_IN_USERS,
  INPUT_SEARCH_IN_ACCOUNTS,
  INPUT_SEARCH_IN_TX_RULES,
  INPUT_SEARCH_IN_TRANSACTIONS_FFCCTRNS,
  INPUT_SEARCH_IN_TRANSACTIONS_FFPSRPRT,
  INPUT_SEARCH_IN_TRANSACTIONS_FFPCRQST,
  INPUT_SEARCH_IN_TRANSACTIONS_INTERNAL,
  INPUT_SEARCH_IN_TRANSACTIONS_PRTRN,
  INPUT_SEARCH_IN_TRANSACTIONS_ROINVSTG,
  INPUT_SEARCH_IN_TRANSACTIONS_ALL,
  INPUT_SEARCH_IN_INBOX,
  INPUT_SEARCH_IN_RATES,
  SET_CURRENT_SEARCH_DATA
} from "ibankConstants/actionTypes";

const initialState = {
  isSearch: false,
  reset: false,

  usersSearch: {},
  accountsSearch: {},
  txsALLSearch: {},
  txsFFCCTRNSSearch: {},
  txsFFPSRPRTSearch: {},
  txsFFPCRQSTSearch: {},
  txsPRTRNSearch: {},
  txsROINVSTGSearch: {},
  txsINTERNALSearch: {},
  inboxSearch: {},
  currentSearchData: {}
};

export default function search(state = initialState, action) {
  switch(action.type) {
  case INVERSE_SEARCH:
    return {
      ...state,
      isSearch: action.data
    };
  case RESET_SEARCH:
    return {
      ...initialState,
      reset: true,
      isSearch: true
    };
  case INPUT_SEARCH_IN_USERS:
    return {
      ...state,
      reset: false,
      usersSearch: {
        ...state.usersSearch,
        ...action.data
      }
    };
  case INPUT_SEARCH_IN_RATES:
    return {
      ...state,
      reset: false,
      ratesSearch: {
        ...state.ratesSearch,
        ...action.data
      }
    };
  case INPUT_SEARCH_IN_INBOX:
    return {
      ...state,
      reset: false,
      inboxSearch: {
        ...state.inboxSearch,
        ...action.data
      }
    };
  case INPUT_SEARCH_IN_ACCOUNTS:
    return {
      ...state,
      reset: false,
      accountsSearch: {
        ...state.accountsSearch,
        ...action.data
      }
    };
  case INPUT_SEARCH_IN_TX_RULES:
    return {
      ...state,
      reset: false,
      txRulesSearch: {
        ...state.txRulesSearch,
        ...action.data
      }
    };
  case INPUT_SEARCH_IN_TRANSACTIONS_ALL:
    return { 
      ...state,
      reset: false,
      txsALLSearch: {
        ...state.txsALLSearch,
        ...action.data
      }
    };    
  case INPUT_SEARCH_IN_TRANSACTIONS_FFCCTRNS:
    return { 
      ...state,
      reset: false,
      txsFFCCTRNSSearch: {
        ...state.txsFFCCTRNSSearch,
        ...action.data
      }
    };
  case INPUT_SEARCH_IN_TRANSACTIONS_FFPSRPRT:
    return { 
      ...state,
      reset: false,
      txsFFPSRPRTSearch: {
        ...state.txsFFPSRPRTSearch,
        ...action.data
      }
    };  
  case INPUT_SEARCH_IN_TRANSACTIONS_FFPCRQST:
    return { 
      ...state,
      reset: false,
      txsFFPCRQSTSearch: {
        ...state.txsFFPCRQSTSearch,
        ...action.data
      }
    }; 
  case INPUT_SEARCH_IN_TRANSACTIONS_PRTRN:
    return { 
      ...state,
      reset: false,
      txsPRTRNSearch: {
        ...state.txsPRTRNSearch,
        ...action.data
      }
    };
  case INPUT_SEARCH_IN_TRANSACTIONS_INTERNAL:
    return { 
      ...state,
      reset: false,
      txsINTERNALSearch: {
        ...state.txsINTERNALSearch,
        ...action.data
      }
    };
  case INPUT_SEARCH_IN_TRANSACTIONS_ROINVSTG:
    return { 
      ...state,
      reset: false,
      txsROINVSTGSearch: {
        ...state.txsROINVSTGSearch,
        ...action.data
      }
    };                          
  case SET_CURRENT_SEARCH_DATA:
    return {
      ...state,
      reset: false,
      currentSearchData: {
        ...state.currentSearchData,
        ...action.data
      }
    };
  default:
    return state;
  }
}