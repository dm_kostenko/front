import {
  GET_ALL,
  GET_ALL_FAILED,
  GET_ALL_SUCCEED,
  GET_ALL_TX_RULES,
  GET_ALL_TX_RULES_SUCCEED,
  GET_ALL_TX_RULES_FAILED,
  GET_FFCCTRNS,
  GET_FFCCTRNS_FAILED,
  GET_FFCCTRNS_SUCCEED,
  GET_WAITING_PAYMENTS,
  GET_WAITING_PAYMENTS_FAILED,
  GET_WAITING_PAYMENTS_SUCCEED,
  GET_INTERNAL,
  GET_INTERNAL_SUCCEED,
  GET_INTERNAL_FAILED,
  GET_FFPCRQST,
  GET_FFPCRQST_FAILED,
  GET_FFPCRQST_SUCCEED,
  GET_FFPSRPRT,
  GET_FFPSRPRT_FAILED,
  GET_FFPSRPRT_SUCCEED,
  GET_PRTRN,
  GET_PRTRN_FAILED,
  GET_PRTRN_SUCCEED,
  GET_ROINVSTG,
  GET_ROINVSTG_FAILED,
  GET_ROINVSTG_SUCCEED, 
  GET_INTERNAL_DETAIL,
  GET_INTERNAL_DETAIL_FAILED,
  GET_INTERNAL_DETAIL_SUCCEED,
  GET_FFCCTRNS_DETAIL,
  GET_FFCCTRNS_DETAIL_FAILED,
  GET_FFCCTRNS_DETAIL_SUCCEED,
  GET_FFPCRQST_DETAIL,
  GET_FFPCRQST_DETAIL_FAILED,
  GET_FFPCRQST_DETAIL_SUCCEED,
  GET_FFPSRPRT_DETAIL,
  GET_FFPSRPRT_DETAIL_FAILED,
  GET_FFPSRPRT_DETAIL_SUCCEED,
  GET_PRTRN_DETAIL,
  GET_PRTRN_DETAIL_FAILED,
  GET_PRTRN_DETAIL_SUCCEED,
  GET_ROINVSTG_DETAIL,
  GET_ROINVSTG_DETAIL_FAILED,
  GET_ROINVSTG_DETAIL_SUCCEED,
  CREATE_TRANSACTION,
  CREATE_TRANSACTIONS,
  CREATE_TRANSACTION_RULE,
  EDIT_TRANSACTION_STATUS,
  REQUEST_OTP,
  GET_TXS_TEMPLATES,
  GET_TXS_TEMPLATES_FAILED,
  GET_TXS_TEMPLATES_SUCCEED,
  GET_EXTERNAL_TXS_TEMPLATES,
  GET_EXTERNAL_TXS_TEMPLATES_FAILED,
  GET_EXTERNAL_TXS_TEMPLATES_SUCCEED,
  CREATE_EXTERNAL_TX_TEMPLATE,
  GET_INTERNAL_TXS_TEMPLATES,
  GET_INTERNAL_TXS_TEMPLATES_FAILED,
  GET_INTERNAL_TXS_TEMPLATES_SUCCEED,
  CREATE_INTERNAL_TX_TEMPLATE,
  RESET,
  CHANGE_TEMPLATE_CHECKBOX,
  CHANGE_WAITING_CHECKBOX,
  GET_TXS_FEES,
  UPSERT_TXS_TEMPLATES
} from "../ibankConstants/actionTypes";

const initialState = {

  ALL: [],                 // txs
  countALL: 0,
  loadingALL: true,
  errorALL: false,

  INTERNAL: [],
  countINTERNAL: 0,
  loadingINTERNAL: true,
  errorINTERNAL: false,

  txRules: [],
  countTxRules: 0,
  loadingTxRules: true,
  errorTxRules: false,
  
  FFCCTRNS: [],
  countFFCCTRNS: 0,
  loadingFFCCTRNS: true,
  errorFFCCTRNS: false,

  FFPSRPRT: [],
  countFFPSRPRT: 0,
  loadingFFPSRPRT: true,
  errorFFPSRPRT: false,

  FFPCRQST: [],
  countFFPCRQST: 0,
  loadingFFPCRQST: true,
  errorFFPCRQST: false,

  PRTRN: [],
  countPRTRN: 0,
  loadingPRTRN: true,
  errorPRTRN: false,

  ROINVSTG: [],
  countROINVSTG: 0,
  loadingROINVSTG: true,
  errorROINVSTG: false,

  internalDetail: [],           // tx
  loadingInternalDetail: true,
  errorInternalDetail: false,

  FFCCTRNSDetail: [],                 
  loadingFFCCTRNSDetail: true,
  errorFFCCTRNSDetail: false,

  FFPSRPRTDetail: [],
  loadingFFPSRPRTDetail: true,
  errorFFPSRPRTDetail: false,

  FFPCRQSTDetail: [],
  loadingFFPCRQSTDetail: true,
  errorFFPCRQSTDetail: false,

  PRTRNDetail: [],
  loadingPRTRNDetail: true,
  errorPRTRNDetail: false,

  ROINVSTGDetail: [],
  loadingROINVSTGDetail: true,
  errorROINVSTGDetail: false,

  responseTransaction: {},

  responseTransactionStatus: {},

  responseOtp: {},

  txTemplates: [],
  countTxTemplates: 0,
  loadingTxTemplates: true,
  errorTxTemplates: false,

  externalTemplates: [],
  countExternalTemplates: 0,
  loadingExternalTemplates: true,
  errorExternalTemplates: false,
  responseExternalTemplate: {},

  internalTemplates: [],
  countInternalTemplates: 0,
  loadingInternalTemplates: true,
  errorInternalTemplates: false,
  responseInternalTemplate: {},

  waitingPayments: [],
  countWaitingPayments: 0,
  loadingWaitingPayments: true,
  errorWaitingPayments: false,

  checkedTemplates: [],
  checkedWaiting: [],

  responseTransactionsFee: {},

  responseTransactions: {},

  responseUpsertTxsTemplates: {}
};

export default function transactions(state = initialState, action) {
  switch (action.type) {
  case GET_ALL:
    return {
      ...state,
      loadingALL: true,
      errorALL: false
    };
  case GET_ALL_SUCCEED: {
    return {
      ...state,
      ALL: action.payload.data,
      countALL: action.payload.count,
      loadingALL: false,
      errorALL: false
    };
  }
  case GET_ALL_FAILED:
    return {
      ...state,
      loadingALL: false,
      errorALL: true
    };
  case GET_WAITING_PAYMENTS:
    return {
      ...state,
      loadingWaitingPayments: true,
      errorWaitingPayments: false
    };
  case GET_WAITING_PAYMENTS_SUCCEED: {
    return {
      ...state,
      waitingPayments: action.payload.data,
      countWaitingPayments: action.payload.count,
      loadingWaitingPayments: false,
      errorWaitingPayments: false
    };
  }
  case GET_WAITING_PAYMENTS_FAILED:
    return {
      ...state,
      loadingWaitingPayments: false,
      errorWaitingPayments: true
    };
  case GET_ALL_TX_RULES:
    return {
      ...state,
      loadingTxRules: true,
      errorTxRules: false
    };
  case GET_ALL_TX_RULES_SUCCEED:
    return {
      ...state,
      txRules: action.payload.data,
      countTxRules: action.payload.count,
      pageSize: action.pageSize,
      currentPage: action.currentPage,
      loadingTxRules: false,
      errorTxRules: false
    };
  case GET_ALL_TX_RULES_FAILED:
    return {
      ...state,
      loadingTxRules: false,
      errorTxRules: true
    };
  case GET_INTERNAL:
    return {
      ...state,
      loadingINTERNAL: true,
      errorINTERNAL: false
    };
  case GET_INTERNAL_SUCCEED: {
    return {
      ...state,
      INTERNAL: action.payload.data,
      countINTERNAL: action.payload.count,
      loadingINTERNAL: false,
      errorINTERNAL: false
    };
  }
  case GET_INTERNAL_FAILED:
    return {
      ...state,
      loadingINTERNAL: false,
      errorINTERNAL: true
    };
  case GET_FFCCTRNS:
    return {
      ...state,
      loadingFFCCTRNS: true,
      errorFFCCTRNS: false
    };
  case GET_FFCCTRNS_SUCCEED: {
    return {
      ...state,
      FFCCTRNS: action.payload.data,
      countFFCCTRNS: action.payload.count,
      loadingFFCCTRNS: false,
      errorFFCCTRNS: false
    };
  }
  case GET_FFCCTRNS_FAILED:
    return {
      ...state,
      loadingFFCCTRNS: false,
      errorFFCCTRNS: true
    };
  case GET_FFPSRPRT:
    return {
      ...state,
      loadingFFPSRPRT: true,
      errorFFPSRPRT: false
    };
  case GET_FFPSRPRT_SUCCEED: {
    return {
      ...state,
      FFPSRPRT: action.payload.data,
      countFFPSRPRT: action.payload.count,
      loadingFFPSRPRT: false,
      errorFFPSRPRT: false
    };
  }
  case GET_FFPSRPRT_FAILED:
    return {
      ...state,
      loadingFFPSRPRT: false,
      errorFFPSRPRT: true
    };
  case GET_FFPCRQST:
    return {
      ...state,
      loadingFFPCRQST: true,
      errorFFPCRQST: false
    };
  case GET_FFPCRQST_SUCCEED: {
    return {
      ...state,
      FFPCRQST: action.payload.data,
      countFFPCRQST: action.payload.count,
      loadingFFPCRQST: false,
      errorFFPCRQST: false
    };
  }
  case GET_FFPCRQST_FAILED:
    return {
      ...state,
      loadingFFPCRQST: false,
      errorFFPCRQST: true
    };
  case GET_PRTRN:
    return {
      ...state,
      loadingPRTRN: true,
      errorPRTRN: false
    };
  case GET_PRTRN_SUCCEED: {
    return {
      ...state,
      PRTRN: action.payload.data,
      countPRTRN: action.payload.count,
      loadingPRTRN: false,
      errorPRTRN: false
    };
  }
  case GET_PRTRN_FAILED:
    return {
      ...state,
      loadingPRTRN: false,
      errorPRTRN: true
    };
  case GET_ROINVSTG:
    return {
      ...state,
      loadingROINVSTG: true,
      errorROINVSTG: false
    };
  case GET_ROINVSTG_SUCCEED: {
    return {
      ...state,
      ROINVSTG: action.payload.data,
      countROINVSTG: action.payload.count,
      loadingROINVSTG: false,
      errorROINVSTG: false
    };
  }
  case GET_ROINVSTG_FAILED:
    return {
      ...state,
      loadingROINVSTG: false,
      errorROINVSTG: true
    };
  case GET_INTERNAL_DETAIL:
    return {
      ...state,
      loadingInternalDetail: true,
      errorInternalDetail: false
    };
  case GET_INTERNAL_DETAIL_SUCCEED: {
    return {
      ...state,
      internalDetail: action.payload.data,
      loadingInternalDetail: false,
      errorInternalDetail: false
    };
  }
  case GET_INTERNAL_DETAIL_FAILED:
    return {
      ...state,
      loadingInternalDetail: false,
      errorInternalDetail: true
    };
  case GET_FFCCTRNS_DETAIL:
    return {
      ...state,
      loadingFFCCTRNSDetail: true,
      errorFFCCTRNSDetail: false
    };
  case GET_FFCCTRNS_DETAIL_SUCCEED: {
    return {
      ...state,
      FFCCTRNSDetail: action.payload.data,
      loadingFFCCTRNSDetail: false,
      errorFFCCTRNSDetail: false
    };
  }
  case GET_FFCCTRNS_DETAIL_FAILED:
    return {
      ...state,
      loadingFFCCTRNSDetail: false,
      errorFFCCTRNSDetail: true
    };
  case GET_FFPSRPRT_DETAIL:
    return {
      ...state,
      loadingFFPSRPRTDetail: true,
      errorFFPSRPRTDetail: false
    };
  case GET_FFPSRPRT_DETAIL_SUCCEED: {
    return {
      ...state,
      FFPSRPRTDetail: action.payload.data,
      loadingFFPSRPRTDetail: false,
      errorFFPSRPRTDetail: false
    };
  }
  case GET_FFPSRPRT_DETAIL_FAILED:
    return {
      ...state,
      loadingFFPSRPRTDetail: false,
      errorFFPSRPRTDetail: true
    };
  case GET_FFPCRQST_DETAIL:
    return {
      ...state,
      loadingFFPCRQSTDetail: true,
      errorFFPCRQSTDetail: false
    };
  case GET_FFPCRQST_DETAIL_SUCCEED: {
    return {
      ...state,
      FFPCRQSTDetail: action.payload.data,
      loadingFFPCRQSTDetail: false,
      errorFFPCRQSTDetail: false
    };
  }
  case GET_FFPCRQST_DETAIL_FAILED:
    return {
      ...state,
      loadingFFPCRQSTDetail: false,
      errorFFPCRQSTDetail: true
    };
  case GET_PRTRN_DETAIL:
    return {
      ...state,
      loadingPRTRNDetail: true,
      errorPRTRNDetail: false
    };
  case GET_PRTRN_DETAIL_SUCCEED: {
    return {
      ...state,
      PRTRNDetail: action.payload.data,
      loadingPRTRNDetail: false,
      errorPRTRNDetail: false
    };
  }
  case GET_PRTRN_DETAIL_FAILED:
    return {
      ...state,
      loadingPRTRNDetail: false,
      errorPRTRNDetail: true
    };
  case GET_ROINVSTG_DETAIL:
    return {
      ...state,
      loadingROINVSTGDetail: true,
      errorROINVSTGDetail: false
    };
  case GET_ROINVSTG_DETAIL_SUCCEED: {
    return {
      ...state,
      ROINVSTGDetail: action.payload.data,
      loadingROINVSTGDetail: false,
      errorROINVSTGDetail: false
    };
  }
  case GET_ROINVSTG_DETAIL_FAILED:
    return {
      ...state,
      loadingROINVSTGDetail: false,
      errorROINVSTGDetail: true
    };
                                              
  case CREATE_TRANSACTION:
    return { 
      ...state, 
      responseTransaction: action.payload 
    };
  case CREATE_TRANSACTIONS:
    return { 
      ...state, 
      responseTransactions: action.payload 
    };
  case CREATE_TRANSACTION_RULE:
    return { 
      ...state, 
      responseTransactionRule: action.payload 
    };
  case EDIT_TRANSACTION_STATUS:
    return {
      ...state,
      responseTransactionStatus: action.payload
    };
  case GET_TXS_TEMPLATES:
    return {
      ...state,
      loadingTxTemplates: true,
      errorTxTemplates: false
    }
  case GET_TXS_TEMPLATES_SUCCEED:
    return {
      ...state,
      txTemplates: action.payload.data,
      countTxTemplates: action.payload.count,
      loadingTxTemplates: false,
      errorTxTemplates: false
    }
  case GET_TXS_TEMPLATES_FAILED:
    return {
      ...state,
      loadingTxTemplates: false,
      errorTxTemplates: true
    }
  case GET_EXTERNAL_TXS_TEMPLATES:
    return {
      ...state,
      loadingExternalTemplates: true,
      errorExternalTemplates: false
    }
  case GET_EXTERNAL_TXS_TEMPLATES_SUCCEED:
    return {
      ...state,
      externalTemplates: action.payload.data,
      loadingExternalTemplates: false,
      errorExternalTemplates: false
    }
  case GET_EXTERNAL_TXS_TEMPLATES_FAILED:
    return {
      ...state,
      loadingExternalTemplates: false,
      errorExternalTemplates: true
    }
  case CREATE_EXTERNAL_TX_TEMPLATE:
    return {
      ...state,
      responseExternalTemplate: action.payload
    }
  case GET_INTERNAL_TXS_TEMPLATES:
    return {
      ...state,
      loadingInternalTemplates: true,
      errorInternalTemplates: false
    }
  case GET_INTERNAL_TXS_TEMPLATES_SUCCEED:
    return {
      ...state,
      internalTemplates: action.payload.data,
      loadingInternalTemplates: false,
      errorInternalTemplates: false
    }
  case GET_INTERNAL_TXS_TEMPLATES_FAILED:
    return {
      ...state,
      loadingInternalTemplates: false,
      errorInternalTemplates: true
    }
  case CREATE_INTERNAL_TX_TEMPLATE:
    return {
      ...state,
      responseInternalTemplate: action.payload
    }
  case REQUEST_OTP:
    return {
      ...state,
      responseOtp: action.payload
    };
  case RESET:
    return {
      ...state,
      responseTransaction: {},
      responseOtp: {},
      responseTransactionStatus: {}
    };
  case CHANGE_TEMPLATE_CHECKBOX:
    return {
      ...state,
      checkedTemplates: Array.isArray(action.payload)
        ? action.payload
        : state.checkedTemplates.map(i => i.guid).includes(action.payload.guid)
          ? state.checkedTemplates.filter(i => i.guid !== action.payload.guid)
          : [
            ...state.checkedTemplates,
            action.payload
          ]
    }
  case CHANGE_WAITING_CHECKBOX:
    return {
      ...state,
      checkedWaiting: Array.isArray(action.payload)
        ? action.payload
        : state.checkedWaiting.map(i => i.guid).includes(action.payload.guid)
          ? state.checkedWaiting.filter(i => i.guid !== action.payload.guid)
          : [
            ...state.checkedWaiting,
            action.payload
          ]
    }
  case GET_TXS_FEES:
    return {
      ...state,
      responseTransactionsFee: action.payload
    }
  case UPSERT_TXS_TEMPLATES:
    return {
      ...state,
      responseUpsertTxsTemplates: action.payload
    }
  default:
    return state;
  }
}