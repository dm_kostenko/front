import {
  GET_RATES,
  GET_RATES_FAILED,
  GET_RATES_SUCCEED,
  GET_RATE_RULES,
  GET_RATE_RULES_FAILED,
  GET_RATE_RULES_SUCCEED,
  GET_RATE_RULE_PARAMS,
  GET_RATE_RULE_PARAMS_FAILED,
  GET_RATE_RULE_PARAMS_SUCCEED,
  CREATE_RATE,
  CHANGE_RATE_RULE_PARAMS
} from "ibankConstants/actionTypes";

const initialState = {
  rates: [],
  rate_rules: [],
  rate_rule_params: [],
  count: 0,
  changeRateRulesResponse: {},
  loading: true,
  loadingParams: true,
  error: false,
  pageSize: 10,
  currentPage: 1
};

export default function rates(state = initialState, action) {
  switch (action.type) {
  case GET_RATES:
    return {
      ...state,
      loading: true,
      error: false
    };
  case GET_RATES_SUCCEED: {
    return {
      ...state,
      rates: action.payload.data,
      count: action.payload.count,
      pageSize: action.pageSize,
      currentPage: action.currentPage,
      loading: false,
      error: false
    };
  }
  case GET_RATES_FAILED:
    return {
      ...state,
      loading: false,
      error: true
    };
  case GET_RATE_RULES:
    return {
      ...state,
      loadingRules: true,
      errorRules: false
    };
  case GET_RATE_RULES_SUCCEED: {
    return {
      ...state,
      rate_rules: action.payload.data,
      // count: action.payload.count,
      loadingRules: false,
      errorRules: false
    };
  }
  case GET_RATE_RULES_FAILED:
    return {
      ...state,
      loadingRules: false,
      errorRules: true
    };
  case GET_RATE_RULE_PARAMS:
    return {
      ...state,
      loadingParams: true,
      errorParams: false
    };
  case GET_RATE_RULE_PARAMS_SUCCEED: {
    return {
      ...state,
      rate_rule_params: action.payload.data,
      count: action.payload.count,
      loadingParams: false,
      errorParams: false
    };
  }
  case GET_RATE_RULE_PARAMS_FAILED:
    return {
      ...state,
      loadingParams: false,
      errorParams: true
    };  
  case CREATE_RATE:
    return {
      ...state,
      responseRate: action.payload
    };  
  case CHANGE_RATE_RULE_PARAMS:
    return {
      ...state,
      changeRateRulesResponse: action.payload
    }
  default:
    return state;
  }
}