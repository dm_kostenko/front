import {
  GET_ACCOUNTS,
  GET_ACCOUNTS_FAILED,
  GET_ACCOUNTS_SUCCEED,
  GET_ACCOUNTS_STATEMENTS,
  GET_ACCOUNTS_STATEMENTS_FAILED,
  GET_ACCOUNTS_STATEMENTS_SUCCEED,
  GET_ACCOUNTS_STATEMENTS_IN_XML,
  GET_ACCOUNTS_STATEMENTS_IN_XML_FAILED,
  GET_ACCOUNTS_STATEMENTS_IN_XML_SUCCEED,
  GET_ACCOUNTS_PAYS_DOCS,
  GET_ACCOUNTS_PAYS_DOCS_FAILED,
  GET_ACCOUNTS_PAYS_DOCS_SUCCEED,
  GET_ACCOUNTS_BALANCES_DOCS,
  GET_ACCOUNTS_BALANCES_DOCS_FAILED,
  GET_ACCOUNTS_BALANCES_DOCS_SUCCEED,
  GET_TX_ACCOUNTS,
  GET_TX_ACCOUNTS_FAILED,
  GET_TX_ACCOUNTS_SUCCEED,
  GET_ACCOUNT,
  GET_ACCOUNT_FAILED,
  GET_ACCOUNT_SUCCEED,
  CREATE_ACCOUNT,
  ADD_CURRENCY_TO_ACCOUNT
} from "ibankConstants/actionTypes";

const initialState = {
  accounts: [],
  count: 0,
  loading: true,
  error: false,

  statements: {},
  loadingStatements: true,
  errorStatements: false,

  paysDocs: [],
  countPaysDocs: 0,
  loadingPaysDocs: true,
  errorPaysDocs: false,

  balancesDocs: [],
  countBalancesDocs: 0,
  loadingBalancesDocs: true,
  errorBalancesDocs: false,

  txAccounts: [],
  txAccountsCount: 0,
  txAccountsLoading: true,
  txAccountsError: false,

  pageSize: 10,
  currentPage: 1,
  account: {},
  loadingDetail: true,
  errorDetail: false,

  xmlStatement: "",
  loadingXmlStatement: true,
  errorXmlStatement: false
};

export default function accounts(state = initialState, action) {
  switch (action.type) {
  case GET_ACCOUNTS:
    return {
      ...state,
      loading: true,
      error: false
    };
  case GET_ACCOUNTS_SUCCEED: {
    return {
      ...state,
      accounts: action.payload.data,
      count: action.payload.count,
      pageSize: action.pageSize,
      currentPage: action.currentPage,
      loading: false,
      error: false
    };
  }
  case GET_ACCOUNTS_FAILED:
    return {
      ...state,
      loading: false,
      error: true
    };
  case GET_ACCOUNTS_STATEMENTS:
    return {
      ...state,
      loadingStatements: true,
      errorStatements: false
    };
  case GET_ACCOUNTS_STATEMENTS_SUCCEED: {
    return {
      ...state,
      statements: action.payload,
      pageSize: action.pageSize,
      currentPage: action.currentPage,
      loadingStatements: false,
      errorStatements: false
    };
  }
  case GET_ACCOUNTS_STATEMENTS_FAILED:
    return {
      ...state,
      loadingStatements: false,
      errorStatements: true
    };
  case GET_ACCOUNTS_STATEMENTS_IN_XML:
    return {
      ...state,
      loadingXmlStatement: true,
      errorXmlStatement: false
    };
  case GET_ACCOUNTS_STATEMENTS_IN_XML_SUCCEED: {
    return {
      ...state,
      xmlStatement: action.payload.document,
      loadingXmlStatement: false,
      errorXmlStatement: false
    };
  }
  case GET_ACCOUNTS_STATEMENTS_IN_XML_FAILED:
    return {
      ...state,
      loadingXmlStatement: false,
      errorXmlStatement: true
    };
  case GET_ACCOUNTS_PAYS_DOCS:
    return {
      ...state,
      loadingPaysDocs: true,
      errorPaysDocs: false
    };
  case GET_ACCOUNTS_PAYS_DOCS_SUCCEED: {
    return {
      ...state,
      paysDocs: action.payload.data,
      countPaysDocs: action.payload.count,
      loadingPaysDocs: false,
      errorPaysDocs: false
    };
  }
  case GET_ACCOUNTS_PAYS_DOCS_FAILED:
    return {
      ...state,
      loadingPaysDocs: false,
      errorPaysDocs: true
    };
  case GET_ACCOUNTS_BALANCES_DOCS:
    return {
      ...state,
      loadingBalancesDocs: true,
      errorBalancesDocs: false
    };
  case GET_ACCOUNTS_BALANCES_DOCS_SUCCEED: {
    return {
      ...state,
      balancesDocs: action.payload.data,
      countBalancesDocs: action.payload.count,
      loadingBalancesDocs: false,
      errorBalancesDocs: false
    };
  }
  case GET_ACCOUNTS_BALANCES_DOCS_FAILED:
    return {
      ...state,
      loadingBalancesDocs: false,
      errorBalancesDocs: true
    };
  case GET_TX_ACCOUNTS:
    return {
      ...state,
      txAccountsLoading: true,
      txAccountsError: false
    };
  case GET_TX_ACCOUNTS_SUCCEED: {
    return {
      ...state,
      txAccounts: action.payload.data,
      txAccountsCount: action.payload.count,
      txAccountsLoading: false,
      txAccountsError: false
    };
  }
  case GET_TX_ACCOUNTS_FAILED:
    return {
      ...state,
      txAccountsLoading: false,
      txAccountsError: true
    };
  case GET_ACCOUNT:
    return {
      ...state,
      loadingDetail: true,
      errorDetail: false
    };
  case GET_ACCOUNT_SUCCEED: {
    return {
      ...state,
      account: action.payload,
      loadingDetail: false,
      errorDetail: false
    };
  }
  case GET_ACCOUNT_FAILED:
    return {
      ...state,
      loadingDetail: false,
      errorDetail: true
    };
  case CREATE_ACCOUNT:
    return {
      ...state,
      responseAccount: action.payload
    };  
  case ADD_CURRENCY_TO_ACCOUNT: 
    return {
      ...state,
      responseAccountCurrency: action.payload
    }
  default:
    return state;
  }
}