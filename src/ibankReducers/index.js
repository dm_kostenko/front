import { combineReducers } from "redux";
import auth from "./auth";
import users from "./users";
import transactions from "./transactions";
import documents from "./documents";
import accounts from "./accounts";
import search from "./search";
import modal from "./modal";
import sidebar from "./sidebar";
import privileges from "./privileges";
import currencies from "./currencies";
import confirmation from "./confirmation";
import history from "./history";
import reports from "./reports";
import rates from "./rates";
import aml from "./aml";

const rootReducer = combineReducers({
  auth,
  users,
  accounts,
  currencies,
  transactions,
  documents,
  search,
  modal,
  sidebar,
  privileges,
  rates,
  confirmation,
  history,
  reports,
  aml
});

export default rootReducer;