import {
  // GET_FFCCTRNS_DOCS,
  // GET_FFCCTRNS_DOCS_FAILED,
  // GET_FFCCTRNS_DOCS_SUCCEED,
  // GET_FFPCRQST_DOCS,
  // GET_FFPCRQST_DOCS_FAILED,
  // GET_FFPCRQST_DOCS_SUCCEED,
  // GET_FFPSRPRT_DOCS,
  // GET_FFPSRPRT_DOCS_FAILED,
  // GET_FFPSRPRT_DOCS_SUCCEED,
  // GET_PRTRN_DOCS,
  // GET_PRTRN_DOCS_FAILED,
  // GET_PRTRN_DOCS_SUCCEED,
  // GET_ROINVSTG_DOCS,
  // GET_ROINVSTG_DOCS_FAILED,
  // GET_ROINVSTG_DOCS_SUCCEED,  
  // GET_FFCCTRNS_DOC,
  // GET_FFCCTRNS_DOC_FAILED,
  // GET_FFCCTRNS_DOC_SUCCEED,
  // GET_FFPCRQST_DOC,
  // GET_FFPCRQST_DOC_FAILED,
  // GET_FFPCRQST_DOC_SUCCEED,
  // GET_FFPSRPRT_DOC,
  // GET_FFPSRPRT_DOC_FAILED,
  // GET_FFPSRPRT_DOC_SUCCEED,
  // GET_PRTRN_DOC,
  // GET_PRTRN_DOC_FAILED,
  // GET_PRTRN_DOC_SUCCEED,
  // GET_ROINVSTG_DOC,
  // GET_ROINVSTG_DOC_FAILED,
  // GET_ROINVSTG_DOC_SUCCEED, 
  // GENERATE_DOC
  GET_INBOX,
  GET_INBOX_FAILED,
  GET_INBOX_SUCCEED,
  GET_INBOX_DETAIL,
  GET_INBOX_DETAIL_FAILED,
  GET_INBOX_DETAIL_SUCCEED,
  EDIT_INBOX_STATUS
} from "../ibankConstants/actionTypes";

const initialState = {
  inbox: [],
  countInbox: 0,
  errorInbox: false,
  loadingInbox: false,

  inboxDetail: {},
  errorInboxDetail: false,
  loadingInboxDetail: false
  
  // DocsFFCCTRNS: [],             // docs
  // countDocsFFCCTRNS: 0,
  // loadingDocsFFCCTRNS: true,
  // errorDocsFFCCTRNS: false,

  // DocsFFPCRQST: [],
  // countDocsFFPCRQST: 0,
  // loadingDocsFFPCRQST: true,
  // errorDocsFFPCRQST: false,

  // DocsFFPSRPRT: [],
  // countDocsFFPSRPRT: 0,
  // loadingDocsFFPSRPRT: true,
  // errorDocsFFPSRPRT: false,

  // DocsPRTRN: [],
  // countDocsPRTRN: 0,
  // loadingDocsPRTRN: true,
  // errorDocsPRTRN: false,

  // DocsROINVSTG: [],
  // countDocsROINVSTG: 0,
  // loadingDocsROINVSTG: true,
  // errorDocsROINVSTG: false,

  // DocFFCCTRNS: [],             // doc detail
  // loadingDocFFCCTRNS: true,
  // errorDocFFCCTRNS: false,

  // DocFFPCRQST: [],
  // loadingDocFFPCRQST: true,
  // errorDocFFPCRQST: false,

  // DocFFPSRPRT: [],
  // loadingDocFFPSRPRT: true,
  // errorDocFFPSRPRT: false,

  // DocPRTRN: [],
  // loadingDocPRTRN: true,
  // errorDocPRTRN: false,

  // DocROINVSTG: [],
  // loadingDocROINVSTG: true,
  // errorDocROINVSTG: false,

  // docResponse: {}
};

export default function documents(state = initialState, action) {
  switch (action.type) {
  case GET_INBOX: {
    return {
      ...state,
      loadingInbox: true,
      errorInbox: false
    };
  }
  case GET_INBOX_SUCCEED: {
    return {
      ...state,
      inbox: action.payload.data,
      countInbox: action.payload.count,
      loadingInbox: false,
      errorInbox: false
    };
  }
  case GET_INBOX_FAILED: {
    return {
      ...state,
      loadingInbox: false,
      errorInbox: true
    };
  }
  case GET_INBOX_DETAIL: {
    return {
      ...state,
      loadingInboxDetail: true,
      errorInboxDetail: false
    };
  }
  case GET_INBOX_DETAIL_SUCCEED: {
    return {
      ...state,
      inboxDetail: action.payload.data,
      loadingInboxDetail: false,
      errorInboxDetail: false
    };
  }
  case GET_INBOX_DETAIL_FAILED: {
    return {
      ...state,
      loadingInboxDetail: false,
      errorInboxDetail: true
    };
  }
  // case GET_FFCCTRNS_DOCS:
  //   return {
  //     ...state,
  //     loadingDocsFFCCTRNS: true,
  //     errorDocsFFCCTRNS: false
  //   };
  // case GET_FFCCTRNS_DOCS_SUCCEED: {
  //   return {
  //     ...state,
  //     DocsFFCCTRNS: action.payload.data,
  //     countDocsFFCCTRNS: action.payload.count,
  //     loadingDocsFFCCTRNS: false,
  //     errorDocsFFCCTRNS: false
  //   };
  // }
  // case GET_FFCCTRNS_DOCS_FAILED:
  //   return {
  //     ...state,
  //     loadingDocsFFCCTRNS: false,
  //     errorDocsFFCCTRNS: true
  //   };
  // case GET_FFPCRQST_DOCS:
  //   return {
  //     ...state,
  //     loadingDocsFFPCRQST: true,
  //     errorDocsFFPCRQST: false
  //   };
  // case GET_FFPCRQST_DOCS_SUCCEED: {
  //   return {
  //     ...state,
  //     DocsFFPCRQST: action.payload.data,
  //     countDocsFFPCRQST: action.payload.count,
  //     loadingDocsFFPCRQST: false,
  //     errorDocsFFPCRQST: false
  //   };
  // }
  // case GET_FFPCRQST_DOCS_FAILED:
  //   return {
  //     ...state,
  //     loadingDocsFFPCRQST: false,
  //     errorDocsFFPCRQSTS: true
  //   };
  // case GET_FFPSRPRT_DOCS:
  //   return {
  //     ...state,
  //     loadingDocsFFPSRPRT: true,
  //     errorDocsFFPSRPRT: false
  //   };
  // case GET_FFPSRPRT_DOCS_SUCCEED: {
  //   return {
  //     ...state,
  //     DocsFFPSRPRT: action.payload.data,
  //     countDocsFFPSRPRT: action.payload.count,
  //     loadingDocsFFPSRPRT: false,
  //     errorDocsFFPSRPRT: false
  //   };
  // }
  // case GET_FFPSRPRT_DOCS_FAILED:
  //   return {
  //     ...state,
  //     loadingDocsFFPSRPRT: false,
  //     errorDocsFFPSRPRT: true
  //   };
  // case GET_PRTRN_DOCS:
  //   return {
  //     ...state,
  //     loadingDocsPRTRN: true,
  //     errorDocsPRTRN: false
  //   };
  // case GET_PRTRN_DOCS_SUCCEED: {
  //   return {
  //     ...state,
  //     DocsPRTRN: action.payload.data,
  //     countDocsPRTRN: action.payload.count,
  //     loadingDocsPRTRN: false,
  //     errorDocsPRTRN: false
  //   };
  // }
  // case GET_PRTRN_DOCS_FAILED:
  //   return {
  //     ...state,
  //     loadingDocsPRTRN: false,
  //     errorDocsPRTRN: true
  //   };
  // case GET_ROINVSTG_DOCS:
  //   return {
  //     ...state,
  //     loadingDocsROINVSTG: true,
  //     errorDocsROINVSTG: false
  //   };
  // case GET_ROINVSTG_DOCS_SUCCEED: {
  //   return {
  //     ...state,
  //     DocsROINVSTG: action.payload.data,
  //     countDocsROINVSTG: action.payload.count,
  //     loadingDocsROINVSTG: false,
  //     errorDocsROINVSTG: false
  //   };
  // }
  // case GET_ROINVSTG_DOCS_FAILED:
  //   return {
  //     ...state,
  //     loadingDocsROINVSTG: false,
  //     errorDocsROINVSTG: true
  //   };        
  // case GET_FFCCTRNS_DOC:
  //   return {
  //     ...state,
  //     loadingDocFFCCTRNS: true,
  //     errorDocFFCCTRNS: false
  //   };
  // case GET_FFCCTRNS_DOC_SUCCEED: {
  //   return {
  //     ...state,
  //     DocFFCCTRNS: action.payload.data,
  //     loadingDocFFCCTRNS: false,
  //     errorDocFFCCTRNS: false
  //   };
  // }
  // case GET_FFCCTRNS_DOC_FAILED:
  //   return {
  //     ...state,
  //     loadingDocFFCCTRNS: false,
  //     errorDocFFCCTRNS: true
  //   };
  // case GET_FFPCRQST_DOC:
  //   return {
  //     ...state,
  //     loadingDocFFPCRQST: true,
  //     errorDocFFPCRQST: false
  //   };
  // case GET_FFPCRQST_DOC_SUCCEED: {
  //   return {
  //     ...state,
  //     DocFFPCRQST: action.payload.data,
  //     loadingDocFFPCRQST: false,
  //     errorDocFFPCRQST: false
  //   };
  // }
  // case GET_FFPCRQST_DOC_FAILED:
  //   return {
  //     ...state,
  //     loadingDocFFPCRQST: false,
  //     errorDocFFPCRQST: true
  //   };
  // case GET_FFPSRPRT_DOC:
  //   return {
  //     ...state,
  //     loadingDocFFPSRPRT: true,
  //     errorDocFFPSRPRT: false
  //   };
  // case GET_FFPSRPRT_DOC_SUCCEED: {
  //   return {
  //     ...state,
  //     DocFFPSRPRT: action.payload.data,
  //     loadingDocFFPSRPRT: false,
  //     errorDocFFPSRPRT: false
  //   };
  // }
  // case GET_FFPSRPRT_DOC_FAILED:
  //   return {
  //     ...state,
  //     loadingDocPRTRN: false,
  //     errorDocPRTRN: true
  //   };
  // case GET_PRTRN_DOC:
  //   return {
  //     ...state,
  //     loadingDocPRTRN: true,
  //     errorDocPRTRN: false
  //   };
  // case GET_PRTRN_DOC_SUCCEED: {
  //   return {
  //     ...state,
  //     DocPRTRN: action.payload.data,
  //     loadingDocPRTRN: false,
  //     errorDocPRTRN: false
  //   };
  // }
  // case GET_PRTRN_DOC_FAILED:
  //   return {
  //     ...state,
  //     loadingDocPRTRN: false,
  //     errorDocPRTRN: true
  //   };
  // case GET_ROINVSTG_DOC:
  //   return {
  //     ...state,
  //     loadingDocROINVSTG: true,
  //     errorDocROINVSTG: false
  //   };
  // case GET_ROINVSTG_DOC_SUCCEED: {
  //   return {
  //     ...state,
  //     DocROINVSTG: action.payload.data,
  //     loadingDocROINVSTG: false,
  //     errorDocROINVSTG: false
  //   };
  // }
  // case GET_ROINVSTG_DOC_FAILED:
  //   return {
  //     ...state,
  //     loadingDocROINVSTG: false,
  //     errorDocROINVSTG: true
  //   };
  // case GENERATE_DOC:
  //   return {
  //     ...state,
  //     docResponse: action.payload
  //   };                        
  default: 
    return state;
  }
}