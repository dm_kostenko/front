import {
  GET_PRIVILEGES,
  GET_PRIVILEGES_FAILED,
  GET_PRIVILEGES_SUCCEED,
} from "ibankConstants/actionTypes";

const initialState = {
  privileges: [],
  count: 0,
  loading: false,
  error: false,
  pageSize: 10,
  currentPage: 1
};

export default function privileges(state = initialState, action) {
  switch (action.type) {
  case GET_PRIVILEGES:
    return {
      ...state,
      loading: true,
      error: false
    };
  case GET_PRIVILEGES_SUCCEED: {
    return {
      ...state,
      privileges: action.payload.data,
      count: action.payload.count,
      pageSize: action.pageSize,
      currentPage: action.currentPage,
      loading: false,
      error: false
    };
  }
  case GET_PRIVILEGES_FAILED:
    return {
      ...state,
      loading: false,
      error: true
    };
  default:
    return state;
  }
}