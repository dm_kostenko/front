import * as types from "ibankConstants/actionTypes";

const initialState = {
  entity: false,
  profile: false,
  users: false,
  accounts: false,
  changePassword: false,
  confirmModalState: false,
  attachTags: false,
  uploadFile: false
};

export default function modal (state = initialState, action) {
  switch (action.type) {
  // case types.SHOW_MANAGER_MODAL:
  //   return {
  //     ...state,
  //     modalManagerState: action.managerModalState,
  //     type: action.type
  //   };
  case types.SHOW_MODAL:
    return {
      ...state,
      [action.payload.name]: action.payload.modalState,
      // type: action.type
    };
  case types.SHOW_CONFIRMATION_MODAL:
    if(action.modalState)
      return {
        ...state,
        confirmModalState: action.modalState
      }
    return {
      ...state,
      entity: false,
      profile: false,
      users: false,
      accounts: false,
      changePassword: false,
      confirmModalState: false
    }
  default:
    return state;
  }
};