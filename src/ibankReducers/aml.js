import {
  GET_AML_HISTORY,
  GET_AML_HISTORY_FAILED,
  GET_AML_HISTORY_SUCCEED,
  GET_AML_DETAIL,
  GET_AML_DETAIL_FAILED,
  GET_AML_DETAIL_SUCCEED,
  SEARCH_IN_AML,
  UPDATE_AML_DETAIL,
  GET_AML_CASE_COMMENTS,
  GET_AML_CASE_COMMENTS_SUCCEED,
  GET_AML_CASE_COMMENTS_FAILED,
  ADD_AML_CASE_COMMENT,
  GET_TAGS,
  GET_TAGS_SUCCEED,
  GET_TAGS_FAILED,
  UPDATE_TAG,
  GET_CASE_TAGS,
  GET_CASE_TAGS_SUCCEED,
  GET_CASE_TAGS_FAILED,
  ATTACH_TAGS,
  GET_FILES,
  GET_FILES_SUCCEED,
  GET_FILES_FAILED,
  ADD_FILES,
  GET_AML_CERTIFICATE,
  GET_AML_CERTIFICATE_SUCCEED,
  GET_AML_CERTIFICATE_FAILED
} from "ibankConstants/actionTypes";

const initialState = {
  amlHistory: [],
  count: 0,
  loading: true,
  error: false,

  responses: [],

  amlDetail: {},
  loadingDetail: true,
  errorDetail: false,

  updateAmlDetailResponse: {},

  caseComments: [],
  countCaseComments: 0,
  loadingCaseComments: true,
  errorCaseComments: false,

  addCaseCommentResponse: {},

  tags: [],
  loadingTags: true,
  errorTags: false,

  caseTags: [],
  loadingCaseTags: true,
  errorCaseTags: false,

  updateTagResponse: {},
  attachTagsResponse: {},

  caseFiles: [],
  loadingCaseFiles: true,
  errorCaseFiles: false,

  addFilesResponse: {},

  amlCertificate: null,
  amlCertificateLoading: false,
  amlCertificateError: false
};

export default function aml(state = initialState, action) {
  switch (action.type) {
  case GET_AML_HISTORY:
    return {
      ...state,
      loading: true,
      error: false
    };
  case GET_AML_HISTORY_SUCCEED: {
    return {
      ...state,
      amlHistory: action.payload.data,
      count: action.payload.count,
      loading: false,
      error: false
    };
  }
  case GET_AML_HISTORY_FAILED:
    return {
      ...state,
      loading: false,
      error: true
    };

  case GET_AML_CERTIFICATE:
    return {
      ...state,
      amlCertificateLoading: true,
      amlCertificateError: false
    };
  case GET_AML_CERTIFICATE_SUCCEED:
    return {
      ...state,
      amlCertificate: action.payload,
      amlCertificateLoading: false,
      amlCertificateError: false
    };
  case GET_AML_CERTIFICATE_FAILED:
    return {
      ...state,
      amlCertificateLoading: false,
      amlCertificateError: true
    };
  case GET_FILES:
    return {
      ...state,
      loadingCaseFiles: true,
      errorCaseFiles: false
    };
  case GET_FILES_SUCCEED: {
    return {
      ...state,
      caseFiles: action.payload.data,
      loadingCaseFiles: false,
      errorCaseFiles: false
    };
  }
  case GET_FILES_FAILED:
    return {
      ...state,
      loadingCaseFiles: false,
      errorCaseFiles: true
    };
  case ADD_FILES:
    return {
      ...state,
      addFilesResponse: action.payload
    }
  case GET_TAGS:
    return {
      ...state,
      loadingTags: true,
      errorTags: false
    };
  case GET_TAGS_SUCCEED: {
    return {
      ...state,
      tags: action.payload.data,
      loadingTags: false,
      errorTags: false
    };
  }
  case GET_TAGS_FAILED:
    return {
      ...state,
      loadingTags: false,
      errorTags: true
    };
  case GET_CASE_TAGS:
    return {
      ...state,
      loadingCaseTags: true,
      errorCaseTags: false
    };
  case GET_CASE_TAGS_SUCCEED: {
    return {
      ...state,
      caseTags: action.payload.data,
      loadingCaseTags: false,
      errorCaseTags: false
    };
  }
  case GET_CASE_TAGS_FAILED:
    return {
      ...state,
      loadingCaseTags: false,
      errorCaseTags: true
    };
  case GET_AML_DETAIL:
    return {
      ...state,
      loadingDetail: true,
      errorDetail: false
    };
  case GET_AML_DETAIL_SUCCEED: {
    return {
      ...state,
      amlDetail: action.payload,
      loadingDetail: false,
      errorDetail: false
    };
  }
  case GET_AML_DETAIL_FAILED:
    return {
      ...state,
      loadingDetail: false,
      errorDetail: true
    };
  case SEARCH_IN_AML:
    return {
      ...state,
      responses: action.payload.responses
    };
  case UPDATE_TAG:
    return {
      ...state,
      updateTagResponse: action.payload
    }
  case ATTACH_TAGS:
    return {
      ...state,
      attachTagsResponse: action.payload
    }
  case UPDATE_AML_DETAIL:
    return {
      ...state,
      updateAmlDetailResponse: action.payload
    }
  case GET_AML_CASE_COMMENTS:
    return {
      ...state,
      loadingCaseComments: true,
      errorCaseComments: false
    };
  case GET_AML_CASE_COMMENTS_SUCCEED: {
    return {
      ...state,
      caseComments: action.payload.data,
      countCaseComments: action.payload.count,
      loadingCaseComments: false,
      errorCaseComments: false
    };
  }
  case GET_AML_CASE_COMMENTS_FAILED:
    return {
      ...state,
      loadingCaseComments: false,
      errorCaseComments: true
    };
  case ADD_AML_CASE_COMMENT:
    return {
      ...state,
      addCaseCommentResponse: action.payload
    }
  default:
    return state;
  }
}