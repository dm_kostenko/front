import {
  SAVE_AXIOS_CONFIG,
  ADD_OTP_TO_CONFIG,
  CONFIRMATION_SUCCEED,
  CREATE_REQUEST,
  CLEAR_DISPATCH_ARRAY,
  RESET,
  SAVE_SWAL_TEXT
} from "ibankConstants/actionTypes";

const initialState = {
  isConfirmation: false,
  confirmation: false,
  config: {},
  dispatchArray: [],
  swalText: ""
};

export default function confirmation(state = initialState, action) {
  switch(action.type) {
  case SAVE_AXIOS_CONFIG:
    return {
      ...state,
      config: action.payload
    };
  case ADD_OTP_TO_CONFIG:
    let { config } = state;
    config.data = JSON.stringify({
      ...JSON.parse(config.data),
      otp: action.payload
    });
    return {
      ...state,
      config
    };
  case CONFIRMATION_SUCCEED:
    return {
      ...state,
      config: {},
      confirmation: action.payload
    };
  case CREATE_REQUEST:
    return {
      ...state,
      dispatchArray: action.payload
    };
  case CLEAR_DISPATCH_ARRAY:
    return {
      ...state,
      dispatchArray: []
    };
  case RESET:
    return {
      ...state,
      confirmation: false
    };
  case SAVE_SWAL_TEXT:
    return {
      ...state,
      swalText: action.payload
    }
  default:
    return state;
  }
}