import React from "react";
import { Pie } from "react-chartjs-2";
//import "chartjs-plugin-annotation";
import PropTypes from "prop-types";

class PieChart extends React.Component {
  render() {
    const data = {
      labels: this.props.labels,
      datasets: this.props.datasets 
    };
    return (
      <Pie
        data={data}
        height={200}
        options={{ maintainAspectRation: false }}
      />
    );
  }
}
export default PieChart;

PieChart.propTypes = {
  options: PropTypes.array,
  height: PropTypes.number,
  labels: PropTypes.array,
  datasets: PropTypes.array,
};
