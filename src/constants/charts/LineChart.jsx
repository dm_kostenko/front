import React from "react";
import { Line } from "react-chartjs-2";
import PropTypes from "prop-types";

export default class LineChart extends React.Component {
  render() {
    const data = {
      labels: this.props.labels,
      datasets: this.props.datasets
    };
    return (
      <Line 
        data={data} 
        height={100}
        options={{ maintainAspectRation: false, ...this.props.options }}
      />
    );
  }
}

LineChart.propTypes = {
  options: PropTypes.array,
  height: PropTypes.number,
  labels: PropTypes.array,
  datasets: PropTypes.array,
};
