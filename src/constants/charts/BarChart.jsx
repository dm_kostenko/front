import React from "react";
import { Bar } from "react-chartjs-2";
import PropTypes from "prop-types";

export default class BarChart extends React.Component {

  render() {
    const data = {
      labels: this.props.labels,
      datasets: this.props.datasets
    };
    return (

      <Bar 
        data={data} 
        height={this.props.height}
        options={this.props.options}
      />
    );
  }
}

BarChart.propTypes = {
  options: PropTypes.array,
  height: PropTypes.number,
  labels: PropTypes.array,
  datasets: PropTypes.array,
};


