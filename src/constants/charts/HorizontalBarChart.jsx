import React from "react";
import { HorizontalBar } from "react-chartjs-2";
import PropTypes from "prop-types";

export default class HorizontalBarChart extends React.Component {

  render() {
    const data = {
      labels: this.props.labels,
      datasets: this.props.datasets
    };
    return (

      <HorizontalBar
        data={data}
        height={this.props.height}
      />
    );
  }
}

HorizontalBarChart.propTypes = {
  options: PropTypes.array,
  height: PropTypes.number,
  labels: PropTypes.array,
  datasets: PropTypes.array,
};

