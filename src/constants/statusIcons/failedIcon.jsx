import React from "react";
import { OverlayTrigger, Popover } from "react-bootstrap";


export const failedIcon = (name, status, error = null) =>

  error === null
    ?
    <OverlayTrigger
      placement={"top"}
      overlay={
        name
          ? <Popover>
            <strong>{name}</strong>: {status}
          </Popover>
          : <Popover>
            <strong>{status}</strong>
          </Popover>}
    >
      <span className="span-container">  
        <span className="step-icon-failed">
          <i
            className="fa fa-times fa-stack-sm"
            style={{ color: "red", position: "relative", top: "5px" }}
          />
        </span>
        <i
          id="show"
          className="fas fa-sort-up arrow-icon-red"
        />
      </span>
    </OverlayTrigger>

    :
    <OverlayTrigger
      placement={"bottom"}
      overlay={
        <Popover id={"1"}>
          <strong style={{ wordWrap: "break-word", whiteSpace: "pre-wrap" }}>{JSON.stringify(error, null, "\t")}</strong>
        </Popover>}
    >      
      <OverlayTrigger
        placement={"top"}
        overlay={
          name
            ? <Popover>
              <strong>{name}</strong>: {status}
            </Popover>
            : <Popover>
              <strong>{status}</strong>
            </Popover>}
      >
        <span className="span-container">  
          <span className="step-icon-failed">
            <i
              className="fa fa-times fa-stack-sm"
              style={{ color: "red", position: "relative", top: "5px" }}
            />
          </span>
          <i
            id="show"
            className="fas fa-sort-up arrow-icon-red"
          />
        </span>
      </OverlayTrigger>
    </OverlayTrigger>;