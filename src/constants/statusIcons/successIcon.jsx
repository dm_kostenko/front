import React from "react";
import { OverlayTrigger, Popover } from "react-bootstrap";

export const successIcon = (name, status) =>
  <OverlayTrigger 
    placement={"top"}
    overlay={
      name
        ? <Popover>
          <strong>{name}</strong>: {status}
        </Popover>
        : <Popover>
          <strong>{status}</strong>
        </Popover>}
  >
    <span className="span-container">
      <span className="step-icon-success">
        <i
          className="fa fa-check fa-stack-sm"
          style={{ color: "green", position: "relative", top: "5px" }}
        />
      </span>
      <i
        id="show"
        className="fas fa-sort-up arrow-icon-green"
      />
    </span>
  </OverlayTrigger>;