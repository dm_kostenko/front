import React from "react";
import { OverlayTrigger, Popover } from "react-bootstrap";

export const waitingIcon = (name, status) => 
  <OverlayTrigger 
    placement={"top"}
    overlay={
      name
        ? <Popover>
          <strong>{name}</strong>: {status}
        </Popover>
        : <Popover>
          <strong>{status}</strong>
        </Popover>}
  >
    <span className="span-container">  
      <span className="step-icon-waiting" >
        <i
          className="fas fa-circle fa-stack-sm"
          style={{ color: "rgb(202, 202, 202)", position: "relative", top: "5px", left: "2px" }}
        />
      </span>
      <i
        id="show"
        className="fas fa-sort-up arrow-icon-grey"
      />
    </span>
  </OverlayTrigger>;