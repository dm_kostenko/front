import React from "react";
import { OverlayTrigger, Popover } from "react-bootstrap";

export const pendingIcon = (name, status) =>
  <OverlayTrigger 
    placement={"top"}
    overlay={
      name
        ? <Popover>
          <strong>{name}</strong>: {status}
        </Popover>
        : <Popover>
          <strong>{status}</strong>
        </Popover>
    }>
    <span className="span-container">  
      <span className="step-icon-pending">
        <i
          className="fas fa-circle fa-stack-sm"
          style={{ color: "rgb(73, 73, 253)", position: "relative", top: "5px", left: "2px" }}              
        />
      </span>
      <i
        id="show"
        className="fas fa-sort-up arrow-icon-blue"
      />
    </span>
  </OverlayTrigger>;