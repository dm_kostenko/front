import Home from "../ibankViews/Home";
import Dashboard from "ibankViews/Dashboard";
import Transactions from "../ibankViews/Transactions";
import InternalDetail from "ibankComponents/Transactions/Detail/Internal";
import FFCCTRNSDetail from "ibankComponents/Transactions/Detail/FFCCTRNS";
import FFPCRQSTDetail from "ibankComponents/Transactions/Detail/FFPCRQST";
import FFPSRPRTDetail from "ibankComponents/Transactions/Detail/FFPSRPRT";
import PRTRNDetail from "ibankComponents/Transactions/Detail/PRTRN";
import ROINVSTGDetail from "ibankComponents/Transactions/Detail/ROINVSTG";
import Users from "ibankViews/Users";
import UserDetail from "ibankComponents/Users/Detail";
import Accounts from "ibankViews/Accounts";
import AccountDetail from "ibankComponents/Accounts/Detail";
import AccountsStatements from "ibankViews/AccountsStatements";
import AccountsBalances from "ibankViews/AccountsBalances";
import Profile from "ibankViews/Profile";
import Transfers from "ibankViews/Transfers";
import Rates from "ibankViews/Rates";
import RateDetail from "ibankComponents/Rates/Detail";
import Inbox from "ibankViews/Inbox";
import InboxDetail from "ibankComponents/Documents/Detail/Inbox";
import Support from "ibankViews/Support";
import { isRoute } from "helpers/isRouteAvailable";
import { getUserType, getUsername } from "../services/paymentBackendAPI/backendPlatform";
import TxRules from "ibankViews/TxRules";
import Payments from "ibankViews/Payments";
import TransactionsTemplatesSubmit from "ibankComponents/Transactions/Templates/Submit";
import RateEditor from "ibankComponents/Rates/Editor";
import AmlHistory from "ibankViews/AmlHistory";
import AmlDetail from "ibankComponents/AML/Detail";
import AmlDetailEntity from "ibankComponents/AML/Detail/Entity";
import AmlCheck from "ibankViews/AmlCheck";
import AmlTags from "ibankViews/AmlTags";


let navRoutes = [];
let nonNavRoutes = [];

const type = getUserType();
const username = getUsername();

switch(type) {
case "admin":
  if(username == "reportuser")  { // crutch
    navRoutes = [
      { text: "Accounts", routes: [
        {
          path: "/statements",
          name: "Accounts statements",
          icon: "pe-7s-note2",
          component: AccountsStatements
        },
        {
          path: "/balances",
          name: "Accounts balances",
          icon: "pe-7s-note2",
          component: AccountsBalances
        }    
      ] },
      { text: "Transactions", routes: [
        {
          path: "/transactions",
          name: "Transactions history",
          icon: "pe-7s-credit",
          component: Transactions
        }, 
        {
          path: "/payments",
          name: "Payments",
          icon: "pe-7s-credit",
          component: Payments
        },
      ] },
      { redirect: true, path: "/", to: "/statements", name: "Accounts statements" }  
    ];
    nonNavRoutes = [
      {
        path: "/templatessubmit",
        name: "about",
        component: TransactionsTemplatesSubmit
      },
      {
        path: "/waitingpayments",
        name: "about",
        component: TransactionsTemplatesSubmit
      }
    ];    
  }
  else if(username === "andrea@tbffinance.com") {
    navRoutes = [
      { text: "Dashboard", routes: [
        {
          path: "/dashboard",
          name: "Dashboard",
          icon: "pe-7s-graph1",
          component: Home
        }
      ] },
      { text: "Users", routes: [
        {
          path: "/users",
          name: "Users list",
          icon: "pe-7s-users",
          component: Users
        }
      ] },      
      { text: "Accounts", routes: [
        {
          path: "/accounts",
          name: "Accounts list",
          icon: "pe-7s-note2",
          component: Accounts
        },
        {
          path: "/statements",
          name: "Accounts statements",
          icon: "pe-7s-note2",
          component: AccountsStatements
        },
        {
          path: "/balances",
          name: "Accounts balances",
          icon: "pe-7s-note2",
          component: AccountsBalances
        }
      ] },  
      { text: "Transactions", routes: [
        {
          path: "/transactions",
          name: "Transactions history",
          icon: "pe-7s-credit",
          component: Transactions
        },
        {
          path: "/txrules",
          name: "Transaction rules",
          icon: "pe-7s-credit",
          component: TxRules
        },
        {
          path: "/rates",
          name: "Rates",
          icon: "pe-7s-credit",
          component: Rates
        }
      ] },  
      { text: "Docs", routes: [
        {
          path: "/inbox",
          name: "Inbox",
          icon: "pe-7s-right-arrow",
          component: Inbox
        }
      ] },
      { text: "AML", routes: [
        {
          path: "/searchaml",
          name: "Search",
          icon: "pe-7s-graph1",
          component: AmlCheck
        },
        {
          path: "/aml",
          name: "Case Management",
          icon: "pe-7s-graph1",
          component: AmlHistory
        },
        {
          path: "/tagsaml",
          name: "Tags",
          icon: "pe-7s-graph1",
          component: AmlTags
        }
      ] },
      { redirect: true, path: "/", to: "/dashboard", name: "Dashboard" }
    ]

    nonNavRoutes = [
      {
        path: "/users/:id",
        name: "about",
        component: UserDetail
      },
      {
        path: "/accounts/:id",
        name: "about",
        component: AccountDetail
      },
      {
        path: "/rates/:id/:name",
        name: "about",
        component: RateDetail
      },
      {
        path: "/rates_edit/:id",
        name: "about",
        component: RateEditor
      },
      {
        path: "/transactions/internal/:id",
        name: "about",
        component: InternalDetail
      },
      {
        path: "/transactions/ffcctrns/:id",
        name: "about",
        component: FFCCTRNSDetail
      },
      {
        path: "/transactions/ffpcrqst/:id",
        name: "about",
        component: FFPCRQSTDetail
      },
      {
        path: "/transactions/ffpsrprt/:id",
        name: "about",
        component: FFPSRPRTDetail
      },
      {
        path: "/transactions/prtrn/:id",
        name: "about",
        component: PRTRNDetail
      },
      {
        path: "/transactions/roinvstg/:id",
        name: "about",
        component: ROINVSTGDetail
      },
      {
        path: "/documents/inbox/:id",
        name: "about",
        component: InboxDetail
      },
      {
        path: "/aml/:searchId/entity/:entityId",
        name: "about",
        component: AmlDetailEntity
      },
      {
        path: "/aml/:id",
        name: "about",
        component: AmlDetail
      }
    ];
  }
  else {
    navRoutes = [
      { text: "Dashboard", routes: [
        {
          path: "/dashboard",
          name: "Dashboard",
          icon: "pe-7s-graph1",
          component: Home
        }
      ] },
      { text: "Users", routes: [
        {
          path: "/users",
          name: "Users list",
          icon: "pe-7s-users",
          component: Users
        }
      ] },      
      { text: "Accounts", routes: [
        {
          path: "/accounts",
          name: "Accounts list",
          icon: "pe-7s-note2",
          component: Accounts
        },
        {
          path: "/statements",
          name: "Accounts statements",
          icon: "pe-7s-note2",
          component: AccountsStatements
        },
        {
          path: "/balances",
          name: "Accounts balances",
          icon: "pe-7s-note2",
          component: AccountsBalances
        }
      ] },  
      { text: "Transactions", routes: [
        {
          path: "/transactions",
          name: "Transactions history",
          icon: "pe-7s-credit",
          component: Transactions
        },
        {
          path: "/txrules",
          name: "Transaction rules",
          icon: "pe-7s-credit",
          component: TxRules
        },
        {
          path: "/payments",
          name: "Payments",
          icon: "pe-7s-credit",
          component: Payments
        },
        {
          path: "/rates",
          name: "Rates",
          icon: "pe-7s-credit",
          component: Rates
        }
      ] },  
      { text: "Docs", routes: [
        {
          path: "/inbox",
          name: "Inbox",
          icon: "pe-7s-right-arrow",
          component: Inbox
        }
      ] },
      { text: "AML", routes: [
        {
          path: "/searchaml",
          name: "Search",
          icon: "pe-7s-graph1",
          component: AmlCheck
        },
        {
          path: "/aml",
          name: "Case Management",
          icon: "pe-7s-graph1",
          component: AmlHistory
        },
        {
          path: "/tagsaml",
          name: "Tags",
          icon: "pe-7s-graph1",
          component: AmlTags
        }
      ] },
      { redirect: true, path: "/", to: "/dashboard", name: "Dashboard" }
    ];
    nonNavRoutes = [
      {
        path: "/templatessubmit",
        name: "about",
        component: TransactionsTemplatesSubmit
      },
      {
        path: "/waitingpayments",
        name: "about",
        component: TransactionsTemplatesSubmit
      },
      {
        path: "/users/:id",
        name: "about",
        component: UserDetail
      },
      {
        path: "/accounts/:id",
        name: "about",
        component: AccountDetail
      },
      {
        path: "/rates/:id/:name",
        name: "about",
        component: RateDetail
      },
      {
        path: "/rates_edit/:id",
        name: "about",
        component: RateEditor
      },
      {
        path: "/transactions/internal/:id",
        name: "about",
        component: InternalDetail
      },
      {
        path: "/transactions/ffcctrns/:id",
        name: "about",
        component: FFCCTRNSDetail
      },
      {
        path: "/transactions/ffpcrqst/:id",
        name: "about",
        component: FFPCRQSTDetail
      },
      {
        path: "/transactions/ffpsrprt/:id",
        name: "about",
        component: FFPSRPRTDetail
      },
      {
        path: "/transactions/prtrn/:id",
        name: "about",
        component: PRTRNDetail
      },
      {
        path: "/transactions/roinvstg/:id",
        name: "about",
        component: ROINVSTGDetail
      },
      {
        path: "/documents/inbox/:id",
        name: "about",
        component: InboxDetail
      },
      {
        path: "/aml/:searchId/entity/:entityId",
        name: "about",
        component: AmlDetailEntity
      },
      {
        path: "/aml/:id",
        name: "about",
        component: AmlDetail
      }
    ];
  }
  break;
case "individual":
  navRoutes = [
    { text: "Dashboard", routes: [
      {
        path: "/dashboard",
        name: "Dashboard",
        icon: "pe-7s-graph1",
        component: Dashboard
      }
    ] },
    { text: "Accounts", routes: [
      {
        path: "/accounts",
        name: "Accounts list",
        icon: "pe-7s-note2",
        component: Accounts
      }    
    ] },  
    { text: "Transfers", routes: [
      {
        path: "/transfers",
        name: "Transfers history",
        icon: "pe-7s-credit",
        component: Transfers
      },
      {
        path: "/payments",
        name: "Payments",
        icon: "pe-7s-credit",
        component: Payments
      },
    ] },
    { text: "Profile", routes: [
      {
        path: "/profile",
        name: "Profile info",
        icon: "pe-7s-user",
        component: Profile
      }
    ] },
    // { text: "Support", routes: [
    //   {
    //     path: "/support",
    //     name: "Support",
    //     icon: "pe-7s-graph1",
    //     component: Support
    //   }
    // ] },
    { redirect: true, path: "/", to: "/dashboard", name: "Dashboard" }
  ];

  navRoutes = navRoutes.map(route => {
    if(route.routes) {
      route.routes = route.routes.map(item => { return isRoute(item); }).filter(route => route);
      return route.routes.length > 0 ? route : false;
    }
    return isRoute(route);
  });
  navRoutes = navRoutes.filter(route => route);
  nonNavRoutes = [
    {
      path: "/accounts/:id",
      name: "about",
      component: AccountDetail
    },
  ];
  break;

case "entity":
  navRoutes = [
    { text: "Dashboard", routes: [
      {
        path: "/dashboard",
        name: "Dashboard",
        icon: "pe-7s-graph1",
        component: Dashboard
      }
    ] },
    { text: "Accounts", routes: [
      {
        path: "/accounts",
        name: "Accounts list",
        icon: "pe-7s-note2",
        component: Accounts
      }    
    ] },  
    { text: "Transfers", routes: [
      {
        path: "/transfers",
        name: "Transfers history",
        icon: "pe-7s-credit",
        component: Transfers
      },
      {
        path: "/payments",
        name: "Payments",
        icon: "pe-7s-credit",
        component: Payments
      },
    ] },
    { text: "Profile", routes: [
      {
        path: "/profile",
        name: "Profile info",
        icon: "pe-7s-user",
        component: Profile
      }
    ] },
    // { text: "Support", routes: [
    //   {
    //     path: "/support",
    //     name: "Support",
    //     icon: "pe-7s-graph1",
    //     component: Support
    //   }
    // ] },
    { redirect: true, path: "/", to: "/dashboard", name: "Dashboard" }
  ];

  nonNavRoutes = [
    {
      path: "/accounts/:id",
      name: "about",
      component: AccountDetail
    },
  ];

  break;

case "finance":
  navRoutes = [
    { text: "Accounts", routes: [
      {
        path: "/statements",
        name: "Accounts statements",
        icon: "pe-7s-note2",
        component: AccountsStatements
      },
      {
        path: "/balances",
        name: "Accounts balances",
        icon: "pe-7s-note2",
        component: AccountsBalances
      }    
    ] },
    { text: "Transactions", routes: [
      {
        path: "/transactions",
        name: "Transactions history",
        icon: "pe-7s-credit",
        component: Transactions
      }, 
      {
        path: "/payments",
        name: "Payments",
        icon: "pe-7s-credit",
        component: Payments
      },
    ] },
    { redirect: true, path: "/", to: "/statements", name: "Accounts statements" }  
  ];
  nonNavRoutes = [
    {
      path: "/templatessubmit",
      name: "about",
      component: TransactionsTemplatesSubmit
    },
    {
      path: "/waitingpayments",
      name: "about",
      component: TransactionsTemplatesSubmit
    }
  ];   

  break;

case "aml":
  navRoutes = [
    { text: "AML", routes: [
        {
          path: "/searchaml",
          name: "Search",
          icon: "pe-7s-graph1",
          component: AmlCheck
        },
        {
          path: "/aml",
          name: "Case Management",
          icon: "pe-7s-graph1",
          component: AmlHistory
        },
        {
          path: "/tagsaml",
          name: "Tags",
          icon: "pe-7s-graph1",
          component: AmlTags
        }
      ] 
    },
    { redirect: true, path: "/", to: "/aml", name: "AML" }
  ]

  break;

default: break;
}



export { navRoutes };
export { nonNavRoutes };