// import { AbilityBuilder } from "casl";
// import { getPermissions } from "../services/paymentBackendAPI/backendPlatform";

// const getComponentsNames = () => {
//   const permissions = getPermissions();
//   if(permissions) {
//     let set = new Set();
//     permissions.forEach(permission => {
//       let name = permission.name.split("_")[1].toUpperCase();
//       set.add(name);
//     });
//     let arr = [];
//     set.forEach(item => arr = [ ...arr, item ]);
//     return arr;
//   }
//   else {
//     return null;
//   }

// };

// // this function is used in getFuncString below
// const getComponentPermissions = componentName => {
//   const permissions = getPermissions();
//   if(permissions) {
//     let arr = [];
//     permissions.forEach(permission => {
//       if(permission.name.split("_")[1].toUpperCase() === componentName.toUpperCase())
//         arr = [ ...arr, permission.name.split("_")[0] ];
//     });
//     return arr;
//   }
//   else return [ "null" ];
// };

// const getFuncsString = () => {
//   let str = "";
//   const names = getComponentsNames();
//   if(names) {
//     names.forEach(name => {
//       str += `can(getComponentPermissions('${name}'), '${name}'); `;
//     });
//     return str;
//   }
//   else 
//     return null;
// };

// export default AbilityBuilder.define(async(can) => {
//   await eval(getFuncsString());
// });

import { getPermissions, getUserType } from "../services/paymentBackendAPI/backendPlatform";

const permissions = getPermissions();
const userType = getUserType();

export const ability = (permissionName) => {
  return userType === "individual" ? permissions.includes(permissionName) : true;
};

export const permissionsDictionary = {
  "Accounts list": "READ_ACCOUNTS",
  "Transfers history": "READ_TRANSACTIONS",
  "Send transfer": "CREATE_TRANSACTIONS"
};