const config = {
  node: {
    host: process.env.REACT_APP_BACKEND_HOST
  },
  tfa: process.env.REACT_APP_TFA !== "disable",
  iban: {
    settlement: process.env.REACT_APP_STAGE === "prod" ? {
      internal: "LT813180027116000001",
      external: "LT031020105000031800"
    }
    : {
      internal: "LT283180110201010000",
      external: "LT731020105000031801"
    },
    customer: process.env.REACT_APP_STAGE === "prod" ? {
      internal: "LT223180027123000001",
      external: "LT931020103000031800"
    } 
    : {
      internal: "LT273180110201030000",
      external: "LT661020103000031801"
    }
  },
  bic: process.env.REACT_APP_STAGE === "prod" ? "TBFULT21XXX" : "TBFULT31XXX"
};


export default config;