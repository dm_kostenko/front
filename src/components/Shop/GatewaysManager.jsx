import React, { Component } from "react";
import { connect } from "react-redux";
import { upsertChangedGatewayAction } from "../../actions/shops";
import GatewayManager from "./GatewayManager";
import PropTypes from "prop-types";

class GatewaysManager extends Component {
  state = {
    shopGuid: "",
    allGateways: [],
    shopGateways: [],
    notShopGateways: [],
    showGuid: "",
    changedGateways: [],
    firstMount: false,
    filteredShopGateways: [],
    filteredNotShopGateways: [],
    query: ""
  };

  componentDidMount() {
    if (this.props.newShop) {
      const allGateways = this.props.gateways;
      this.setState({
        allGateways,
        filteredNotShopGateways: allGateways,
        firstMount: true
      });
    }
    else {
      const shopGateways = this.props.shopGateways;
      const allGateways = this.props.gateways;
      let notShopGateways;
      this.setState({ allGateways });
      if (!shopGateways || shopGateways.length === 0) {
        this.setState({
          filteredNotShopGateways: allGateways
        });
      }
      else {
        let shopGatewaysGuids = [];
        for (let i = 0; i < shopGateways.length; i += 1) {
          shopGatewaysGuids = [ ...shopGatewaysGuids, shopGateways[i].guid ];
        }
        notShopGateways = allGateways.filter(gateway => !shopGatewaysGuids.includes(gateway.guid));
        this.setState({
          shopGateways,
          notShopGateways,
          filteredShopGateways: shopGateways,
          filteredNotShopGateways: []
        });
      }
      this.setState({
        firstMount: true
      });
    }
  }


  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.shopGateways !== prevState.shopGateways && prevState.firstMount) {
      const allGateways = prevState.allGateways;
      const shopGateways = nextProps.shopGateways;
      let notShopGateways;
      if (!shopGateways || shopGateways.length === 0) {
        return {
          shopGuid: nextProps.shop_guid,
          allGateways,
          notShopGateways: allGateways,
          shopGateways: []
        };
      }
      else {
        let shopGatewaysGuids = [];
        for (let i = 0; i < shopGateways.length; i += 1) {
          shopGatewaysGuids = [ ...shopGatewaysGuids, shopGateways[i].guid ];
        }
        notShopGateways = allGateways.filter(gateway => !shopGatewaysGuids.includes(gateway.guid));
        return {
          shopGuid: nextProps.shop_guid,
          allGateways,
          shopGateways,
          notShopGateways
        };
      }
    }
    else {
      return {
        shopGuid: nextProps.shop_guid
      };
    }
  }

  returnGateway = (guid) => {
    this.state.allGateways.map(gateway => {
      if (gateway.guid === guid) return gateway;
      else return null;
    });
  }
  handleDelete = async (e) => {
    const guid = e.target.id;
    if (guid === this.state.showGuid)
      this.setState({ showGuid: "" });

    const newDel = {
      guid,
      delete: true
    };
    await this.props.upsertChangedGatewayAction(newDel);
    this.filterGateways(this.state.query);
  };

  handleAdd = async (e) => {
    const guid = e.target.id.split(" ")[0];
    const name = e.target.id.split(" ")[1];
    const newAdd = {
      guid,
      name
    };
    this.setState({ changedGateways: [ ...this.state.changedGateways, newAdd ], showGuid: guid });
    await this.props.upsertChangedGatewayAction(newAdd);
    this.filterGateways(this.state.query);
  };

  handleSet = (e) => {
    if (e.target.id === this.state.showGuid)
      this.setState({
        showGuid: ""
      });
    else
      this.setState({
        showGuid: e.target.id
      });
  }

  filterGateways = (query) => {
    const filteredShopGateways = this.state.shopGateways.filter(gateway => {
      return gateway.name.toLowerCase().includes(query.toLowerCase());
    });
    const filteredNotShopGateways = this.state.notShopGateways.filter(gateway => {
      return gateway.name.toLowerCase().includes(query.toLowerCase());
    });
    if (!query && this.state.shopGateways.length === 0) {
      this.setState({
        query,
        filteredShopGateways: this.state.shopGateways,
        filteredNotShopGateways: this.state.notShopGateways
      });
    }
    else if (!query) {
      this.setState({
        query,
        filteredShopGateways: this.state.shopGateways,
        filteredNotShopGateways: []
      });
    } else {
      this.setState({
        query,
        filteredShopGateways,
        filteredNotShopGateways
      });
    }
  }

  handleInputChange = (event) => {
    const query = event.target.value;
    this.filterGateways(query);

  }

  render() {
    const { filteredShopGateways, filteredNotShopGateways, showGuid } = this.state;
    if (filteredShopGateways.length === 0 && filteredNotShopGateways.length === 0)
      return (
        <div>
          <n />
          <label htmlFor="">There are no available gateways</label>
        </div>
      );
    let row = [];
    filteredShopGateways.forEach(item => {
      row = [ ...row,
        <div key={item.guid}>
          <li className="list-group-item" style={{ textAlign: "right", backgroundColor: "#87cefa" }}>
            {item.name + " "}
            <i
              id={item.guid}
              className="fas fa-bars fa-lg"
              style={{ cursor: "pointer", color: "black", marginRight: "10px", marginLeft: "15px" }}
              onClick={this.handleSet}
            />
            <i
              id={item.guid}
              className="far fa-trash-alt fa-lg"
              style={{ cursor: "pointer", color: "red" }}
              onClick={this.handleDelete}
            />
          </li>
          {showGuid === item.guid
            ? <GatewayManager
              shopGuid={this.props.shop_guid}
              gatewayGuid={item.guid}
              save={this.save}
            />
            : ""
          }
        </div> ];
    });
    let newRow = [];
    filteredNotShopGateways.forEach(item => {
      newRow = [ ...newRow,
        <li key={item.guid} className="list-group-item" style={{ textAlign: "right" }}>
          {item.name + " "}
          <i
            id={item.guid + " " + item.name}
            className="fas fa-plus fa-2x"
            style={{ cursor: "pointer", color: "#1e90ff", marginRight: "10px", marginLeft: "25px" }}
            onClick={this.handleAdd}
          />
        </li> ];
    });
    row = [ ...row, newRow ];

    return (
      <div>
        <div className="search-box">
          <form className="form-inline md-form form-sm mt-0">
            <input
              className="form-control form-control-sm ml-3 w-75"
              type="text"
              placeholder="Search"
              aria-label="Search"
              value={this.state.query}
              onChange={e => this.handleInputChange(e)}
            />{"    "}
            <i className="fas fa-search" aria-hidden="true"></i>
          </form>
        </div>
        <ul className="list-group">{row}</ul>
      </div>);
  }
}

const mapStateToProps = (state) => {
  return {
    shopGateways: state.shops.shopGateways,
    gateways: state.gateways.gatewaysList,
  };
};

export default connect(mapStateToProps, {
  upsertChangedGatewayAction
})(GatewaysManager);

GatewaysManager.propTypes = {
  shopGateways: PropTypes.array,
  gateways: PropTypes.array,
  newShop: PropTypes.object,
  shop_guid: PropTypes.string,
  upsertChangedGatewayAction: PropTypes.func,
};