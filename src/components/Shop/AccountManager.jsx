import React, { Component } from "react";
import { connect } from "react-redux";
import { upsertChangedAccountAction } from "actions/shops";
import PropTypes from "prop-types";

class AccountManager extends Component {
  state = {
    shopGuid: "",
    shopAccounts: [],
    notShopAccounts: [],
    filteredShopAccounts: [],
    filteredNotShopAccounts: [],
    changedAccounts: [],
    query: ""
  }

  async componentDidMount() {
    this.setState({ shopGuid: this.props.shopGuid });
    const shopAccounts = this.props.shopAccounts;
    const allAccounts = this.props.accounts;
    let notShopAccounts;
    this.setState({ allAccounts });
    if (!shopAccounts || shopAccounts.length === 0) {
      this.setState({
        notShopAccounts: allAccounts,
        filteredNotShopAccounts: allAccounts
      });
    }
    else {
      let shopAccountsGuids = [];
      for (let i = 0; i < shopAccounts.length; i += 1) {
        shopAccountsGuids = [ ...shopAccountsGuids, shopAccounts[i].guid ];
      }
      notShopAccounts = allAccounts.filter(account => !shopAccountsGuids.includes(account.guid));
      this.setState({
        shopAccounts,
        notShopAccounts,
        filteredShopAccounts: shopAccounts,
        filteredNotShopAccounts: []
      });
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.shopAccounts !== prevState.shopAccounts && prevState.firstMount) {
      const allAccounts = prevState.allAccounts;
      const shopAccounts = nextProps.shopAccounts;
      let notShopAccounts;
      if (!shopAccounts || shopAccounts.length === 0) {
        return {
          shopGuid: nextProps.shop_guid,
          showMore: true,
          notShopAccounts: allAccounts,
          shopAccounts: []
        };
      }
      else {
        let shopAccountsGuids = [];
        for (let i = 0; i < shopAccounts.length; i += 1) {
          shopAccountsGuids = [ ...shopAccountsGuids, shopAccounts[i].guid ];
        }
        notShopAccounts = allAccounts.filter(account => !shopAccountsGuids.includes(account.guid));
        return {
          shopGuid: nextProps.shop_guid,
          shopAccounts,
          notShopAccounts
        };
      }
    }
    else {
      return {
        shopGuid: nextProps.shop_guid
      };
    }
  }

  returnAccount = (guid) => {
    this.state.allAccounts.map(account => {
      if (account.guid === guid) return account;
      else return null;
    });
  }

  handleDelete = async (e) => {
    const guid = e.target.id;
    const shopAccounts = this.state.shopAccounts.filter(account => account.guid !== guid);
    const account = this.state.shopAccounts.filter(account => account.guid === guid);
    const notShopAccounts = [ account[0], ...this.state.notShopAccounts ];
    this.setState({
      notShopAccounts,
      shopAccounts
    });
    const newDel = {
      guid,
      delete: true
    };
    this.setState({ changedAccounts: [ ...this.state.changedAccounts, newDel ] });
    await this.props.upsertChangedAccountAction(newDel);
    this.filterAccounts(this.state.query);
  }

  handleAdd = async (e) => {
    const guid = e.target.id;
    const notShopAccounts = this.state.notShopAccounts.filter(account => account.guid !== guid);
    const account = this.state.notShopAccounts.filter(account => account.guid === guid);
    const shopAccounts = [ ...this.state.shopAccounts, account[0] ];
    this.setState({
      notShopAccounts,
      shopAccounts
    });
    const newAdd = {
      guid
    };
    this.setState({ changedAccounts: [ ...this.state.changedAccounts, newAdd ] });
    await this.props.upsertChangedAccountAction(newAdd);
    this.filterAccounts(this.state.query);
  }

  filterAccounts = (query) => {
    const filteredShopAccounts = this.state.shopAccounts.filter(account => {
      return account.number.toLowerCase().includes(query.toLowerCase());
    });
    const filteredNotShopAccounts = this.state.notShopAccounts.filter(account => {
      return account.number.toLowerCase().includes(query.toLowerCase());
    });
    if (!query && this.state.shopAccounts.length === 0) {
      this.setState({
        query,
        filteredShopAccounts: this.state.shopAccounts,
        filteredNotShopAccounts: this.state.notShopAccounts
      });
      return null;
    }
    else if (!query) {
      this.setState({
        query,
        filteredShopAccounts: this.state.shopAccounts,
        filteredNotShopAccounts: []
      });
      return null;
    } else {
      this.setState({
        query,
        filteredShopAccounts,
        filteredNotShopAccounts
      });
    }
  }

  handleInputChange = (event) => {
    const query = event.target.value;
    this.filterAccounts(query);
  }

  render() {
    const { filteredShopAccounts, filteredNotShopAccounts } = this.state;
    let row = [];
    filteredShopAccounts.forEach(item => {
      row = [ ...row,
        <li key={item.guid} className="list-group-item" style={{ textAlign: "right", backgroundColor: "#87cefa" }}>
          {item.number + " "}
          <i
            id={item.guid}
            className="far fa-trash-alt"
            style={{ cursor: "pointer", color: "red", marginLeft: "10px" }}
            onClick={this.handleDelete}
          />
        </li> ];
    });
    let newRow = [];
    filteredNotShopAccounts.forEach(item => {
      newRow = [ ...newRow,
        <li key={item.guid} className="list-group-item" style={{ textAlign: "right" }}>
          {item.number + " "}
          <i
            id={item.guid}
            className="fas fa-plus"
            style={{ cursor: "pointer", color: "#1e90ff", marginLeft: "10px" }}
            onClick={this.handleAdd}
          />
        </li> ];
    });
    row = [ ...row, newRow ];
    return (
      <div>
        <div className="search-box">
          <form className="form-inline md-form form-sm mt-0">
            <input
              className="form-control form-control-sm ml-3 w-75"
              type="text"
              placeholder="Search"
              aria-label="Search"
              value={this.state.query}
              onChange={e => this.handleInputChange(e)}
            />{"    "}
            <i className="fas fa-search" aria-hidden="true"></i>
          </form>
        </div>
        <ul className="list-group">{row}</ul>
      </div>);
  }
}

const mapStateToProps = (state) => {
  return {
    shopAccounts: state.shops.shopAccounts,
    accounts: state.accounts.accountsList
  };
};

export default connect(mapStateToProps, {
  upsertChangedAccountAction
})(AccountManager);

AccountManager.propTypes = {
  shopAccounts: PropTypes.array,
  accounts: PropTypes.array,
  shop_guid: PropTypes.string,
  shopGuid: PropTypes.string,
  upsertChangedAccountAction: PropTypes.func,
};