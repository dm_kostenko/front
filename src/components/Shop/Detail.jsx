import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import Table from "components/UI/Table/index";
import Pagination from "components/UI/Pagination/index";
import Modal from "components/UI/Modal";
import GatewayPropsEditor from "./GatewayProps/Editor";
import { connect } from "react-redux";
import { getShopAction, getShopGatewaysAction, getShopAccountsAction, getShopGatewayPropsAction } from "actions/shops";
import moment from "moment";
import { getAuthData } from "services/paymentBackendAPI/backendPlatform";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";

class ShopDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentGatewaysPage: 1,
      currentAccountsPage: 1,
      pageSize: 10,
      isSecretCopied: false,
      isHashKeyCopied: false
    };
  }

  columnsShopGateways = [
    { path: "guid", label: "Guid" },
    { path: "name", label: "Gateway" },
    { path: "description", label: "Description" },
    { path: "billing_descriptor", label: "Billing descriptor" },
    { path: "routing_string", label: "Routing string" },
    { path: "supported_brands", label: "Supported brands" },
    { path: "supported_currencies", label: "Supported currencies" },
    { path: "payment_amount_limit", label: "Payment amount limit" },
    { path: "monthly_amount_limit", label: "Monthly amount limit" },
    {
      key: "generate_statement",
      content: gateway => (
        <i className={gateway.generate_statement === 1 ? "fas fa-check-circle green" : "fas fa-times-circle red"} />
      ),
      label: "Generate statement"
    },
    {
      key: "enabled",
      content: gateway => (
        <i className={gateway.enabled === 1 ? "fas fa-check-circle green" : "fas fa-times-circle red"} />
      ),
      label: "Enabled"
    },
    {
      label: "Properties",
      content: (gateway) =>
        (
          <Modal
            header="Properties"
            content={
              <div style={{ whiteSpace: "pre-wrap", padding: "30px" }}>
                <GatewayPropsEditor gatewayGuid={gateway.guid} shopGuid={this.props.match.params.id} />
              </div>}
            buttonName="Show"
            button={
              <button
                type="button"
                className="btn"
                style={{ marginBottom: "-15px", marginTop: "-15px" }}
              >
                Show
              </button>
            }
          />
        )
    },
  ];

  columnsShopAccounts = [
    { path: "guid", label: "Guid" },
    { path: "number", label: "Account number" },
    { path: "currency_guid", label: "Currency" },
    { path: "balance", label: "Balance" }
  ];



  componentDidMount = async () => {
    const token = getAuthData();
    const guid = this.props.match.params.id;
    if (token.userPayload.merchant)
      this.setState({ isMerchant: true });
    await this.props.getShopAction(guid);
    await this.props.getShopGatewaysAction(guid, 1, this.state.pageSize);
    await this.props.getShopAccountsAction(guid, 1, this.state.pageSize);
  };

  handleGatewaysPageChange = async (page) => {
    await this.props.getShopGatewaysAction(this.props.match.params.id, page, this.state.pageSize);
    this.setState({
      currentGatewaysPage: page
    });
  };

  handleGatewaysPageChange = async (page) => {
    await this.props.getShopAccountsAction(this.props.match.params.id, page, this.state.pageSize);
    this.setState({
      currentAccountsPage: page
    });
  };


  update = (id) => {
    this.props.getShopAction(id);
    this.props.getShopGatewaysAction(id);
    this.props.getShopAccountsAction(id);
  };

  getRowsFromLength = (text) => {
    if (!text)
      return 0;
    let res = text.length / 35 + 1;
    if (text.length % 35)
      res++;
    return res;
  }

  handleCopy = (target) => {
    var textField = document.createElement("textarea");
    if (target === "secret") {
      textField.innerText = this.props.shop.secret;
      document.body.appendChild(textField);
      textField.select();
      document.execCommand("copy");
      textField.remove();
      this.setState({
        isSecretCopied: true,
        isHashKeyCopied: false,
      });
    }
    else {
      textField.innerText = this.props.shop.hash_key;
      document.body.appendChild(textField);
      textField.select();
      document.execCommand("copy");
      textField.remove();
      this.setState({
        isSecretCopied: false,
        isHashKeyCopied: true,
      });
    }
    setTimeout(() =>
      this.setState({
        isSecretCopied: false,
        isHashKeyCopied: false,
      }), 3000
    );
  }


  getGatewaysPagedData = () => {
    const { gatewaysCount } = this.props;
    let { pageSize } = this.state;
    const gatewaysPagesCount = gatewaysCount ? (gatewaysCount / pageSize) + (1 && !!(gatewaysCount % pageSize)) : 0;
    return { gatewaysPagesCount };
  };


  getAccountsPagedData = () => {
    const { accountCount } = this.props;
    let { pageSize } = this.state;
    const accountsPagesCount = accountCount ? (accountCount / pageSize) + (1 && !!(accountCount % pageSize)) : 0;
    return { accountsPagesCount };
  };

  render() {
    const { currentAccountsPage, currentGatewaysPage } = this.state;
    const { shop } = this.props;
    const shopAccounts = this.props.shopAccounts;
    const shopGateways = this.props.shopGateways;
    const { accountsPagesCount } = this.getAccountsPagedData();
    const { gatewaysPagesCount } = this.getGatewaysPagedData();
    if (this.props.shopLoading || this.props.shopGatewaysLoading || this.shopAccountsLoading) return <Spinner/>;
    else return (
      <>
      <Button onClick={this.props.history.goBack} className="detail-back-button"><i className="fas fa-arrow-left"></i></Button>
      <div className="content" style={{ overflow: "auto" }}>
        <Row style={{ marginLeft: "0px" }}>
          <div className="card">
            <div className="title">{shop.name}</div>
          </div>
          <div className="card-container">
            <div className="card">
              <div className="header">Info</div>
              <div className="content">
                <Row >
                  <Col md={6} lg={6} xs={6} className="right"><label>GUID:</label></Col>
                  <Col md={6} lg={6} xs={6}><span>{this.props.match.params.id}</span></Col>
                </Row>
                {this.state.isMerchant &&
                  <React.Fragment>
                    <Row className={"row1"}>
                      <Col md={6} lg={6} xs={6} className="right"><label>Secret:</label></Col>
                      <Col md={5} lg={5} xs={5}>
                        <div
                          id="secret"
                          className="card-shop-secret noselect"
                        >
                          {shop.secret}
                        </div>
                      </Col>
                      <Col md={1}>
                        {shop.hash_key !== undefined ? <Button
                          onClick={() => this.handleCopy("secret")}
                          className={"btn btn-copy"}
                        >
                          {this.state.isSecretCopied
                            ? <div style={{ cursor: "default" }}><span style={{ color: "green" }}>✔</span> Successfully copied</div>
                            : <div>Copy secret</div>}
                        </Button>
                          : null}
                      </Col>
                    </Row>
                    <br />
                    <Row className={"row1"}>
                      <Col md={6} lg={6} xs={6} className="right"><label>Hash-key:</label></Col>
                      <Col md={5} lg={5} xs={5}>
                        <div
                          id="hash-hey"
                          className="card-shop-secret noselect"
                        >
                          {shop.hash_key}
                        </div>
                      </Col>
                      <Col md={1}>
                        {shop.hash_key  !== undefined  ? <Button
                          onClick={() => this.handleCopy("hash-key")}
                          className={"btn btn-copy"}
                          name="hashCopyButton"
                        >
                          {this.state.isHashKeyCopied
                            ? <div style={{ cursor: "default" }}><span style={{ color: "green" }}>✔</span> Successfully copied</div>
                            : <div>Copy hash-key</div>}
                        </Button>
                          : null}

                      </Col>
                    </Row>
                    <br />
                  </React.Fragment>}
                <Row>
                  <Col md={6} lg={6} xs={6} className="right"><label>URL:</label></Col>
                  <Col md={6} lg={6} xs={6}><span>{shop.url}</span></Col>
                </Row>
                <Row>
                  <Col md={6} lg={6} xs={6} className="right"><label>Phone:</label></Col>
                  <Col md={6} lg={6} xs={6}><span>{shop.phone}</span></Col>
                </Row>
                <Row>
                  <Col md={6} lg={6} xs={6} className="right"><label>Email:</label></Col>
                  <Col md={6} lg={6} xs={6}><span>{shop.email}</span></Col>
                </Row>
                <Row>
                  <Col md={6} lg={6} xs={6} className="right"><label>Note:</label></Col>
                  <Col md={6} lg={6} xs={6}><span>{shop.note}</span></Col>
                </Row>
                <Row>
                  <Col md={6} lg={6} xs={6} className="right"><label>Merchant:</label></Col>
                  <Col md={6} lg={6} xs={6}><span>{shop.merchant_guid}</span></Col>
                </Row>
                <Row>
                  <Col md={6} lg={6} xs={6} className="right"><label>Created at:</label></Col>
                  <Col md={6} lg={6} xs={6}><span>{shop.created_at !== null ? moment(shop.created_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
                </Row>
                <Row>
                  <Col md={6} lg={6} xs={6} className="right"><label>Created by:</label></Col>
                  <Col md={6} lg={6} xs={6}><span>{shop.created_by}</span></Col>
                </Row>
                <Row>
                  <Col md={6} lg={6} xs={6} className="right"><label>Updated at:</label></Col>
                  <Col md={6} lg={6} xs={6}><span>{shop.updated_at !== null ? moment(shop.updated_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
                </Row>
                <Row>
                  <Col md={6} lg={6} xs={6} className="right"><label>Updated by:</label></Col>
                  <Col md={6} lg={6} xs={6}><span>{shop.updated_by}</span></Col>
                </Row>
              </div>
            </div>
            <div className="card">
              <div className="header">Accounts</div>
              <div className="content">
                <Row>
                  <Col md={12} sm={12} xs={12}>
                    <Table
                      columns={this.columnsShopAccounts}
                      data={shopAccounts}
                      disableSearch={true}
                    />
                    <Pagination
                      pagesCount={accountsPagesCount}
                      currentPage={currentAccountsPage}
                      onPageChange={this.handleAccountsPageChange}
                    />
                  </Col>
                </Row>
              </div>
            </div>
          </div>
          <div className="card" style={{ overflow: "auto" }}>
            <div className="header">Gateways</div>
            <div className="content">
              <Row>
                <Col md={12} sm={12} xs={12}>
                  <Table
                    columns={this.columnsShopGateways}
                    data={shopGateways}
                    disableSearch={true}
                  />
                  <Pagination
                    pagesCount={gatewaysPagesCount}
                    currentPage={currentGatewaysPage}
                    onPageChange={this.handleGatewaysPageChange}
                  />
                </Col>
              </Row>
            </div>
          </div>
        </Row>
      </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    shop: state.shops.shop,
    shopGateways: state.shops.shopGateways,
    gatewaysCount: state.shops.gatewaysCount,
    shopAccounts: state.shops.shopAccounts,
    accountCount: state.shops.accountCount,
    gatewayProps: state.shops.gatewayProps,
    shopLoading: state.shops.shopLoading,
    shopGatewaysLoading: state.shops.shopGatewaysLoading,
    shopAccountsLoading: state.shops.shopAccountsLoading
  };
};

export default connect(mapStateToProps, {
  getShopAction,
  getShopGatewaysAction,
  getShopAccountsAction,
  getShopGatewayPropsAction
})(ShopDetail);

ShopDetail.propTypes = {
  shop: PropTypes.object,
  shopGateways: PropTypes.array,
  gatewaysCount: PropTypes.number,
  shopAccounts: PropTypes.array,
  accountCount: PropTypes.number,
  shopLoading: PropTypes.bool,
  shopGatewaysLoading: PropTypes.bool,
  shopAccountsLoading: PropTypes.bool,

  getShopAction: PropTypes.func,
  getShopGatewaysAction: PropTypes.func,
  getShopAccountsAction: PropTypes.func,
  getShopGatewayPropsAction: PropTypes.func,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    })
  })
};