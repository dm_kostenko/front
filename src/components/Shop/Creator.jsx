import React, { Component } from "react";
import { Row, Col, FormGroup, Form, ControlLabel, FormControl } from "react-bootstrap";
import Button from "react-bootstrap/es/Button";
import MultiSelect from "components/UI/MultiSelect/index";
import GatewaysManager from "./GatewaysManager";
import { connect } from "react-redux";
import { addShopAction, upsertGatewayToShopAction, addShopAccountAction, getShopGatewaysAction } from "actions/shops";
import { getAllMerchants } from "actions/merchants";
import { getAllGateways } from "actions/gateways";
import { getAllAccounts } from "actions/accounts";
import { showModal } from "actions/modal";
import { getAuthData } from "../../services/paymentBackendAPI/backendPlatform";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner/index";
import swal from "sweetalert";
import ReactLoading from "react-loading";

export class ShopCreator extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: "",
      selectedMerchant: "",
      merchantGuid: "",
      merchantName: "",
      merchants: [],
      gateways: [],
      selectedAccounts: [],
      email: "",
      url: "",
      phone: "",
      enabled: false,
      isMerchant: false,
      token: getAuthData().userPayload,
      isLoading: false
    };
  }

  async componentDidMount() {
    if (this.state.token.merchant !== undefined)
      this.setState({
        selectedMerchant: this.state.token.merchant.merchant_guid,
        isMerchant: true
      });
    else {
      await this.props.getAllMerchants();
    }
    await
    await this.props.getAllGateways();
    await this.props.getAllAccounts();
    this.setState({
      merchants: this.props.merchants,
      gateways: this.props.gateways
    });
  }

  formValidation = (data) => {
    if (data === "" || typeof (data) === "undefined") return "error";
    else return "success";
  }


  onSelectMerchant = (option) => {
    this.setState({
      selectedMerchant: option.guid
    });
  }

  onSelectAccount = (option) => {
    this.setState({
      selectedAccounts: option
    });
  };

  handleChange = (e) => {
    const property = e.target.name;
    this.setState({ [property]: e.target.value });
  };

  handleEnabledCheckbox = () => {
    this.setState({
      enabled: !(this.state.enabled)
    });
  };

  gatewaysValidation = (gateways) => {
    let validate = true;
    if (!gateways) {
      return !validate;
    }
    gateways.forEach(gateway => {
      validate = validate
        && this.formValidation(gateway.guid) === "success"
        && ((this.formValidation(gateway.billing_descriptor) === "success"
          && this.formValidation(gateway.routing_string) === "success"
          && this.formValidation(gateway.payment_amount_limit) === "success"
          && this.formValidation(gateway.monthly_amount_limit) === "success"
          && this.formValidation(gateway.supported_brands) === "success"
          && this.formValidation(gateway.supported_currencies) === "success"
          && this.formValidation(gateway.generate_statement) === "success"
          && this.formValidation(gateway.enabled) === "success")
          || this.formValidation(gateway.delete) === "success");
    });
    return validate;
  }

  generalValidation = () => {
    const data = this.state;
    if (this.formValidation(data.name) === "success" &&
      this.formValidation(data.selectedMerchant) === "success" &&
      this.gatewaysValidation(data.gateways)
    ) return true;
    else return false;
  }


  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    const enabled = this.state.enabled ? 1 : 0;
    let data = {
      guid: this.props.guid,
      name: this.state.name,
      merchant_guid: this.state.selectedMerchant,
      email: this.state.email,
      description: this.state.description,
      url: this.state.url,
      phone: this.state.phone,
      enabled: enabled,
      enable_checkout: 1,
      checkout_method: "",
      antiFraudMonitor: 1,
      antiFraudMonitorValue: 1
    };
    await this.setState({ gateways: this.props.shopGateways });
    const gateways = this.state.gateways;
    const { selectedAccounts } = this.state;
    if (this.generalValidation()) {
      try {
        await this.props.addShopAction(data, this.props.currentPage, this.props.pageSize, this.props.shopsSearch);
        const shopGuid = this.props.shopGuid;
        let dataAccount = [];
        for (let i = 0; i < selectedAccounts.length; i += 1) {
          const account = {
            guid: selectedAccounts[i].guid
          };
          dataAccount = [ ...dataAccount, account ];
        }
        await this.props.addShopAccountAction(shopGuid, dataAccount);
        if (gateways) {
          const req = {
            guid: shopGuid,
            gateways
          };
          await this.props.upsertGatewayToShopAction(req);
        }
        swal({
          title: "Shop is created",
          icon: "success",
          button:false,
          timer: 2000
        });
        await this.props.showModal(false);
      } catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
    }
    else {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    }
  }


  render() {
    if (this.props.gatewaysLoading)
      return <Spinner />;
    else {
      const { merchants, isMerchant } = this.state;
      let { accounts } = this.props;
      if (accounts) accounts.map(account => { return account.name = account.number; });
      return (
        <div className="card">
          <div className="content">
            <Form onSubmit={this.handleSubmit} autoComplete="off">
              <Row className="rowEdit" >
                <Col className='col-md-3'>
                  <ControlLabel>Name*:</ControlLabel>
                </Col>
                <Col className='col-md-9'>
                  <FormGroup validationState={this.formValidation(this.state.name)}>
                    <FormControl
                      name="name"
                      type="text"
                      value={this.state.name}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row className="rowEdit">
                <Col className='col-md-3'>
                  <ControlLabel>Enable*:</ControlLabel>
                </Col>
                <Col className='col-md-4'>
                  <input type="checkbox" id='enabledCheckbox' checked={this.state.enabled} onChange={this.handleEnabledCheckbox} />{" "}
                </Col>
              </Row>

              {!isMerchant &&
                <Row className="rowEdit">
                  <Col className='col-md-3'>
                    <ControlLabel>Merchant*:</ControlLabel>
                  </Col>
                  <Col className='col-md-9'>
                    <MultiSelect
                      name="merchant"
                      options={merchants}
                      multi={false}
                      onSelect={this.onSelectMerchant}
                    />
                  </Col>
                </Row>}
              <Row className="rowEdit" >
                <Col className='col-md-3'>
                  <ControlLabel>URL*:</ControlLabel>
                </Col>
                <Col className='col-md-9'>
                  <FormGroup validationState={this.formValidation(this.state.url)}>
                    <FormControl
                      name="url"
                      type="text"
                      value={this.state.url}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row className="rowEdit" >
                <Col className='col-md-3'>
                  <ControlLabel>Email*:</ControlLabel>
                </Col>
                <Col className='col-md-9'>
                  <FormGroup validationState={this.formValidation(this.state.email)}>
                    <FormControl
                      name="email"
                      type="text"
                      value={this.state.email}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row className="rowEdit" >
                <Col className='col-md-3'>
                  <ControlLabel>Phone*:</ControlLabel>
                </Col>
                <Col className='col-md-9'>
                  <FormGroup validationState={this.formValidation(this.state.phone)}>
                    <FormControl
                      name="phone"
                      type="text"
                      value={this.state.phone}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row className="rowEdit" >
                <Col className='col-md-3'>
                  <ControlLabel>Gateways*:</ControlLabel>
                </Col>
                <Col className='col-md-9'>
                  <GatewaysManager
                    newShop ={true}
                    shop_guid={this.props.guid}
                  />
                </Col>
              </Row>
              <Row className="rowEdit">
                <Col className='col-md-3'>
                  <ControlLabel>Accounts*:</ControlLabel>
                </Col>
                <Col className='col-md-9'>
                  <MultiSelect
                    name="Shops"
                    options={accounts ? accounts : [ { name: "", guid: "" } ]}
                    multi={true}
                    onSelect={this.onSelectAccount}
                  />
                </Col>
              </Row>

              <div align="center">
                {this.state.isLoading 
                  ? <ReactLoading type='cylon' color='grey' /> 
                  : <Button type="submit">Save</Button>}
              </div>
            </Form>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    merchants: state.merchants.merchantsList,
    gateways: state.gateways.gatewaysList,
    accounts: state.accounts.accountsList,
    shopGateways: state.shops.changedGateways,
    shopGuid: state.shops.shopGuid,
    currentPage: state.shops.currentPage,
    pageSize: state.shops.pageSize,

    gatewaysLoading: state.gateways.gatewaysLoading,

    shopsSearch: state.search.shopsSearch
  };
};

export default connect(mapStateToProps, {
  getShopGatewaysAction,
  addShopAction,
  upsertGatewayToShopAction,
  getAllMerchants,
  getAllGateways,
  getAllAccounts,
  addShopAccountAction,
  showModal
})(ShopCreator);


ShopCreator.propTypes = {
  merchants: PropTypes.array,
  gateways: PropTypes.array,
  accounts: PropTypes.array,
  shopGateways: PropTypes.array,
  shopGuid: PropTypes.string,
  guid: PropTypes.string,

  gatewaysLoading: PropTypes.bool,

  addShopAction: PropTypes.func,
  upsertGatewayToShopAction: PropTypes.func,
  getAllMerchants: PropTypes.func,
  getAllGateways: PropTypes.func,
  getAllAccounts: PropTypes.func,
  addShopAccountAction: PropTypes.func,
  showModal: PropTypes.func,

  currentPage: PropTypes.number,
  pageSize: PropTypes.number,
  shopsSearch: PropTypes.array,
};