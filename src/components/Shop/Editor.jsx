import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Row, FormGroup, ControlLabel, FormControl, Button, Form } from "react-bootstrap";
import GatewaysManager from "./GatewaysManager";
import { getAllMerchants, getMerchantAction } from "../../actions/merchants";
import { getShopAction, editShopAction, upsertGatewayToShopAction, upsertAccountToShopAction, getShopAccountsAction, getShopGatewaysAction } from "../../actions/shops";
import { getAllAccounts } from "../../actions/accounts";
import { getAllGateways } from "../../actions/gateways";
import { showModal } from "../../actions/modal";
import AccountManager from "./AccountManager";
import { parseResponse } from "helpers/parseResponse";
import { getAuthData } from "../../services/paymentBackendAPI/backendPlatform";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class ShopEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      secret: "",
      selectedValue: "",
      merchantGuid: "",
      merchantName: "",
      merchants: [],
      email: "",
      url: "",
      phone: "",
      enabled: false,
      gateways: [],
      changedAccounts: [],
      isMerchant: false,
      generateSecret: false,
      isLoading: false
    };
  }

  async componentDidMount() {
    const token = getAuthData();
    if (token.userPayload.merchant)
      this.setState({ isMerchant: true });
    await this.props.getShopAction(this.props.guid);

    await this.props.getAllAccounts();
    await this.props.getShopAccountsAction(this.props.guid);

    await this.props.getAllGateways();
    await this.props.getShopGatewaysAction(this.props.guid);

    const shop = this.props.shop;
    await this.props.getMerchantAction(shop.merchant_guid);
    if (!this.state.isMerchant)
      await this.props.getAllMerchants();
    const { merchant, merchants } = this.props;
    this.setState({
      name: shop.name,
      secret: shop.secret ? shop.secret : "No secret",
      email: shop.email,
      url: shop.url,
      phone: shop.phone,
      enabled: shop.enabled,
      merchantGuid: merchant.guid,
      selectedValue: merchant.guid,
      merchantName: merchant.name
    });
    if (!this.state.isMerchant)
      this.setState({
        merchants
      });

  }

  spliceMerchantsArray = () => {
    const merchants = this.state.merchants;
    const names = merchants.map(merchant => {
      return merchant.name;
    });
    const index = names.indexOf(this.state.merchantName);
    merchants.splice(index, 1);
    this.setState({ merchants });
  }

  formValidation = (data) => {
    if (data === "" || typeof (data) === "undefined") return "error";
    else return "success";
  }


  handleSelectChange = (e) => {
    this.setState({
      selectedValue: e.target.value,
    });
  }

  handleChange = (e) => {
    const property = e.target.name;
    this.setState({ [property]: e.target.value });
  };

  handleEnabledCheckbox = () => {
    this.setState({
      enabled: !(this.state.enabled)
    });
  };

  handleGenerateSecretCheckbox = () => {
    this.setState({
      generateSecret: !(this.state.generateSecret)
    });
  };

  handleSave = async changedAccounts => {
    const data = { ...this.state.data };
    data.changedAccounts = await changedAccounts;
    this.setState({ data });
  };

  gatewaysValidation = (gateways) => {
    let validate = true;
    if (gateways && gateways.length !== 0)
      gateways.forEach(gateway => {
        validate = validate
          && this.formValidation(gateway.guid) === "success"
          && ((this.formValidation(gateway.billing_descriptor) === "success"
            && this.formValidation(gateway.routing_string) === "success"
            && this.formValidation(gateway.payment_amount_limit) === "success"
            && this.formValidation(gateway.monthly_amount_limit) === "success"
            && this.formValidation(gateway.supported_brands) === "success"
            && this.formValidation(gateway.supported_currencies) === "success"
            && this.formValidation(gateway.generate_statement) === "success"
            && this.formValidation(gateway.enabled) === "success")
            || this.formValidation(gateway.delete) === "success");
      });
    return validate;
  }

  accountsValidation = (accounts) => {
    let validate = true;
    if (accounts && accounts.length !== 0)
      accounts.forEach(account => {
        validate = validate
          && this.formValidation(account.guid) === "success";
      });
    return validate;
  }

  generalValidation = () => {
    const data = this.state;
    return (this.formValidation(data.name) === "success" &&
      this.formValidation(data.url) === "success" &&
      this.formValidation(data.email) === "success" &&
      this.formValidation(data.phone) === "success" &&
      this.formValidation(data.selectedValue) === "success" &&
      this.formValidation(data.enabled) === "success" &&
      this.gatewaysValidation(data.gateways) &&
      this.accountsValidation(data.accounts)
    );
  }

  getRowsFromLength = (text) => {
    if (!text)
      return 0;
    let res = text.length / 41;
    if (text.length % 41)
      res++;
    return res;
  }

  handleSubmit = async (e) => {
    try {
      this.setState({ isLoading: true });
      e.preventDefault();
      const data = this.state;
      const enabled = this.state.enabled ? 1 : 0;
      const generateSecret = this.state.generateSecret ? 1 : 0;
      const req = {
        guid: this.props.guid,
        name: data.name,
        merchant_guid: this.state.isMerchant ? this.props.shop.merchant_guid : data.selectedValue,
        email: data.email,
        url: data.url,
        phone: data.phone,
        enabled,
        generateSecret
      };
      await this.setState({ gateways: this.props.shopGateways, accounts: this.props.shopAccounts });
      const gateways = this.state.gateways;
      if (this.generalValidation()) {
        await this.props.editShopAction(req);
        data.changedAccounts = await this.props.shopAccounts;
        this.setState({ data });
        if (data.changedAccounts) {
          const req = {
            guid: this.props.guid,
            accounts: data.changedAccounts
          };
          await this.props.upsertAccountToShopAction(req);
        }
        if (gateways) {
          const req = {
            guid: this.props.guid,
            gateways
          };
          await this.props.upsertGatewayToShopAction(req);
        }
        swal({
          title: "Shop is updated",
          icon: "success",
          button:false,
          timer: 2000
        });
        await this.props.showModal(false);
      }
      else {
        swal({
          title: "Please, fill all the fields",
          icon: "warning"
        });
      }
    }
    catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  }


  render() {
    const merchants = this.state.merchants;
    if (this.props.shopLoading || this.props.shopAccountsLoading || this.props.shopGatewaysLoading || this.props.accountsLoading || this.props.gatewaysLoading)
      return <Spinner />;
    else return (
      <div>
        <div className="card">
          <div className="content">
            <label>ID: {this.props.guid}</label>
          </div>
        </div>
        <div className="card">

          <Form autoComplete="off">
            <Row className="rowEdit" >
              <Col className='col-md-3'>
                <ControlLabel>Name*:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <FormGroup validationState={this.formValidation(this.state.name)}>
                  <FormControl
                    name="name"
                    type="text"
                    value={this.state.name}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
            {this.state.isMerchant &&
              <Row className="rowEdit" >
                <Col className='col-md-3'>
                  <ControlLabel>Secret*:</ControlLabel>
                </Col>
                <Col className='col-md-9'>
                  <div
                    id="secret"
                    className="card-shop-secret noselect"
                    style={{ width: "600px !important" }}
                  >
                    {this.state.secret}
                  </div>
                </Col>
              </Row>}
            {this.state.isMerchant &&
              <Row className="rowEdit" >
                <Col className='col-md-3'>
                  <label>Generate new secret:</label>
                </Col>
                <Col className='col-md-6'>
                  <input type="checkbox" id='generateSecretCheckbox' checked={this.state.generateSecret} onChange={this.handleGenerateSecretCheckbox} />{" "}
                </Col>
              </Row>}
            <Row className="rowEdit" >
              <Col className='col-md-3'>
                <label>Enable</label>
              </Col>
              <Col className='col-md-6'>
                <input type="checkbox" id='enabledCheckbox' checked={this.state.enabled} onChange={this.handleEnabledCheckbox} />{" "}
              </Col>
            </Row>

            {!this.state.isMerchant &&
              <Row className="rowEdit">
                <Col className='col-md-3'>
                  <ControlLabel>Merchant*:</ControlLabel>
                </Col>
                <Col className='col-md-9'>
                  <FormControl
                    name="merchant"
                    componentClass="select"
                    onChange={(e) => this.handleSelectChange(e)}
                    validationstate={this.formValidation(this.state.selectedValue)}
                  >
                    <option value={this.state.merchantGuid}>{this.state.merchantName}</option>
                    {merchants.filter(merchant => merchant.guid !== this.state.merchantGuid).map(merchant => {
                      return (<option key={merchant.guid} value={merchant.guid}>{merchant.name}</option>);
                    })
                    }
                  </FormControl>
                </Col>
              </Row>}
            <Row className="rowEdit" >
              <Col className='col-md-3'>
                <ControlLabel>URL*:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <FormGroup validationState={this.formValidation(this.state.url)}>
                  <FormControl
                    name="url"
                    type="text"
                    value={this.state.url}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row className="rowEdit" >
              <Col className='col-md-3'>
                <ControlLabel>Email*:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <FormGroup validationState={this.formValidation(this.state.email)}>
                  <FormControl
                    name="email"
                    type="text"
                    value={this.state.email}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row className="rowEdit" >
              <Col className='col-md-3'>
                <ControlLabel>Phone*:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <FormGroup validationState={this.formValidation(this.state.phone)}>
                  <FormControl
                    name="phone"
                    type="text"
                    value={this.state.phone}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row className="rowEdit" >
              <Col className='col-md-3'>
                <ControlLabel>Gateways*:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <GatewaysManager
                  shop_guid={this.props.guid}
                />
              </Col>
            </Row>

            <Row className="rowEdit" >
              <Col className='col-md-3'>
                <ControlLabel>Accounts*:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <AccountManager
                  shopGuid={this.props.guid}
                  save={this.handleSave}
                />
              </Col>
            </Row>

            <div align="center">
              {this.state.isLoading 
                ? <ReactLoading type='cylon' color='grey' />
                : <Button type="submit" onClick={this.handleSubmit} style={{ "margin": "10px" }}>Save</Button>}
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    shop: state.shops.shop,
    shopGateways: state.shops.changedGateways,
    shopAccounts: state.shops.changedAccounts,
    merchants: state.merchants.merchantsList,
    merchant: state.merchants.merchant,
    shopLoading: state.shops.shopLoading,
    shopGatewaysLoading: state.shops.shopGatewaysLoading,
    shopAccountsLoading: state.shops.shopAccountsLoading,
    accountsLoading: state.accounts.accountsLoading,
    gatewaysLoading: state.gateways.gatewaysLoading
  };
};

export default connect(mapStateToProps, {
  getShopAction,
  editShopAction,
  upsertGatewayToShopAction,
  upsertAccountToShopAction,
  getAllMerchants,
  getMerchantAction,
  showModal,

  getAllAccounts,
  getShopAccountsAction,

  getAllGateways,
  getShopGatewaysAction,
})(ShopEditor);

ShopEditor.propTypes = {
  shop: PropTypes.array,
  shopGateways: PropTypes.array,
  shopAccounts: PropTypes.array,
  merchants: PropTypes.array,
  merchant: PropTypes.array,
  shopAccountsLoading: PropTypes.bool,
  shopLoading: PropTypes.bool,
  shopGatewaysLoading: PropTypes.bool,
  accountsLoading: PropTypes.bool,
  gatewaysLoading: PropTypes.bool,
  guid: PropTypes.string,

  editShopAction: PropTypes.func,
  getShopAction: PropTypes.func,
  upsertGatewayToShopAction: PropTypes.func,
  upsertAccountToShopAction: PropTypes.func,
  getAllMerchants: PropTypes.func,
  getMerchantAction: PropTypes.func,
  showModal: PropTypes.func,

  getAllGateways: PropTypes.func,
  getShopGatewaysAction: PropTypes.func,

  getAllAccounts: PropTypes.func,
  getShopAccountsAction: PropTypes.func,
};
