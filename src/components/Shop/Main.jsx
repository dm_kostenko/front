import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../factories/Main";
import ShopCreator from "./Creator";
import ability from "config/ability";
import { getAllShops, deleteShopAction } from "../../actions/shops";
import { searchInShops, reset } from "../../actions/search";
import Modal from "components/UI/Modal";
import ShopEditor from "./Editor";
import { Link } from "react-router-dom";
import { getAuthData } from "../../services/paymentBackendAPI/backendPlatform";

let columns = [
  {
    path: "guid",
    label: "ID",
    key: "guid",
    content: shop => (
      <Link className="link" to={`/about/shop/${shop.guid}`} >{shop.guid}</Link>
    ),
  },
  { path: "name", label: "Name" },
  { 
    path: "merchant_name",
    label: "Merchant",
    key: "merchant_name",
    content: shop => (
      <Link className="link" to={`/about/merchant/${shop.merchant_guid}`} >{shop.merchant_name}</Link>
    )
  },
  {
    key: "enabled",
    content: shop => (
      // StatusIcon(null, shop.enabled ? 'Enabled' : 'Disabled')
      <i className={shop.enabled === 1 ? "far fa-check-circle green" : "far fa-times-circle red"} />
    ),
    label: "Status"
  },
  ability.can("EXECUTE", "SHOPS") &&
  {
    key: "edit",
    content: shop => (
      <Modal 
        header="Edit shop" 
        button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }}/>}
        content={<ShopEditor guid={shop.guid} type="shop"/>} />
    ),
    label: "Edit"
  },
  ability.can("EXECUTE", "SHOPS") &&
  {
    key: "delete",
    content: () => (
      <i
        className="far fa-trash-alt"
        style={{ cursor: "pointer", color: "red" }}
      />
    ),
    label: "Delete"
  }
];

if(getAuthData() && getAuthData().userPayload.merchant)
  columns.splice(2,1);



const mapStateToProps = (state) => {
  return {
    data: state.shops.shopsList,
    count: state.shops.count,
    searchData: state.search.shopsSearch,
    isSearch: state.search.isSearch,
    name:"shops",
    columns,
    loading: state.shops.shopsLoading,
    modal: <ShopCreator/>
  };
};

export default connect(mapStateToProps, {
  get: getAllShops,
  delete: deleteShopAction,
  search: searchInShops,
  reset
})(AbstractComponent);
