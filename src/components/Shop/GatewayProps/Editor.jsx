import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Row, FormGroup, ControlLabel, FormControl, Button, Form } from "react-bootstrap";
import { getShopGatewayPropsAction, editShopGatewayPropsAction } from "actions/shops";
import { showModal } from "actions/modal";
import { parseResponse } from "helpers/parseResponse";
import { getAuthData } from "services/paymentBackendAPI/backendPlatform";
import PropTypes from "prop-types";
import Loading from "components/UI/Spinner/index";
import swal from "sweetalert";

class GatewayPropsEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gatewayProps: [],
      changedGatewayProps: []
    };
  }

  async componentDidMount() {
    const token = getAuthData();
    if (token.userPayload.merchant)
      this.setState({ isMerchant: true });
    await this.props.getShopGatewayPropsAction(this.props.shopGuid, this.props.gatewayGuid);
    const gatewayProps = JSON.parse(JSON.stringify(this.props.gatewayProps));
    const changedGatewayProps = JSON.parse(JSON.stringify(this.props.gatewayProps));
    this.setState({
      gatewayProps,
      changedGatewayProps
    });
  }

  handleChange = (e) => {
    const property = e.target.name;
    const { changedGatewayProps } = this.state;
    const index = changedGatewayProps.findIndex((prop) => { return prop.name === property; });
    changedGatewayProps[index].value = e.target.value;
    this.setState({ changedGatewayProps });
  };

  handleDelete = (propName) => {
    const { changedGatewayProps } = this.state;
    const index = changedGatewayProps.findIndex(property => { return propName === property.name; });
    changedGatewayProps[index].delete = "true";
    this.setState({
      changedGatewayProps
    });
  }

  handleSubmit = async (e) => {
    try {
      e.preventDefault();
      const { gatewayProps, changedGatewayProps } = this.state;
      let difference = [];
      gatewayProps.forEach((property, index) => {
        if (changedGatewayProps[index].value !== property.value && changedGatewayProps[index].value==="") {
          let propToDelete = changedGatewayProps[index];
          propToDelete.delete = "true";
          difference.push(propToDelete);
        } else if (changedGatewayProps[index].value !== property.value) 
          difference.push(changedGatewayProps[index]);
      });
      const { shopGuid, gatewayGuid } = this.props;
      difference.forEach(async (property) => {
        await this.props.editShopGatewayPropsAction(shopGuid, gatewayGuid, property);
      });
      swal({
        title: "Properties successfully updated",
        icon: "success",
        button:false,
        timer: 2000
      });
      await this.props.showModal(false);
    }
    catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  }

  getPropsLayout = () => {
    const changedGatewayProps = this.state.changedGatewayProps.filter(property => !property.delete);
    const layout = changedGatewayProps.map(prop => {
      return (
        <Row className="rowEdit" key={prop.name}>
          <Col className="col-md-3">
            <ControlLabel>{prop.label}:</ControlLabel>
          </Col>
          <Col className="col-md-7">
            <FormGroup>
              <FormControl
                name={prop.name}
                type="text"
                value={prop.value}
                onChange={(e) => this.handleChange(e)}
              />
            </FormGroup>
          </Col>
          {/* <Col className="col-md-2">
            <i
              className="far fa-trash-alt"
              name={prop.name}
              onClick={() => this.handleDelete(prop.name)}
              style={{ cursor: "pointer", color: "red" }}
            />
          </Col> */}
        </Row>
      );
    });
    return layout;
  }


  render() {
    if (this.props.gatewayPropsLoading)
      return <Loading />;
    else if (this.props.gatewayProps.length === 0) return (
      <div className="card">
        <Row>
          <Col>
            <div style={{ textAlign:"center" }}>
              <label>Gateway has no configurable properties</label>
            </div>         
          </Col>
        </Row>
      </div>
    );
    else return (
      <div className="card">
        <Form autoComplete="off">
          {this.getPropsLayout()}
          <div align="center">
            <Button type="submit" onClick={this.handleSubmit} style={{ "margin": "10px" }}>Save</Button>
          </div>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    gatewayPropsLoading: state.shops.gatewayPropsLoading,
    gatewayProps: state.shops.gatewayProps,
  };
};

export default connect(mapStateToProps, {
  showModal,
  getShopGatewayPropsAction,
  editShopGatewayPropsAction
})(GatewayPropsEditor);

GatewayPropsEditor.propTypes = {
  getShopGatewayPropsAction: PropTypes.func,
  showModal: PropTypes.func,
  gatewayGuid: PropTypes.string,
  shopGuid: PropTypes.string,
  gatewayProps: PropTypes.array,
  gatewayPropsLoading: PropTypes.bool,
  editShopGatewayPropsAction: PropTypes.func
};