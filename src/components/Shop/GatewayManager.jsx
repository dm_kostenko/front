import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Row, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import { upsertChangedGatewayDataAction } from "../../actions/shops";
import { getAllCurrencies } from "../../actions/currencies";
import MultiSelect from "components/UI/MultiSelect";
import PropTypes from "prop-types";

class GatewayManager extends Component {
  state = {
    guid: "",
    name: "",
    billing_descriptor: "",
    routing_string: "",
    payment_amount_limit: "",
    monthly_amount_limit: "",
    supported_brands: "",

    supported_currencies: "",
    generate_statement: 0,
    enabled: 0
  }

  componentDidMount = async () => {
    await this.props.getAllCurrencies();
    const gateway = this.props.gateway;
    const guid = gateway.guid;
    this.setState({
      guid,
      name: gateway.name,
      enabled: gateway.enabled,
      billing_descriptor: gateway.billing_descriptor,
      routing_string: gateway.routing_string,
      payment_amount_limit: gateway.payment_amount_limit,
      monthly_amount_limit: gateway.monthly_amount_limit,
      supported_brands: gateway.supported_brands,
      supported_currencies: gateway.supported_currencies,
      generate_statement: gateway.generate_statement
    });
    if (typeof (gateway.enabled) === "undefined" || typeof (gateway.generate_statement) === "undefined")
      this.setState({
        enabled: 0,
        generate_statement: 0
      });
  }

  formValidation = (data) => {
    if (data === "" || typeof (data) === "undefined") return "error";
    else return "success";
  }

  onCurrencySelect = async (option) => {
    await this.setState({
      supported_currencies: option.name
    });
    await this.props.upsertChangedGatewayDataAction(this.state);
  }

  handleNumberChange = async (e) => {
    const number = e.target.value.replace(/\D/, "");
    await this.setState({
      [e.target.name]: number
    });
    await this.props.upsertChangedGatewayDataAction(this.state);
  }

  handleChange = async (e) => {
    await this.setState({
      [e.target.name]: e.target.value
    });
    await this.props.upsertChangedGatewayDataAction(this.state);
  }

  handleCheck = async (e) => {
    await this.setState({
      [e.target.name]: e.target.checked ? 1 : 0
    });
    await this.props.upsertChangedGatewayDataAction(this.state);
  }

  render() {
    const currencies = this.props.currencies ? this.props.currencies : [];
    return (
      <div className="card">
        <div className="card-body">
          <Row className="rowEdit">
            <Col className='col-md-6'>
              <ControlLabel>Billing descriptor: *</ControlLabel>
            </Col>
            <Col className='col-md-6'>
              <FormGroup validationState={this.formValidation(this.state.billing_descriptor)}>
                <FormControl
                  name="billing_descriptor"
                  type="text"
                  value={this.state.billing_descriptor}
                  onChange={this.handleChange}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row className="rowEdit">
            <Col className='col-md-6'>
              <ControlLabel>Routing string: *</ControlLabel>
            </Col>
            <Col className='col-md-6'>
              <FormGroup validationState={this.formValidation(this.state.routing_string)}>
                <FormControl
                  name="routing_string"
                  type="text"
                  value={this.state.routing_string}
                  onChange={this.handleChange}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row className="rowEdit">
            <Col className='col-md-6'>
              <ControlLabel>Payment amount limit: *</ControlLabel>
            </Col>
            <Col className='col-md-6'>
              <FormGroup validationState={this.formValidation(this.state.payment_amount_limit)}>
                <FormControl
                  name="payment_amount_limit"
                  type="text"
                  value={this.state.payment_amount_limit}
                  onChange={this.handleNumberChange}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row className="rowEdit">
            <Col className='col-md-6'>
              <ControlLabel>Monthly amount limit: *</ControlLabel>
            </Col>
            <Col className='col-md-6'>
              <FormGroup validationState={this.formValidation(this.state.monthly_amount_limit)}>
                <FormControl
                  name="monthly_amount_limit"
                  type="text"
                  value={this.state.monthly_amount_limit}
                  onChange={this.handleNumberChange}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row className="rowEdit">
            <Col className='col-md-6'>
              <ControlLabel>Supported brands: *</ControlLabel>
            </Col>
            <Col className='col-md-6'>
              <FormGroup validationState={this.formValidation(this.state.supported_brands)}>
                <FormControl
                  name="supported_brands"
                  type="text"
                  value={this.state.supported_brands}
                  onChange={this.handleChange}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row className="rowEdit">
            <Col className='col-md-6'>
              <ControlLabel>Supported currencies: *</ControlLabel>
            </Col>
            <Col className='col-md-6'>
              <FormGroup validationState={this.formValidation(this.state.supported_currencies)}>
                <MultiSelect
                  name="supported_currencies"
                  options={currencies}
                  value={this.state.supported_currencies}
                  multi={false}
                  onSelect={this.onCurrencySelect}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row className="rowEdit">
            <Col className='col-md-6'>
              <ControlLabel>Generate statement: *</ControlLabel>
            </Col>
            <Col className='col-md-2'>
              <FormGroup>
                <input
                  name="generate_statement"
                  type="checkbox"
                  checked={this.state.generate_statement}
                  onChange={this.handleCheck}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row className="rowEdit">
            <Col className='col-md-6'>
              <ControlLabel>Enabled: *</ControlLabel>
            </Col>
            <Col className='col-md-2'>
              <FormGroup>
                <input
                  name="enabled"
                  type="checkbox"
                  checked={this.state.enabled}
                  onChange={this.handleCheck}
                />
              </FormGroup>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const [ gateway ] = state.shops.shopGateways.filter(gateway => gateway.guid === ownProps.gatewayGuid);
  return {
    gateway,
    currencies: state.currencies.currenciesList
  };
};

export default connect(mapStateToProps, {
  upsertChangedGatewayDataAction,
  getAllCurrencies
})(GatewayManager);

GatewayManager.propTypes = {
  gateway: PropTypes.object,
  currencies: PropTypes.array,

  upsertChangedGatewayDataAction: PropTypes.func,
  getAllCurrencies: PropTypes.func,
};