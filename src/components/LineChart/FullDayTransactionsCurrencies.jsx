import React from "react";
import { Row, Col } from "react-bootstrap";
import { LineChart } from "components/UI/LineChart";
import { formatDate } from "services/dateTime/dateTime";
import PropTypes from "prop-types";
import CustomCheckbox from "components/UI/Checkbox";
import CustomSelect from "components/UI/MultiSelect";
import CustomButton from "components/UI/Button";

export default class LineChartFullDayTransactionsCurrencies extends React.Component {
  state = {
    daysCount: 7,
    loading: true
  }

  getCurrencies = (array, currenciesData) => {
    let res = [];
    res = [ ...res, "" ];
    return res;
  }

  getCurrenciesData = (array, days) => {
    let res = [];
    res = [ ...res, []];
    return res;
  }

  convertData = (array, days) => {
    const arrayDays = array.map(item => formatDate(item.date));
    let res = [];
    days.forEach(day => {
      arrayDays.includes(day)
        ? res = [ ...res, array[arrayDays.indexOf(day)].success + array[arrayDays.indexOf(day)].failed ]
        : res = [ ...res, 0 ];
    });
    return res;
  }

  render() {
    const labels = [ "00:00", "06:00", "12:00", "18:00" ];

    let datasetsPie;

    return (
      <div className="card">
        <Row>
          <div className="header">Transactions</div>
        </Row>
        <Row style={{ marginLeft: "15px" }}>
          {/* <Col md={4} sm={4} xs={4}>
            <input 
              className="input"
              type="datetime-local" 
              value="from_date"
              style={{ textAlign: "center", minHeight: "38px", marginRight:"20px" }}
            />
            <input 
              className="input"
              type="datetime-local" 
              value="to_date"
              style={{ textAlign: "center", minHeight: "38px" }}
            />
          </Col>
          <Col md={2} sm={2} xs={2}>
            <CustomSelect 
              multi={false} 
              name="Select type" 
              options={[ { guid:"1", name:"Merchant 1" }, { id:"2", name:"Merchant 2" } ]} 
              onSelect={this.handleSelectPageSize}
            />
          </Col>
          <Col md={2} sm={2} xs={2}>
            <CustomButton className="btn" style={{ float:"left" }}>Find</CustomButton>            
          </Col> */}
          <div style={{ display:"inline-block" }}>
            From:
            <input 
              className="input"
              type="date" 
              style={{ textAlign: "center", minHeight: "38px", marginRight:"15px" }}
            />
            To:
            <input 
              className="input"
              type="date" 
              style={{ textAlign: "center", minHeight: "38px" }}
            />
          </div>
          <div style={{ display:"inline-block", minWidth:"150px", marginLeft:"15px" }}>
            <CustomSelect 
              multi={false} 
              name="Select type" 
              options={[ { guid:"1", name:"Merchant 1" }, { id:"2", name:"Merchant 2" } ]} 
              onSelect={this.handleSelectPageSize}
            />
          </div>
          <div style={{ display:"inline-block", minWidth:"100px", marginBottom:"-15px", marginLeft:"15px" }}>
            <CustomButton className="btn" style={{ float:"left" }}>Find</CustomButton>            
          </div>
        </Row>
        <Row style={{ marginLeft: "15px", marginRight: "15px" }}>
          <Col md={10} sm={10} xs={10}>
            <CustomCheckbox number="1" label="payments" isChecked={true} inline={true} />
            <CustomCheckbox number="2" label="refunds" isChecked={true} inline={true} />
            <CustomCheckbox number="3" label="chargeback" isChecked={true} inline={true} />
          </Col>
          <Col md={2} sm={2} xs={2}>
            <CustomSelect
              multi={false}
              name="Select type"
              options={[ { guid: "1", name: "Sum" }, { id: "2", name: "Count" } ]}
              onSelect={this.handleSelectPageSize}
            />
          </Col>
        </Row>
        <Row>
          <Col md={3} sm={3} xs={3} />
          <Col md={6} sm={6} xs={6}>
            <div className="card-large">
              <LineChart
                labels={labels}
                datasets={datasetsPie}
                options={{
                  scales: {
                    yAxes: [ {
                      ticks: {
                        beginAtZero: true
                      }
                    } ]
                  }
                }}
              />
            </div>
          </Col>
          <Col md={3} sm={3} xs={3} />
        </Row>
      </div>
    );
  }
}