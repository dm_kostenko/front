import React from "react";
import { connect } from "react-redux";
import { LineChart } from "../UI/LineChart";
import { lastNDays, formatDate } from "../../services/dateTime/dateTime";
import PropTypes from "prop-types";
import { getTransactionTypesAction } from "../../actions/transactions";
import Spinner from "components/UI/Spinner";
import moment from "moment";

class LineChartTransactionsAmount extends React.Component {
  state = {
    daysCount: 7,
    loading: true
  }

  componentDidMount() {
    this.props.getTransactionTypesAction({ days:this.state.daysCount });
  }

  render() {
    if (this.props.loading) return <Spinner />;
    else {
      const labelsPie = lastNDays(this.state.daysCount);
      let transactionTypes = Object.keys(this.props.types);
      let datasetSuccess = [];
      let datasetFailed = [];
      transactionTypes.map(transactionType => {
        this.props.types[transactionType].map(item => {
          const findIndex = labelsPie.findIndex(itemD => itemD == moment(item.date).format("DD.MM"));
          if (findIndex != -1) {
            if (datasetSuccess[findIndex])
              datasetSuccess[findIndex] += item.success;
            else
              datasetSuccess[findIndex] = item.success;
            if (datasetFailed[findIndex])
              datasetFailed[findIndex] += item.failed;
            else
              datasetFailed[findIndex] = item.failed;
          }          
        });
      });
      for ( let i=0;i<labelsPie.length;i++ ) {
        if (!datasetSuccess[i])
          datasetSuccess[i] = 0;
        if (!datasetFailed[i])
          datasetFailed[i] = 0;
      }
      const datasetsPie = [
        {
          label: "Success transactions",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(75,192,192,0.4)",
          borderColor: "rgba(75,192,192,1)",
          borderCapStyle: "butt",
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: "rgba(75,192,192,1)",
          pointBackgroundColor: "#fff",
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgba(75,192,192,1)",
          pointHoverBorderColor: "black",
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: datasetSuccess
        },
        {
          label: "Failed transactions",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(255,99,132,0.4)",
          borderColor: "rgba(255,99,132,1)",
          borderCapStyle: "butt",
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: "rgba(255,99,132,1)",
          pointBackgroundColor: "#fff",
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgba(255,99,132,1)",
          pointHoverBorderColor: "black",
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: datasetFailed
        }
      ];

      return (
        <div>
          <div className="header">Amount of transactions</div>
          <LineChart
            labels={labelsPie}
            datasets={datasetsPie}
            name="Transactions amount"
            options={{
              scales: {
                yAxes: [ {
                  ticks: {
                    beginAtZero: true
                  }
                } ]
              }
            }}
          />
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    types: state.transactions.types,
    loading: state.transactions.transactionTypesLoading
  };
};

export default connect(mapStateToProps, {
  getTransactionTypesAction
})(LineChartTransactionsAmount);

LineChartTransactionsAmount.propTypes = {
  getTransactionTypesAction: PropTypes.func,
  types: PropTypes.object,
  loading: PropTypes.bool
};