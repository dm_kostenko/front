import React from "react";
import { connect } from "react-redux";
import { PieChart } from "../UI/PieChart";
import { getTransactionHistoryAction } from "../../actions/transactions";
import Spinner from "components/UI/Spinner";
import PropTypes from "prop-types";

class PieChartTransactionsStatusesCount extends React.Component {
  state = {
    daysCount: 7,
    loading: true
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.loading !== nextProps.loading) {
      return {
        loading: nextProps.loading
      };
    }
    return null;
  }


  render() {
    if (this.state.loading || this.props.types.length === 0) return <Spinner />;
    else {
      let success = 0;
      let failed = 0;
      Object.keys(this.props.types).forEach(type => {
        this.props.types[type].map((typeItem) => {
          success += typeItem.success;
          failed += typeItem.failed;
        });
      });

      const datasetsPie = success + failed > 0 
        ? [ {
          data: [ success, failed ],
          backgroundColor: [
            "rgba(75,192,192,1)",
            "rgba(255,99,132,1)"
          ]
        } ]
        : [ {
          data: [ 1 ],
          backgroundColor: "#CCC"
        } ];

      const labelsPie =
        datasetsPie
          ? datasetsPie[0].data.length === 1
            ? [
              "There are no transactions yet"
            ]
            : [
              "Success",
              "Failed",
            ]
          : null;


      return (
        <div className="content">
          <div className="header">Transactions statuses</div>
          <PieChart
            labels={labelsPie}
            datasets={datasetsPie}
            name="Transactions statuses amount"
          />
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    types: state.transactions.types,
    loading: state.transactions.transactionTypesLoading
  };
};

export default connect(mapStateToProps, {
  getTransactionHistoryAction
})(PieChartTransactionsStatusesCount);

PieChartTransactionsStatusesCount.propTypes = {
  success: PropTypes.number,
  failed: PropTypes.number,
  loading: PropTypes.bool,
  getTransactionHistoryAction: PropTypes.func, 
};