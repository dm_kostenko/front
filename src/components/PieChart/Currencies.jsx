import React from "react";
import { PieChart } from "components/UI/PieChart";
import { getReportDailyCurrencyAction } from "actions/currencies";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";
import { backgroundColors } from "constants/charts/colors";

class PieChartCurrencies extends React.Component {

  componentDidMount() {
    this.props.getReportDailyCurrencyAction();
  }

  render() {
    if (this.props.loading) return <Spinner />;
    else {
      let datasetsPie = [];
      let labelsPie = [];
      let data = [];
      let colors = [];
      this.props.data.map((item, index) => {
        data.push(item.count);
        colors.push(backgroundColors[index]);
        datasetsPie = [ { data: data, backgroundColor: colors } ];
        labelsPie = [ ...labelsPie, item.name ];
      });
      
      return (
        <PieChart
          name="Currencies"
          labels={labelsPie.length !== 0 ? labelsPie : [ "There are no currencies yet" ]}
          datasets={datasetsPie.length !== 0 ? datasetsPie : [ {
            data: [ 1 ],
            backgroundColor: "#CCC"
          } ]}
        />
      );
    }    
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.currencies.dailyCurrencyReport,
    loading: state.currencies.dailyCurrencyReportLoading
  };
};

export default connect(mapStateToProps, {
  getReportDailyCurrencyAction
})(PieChartCurrencies);

PieChartCurrencies.propTypes = {
  getReportDailyCurrencyAction: PropTypes.func,
  data: PropTypes.array,
  loading: PropTypes.bool
};


