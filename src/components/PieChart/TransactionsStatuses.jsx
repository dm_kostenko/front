import React from "react";
import { connect } from "react-redux";
import { PieChart } from "components/UI/PieChart";
import { getReportDailyPassabilityAction } from "actions/transactions";
import Spinner from "components/UI/Spinner";
import PropTypes from "prop-types";

class PieChartTransactionsStatuses extends React.Component {

  componentDidMount = () => {
    this.props.getReportDailyPassabilityAction();
  }


  render() {
    if (this.props.loading) return <Spinner />;
    else {
      const datasetsPie =
        this.props.success > 0 || this.props.failed > 0
          ? [
            {
              data: [ this.props.success, this.props.failed ],
              backgroundColor: [
                "rgba(75,192,192,1)",
                "rgba(255,99,132,1)"
              ]
            }
          ]
          : this.props.success === null && this.props.failed === null
            ? [
              {
                data: [ this.props.success, this.props.failed ],
                backgroundColor: [
                  "#63FF84",
                  "#FF6384"
                ]
              }
            ]
            : this.props.success === null && this.props.failed === null
              ? [
                {
                  data: [ 1 ],
                  backgroundColor: "#CCC"
                }
              ]
              : null;

      const labelsPie =
        datasetsPie
          ? datasetsPie[0].data.length === 1
            ? [
              "There are no transactions yet"
            ]
            : [
              "Success",
              "Failed",
            ]
          : null;


      return (
        <PieChart
          name="Transaction statuses"
          labels={labelsPie}
          datasets={datasetsPie}
        />
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    success: state.transactions.dailySuccess,
    failed: state.transactions.dailyFailed,
    loading: state.transactions.dailyPassabilityLoading
  };
};

export default connect(mapStateToProps, {
  getReportDailyPassabilityAction
})(PieChartTransactionsStatuses);

PieChartTransactionsStatuses.propTypes = {
  success: PropTypes.number,
  failed: PropTypes.number,
  loading: PropTypes.bool,
  getReportDailyPassabilityAction: PropTypes.func, 
};