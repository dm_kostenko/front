import React from "react";
import { PieChart } from "components/UI/PieChart";
import { getReportDailyTransactionTypeAction } from "actions/transactions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";
import { backgroundColors } from "constants/charts/colors";

class PieChartTransactionsTypes extends React.Component {

  componentDidMount() {
    this.props.getReportDailyTransactionTypeAction();
  }

  render() {
    if (this.props.loading) return <Spinner />;
    else {
      let datasetsPie = [];
      let labelsPie = [];
      let data = [];
      let colors = [];
      this.props.data.map((item, index) => {
        data.push(item.count);
        colors.push(backgroundColors[index]);
        datasetsPie = [ { data: data, backgroundColor: colors } ];
        labelsPie = [ ...labelsPie, item.type ];
      });
  
      return (
        <PieChart
          name="Transaction types"
          labels={labelsPie.length !== 0 ? labelsPie : [ "There are no transactions yet" ]}
          datasets={datasetsPie.length !== 0 ? datasetsPie : [ {
            data: [ 1 ],
            backgroundColor: "#CCC"
          } ]}
        />
      );
    }   
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.transactions.dailyTransactionTypeReport,
    loading: state.transactions.dailyTransactionTypeReportLoading
  };
};

export default connect(mapStateToProps, {
  getReportDailyTransactionTypeAction
})(PieChartTransactionsTypes);

PieChartTransactionsTypes.propTypes = {
  getReportDailyTransactionTypeAction: PropTypes.func,
  data: PropTypes.array,
  loading: PropTypes.bool
};
