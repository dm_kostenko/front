import React from "react";
import { connect } from "react-redux";
import { PieChart } from "../UI/PieChart";
import PropTypes from "prop-types";
import { getTransactionTypesAction } from "../../actions/transactions";

class PieChartShopTransactionsStatusesCount extends React.Component {
  state = {
    daysCount: 7,
    loading: true,
    types:{}
  }

  componentDidMount = async () => {
    if (this.props.shopGuid) {
      await this.props.getTransactionTypesAction({ days: this.state.daysCount, shop_guid: this.props.shopGuid });
      this.setState({
        types:this.props.types
      });
    }    
  }


  render() {

    let success = 0;
    Object.keys(this.state.types).forEach(type => {
      success += this.state.types[type][0].success;
    });
    let failed = 0;
    Object.keys(this.state.types).forEach(type => {
      failed += this.state.types[type][0].failed;
    });

    const datasetsPie = success + failed > 0
      ? [ {
        data: [ success, failed ],
        backgroundColor: [
          "rgba(75,192,192,1)",
          "rgba(255,99,132,1)"
        ]
      } ]
      : [ {
        data: [ 1 ],
        backgroundColor: "#CCC"
      } ];

    const labelsPie =
      datasetsPie
        ? datasetsPie[0].data.length === 1
          ? [
            "There are no transactions yet"
          ]
          : [
            "Success",
            "Failed",
          ]
        : null;

    return (
      <PieChart
        name={`Transactions statuses amount of shop: ${this.props.shopGuid ? this.props.shopGuid : ""}`}
        labels={labelsPie}
        datasets={datasetsPie}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    types: state.transactions.typesTransactionsOfShop
  };
};

export default connect(mapStateToProps, {
  getTransactionTypesAction
})(PieChartShopTransactionsStatusesCount);

PieChartShopTransactionsStatusesCount.propTypes = {
  types: PropTypes.array,
  getTransactionTypesAction: PropTypes.func,
  shopGuid: PropTypes.string,
};