import React from "react";
import { PieChart } from "components/UI/PieChart";


export default class PieChartCardsTypes extends React.Component {
  state = {
    daysCount: 7,
    loading: true
  }

  render() {

    const datasetsPie =
      [
        {
          data: [ 1 ],
          backgroundColor: "#CCC"
        }
      ];

    const labelsPie = 
      datasetsPie
        ? datasetsPie[0].data.length === 1
          ? [
            "There are no card types yet"
          ]
          : [
            "MASTERCARD",
            "VISA"
          ]
        : null;
        

    return (
      <PieChart
        labels={labelsPie}
        datasets={datasetsPie}
      />
    );
  }
}
