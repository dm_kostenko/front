import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../../factories/Main";
import { getMerchantsBlackListAction, deleteItemMerchantsBlackListAction } from "../../../actions/blacklists";
import { searchInMerchantsBlacklist, reset } from "../../../actions/search";
import { Link } from "react-router-dom";
import { getAuthData } from "services/paymentBackendAPI/backendPlatform";

const token = getAuthData();

const columns = [
  (token && token.userPayload && token.userPayload.merchant) ? null : { 
    path: "merchant_guid", 
    label: "Merchant GUID",
    key: "merchant_guid",
    content: item => (
      <Link className="link" to={`/about/merchant/${item.merchant_guid}`} >{item.merchant_guid}</Link>
    )
  },
  { 
    path: "blacklist_rule_guid", 
    label: "Rule GUID",
    key: "blacklist_rule_guid",
    content: item => (
      <Link className="link" to={`/about/blacklist/${item.blacklist_rule_guid}`} >{item.blacklist_rule_guid}</Link>
    )
  },
  { path: "blacklist_rule_name", label: "Rule name" },
  { path: "type", label: "Type" },
  {
    key: "deleteMerchantsBlacklist",
    content: () => (
      <i
        className="far fa-trash-alt"
        style={{ cursor: "pointer", color: "red" }}
      />
    ),
    label: "Delete"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.blacklist.merchantsBlackList,
    count: state.blacklist.count,
    searchData: state.search.merchantsBlacklistSearch,
    isSearch: state.search.isSearch,
    loading: state.blacklist.merchantsBlackLoading,
    name: "blacklists",
    columns
  };
};

export default connect(mapStateToProps, {
  get: getMerchantsBlackListAction,
  delete: deleteItemMerchantsBlackListAction,
  search: searchInMerchantsBlacklist,
  reset
})(AbstractComponent);
