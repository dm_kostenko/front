import React from "react";
import { connect } from "react-redux";
import Joi from "joi-browser";
import CustomForm from "components/UI/Form/index";
import { Form } from "react-bootstrap";
import { createItemMerchantsBlackListAction, getAllBlackListItems } from "actions/blacklists";
import { getAllMerchants } from "actions/merchants";
import { showModal } from "actions/modal";
import PropTypes from "prop-types";
import { parseResponse } from "helpers/parseResponse";
import swal from "sweetalert";
import ReactLoading from "react-loading";
import { getAuthData } from "services/paymentBackendAPI/backendPlatform";

class MerchantsBlackListCreator extends CustomForm {
  state = {
    data: {
      blacklist_rule_guid: "",
      merchant_guid:"",
      type: ""
    },
    types: [
      { "name": "allow", "label": "allow" },
      { "name": "deny", "label": "deny" }
    ],
    errors: {},
    isLoading: false,
    token: getAuthData().userPayload
  };

  schema = {
    blacklist_rule_guid: Joi.string()
      .required()
      .min(0)
      .max(100)
      .label("Rule"),
    merchant_guid: Joi.string()
      .required()
      .min(0)
      .max(100)
      .label("Merchant"),
    type: Joi.string()
      .required()
      .min(0)
      .max(100)
      .label("Type")
  };

  componentDidMount = () => {
    this.props.getAllBlackListItems();
    if (!this.state.token.merchant)
      this.props.getAllMerchants();
    else {
      const data = {};
      data.merchant_guid = this.state.token.merchant.merchant_guid;
      this.setState({ data });
    }

  }

  doSubmit = async () => {
    try {
      this.setState({ isLoading: true });
      const { blacklist_rule_guid, merchant_guid, type } = this.state.data;
      await this.props.createItemMerchantsBlackListAction({ blacklist_rule_guid, merchant_guid, type }, this.props.currentPage, this.props.pageSize);
      swal({
        title: "Record is created",
        icon: "success",
        button: false,
        timer: 2000
      });
      await this.props.showModal(false);
    } catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  handleValueChange = (e) => {
    this.setState({
      value: e.target.value
    });
  }

  render() {
    return (
      <div className="card">
        <div className="content">
          <Form onSubmit={this.handleSubmit}>
            {this.renderSelect("blacklist_rule_guid", "Rule", this.props.rules)}
            {!this.state.token.merchant ? this.renderSelect("merchant_guid", "Merchant", this.props.merchants) : null}
            {this.renderSelect("type", "Type", this.state.types)}
            <div align="center">
              {this.state.isLoading
                ? <ReactLoading type='cylon' color='grey' />
                : this.renderButton("Add")
              }
            </div>
          </Form>
        </div>
      </div >
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentPage: state.blacklist.currentPage,
    pageSize: state.blacklist.pageSize,
    rules: state.blacklist.blackList,
    merchants: state.merchants.merchantsList
  };
};

export default connect(
  mapStateToProps,
  {
    createItemMerchantsBlackListAction,
    getAllBlackListItems,
    getAllMerchants,
    showModal
  }
)(MerchantsBlackListCreator);

MerchantsBlackListCreator.propTypes = {
  createItemMerchantsBlackListAction: PropTypes.func,
  getAllBlackListItems: PropTypes.func,
  getAllMerchants: PropTypes.func,
  showModal: PropTypes.func,
};