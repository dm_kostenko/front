import React from "react";
import { connect } from "react-redux";
import Joi from "joi-browser";
import CustomForm from "components/UI/Form/index";
import { Form } from "react-bootstrap";
import { createItemGlobalBlackListAction, getAllBlackListItems } from "actions/blacklists";
import { showModal } from "actions/modal";
import PropTypes from "prop-types";
import { parseResponse } from "helpers/parseResponse";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class GlobalBlackListCreator extends CustomForm {
  state = {
    data: {
      blacklist_rule_guid: "",
      type: ""
    },
    types: [
      { "name": "allow", "label": "allow" },
      { "name": "deny", "label": "deny" }
    ],
    errors: {},
    isLoading: false,
    rules: []
  };

  componentDidMount = () => {
    this.props.getAllBlackListItems();
  };

  schema = {
    blacklist_rule_guid: Joi.string()
      .required()
      .min(0)
      .max(100)
      .label("Rule"),
    type: Joi.string()
      .required()
      .min(0)
      .max(100)
      .label("Type")
  };

  doSubmit = async () => {
    try {
      this.setState({ isLoading: true });
      const { blacklist_rule_guid, type } = this.state.data;
      await this.props.createItemGlobalBlackListAction({ blacklist_rule_guid, type }, this.props.currentPage, this.props.pageSize);
      swal({
        title: "Record is created",
        icon: "success",
        button: false,
        timer: 2000
      });
      await this.props.showModal(false);
    } catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  handleValueChange = (e) => {
    this.setState({
      value: e.target.value
    });
  }

  render() {
    return (
      <div className="card">
        <div className="content">
          <Form onSubmit={this.handleSubmit}>
            {this.renderSelect("blacklist_rule_guid", "Rule", this.props.rules)}
            {this.renderSelect("type", "Type", this.state.types)}
            <div align="center">
              {this.state.isLoading
                ? <ReactLoading type='cylon' color='grey' />
                : this.renderButton("Add")
              }
            </div>
          </Form>
        </div>
      </div >
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentPage: state.blacklist.currentPage,
    pageSize: state.blacklist.pageSize,
    rules: state.blacklist.blackList
  };
};

export default connect(
  mapStateToProps,
  {
    createItemGlobalBlackListAction,
    getAllBlackListItems,
    showModal
  }
)(GlobalBlackListCreator);

GlobalBlackListCreator.propTypes = {
  createItemGlobalBlackListAction: PropTypes.func,
  getAllBlackListItems: PropTypes.func,
  showModal: PropTypes.func,
};