import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "factories/Main";
import { getBlackListGlobalAction, deleteItemGlobalBlackListAction } from "actions/blacklists";
import { searchInGlobalBlacklist, reset } from "actions/search";
import { Link } from "react-router-dom";

const columns = [
  { 
    path: "blacklist_rule_guid", 
    label: "Rule GUID",
    key: "blacklist_rule_guid",
    content: item => (
      <Link className="link" to={`/about/blacklist/${item.blacklist_rule_guid}`} >{item.blacklist_rule_guid}</Link>
    ),
  },
  { path: "blacklist_rule_name", label: "Rule name" },
  { path: "type", label: "Type" },
  {
    key: "deleteGlobalBlacklist",
    content: () => (
      <i
        className="far fa-trash-alt"
        style={{ cursor: "pointer", color: "red" }}
      />
    ),
    label: "Delete"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.blacklist.globalBlackList,
    count: state.blacklist.count,
    searchData: state.search.globalBlacklistSearch,
    isSearch: state.search.isSearch,
    loading: state.blacklist.globalLoading,
    name: "blacklists",
    columns
  };
};

export default connect(mapStateToProps, {
  get: getBlackListGlobalAction,
  delete: deleteItemGlobalBlackListAction,
  search: searchInGlobalBlacklist,
  reset
})(AbstractComponent);
