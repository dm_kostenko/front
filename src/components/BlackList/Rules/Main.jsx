import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../../factories/Main";
import BlackListCreator from "./Creator";
import { getAllBlackListItems, deleteBlackListItemAction } from "../../../actions/blacklists";
import { searchInBlacklist, reset } from "../../../actions/search";
import BlackListEditor from "./Editor";
import Modal from "components/UI/Modal";
import { Link } from "react-router-dom";

const columns = [
  {
    path: "guid",
    label: "ID",
    key: "guid",
    content: item => (
      <Link className="link" to={`/about/blacklist/${item.guid}`} >{item.guid}</Link>
    ),
  },
  { path: "name", label: "Name" },
  { path: "type", label: "Type" },
  { path: "description", label: "Description" },
  {
    key: "edit",
    content: item => (
      <Modal
        header="Edit rule" 
        action="Save" 
        content={<BlackListEditor blacklist_guid={item.guid} type="blacklist"/>} 
        button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }}/>} />        
    ),
    label: "Edit"
  },
  {
    key: "delete",
    content: () => (
      <i
        className="far fa-trash-alt"
        style={{ cursor: "pointer", color: "red" }}
      />
    ),
    label: "Delete"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.blacklist.blackList,
    count: state.blacklist.count,
    searchData: state.search.blacklistSearch,
    isSearch: state.search.isSearch,
    loading: state.blacklist.itemsLoading,
    name: "blacklists",
    columns,
    modal: <BlackListCreator />
  };
};

export default connect(mapStateToProps, {
  get:  getAllBlackListItems,
  delete: deleteBlackListItemAction,
  search: searchInBlacklist,
  reset
})(AbstractComponent);
