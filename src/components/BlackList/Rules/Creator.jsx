import React from "react";
import { connect } from "react-redux";
import Joi from "joi-browser";
import CustomForm from "components/UI/Form/index";
import { Col, Row, Form, ControlLabel, FormControl, FormGroup } from "react-bootstrap";
import { addBlackListItemAction } from "actions/blacklists";
import { showModal } from "actions/modal";
import PropTypes from "prop-types";
import { parseResponse } from "helpers/parseResponse";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class BlackListCreator extends CustomForm {
  state = {
    data: {
      name: "",
      type: "",
      description: ""
    },
    cardValues: [],
    countryValues: [],
    ipValues: [],
    types: [
      { "name": "ip", "label": "ip" },
      { "name": "card", "label": "card" },
      { "name": "country", "label": "Country" }
    ],
    value: "",
    valid: false,
    errors: {},
    isLoading: false
  };

  schema = {
    name: Joi.string()
      .required()
      .min(0)
      .max(100)
      .label("Name"),
    type: Joi.string()
      .required()
      .min(0)
      .max(100)
      .label("Type"),
    description: Joi.string()
      .required()
      .min(0)
      .max(100)
      .label("Description")
  };

  doSubmit = async () => {
    try {
      this.setState({ isLoading: true });
      if (this.state.cardValues.length !== 0 || this.state.ipValues.length !== 0 || this.state.countryValues.length !== 0) {
        const name = this.state.data.name;
        const type = this.state.data.type;
        const description = this.state.data.description;
        let values = [];
        if (type === "card")
          values = this.state.cardValues;
        else if (type === "country")
          values = this.state.countryValues;
        else
          values = this.state.ipValues;
        await this.props.addBlackListItemAction({ name, type, description, values }, this.props.currentPage, this.props.pageSize);
        swal({
          title: "Record is created",
          icon: "success",
          button: false,
          timer: 2000
        });
        await this.props.showModal(false);
      }
      else {
        this.setState({ isLoading: false });
        swal({
          title: "Please, enter value and press \"+\" icon",
          icon: "warning"
        });
      } 
    } catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  handleValueChange = (e) => {
    this.setState({
      value: e.target.value
    });
  }

  cardOrIp = () => {
    if (this.state.data.type === "card")
      return <Row><Col md={11} sm={11}>{
        <div>
          <ControlLabel htmlFor="value">Card</ControlLabel>
          <FormGroup>
            <FormControl
              name="value"
              type="text"
              label="Card"
              onChange={this.handleValueChange}
              value={this.state.value}
            />
          </FormGroup>
        </div>
      }</Col>
      <Col md={1} sm={1}>
        <i type="button"
          src="fas fa-plus"
          className="fas fa-plus"
          style={{ cursor: "pointer", color: "green", marginTop: "30px" }}
          onClick={this.click}
          value="" />
      </Col></Row>;
    else if (this.state.data.type === "ip")
      return <Row><Col md={11} sm={11}>{
        <div>
          <ControlLabel htmlFor="value">IP</ControlLabel>
          <FormGroup>
            <FormControl
              name="value"
              type="text"
              label="IP"
              onChange={this.handleValueChange}
              value={this.state.value}
            />
          </FormGroup>
        </div>
      }</Col>
      <Col md={1} sm={1}>
        <i type="button" src="fas fa-plus"
          className="fas fa-plus"
          style={{ cursor: "pointer", color: "green", marginTop: "30px" }}
          onClick={this.click} value="" />
      </Col></Row>;
    else if (this.state.data.type === "country")
      return <Row><Col md={11} sm={11}>{
        <div>
          <ControlLabel htmlFor="value">Country</ControlLabel>
          <FormGroup>
            <FormControl
              name="value"
              type="text"
              label="IP"
              onChange={this.handleValueChange}
              value={this.state.value}
            />
          </FormGroup>
        </div>
      }</Col>
      <Col md={1} sm={1}>
        <i type="button" src="fas fa-plus"
          className="fas fa-plus"
          style={{ cursor: "pointer", color: "green", marginTop: "30px" }}
          onClick={this.click} value="" />
      </Col></Row>;
    else
      return <div></div>;
  }

  valid = () => {
    if (this.state.data.type === "card") {
      const valid = Joi.validate(this.state.value, Joi.string().creditCard());
      if (valid.error)
        swal({
          title: `${valid.error}`,
          icon: "warning"
        });
      else
        this.setState({
          valid: true
        });
    }
    else if (this.state.data.type === "ip") {
      const valid = Joi.validate(this.state.value, Joi.string().ip({ version: [ "ipv4", "ipv6" ] }));
      if (valid.error)
        swal({
          title: `${valid.error}`,
          icon: "warning"
        });
      else
        this.setState({
          valid: true
        });
    }
    else if (this.state.data.type === "country") {
      const valid = Joi.validate(this.state.value, Joi.string().regex(/^[A-Z]{2}$/).required());
      if (valid.error)
        swal({
          title: `${valid.error}`,
          icon: "warning"
        });
      else
        this.setState({
          valid: true
        });
    }
  }

  click = async () => {
    await this.valid();
    if (this.state.value !== "" && this.state.valid === true) {
      await this.change(this.state.value);
      this.setState({
        value: ""
      });
    }
  }

  change = (val) => {
    if (this.state.data.type === "card")
      this.setState((prevState) => {
        return {
          cardValues: [ ...prevState.cardValues, val ],
          valid: false
        };
      });
    else if (this.state.data.type === "ip")
      this.setState((prevState) => {
        return {
          ipValues: [ ...prevState.ipValues, val ],
          valid: false
        };
      });
    else if (this.state.data.type === "country")
      this.setState((prevState) => {
        return {
          countryValues: [ ...prevState.countryValues, val ],
          valid: false
        };
      });
  }

  delete = (e) => {
    if (this.state.data.type === "card") {
      let cardValues = this.state.cardValues;
      cardValues.splice(e.target.name, 1);
      this.setState({
        cardValues
      });
    }
    else if (this.state.data.type === "ip") {
      let ipValues = this.state.ipValues;
      ipValues.splice(e.target.name, 1);
      this.setState({
        ipValues
      });
    }
    else if (this.state.data.type === "country") {
      let countryValues = this.state.countryValues;
      countryValues.splice(e.target.name, 1);
      this.setState({
        countryValues
      });
    }
  }

  render() {
    let row = [];
    let indexCard = 0;
    let indexValue = 0;
    if (this.state.data.type === "card") {
      this.state.cardValues.forEach(item => {
        row = [ ...row,
          <li className="list-group-item" style={{ textAlign: "right", backgroundColor: "#87cefa" }} key={indexCard}>
            {item + " "}<i
              className="far fa-trash-alt"
              style={{ cursor: "pointer", color: "red" }}
              name={indexCard}
              onClick={this.delete}
            />
          </li> ];
        indexCard++;
      });
    }
    else if (this.state.data.type === "ip") {
      this.state.ipValues.forEach(item => {
        row = [ ...row,
          <li className="list-group-item" style={{ textAlign: "right", backgroundColor: "#87cefa" }} key={indexValue}>
            {item + " "}<i
              className="far fa-trash-alt"
              style={{ cursor: "pointer", color: "red" }}
              name={indexValue}
              onClick={this.delete}
            />
          </li> ];
        indexValue++;
      });
    }
    else if (this.state.data.type === "country") {
      this.state.countryValues.forEach(item => {
        row = [ ...row,
          <li className="list-group-item" style={{ textAlign: "right", backgroundColor: "#87cefa" }} key={indexValue}>
            {item + " "}<i
              className="far fa-trash-alt"
              style={{ cursor: "pointer", color: "red" }}
              name={indexValue}
              onClick={this.delete}
            />
          </li> ];
        indexValue++;
      });
    }
    else
      row = [];
    return (
      <div className="card">
        <div className="content">
          <Form onSubmit={this.handleSubmit}>
            {this.renderInput("name", "Name")}
            {this.renderSelect("type", "Type", this.state.types)}
            {this.renderInput("description", "Description")}
            {this.cardOrIp()}
            {row}
            <div align="center">
              {this.state.isLoading
                ? <ReactLoading type='cylon' color='grey' />
                : this.renderButton("Add")
              }
            </div>
          </Form>
        </div>
      </div >
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentPage: state.blacklist.currentPage,
    pageSize: state.blacklist.pageSize
  };
};

export default connect(
  mapStateToProps,
  {
    addBlackListItemAction,
    showModal
  }
)(BlackListCreator);

BlackListCreator.propTypes = {
  addBlackListItemAction: PropTypes.func,
  showModal: PropTypes.func,
};