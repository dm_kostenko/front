import React from "react";
import { connect } from "react-redux";
import Joi from "joi-browser";
import CustomForm from "components/UI/Form/index";
import { Form } from "react-bootstrap";
import { showModal } from "../../../actions/modal";
import { getBlackListItemAction, updateBlackListItemAction } from "../../../actions/blacklists";
import PropTypes from "prop-types";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse";
import Spinner from "components/UI/Spinner";
import ReactLoading from "react-loading";

class BlackListEditor extends CustomForm {
  state = {
    data: {
      name: "",
      description: ""
    },
    errors: {},
    isLoading: false
  };

  schema = {
    name: Joi.string()
      .required()
      .min(0)
      .max(100)
      .label("Name"),
    description: Joi.string()
      .required()
      .min(0)
      .max(100)
      .label("Description")
  };

  async componentDidMount() {
    await this.props.getBlackListItemAction(this.props.blacklist_guid);
    const blackListItem = this.props.blackListItem;
    const data = this.state.data;
    data.name = blackListItem.name;
    data.description = blackListItem.description;
    this.setState({
      data
    });
  }

  doSubmit = async () => {
    try {
      this.setState({ isLoading: true });
      const guid = this.props.blacklist_guid;
      const name = this.state.data.name;
      const description = this.state.data.description;
      const type = this.props.blackListItem.type;
      await this.props.updateBlackListItemAction({ guid, type, name, description });
      this.setState({ isLoading: false });
      swal({
        title: "Successfully updated",
        icon: "success",
        button: false,
        timer: 2000
      });
      await this.props.showModal(false);
    } catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  change = (val) => {
    if (this.state.data.type === "card")
      this.setState((prevState) => {
        return {
          cardValues: [ ...prevState.cardValues, val ]
        };
      });
    else if (this.state.data.type === "ip")
      this.setState((prevState) => {
        return {
          ipValues: [ ...prevState.ipValues, val ]
        };
      });
  }

  delete = (e) => {

    if (this.state.data.type === "card") {
      let cardValues = this.state.cardValues;
      cardValues.splice(e.target.name, 1);
      this.setState({
        cardValues
      });
    }

    else if (this.state.data.type === "ip") {
      let ipValues = this.state.ipValues;
      ipValues.splice(e.target.name, 1);
      this.setState({
        ipValues
      });
    }
  }

  render() {
    if (this.props.itemLoading) {
      return <Spinner />;
    }
    else {
      return (
        <div className="card">
          <div className="content">
            <Form onSubmit={this.handleSubmit}>
              {this.renderInput("name", "Name")}
              {this.renderInput("description", "Description")}
              <div align="center">
                {this.state.isLoading
                  ? <ReactLoading type='cylon' color='grey' />
                  : this.renderButton("Save")}
              </div>
            </Form>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
    blackListItem: state.blacklist.blackItem,
    itemLoading: state.blacklist.itemLoading
  };
};

export default connect(
  mapStateToProps,
  {
    getBlackListItemAction,
    updateBlackListItemAction,
    showModal
  }
)(BlackListEditor);

BlackListEditor.propTypes = {
  blackListItem: PropTypes.array,
  getBlackListItemAction: PropTypes.func,
  updateBlackListItemAction: PropTypes.func,
  showModal: PropTypes.func,
};