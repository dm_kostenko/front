import React, { Component } from "react";
import { Row, Col, ControlLabel, FormGroup, FormControl, Button } from "react-bootstrap";
import Table from "components/UI/Table/index";
import { connect } from "react-redux";
import { getBlackListItemAction, addBlackListRuleAction, deleteBlackListRuleAction } from "actions/blacklists";
import Joi from "joi-browser";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ability from "config/ability";
import Spinner from "components/UI/Spinner";

class BlackListDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ""
    };
  }

  componentDidMount = async () => {
    await this.props.getBlackListItemAction(this.props.match.params.id);
  };

  delete = async (e) => {
    await this.props.deleteBlackListRuleAction({ blacklist_rule_guid:this.props.match.params.id, value:e.target.name, delete: true });
    await this.props.getBlackListItemAction(this.props.match.params.id);
  }

  click = () => {
    if (this.state.value !== "") {
      const request = async () => {
        await this.props.addBlackListRuleAction({ blacklist_rule_guid:this.props.match.params.id, value:this.state.value });
        await this.props.getBlackListItemAction(this.props.match.params.id);
        this.setState({
          value:""
        });
      };
      if (this.props.blackItem.type === "card") {
        const valid = Joi.validate(this.state.value, Joi.string().creditCard());
        if (valid.error)
          swal({
            title: `${valid.error}`,
            icon: "warning"
          });
        else {
          request();
        }
      }    
      else if (this.props.blackItem.type === "ip") {
        const valid = Joi.validate(this.state.value, Joi.string().ip({ version: [ "ipv4", "ipv6" ] }));
        if (valid.error)
          swal({
            title: `${valid.error}`,
            icon: "warning"
          });
        else {
          request();
        }
      }
      else if (this.props.blackItem.type === "country") {
        const valid = Joi.validate(this.state.value, Joi.string().regex(/^[A-Z]{2}$/).required());
        if (valid.error)
          swal({
            title: `${valid.error}`,
            icon: "warning"
          });
        else {
          request();
        }
      }      
    }
    else
      swal({
        title: "Value cannot be null",
        icon: "warning"
      });
  }

  change = (e) => {
    this.setState({
      value: e.target.value
    });
  }

  columns = [
    {
      content: value => (value),
      label: "Value"
    },
    ability.can("EXECUTE", "BLACKLISTDETAIL") && 
    {
      key: "delete",
      content: value => (
        <input
          type="image"
          src="far fa-trash-alt"
          className="far fa-trash-alt"
          style={{ cursor: "pointer", color: "red" }}
          name={value}
          value=""
          onClick={this.delete}
          alt=""
        ></input>
      ),
      label: "Delete"
    }
  ];

  render() {
    const blackItem = this.props.blackItem;
    if (this.props.itemLoading) return <Spinner/>;
    else return (
      <>
      <Button onClick={this.props.history.goBack} className="detail-back-button"><i className="fas fa-arrow-left"></i></Button>
      <div className="content">
        <Col md={6} sm={6} xs={6}>
          <div className="card">
            <div className="content">
              <div className="header">Info</div>
              <Row>
                <Col md={6} sm={6} xs={6} className="right"><label>Name:</label></Col>
                <Col md={6} sm={6} xs={6}><span>{blackItem.name}</span></Col>
              </Row>
              <Row>
                <Col md={6} sm={6} xs={6} className="right"><label>Type:</label></Col>
                <Col md={6} sm={6} xs={6}><span>{blackItem.type}</span></Col>
              </Row>
              <Row>
                <Col md={6} sm={6} xs={6} className="right"><label>Description:</label></Col>
                <Col md={6} sm={6} xs={6}><span>{blackItem.description}</span></Col>
              </Row>
            </div>
          </div>
        </Col>
        <Col md={6} sm={6} xs={6}>
          <div className="card">
            <div className="content">
              <div className="header">Values</div>
              {ability.can("EXECUTE", "BLACKLISTDETAIL") && 
            <>
            <Col className="col">
              <ControlLabel>{
                blackItem.type === "card" 
                  ? "card" 
                  : blackItem.type === "ip"
                    ? "IP"
                    : "Country"
              }
              </ControlLabel>
              <FormGroup>
                <FormControl
                  type="text"
                  onChange={this.change}
                  value={this.state.value}
                />
              </FormGroup>
            </Col>
          <i
            src="fas fa-plus"
            className="fas fa-plus"
            style={{ cursor: "pointer", color: "green" }} 
            onClick={this.click} 
            value="" 
          />
        </>}
              <Table 
                disableSearch={true}
                columns={this.columns} 
                data={blackItem.values}
              /> 
            </div>
          </div>
        </Col>   
      </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    blackItem: state.blacklist.blackItem,
    rules: state.blacklist.blackListRules,
    itemLoading: state.blacklist.itemLoading
  };
};

export default connect(mapStateToProps, {
  getBlackListItemAction,
  addBlackListRuleAction,
  deleteBlackListRuleAction
})(BlackListDetail);

BlackListDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  blackItem: PropTypes.object,
  rules: PropTypes.array,
  getBlackListItemAction: PropTypes.func,
  addBlackListRuleAction: PropTypes.func,
  deleteBlackListRuleAction: PropTypes.func,
  itemLoading: PropTypes.bool,
};