import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Row, Form, ControlLabel, FormControl, Button, FormGroup } from "react-bootstrap";
import { getUserAccountAction, editUserAccountAction } from "actions/useraccounts";
import { parseResponse } from "helpers/parseResponse";
import Spinner from "components/UI/Spinner";
import PasswordStrengthMeter from "components/UI/PasswordIndicator/PasswordStrengthMeter";
import swal from "sweetalert";
import { getLoginUserProfileAction } from "actions/logins";
import PropTypes from "prop-types";

class UserAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      newPassword: "",
      confirmPassword: "",
      isPasswordConfirmed: false,
      changePassword: false,
      email: "",
      phone: "",
      authType: false,
      userProfileGuid: "",
      firstName: "",
      lastName: "",
      personalEmail: "",
      personalPhone: "",
      companyName: "",
      note: "",
    };
  }

  async componentDidMount() {
    try {
      await this.props.getUserAccountAction();
      await this.props.getLoginUserProfileAction(this.props.useraccount.user_profile_id);
      return this.setState(
        this.mapToViewModel(this.props.useraccount, this.props.userprofile)
      );
    }
    catch (ex) {
      throw (ex);
    }
  }

  mapToViewModel(useraccount, user_profile) {
    return {
      guid: useraccount.guid,
      username: useraccount.username,
      email: useraccount.email,
      phone: useraccount.phone,
      authType: useraccount.auth_type !== "login-password",
      userProfileGuid: user_profile.guid,
      firstName: user_profile.first_name,
      lastName: user_profile.last_name,
      personalEmail: user_profile.email,
      personalPhone: user_profile.phone,
      companyName: user_profile.company_name,
      note: user_profile.note
    };
  }

  handleChange = ({ currentTarget: input }) => {
    const data = { ...this.state.data };
    data[input.name] = input.value;

    this.setState(data);
  };

  isPasswordConfirmed = () => {
    if (this.state.newPassword === this.state.confirmPassword) {
      this.setState({
        isPasswordConfirmed: true
      });
      return true;
    }
    else {
      this.setState({
        isPasswordConfirmed: false
      });
      return false;
    }
  };

  handlePasswordChanges(event) {
    this.setState({ newPassword: event.target.value });
  }


  handleChangePassword = () => {
    this.setState({
      changePassword: !this.state.changePassword
    });
  }

  cancelChangePassword = () => {
    this.setState({
      changePassword: !this.state.changePassword,
      password: "",
      newPassword: "",
      confirmPassword: ""
    });
  }

  handleChangeCheckbox = () => {
    this.setState({
      authType: !this.state.authType
    });
  }

  formValidation = (data) => {
    if (data === "") return "error";
    else return "success";
  };

  generalValidation = () => {
    if(this.state.changePassword)
      return this.formValidation(this.state.username) === "success" &&
        this.formValidation(this.state.password) === "success" &&
        this.formValidation(this.state.newPassword) === "success" &&
        this.formValidation(this.state.confirmPassword) === "success" &&
        this.formValidation(this.state.email) === "success" &&
        this.formValidation(this.state.phone) === "success";
    else
      return this.formValidation(this.state.username) === "success" &&
        this.formValidation(this.state.email) === "success" &&
        this.formValidation(this.state.phone) === "success";
  };

  personalDataValidation = () => {
    if
    (
      this.state.firstName === "" &&
      this.state.lastName === "" &&
      this.state.personalEmail === "" &&
      this.state.personalPhone === "" &&
      this.state.companyName === "" &&
      this.state.note === ""
    ) return "empty";
    else if
    (
      this.state.firstName !== "" &&
      this.state.lastName !== "" &&
      this.state.personalEmail !== "" &&
      this.state.personalPhone !== "" &&
      this.state.companyName !== "" &&
      this.state.note !== ""
    )
      return "ok";
    else return "error";
  }


  handleSubmit = async (e) => {
    this.setState({ 
      changePassword: false,
      password: "",
      newPassword: "",
      confirmPassword: ""
    });
    e.preventDefault();
    if (!this.generalValidation())
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    else if (this.generalValidation() && !this.isPasswordConfirmed())
      swal({
        title: "Passwords don't match",
        icon: "error"
      });
    else {
      const data = this.state;
      try {
        switch (this.personalDataValidation()) {
        case "ok": { 
          await this.props.editUserAccountAction({
            guid: this.props.useraccount.guid,
            user_profile_guid: data.userProfileGuid,
            username: data.username,
            auth_type: data.authType ? "login-password-mail" : "login-password",
            username_canonical: data.username,
            email: data.email,
            email_canonical: data.email,
            old_password: data.changePassword === true ? data.password : undefined,
            password: data.changePassword === true ? data.newPassword : undefined,
            phone: data.phone,
            user_profile: {
              first_name: data.firstName,
              last_name: data.lastName,
              email: data.personalEmail,
              phone: data.personalPhone,
              company_name: data.companyName,
              note: data.note
            }
          });
          swal({
            title: "Account is updated",
            icon: "success",
            button: false,
            timer: 2000
          });
          break;
        }
        case "empty": {
          await this.props.editUserAccountAction({
            guid: this.props.useraccount.guid,
            username: data.username,
            auth_type: data.authType ? "login-password-mail" : "login-password",
            username_canonical: data.username,
            email: data.email,
            email_canonical: data.email,
            old_password: data.changePassword === true ? data.password : undefined,
            password: data.changePassword === true ? data.newPassword : undefined,
            phone: data.phone,
          });
          swal({
            title: "Account is updated",
            icon: "success",
            button: false,
            timer: 2000
          });
          break;
        }
        case "error": {
          swal({
            title: "Please, enter information in every required field of personal Info",
            icon: "warning"
          });
          break;
        }
        default: break;
        }
      }
      catch (error) {
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
    }
  };



  render() {
    const data = this.state;
    if (this.props.isLoginLoading) {
      return <Spinner />;
    }
    else return (
      <div style={{ width: "70%", margin: "auto", textAlign: "center", marginTop:"20px" }}>
        <Form onSubmit={this.handleSubmit} autoComplete="off">
        
          <div className="card">
            <h3 align='center'>General info</h3>
            <Col>
              <Row className="rowEdit">
                <Col className='col-md-3'/>
                <Col className='col-md-6'>
                  <ControlLabel htmlFor="username" style={{ float:"left" }}>Username:</ControlLabel>
                  <FormGroup validationState={this.formValidation(data.username)}>
                    <FormControl
                      name="username"
                      type="text"
                      value={data.username}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
                <Col className='col-md-3'/>
              </Row>

              <Row className="rowEdit">
                <Col className='col-md-3'/>
                <Col className='col-md-6'>
                  <span style={{ float:"left", paddingTop:"10px", color: "grey" }}>Do you want to change your password?</span>
                  {!this.state.changePassword ?      
                    <Button onClick={this.handleChangePassword} style={{ float:"right" }}>Change Password</Button>
                    :
                    <Button onClick={this.cancelChangePassword} style={{ float:"right" }}>Cancel</Button>}
                </Col>
                <Col className='col-md-3'/>
              </Row>

              {this.state.changePassword ?           
                (<React.Fragment>
                  <Row className="rowEdit">
                    <Col className='col-md-3'/>
                    <Col className='col-md-6'>
                      <ControlLabel style={{ float:"left" }}>Current Password:</ControlLabel>
                      <FormGroup>
                        <FormControl
                          name="password"
                          type="password"
                          value={data.password}
                          onChange={(e) => this.handleChange(e)}
                        />
                      </FormGroup>
                    </Col>
                    <Col className='col-md-3'/>
                  </Row>         
                  <Row className="rowEdit">
                    <Col className='col-md-3'/>
                    <Col className='col-md-6'>
                      <ControlLabel style={{ float:"left" }}>New Password:</ControlLabel>
                      {/* <FormGroup validationState={this.formValidation(data.newPassword)}> */}
                      <FormGroup>
                        <PasswordStrengthMeter 
                          name="newPassword" 
                          value={data.newPassword} 
                          password={data.newPassword} 
                          onChange={(e) => this.handlePasswordChanges(e)}/>
                      </FormGroup>
                    </Col>
                    <Col className='col-md-3'/>
                  </Row>

                  <Row className="rowEdit">
                    <Col className='col-md-3'/>
                    <Col className='col-md-6'>
                      <ControlLabel style={{ float:"left" }}>Confirm New Password:</ControlLabel>
                      <FormGroup>
                        <FormControl
                          name="confirmPassword"
                          type="password"
                          value={data.confirmPassword}
                          onChange={(e) => this.handleChange(e)}
                        />
                      </FormGroup>
                    </Col>
                    <Col className='col-md-3'/>
                  </Row></React.Fragment>) : null}

              <Row className="rowEdit">
                <Col className='col-md-3'/>
                <Col className='col-md-6'>
                  <ControlLabel style={{ float:"left" }}>Email:</ControlLabel>
                  <FormGroup validationState={this.formValidation(data.email)}>
                    <FormControl
                      name="email"
                      type="email"
                      value={data.email}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
                <Col className='col-md-3'/>
              </Row>

              <Row className="rowEdit">
                <Col className='col-md-3'/>
                <Col className='col-md-6'>
                  <ControlLabel style={{ float:"left" }}>Phone:</ControlLabel>
                  <FormGroup validationState={this.formValidation(data.phone)}>
                    <FormControl
                      name="phone"
                      type="text"
                      value={data.phone}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
                <Col className='col-md-3'/>
              </Row>

              <Row className="rowEdit">
                <Col className='col-md-3'/>
                <Col className='col-md-6'>
                  <ControlLabel style={{ float: "left" }}>Two-factor authentication:</ControlLabel>                                  
                  <input
                    type="checkbox"
                    onChange={this.handleChangeCheckbox}
                    checked={this.state.authType}
                  />
                </Col>
                <Col className='col-md-3'/>
              </Row>
            </Col>
          </div>

        
          <div className="card">
            <h3 align='center'>Personal data</h3>
            <Col>
              <Row className="rowEdit">
                <Col className='col-md-3'/>
                <Col className='col-md-6'>
                  <ControlLabel style={{ float:"left" }}>Name:</ControlLabel>
                  <FormGroup validationState={this.formValidation(data.firstName)}>
                    <FormControl
                      name="firstName"
                      type="text"
                      value={data.firstName}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
                <Col className='col-md-3'/>
              </Row>

              <Row className="rowEdit">
                <Col className='col-md-3'/>
                <Col className='col-md-6'>
                  <ControlLabel style={{ float:"left" }}>Surname:</ControlLabel>
                  <FormGroup validationState={this.formValidation(data.lastName)}>
                    <FormControl
                      name="lastName"
                      type="text"
                      value={data.lastName}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
                <Col className='col-md-3'/>
              </Row>

              <Row className="rowEdit">
                <Col className='col-md-3'/>
                <Col className='col-md-6'>
                  <ControlLabel style={{ float:"left" }}>Email:</ControlLabel>
                  <FormGroup validationState={this.formValidation(data.personalEmail)}>
                    <FormControl
                      name="personalEmail"
                      type="email"
                      value={data.personalEmail}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
                <Col className='col-md-3'/>
              </Row>

              <Row className="rowEdit">
                <Col className='col-md-3'/>
                <Col className='col-md-6'>
                  <ControlLabel style={{ float:"left" }}>Phone:</ControlLabel>
                  <FormGroup validationState={this.formValidation(data.personalPhone)}>
                    <FormControl
                      name="personalPhone"
                      type="text"
                      value={data.personalPhone}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
                <Col className='col-md-3'/>
              </Row>

              <Row className="rowEdit">
                <Col className='col-md-3'/>
                <Col className='col-md-6'>
                  <ControlLabel style={{ float:"left" }}>Company:</ControlLabel>
                  <FormGroup validationState={this.formValidation(data.companyName)}>
                    <FormControl
                      name="companyName"
                      type="text"
                      value={data.companyName}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
                <Col className='col-md-3'/>
              </Row>

              <Row className="rowEdit">
                <Col className='col-md-3'/>
                <Col className='col-md-6'>
                  <ControlLabel style={{ float:"left" }}>Note:</ControlLabel>
                  <FormGroup validationState={this.formValidation(data.note)}>
                    <FormControl
                      name="note"
                      type="textarea"
                      value={data.note}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
                <Col className='col-md-3'/>
              </Row>
            </Col>
          </div>
          <div align="center">
            <Button type="submit">Save</Button>
          </div>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    useraccount: state.useraccounts.useraccount,
    userprofile: state.logins.userProfile,
    isLoginLoading: state.useraccounts.userAccountLoading
  };
};

export default connect(mapStateToProps, {
  getUserAccountAction,
  getLoginUserProfileAction,
  editUserAccountAction
})(UserAccount);

UserAccount.propTypes = {
  isLoginLoading: PropTypes.bool,
  editUserAccountAction:  PropTypes.func,
  useraccount: PropTypes.object,
  getLoginUserProfileAction: PropTypes.func,
  getUserAccountAction: PropTypes.func,
  userprofile: PropTypes.object,
};