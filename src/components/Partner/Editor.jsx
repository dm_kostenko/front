import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Row, FormGroup, ControlLabel, FormControl, Button, Form } from "react-bootstrap";
import { getAllGroups } from "actions/groups";
import { getPartnerAction, editPartnerAction, deleteGroupFromPartnerAction, addGroupToPartnerAction, getPartnerGroupsAction } from "actions/partners";
import { showModal } from "actions/modal";
import PropTypes from "prop-types";
import PartnerGroupsManagement from "./GroupsManagement";
import { parseResponse } from "helpers/parseResponse.js";
import Spinner from "components/UI/Spinner/index";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class PartnerEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      label: "",
      data: {
        name: "",
        type: "",
        description: "",
        selectedValue: "",
        group_guid: "",
        partner_guid: "",
      },
      select: [],
      selectedValue: [],
      changedGroups: [],
      isLoading: false
    };
  }

  async componentDidMount() {
    await this.props.getPartnerAction(this.props.guid);
    await this.props.getAllGroups();
    await this.props.getPartnerGroupsAction(this.props.guid);
    const { partner, groups } = this.props;
    this.setState({
      label: "Groups",
      data: partner,
      selectedValue: partner.groups,
      select: groups
    });
  }

  formValidation = (data) => {
    if (data === "") return "error";
    else return "success";
  };

  handleChange = ({ currentTarget: input }) => {
    const data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data });
  };

  generalValidation = () => {
    const { data } = this.state;
    return this.formValidation(data.name) === "success" &&
      this.formValidation(data.type) === "success" &&
      this.formValidation(data.selectedValue) === "success";
  };

  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    try {
      const groups = this.props.changedGroups;
      groups.forEach(group => {
        if (group.delete) {
          this.props.deleteGroupFromPartnerAction(group.guid);
        }
        else {
          this.props.addGroupToPartnerAction(group.guid, this.props.guid);
        }
      });
      const data = {
        guid: this.props.guid,
        name: this.state.data.name,
        type: this.state.data.type
      };
      await this.props.editPartnerAction(data);
      swal({
        title: "Partner is updated",
        icon: "success",
        button:false,
        timer: 2000
      });
      await this.props.showModal(false);
    }
    catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };


  render() {
    const { data } = this.state;
    if (this.props.groupsLoading ||
      this.props.partnerLoading ||
      this.props.partnerGroupsLoading) {
      return <Spinner />;
    }
    else return (
      <div>
        <div className="card">
          <div className="content"><label>ID: {this.props.guid}</label></div>
        </div>
        <div className="card">
          <Form onSubmit={this.handleSubmit} autoComplete="off">
            <Row className="rowEdit" >
              <Col className='col'>
                <ControlLabel>Name:</ControlLabel>
                <FormGroup validationState={this.formValidation(data.username)}>
                  <FormControl
                    name="name"
                    type="text"
                    value={data.name}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row className="rowEdit">
              <Col className='col'>
                <ControlLabel>Type:</ControlLabel>
                <FormGroup validationState={this.formValidation(this.state.data.username)}>
                  <FormControl
                    name="type"
                    type="text"
                    value={this.state.data.type}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row className="rowEdit">
              <Col className='col'>
                <ControlLabel>Groups:</ControlLabel>
                <PartnerGroupsManagement partner_guid={this.props.guid} />
              </Col>
            </Row>
            <div align="center">
              {this.state.isLoading 
                ? <ReactLoading type='cylon' color='grey' />
                : <Button type="submit">Save</Button>}
            </div>
          </Form>
        </div>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    groups: state.groups.groupsList,
    changedMerchants: state.groups.changedMerchants,
    groupsLoading: state.groups.groupsLoading,

    partner: state.partners.partner,
    changedGroups: state.partners.changedGroups,
    partnerLoading: state.partners.partnerLoading,
    partnerGroupsLoading: state.partners.partnerGroupsLoading,
  };
};

export default connect(mapStateToProps, {
  getAllGroups,
  getPartnerAction,
  getPartnerGroupsAction,
  editPartnerAction,
  showModal,
  deleteGroupFromPartnerAction,
  addGroupToPartnerAction,
})(PartnerEditor);

PartnerEditor.propTypes = {
  getAllGroups: PropTypes.func,
  getPartnerAction: PropTypes.func,
  editPartnerAction: PropTypes.func,
  showModal: PropTypes.func,
  deleteGroupFromPartnerAction: PropTypes.func,
  addGroupFromPartnerAction: PropTypes.func,
  addGroupToPartnerAction: PropTypes.func,
  getPartnerGroupsAction: PropTypes.func,

  groups: PropTypes.array,

  partner: PropTypes.object,
  changedGroups: PropTypes.array,

  type: PropTypes.string,
  id: PropTypes.string,
  guid: PropTypes.string,

  groupsLoading: PropTypes.bool,
  partnerLoading: PropTypes.bool, 
  partnerGroupsLoading: PropTypes.bool,

};