import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import Table from "components/UI/Table/index";
import RowAddLogin from "components/RowAddLogin";
import Pagination from "components/UI/Pagination/index";
import moment from "moment";
import { connect } from "react-redux";
import {
  getPartnerAction,
  getPartnerLoginsAction,
  addPartnerLoginAction,
  deleteGroupFromPartnerAction,
  getPartnerGroupsAction
} from "actions/partners";
import { getFreeLoginsAction } from "actions/logins";
import { getAllRoles } from "actions/roles";
import Can from "components/Can/index";
import ability from "config/ability";
import PropTypes from "prop-types";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse";
import Spinner from "components/UI/Spinner";

class PartnerDetail extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentLoginsPage: 1,
      currentGroupsPage: 1,
      pageSize: 10
    };
  }

  columnsPartnerGroups = [
    { path: "guid", label: "Guid" },
    { path: "name", label: "Group" },
    ability.can("EXECUTE", "PARTNERDETAIL") && {
      key: "delete",
      content: group => (
        <i
          className="far fa-trash-alt"
          style={{ cursor: "pointer", color: "red", marginLeft: "50px" }}
          onClick={() =>
            swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
              .then((willDelete) => {
                if (willDelete) {
                  this.handleDeleteGroup(group.guid);
                  swal("Deleted", {
                    icon: "success",
                    button:false,
                    timer: 2000
                  });
                }
              })
          }
        />
      ),
      label: "Delete from the partner"
    }
  ];

  handleDeleteGroup = async (guid) => {
    try {
      await this.props.deleteGroupFromPartnerAction(guid, this.state.currentGroupsPage, this.state.pageSize, this.props.match.params.id);
    } catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  handleDelete = async (loginid, roleid) => {
    try {
      await this.props.addPartnerLoginAction(this.props.match.params.id, {
        login_guid: loginid,
        role_guid: roleid,
        delete: true
      });
      await this.props.getPartnerLoginsAction(this.props.match.params.id);
    } catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  columnsLoginsForPartner = [
    { path: "username", label: "Username" },
    { path: "role.name", label: "Role" },
    ability.can("EXECUTE", "PARTNERDETAIL") && {
      key: "delete",
      content: partnerLogin => (
        <i
          className="far fa-trash-alt"
          style={{ cursor: "pointer", color: "red" }}
          onClick={() =>
            swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
              .then((willDelete) => {
                if (willDelete) {
                  this.handleDelete(partnerLogin.guid, partnerLogin.role_guid);
                  swal("Deleted", {
                    icon: "success",
                    button:false,
                    timer: 2000
                  });
                }
              })
          }
        />
      ),
      label: "Delete"
    }
  ];

  componentDidMount = async () => {
    await this.props.getPartnerAction(this.props.match.params.id);
    await this.props.getPartnerGroupsAction(this.props.match.params.id, 1, this.state.pageSize);
    await this.props.getPartnerLoginsAction(this.props.match.params.id, 1, this.state.pageSize);
    await this.props.getFreeLoginsAction("partner");
    await this.props.getAllRoles(undefined, undefined, undefined, "partner");
  };

  handleLoginsPageChange = async (page) => {
    await this.props.getGroupLoginsAction(this.props.match.params.id, page, this.state.pageSize);
    this.setState({
      currentLoginsPage: page
    });
  };

  handleGroupsPageChange = async (page) => {
    await this.props.getpartnerGroupsAction(this.props.match.params.id, page, this.state.pageSize);
    this.setState({
      currentGroupsPage: page
    });
  };

  update = async id => {
    await this.props.getPartnerAction(id);
    await this.props.getPartnerGroupsAction(id);
    await this.props.getPartnerLoginsAction(id);
  };

  getLoginsPagedData = () => {
    const { loginsCount } = this.props;
    let { pageSize } = this.state;
    const loginsPagesCount = loginsCount ? (loginsCount / pageSize) + (1 && !!(loginsCount % pageSize)) : 0;
    return { loginsPagesCount };
  };


  getGroupsPagedData = () => {
    const { groupsCount } = this.props;
    let { pageSize } = this.state;
    const groupsPagesCount = groupsCount ? (groupsCount / pageSize) + (1 && !!(groupsCount % pageSize)) : 0;
    return { groupsPagesCount };
  };

  render() {
    const { partner } = this.props;
    const { currentLoginsPage, currentGroupsPage } = this.state;
    const { loginsPagesCount } = this.getLoginsPagedData();
    const { groupsPagesCount } = this.getGroupsPagedData();
    const { partnerLogins, partnerGroups } = this.props;
    if (this.props.partnerLoginsLoading || this.props.partnerLoading || this.props.partnerGroupsLoading) return <Spinner/>;
    else return (
      <>
      <Button onClick={this.props.history.goBack} className="detail-back-button"><i className="fas fa-arrow-left"></i></Button>
      <div className="content" style={{ overflow:"auto" }}>
        <Row style={{ marginLeft: "0px" }}>
          <div className="card">
            <div className="title">{partner ? partner.name : null}</div>
          </div>
          <div className="card-container">
            <div className="card">
              <div className="header">Info</div>
              <div className="content">
                <Row>
                  <Col md={12}>
                    <Row>
                      <Col md={5} lg={5} xs={5} className="right">
                        <label>GUID:</label>
                      </Col>
                      <Col md={5} lg={5} xs={5}>
                        <span>{this.props.match.params.id}</span>
                      </Col>
                    </Row>
                    <Row>
                      <Col md={5} lg={5} xs={5} className="right">
                        <label>Type:</label>
                      </Col>
                      <Col md={5} lg={5} xs={5}>
                        <span>{partner ? partner.type : null}</span>
                      </Col>
                    </Row>
                    <Row>
                      <Col md={5} lg={5} xs={5} className="right">
                        <label>Created at:</label>
                      </Col>
                      <Col md={5} lg={5} xs={5}>
                        <span>
                          {partner
                            ? partner.created_at !== null
                              ? moment(partner.created_at)
                                .utcOffset(0)
                                .format("D MMM YYYY")
                              : null
                            : null}
                        </span>
                      </Col>
                    </Row>
                    <Row>
                      <Col md={5} lg={5} xs={5} className="right">
                        <label>Created by:</label>
                      </Col>
                      <Col md={5} lg={5} xs={5}>
                        <span>{partner ? partner.created_by : null}</span>
                      </Col>
                    </Row>
                    <Row>
                      <Col md={5} lg={5} xs={5} className="right">
                        <label>Updated at:</label>
                      </Col>
                      <Col md={5} lg={5} xs={5}>
                        <span>
                          {partner
                            ? partner.updated_at !== null
                              ? moment(partner.updated_at)
                                .utcOffset(0)
                                .format("D MMM YYYY")
                              : null
                            : null}
                        </span>
                      </Col>
                    </Row>
                    <Row>
                      <Col md={5} lg={5} xs={5} className="right">
                        <label>Updated by:</label>
                      </Col>
                      <Col md={5} lg={5} xs={5}>
                        <span>{partner ? partner.updated_by : null}</span>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </div>
            </div>

            <div className="card" >
              <div className="header">Groups</div>
              <div className="content">
                <Table
                  columns={this.columnsPartnerGroups}
                  data={partnerGroups ? partnerGroups : null}
                  disableSearch={true}
                />
                <Pagination
                  pagesCount={groupsPagesCount}
                  currentPage={currentGroupsPage}
                  onPageChange={this.handleGroupsPageChange}
                />
              </div>
            </div>

            <div className="card">
              <div className="header">Logins</div>
              <div className="content">
                <Table
                  columns={this.columnsLoginsForPartner}
                  data={partnerLogins ? partnerLogins : []}
                  disableSearch={true}
                />
                <Pagination
                  pagesCount={loginsPagesCount}
                  currentPage={currentLoginsPage}
                  onPageChange={this.handleLoginsPageChange}
                />
                <Can do="EXECUTE" on="PARTNERDETAIL">
                  <Col md={3} sm={3} xs={3} />
                  <Col md={6} sm={6} xs={6}>
                    <RowAddLogin
                      type="partner"
                      id={this.props.match.params.id}
                      update={this.update}
                    />
                  </Col>
                  <Col md={3} sm={3} xs={3} />
                </Can>
              </div>
            </div>
          </div>
        </Row>
      </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    partner: state.partners.partner,
    partnerGroups: state.partners.partnerGroups,
    groupsCount: state.partners.groupsCount,
    partnerLogins: state.partners.partnerLogins,
    loginsCount: state.partners.loginsCount,
    partnerLoading: state.partners.partnerLoading,
    partnerLoginsLoading: state.partners.partnerLoginsLoading,
    partnerGroupsLoading: state.partners.partnerGroupsLoading
  };
};

export default connect(
  mapStateToProps,
  {
    getPartnerAction,
    getPartnerLoginsAction,
    addPartnerLoginAction,
    deleteGroupFromPartnerAction,
    getPartnerGroupsAction,
    getAllRoles,
    getFreeLoginsAction
  }
)(PartnerDetail);

PartnerDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  shops: PropTypes.array,
  partner: PropTypes.object,
  count: PropTypes.number,
  loginsCount: PropTypes.number,
  groupsCount: PropTypes.number,
  partnerLogins: PropTypes.array,
  partnerGroups: PropTypes.array,
  partnerLoading: PropTypes.bool,
  partnerLoginsLoading: PropTypes.bool,
  partnerGroupsLoading: PropTypes.bool,

  getPartnerAction: PropTypes.func,
  getPartnerLoginsAction: PropTypes.func,
  addPartnerLoginAction: PropTypes.func,
  deleteGroupFromPartnerAction: PropTypes.func,
  getPartnerGroupsAction: PropTypes.func,
  getGroupLoginsAction: PropTypes.func,
  getpartnerGroupsAction:  PropTypes.func,
  getFreeLoginsAction: PropTypes.func,
  getAllRoles: PropTypes.func,
};