import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../factories/Main";
import PartnerCreator from "./Creator";
import ability from "config/ability";
import { getAllPartners, deletePartnerAction } from "../../actions/partners";
import { searchInPartners, reset } from "../../actions/search";
import PartnerEditor from "./Editor";
import Modal from "components/UI/Modal";
import { Link } from "react-router-dom";

const columns = [
  {
    path: "guid",
    label: "ID",
    key: "guid",
    content: partner => (
      <Link className="link" to={`/about/partner/${partner.guid}`} >{partner.guid}</Link>
    ),
  },
  { path: "name", label: "Name" },
  ability.can("EXECUTE", "PARTNERS") &&
  {
    key: "edit",
    content: partner => (
      <Modal 
        header="Edit partner" 
        button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }}/>}
        content={<PartnerEditor guid={partner.guid} type="partner"/>} 
      />),
    label: "Edit"
  },
  ability.can("EXECUTE", "PARTNERS") &&
  {
    key: "delete",
    content: () => (
      <i
        className="far fa-trash-alt"
        style={{ cursor: "pointer", color: "red" }}
      />
    ),
    label: "Delete"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.partners.partnersList,
    count: state.partners.count,
    searchData: state.search.partnersSearch,
    isSearch: state.search.isSearch,
    loading: state.partners.partnersLoading,
    name: "partners",
    columns,
    modal: <PartnerCreator />
  };
};

export default connect(mapStateToProps, {
  get: getAllPartners,
  delete: deletePartnerAction,
  search: searchInPartners,
  reset
})(AbstractComponent);
