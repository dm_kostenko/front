import React, { Component } from "react";
import { Col, Row, ControlLabel, FormControl, FormGroup } from "react-bootstrap";
import Button from "react-bootstrap/es/Button";
import MultiSelect from "components/UI/MultiSelect";
import LoginCreator from "components/Login/Creator";
import Radio from "components/UI/Radio";
import { connect } from "react-redux";
import { getAllGroups } from "../../actions/groups";
import { getAllRoles } from "../../actions/roles";
import { addPartnerAction, addPartnerLoginAction, } from "../../actions/partners";
import { editGroupAction } from "../../actions/groups";
import { getFreeLoginsAction } from "../../actions/logins";
import { showModal } from "../../actions/modal";
import { getAuthData } from "../../services/paymentBackendAPI/backendPlatform";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class PartnerCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: "0",
      radioValid: false,
      name: "",
      type: "",
      login_guid: "",
      role_guid: "",
      group_guid: "",
      partner_guid: "",
      newLoginGuid: "",
      token: {},
      selectedGroups: [],
      isLoading: false
    };
  }

  async componentDidMount() {
    const token = getAuthData().userPayload;
    this.setState({ token });
    if (token.partner) this.setState({ partner_guid: token.partner.partner_guid });
    await this.props.getAllRoles(undefined, undefined, undefined, "partner");
    await this.props.getAllGroups();
    await this.props.getFreeLoginsAction("partner");
  }

  validate(val) {
    return (val !== "");
  }

  updateState = (newLoginGuid) => {
    this.setState({
      newLoginGuid
    });
  };

  chooseExistingLogin = () => {
    if (this.state.radio === "2") {
      let array = [];
      const logins = this.props.logins;
      for (let i = 0; i < logins.length; i++) {
        let item = {
          guid: logins[i].guid,
          name: logins[i].username
        };
        array = [ ...array, item ];
      }
      return (<div style={{ textAlign: "center" }}>
        <Row>
          <Col md={1} />
          <Col md={10}>
            <MultiSelect
              options={array}
              multi={false}
              onSelect={this.onSelectLogin}
            />
          </Col>
          <Col md={1} />
        </Row>
      </div>);
    }
    return null;
  }

  chooseNewLogin = () => {
    if (this.state.radio === "1")
      return <LoginCreator flag={true} updateState={this.updateState} />;
    return null;
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSelectLogin = (option) => {
    this.setState({
      login_guid: option.guid
    });
  };

  onSelectGroups = (option) => {
    this.setState({
      selectedGroups: option
    });
  };

  onSelectRole = (option) => {
    this.setState({
      role_guid: option.guid
    });
  };

  handleRadio = event => {
    const target = event.target;
    if (target.value === "1")
      this.setState({
        radio: "1",
        radioValid: true
      });
    if (target.value === "2") {
      this.setState({
        radio: "2",
        radioValid: true
      });
    }
  };

  requestServices = async (userGuid) => {
    let { name, type, role_guid, selectedGroups } = this.state;
    let login_guid = userGuid;
    try {
      await this.props.addPartnerAction({
        name,
        type
      }, this.props.currentPagePartner, this.props.pageSizePartner, this.props.partnersSearch);
      const partner_guid = this.props.partner_guid;
      for (let i = 0; i < selectedGroups.length; i++) {
        const data = {
          guid: selectedGroups[i].guid,
          partner_guid
        };
        this.props.editGroupAction(data);
      }
      await this.props.addPartnerLoginAction(
        partner_guid,
        {
          login_guid,
          role_guid
        });
      swal({
        title: "Partner is created",
        icon: "success",
        button:false,
        timer: 2000
      });
      await this.props.showModal(false);
    } catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  }

  handleSubmit = (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    const { name, role_guid, type, newLoginGuid, login_guid } = this.state;
    if (this.validate(name)
      && this.validate(type)
      && this.validate(role_guid)) {
      if (this.validate(newLoginGuid)) {
        this.requestServices(this.state.newLoginGuid);
      }
      else if(this.validate(login_guid)) {
        this.requestServices(this.state.login_guid);
      }
    }
    else {
      this.setState({ isLoading: false });
      swal({
        title: "Please, fill in all the fields",
        icon: "warning"
      });
    }    
  }

  render() {
    const chooseNewLogin = this.chooseNewLogin();
    const chooseExistingLogin = this.chooseExistingLogin();
    const groups = this.props.groups ? this.props.groups : [];
    const filteredGroups = groups.filter(group => group.partner_guid === null);
    return (
      <div style={{ padding: "20px" }}>
        <Row>
          <Col sm={1} />
          <Col md={12}>
            <div className="card">
              <Row className="rowReg">
                <Col md={3}>
                  <ControlLabel>Name*</ControlLabel>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <FormControl
                      name="name"
                      type="text"
                      value={this.state.name}
                      onChange={this.handleChange}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row className="rowReg">
                <Col md={3}>
                  <ControlLabel>Type*</ControlLabel>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <FormControl
                      name="type"
                      type="text"
                      value={this.state.type}
                      onChange={this.handleChange}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row className="rowReg">
                <Col md={3}>
                  <ControlLabel>Role*</ControlLabel>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <MultiSelect name="Roles" options={this.props.roles ? this.props.roles : []} multi={false} onSelect={this.onSelectRole} />
                  </FormGroup>
                </Col>
              </Row>
              <Row className="rowReg">
                <Col md={3}>
                  <ControlLabel>Groups*</ControlLabel>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <MultiSelect
                      name="Groups"
                      options={filteredGroups}
                      multi={true}
                      onSelect={this.onSelectGroups}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row className="rowReg">
                <Col md={12}>
                  <FormGroup>
                    <Radio
                      number="3"
                      option="1"
                      name="radio"
                      onChange={this.handleRadio}
                      label="Add new login"
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <FormGroup>
                    <Radio
                      number="4"
                      option="2"
                      name="radio"
                      onChange={this.handleRadio}
                      label="Choose existing login"
                    />
                  </FormGroup>
                </Col>
              </Row>
              {chooseExistingLogin}
            </div>
          </Col>
        </Row>
        {chooseNewLogin}
        <div align="center">
          {this.state.isLoading 
            ? <ReactLoading type='cylon' color='grey' />
            : <Button type="submit" onClick={this.handleSubmit}>Add</Button>}
        </div>
        <Col sm={1} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    groups: state.groups.groupsList,
    roles: state.roles.rolesList,
    logins: state.logins.freeLoginsList,
    merchant_guid: state.merchants.merchant_guid,
    partner_guid: state.partners.partner_guid,
    currentPagePartner: state.partners.currentPage,
    pageSizePartner: state.partners.pageSize,
    partnersSearch: state.search.partnersSearch,
  };
};

export default connect(mapStateToProps,
  {
    addPartnerAction,
    addPartnerLoginAction,
    getAllGroups,
    getAllRoles,
    getFreeLoginsAction,
    editGroupAction,
    showModal,
  })(PartnerCreator);

PartnerCreator.propTypes = {
  groups: PropTypes.array,
  roles: PropTypes.array,
  logins: PropTypes.array,
  partner_guid: PropTypes.string,
  currentPagePartner: PropTypes.number,
  pageSizePartner: PropTypes.number,

  addPartnerAction: PropTypes.func,
  editGroupAction: PropTypes.func,
  addPartnerLoginAction: PropTypes.func,
  getAllGroups: PropTypes.func,
  getAllRoles: PropTypes.func,
  getFreeLoginsAction: PropTypes.func,
  partnersSearch: PropTypes.object,
  showModal: PropTypes.func,
};