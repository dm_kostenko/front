import React, { Component } from "react";
import { connect } from "react-redux";
import { upsertChangedGroupAction } from "../../actions/partners";
import PropTypes from "prop-types";

class PartnerGroupsManagement extends Component {
  state = {
    partnerGroups: [],
    notPartnerGroups: [],
    filteredPartnerGroups: [],
    filteredNotPartnerGroups: [],
    changedGroups: [],
    query: ""
  };

  async componentDidMount() {
    const allGroups = this.props.groups.filter(group => group.partner_guid === null);
    const partnerGroups = this.props.partnerGroups;
    let notPartnerGroups;
    if (partnerGroups.length === 0) {
      this.setState({
        notPartnerGroups: allGroups,
        filteredNotPartnerGroups: allGroups
      });
    }
    else {
      let partnerGroupsGuids = [];
      for (let i = 0; i < partnerGroups.length; i += 1) {
        partnerGroupsGuids = [ ...partnerGroupsGuids, partnerGroups[i].guid ];
      }
      notPartnerGroups = allGroups.filter(group => !partnerGroupsGuids.includes(group.guid));
      this.setState({
        partnerGroups,
        notPartnerGroups,
        filteredPartnerGroups: partnerGroups,
        filteredNotPartnerGroups: []
      });
    }
  }

  handleDelete = async (e) => {
    const guid = e.target.id;
    const partnerGroups = this.state.partnerGroups.filter(group => group.guid !== guid);
    const group = this.state.partnerGroups.filter(group => group.guid === guid);
    const notPartnerGroups = [ group[0], ...this.state.notPartnerGroups ];
    await this.setState({
      notPartnerGroups,
      partnerGroups
    });
    this.filterGroups(this.state.query);
    const newDel = {
      guid,
      delete: true
    };
    this.setState({ changedGroups: [ ...this.state.changedGroups, newDel ] });
    await this.props.upsertChangedGroupAction(newDel);
  };

  handleAdd = async (e) => {
    const guid = e.target.id;
    const notPartnerGroups = this.state.notPartnerGroups.filter(group => group.guid !== guid);
    const group = this.state.notPartnerGroups.filter(group => group.guid === guid);
    const partnerGroups = [ ...this.state.partnerGroups, group[0] ];
    await this.setState({
      notPartnerGroups,
      partnerGroups
    });
    this.filterGroups(this.state.query);
    const newAdd = {
      guid
    };
    this.setState({ changedGroups: [ ...this.state.changedGroups, newAdd ] });
    await this.props.upsertChangedGroupAction(newAdd);
  };

  filterGroups = (query) => {
    const filteredPartnerGroups = this.state.partnerGroups.filter(group => {
      return group.name.toLowerCase().includes(query.toLowerCase());
    });
    const filteredNotPartnerGroups = this.state.notPartnerGroups.filter(group => {
      return group.name.toLowerCase().includes(query.toLowerCase());
    });
    if (!query && this.state.partnerGroups.length === 0) {
      this.setState({
        query,
        filteredPartnerGroups: this.state.partnerGroups,
        filteredNotPartnerGroups: this.state.notPartnerGroups
      });
    }
    else if (!query) {
      this.setState({
        query,
        filteredPartnerGroups: this.state.partnerGroups,
        filteredNotPartnerGroups: []
      });
    } else {
      this.setState({
        query,
        filteredPartnerGroups,
        filteredNotPartnerGroups
      });
    }
  }

  handleInputChange = (event) => {
    const query = event.target.value;
    this.filterGroups(query);
  }

  render() {
    const { filteredPartnerGroups, filteredNotPartnerGroups } = this.state;
    let row = [];
    filteredPartnerGroups.forEach((item, index) => {
      row = [ ...row,
        <li className="list-group-item" style={{ textAlign: "right", backgroundColor: "#87cefa" }} key={index}>
          {item.name + " "}
          <i
            className="far fa-trash-alt"
            style={{ cursor: "pointer", color: "red", marginLeft: "10px" }}
            onClick={this.handleDelete}
            key
          />
        </li> ];
    });
    let newRow = [];
    filteredNotPartnerGroups.forEach((item, index)  => {
      newRow = [ ...newRow,
        <li className="list-group-item" style={{ textAlign: "right" }} key={index}>
          {item.name + " "}
          <i
            id={item.guid}
            className="fas fa-plus"
            style={{ cursor: "pointer", color: "#1e90ff", marginLeft: "10px" }}
            onClick={this.handleAdd}
          />
        </li> ];
    });
    row = [ ...row, newRow ];
    return (
      <div>
        <div className="search-box">
          <form className="form-inline md-form form-sm mt-0">
            <input
              className="form-control form-control-sm ml-3 w-75"
              type="text"
              placeholder="Search"
              aria-label="Search"
              value={this.state.query}
              onChange={e => this.handleInputChange(e)}
            />{"    "}
            <i className="fas fa-search" aria-hidden="true"></i>
          </form>
        </div>
        <ul className="list-group">{row}</ul>
        {(filteredPartnerGroups.length === 0 && filteredNotPartnerGroups === 0) ?
          <div>
            <n />
            <label htmlFor="">There are no available groups</label>
          </div>
          : null
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    partner: state.partners.partner,
    partnerGroups: state.partners.partnerGroups,
    groups: state.groups.groupsList
  };
};

export default connect(mapStateToProps, {
  upsertChangedGroupAction
})(PartnerGroupsManagement);

PartnerGroupsManagement.propTypes = {
  partner: PropTypes.array,
  partnerGroups: PropTypes.array,
  groups: PropTypes.array,
  partner_guid: PropTypes.string,

  getAllGroups: PropTypes.func,
  getPartnerAction: PropTypes.func,
  upsertChangedGroupAction: PropTypes.func,
  getPartnerGroupsAction: PropTypes.func,
};