import React from "react";
import {
  ControlLabel,
  FormControl,
  Button,
  FormGroup,
  Badge
} from "react-bootstrap";

export default class ChargeCreator extends React.Component {
  state = {
    open: false
  }

  handleClick = () => {
    this.setState({
      open: !this.state.open
    });
  };

  render() {
    return (
      <>
        <h4 style={{ textAlign: "center" }}>
          Charges:{" "} {
            this.state.open ?
              <a onClick={this.handleClick}><i className="fas fa-minus" style={{ cursor: "pointer", color: "red" }} /></a>
              :
              <a onClick={this.handleClick}><i className="fas fa-plus" style={{ cursor: "pointer", color: "green" }} /></a>}
        </h4>
        {this.state.open ?
          <>
            <div>
              <div>
                <Badge className="badge-rate">
                  <h6>One Time Charges</h6>
                </Badge>{" "}
              </div>
              <div className="charge-form">
                <ControlLabel>Charge Name * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="oneTimeChargeName"
                    id="oneTimeChargeName"
                    value={this.props.oneTimeChargeName}
                    onChange={this.props.handleChargeChange}
                    type="text"
                  />
                </FormGroup>

                <ControlLabel>Sum (flat rate) * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="oneTimeChargeSum"
                    id="oneTimeChargeSum"
                    value={this.props.oneTimeChargeSum}
                    onChange={this.props.handleChargeChange}
                    type="number"
                  />
                </FormGroup>
              </div>
            </div>

            <div>
              <div style={{ marginTop: "15px" }}>
                <Badge className="badge-rate">
                  <h6>Reccuring Periodic Charges</h6>
                </Badge>{" "}
              </div>

              <div className="charge-form">
                <ControlLabel>Charge Name * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="periodicChargeName"
                    id="periodicChargeName"
                    value={this.props.periodicChargeName}
                    onChange={this.props.handleChargeChange}
                    type="text"
                  />
                </FormGroup>

                <ControlLabel>Sum (flat rate) * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="periodicChargeSum"
                    id="periodicChargeSum"
                    value={this.props.periodicChargeSum}
                    onChange={this.props.handleChargeChange}
                    type="number"
                  />
                </FormGroup>

                <ControlLabel>Percent * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="periodicChargePercent"
                    id="periodicChargePercent"
                    value={this.props.periodicChargePercent}
                    onChange={this.props.handleChargeChange}
                    type="number"
                  />
                </FormGroup>

                <ControlLabel>Period * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="periodicChargePeriod"
                    id="periodicChargePeriod"
                    onChange={this.props.handleChargeChange}
                    componentClass="select"
                  >
                    <option value="" />
                    {this.props.periods.map(period => {
                      return (
                        <option key={period.id} value={period.name}>
                          {period.name}
                        </option>
                      );
                    })}
                  </FormControl>
                </FormGroup>

              </div>
            </div>

            <div>
              <div style={{ marginTop: "15px" }}>
                <Badge className="badge-rate">
                  <h6>Reccuring Conditional Charges</h6>
                </Badge>{" "}
              </div>

              <div className="charge-form">
                <ControlLabel>Charge Name * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="conditionalChargeName"
                    id="conditionalChargeName"
                    value={this.props.conditionalChargeName}
                    onChange={this.props.handleChargeChange}
                    type="text"
                  />
                </FormGroup>

                <ControlLabel>Sum (flat rate) * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="conditionalChargeSum"
                    id="conditionalChargeSum"
                    value={this.props.conditionalChargeSum}
                    onChange={this.props.handleChargeChange}
                    type="number"
                  />
                </FormGroup>

                <ControlLabel>Percent * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="conditionalChargePercent"
                    id="conditionalChargePercent"
                    value={this.props.conditionalChargePercent}
                    onChange={this.props.handleChargeChange}
                    type="number"
                  />
                </FormGroup>

                <ControlLabel>Condition * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="conditionalChargeCondition"
                    id="conditionalChargeCondition"
                    onChange={this.props.handleChargeChange}
                    componentClass="select"
                  >
                    <option value="" />
                    {this.props.conditions.map(condition => {
                      return (
                        <option key={condition.id} value={condition.name}>
                          {condition.name}
                        </option>
                      );
                    })}
                  </FormControl>
                </FormGroup>

                <ControlLabel>Condition value * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="conditionalChargeConditionValue"
                    id="conditionalChargeConditionValue"
                    value={this.props.conditionalChargeConditionValue}
                    onChange={this.props.handleChargeChange}
                  />
                </FormGroup>

              </div>
            </div>
          </>
          : null}
      </>
    );
  };
}
