import React from "react";
import { connect } from "react-redux";
import AbstractComponentRate from "./Factory";
import RateCreator from "./Creator";
import ability from "config/ability";
import { getAllRates, deleteRateAction } from "../../actions/rates";
import { searchInRates, reset } from "../../actions/search";
import RateEditor from "./Editor";
import Modal from "components/UI/Modal";
import moment from "moment";
import { Link } from "react-router-dom";


const columns = [
  { path: "apply_from", label: "From", content: rate => (moment(rate.apply_from).utcOffset(0).format("DD.MM.YYYY HH.mm")) },
  { path: "apply_to", label: "To", content: rate => (moment(rate.apply_to).utcOffset(0).format("DD.MM.YYYY HH.mm")) },
  { path: "currency_name", label: "Currency" },
  { path: "gateway_name", label: "Gateway" },
  { path: "shop_name", label: "Shop" },
  { path: "hold_rate", label: "Hold Rate" },
  { path: "hold_period", label: "Hold Period" },
  { 
    label: "View",
    key: "view",
    content: rate => (

      <Link className="link" to={`/rates/shop/${rate.shop_guid}/gateway/${rate.gateway_guid}/currency/${rate.currency_guid}`} ><i className="far fa-eye"></i></Link>
    ), },
  ability.can("EXECUTE", "RATES") &&
        {
          key: "edit",
          content: rate => (
            <Modal 
              header="Edit rate" 
              button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }}/>} 
              content={<RateEditor shop_guid={rate.shop_guid} gateway_guid={rate.gateway_guid} currency_guid={rate.currency_guid} type="rate"/>} />
          ),
          label: "Edit"
        },
  ability.can("EXECUTE", "RATES") &&
        {
          key: "delete",
          content: () => (
            <i
              className="far fa-trash-alt"
              style={{ cursor: "pointer", color: "red" }}
            />
          ),
          label: "Delete"
        }    
];

const mapStateToProps = (state) => {
  return {
    data: state.rates.ratesList,
    count: state.rates.count,
    loading: state.rates.ratesLoading,
    searchData: state.search.ratesSearch,
    isSearch: state.search.isSearch,
    name:"rates",
    columns,
    modal: <RateCreator/>
  };
};

export default connect(mapStateToProps, {
  get: getAllRates,
  delete: deleteRateAction,
  search: searchInRates,
  reset
})(AbstractComponentRate);


