import React, { Component } from "react";
import { Col, Row, Badge, Button } from "react-bootstrap";
import moment from "moment";
import { connect } from "react-redux";
import { 
  getRateAction, 
  getPaymentStatusRateAction, 
  getOneTimeChargesAction, 
  getPeriodicChargesAction,
  getConditionalChargesAction } from "actions/rates";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";

class RateDetail extends Component {
  
  async componentDidMount () {
    await this.props.getRateAction(this.props.match.params.shop_id, this.props.match.params.gateway_id, this.props.match.params.currency_id);
    await this.props.getPaymentStatusRateAction(this.props.match.params.shop_id, this.props.match.params.gateway_id, this.props.match.params.currency_id);
    await this.props.getOneTimeChargesAction(this.props.match.params.shop_id, this.props.match.params.gateway_id, this.props.match.params.currency_id)
    await this.props.getPeriodicChargesAction(this.props.match.params.shop_id, this.props.match.params.gateway_id, this.props.match.params.currency_id)
    await this.props.getConditionalChargesAction(this.props.match.params.shop_id, this.props.match.params.gateway_id, this.props.match.params.currency_id)
  }

  renderPaymentStatusRates = () => {
    let array = [];
    const ratePaymentStatus = this.props.ratePaymentStatus
      ? this.props.ratePaymentStatus
      : null;
    for (let i = 0; i < ratePaymentStatus.length; i++) {
      let item = {
        name: ratePaymentStatus[i].transaction_name,
        type: ratePaymentStatus[i].type,
        success: ratePaymentStatus[i].success,
        failure: ratePaymentStatus[i].failure
      };
      array = [ ...array, item ];
    }
    let buyArray = array.filter(item => item.type==="buy");
    let sellArray = array.filter(item => item.type==="sell");
    return (
      <React.Fragment>
        <div>
          <Badge className="badge-rate" style={{marginBottom:"15px"}}><h6>Sell</h6></Badge>
        </div>
        {
          sellArray.map(item => (
            
            <>
              {(item.failure) ? 
              <>
                <Row>
                  <label className="col-md-5 right">{`${item.name} success` }: </label>
                  <Col md={5}>
                    <span>{item.success}</span>
                  </Col>
                </Row>
                <Row>
                  <label className="col-md-5 right">{`${item.name} failure` }: </label>
                  <Col md={5}>
                    <span>{item.failure}</span>
                  </Col>
                </Row>
              </>
                :
                <Row>
                  <label className="col-md-5 right">{`${item.name}` }: </label>
                  <Col md={5}>
                    <span>{item.success}</span>
                  </Col>
                </Row>
              }
            </>
          ))}
        <div>
          <Badge className="badge-rate" style={{marginBottom:"15px"}}><h6>Buy</h6></Badge>
        </div>
        {buyArray.map(item => (
            <>
              {(item.failure) ?
              <>
                <Row>
                  <label className="col-md-5 right">{`${item.name} success` }: </label>
                  <Col md={5}>
                    <span>{item.success}</span>
                  </Col>
                </Row>
                <Row>
                  <label className="col-md-5 right">{`${item.name} failure` }: </label>
                  <Col md={5}>
                    <span>{item.failure}</span>
                  </Col>
                </Row>
              </>
                :
                <Row>
                  <label className="col-md-5 right">{`${item.name}` }: </label>
                  <Col md={5}>
                    <span>{item.success}</span>
                  </Col>
                </Row>
              }
            </>
        ))}
      </React.Fragment>
    );
  };


  render() {
    const rate = this.props.rate;
    const oneTimeCharges = this.props.oneTimeCharges;
    const conditionalCharges = this.props.conditionalCharges;
    const periodicCharges = this.props.periodicCharges;
    const paymentStatusRates = this.renderPaymentStatusRates();

    if (this.props.rateLoading || this.props.conditionalLoading || this.props.periodicLoading || this.props.oneTimeLoading || this.props.ratePaymentLoading) return <Spinner/>;
    else return (
      <>
      <Button onClick={this.props.history.goBack} className="detail-back-button"><i className="fas fa-arrow-left"></i></Button>
      <div className="content" style={{ overflow:"auto" }}>
        <div className="row"  style={{ marginLeft:"0px" }}>
          <div className="card-container">
            <div className="card">
              <div className="header">General Info:</div>
              <div className="content">
                <Row>
                  <Col md={7} lg={7} xs={7}>
                    <span>{this.props.match.params.id}</span>
                  </Col>
                </Row>

                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Currency: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{rate.currency_name}</span>
                  </Col>
                </Row>

                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Gateway: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{rate.gateway_name}</span>
                  </Col>
                </Row>

                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Shop: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{rate.shop_name}</span>
                  </Col>
                </Row>

                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Hold Rate: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{rate.hold_rate}</span>
                  </Col>
                </Row>

                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Hold Period: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{rate.hold_period}</span>
                  </Col>
                </Row>

                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Apply From: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{rate.apply_from!==null ? moment(rate.apply_from).utcOffset(0).format("YYYY-MM-DD") : ""}</span>
                  </Col>
                </Row>

                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Apply To: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{rate.apply_to!==null ? moment(rate.apply_to).utcOffset(0).format("YYYY-MM-DD") : ""}</span>
                  </Col>
                </Row>

                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Created at: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{rate.created_at!==null ? moment(rate.created_at).utcOffset(0).format("YYYY-MM-DD hh-mm") : ""}</span>
                  </Col>
                </Row>

                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Created by: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{rate.created_by}</span>
                  </Col>
                </Row>

                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Updated at: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{rate.updated_at ? moment(rate.updated_at).utcOffset(0).format("YYYY-MM-DD hh-mm") : ""}</span>
                  </Col>
                </Row>

                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Updated by: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    {rate.updated_by}
                  </Col>
                </Row>
              </div>
            </div>

            <div className="card">
              <div className="header">Payment Processing Status Rates:</div>
              <div className="content">

                {paymentStatusRates}
              </div>
            </div>
            <div className="card" style={{ overflow: "auto" }}>
            <div className="header">Charges:</div>
            <div className="content">
              <div>
                <Badge className="badge-rate">
                  <h6>One Time  Charges</h6>
                </Badge>
              </div>
              <div className="content">
                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Name: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{oneTimeCharges.name}</span>
                  </Col>
                </Row>
                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Sum (flat rate): </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{oneTimeCharges.flat_rate}</span>
                  </Col>
                </Row>
              </div>

              <div>
                <Badge className="badge-rate">
                  <h6>Periodic Charges</h6>
                </Badge>{" "}
              </div>
              <div className="content">
                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Name: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{periodicCharges.name}</span>
                  </Col>
                </Row>
                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Sum (flat rate): </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{periodicCharges.flat_rate}</span>
                  </Col>
                </Row>
                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Percent: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{periodicCharges.percent}</span>
                  </Col>
                </Row>
                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Period: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{periodicCharges.period}</span>
                  </Col>
                </Row>
              </div>

              <div>
                <Badge className="badge-rate">
                  <h6>Conditional Charges</h6>
                </Badge>{" "}
              </div>
              <div className="content">
                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Name: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{conditionalCharges.name}</span>
                  </Col>
                </Row>
                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Sum (flat rate): </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{conditionalCharges.flat_rate}</span>
                  </Col>
                </Row>
                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Percent: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{conditionalCharges.percent}</span>
                  </Col>
                </Row>
                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Condition: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{conditionalCharges.condition_name}</span>
                  </Col>
                </Row>
                <Row>
                  <Col className="right" md={5} lg={5} xs={5}>
                    <label>Condition value: </label>
                  </Col>
                  <Col md={7} lg={7} xs={7}>
                    <span>{conditionalCharges.condition_value}</span>
                  </Col>
                </Row>
              </div>
              </div>
              </div>
          </div>
        </div>
      </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    rate: state.rates.rate,
    oneTimeCharges: state.rates.oneTimeCharges,
    conditionalCharges: state.rates.conditionalCharges,
    periodicCharges: state.rates.periodicCharges,
    ratePaymentStatus: state.rates.ratePaymentStatus,
    rateLoading: state.rates.rateLoading,
    ratePaymentLoading: state.rates.ratePaymentLoading,
    oneTimeLoading: state.rates.oneTimeLoading,
    periodicLoading: state.rates.periodicLoading,
    conditionalLoading: state.rates.conditionalLoading
  };
};

export default connect(mapStateToProps, { 
  getRateAction, 
  getPaymentStatusRateAction,
  getConditionalChargesAction,
  getPeriodicChargesAction,
  getOneTimeChargesAction
})(RateDetail);

RateDetail.propTypes = {
  rate:  PropTypes.object,
  rateLoading: PropTypes.bool,
  ratePaymentStatus: PropTypes.object,
  oneTimeLoading: PropTypes.bool,
  periodicLoading: PropTypes.bool,
  conditionalLoading: PropTypes.bool,
  getRateAction: PropTypes.func,
  getPaymentStatusRateAction: PropTypes.func,
  
  match: PropTypes.shape({
    params: PropTypes.shape({
      shop_id: PropTypes.string.isRequired,
      gateway_id: PropTypes.string,
      currency_id: PropTypes.string,
      id: PropTypes.string,
    })
  })
};