import React, { Component } from "react";
import { connect } from "react-redux";
import {
  addRateAction,
  addOneTimeChargeAction, 
  addPeriodicChargeAction, 
  addConditionalChargeAction,
  getRateAction,
  getPaymentStatusRateAction,
  getConditionalChargesAction,
  getPeriodicChargesAction,
  getOneTimeChargesAction
} from "actions/rates";
import { getAllShops, getShopGatewaysAction } from "actions/shops";
import { getAllCurrencies } from "actions/currencies";
import { getTransactionsForGatewayAction } from "actions/transactions";
import { showModal } from "actions/modal";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";
import Spinner from "components/UI/Spinner";
import {
  Col,
  Form,
  ControlLabel,
  FormControl,
  Button,
  FormGroup,
  Badge
} from "react-bootstrap";
import moment from "moment";

class RateEditor extends Component {
  state = {
    shop: "",
    shopGuid: "",
    holdRate: "",
    holdPeriod: "",
    applyFrom: "",
    applyTo: "",
    types: [],
    currency: "",
    gateway: "",
    open: false,
    periods: [
      { id: 1, name: "Daily" },
      { id: 2, name: "Weekly" },
      { id: 3, name: "Monthly" },
      { id: 4, name: "Annually" },
      { id: 5, name: "Every Payout" }
    ],
    conditions: [{ id: 1, name: "Sum" }, { id: 2, name: "Transactions" }],
    gateways: [],
    currencies: [],
    transactionsForGateway: [],
    //charges
    oneTimeChargeName: "",
    oneTimeChargeSum: "",
    periodicChargeName: "",
    periodicChargeSum: "",
    periodicChargePercent: "",
    periodicChargePeriod: "",
    conditionalChargeName: "",
    conditionalChargeSum: "",
    conditionalChargePercent: "",
    conditionalChargeCondition: "",
    conditionalChargeConditionValue: ""
  };

  async componentDidMount() {
    await this.props.getAllShops();
    await this.props.getAllCurrencies();
    await this.props.getRateAction(
      this.props.shop_guid,
      this.props.gateway_guid,
      this.props.currency_guid
    );
    await this.props.getPaymentStatusRateAction(
      this.props.shop_guid,
      this.props.gateway_guid,
      this.props.currency_guid
    );
    await this.props.getOneTimeChargesAction(
      this.props.shop_guid,
      this.props.gateway_guid,
      this.props.currency_guid
    );
    await this.props.getPeriodicChargesAction(
      this.props.shop_guid,
      this.props.gateway_guid,
      this.props.currency_guid
    );
    await this.props.getConditionalChargesAction(
      this.props.shop_guid,
      this.props.gateway_guid,
      this.props.currency_guid
    );
    if (
      Object.keys(this.props.periodicCharges).length !== 0 ||
      Object.keys(this.props.conditionalCharges).length !== 0 ||
      Object.keys(this.props.oneTimeCharges).length !== 0
    ) {
      this.handleClick();
    }
    if (Object.keys(this.props.periodicCharges).length !== 0) 
      this.setState({
        periodicChargeName: this.props.periodicCharges.name,
        periodicChargeSum: this.props.periodicCharges.flat_rate,
        periodicChargePercent: this.props.periodicCharges.percent,
        periodicChargePeriod: this.props.periodicCharges.period
      });
    if (Object.keys(this.props.conditionalCharges).length !== 0) 
      this.setState({
        conditionalChargeName: this.props.conditionalCharges.name,
        conditionalChargeSum: this.props.conditionalCharges.flat_rate,
        conditionalChargePercent: this.props.conditionalCharges.percent,
        conditionalChargeCondition: this.props.conditionalCharges.condition_name,
        conditionalChargeConditionValue: this.props.conditionalCharges.condition_value
      });
    if (Object.keys(this.props.oneTimeCharges).length) 
      this.setState({
        oneTimeChargeName: this.props.oneTimeCharges.name,
        oneTimeChargeSum: this.props.oneTimeCharges.flat_rate
      });
    let types = [];
    let objItem = {};
    let typeIndex = -1;
    this.props.ratePaymentStatus.map(item => {
      typeIndex = types.findIndex(
        type => type.transaction_guid === item.transaction_guid
      );
      if (typeIndex === -1) {
        objItem = {
          transaction_guid: item.transaction_guid,
          [item.type]: {
            success: item.success,
            failure: item.failure
          }
        };
        types.push(objItem);
      } else
        types[typeIndex] = {
          ...types[typeIndex],
          [item.type]: {
            success: item.success,
            failure: item.failure
          }
        };
    });
    this.setState({
      currencies: this.props.currencies,
      currency: this.props.rate.currency_guid,
      gateway: this.props.rate.gateway_guid,
      shop: this.props.rate.shop_guid,
      applyFrom: moment(this.props.rate.apply_from)
        .utcOffset(0)
        .format("YYYY-MM-DD"),
      applyTo: moment(this.props.rate.apply_to)
        .utcOffset(0)
        .format("YYYY-MM-DD"),
      holdRate: this.props.rate.hold_rate,
      holdPeriod: this.props.rate.hold_period,
      types
    });
  }

  handleChange = e => {
    const property = e.target.name;
    const value = e.target.value;
    this.setState({
      [property]: value
    });
  };

  handleCurrencySelect = e => {
    this.setState({
      currency: e.target.value
    });
  };

  handleGatewaySelect = async e => {
    await this.setState({
      gateway: e.target.value
    });
    await this.recieveTypesTransactions();
    await this.renderRates();
  };

  handleShopSelect = async e => {
    await this.setState({
      shop: e.target.value
    });
    await this.recieveGateways();
  };

  handleTypeChange = e => {
    const guid = e.target.id.split(" ")[0];
    const buyout = e.target.id.split(" ")[1];
    const status = e.target.id.split(" ")[2];
    let types = this.state.types;

    let f = 0;
    types = types.map(type => {
      if (type.transaction_guid === guid) {
        f = 1;
        return {
          ...type,
          [buyout]: {
            ...type[buyout],
            [status]: e.target.value
          }
        };
      } else return type;
    });
    if (!f)
      types.push({
        transaction_guid: guid,
        [buyout]: {
          [status]: e.target.value
        }
      });
    this.setState({ types });
  };

  recieveTypesTransactions = async () => {
    await this.props.getTransactionsForGatewayAction(this.state.gateway);
    this.setState({
      transactionsForGateway: this.props.transactionsForGateway
    });
  };

  recieveGateways = async () => {
    await this.props.getShopGatewaysAction(this.state.shop);
    this.setState({
      gateways: this.props.shopGateways,
      gateway: this.props.shopGateways[0].guid
    });

    await this.recieveTypesTransactions();
    await this.renderRates();
  };

  renderRates = () => {
    let buyes = this.props.ratePaymentStatus.filter(
      item => item.type === "buy"
    );
    let sells = this.props.ratePaymentStatus.filter(
      item => item.type === "sell"
    );
    return (
      <React.Fragment>
        <h4 style={{ textAlign: "center" }}>
          Payment Processing Status Rates:{" "}
        </h4>
        <div>
          <div>
            <Badge className="badge-rate">
              <h6>Sell</h6>
            </Badge>
          </div>
        </div>
        {sells.map((input, index) => (
          <>
            {input.failure ? (
              <React.Fragment>
                <Col md={6}>
                  <ControlLabel>{`${
                    input.transaction_name
                  } Success`}</ControlLabel>
                  <FormGroup
                    validationState={this.formValidation(
                      `${input.transaction_name}_success`
                    )}
                  >
                    <FormControl
                      name={`${input.transaction_name}_success`}
                      type="number"
                      id={`${input.transaction_guid} sell success`}
                      onChange={this.handleTypeChange}
                      value={
                        this.state.types[index]
                          ? this.state.types[index][input.type]["success"]
                          : null
                      }
                    />
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <ControlLabel>{`${
                    input.transaction_name
                  } Failure`}</ControlLabel>
                  <FormGroup
                    validationState={this.formValidation(
                      `${input.transaction_name}_failure`
                    )}
                  >
                    <FormControl
                      name={`${input.transaction_name}_failure`}
                      type="number"
                      id={`${input.transaction_guid} sell failure`}
                      onChange={this.handleTypeChange}
                      value={
                        this.state.types[index]
                          ? this.state.types[index][input.type]["failure"]
                          : null
                      }
                    />
                  </FormGroup>
                </Col>
              </React.Fragment>
            ) : (
              <>
                <ControlLabel>{`${input.transaction_name}`}</ControlLabel>
                <FormGroup
                  validationState={this.formValidation(input.transaction_name)}
                >
                  <FormControl
                    name={input.transaction_name}
                    type="number"
                    id={`${input.transaction_guid} sell success`}
                    onChange={this.handleTypeChange}
                    value={
                      this.state.types[index]
                        ? this.state.types[index][input.type]["success"]
                        : null
                    }
                  />
                </FormGroup>
              </>
            )}
          </>
        ))}
        <div>
          <div>
            <Badge className="badge-rate">
              <h6>Buy</h6>
            </Badge>
          </div>
        </div>
        {buyes.map((input, index) => (
          <>
            {input.failure ? (
              <React.Fragment>
                <Col md={6}>
                  <ControlLabel>{`${
                    input.transaction_name
                  } Success`}</ControlLabel>
                  <FormGroup
                    validationState={this.formValidation(
                      `${input.transaction_name}_success`
                    )}
                  >
                    <FormControl
                      name={`${input.transaction_name}_success`}
                      type="number"
                      id={`${input.transaction_guid} buy success`}
                      onChange={this.handleTypeChange}
                      value={
                        this.state.types[index]
                          ? this.state.types[index][input.type]["success"]
                          : null
                      }
                    />
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <ControlLabel>{`${
                    input.transaction_name
                  } Failure`}</ControlLabel>
                  <FormGroup
                    validationState={this.formValidation(
                      `${input.transaction_name}_failure`
                    )}
                  >
                    <FormControl
                      name={`${input.transaction_name}_failure`}
                      type="number"
                      id={`${input.transaction_guid} buy failure`}
                      onChange={this.handleTypeChange}
                      value={
                        this.state.types[index]
                          ? this.state.types[index][input.type]["failure"]
                          : null
                      }
                    />
                  </FormGroup>
                </Col>
              </React.Fragment>
            ) : (
              <>
                <ControlLabel>{`${input.transaction_name}`}</ControlLabel>
                <FormGroup
                  validationState={this.formValidation(input.transaction_name)}
                >
                  <FormControl
                    name={input.transaction_name}
                    type="number"
                    id={`${input.transaction_guid} buy success`}
                    onChange={this.handleTypeChange}
                    value={
                      this.state.types[index]
                        ? this.state.types[index][input.type]["success"]
                        : null
                    }
                  />
                </FormGroup>
              </>
            )}
          </>
        ))}
      </React.Fragment>
    );
  };

  handleClick = () => {
    this.setState({
      open: !this.state.open
    });
  };

  renderCharges = () => {
    return (
      <>
        <h4 style={{ textAlign: "center" }}>
          Charges:{" "}
          {this.state.open ? (
            <a onClick={this.handleClick}>
              <i
                className="fas fa-minus"
                style={{ cursor: "pointer", color: "red" }}
              />
            </a>
          ) : (
            <a onClick={this.handleClick}>
              <i
                className="fas fa-plus"
                style={{ cursor: "pointer", color: "green" }}
              />
            </a>
          )}
        </h4>
        {this.state.open ? (
          <>
            <div>
              <div>
                <Badge className="badge-rate">
                  <h6>One Time Charges</h6>
                </Badge>{" "}
              </div>
              <div className="charge-form">
                <ControlLabel>Charge Name * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="oneTimeChargeName"
                    id="oneTimeChargeName"
                    value={this.state.oneTimeChargeName}
                    onChange={this.handleChange}
                    type="text"
                  />
                </FormGroup>

                <ControlLabel>Sum (flat rate) * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="oneTimeChargeSum"
                    id="oneTimeChargeSum"
                    value={this.state.oneTimeChargeSum}
                    onChange={this.handleChange}
                    type="text"
                  />
                </FormGroup>
              </div>
            </div>

            <div>
              <div style={{ marginTop: "15px" }}>
                <Badge className="badge-rate">
                  <h6>Reccuring Periodic Charges</h6>
                </Badge>{" "}
              </div>

              <div className="charge-form">
                <ControlLabel>Charge Name * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="periodicChargeName"
                    id="periodicChargeName"
                    value={this.state.periodicChargeName}
                    onChange={this.handleChange}
                    type="text"
                  />
                </FormGroup>

                <ControlLabel>Sum (flat rate) * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="periodicChargeSum"
                    id="periodicChargeSum"
                    value={this.state.periodicChargeSum}
                    onChange={this.handleChange}
                    type="text"
                  />
                </FormGroup>

                <ControlLabel>Percent * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="periodicChargePercent"
                    id="periodicChargePercent"
                    value={this.state.periodicChargePercent}
                    onChange={this.handleChange}
                    type="text"
                  />
                </FormGroup>

                <ControlLabel>Period * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="periodicChargePeriod"
                    id="periodicChargePeriod"
                    onChange={this.handleChange}
                    componentClass="select"
                  >
                    <option value={this.state.periodicChargePeriod}>
                      {this.state.periodicChargePeriod}
                    </option>
                    {this.state.periods
                      .filter(
                        period =>
                          period.name !== this.state.periodicChargePeriod
                      )
                      .map(period => {
                        return (
                          <option key={period.id} value={period.name}>
                            {period.name}
                          </option>
                        );
                      })}
                  </FormControl>
                </FormGroup>
              </div>
            </div>

            <div>
              <div style={{ marginTop: "15px" }}>
                <Badge className="badge-rate">
                  <h6>Reccuring Conditional Charges</h6>
                </Badge>{" "}
              </div>

              <div className="charge-form">
                <ControlLabel>Charge Name * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="conditionalChargeName"
                    id="conditionalChargeName"
                    value={this.state.conditionalChargeName}
                    onChange={this.handleChange}
                    type="text"
                  />
                </FormGroup>

                <ControlLabel>Sum (flat rate) * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="conditionalChargeSum"
                    id="conditionalChargeSum"
                    value={this.state.conditionalChargeSum}
                    onChange={this.handleChange}
                    type="text"
                  />
                </FormGroup>

                <ControlLabel>Percent * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="conditionalChargePercent"
                    id="conditionalChargePercent"
                    value={this.state.conditionalChargePercent}
                    onChange={this.handleChange}
                    type="text"
                  />
                </FormGroup>

                <ControlLabel>Condition * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="conditionalChargeCondition"
                    id="conditionalChargeCondition"
                    onChange={this.handleChange}
                    componentClass="select"
                  >
                    <option
                      value={this.state.conditionalChargeCondition}
                    >
                      {this.state.conditionalChargeCondition}
                    </option>
                    {this.state.conditions
                      .filter(
                        condition =>
                          condition.name !==
                          this.props.conditionalCharges.condition_name
                      )
                      .map(condition => {
                        return (
                          <option key={condition.id} value={condition.name}>
                            {condition.name}
                          </option>
                        );
                      })}
                  </FormControl>
                </FormGroup>

                <ControlLabel>Condition value * </ControlLabel>
                <FormGroup>
                  <FormControl
                    name="conditionalChargeConditionValue"
                    id="conditionalChargeConditionValue"
                    value={this.state.conditionalChargeConditionValue}
                    onChange={this.handleChange}
                  />
                </FormGroup>
              </div>
            </div>
          </>
        ) : null}
      </>
    );
  };

  formValidation = data => {
    if (data === "") return "error";
    else return "success";
  };

  timeValidation = () => {
    if (this.state.applyFrom > this.state.applyTo) return "error";
    else return "success";
  };

  transactionTypesRatesValidation = () => {
    const transactionsTypes = this.state.transactionsForGateway
      ? this.state.transactionsForGateway
      : [];
    let status = true;
    transactionsTypes.forEach(
      type => status && this.formValidation(type.name) === "success"
    );
    return status;
  };

  generalValidation = () => {
    return (
      this.formValidation(this.state.shop) === "success" &&
      this.formValidation(this.state.holdRate) === "success" &&
      this.formValidation(this.state.holdPeriod) === "success" &&
      this.formValidation(this.state.applyFrom) === "success" &&
      this.formValidation(this.state.applyTo) === "success" &&
      this.transactionTypesRatesValidation() &&
      this.formValidation(this.state.currency) === "success" &&
      this.formValidation(this.state.gateway) === "success"
    );
  };

  generalTimeValidation = () => {
    return (
      this.timeValidation(this.state.applyFrom) === "success" &&
      this.timeValidation(this.state.applyTo) === "success"
    );
  };

  chargeValidation = () => {
    if (
      (this.state.oneTimeChargeName === "" || this.state.oneTimeChargeSum === "") && (this.state.oneTimeChargeName !== "" || this.state.oneTimeChargeSum !== "") ||
      (
        (
          this.state.periodicChargeName === "" ||
          this.state.periodicChargeSum === "" ||
          this.state.periodicChargePercent === "" ||
          this.state.periodicChargePeriod === "") &&
        (this.state.periodicChargeName !== "" ||
          this.state.periodicChargeSum !== "" ||
          this.state.periodicChargePercent !== "" ||
          this.state.periodicChargePeriod !== "")
      ) || 
      (
        (this.state.conditionalChargeName === "" ||
          this.state.conditionalChargeSum === "" ||
          this.state.conditionalChargePercent === "" ||
          this.state.conditionalChargeCondition === "" ||
          this.state.conditionalChargeConditionValue === "") &&
        (this.state.conditionalChargeName !== "" ||
          this.state.conditionalChargeSum !== "" ||
          this.state.conditionalChargePercent !== "" ||
          this.state.conditionalChargeCondition !== "" ||
          this.state.conditionalChargeConditionValue !== ""))
    )
      return false;
    return true;
  }

  handleSubmit = async e => {
    e.preventDefault();
    this.setState({ isLoading: true });
    let data = {
      currency_guid: this.state.currency,
      gateway_guid: this.state.gateway,
      shop_guid: this.state.shop,
      apply_from: this.state.applyFrom,
      apply_to: this.state.applyTo,
      hold_rate: this.state.holdRate,
      hold_period: this.state.holdPeriod,
      transaction_rates: this.state.types
    };
    if (!this.generalValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field!",
        icon: "warning"
      });
    } else if (this.generalValidation() && !this.generalTimeValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "The date range is incorrect",
        icon: "warning"
      });
    } else if (!this.chargeValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, check charges!",
        icon: "warning"
      });
    } else {
      try {
        await this.props.addRateAction(
          data,
          this.props.currentPage,
          this.props.pageSize,
          this.props.ratesSearch
        );
        if (this.state.oneTimeChargeName !== "") 
          await this.props.addOneTimeChargeAction({
            currency_guid: this.state.currency,
            gateway_guid: this.state.gateway,
            shop_guid: this.state.shop,
            name: this.state.oneTimeChargeName,
            flat_rate: this.state.oneTimeChargeSum
          });
        if (this.state.periodicChargeName !== "") 
          await this.props.addPeriodicChargeAction({
            currency_guid: this.state.currency,
            gateway_guid: this.state.gateway,
            shop_guid: this.state.shop,
            name: this.state.periodicChargeName,
            flat_rate: this.state.periodicChargeSum,
            percent: this.state.periodicChargePercent,
            period: this.state.periodicChargePeriod
          });
        if (this.state.conditionalChargeName !== "") 
          await this.props.addConditionalChargeAction({
            currency_guid: this.state.currency,
            gateway_guid: this.state.gateway,
            shop_guid: this.state.shop,
            name: this.state.conditionalChargeName,
            flat_rate: this.state.conditionalChargeSum,
            percent: this.state.conditionalChargePercent,
            condition_name: this.state.conditionalChargeCondition,
            condition_value: this.state.conditionalChargeConditionValue
          });
        swal({
          title: "Rate is updated",
          icon: "success",
          button: false,
          timer: 2000
        });
        await this.props.showModal(false);
      } catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error"
        });
      }
    }
  };

  render() {
    const renderRatesStatus = this.renderRates();
    const renderChargesFields = this.renderCharges();
    if (
      this.props.shopsLoading ||
      this.props.shopGatewaysLoading ||
      this.props.rateLoading ||
      this.props.ratePaymentLoading ||
      this.props.currenciesLoading ||
      this.props.conditionalLoading ||
      this.props.periodicLoading ||
      this.props.oneTimeLoading
    )
      return <Spinner />;
    else
      return (
        <div>
          <div className="card">
            <div className="content">
              <label>Shop: {this.props.rate.shop_name}</label>
              <br />
              <label>Gateway: {this.props.rate.gateway_name}</label>
              <br />
              <label>Currency: {this.props.rate.currency_name}</label>
            </div>
          </div>

          <div className="card">
            <div className="content">
              <Form autoComplete="off" onSubmit={this.handleSubmit}>
                <Col className="col">
                  <ControlLabel>Hold Rate* </ControlLabel>
                  <FormGroup
                    validationState={this.formValidation(this.state.holdRate)}
                  >
                    <FormControl
                      name="holdRate"
                      type="text"
                      value={this.state.holdRate}
                      onChange={this.handleChange}
                    />
                  </FormGroup>
                </Col>

                <Col className="col">
                  <ControlLabel>Hold Period* </ControlLabel>
                  <FormGroup
                    validationState={this.formValidation(this.state.holdPeriod)}
                  >
                    <FormControl
                      name="holdPeriod"
                      type="text"
                      value={this.state.holdPeriod}
                      onChange={this.handleChange}
                    />
                  </FormGroup>
                </Col>

                <Col className="col">
                  <ControlLabel>Apply From* </ControlLabel>
                  <FormGroup
                    validationState={
                      this.formValidation(this.state.applyFrom) &&
                      this.timeValidation(this.state.applyFrom)
                    }
                  >
                    <FormControl
                      name="applyFrom"
                      type="date"
                      value={this.state.applyFrom}
                      onChange={e => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>

                <Col className="col">
                  <ControlLabel>Apply To* </ControlLabel>
                  <FormGroup
                    validationState={
                      this.formValidation(this.state.applyTo) &&
                      this.timeValidation(this.state.applyTo)
                    }
                  >
                    <FormControl
                      name="applyTo"
                      type="date"
                      value={this.state.applyTo}
                      onChange={e => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>

                {renderRatesStatus}
                {renderChargesFields}
                <div align="center">
                  {this.state.isLoading ? (
                    <ReactLoading type="cylon" color="grey" />
                  ) : (
                    <Button type="submit">Save</Button>
                  )}
                </div>
              </Form>
            </div>
          </div>
        </div>
      );
  }
}

const mapStateToProps = state => {
  return {
    shops: state.shops.shopsList,
    shopGateways: state.shops.shopGateways,
    currencies: state.currencies.currenciesList,
    ratePaymentStatus: state.rates.ratePaymentStatus,
    rateLoading: state.rates.rateLoading,
    ratePaymentLoading: state.rates.ratePaymentLoading,
    shopsLoading: state.shops.shopsLoading,
    shopGatewaysLoading: state.shops.shopGatewaysLoading,
    currenciesLoading: state.currencies.currenciesLoading,
    conditionalLoading: state.rates.conditionalLoading,
    periodicLoading: state.rates.periodicLoading,
    oneTimeLoading: state.rates.oneTimeLoading,
    currentPage: state.rates.currentPage,
    pageSize: state.rates.pageSize,
    rate: state.rates.rate,
    oneTimeCharges: state.rates.oneTimeCharges,
    conditionalCharges: state.rates.conditionalCharges,
    periodicCharges: state.rates.periodicCharges,
    ratesSearch: state.search.ratesSearch
  };
};

export default connect(
  mapStateToProps,
  {
    getAllShops,
    getShopGatewaysAction,
    getAllCurrencies,
    addRateAction,
    getTransactionsForGatewayAction,
    showModal,
    getRateAction,
    getConditionalChargesAction,
    getPeriodicChargesAction,
    getOneTimeChargesAction,
    getPaymentStatusRateAction,
    addOneTimeChargeAction, 
    addPeriodicChargeAction, 
    addConditionalChargeAction
  }
)(RateEditor);

RateEditor.propTypes = {
  gateways: PropTypes.array,
  currencies: PropTypes.array,
  shop_guid: PropTypes.string,
  gateway_guid: PropTypes.string,
  currency_guid: PropTypes.string,
  getShopGatewaysAction: PropTypes.func,
  getAllCurrencies: PropTypes.func,
  getAllShops: PropTypes.func,
  getRateAction: PropTypes.func,
  addRateAction: PropTypes.func,
  showModal: PropTypes.func,
  getPaymentStatusRateAction: PropTypes.func,
  rate: PropTypes.object,
  shopGateways: PropTypes.array,
  ratePaymentStatus: PropTypes.array,
  shopsLoading: PropTypes.bool,
  rateLoading: PropTypes.bool,
  currenciesLoading: PropTypes.bool,
  shopGatewaysLoading: PropTypes.bool,
  transactionsForGateway: PropTypes.array,
  getTransactionsForGatewayAction: PropTypes.array,
  currentPage: PropTypes.number,
  pageSize: PropTypes.number,
  ratesSearch: PropTypes.object,
  addOneTimeChargeAction: PropTypes.func, 
  addPeriodicChargeAction: PropTypes.func, 
  addConditionalChargeAction: PropTypes.func,
};
