import React, { Component } from "react";
import { connect } from "react-redux";
import { addRateAction, addOneTimeChargeAction, addPeriodicChargeAction, addConditionalChargeAction } from "../../actions/rates";
import { getAllShops, getShopGatewaysAction } from "../../actions/shops";
import { getAllCurrencies } from "../../actions/currencies";
import { getTransactionsForGatewayAction } from "actions/transactions";
import { showModal } from "../../actions/modal";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";
import Spinner from "components/UI/Spinner";
import {
  Col,
  Form,
  ControlLabel,
  FormControl,
  Button,
  FormGroup,
  Badge
} from "react-bootstrap";
import ChargeCreator from "./ChargeCreator";

class RateCreator extends Component {
  state = {
    shop: "",
    shops: [],
    shopGuid: "",
    holdRate: "",
    holdPeriod: "",
    applyFrom: "",
    applyTo: "",
    types: [],
    currency: "",
    gateway: "",
    gateways: [],
    currencies: [],
    transactionsForGateway: [],
    oneTimeChargeName: "",
    oneTimeChargeSum: "",
    periodicChargeName: "",
    periodicChargeSum: "",
    periodicChargePercent: "",
    periodicChargePeriod: "",
    conditionalChargeName: "",
    conditionalChargeSum: "",
    conditionalChargePercent: "",
    conditionalChargeCondition: "",
    conditionalChargeConditionValue: "",
    periods: [
      { id: 1, name: "Daily" },
      { id: 2, name: "Weekly" },
      { id: 3, name: "Monthly" },
      { id: 4, name: "Annually" },
      { id: 5, name: "Every Payout" }
    ],
    conditions: [
      { id: 1, name: "Sum" },
      { id: 2, name: "Transactions" }
    ]
  };

  async componentDidMount() {
    await this.props.getAllShops();
    await this.props.getAllCurrencies();
    this.setState({
      shops: this.props.shops,
      currencies: this.props.currencies
    });
  }

  handleChange = e => {
    const property = e.target.name;
    const value = e.target.value;
    this.setState({
      [property]: value
    });
  };

  validComboShopGatewayCurrency = () => {
    let filteredRates = this.props.rates.filter(rate => (rate.shop_guid === this.state.shop && rate.gateway_guid === this.state.gateway && rate.currency_guid === this.state.currency));
    return (filteredRates.length !== 0);
  }

  handleCurrencySelect = e => {
    this.setState({
      currency: e.target.value
    });
  };

  handleGatewaySelect = async e => {
    await this.setState({
      gateway: e.target.value
    });
    await this.recieveTypesTransactions();
    await this.renderRates();
  };

  handleShopSelect = async e => {
    await this.setState({
      shop: e.target.value
    });
    await this.recieveGateways();
  };

  handleTypeChange = e => {
    const guid = e.target.id.split(" ")[0];
    const buyout = e.target.id.split(" ")[1];
    const status = e.target.id.split(" ")[2];
    let types = this.state.types;

    let f = 0;
    types = types.map(type => {
      if (type.transaction_guid === guid) {
        f = 1;
        return {
          ...type,
          [buyout]: {
            ...type[buyout],
            [status]: e.target.value
          }
        };
      }
      else
        return type;
    });
    if (!f)
      types.push({
        transaction_guid: guid,
        [buyout]: {
          [status]: e.target.value
        }
      });
    this.setState({ types });
  }

  recieveTypesTransactions = async () => {
    await this.props.getTransactionsForGatewayAction(this.state.gateway);
    this.setState({
      transactionsForGateway: this.props.transactionsForGateway
    });
  };

  recieveGateways = async () => {
    await this.props.getShopGatewaysAction(this.state.shop);
    this.setState({
      gateways: this.props.shopGateways,
      gateway: this.props.shopGateways[0].guid
    });

    await this.recieveTypesTransactions();
    await this.renderRates();
  };

  renderRates = () => {
    if (this.state.gateway) {
      let array = [];
      const transactionsTypes = this.state.transactionsForGateway
        ? this.state.transactionsForGateway
        : null;
      for (let i = 0; i < transactionsTypes.length; i++) {
        let item = {
          guid: transactionsTypes[i].guid,
          name: transactionsTypes[i].name,
          failure: transactionsTypes[i].failure
        };
        array = [...array, item];
      }
      if (this.props.transactionsForGatewayLoading) return <Spinner />;
      else return (
        <React.Fragment>
          <h4 style={{ textAlign: "center" }}>
            Payment Processing Status Rates:{" "}
          </h4>
          <div>
            <Badge className="badge-rate"><h6>Sell</h6></Badge>
          </div>
          {array.map(input => (
            <>
              {input.failure ? (
                <React.Fragment>
                  <Col md={6}>
                    <ControlLabel>{`${input.name} Success`}</ControlLabel>
                    <FormGroup
                      validationState={this.formValidation(
                        `${input.name}_success`
                      )}
                    >
                      <FormControl
                        name={`${input.name}_success`}
                        id={`${input.guid} sell success`}
                        onChange={this.handleTypeChange}
                        type="number"
                      />
                    </FormGroup>
                  </Col>
                  <Col md={6}>
                    <ControlLabel>{`${input.name} Failure`}</ControlLabel>
                    <FormGroup
                      validationState={this.formValidation(
                        `${input.name}_failure`
                      )}
                    >
                      <FormControl
                        name={`${input.name}_failure`}
                        id={`${input.guid} sell failure`}
                        onChange={this.handleTypeChange}
                        type="number"
                      />
                    </FormGroup>
                  </Col>
                </React.Fragment>
              ) : (
                  <>
                    <ControlLabel>{`${input.name}`}</ControlLabel>
                    <FormGroup validationState={this.formValidation(input.name)}>
                      <FormControl
                        name={input.name}
                        id={`${input.guid} sell success`}
                        onChange={this.handleTypeChange}
                        type="number"
                      />
                    </FormGroup>
                  </>
                )}
            </>
          ))}
          <div>
            <Badge className="badge-rate"><h6>Buy</h6></Badge>
          </div>
          {array.map(input => (
            <>
              {input.failure ? (
                <React.Fragment>
                  <Col md={6}>
                    <ControlLabel>{`${input.name} Success`}</ControlLabel>
                    <FormGroup
                      validationState={this.formValidation(
                        `${input.name}_success`
                      )}
                    >
                      <FormControl
                        name={`${input.name}_success`}
                        id={`${input.guid} buy success`}
                        onChange={this.handleTypeChange}
                        type="number"
                      />
                    </FormGroup>
                  </Col>
                  <Col md={6}>
                    <ControlLabel>{`${input.name} Failure`}</ControlLabel>
                    <FormGroup
                      validationState={this.formValidation(
                        `${input.name}_failure`
                      )}
                    >
                      <FormControl
                        name={`${input.name}_failure`}
                        id={`${input.guid} buy failure`}
                        onChange={this.handleTypeChange}
                        type="number"
                      />
                    </FormGroup>
                  </Col>
                </React.Fragment>
              ) : (
                  <>
                    <ControlLabel>{`${input.name}`}</ControlLabel>
                    <FormGroup validationState={this.formValidation(input.name)}>
                      <FormControl
                        name={input.name}
                        id={`${input.guid} buy success`}
                        onChange={this.handleTypeChange}
                        type="number"
                      />
                    </FormGroup>
                  </>
                )}
            </>
          ))}
        </React.Fragment>
      );
    }
  };

  formValidation = data => {
    if (data === "") return "error";
    else return "success";
  };

  timeValidation = () => {
    if (this.state.applyFrom > this.state.applyTo) return "error";
    else return "success";
  };

  transactionTypesRatesValidation = () => {
    const transactionsTypes = this.state.transactionsForGateway
      ? this.state.transactionsForGateway
      : [];
    let status = true;
    transactionsTypes.forEach(
      type => status && this.formValidation(type.name) === "success"
    );
    return status;
  };

  generalValidation = () => {
    return (
      this.formValidation(this.state.shop) === "success" &&
      this.formValidation(this.state.holdRate) === "success" &&
      this.formValidation(this.state.holdPeriod) === "success" &&
      this.formValidation(this.state.applyFrom) === "success" &&
      this.formValidation(this.state.applyTo) === "success" &&
      this.transactionTypesRatesValidation() &&
      this.formValidation(this.state.currency) === "success" &&
      this.formValidation(this.state.gateway) === "success"
    );
  };

  generalTimeValidation = () => {
    return (
      this.timeValidation(this.state.applyFrom) === "success" &&
      this.timeValidation(this.state.applyTo) === "success"
    );
  };

  chargeValidation = () => {
    if (
      (this.state.oneTimeChargeName === "" || this.state.oneTimeChargeSum === "") && (this.state.oneTimeChargeName !== "" || this.state.oneTimeChargeSum !== "") ||
      (
        (
          this.state.periodicChargeName === "" ||
          this.state.periodicChargeSum === "" ||
          this.state.periodicChargePercent === "" ||
          this.state.periodicChargePeriod === "") &&
        (this.state.periodicChargeName !== "" ||
          this.state.periodicChargeSum !== "" ||
          this.state.periodicChargePercent !== "" ||
          this.state.periodicChargePeriod !== "")
      ) || 
      (
        (this.state.conditionalChargeName === "" ||
          this.state.conditionalChargeSum === "" ||
          this.state.conditionalChargePercent === "" ||
          this.state.conditionalChargeCondition === "" ||
          this.state.conditionalChargeConditionValue === "") &&
        (this.state.conditionalChargeName !== "" ||
          this.state.conditionalChargeSum !== "" ||
          this.state.conditionalChargePercent !== "" ||
          this.state.conditionalChargeCondition !== "" ||
          this.state.conditionalChargeConditionValue !== ""))
    )
      return false;
    return true;
  }

  handleSubmit = async e => {
    e.preventDefault();
    this.setState({ isLoading: true });
    let data = {
      currency_guid: this.state.currency,
      gateway_guid: this.state.gateway,
      shop_guid: this.state.shop,
      apply_from: this.state.applyFrom,
      apply_to: this.state.applyTo,
      hold_rate: this.state.holdRate,
      hold_period: this.state.holdPeriod,
      transaction_rates: this.state.types
    };
    if (!this.generalValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field!",
        icon: "warning"
      });
    } else if (this.generalValidation() && !this.generalTimeValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "The date range is incorrect",
        icon: "warning"
      });
    } else if (this.validComboShopGatewayCurrency()) {
      this.setState({ isLoading: false });
      swal({
        title: "Combination shop, gateway and currency is already exist!",
        icon: "warning"
      });
    }
    else if (!this.chargeValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, check charges!",
        icon: "warning"
      });
    } 
    else {
      try {
        await this.props.addRateAction(
          data,
          this.props.currentPage,
          this.props.pageSize,
          this.props.ratesSearch
        );
        if (this.state.oneTimeChargeName !== "") 
          await this.props.addOneTimeChargeAction({
            currency_guid: this.state.currency,
            gateway_guid: this.state.gateway,
            shop_guid: this.state.shop,
            name: this.state.oneTimeChargeName,
            flat_rate: this.state.oneTimeChargeSum
          });
        if (this.state.periodicChargeName !== "") 
          await this.props.addPeriodicChargeAction({
            currency_guid: this.state.currency,
            gateway_guid: this.state.gateway,
            shop_guid: this.state.shop,
            name: this.state.periodicChargeName,
            flat_rate: this.state.periodicChargeSum,
            percent: this.state.periodicChargePercent,
            period: this.state.periodicChargePeriod
          });
        if (this.state.conditionalChargeName !== "") 
          await this.props.addConditionalChargeAction({
            currency_guid: this.state.currency,
            gateway_guid: this.state.gateway,
            shop_guid: this.state.shop,
            name: this.state.conditionalChargeName,
            flat_rate: this.state.conditionalChargeSum,
            percent: this.state.conditionalChargePercent,
            condition_name: this.state.conditionalChargeCondition,
            condition_value: this.state.conditionalChargeConditionValue
          });
        swal({
          title: "Rate is created",
          icon: "success",
          button: false,
          timer: 2000
        });
        await this.props.showModal(false);
      } catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error"
        });
      }
    }
  };

  render() {
    const renderRatesStatus = this.renderRates();

    return (
      <div className="card">
        <div className="content">
          <Form autoComplete="off" onSubmit={this.handleSubmit}>
            <Col className="col">
              <ControlLabel>Shop* </ControlLabel>
              <FormGroup
                validationState={this.formValidation(this.state.shop)}
              >
                <FormControl
                  name="shopSelect"
                  componentClass="select"
                  onChange={e => this.handleShopSelect(e)}
                >
                  <option value=""></option>
                  {this.state.shops.map(shop => {
                    return (
                      <option key={shop.guid} value={shop.guid}>
                        {shop.name}
                      </option>
                    );
                  })}
                </FormControl>
              </FormGroup>
            </Col>

            <Col className="col">
              <ControlLabel>Gateway* </ControlLabel>
              <FormGroup
                validationState={this.formValidation(this.state.gateway)}
              >
                <FormControl
                  name="gatewaySelect"
                  componentClass="select"
                  onChange={e => this.handleGatewaySelect(e)}
                >
                  {/* <option value="">{this.state.gateways.name}</option> */}
                  {this.state.gateways.map(gateway => {
                    return (
                      <option key={gateway.guid} value={gateway.guid}>
                        {gateway.name}
                      </option>
                    );
                  })}
                </FormControl>
              </FormGroup>
            </Col>

            <Col className="col">
              <ControlLabel>Currency* </ControlLabel>
              <FormGroup
                validationState={this.formValidation(this.state.currency)}
              >
                <FormControl
                  name="currencySelect"
                  componentClass="select"
                  onChange={e => this.handleCurrencySelect(e)}
                >
                  <option value=""></option>
                  {this.state.currencies.map(currency => {
                    return (
                      <option key={currency.guid} value={currency.guid}>
                        {currency.name}
                      </option>
                    );
                  })}
                </FormControl>
              </FormGroup>
            </Col>

            <Col className="col">
              <ControlLabel>Hold Rate* </ControlLabel>
              <FormGroup
                validationState={this.formValidation(this.state.holdRate)}
              >
                <FormControl
                  name="holdRate"
                  type="number"
                  value={this.state.holdRate}
                  onChange={this.handleChange}
                />
              </FormGroup>
            </Col>

            <Col className="col">
              <ControlLabel>Hold Period* </ControlLabel>
              <FormGroup
                validationState={this.formValidation(this.state.holdPeriod)}
              >
                <FormControl
                  name="holdPeriod"
                  type="number"
                  value={this.state.holdPeriod}
                  onChange={this.handleChange}
                />
              </FormGroup>
            </Col>

            <Col className="col">
              <ControlLabel>Apply From* </ControlLabel>
              <FormGroup
                validationState={
                  this.formValidation(this.state.applyFrom) &&
                  this.timeValidation(this.state.applyFrom)
                }
              >
                <FormControl
                  name="applyFrom"
                  type="date"
                  value={this.state.applyFrom}
                  onChange={e => this.handleChange(e)}
                />
              </FormGroup>
            </Col>

            <Col className="col">
              <ControlLabel>Apply To* </ControlLabel>
              <FormGroup
                validationState={
                  this.formValidation(this.state.applyTo) &&
                  this.timeValidation(this.state.applyTo)
                }
              >
                <FormControl
                  name="applyTo"
                  type="date"
                  value={this.state.applyTo}
                  onChange={e => this.handleChange(e)}
                />
              </FormGroup>
            </Col>


            <div>
              {renderRatesStatus}
            </div>

            <ChargeCreator
              handleChargeChange={this.handleChange}
              periods={this.state.periods}
              conditions={this.state.conditions}
              oneTimeChargeName={this.state.oneTimeChargeName}
              oneTimeChargeSum={this.state.oneTimeChargeSum}
              periodicChargeName={this.state.periodicChargeName}
              periodicChargeSum={this.state.periodicChargeSum}
              periodicChargePercent={this.state.periodicChargePercent}
              periodicChargePeriod={this.state.periodicChargePeriod}
              conditionalChargeName={this.state.conditionalChargeName}
              conditionalChargeSum={this.state.conditionalChargeSum}
              conditionalChargePercent={this.state.conditionalChargePercent}
              conditionalChargeCondition={this.state.conditionalChargeCondition}
              conditionalChargeConditionValue={this.state.conditionalChargeConditionValue} />

            <div align="center">
              {this.state.isLoading ? (
                <ReactLoading type="cylon" color="grey" />
              ) : (
                  <Button type="submit">Add</Button>
                )}
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    shops: state.shops.shopsList,
    shopGateways: state.shops.shopGateways,
    currencies: state.currencies.currenciesList,
    transactionsForGateway: state.transactions.transactionsForGateway,
    transactionsForGatewayLoading: state.transactions.transactionsForGatewayLoading,
    currentPage: state.rates.currentPage,
    pageSize: state.rates.pageSize,
    rates: state.rates.ratesList,
    ratesSearch: state.search.ratesSearch
  };
};

export default connect(
  mapStateToProps,
  {
    getAllShops,
    getShopGatewaysAction,
    getAllCurrencies,
    addRateAction,
    addOneTimeChargeAction,
    addPeriodicChargeAction,
    addConditionalChargeAction,
    getTransactionsForGatewayAction,
    showModal
  }
)(RateCreator);

RateCreator.propTypes = {
  gateways: PropTypes.array,
  currencies: PropTypes.array,
  rates: PropTypes.array,
  getShopGatewaysAction: PropTypes.func,
  getAllCurrencies: PropTypes.func,
  addRateAction: PropTypes.func,
  showModal: PropTypes.func,
  shops: PropTypes.array,
  getAllShops: PropTypes.func,
  currentPage: PropTypes.number,
  pageSize: PropTypes.number,
  ratesSearch: PropTypes.array,
  shopGateways: PropTypes.object,
  transactionsForGateway: PropTypes.array,
  transactionsForGatewayLoading: PropTypes.func,
  getTransactionsForGatewayAction: PropTypes.func,
};
