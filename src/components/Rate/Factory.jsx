import React, { Component } from "react";
import Table from "components/UI/Table";
import Button from "components/UI/Button";
import Pagination from "components/UI/Pagination";
import ability from "config/ability";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";
import Select from "components/UI/MultiSelect";
import { Row, Col } from "react-bootstrap";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse";
import { ExportButton } from "components/ExportButton";

export default class AbstractComponentRate extends Component {
  state = {
    currentPage: 1,
    pageSize: 10,
    isSearch: false
  };

  componentDidMount = async () => {
    await this.props.get(1, this.state.pageSize);
  };

  componentWillUnmount = () => {
    this.props.reset();
  }

  handleSelectPageSize = (e) => {
    if (e.name==="All") {
      if (this.state.isSearch)
        this.props.get(1, this.props.count, this.props.searchData);
      else  
        this.props.get(1, this.props.count);
      this.setState({
        pageSize: String(this.props.count), 
        currentPage: 1
      }); 
    }
    else {
      if (this.state.isSearch)
        this.props.get(1, e.name, this.props.searchData);
      else
        this.props.get(1, e.name);
      this.setState({
        pageSize: e.name, 
        currentPage: 1
      });  
    }   
  }
  
  handlePageChange = async (page) => {
    if (this.state.isSearch)
      await this.props.get(page, this.state.pageSize, this.props.searchData);
    else
      await this.props.get(page, this.state.pageSize);
    this.setState({
      currentPage: page
    });
  };

  handleSearch = async () => {
    this.setState({
      isSearch: true,
      currentPage: 1
    });
    await this.props.get(1, this.state.pageSize, this.props.searchData);
  }

  handleReset = async () => {
    this.setState({
      isSearch: false,
      currentPage: 1
    });
    await this.props.reset();
    await this.props.get(1, this.state.pageSize);
  }

  handleDelete = async (shop_guid, gateway_guid, currency_guid) => {
    try {
      let currentPage = this.state.currentPage;
      if (this.props.data.length === 1) {
        currentPage = currentPage === 1 ? currentPage : currentPage - 1;
        this.setState({ currentPage });
      }
      await this.props.delete(shop_guid, gateway_guid, currency_guid, currentPage, this.state.pageSize, this.props.searchData);
    }
    catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  getPagedData = () => {
    const {
      pageSize
    } = this.state;
    const data = this.props.data ? this.props.data : [];
    const count = this.props.count;
    const pagesCount = (count / pageSize) + (1 && !!(count % pageSize));
    return { pagesCount, data };
  };

  addDeleteField = (columns) => {
    let newColumns = columns;
    if (newColumns[newColumns.length - 1].key === "delete")
      return [
        ...newColumns.slice(0, -1),
        ability.can("EXECUTE", this.props.name.toUpperCase()) &&
        {
          key: "delete",
          content: item => (
            <i
              className="far fa-trash-alt"
              style={{ cursor: "pointer", color: "red" }}
              onClick={() =>
                swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                  .then((willDelete) => {
                    if (willDelete) {
                      this.handleDelete(item.shop_guid, item.gateway_guid, item.currency_guid);
                      swal("Deleted", {
                        icon: "success",
                        button:false,
                        timer: 2000
                      });
                    }
                  })
              }
            />
          ),
          label: "Delete"
        }
      ];
    else
      return newColumns;
  }

  styles = {
    menu: (styles) => {
      return {
        ...styles,
        top:"auto",
        bottom:"100%"
      };
    }
  }

  render() {
    let columns = this.addDeleteField(this.props.columns);
    const { currentPage } = this.state;
    const { pagesCount, data } = this.getPagedData();
    if (this.props.loading) return <Spinner />;
    else return (
      <React.Fragment>
        <div className={this.props.isSearch ? "show" : "hid"}>
          <Button
            id="search-button"
            className={"btn btn-primary search-button"}
            onClick={this.handleSearch}
          >
            Search
          </Button>
          <Button
            className={"btn btn-primary reset-search-button"}
            onClick={this.handleReset}
          >
            Reset
          </Button>
        </div>
        <Row>
          <Col md={10}/>
          <Col md={2}>   
            <ExportButton
              id="table"
              table={true}
              columns={this.props.columns}
              data={this.props.data}
              name={this.props.name}
            >
              export
            </ExportButton>
          </Col> 
        </Row> 
        <Table
          columns={columns}
          search={this.props.search}
          data={data}
        />
        <Row>
          <Col md={10}>
            <Pagination
              pagesCount={pagesCount}
              currentPage={currentPage}
              onPageChange={this.handlePageChange}
            />
          </Col>
          <Col md={1} style={{ marginTop: "30px", textAlign:"right" }}>
            <div>
              <label>Page size: {this.state.pageSize}</label>
            </div>
          </Col>
          <Col md={1} style={{ marginTop: "20px" }}>
            <Select 
              multi={false} 
              name="Select the page size" 
              options={[ { guid:"1", name:"10" }, { guid:"2", name:"20" }, { guid:"3", name:"50" }, { guid:"4", name:"100" }, { guid:"5", name:"All" } ]} 
              onSelect={this.handleSelectPageSize}
              isSearchable={false}
              styles={this.styles}
            />
          </Col>
        </Row>  
      </React.Fragment>
    );
  }
}

AbstractComponentRate.propTypes = {
  data: PropTypes.array,
  count: PropTypes.number,
  columns: PropTypes.array,
  get: PropTypes.func,
  delete: PropTypes.func,
  search: PropTypes.func,
  reset: PropTypes.func,
  loading: PropTypes.bool,
  searchData: PropTypes.object,
  isSearch: PropTypes.bool,
  name: PropTypes.string,
};