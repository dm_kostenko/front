import React, { Component } from "react";
import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography
} from "react-simple-maps";
import tooltip from "wsdm-tooltip";
import { countries } from "helpers/countryArray";
import Spinner from "components/UI/Spinner";

const wrapperStyles = {
  width: "100%",
  maxWidth: "auto",
  margin: "0 auto",
};





class Map extends Component {

  state = {
    data: {},
    loading: true
  }
  

  componentDidMount = async() => {
    this.tip = tooltip({
      styles: {
        color: "black",
        "font-size": "1.2rem",
        "background-color": "white",
        "border-radius": "3px",
      },
    });
    this.tip.create();

    
    let data = {};

    let min = 1000000000000000000, max = -1;

    countries.forEach(country => {
      let value = Math.floor(Math.random() * 1000);
      if(value > max)
        max = value;
      if(value < min)
        min = value;
      data[country.code] = { value };
    });


    const keys = Object.keys(data);
    for(let i = 0; i < keys.length; i++) {
      let j = keys[i];
      data[j] = {
        ...data[j],
        fill: `rgba(30, 130, 76, ${(data[j].value - min)/(max - min) })`
      };
    }

    this.setState({ data, loading: false });
  }
  handleMouseMove = (geography, event) => {
    const obj = this.state.data[geography.properties.ISO_A2];
    if(obj) {
      this.tip.show(`
        <div>
          <b>${geography.properties.NAME}</b><br/>
          Transactions: <b>${obj.value}</b>
        </div>
     `);
      this.tip.position({ pageX: event.pageX, pageY: event.pageY });
    }
  }
  handleMouseLeave = () => {
    this.tip.hide();
  }

  
  
  render() {

    if(this.state.loading)
      return <Spinner/>;

    return(
      <div style={wrapperStyles}>
        <ComposableMap
          projectionConfig={{
            scale: 200,
            rotation: [ -11,0,0 ],
          }}
          width={1600}
          height={800}
          style={{
            width: "auto",
            height: "auto"
          }}
        >
          <ZoomableGroup zoom={1} center={[ 0,20 ]} disablePanning>
            <Geographies geography={ "/config.json" }>
              {(geographies, projection) => geographies.map((geography, i) => geography.id !== "ATA" && (
                <Geography
                  key={i}
                  geography={geography}
                  projection={projection}                  
                  onMouseMove={this.handleMouseMove}
                  onMouseLeave={this.handleMouseLeave}
                  style={{
                    default: {
                      fill: this.state.data[geography.properties.ISO_A2] ? this.state.data[geography.properties.ISO_A2].fill : "#aaaaaa",
                      stroke: "#607D8B",
                      strokeWidth: 0.75,
                      outline: "none"
                    },
                    hover: {
                      fill: this.state.data[geography.properties.ISO_A2] ? this.state.data[geography.properties.ISO_A2].fill : "#ECEFF1",
                      stroke: "black",
                      title: "text",
                      strokeWidth: 0.75,
                      outline: "none"
                    },
                    pressed: {                      
                      fill: this.state.data[geography.properties.ISO_A2] ? this.state.data[geography.properties.ISO_A2].fill : "#ECEFF1",
                      stroke: "black",
                      strokeWidth: 0.75,
                      outline: "none"
                    },
                  }}
                />
              ))}
            </Geographies>     
          </ZoomableGroup>
        </ComposableMap>
      </div>
    );
  }
}

export default Map;