import React from "react";
import { connect } from "react-redux";
import { LineChart } from "components/UI/LineChart";
import { getTransactionTypesAction } from "actions/transactions";
import { lastNDays, formatDate, daysSinceTo } from "services/dateTime/dateTime";
import { borderColors, backgroundColors } from "constants/charts/colors";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";
import moment from "moment";

class LineChartTransactionsHistory extends React.Component {
  state = {
    loading: true,
    labels: lastNDays(7)
  }

  componentDidMount = () => {
    this.props.getTransactionTypesAction();
  }

  shouldComponentUpdate = (nextProps) => {
    if (nextProps.data !== this.props.data) {
      let labels;
      if (nextProps.data.from_date && nextProps.data.to_date) {
        labels = daysSinceTo(new Date(moment(nextProps.data.from_date).format("MM.DD")), new Date(moment(nextProps.data.to_date).format("MM.DD")));
        labels.shift();
        labels.push(moment(nextProps.data.to_date).format("DD.MM"));
      }
      else {
        labels = lastNDays(7);
      }
      this.setState({ labels });
      this.props.getTransactionTypesAction(nextProps.data);
    }
    return true;
  }

  render() {
    const labels = this.state.labels;
    let datasetsPie = [];
    if (this.props.loading) return <Spinner />;
    else {    
      let transactionTypes = Object.keys(this.props.reportTransactionType);
      let datasets = [];
      transactionTypes.map((transactionType, index) => {
        datasets[index] = [];
        this.props.reportTransactionType[transactionType].map(item => {
          const findIndex = this.state.labels.findIndex(itemD => itemD == moment(item.date).format("DD.MM"));
          if (findIndex != -1)
            datasets[index][findIndex] = item.success;
        });
        for ( let i=0;i<this.state.labels.length;i++ ) {
          if (!datasets[index][i])
            datasets[index][i] = 0;
        }
      });
      datasetsPie = datasets.map((dataset, index) => {
        return {
          label: transactionTypes[index],
          fill: false,
          lineTension: 0.1,
          backgroundColor: backgroundColors[index],
          borderColor: borderColors[index],
          borderCapStyle: "butt",
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: borderColors[index],
          pointBackgroundColor: "#fff",
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: borderColors[index],
          pointHoverBorderColor: "black",
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: dataset
        };
      });
    }

    return (
      <div style={{ padding: 30 }}>
        <div className="header">Transactions</div>
        <LineChart
          labels={labels}
          datasets={datasetsPie}
          name="Transactions"
          options={{
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }}
        />
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    reportTransactionType: state.transactions.types,
    loading: state.transactions.transactionTypeLoading
  };
};

export default connect(mapStateToProps, {
  getTransactionTypesAction
})(LineChartTransactionsHistory);

LineChartTransactionsHistory.propTypes = {
  reportTransactionType: PropTypes.object,
  getTransactionTypesAction: PropTypes.func,
  data: PropTypes.object,
  params: PropTypes.object,
  loading: PropTypes.bool,
};