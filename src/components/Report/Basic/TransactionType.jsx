import React from "react";
import { PieChart } from "components/UI/PieChart";
import { getTransactionTypesAction } from "actions/transactions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { backgroundColors } from "constants/charts/colors";

class PieChartTransactionTypes extends React.Component {
  state = {
    daysCount: 7,
    loading: true,
    params: {}
  }

  componentDidMount() {
    this.props.getTransactionTypesAction(this.state.params);
  }

  shouldComponentUpdate = (nextProps) => {
    if (nextProps.params !== this.props.params)
    {
      let params = {
        merchant_guid: nextProps.params.merchant_guid,
        shop_guid: nextProps.params.shop_guid,
        from_date: nextProps.params.from_date,
        to_date: nextProps.params.to_date,
      };
      this.props.getTransactionTypesAction(params);
    }
     
    return true;
  }
  render() {
    let datasetsPie = [];
    let labelsPie = [];
    let data = [];
    let colors = [];
    let object = this.props.data;
    Object.keys(object).map(function(key, index) {
      let count = 0;
      object[key].map( item => {
        count += item.success;
      });
      data.push(count);
      colors.push(backgroundColors[index]);
      datasetsPie = [ { data: data, backgroundColor: colors } ];
      labelsPie = [ ...labelsPie, key ];
    });
    
    return (
      <PieChart
        labels={labelsPie.length !== 0 ? labelsPie : [ "There are no transactions type yet" ]}
        datasets={datasetsPie.length !== 0 ? datasetsPie : [ {
          data: [ 1 ],
          backgroundColor: "#CCC"
        } ]}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.transactions.types
  };
};

export default connect(mapStateToProps, {
  getTransactionTypesAction
})(PieChartTransactionTypes);

PieChartTransactionTypes.propTypes = {
  getTransactionTypesAction: PropTypes.func,
  params: PropTypes.object,
  data: PropTypes.array,
};

