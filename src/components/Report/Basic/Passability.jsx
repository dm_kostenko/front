import React from "react";
import { connect } from "react-redux";
import { PieChart } from "components/UI/PieChart";
import { getTransactionTypesAction } from "actions/transactions";
import Spinner from "components/UI/Spinner";
import PropTypes from "prop-types";
import { backgroundColors } from "constants/charts/colors";

class PieChartPassability extends React.Component {
  state = {
    daysCount: 7,
    loading: true,
    params: {}
  }

  componentDidMount = () => {
    this.props.getTransactionTypesAction(this.state.params);
  }

  shouldComponentUpdate = (nextProps) => {
    if (nextProps.params !== this.props.params) {
      let params = {
        merchant_guid: nextProps.params.merchant_guid,
        shop_guid: nextProps.params.shop_guid,
        from_date: nextProps.params.from_date,
        to_date: nextProps.params.to_date,
      };
      this.props.getTransactionTypesAction(params);
    }
    return true;
  }

  render() {
    let object = this.props.data;
    let data = [];
    let success = 0;
    let failed = 0;

    Object.keys(this.props.data).map(function (key) {

      object[key].map(item => {
        success += item.success;
        failed += item.failed;
      });
      data.push(success);
      data.push(failed);
    });

    const datasetsPie =
      success > 0 || failed > 0
        ? [
          {
            data: [success, failed],
            backgroundColor: [
              "rgba(75,192,192,1)",
              "rgba(255,99,132,1)"
            ]
          }
        ] :
        [
          {
            data: [1],
            backgroundColor: "#CCC"
          }
        ];

    const labelsPie =
      datasetsPie[0].data.length != 1 ?
        [
          "Success",
          "Failed",
        ]
        : [
          "There are no transactions yet"
        ];

    return (
      <PieChart
        name="Passability"
        labels={labelsPie}
        datasets={datasetsPie}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.transactions.types
  };
};

export default connect(mapStateToProps, {
  getTransactionTypesAction
})(PieChartPassability);

PieChartPassability.propTypes = {
  getTransactionTypesAction: PropTypes.func,
  params: PropTypes.object,
  data: PropTypes.array
};
