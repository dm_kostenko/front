import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import Table from "components/UI/Table/index";
import Pagination from "components/UI/Pagination/index";
import { connect } from "react-redux";
import { getOrdersAction } from "actions/transactions";
import Spinner from "components/UI/Spinner/index";
import moment from "moment";
import CustomButton from "components/UI/Button";
import PropTypes from "prop-types";
import { ExportButton } from "components/ExportButton";

export class Orders extends Component {
  state = {
    reportOrders: [],
    count: "",
    currentPage: 1,
    pageSize: 10,
    fromDate: null,
    toDate: null,
    data: {
      from_date: null,
      to_date: null,
    },
  };

  columnsOrders = [
    { path: "guid", label: "ID" },
    { path: "status", label: "Status" },
    { path: "date", label: "Date", content: order => (moment(order.date).utcOffset(0).format("DD.MM.YYYY HH:mm")) },
    { path: "type", label: "Type" }
  ];


  async componentDidMount() {
    await this.props.getOrdersAction({
      page:1, 
      items:this.state.pageSize,
      from_date:this.state.data.from_date,
      to_date:this.state.data.to_date
    });
    this.setState({
      reportOrders: this.props.reportOrders,
      count: this.props.count
    });
  }

  handlePageChange = async (page) => {
    await this.props.getOrdersAction({
      page, 
      items:this.state.pageSize,
      from_date:this.state.data.from_date,
      to_date:this.state.data.to_date
    });
    this.setState({
      reportOrders: this.props.reportOrders,
      count: this.props.count,
      currentPage: page
    });
  };

  getPagedData = () => {
    const { count } = this.props;
    let { pageSize } = this.state;
    const pagesCount = count ? (count / pageSize) + (1 && !!(count % pageSize)) : 0;
    return { pagesCount };
  };

  handleTimeChange = (event) => {
    let value = event.target.value;
    value = moment(value).format("YYYY-MM-DDTHH:mm:ss");
    this.setState({ [event.target.name]: value });
  }

  handleClick = () => {
    let from_date = this.state.fromDate === "Invalid date" ? null: this.state.fromDate;
    let to_date = this.state.toDate === "Invalid date" ? null: this.state.toDate;
    let merchant_guid = this.state.selectedMerchant === "" ? null: this.state.selectedMerchant;
    let shop_guid = this.state.selectedShop === "" ? null: this.state.selectedShop;
    let data= {
      merchant_guid: merchant_guid == 1 ? null: merchant_guid,
      shop_guid: shop_guid == 1 ? null: shop_guid,
      from_date: from_date === "" ? null: from_date,
      to_date: to_date === "" ? null: to_date,
    };
    this.setState({ data: data }, () => { this.props.getOrdersAction({
      page:1,
      items:this.state.pageSize,
      from_date:this.state.data.from_date,
      to_date:this.state.data.to_date
    });
    });
  }

  render() {
   
    const { currentPage } = this.state;
    const { pagesCount } = this.getPagedData();
    const { reportOrders } = this.state;
    return (
      <div  className="card-large">
        <Row>
          <Col md={2} sm={2} xs={2}>
            <label style={{ marginRight: "5px" }}>from</label>
            <input 
              type="date" 
              className="datetimepicker" 
              name="fromDate"
              onChange={this.handleTimeChange} >
            </input>
          </Col>
          <Col md={2} sm={2} xs={2}>
            <label style={{ marginRight: "5px" }}>to</label>
            <input 
              type="date" 
              className="datetimepicker" 
              name="toDate"
              onChange={this.handleTimeChange} >
            </input>
              
          </Col>
          <Col md={2} sm={2} xs={2}>
            <CustomButton className="btn"  onClick={this.handleClick}>Find</CustomButton>
          </Col>
            
        </Row>
        { this.props.reportOrdersLoading ? <Spinner /> :
       
          <Row style={{ marginLeft: "0px" }}>

            <div className="header">Orders</div>
            <div className="content" style={{ overflow: "auto" }}>
              <Row>
                <Col md={12}>
                  <div className="table">
                    <ExportButton
                      id="reportTable"
                      table={true}
                      columns={this.columnsOrders}
                      data={reportOrders}
                      name="Orders"
                    >
                        export
                    </ExportButton>
                    <Table
                      columns={this.columnsOrders}
                      data={this.props.reportOrders}
                      disableSearch={true}
                    />
                    <Pagination
                      pagesCount={pagesCount}
                      currentPage={currentPage}
                      onPageChange={this.handlePageChange}
                    />
                  </div>
                </Col>
              </Row>
            </div>

          </Row>
        }
      </div>
    );
    
  }
}

const mapStateToProps = (state) => {
  return {
    reportOrders: state.transactions.orders.data,
    count: state.transactions.orders.count,
    reportOrdersLoading: state.transactions.ordersLoading
  };
};

export default connect(mapStateToProps, {
  getOrdersAction
})(Orders);

Orders.propTypes = {
  getOrdersAction: PropTypes.func,
  count: PropTypes.number,
  reportOrders: PropTypes.array,
  reportOrdersLoading: PropTypes.bool,
};
