import React from "react";
import { PieChart } from "components/UI/PieChart";
import { getTransactionsCurrenciesAction } from "actions/transactions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { backgroundColors } from "constants/charts/colors";

class PieChartCurrencies extends React.Component {
  state = {
    daysCount: 7,
    loading: true,
    params: {}
  }

  componentDidMount() {
    this.props.getTransactionsCurrenciesAction();
  }

  shouldComponentUpdate = (nextProps) => {
    if (nextProps.params !== this.props.params)
    {
      let params = {
        merchant_guid: nextProps.params.merchant_guid,
        shop_guid: nextProps.params.shop_guid,
        from_date: nextProps.params.from_date,
        to_date: nextProps.params.to_date,
      };
      this.props.getTransactionsCurrenciesAction( params );
    }
     
    return true;
  }
  
  render() {
    let datasetsPie = [];
    let labelsPie = [];
    let data = [];
    let colors = [];
    this.props.data.map((item, index) => {
      data.push(item.count);
      colors.push(backgroundColors[index]);
      datasetsPie = [ { data: data, backgroundColor: colors } ];
      labelsPie = [ ...labelsPie, item.currency ];
    });
    return (
      <PieChart
        labels={labelsPie.length !== 0 ? labelsPie : [ "There are no currencies yet" ]}
        datasets={datasetsPie.length !== 0 ? datasetsPie : [ {
          data: [ 1 ],
          backgroundColor: "#CCC"
        } ]}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.transactions.transactionCurrency
  };
};

export default connect(mapStateToProps, {
  getTransactionsCurrenciesAction
})(PieChartCurrencies);

PieChartCurrencies.propTypes = {
  getTransactionsCurrenciesAction: PropTypes.func,
  params: PropTypes.object,
  data: PropTypes.array,
};
