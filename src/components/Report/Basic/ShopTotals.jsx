import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import Table from "components/UI/Table/index";
import Pagination from "components/UI/Pagination/index";
import { connect } from "react-redux";
import { getShopTotalsAction } from "actions/shops";
import Spinner from "components/UI/Spinner/index";
import CustomButton from "components/UI/Button";
import moment from "moment";
import PropTypes from "prop-types";
import { ExportButton } from "components/ExportButton";

export class ShopTotals extends Component {
  state = {
    shops: [],
    count: "",
    currentPage: 1,
    pageSize: 10,
    fromDate: null,
    toDate: null,
    data: {
      from_date: null,
      to_date: null,
    },
  };

  columnsShops = [
    { path: "shop_guid", label: "ID" },
    { path: "shop_name", label: "Shop name" },
    { path: "currency", label: "Currency" },
    { path: "transaction_type", label: "Transaction type" },
    { path: "status", label: "Status" },
    { path: "number", label: "Number" }
  ];


  async componentDidMount() {
    await this.props.getShopTotalsAction({
      page:1, 
      items:this.state.pageSize, 
      from_date:this.state.data.from_date,
      to_date:this.state.data.to_date
    });
    this.setState({
      shops: this.props.shops,
      count: this.props.count
    });
  }

  handlePageChange = async (page) => {
    await this.props.getShopTotalsAction({
      page, 
      items:this.state.pageSize, 
      from_date:this.state.data.from_date,
      to_date:this.state.data.to_date
    });
    this.setState({
      shops: this.props.shops,
      count: this.props.count,
      currentPage: page
    });
  };

  getPagedData = () => {
    const { count } = this.props;
    let { pageSize } = this.state;
    const pagesCount = count ? (count / pageSize) + (1 && !!(count % pageSize)) : 0;
    return { pagesCount };
  };

  handleTimeChange = (event) => {
    let value = event.target.value;
    value = moment(value).format("YYYY-MM-DDTHH:mm:ss");
    this.setState({ [event.target.name]: value });
  }

  handleClick = () => {
    let from_date = this.state.fromDate === "Invalid date" ? null: this.state.fromDate;
    let to_date = this.state.toDate === "Invalid date" ? null: this.state.toDate;
    let data= {
      from_date: from_date === "" ? null: from_date,
      to_date: to_date === "" ? null: to_date,
    };
    this.setState({ data: data }, () => { this.props.getShopTotalsAction({
      page:1, 
      items:this.state.pageSize, 
      from_date:this.state.data.from_date,
      to_date:this.state.data.to_date
    }); 
    });
    this.setState({
      shops: this.props.shops,
      count: this.props.count,
    });
  }


  render() {
    const { currentPage } = this.state;
    const { pagesCount } = this.getPagedData();
    const { shops } = this.state;
    return (
        
      <div  className="card-large">
        <Row>
          <Col md={2} sm={2} xs={2}>
            <label style={{ marginRight: "5px" }}>from</label>
            <input 
              type="date" 
              className="datetimepicker" 
              name="fromDate"
              onChange={this.handleTimeChange} >
            </input>
          </Col>
          <Col md={2} sm={2} xs={2}>
            <label style={{ marginRight: "5px" }}>to</label>
            <input 
              type="date" 
              className="datetimepicker" 
              name="toDate"
              onChange={this.handleTimeChange} >
            </input>
              
          </Col>
          <Col md={2} sm={2} xs={2}>
            <CustomButton className="btn"  onClick={this.handleClick}>Find</CustomButton>
          </Col>
            
        </Row>
        { this.props.reportShopsLoading ? <Spinner /> :
        
          <Row style={{ marginLeft: "0px" }}>

            <div className="header">Shop totals</div>
            <div className="content" style={{ overflow: "auto" }}>
            
              <Row>
                <Col md={12}>
                  <div className="table">
                    <ExportButton
                      id="reportTable"
                      table={true}
                      columns={this.columnsShops}
                      data={shops}
                      name="Shop totals"
                    >
                        export
                    </ExportButton>
                    <Table
                      columns={this.columnsShops}
                      data={this.props.shops}
                      disableSearch={true}
                    />
                    <Pagination
                      pagesCount={pagesCount}
                      currentPage={currentPage}
                      onPageChange={this.handlePageChange}
                    />
                  </div>
                </Col>
              </Row>

            </div>
          </Row>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    shops: state.shops.reportSpops,
    count: state.shops.reportsCount,
    reportShopsLoading: state.shops.reportShopsLoading
  };
};

export default connect(mapStateToProps, {
  getShopTotalsAction
})(ShopTotals);

ShopTotals.propTypes = {
  getShopTotalsAction: PropTypes.func,
  count: PropTypes.number,
  shops: PropTypes.array,
  reportShopsLoading: PropTypes.bool,
};