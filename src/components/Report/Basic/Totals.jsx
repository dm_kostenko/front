import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import Table from "components/UI/Table/index";
import Pagination from "components/UI/Pagination/index";
import { connect } from "react-redux";
import { getTotalsAction } from "actions/transactions";
import Spinner from "components/UI/Spinner/index";
import CustomSelect from "components/UI/MultiSelect";
import CustomButton from "components/UI/Button";
import moment from "moment";
import PropTypes from "prop-types";
import { ExportButton } from "components/ExportButton";
import { getAllShops, getMerchantShopsAction } from "actions/shops";

export class Totals extends Component {
  state = {
    detailedTotals: [],
    count: "",
    currentPage: 1,
    pageSize: 10,
    fromDate: null,
    toDate: null,
    selectedMerchant: null,
    selectedShop: null,
    data: {
      merchant_guid: null,
      shop_guid: null,
      from_date: null,
      to_date: null,
    },
    shops: []
  };

  columnsDetailedTotals = [
    { path: "currency", label: "Currency" },
    { path: "type", label: "Transaction type" },
    { path: "status", label: "Status" },
    { path: "number", label: "Number" }
  ];


  async componentDidMount() {
    await this.props.getTotalsAction({
      page:1, 
      items:this.state.pageSize, 
      merchant_guid:this.state.data.merchant_guid, 
      shop_guid:this.state.data.shop_guid, 
      from_date:this.state.data.from_date,
      to_date:this.state.data.to_date 
    });
    await this.props.getAllShops();
    this.setState({
      detailedTotals: this.props.detailedTotals,
      count: this.props.count,
      shops: [ ...this.props.shops, { guid:"1", name:"All shops", label: "All shops", value: "1" } ]
    });
  }

  handlePageChange = async (page) => {
    await this.props.getShopTotalsAction({
      page, 
      items:this.state.pageSize, 
      merchant_guid:this.state.data.merchant_guid, 
      shop_guid:this.state.data.shop_guid, 
      from_date:this.state.data.from_date,
      to_date:this.state.data.to_date 
    });
    this.setState({
      detailedTotals: this.props.detailedTotals,
      count: this.props.count,
      currentPage: page
    });
  };

  getPagedData = () => {
    const { count } = this.props;
    let { pageSize } = this.state;
    const pagesCount = count ? (count / pageSize) + (1 && !!(count % pageSize)) : 0;
    return { pagesCount };
  };

  handleTimeChange = (event) => {
    let value = event.target.value;
    value = moment(value).format("YYYY-MM-DDTHH:mm:ss");
    this.setState({ [event.target.name]: value });
  }

  handleSelectMerchant = async option => {
    if (option.guid !== "" && option.guid !== "1") {
      await this.props.getMerchantShopsAction(option.guid);
      this.setState({
        shops: [ ...this.props.shops, { guid:"1", name:"All shops", label: "All shops", value: "1" } ]
      });
    } else {
      await this.props.getAllShops();
      this.setState({
        shops: [ ...this.props.shops, { guid:"1", name:"All shops", label: "All shops", value: "1" } ]
      });
    }
    this.setState({
      selectedMerchant: option.guid
    });
  };

  handleSelectShop = (option) => {
    this.setState({
      selectedShop: option.guid
    });
  };

  handleClick = () => {
    let from_date = this.state.fromDate === "Invalid date" ? null: this.state.fromDate;
    let to_date = this.state.toDate === "Invalid date" ? null: this.state.toDate;
    let merchant_guid = this.state.selectedMerchant === "" ? null: this.state.selectedMerchant;
    let shop_guid = this.state.selectedShop === "" ? null: this.state.selectedShop;
    let data= {
      merchant_guid: merchant_guid == 1 ? null: merchant_guid,
      shop_guid: shop_guid == 1 ? null: shop_guid,
      from_date: from_date === "" ? null: from_date,
      to_date: to_date === "" ? null: to_date,
    };
    this.setState({ data: data }, () => { this.props.getTotalsAction({
      page:1, 
      items:this.state.pageSize, 
      merchant_guid:this.state.data.merchant_guid, 
      shop_guid:this.state.data.shop_guid, 
      from_date:this.state.data.from_date,
      to_date:this.state.data.to_date 
    }); 
    });
  }

  render() {
    const { currentPage } = this.state;
    const { pagesCount } = this.getPagedData();
    const { detailedTotals } = this.state;
    return (
      <div  className="card-large">
        <Row>
          <Col md={3} sm={3} xs={3}>
            <CustomSelect 
              multi={false} 
              name="selectedMerchant" 
              options={this.props.merchants} 
              value="All merchants"
              defaultValue = {{ label: "All merchants", value: "1" }}
              onSelect={this.handleSelectMerchant}
            />
          </Col>
          <Col md={3} sm={3} xs={3}>
            <CustomSelect 
              multi={false} 
              name="selectedShop" 
              options={this.state.shops} 
              value="All shops"
              defaultValue = {{ label: "All shops", value: "1" }}
              onSelect={this.handleSelectShop}
            />
          </Col>
          <Col md={2} sm={2} xs={2}>
            <label style={{ marginRight: "5px" }}>from</label>
            <input 
              type="date" 
              className="datetimepicker" 
              name="fromDate"
              onChange={this.handleTimeChange} >
            </input>
          </Col>
          <Col md={2} sm={2} xs={2}>
            <label style={{ marginRight: "5px" }}>to</label>
            <input 
              type="date" 
              className="datetimepicker" 
              name="toDate"
              onChange={this.handleTimeChange} >
            </input>
              
          </Col>
          <Col md={2} sm={2} xs={2}>
            <CustomButton className="btn"  onClick={this.handleClick}>Find</CustomButton>
          </Col>
            
        </Row>
        { this.props.detailedTotalsLoading ? <Spinner /> :
          <Row style={{ marginLeft: "0px" }}>

            <div className="header">Totals</div>
            <div className="content" style={{ overflow: "auto" }}>
              <Row>
                <Col md={12}>
                
                  <div className="table">
                    <ExportButton
                      id="reportTable"
                      table={true}
                      columns={this.columnsDetailedTotals}
                      data={detailedTotals}
                      name="Totals"
                    >
                        export
                    </ExportButton>
                    <Table
                      columns={this.columnsDetailedTotals}
                      data={this.props.detailedTotals}
                      disableSearch={true}
                    />
                    
                    <Pagination
                      pagesCount={pagesCount}
                      currentPage={currentPage}
                      onPageChange={this.handlePageChange}
                    />
                  </div>
                </Col>
              </Row>
            </div>
          </Row>
        }
         
      </div>
    );
  }
  
}

const mapStateToProps = (state) => {
  return {
    detailedTotals: state.transactions.totals.data,
    count: state.transactions.totals.count,
    detailedTotalsLoading: state.transactions.totalsLoading,
    shops: state.shops.shopsList
  };
};

export default connect(mapStateToProps, {
  getTotalsAction,
  getAllShops,
  getMerchantShopsAction
})(Totals);

Totals.propTypes = {
  getTotalsAction: PropTypes.func,
  count: PropTypes.number,
  detailedTotals: PropTypes.array,
  detailedTotalsLoading: PropTypes.bool,
  getMerchantShopsAction: PropTypes.func,
  getAllShops: PropTypes.func,
  shops: PropTypes.array,
  merchants: PropTypes.array,
};