import React from "react";
import { connect } from "react-redux";
import { HorizontalBarChart } from "components/UI/HorizontalBarChart";
import { getTopMerchantsAction } from "actions/merchants";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";

class HorizontalBarChartTopMerchants extends React.Component {
  state = {
    daysCount: 1,
    topMerchantsCount: 10,
    loading: true
  }

  componentDidMount = () => {
    this.props.getTopMerchantsAction(this.state.daysCount, this.state.topMerchantsCount);
  }

  render() {
    if (this.props.loading) return <Spinner />;
    else {
      const labelsPie = this.props.topMerchants.map(merchant => merchant.name);

      const datasetsPie = [
        {
          label: "transactions",
          backgroundColor: "rgba(255,99,132,0.2)",
          borderColor: "rgba(255,99,132,1)",
          borderWidth: 1,
          hoverBackgroundColor: "rgba(255,99,132,0.4)",
          hoverBorderColor: "rgba(255,99,132,1)",
          data: this.props.topMerchants.map(merchant => merchant.count)
        }
      ];

      return (
        <HorizontalBarChart
          labels={labelsPie}
          datasets={datasetsPie}
          options={{
            scales: {
              xAxes: [ {
                ticks: {
                  beginAtZero: true
                }
              } ]
            }
          }}
          height={50}
        />
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    topMerchants: state.merchants.topMerchants,
    loading: state.merchants.topMerchantsLoading
  };
};

export default connect(mapStateToProps, {
  getTopMerchantsAction
})(HorizontalBarChartTopMerchants);

HorizontalBarChartTopMerchants.propTypes = {
  topMerchants: PropTypes.array,
  getTopMerchantsAction: PropTypes.func,
  loading: PropTypes.bool,
};
