import React, { Component } from "react";
import { Button } from "react-bootstrap";
import PropTypes from "prop-types";
import MultiSelect from "components/UI/MultiSelect";
import { connect } from "react-redux";
import { getFreeLoginsAction } from "actions/logins";
import { getAllRoles } from "actions/roles";
import { addMerchantLoginAction } from "../../actions/merchants";
import { addGroupLoginAction } from "../../actions/groups";
import { addPartnerLoginAction } from "../../actions/partners";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse";

class RowAddLogin extends Component {
  state = {
    selectedRole: "",
    selectedLogin: ""
  }

  handleRoleSelect = (option) => {
    this.setState({ selectedRole: option.guid });
  };

  handleLoginSelect = (option) => {
    this.setState({ selectedLogin: option.guid });
  };

  shouldComponentUpdate = (nextProps) => {
    if (nextProps.logins !== this.props.logins)
      nextProps.logins.map(login => { return login.name = login.username; });
    return true;
  }

  handleSubmit = async () => {
    if (this.state.selectedLogin === "" || this.state.selectedRole === "")
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    else {
      const data = {
        login_guid: this.state.selectedLogin,
        role_guid: this.state.selectedRole,
      };
      try {
        switch (this.props.type) {
        case "merchant": {
          await this.props.addMerchantLoginAction(this.props.id, data);
          swal({
            title: "Login is added",
            icon: "success",
            button:false,
            timer: 2000
          });
          await this.props.update(this.props.id);
          break;
        }
        case "group": {
          await this.props.addGroupLoginAction(this.props.id, data);
          swal({
            title: "Login is added",
            icon: "success",
            button:false,
            timer: 2000
          });
          await this.props.update(this.props.id);
          break;
        }
        case "partner": {
          await this.props.addPartnerLoginAction(this.props.id, data);
          swal({
            title: "Login is added",
            icon: "success",
            button:false,
            timer: 2000
          });
          await this.props.update(this.props.id);
          break;
        }
        default:
          break;
        }
      }
      catch (error) {
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
    }
  };

  render() {
    const logins = this.props.logins;
    if (logins) logins.map(login => { return login.name = login.username; });
    const roles = this.props.roles;
    return (
      <div style={{ textAlign: "center" }}>
        <label>Login*</label>
        <MultiSelect options={logins ? logins : [ { name: "", guid: "" } ]} multi={false} onSelect={this.handleLoginSelect} />
        <label>Role*</label>
        <MultiSelect options={roles ? roles : [ { name: "", guid: "" } ]} multi={false} onSelect={this.handleRoleSelect} />
        <Button onClick={this.handleSubmit} style={{ margin: "15px" }}>Add login</Button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    roles: state.roles.rolesList,
    logins: state.logins.freeLoginsList
  };
};

export default connect(mapStateToProps, {
  addMerchantLoginAction,
  addGroupLoginAction,
  addPartnerLoginAction,
  getFreeLoginsAction,
  getAllRoles
})(RowAddLogin);

RowAddLogin.propTypes = {
  id: PropTypes.string,
  roles: PropTypes.array,
  logins: PropTypes.array,
  type: PropTypes.string,

  addMerchantLoginAction: PropTypes.func,
  addGroupLoginAction: PropTypes.func,
  addPartnerLoginAction: PropTypes.func,
  getFreeLoginsAction: PropTypes.func,
  getAllRoles: PropTypes.func,
  update: PropTypes.func,
};