import React, { Component } from "react";
import { Row, Col, ControlLabel } from "react-bootstrap";
import { connect } from "react-redux";
import { Button } from "react-bootstrap";
import { logIn } from "actions/auth";
import { getRolePrivilegesAction } from "actions/roles";
import MultiSelect from "../UI/MultiSelect";
import PropTypes from "prop-types";
import Loading from "components/UI/Spinner";


class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: "",
      password: "",
      otp: "",
      selectedType: "login-password",
      step: 0,
      showPassword: false,
      showOtp: false,
      authFail: false,
      error: "",
      note: "",
      auth: false,
      loading: false
    };

    this.handleLoginChange = this.handleLoginChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

    handleShowPassword = () => {
      this.setState({
        showPassword: !this.state.showPassword
      });
    }

    handleShowOtp = () => {
      this.setState({
        showOtp: !this.state.showOtp
      });
    }

    handleLoginChange = (event) => {
      this.setState({
        login: event.target.value,
        authFail: false,
        note: ""
      });
    };

    handlePasswordChange = (event) => {
      this.setState({
        password: event.target.value,
        authFail: false,
        note: ""
      });
    };

    handleOtpChange = (event) => {
      this.setState({
        otp: event.target.value,
        authFail: false,
        note: ""
      });
    };

    handleSubmit = (e) => {
      e.preventDefault();
      // this.setState({ 
      //   loading: true,
      //   note: "",
      //   error: ""
      // });

      // let request = { 
      //   username: this.state.login,
      //   type: this.state.selectedType 
      // };
      // if(this.state.selectedType.includes("password"))
      //   request.password = this.state.password;
      // if(this.state.step === 1)
      //   request.otp = this.state.otp;

      // await this.props.logIn(request)
      // if(this.props.response && this.props.response.accessToken && this.props.response.refreshToken) {
      //   this.setState({ auth: true });
      //   window.location.replace(process.env.PUBLIC_URL || "/");
      // }
      // else if(this.props.response && this.props.response.status.includes("Success")) {
      //   this.setState({
      //     step: 1,
      //     loading: false,
      //     note: this.props.response.status,
      //     error: ""
      //   });
      // }
      // else {
      //   let state = {
      //     authFail: true, 
      //     error: JSON.stringify(this.props.error), 
      //     loading: false,
      //     otp: "",
      //     note: ""
      //   };
      //   if([ "Code is expired", "Requests limit exceeded" ].includes(this.props.error))
      //     state = {
      //       ...state,
      //       step: 0
      //     };
      //   this.setState({ 
      //     ...state         
      //   });
      // }
      localStorage.setItem("isLoggedIn", "true");
      localStorage.setItem("role", this.state.login === "admin" ? "admin" : "user");
      window.location.replace(process.env.PUBLIC_URL || "/");
    };


    validateForm() {     
      if(this.state.step === 0)
        return this.state.login.length > 0 && this.state.password.length > 0;
      else
        return this.state.login.length > 0 && this.state.password.length > 0 && this.state.otp.length > 0;
    }

    handleButtonRender() {
      return this.state.loading 
        ? <button className="button-submit-loading" disabled>
          <Loading />
        </button> 
        : <button 
          className="button-submit" 
          type="submit" 
          disabled={!this.validateForm()}
        >
            SIGN IN
        </button>;
    }


    render() {
      return (
        <div className="LoginPage">          
          <div className="login-page">
            <div className="form">
              <form className="login-form" onSubmit={this.handleSubmit} autoComplete="off">
                <p className="header-login">
                  <strong>Sign in</strong>
                </p>
                <div>                  
                  {this.state.step === 0 &&
                    <div 
                      id="login-input"
                      className="login"
                    >
                      <i className="fa fa-user icon" />
                      <input type="text" placeholder="login" onChange={this.handleLoginChange} name="login" value={this.state.login} />
                    </div>}
                  {this.state.step === 1 &&
                    <div 
                      id="otp-input" 
                      className={this.state.step === 0 ? "otp-closed" : "otp-open"}
                    >
                      <i className="fa fa-key icon" />
                      <input 
                        type={this.state.showOtp ? "text" : "password"}                        
                        placeholder="verification code" 
                        onChange={this.handleOtpChange} 
                        value={this.state.otp}
                        name="otp" 
                      />
                      <Button 
                        className="login-page-eye-otp" 
                        onClick={this.handleShowOtp}
                      >
                        {this.state.showOtp
                          ? <span className="fa fa-eye-slash"/>
                          : <span className="fa fa-eye"/>}
                      </Button>
                    </div>}
                  <div
                    id="password-input"
                    className={this.state.step === 1 ? "password-hide" : "password-open"}
                  >
                    <i 
                      className={"fa fa-lock icon"}
                    />
                    <input 
                      
                      type={this.state.showPassword ? "text" : "password"} 
                      placeholder="password" 
                      onChange={this.handlePasswordChange} 
                      name="password"
                      value={this.state.password}
                      // disabled={!this.state.selectedType.includes("password") ? "true" : ""}
                    />
                    <Button className="login-page-eye" onClick={this.handleShowPassword}>
                      {this.state.showPassword
                        ? <span className="fa fa-eye-slash"/>
                        : <span className="fa fa-eye"/>
                      }
                    </Button>
                  </div>                                   
                  {this.state.step === 1 &&
                    <p className="login-page-note">
                      {this.state.note}
                    </p>}
                  { this.handleButtonRender() }
                  {this.state.authFail &&
                    <p className="access-denied">
                      {this.state.error}
                    </p>}                  
                </div>
              </form>
            </div>
          </div>
        </div>
      );
    }

}

// const mapStateToProps = (state) => {
//   return {
//     response: state.auth.response,
//     error: state.auth.error,
//     privileges: state.roles.rolePrivileges
//   };
// };

// export default connect (mapStateToProps, {
//   logIn,
//   getRolePrivilegesAction
// })(LoginPage);

// LoginPage.propTypes = {
//   response: PropTypes.bool,
//   error: PropTypes.string,
//   privileges: PropTypes.array,
//   logIn: PropTypes.func,
//   getRolePrivilegesAction: PropTypes.func,
// };

export default LoginPage;