import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../factories/Main";
import AccountCreator from "./Creator";
import AccountEditor from "./Editor";
import Modal from "components/UI/Modal";
import ability from "config/ability";
import { getAllAccounts, deleteAccountAction } from "actions/accounts";
import { searchInAccounts, reset } from "actions/search";
import { Link } from "react-router-dom";

const columns = [
  {
    path: "guid",
    label: "ID",
    key: "guid",
    content: account => (
      <Link className="link" to={`/about/account/${account.guid}`} >{account.guid}</Link>
    ),
  },
  { path: "number", label: "Number" },
  { path: "currency_name", label: "Currency" },
  { path: "balance", label: "Balance" },
  ability.can("EXECUTE", "ACCOUNTS") &&
  {
    key: "edit",
    content: account => (
      <Modal 
        header="Edit account" 
        content={<AccountEditor account_guid={account.guid} type="account"/>}
        button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }}/>}
      />      
    ),
    label: "Edit"
  },
  ability.can("EXECUTE", "ACCOUNTS") &&
  {
    key: "delete",
    content: () => (
      <i
        className="far fa-trash-alt"
        style={{ cursor: "pointer", color: "red" }}
      />
    ),
    label: "Delete"
  }    
];

const mapStateToProps = (state) => {
  return {
    data: state.accounts.accountsList,
    count: state.accounts.count,
    loading: state.accounts.accountsLoading,
    searchData: state.search.accountsSearch,
    isSearch: state.search.isSearch,
    name:"accounts",
    columns,
    modal: <AccountCreator/>
  };
};

export default connect(mapStateToProps, {
  get: getAllAccounts,
  delete: deleteAccountAction,
  search: searchInAccounts,
  reset
})(AbstractComponent);