import React, { Component } from "react";
import { connect } from "react-redux";
import { getAccountAction, updateAccountAction } from "actions/accounts";
import { getAllCurrencies } from "actions/currencies";
import { showModal } from "actions/modal";
import { parseResponse } from "helpers/parseResponse";
import {
  Col,
  Row,
  Form,
  ControlLabel,
  FormControl,
  Button,
  FormGroup
} from "react-bootstrap";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class AccountEditor extends Component {
  state = {
    number: "",
    balance: 0,
    currency: "",
    currencies: [],
    currencyGuid: "",
    selectedCurrencyValue: "",
    isLoading: false
  };

  async componentDidMount() {
    await this.props.getAccountAction(this.props.account_guid);
    const account = this.props.account;
    await this.props.getAllCurrencies();
    const { currencies } = this.props;
    this.setState({
      number: account.number,
      balance: account.balance,
      currencyGuid: account.currency_guid,
      selectedCurrencyValue: account.currency_guid,
      currency: account.currency_name,
      currencies: currencies
    });
  }

  handleChange = (e) => {
    const property = e.target.name;
    this.setState({ [property]: e.target.value });
  };

  handleCurrencySelect = e => {
    this.setState({
      selectedCurrencyValue: e.target.value,
    });
  }

  formValidation = (data) => {
    if (data === "") return "error";
    else return "success";
  };

  generalValidation = () => {

    return this.formValidation(this.state.number) === "success" &&
      this.formValidation(this.state.currency) === "success";
  }

  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    if (!this.generalValidation()) {
      swal({
        title: "Please, enter information in every required field",
        icon: "warning",
      });
    }
    else {
      try {
        await this.props.updateAccountAction({
          guid: this.props.account_guid,
          number: this.state.number,
          balance: this.state.balance,
          currency_guid: this.state.selectedCurrencyValue
        });
        swal({
          title: "Account is updated",
          icon: "success",
          button: false,
          timer: 2000
        });
        this.props.showModal(false);
      } catch (error) {
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
    }
  }

  render() {
    if (this.props.isLoading) return (<Spinner />);
    else return (
      <div>
        <div className="card">
          <div className="content">
            <label>ID: {this.props.account_guid}</label>
          </div>
        </div>
        <div className="card">
          <Form autoComplete="off">
            <Row className="rowEdit">
              <Col className="col">
                <ControlLabel>Number* </ControlLabel>
                <FormGroup
                  validationState={this.formValidation(this.state.number)}
                >
                  <FormControl
                    name="number"
                    type="number"
                    value={this.state.number}
                    onChange={this.handleChange}
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row className="rowEdit">
              <Col className="col">
                <ControlLabel>Currency* </ControlLabel>
                <FormGroup validationState={this.formValidation(this.state.currency)}>
                  <FormControl
                    name="currencySelect"
                    componentClass="select"
                    onChange={(e) => this.handleCurrencySelect(e)}>
                    <option value={this.state.currencyGuid}>{this.state.currency}</option>
                    {this.state.currencies.filter(currency => currency.name !== this.state.currency).map(currency => {
                      return (<option key={currency.guid} value={currency.guid}>{currency.name}</option>);
                    })
                    }
                  </FormControl>
                </FormGroup>
              </Col>
            </Row>
            <div align="center">
              {
                this.state.isLoading
                  ? <ReactLoading type='cylon' color='grey' />
                  : <Button type="submit" onClick={this.handleSubmit}>
                    Save
                  </Button>
              }
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    account: state.accounts.account,
    currencies: state.currencies.currenciesList,
    accountLoading: state.accounts.accountLoading
  };
};

export default connect(
  mapStateToProps,
  {
    getAccountAction,
    updateAccountAction,
    getAllCurrencies,
    showModal
  }
)(AccountEditor);

AccountEditor.propTypes = {
  isLoading: PropTypes.bool,
  account_guid: PropTypes.string,
  account: PropTypes.array,
  currencies: PropTypes.array,
  getAccountAction: PropTypes.func,
  updateAccountAction: PropTypes.func,
  getAllCurrencies: PropTypes.func,
  showModal: PropTypes.func
};