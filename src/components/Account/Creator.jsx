import React from "react";
import { connect } from "react-redux";
import Joi from "joi-browser";
import CustomForm from "components/UI/Form/index";
import { addAccountAction } from "../../actions/accounts";
import { getAllCurrencies } from "../../actions/currencies";
import { showModal } from "../../actions/modal";
import { Form } from "react-bootstrap";
import PropTypes from "prop-types";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse";
import ReactLoading from "react-loading";

class AccountCreator extends CustomForm {
  state = {
    data: {
      number: "",
      currency: "",
    },
    balance: 0,
    currencies: [],
    errors: {},
    isLoading: false
  };

  schema = {
    number: Joi.number()
      .required()
      .min(0)
      .label("Number"),
    currency: Joi.string()
      .required()
      .min(0)
      .max(100)
      .label("Currency")
  };

  async componentDidMount() {
    await this.props.getAllCurrencies();
    const { currencies } = this.props;
    this.setState({ currencies });
  }

  doSubmit = async () => {
    const { data } = this.state;
    try {
      this.setState({ isLoading: true });
      await this.props.addAccountAction({
        number: data.number,
        balance: this.state.balance,
        currency_guid: data.currency
      }, this.props.currentPage, this.props.pageSize, this.props.accountsSearch);
      this.setState({ isLoading: false });
      swal({
        title: "New account is created",
        icon: "success",
        button:false,
        timer: 2000
      });
      await this.props.showModal(false);
    } catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    } 
  }

  render() {
    return (
      <div className="card">
        <div className="content">
          <Form onSubmit={this.handleSubmit}>
            {this.renderInput("number", "Number", "number")}
            {this.renderSelect("currency", "Currency", this.state.currencies)}
            <div align="center" style={{ marginTop: "20px" }}>
              {this.state.isLoading 
                ? <ReactLoading type='cylon' color='grey' /> 
                : this.renderButton("Add")}
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    accounts: state.accounts.accountsList,
    currencies: state.currencies.currenciesList,
    currentPage: state.accounts.currentPage,
    pageSize: state.accounts.pageSize,

    accountsSearch: state.search.accountsSearch
  };
};

export default connect(
  mapStateToProps,
  {
    getAllCurrencies,
    addAccountAction,
    showModal
  }
)(AccountCreator);

AccountCreator.propTypes = {
  accounts: PropTypes.array,
  currencies: PropTypes.array,
  addAccountAction: PropTypes.func,
  getAllCurrencies: PropTypes.func,
  showModal: PropTypes.func
};