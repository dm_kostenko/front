import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { getAccountAction } from "actions/accounts";
import { getCardsAccountAction } from "actions/cards";
import moment from "moment";
import Table from "components/UI/Table/index";
import Pagination from "components/UI/Pagination/index";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";

class AccountDetail extends Component {
  state = {
    count: "",
    currentPage: 1,
    pageSize: 10
  };

  columnsCards = [
    { path: "type", label: "Type" },
    { path: "card_number", label: "Card number" },
    { path: "currency_name", label: "Currency" },
    { path: "name_on_card", label: "Name on card" }
  ];

  async componentDidMount() {
    await this.props.getAccountAction(this.props.match.params.id);
    await this.props.getCardsAccountAction(this.props.accountInfo.number, 1, this.state.pageSize);
    this.setState({
      count: this.props.count
    });
  }

  handlePageChange = async (page) => {
    await this.props.getCardsAccountAction(this.props.match.params.id, page, this.state.pageSize);
    this.setState({
      count: this.props.count,
      currentPage: page
    });
  };

  getPagedData = () => {
    const { count } = this.state;
    let { pageSize } = this.state;
    const pagesCount = count ? (count / pageSize) + (1 && !!(count % pageSize)) : 0;
    return { pagesCount };
  };


  render() {
    const account = this.props.accountInfo;
    const cards = this.props.cards;
    const { currentPage } = this.state;
    const { pagesCount } = this.getPagedData();
    if (this.props.accountLoading) return <Spinner/>;
    else return (
      <>
      <Button onClick={this.props.history.goBack} className="detail-back-button"><i className="fas fa-arrow-left"></i></Button>
      <div className="content">
        <Row style={{ marginLeft: "0px" }}>
          <div className="card">
            <div className="title">{account ? account.number : null}</div>
          </div>

          <div className="card-container">
            <div className="card">
              <div className="header">Detailed info</div>
              <div className="content">
                <Row>  
                  <Col md={5} lg={5} xs={5} className="right">
                    <label>GUID: </label>
                  </Col>
                  <Col md={5} lg={5} xs={5}>
                    <span>{account.guid}</span>
                  </Col>  
                </Row>
                <Row>
                  <Col md={5} lg={5} xs={5} className="right">
                    <label>Balance:</label>
                  </Col>
                  <Col md={5} lg={5} xs={5}>
                    <span>{account.balance}</span>
                  </Col>
                </Row>
                <Row>
                  <Col md={5} lg={5} xs={5} className="right">
                    <label>Created at:</label>
                  </Col>
                  <Col md={5} lg={5} xs={5}>
                    <span> {account.created_at!==null ? moment(account.created_at).utcOffset(0).format("D MMM YYYY") : ""}</span>
                  </Col>
                </Row>
                <Row>
                  <Col md={5} lg={5} xs={5} className="right">
                    <label>Created by:</label>
                  </Col>
                  <Col md={5} lg={5} xs={5}>
                    <span> {account.created_by}</span>
                  </Col>
                </Row>
                <Row>
                  <Col md={5} lg={5} xs={5} className="right">
                    <label>Updated at:</label>
                  </Col>
                  <Col md={5} lg={5} xs={5}>
                    <span> {account.updated_at!==null ? moment(account.updated_at).utcOffset(0).format("D MMM YYYY") : ""}</span>
                  </Col>
                </Row>

                <Row>
                  <Col md={5} lg={5} xs={5} className="right">
                    <label>Updated by:</label>
                  </Col>
                  <Col md={5} lg={5} xs={5}>
                    <span> {account.updated_by}</span>
                  </Col>
                </Row>
              </div>
            </div>

            <div className="card" style={{ overflow:"auto" }}>
              <div className="header">Cards</div>
              <div className="content">
                <Table
                  columns={this.columnsCards}
                  data={cards}
                  disableSearch={true}
                />
                <Pagination
                  pagesCount={pagesCount}
                  currentPage={currentPage}
                  onPageChange={this.handlePageChange}
                />
              </div>
            </div>
          </div>
        </Row>
      </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    accountInfo: state.accounts.account,
    cards: state.cards.accountCards,
    count: state.cards.accountCardsCount,
    accountLoading: state.accounts.accountLoading
  };
};

export default connect(mapStateToProps, {
  getAccountAction,
  getCardsAccountAction
})(AccountDetail);

AccountDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  accountInfo: PropTypes.object,
  cards: PropTypes.array,
  count: PropTypes.number,
  getAccountAction: PropTypes.func,
  getCardsAccountAction: PropTypes.func,
  accountLoading: PropTypes.bool,
};