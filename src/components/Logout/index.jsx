import React, { Component } from "react";
import { connect } from "react-redux";
import { logOut } from "actions/auth";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";

class LogOut extends Component {
  componentDidMount() {
    // await this.props.logOut();
    localStorage.clear();
    window.location.replace(process.env.PUBLIC_URL || "/");
  }

  render() {
    return (
      <div className="LoginPage">
        <div className="login-page">
          <Spinner/>
        </div>  
      </div>
    );
  }
}
// export default connect(null, {
//   logOut
// })(LogOut);

// LogOut.propTypes = {
//   logOut: PropTypes.func,
// };

export default LogOut;