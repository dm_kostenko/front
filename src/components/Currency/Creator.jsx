import React from "react";
import { connect } from "react-redux";
import Joi from "joi-browser";
import CustomForm from "components/UI/Form";
import { Form } from "react-bootstrap";
import { addCurrencyAction } from "actions/currencies";
import { showModal } from "actions/modal";
import PropTypes from "prop-types";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse";
import ReactLoading from "react-loading";

class CurrencyCreator extends CustomForm {
  state = {
    data: {
      name: "",
      code: "",
    },
    errors: {},
    isLoading: false
  };

  schema = {
    name: Joi.string()
      .required()
      .min(0)
      .max(100)
      .label("Name"),
    code: Joi.string()
      .required()
      .min(0)
      .max(100)
      .label("Code"),
  };

  doSubmit = async () => {
    this.setState({ isLoading: true });
    try {
      const { name, code } = this.state.data;
      await this.props.addCurrencyAction({ name, code }, this.props.currentPage, this.props.pageSize);
      swal({
        title: "Currency is created",
        icon: "success",
        button: false,
        timer: 2000
      });
      await this.props.showModal(false);
    } catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  render() {
    return (
      <div className="card">
        <div className="content">
          <Form onSubmit={this.handleSubmit}>
            {this.renderInput("name", "Name")}
            {this.renderInput("code", "Code")}
            <div align="center">
              {this.state.isLoading
                ? <ReactLoading type='cylon' color='grey' />
                : this.renderButton("Add")}
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  {
    addCurrencyAction,
    showModal
  }
)(CurrencyCreator);

CurrencyCreator.propTypes = {
  addCurrencyAction: PropTypes.func,
  showModal: PropTypes.func
};