import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../factories/Main";
import CurrencyCreator from "./Creator";
import ability from "config/ability";
import { getAllCurrencies, deleteCurrencyAction } from "../../actions/currencies";
import { searchInCurrencies, reset } from "../../actions/search";
import Modal from "components/UI/Modal";
import CurrencyEditor from "./Editor";

const columns = [
  { path: "guid", label: "ID" },
  { path: "name", label: "Name" },
  { path: "code", label: "Code" },
  ability.can("EXECUTE", "CURRENCIES") &&
        {
          key: "edit",
          content: currency => (
            <Modal
              header="Edit currency" 
              action="Save" 
              content={<CurrencyEditor guid={currency.guid} type="currency"/>}
              button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }}/>} />
          ),
          label: "Edit"
        },
  ability.can("EXECUTE", "CURRENCIES") &&
        {
          key: "delete",
          content: () => (
            <i
              className="far fa-trash-alt"
              style={{ cursor: "pointer", color: "red" }}
            />
          ),
          label: "Delete"
        }
];

const mapStateToProps = (state) => {
  return {
    data: state.currencies.currenciesList,
    count: state.currencies.count,
    loading: state.currencies.currenciesLoading,
    searchData: state.search.currenciesSearch,
    isSearch: state.search.isSearch,
    name:"currencies",
    columns,
    modal: <CurrencyCreator/>
  };
};

export default connect(mapStateToProps, {
  get: getAllCurrencies,
  delete: deleteCurrencyAction,
  search: searchInCurrencies,
  reset
})(AbstractComponent);
