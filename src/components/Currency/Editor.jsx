import React, { Component } from "react";
import { connect } from "react-redux";
import { getCurrencyAction, editCurrencyAction } from "actions/currencies";
import { showModal } from "actions/modal";
import { parseResponse } from "helpers/parseResponse";
import {
  Col,
  Form,
  ControlLabel,
  FormControl,
  Button,
  FormGroup
} from "react-bootstrap";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class CurrencyEditor extends Component {
  state = {
    name: "",
    code: "",
    isLoading: false
  };

  async componentDidMount() {
    await this.props.getCurrencyAction(this.props.guid);
    const currency = this.props.currency;
    this.setState({
      name: currency.name,
      code: currency.code,
    });
  }

  handleChange = (e) => {
    const property = e.target.name;
    this.setState({ [property]: e.target.value });
  };

  formValidation = (data) => {
    if (data === "") return "error";
    else return "success";
  };

  generalValidation = () => {

    return this.formValidation(this.state.name) === "success" &&
      this.formValidation(this.state.code) === "success";
  }

  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    if (!this.generalValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    }
    try {
      await this.props.editCurrencyAction({
        guid: this.props.guid,
        name: this.state.name,
        code: this.state.code,
      });
      swal({
        title: "Currency is updated",
        icon: "success",
        button:false,
        timer: 2000
      });
      this.props.showModal(false);
    } catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  }

  render() {
    if (this.props.isLoading) return (
      <Spinner />
    );
    else return (
      <div>
        <div className="card">
          <div className="content">
            <label>ID: {this.props.guid}</label>
          </div>
        </div>
        <div className="card">
          <Form autoComplete="off">
            <Col sm={1} />
            <Col sm={12}>

              <Col className="col">
                <ControlLabel>Name* </ControlLabel>
                <FormGroup
                  validationState={this.formValidation(this.state.name)}
                >
                  <FormControl
                    name="name"
                    type="string"
                    value={this.state.name}
                    onChange={this.handleChange}
                  />
                </FormGroup>
              </Col>
              <Col className="col">
                <ControlLabel>Code* </ControlLabel>
                <FormGroup
                  validationState={this.formValidation(this.state.code)}
                >
                  <FormControl
                    name="code"
                    type="string"
                    value={this.state.code}
                    onChange={this.handleChange}
                  />
                </FormGroup>
              </Col>
            </Col>
            <Col sm={1} />
          </Form>
          <div align="center">
            {this.state.isLoading 
              ? <ReactLoading type='cylon' color='grey' />
              : <Button type="submit" onClick={this.handleSubmit}>
              Save
              </Button>}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    currency: state.currencies.currency,
    isLoading: state.currencies.currencyLoading,
    error: state.currencies.error
  };
};

export default connect(
  mapStateToProps,
  {
    getCurrencyAction,
    editCurrencyAction,
    showModal
  }
)(CurrencyEditor);

CurrencyEditor.propTypes = {
  isLoading: PropTypes.bool,
  currency: PropTypes.array,
  guid: PropTypes.string,
  getCurrencyAction: PropTypes.func,
  editCurrencyction: PropTypes.func,
  showModal: PropTypes.func,
  editCurrencyAction: PropTypes.func,
};