import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import Table from "components/UI/Table/index";
import moment from "moment";
import { connect } from "react-redux";
import Pagination from "components/UI/Pagination/index";
import { getRoleAction, getRolePrivilegesAction } from "actions/roles";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";

class RoleDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      pageSize: 10
    };
  }


  columnsRolePrivileges = [
    { path: "guid", label: "Guid" },
    { path: "name", label: "Name" },
    { path: "description", label: "Description" }
  ];


  componentDidMount = async () => {
    await this.props.getRolePrivilegesAction(this.props.match.params.id, 1, this.state.pageSize);
    await this.props.getRoleAction(this.props.match.params.id);
  };


  handlePageChange = async (page) => {
    await this.props.getRolePrivilegesAction(this.props.match.params.id, page, this.state.pageSize);
    this.setState({
      currentPage: page
    });
  };


  getPagedData = () => {
    const { pageSize } = this.state;
    const { count } = this.props;
    const rolePrivileges = this.props.rolePrivileges;
    const pagesCount = (count / pageSize) + (1 && !!(count % pageSize));
    return { pagesCount, data: rolePrivileges };
  };

  render() {
    const role = this.props.role;
    const { currentPage } = this.state;
    const { pagesCount, data: rolePrivileges } = this.getPagedData();
    if (this.props.roleLoading || this.props.privilegesLoading) return <Spinner/>;
    else return (
      <>
      <Button onClick={this.props.history.goBack} className="detail-back-button"><i className="fas fa-arrow-left"></i></Button>
      <div className="content" style={{ overflow:"auto" }}>
        <Row style={{ marginLeft: "0px" }}>
          <div className="card">
            <div className="title">{role.name}</div>
          </div>
          <div className="card">
            <div className="header">Role Info</div>
            <div className="content">
              <Row>
                <Col md={6} lg={6} xs={6} className="right"><label>GUID:</label></Col>
                <Col md={6} lg={6} xs={6}><span>{this.props.match.params.id}</span></Col>
              </Row>
              <Row>
                <Col md={6} lg={6} xs={6} className="right"><label>Description:</label></Col>
                <Col md={6} lg={6} xs={6}><span>{role.description}</span></Col>
              </Row>
              <Row>
                <Col md={6} lg={6} xs={6} className="right"><label>Created at:</label></Col>
                <Col md={6} lg={6} xs={6}><span>{role.created_at !== null ? moment(role.created_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
              </Row>
              <Row>
                <Col md={6} lg={6} xs={6} className="right"><label>Created by:</label></Col>
                <Col md={6} lg={6} xs={6}><span>{role.created_by}</span></Col>
              </Row>
              <Row>
                <Col md={6} lg={6} xs={6} className="right"><label>Updated at:</label></Col>
                <Col md={6} lg={6} xs={6}><span>{role.updated_at !== null ? moment(role.updated_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
              </Row>
              <Row>
                <Col md={6} lg={6} xs={6} className="right"><label>Updated by:</label></Col>
                <Col md={6} lg={6} xs={6}><span>{role.updated_by}</span></Col>
              </Row>
            </div>
          </div>

          <div className="card">
            <div className="header">Privileges</div>
            <div className="content">
              <Table
                columns={this.columnsRolePrivileges}
                data={rolePrivileges ? rolePrivileges : null}
                disableSearch={true}
              />
              <Pagination
                pagesCount={pagesCount}
                currentPage={currentPage}
                onPageChange={this.handlePageChange}
              />
            </div>
          </div>
        </Row>
      </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    role: state.roles.role,
    rolePrivileges: state.roles.rolePrivileges,
    count: state.roles.privilegesCount,
    roleLoading: state.roles.roleLoading,
    privilegesLoading: state.roles.rolePrivilegesLoading
  };
};

export default connect(mapStateToProps, { getRoleAction, getRolePrivilegesAction })(RoleDetail);

RoleDetail.propTypes = {
  role: PropTypes.object,
  rolePrivileges: PropTypes.array,
  count: PropTypes.number,
  roleLoading: PropTypes.bool,
  privilegesLoading: PropTypes.bool,

  getRoleAction: PropTypes.func,
  getRolePrivilegesAction: PropTypes.func,

  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    })
  })
};