import React, { Component } from "react";
import { Col, Row, ControlLabel, FormControl, FormGroup, Button } from "react-bootstrap";
import MultiSelect from "components/UI/MultiSelect/index";
import { connect } from "react-redux";
import { addRoleAction, addRolePrivilegeAction } from "actions/roles";
import { getPrivilegesWithType } from "../../actions/privileges";
import { showModal } from "actions/modal";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class RoleCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      description: "",
      selectedPrivileges: [],
      selectedType: "",
      isLoading: false
    };
  }

  generalValidation = () => {
    if (this.formValidation(this.state.name) === "success" &&
      this.formValidation(this.state.description) === "success" &&
      this.formValidation(this.state.selectedType) === "success"
    ) return true;
    else return false;
  }

  formValidation = (data) => {
    if (data === "" || typeof (data) === "undefined") return "error";
    else return "success";
  }


  onNameChange = (e) => {
    this.setState({
      name: e.target.value
    });
  };

  onDescriptionChange = (e) => {
    this.setState({
      description: e.target.value
    });
  };


  onSelectPrivilege = (option) => {
    this.setState({
      selectedPrivileges: option
    });
  };

  onSelectType = async (option) => {
    this.setState({
      selectedType: option
    });
    await this.props.getPrivilegesWithType(option.name);
  };

  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    const { name, description, selectedPrivileges, selectedType } = this.state;
    if (this.generalValidation() && selectedPrivileges.length > 0) {
      try {
        await this.props.addRoleAction({
          name,
          type: selectedType.name,
          description
        }, this.props.currentPage, this.props.pageSize, this.props.rolesSearch);
        const roleGuid = this.props.role_guid;
        let data = [];
        for (let i = 0; i < selectedPrivileges.length; i += 1) {
          const privilege = {
            guid: selectedPrivileges[i].guid
          };
          data = [ ...data, privilege ];
        }
        await this.props.addRolePrivilegeAction(roleGuid, data);
        swal({
          title: "Role is created",
          icon: "success",
          button: false,
          timer: 2000
        });
        await this.props.showModal(false);
      }
      catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
    }
    else {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    }
  };

  render() {
    const { privileges } = this.props;

    const types = [
      {
        guid: "0",
        name: "merchant"
      },
      {
        guid: "1",
        name: "group"
      },
      {
        guid: "2",
        name: "partner"
      },      
      {
        guid: "3",
        name: "system"
      }      
    ];
    
    return (
      <div className="card" style={{ padding:"15px" }}>
        <Row>
          <Col md={3}>
            <ControlLabel>Name*</ControlLabel>
          </Col>
          <Col md={9}>
            <FormGroup validationState={this.formValidation(this.state.name)}>
              <FormControl
                name="name"
                type="text"
                value={this.state.name}
                onChange={this.onNameChange}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={3}>
            <ControlLabel>Type*</ControlLabel>
          </Col>
          <Col md={9}>
            <FormGroup>
              <MultiSelect
                name="type"
                options={types}
                multi={false}
                onSelect={this.onSelectType}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={3}>
            <ControlLabel>Description*</ControlLabel>
          </Col>
          <Col md={9}>
            <FormGroup validationState={this.formValidation(this.state.description)}>
              <FormControl
                name="description"
                type="text"
                value={this.state.description}
                onChange={this.onDescriptionChange}
              />
            </FormGroup>
          </Col>
        </Row>        
        {this.state.selectedType &&
        <Row>
          <Col md={3}>
            <ControlLabel>Privileges*</ControlLabel>
          </Col>
          <Col md={9}>
            <FormGroup>
              <MultiSelect
                name="Roles"
                options={privileges ? privileges : []}
                multi={true}
                onSelect={this.onSelectPrivilege}
              />
            </FormGroup>
          </Col>
        </Row>}
        <div align="center">
          {this.state.isLoading
            ? <ReactLoading type='cylon' color='grey' />
            : <Button type="submit" onClick={this.handleSubmit} style={{ marginTop: "10px" }}>Add</Button>}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    role_guid: state.roles.role_guid,
    privileges: state.privileges.privilegesList,
    currentPage: state.roles.currentPage,
    pageSize: state.roles.pageSize,

    rolesSearch: state.search.rolesSearch
  };
};

export default connect(mapStateToProps,
  {
    showModal,
    addRoleAction,
    addRolePrivilegeAction,
    getPrivilegesWithType
  })(RoleCreator);

RoleCreator.propTypes = {
  privileges: PropTypes.array,
  role_guid: PropTypes.string,

  addRoleAction: PropTypes.func,
  addRolePrivilegeAction: PropTypes.func,
  getPrivilegesWithType: PropTypes.func,
  showModal: PropTypes.func,

  currentPage: PropTypes.number,
  pageSize: PropTypes.number,
  rolesSearch: PropTypes.array,
};