import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../factories/Main";
import RoleCreator from "./Creator";
import ability from "config/ability";
import { getAllRoles, deleteRoleAction } from "../../actions/roles";
import { searchInRoles, reset } from "../../actions/search";
import RoleEditor from "./Editor";
import Modal from "components/UI/Modal";
import { Link } from "react-router-dom";

const columns = [
  {
    path: "guid",
    label: "ID",
    key: "guid",
    content: role => (
      <Link className="link" to={`/about/role/${role.guid}`}>{role.guid}</Link>
    )
  },
  { path: "name", label: "Name" },
  { path: "type", label: "Type" },
  { path: "description", label: "Description" },
  ability.can("EXECUTE", "ROLES") &&
  {
    key: "edit",
    content: role => (
      <Modal 
        header="Edit role" 
        button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }}/>} 
        content={<RoleEditor role_guid={role.guid} />} />
    ),
    label: "Edit"
  },
  ability.can("EXECUTE", "ROLES") &&
  {
    key: "delete",
    content: () => (
      <i
        className="far fa-trash-alt"
        style={{ cursor: "pointer", color: "red" }}
      />
    ),
    label: "Delete"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.roles.rolesList,
    count: state.roles.count,
    loading: state.roles.rolesLoading,
    searchData: state.search.rolesSearch,
    isSearch: state.search.isSearch,
    name: "roles",
    columns,
    modal: <RoleCreator />
  };
};

export default connect(mapStateToProps, {
  get: getAllRoles,
  delete: deleteRoleAction,
  search: searchInRoles,
  reset
})(AbstractComponent);