
import React, { Component } from "react";
import { Col, Row, Form, ControlLabel, FormControl, Button, FormGroup, Badge } from "react-bootstrap";
import RolePrivilegesManagement from "./PrivilegesManagement";
import { connect } from "react-redux";
import { getRoleAction, updateRoleAction, upsertPrivilegeToRoleAction } from "../../actions/roles";
import { showModal } from "../../actions/modal";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class RoleEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        guid: "",
        name: "",
        description: "",
        type: "",
        changedPrivileges: []        
      },
      selectedType: {},
      isLoading: false
    };
  }

  componentDidMount = async () => {
    const roleGuid = this.props.role_guid;
    await this.props.getRoleAction(roleGuid);
    this.setState({
      data: this.props.role
    });
  }

  handleChange = ({ currentTarget: input }) => {
    const data = { ...this.state.data };
    data[input.name] = input.value;

    this.setState({ data });
  };

  formValidation = data => {
    if (data === "") return "error";
    else return "success";
  };

  generalValidation = () => {
    const { data } = this.state;
    return this.formValidation(data.name) === "success" &&
      this.formValidation(data.description) === "success" &&
      this.formValidation(data.type) === "success";
  };


  handleSave = async changedPrivileges => {
    const data = { ...this.state.data };
    data.changedPrivileges = await changedPrivileges;
    this.setState({ data });
  };


  handleSubmit = async event => {
    this.setState({ isLoading: true });
    event.preventDefault();
    const { data } = this.state;
    if (!this.generalValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    }    
    else {
      try {
        await this.props.updateRoleAction({
          guid: data.guid,
          name: data.name,
          type: data.type,
          description: data.description,
        });
        data.changedPrivileges = await this.props.rolePrivileges;
        this.setState({ data });
        if (data.changedPrivileges) {
          const req = {
            guid: data.guid,
            privileges: data.changedPrivileges
          };
          await this.props.upsertPrivilegeToRoleAction(req);
        }
        swal({
          title: "Role is updated",
          icon: "success",
          button:false,
          timer: 2000
        });
        await this.props.showModal(false);
      }
      catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
    }
  };

  render() {
    const { data } = this.state;

    if (this.props.roleLoading) {
      return <Spinner />;
    }
    else return (
      <div>
        <div className="card">
          <div className="content">
            <label>ID: {this.props.role_guid}</label>
          </div>
        </div>
        <div className="card">
          <div className="header">General info</div>

          <Form onSubmit={this.handleSubmit} autoComplete="off">
            <Row className="rowEdit" >
              <Col md={3} lg={3} xs={3}>
                <ControlLabel>Name:</ControlLabel>
              </Col>
              <Col md={9} lg={9} xs={9}>
                <FormGroup validationState={this.formValidation(data.name)}>
                  <FormControl
                    name="name"
                    type="text"
                    value={data.name}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row className="rowEdit">
              <Col md={3} lg={3} xs={3}>
                <ControlLabel>Type:</ControlLabel>
              </Col>
              <Col md={2} lg={2} xs={2}>
                <FormGroup validationState={this.formValidation(data.type)}>
                  <Badge className="badge-role-type">
                    {this.state.data.type.toUpperCase()}
                  </Badge>
                </FormGroup>
              </Col>
            </Row>
            <Row className="rowEdit">
              <Col md={3} lg={3} xs={3}>
                <ControlLabel>Description:</ControlLabel>
              </Col>
              <Col md={9} lg={9} xs={9}>
                <FormGroup validationState={this.formValidation(data.description)}>
                  <FormControl
                    name="description"
                    type="text"
                    value={data.description}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>            
            <Row className="rowEdit">
              <Col md={3} lg={3} xs={3}>
                <ControlLabel>Privileges:</ControlLabel>
              </Col>
              <Col md={9} lg={9} xs={9}>
                <RolePrivilegesManagement 
                  roleGuid={this.props.role_guid}
                  role={this.props.role}
                />
              </Col>
            </Row>
            <div align="center">
              {this.state.isLoading 
                ? <ReactLoading type='cylon' color='grey' />
                : <Button style={{ "margin": "10px" }} type="submit" onClick={this.handleSubmit}>Save</Button>}
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    role: state.roles.role,
    rolePrivileges: state.roles.changedPrivileges,
    roleLoading: state.roles.loading
  };
};

export default connect(mapStateToProps, {
  getRoleAction,
  updateRoleAction,
  upsertPrivilegeToRoleAction,
  showModal,
})(RoleEditor);

RoleEditor.propTypes = {
  role: PropTypes.array,
  rolePrivileges: PropTypes.array,
  roleLoading: PropTypes.bool,
  role_guid: PropTypes.bool,
  getRoleAction: PropTypes.func,
  updateRoleAction: PropTypes.func,
  upsertPrivilegeToRoleAction: PropTypes.func,
  showModal: PropTypes.func,
  getRolePrivilegesAction: PropTypes.func,
  getAllPrivileges: PropTypes.func
};
