import React, { Component } from "react";
import { connect } from "react-redux";
import { getTotalProcessedAction } from "../../actions/transactions";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";

class ShopTotalProcessed extends Component {
  state = {
    daysCount: 7
  }

  componentDidMount = async () => {
    await this.props.getTotalProcessedAction({ days:this.state.daysCount, shop_guid: this.props.shopGuid });
  }

  render() {
    const { loading, shopReports, classname } = this.props;
    const [ shop ] = shopReports ? shopReports.length ? shopReports.filter(({ guid }) => guid === this.props.shopGuid) : [] : [];

    if (loading) return <Spinner/>;
    else 
      return (
        <div>
          <div className="header">Amount of money processed</div>
          <div className={classname}>$ {shop ? shop.amount : 0}</div>
        </div>
      );
  }
}

const mapStateToProps = (state) => {
  return {
    shopReports: state.transactions.shopReports,
    loading: state.transactions.totalShopProcessedLoading
  };
};

export default connect(mapStateToProps, {
  getTotalProcessedAction
})(ShopTotalProcessed);

ShopTotalProcessed.propTypes = {
  shopReports: PropTypes.array,
  shopGuid: PropTypes.string,
  getTotalProcessedAction: PropTypes.func,
  loading: PropTypes.bool,
  classname: PropTypes.string
};