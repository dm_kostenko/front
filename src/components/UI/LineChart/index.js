import React from "react";
import { Line } from "react-chartjs-2";
import PropTypes from "prop-types";
import { ExportButton } from "components/ExportButton";

export const LineChart = (props) => {
  const data = {
    labels: props.labels,
    datasets: props.datasets
  };
  
  return (
    <>
    <Line
      data={data}
      height={100}
      options={{ maintainAspectRation: false, ...props.options }}
    />
    <ExportButton
      labels={props.labels}
      datasets={props.datasets}
      name={props.name}
    >
      export
    </ExportButton>
    </>
  );
};

LineChart.propTypes = {
  name: PropTypes.string,
  options: PropTypes.object,
  height: PropTypes.number,
  labels: PropTypes.array,
  datasets: PropTypes.array,
};