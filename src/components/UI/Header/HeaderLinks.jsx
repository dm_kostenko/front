import React, { Component } from "react";
import { NavItem, Nav, NavDropdown } from "react-bootstrap";
// import { getAuthData, flushTokens } from "../../../services/paymentBackendAPI/backendPlatform";
import { NavLink } from "react-router-dom";
import { sidebarRoutes } from "routes/dashboard.jsx";

class HeaderLinks extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tokenTtl: 0,
    };
  }

  async componentDidMount() {
    // const token = getAuthData();
    // if (token && token.exp) {
    //   setInterval(() => {
    //     let exp = new Date((token.exp - Math.round(+new Date() / 1000)) * 1000);
    //     if (exp.getMinutes() === 0 & exp.getSeconds() === 0) {
    //       flushTokens();
    //     }
    //     this.setState({ tokenTtl: exp });
    //   }
    //   , 1000);
    // }   
  }

  getTokenTime = () => {
    return (
      <span
        style={
          this.state.tokenTtl.getMinutes() < 1 
            ? {
              color: "red",
              fontWeight: "bold"
            }
            : {}
        }
      >
        {this.state.tokenTtl.getMinutes()}:{(this.state.tokenTtl.getSeconds() < 10 ? "0" : "") + this.state.tokenTtl.getSeconds()}
      </span>
    );
  }

  render() {
    // const username = localStorage.getItem("username");
    return (
      <div>
        <a 
          id={"header-title"}
        >
          IBank
        </a>
        <Nav
          id="header-items"
        >
          {sidebarRoutes.map((item, key) => {
            if(item.collapse && item.views.length)
              return (
                <NavDropdown
                  key={key}                  
                  id={"header-item-collapse"}
                  title={item.name}
                  use
                >
                  {item.views.map((childItem, childKey) => {
                    return (
                      <NavItem
                        key={childKey}
                      >
                        <NavLink                              
                          // id={"header-link"}         
                          title={childItem.name}    
                          to={childItem.path} 
                        >
                          {childItem.name}
                        </NavLink>
                      </NavItem>
                    );
                  })}                  
                </NavDropdown>
              );
            if(!item.redirect)
              return (
                <NavItem              
                  key={key}                  
                  id={"header-item"}
                >
                  <NavLink      
                    id={"header-link"}         
                    title={item.name}    
                    to={item.path}            
                  >
                    {item.name}
                  </NavLink>
                </NavItem>
              );
          })}
        </Nav>
        <Nav pullRight>          
          {this.state.tokenTtl instanceof Date
            ? <NavItem eventKey="disabled" disabled>
              {"Your session will expire in "}
              {this.getTokenTime()}
            </NavItem>
            : ""}
          {/* <NavItem 
            id="profile"
            eventKey={1} 
            className="nav-link"   
            onClick={() => window.location = process.env.PUBLIC_URL+"#/useraccount"}>
            <i
              className="fas fa-user-circle"
            />
            {` ${username || ""}`}
          </NavItem> */}
          <NavItem
            eventKey={3}
            className="nav-link"
            onClick={() => window.location.replace(process.env.PUBLIC_URL+"#/logout")}
          >
            <a id="logout-button">LOG OUT</a>
          </NavItem>
        </Nav>
      </div>
    );
  }
}

export default HeaderLinks;
