import { successIcon } from "constants/statusIcons/successIcon";
import { pendingIcon } from "constants/statusIcons/pendingIcon";
import { failedIcon } from "constants/statusIcons/failedIcon";
import { waitingIcon } from "constants/statusIcons/waitingIcon";

export const StatusIcon = (name, status, error) =>
  [ "Success", "Enabled" ].includes(status)
    ? successIcon(name, status, error)
    : [ "Pending", "Started" ].includes(status)
      ? pendingIcon(name, status, error)
      : [ "Failed", "Disabled" ].includes(status)
        ? failedIcon(name, status, error)
        : status === "Waiting"
          ? waitingIcon(name, status, error)
          : null;