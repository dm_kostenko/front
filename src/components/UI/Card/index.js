import React from "react";
import { CardBody } from "./Body";
import { CardHeader } from "./Header";
import PropTypes from "prop-types";

export const Card = (...props) => {
  return (
    <div className={"card"}>
      <CardHeader
        data={props.header}
      />
      <CardBody
        data={props.body}
      />
    </div>
  );
};

Card.propTypes = {
  header: PropTypes.node,
  body: PropTypes.node,
};