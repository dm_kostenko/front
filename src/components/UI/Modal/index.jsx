import React from "react";
import { Modal } from "react-bootstrap";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { showModal, resetModalData } from "actions/modal";

class ModalCreator extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      modalIsOpen: false,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.modalState !== prevState.modalIsOpen && !nextProps.modalState) {
      return {
        modalIsOpen: nextProps.modalState
      };
    }
    return null;
  }

  handleOpen = async () => {
    await this.props.showModal(true);
    this.setState({
      modalIsOpen: true
    });
  }


  handleClose = async () => {
    await this.props.showModal(false);
    await this.props.resetModalData();
  }

  render() {
    return (
      <React.Fragment>
        <span onClick={() => this.handleOpen()}>
          {this.props.button}
        </span>
        <div>
          <Modal show={this.state.modalIsOpen} onHide={this.handleClose} dialogClassName={this.props.dialogClassName ? this.props.dialogClassName : ""}>
            <Modal.Header closeButton>
              <Modal.Title>{this.props.header}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{this.props.content}</Modal.Body>
          </Modal>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  modalState: state.modal.modalState
});

export default connect(mapStateToProps, {
  showModal,
  resetModalData
})(ModalCreator);

ModalCreator.propTypes = {
  content: PropTypes.node,
  header: PropTypes.string,
  button: PropTypes.node,
  showModal: PropTypes.func,
  resetModalData: PropTypes.func,
  modalState: PropTypes.string,
  dialogClassName: PropTypes.string,
};