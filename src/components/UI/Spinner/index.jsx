import ReactLoading from "react-loading";
import React from "react";


const Spinner = () => {
  return ( 
    <div className="loading">
      <ReactLoading type='cylon' color='grey'/>
    </div>
  );
};
 
export default Spinner;
