import React, { Component } from "react";
import TableHeader from "./Header/index";
import TableBody from "./Body";
import TableEmptyBody from "./EmptyBody";
import { sortBy } from "lodash";
import PropTypes from "prop-types";

class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sortKey: "",
      isSortReverse: false,
      loading: true
    };
  }

  componentDidUpdate(prevProps) {
    if(prevProps.data !== this.props.data)
      setTimeout(() => this.setState({ loading: false }), 500);
  }

  deleteEmptyColumns = (columns) => {
    return columns.filter(column => column);
  }

  render() {
    const columns = this.deleteEmptyColumns(this.props.columns);
    const { data } = this.props;
    const { sortKey, isSortReverse } = this.state;
    const sortedList = sortBy(data, sortKey);
    const reverseSortedList = isSortReverse
      ? sortedList.reverse()
      : sortedList;

    if ((data && !data.length) || !data)
      return (
        <table className="table card card-grid">
          <TableHeader 
            columns={columns} 
            onSort={this.onSort} 
            sortKey={sortKey} 
            isSortReverse={isSortReverse} 
            search={this.props.search} 
            name={this.props.name}
            disableSearch={this.props.disableSearch} 
          />
          <TableEmptyBody/>
        </table>
      );
    return (
      <table className="table card card-grid tab">
        <TableHeader 
          columns={columns} 
          onSort={this.onSort} 
          sortKey={sortKey} 
          isSortReverse={isSortReverse} 
          search={this.props.search} 
          name={this.props.name}
          disableSearch={this.props.disableSearch} 
        />
        <TableBody 
          columns={columns} 
          data={reverseSortedList} 
        />
      </table>
    );
  }
}

export default Table;

Table.propTypes = {
  columns: PropTypes.array,
  data: PropTypes.array,
  search: PropTypes.func,
  name: PropTypes.string,
  disableSearch: PropTypes.bool,
};