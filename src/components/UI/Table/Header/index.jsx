import React, { Component } from "react";
import { connect } from "react-redux";
import Search from "../../Search/index";
import { inverseSearch } from "../../../../actions/search";
import StatusSelector from "../../StatusSelector";
import MethodSelector from "../../MethodSelector";
import PropTypes from "prop-types";

class TableHeader extends Component {

  handleClick = async() => {
    await this.props.inverseSearch(!this.props.isSearch);
  }

  render() {

    const notSearchLabels = [
      "Edit", 
      "Delete", 
      "Body", 
      "Request",
      "Response", 
      "Parameters",
      "Headers",
      "Description", 
      "Params", 
      "Steps",
      "Created by",
      "Message",
      "Datetime",
      "View"
    ];

    return (
      <thead
        id={"table-header"}
      >
      
        {!this.props.disableSearch &&
        <button
          className="search-wrap" 
          onClick={this.handleClick}
        >
          <i className={this.props.isSearch ? "fas fa-search-minus" : "fas fa-search-plus"}/>
        </button>}
        <tr style={{ textAlign: "center" }}>
          {this.props.columns.map(column => (
            <th key={column.path || column.key} style={{ textAlign: "center", color: "black", fontWeight: "bold" }}>
              <button
                //onClick={() => this.props.onSort(column.path)}
                className="button-inline "
              >
                {column.label}
                {this.props.sortKey === column.path ?
                  <i className={this.props.isSortReverse === true ? "fas fa-angle-down" : "fas fa-angle-up"} />
                  : null
                }
              </button>
            </th>
          ))}
        </tr>
        {!this.props.disableSearch &&
        <tr>
          {this.props.columns.map(column => (
            <th
              key={column.path || column.key}
              className={this.props.isSearch ? "searchOpen" : "searchClosed"}
              style={{ 
                textAlign: "center"
                //height: "50px" 
              }} 
            >
              {notSearchLabels.includes(column.label)
                ? null
                : [ "createdAt", "date", "created_at" ].includes(column.path)
                  ? <div
                    id="search-wrapper"
                    className={this.props.isSearch ? "searchOpen search" : "searchClosed search"}
                  >
                    From: <Search type="datetime-local" value={"from_date"} search={this.props.search} />
                    To: <Search type="datetime-local" value={"to_date"} search={this.props.search} />
                  </div>
                  : [ "From", "To" ].includes(column.label) 
                    ? <Search type="datetime-local" value={column.path} search={this.props.search} />
                    : column.label === "Status"
                      ? <StatusSelector type={this.props.name} value={column.path || column.key} search={this.props.search} /> 
                      : column.label === "Method" 
                        ? <MethodSelector value={column.path || column.key} search={this.props.search} />
                        : <Search value={column.path || column.key} search={this.props.search} />
              }
            </th>
          ))}
        </tr>}
      </thead>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isSearch: state.search.isSearch
  };
};

export default connect(mapStateToProps, {
  inverseSearch
})(TableHeader);

TableHeader.propTypes = {
  columns: PropTypes.array,
  sortKey: PropTypes.string,
  isSortReverse: PropTypes.bool,
  onSort: PropTypes.func,
  inverseSearch: PropTypes.func,
  isSearch: PropTypes.bool,
  disableSearch: PropTypes.bool,
  search: PropTypes.func,
  name: PropTypes.string
};