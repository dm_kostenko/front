import React from "react";
import {
  Col,
  ControlLabel,
  FormControl,
  FormGroup
} from "react-bootstrap";
import PropTypes from "prop-types";

const Input = ({ name, label, error, formValidation, ...rest }) => {
  return (
    <Col className="col">
      <ControlLabel htmlFor={name}>{label}</ControlLabel>
      <FormGroup validationState={formValidation(error)}>
        <FormControl
          {...rest}
          name={name}
          id={name}
        />
        {error && <span className="validate-error">{error}</span>}
      </FormGroup>
    </Col>
  );
};

export default Input;

Input.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  error: PropTypes.string,
  formValidation: PropTypes.array,
};

