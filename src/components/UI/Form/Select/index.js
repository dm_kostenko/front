import React from "react";
import {
  Col,
  ControlLabel,
  FormControl,
  FormGroup
} from "react-bootstrap";
import PropTypes from "prop-types";

const Select = ({ name, label, options, error, formValidation, ...rest }) => {
  return ( 
    <Col className="col">
      <ControlLabel>{label}</ControlLabel>
      <FormGroup validationState={formValidation(error)}>
        <FormControl
          {...rest}
          name={name}
          id={name}
          componentClass="select"
        >
          <option value="">{options.name}</option>
          {options.map(option => (
            <option key={option.guid} value={option.guid}>
              {option.name}
            </option>
          ))}
        </FormControl>
        {error && <span className="validate-error">{error}</span>}
      </FormGroup>
    </Col>
  );
};
 
export default Select;

Select.propTypes = {
  name: PropTypes.node,
  label: PropTypes.string,
  options: PropTypes.array,
  error: PropTypes.string,
  formValidation: PropTypes.array,
};
