import React from "react";
import Select from "react-select";
import makeAnimated from "react-select/lib/animated";
import PropTypes from "prop-types";

const MultiSelect = (props) => {

  const convertOptions = (options) => {
    return options.map(item => {
      item.label = item.name;
      item.value = item.guid;
      return item;
    });
  };

  convertOptions(props.options);

  if(props.defaultValue) {
    return (
      <div>
        <Select
          isMulti={props.multi}
          selectOption={props.options[0]}
          components={makeAnimated()}
          options={props.options}
          className={props.name}
          classNamePrefix="select"
          defaultValue={props.defaultValue}
          onChange = {props.onSelect}
          styles = {props.styles}
          placeholder={props.placeholder || "Select..."}
        />
      </div>
    );
  }
  else
    return (
      <div>
        <Select
          isMulti={props.multi}
          selectOption={props.options[0]}
          components={makeAnimated()}
          options={props.options}
          className={props.name}
          classNamePrefix="select"
          onChange = {props.onSelect}
          styles = {props.styles}
          isSearchable = {props.isSearchable}
          placeholder={props.placeholder || "Select..."}
        />
      </div>
    );
};
 
export default MultiSelect;


MultiSelect.propTypes = {
  multi: PropTypes.bool,
  options: PropTypes.array,
  name: PropTypes.string,
  onSelect: PropTypes.func,
};