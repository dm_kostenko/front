import React from "react";
import PropTypes from "prop-types";

export const TableLabel = (...props) => {
  return (
    <div className="title">{props.label}</div>
  );
};

TableLabel.propTypes = {
  label: PropTypes.number,
};