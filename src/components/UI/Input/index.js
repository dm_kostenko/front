import React from "react";
import {
  Col,
  ControlLabel,
  FormControl,
  FormGroup,
  Alert
} from "react-bootstrap";
import PropTypes from "prop-types";

const Input = ({ name, label, error, ...rest }) => {
  return (
    <Col className="col">
      <ControlLabel htmlFor={name}>{label}</ControlLabel>
      <FormGroup>
        <FormControl
          {...rest}
          name={name}
          id={name}
        />
        {error && <Alert>{error}</Alert>}
      </FormGroup>
    </Col>
  );
};

export default Input;

Input.propTypes = {
  name: PropTypes.node,
  label: PropTypes.string,
  error: PropTypes.string,
};
