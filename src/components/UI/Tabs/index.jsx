import React from "react";
import { Tabs as BootstrapTabs } from "react-bootstrap";

export const Tabs = (...props) => {
  return (
    <BootstrapTabs
      {...props}
    />
  );
};