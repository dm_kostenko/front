import { Tab as BootstrapTab } from "react-bootstrap";
import React from "react";

export const Tab = (...props) => {
  return (
    <BootstrapTab
      {...props}
    />
  );
};