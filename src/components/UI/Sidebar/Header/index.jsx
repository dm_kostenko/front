import React from "react";
import { connect } from "react-redux";
import logo from "assets/img/free-logo-bank.png";
import PropTypes from "prop-types";


const SidebarHeader = ({ isHide }) => {
  return (
    <div className="logo" style={{ position: "absolute" }}>
      <a
        className="simple-text logo-mini"
      >
        <div 
          id="sidebar-header-logo"
          className={isHide ? "logo-img slideOut" : "logo-img slideIn"}
        >
          <img src={logo} alt="logo"  />
        </div>
      </a>
      <div id="sidebar-text-wrapper">
        <span 
          id="sidebar-header-text" 
          className={isHide ? "simple-text logo-normal slideOut" : "simple-text logo-normal slideIn"}
        >
          Internet bank
        </span>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    isHide: state.sidebar.isHide
  };
};

export default connect(mapStateToProps, null)(SidebarHeader);

SidebarHeader.propTypes = {
  isHide: PropTypes.bool
};