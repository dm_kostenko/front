import React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";

const activePath = (path, location) => {
  return location.includes(path) ? "active" : "";
};


const textIcon = (child, isHide) => {
  let text = "";
  for(let i = 0; i < child.name.length; i++)
    if(child.name[i] === child.name[i].toUpperCase() && child.name[i].match(/^[A-Za-z]+$/))
      text += child.name[i];
  return (
    <i 
      id="sidebar-text-icon"
      className={isHide ? "slideOut" : "slideIn"}
      style={{
        font: "14px Ubuntu, sans-serif", 
        fontWeight: "lighter",
        marginTop: "2px",
        opacity: ".8"
      }}
    >
      {text}
    </i>);
};

const SidebarCollapseItemChild = ({ child, key, isHide, location }) => {
  const className = activePath(child.path, location);
  return (
    <li
      className={className}
      key={key}
    >
      <NavLink
        id="sidebar-text-icon-active"
        title={isHide ? child.name : ""}
        className={isHide ? "nav-link sub slideOut" : "nav-link sub slideIn"}
        to={child.path}
        activeClassName="active"
      >
        {textIcon(child, isHide)}
        <div id="sidebar-collapse-text-wrapper">
          <p
            id="sidebar-text"
            className={isHide ? "slideOut" : "slideIn"}
          >
            {child.name}
          </p>
        </div>                          
      </NavLink>
    </li>
  );
};

const mapStateToProps = (state) => {
  return {
    isHide: state.sidebar.isHide,
    location: window.location.hash
  };
};
                
export default connect(mapStateToProps, null)(SidebarCollapseItemChild);

SidebarCollapseItemChild.propTypes = {
  child: PropTypes.object,
  key: PropTypes.string,
  isHide: PropTypes.bool,
  location: PropTypes.string
};