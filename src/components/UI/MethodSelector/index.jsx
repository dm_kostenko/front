import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Select from "components/UI/MultiSelect";

class MethodSelector extends Component {
  onSelect = (e) => {
    this.props.search({
      [this.props.value]: e.name
    });
  }

  styles = {
    control: styles => ({
      ...styles,
      backgroundColor: "rgba(255,255,255,0.4)",
      color: "black",
      boxShadow: "none !important",
      borderColor: "rgba(255,255,255,0.4)",
      ":active": {
        ...styles[":active"],
        boxShadow: "none !important"
      },
      ":hover": {
        ...styles[":hover"],
        borderColor: "rgba(255,255,255,0.4)",
        boxShadow: "none !important"
      },
    }),
    option: (styles, { isDisabled, isFocused, isSelected }) => {
      return {
        ...styles,
        backgroundColor: isDisabled
          ? null
          : isSelected
            ? "#beecff"
            : isFocused
              ? "#deebff"
              : null,
        ":active": {
          ...styles[":active"],
          backgroundColor: "#deebff",
        },
        color: "black"
      };
    },
  };

  render() {
    return (
      <div
        id="search-wrapper"
        className={this.props.isSearch ? "searchOpen search" : "searchClosed search"}
      >
        <Select
          multi={false}
          name="method_select"
          options={[ { guid: "1", name: "GET" }, { guid: "2", name: "POST" } ]}
          onSelect={this.onSelect}
          styles={this.styles}
          isSearchable={false}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isSearch: state.search.isSearch
  };
};

export default connect(mapStateToProps, {})(MethodSelector);

MethodSelector.propTypes = {
  value: PropTypes.string,
  reset: PropTypes.bool,
  search: PropTypes.func,
  isSearch: PropTypes.bool,
};