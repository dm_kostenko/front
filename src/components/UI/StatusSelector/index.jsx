import React, { Component } from "react";
import { connect } from "react-redux";
import Select from "components/UI/MultiSelect";
import PropTypes from "prop-types";

class StatusSelector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      value: ""
    };
  }

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.reset) {
      return {
        id: ""
      };
    }     
  }

  parseValue = (key, value) => {
    if(value === "Success") {
      if(key === "enabled")
        return 1;
      return value;
    }
    else {
      if(key === "enabled")
        return 0;
      return value;
    }
          
  }

  handleClick = (e) => {
    if(this.state.id !== e.target.id) {
      this.setState({
        id: e.target.id
      });
      this.props.search({
        [this.props.value]: this.parseValue(this.props.value, e.target.id)
      });
    }
    else {
      this.setState({
        id: ""
      });
      this.props.search({
        [this.props.value]: ""
      });
    }
  }

  types = {
    transactions: [
      {
        guid: "0",
        name: "pending"
      },
      {
        guid: "1",
        name: "processed"
      },
      {
        guid: "2",
        name: "sent"
      },
      {
        guid: "3",
        name: "approved"
      },
      {
        guid: "4",
        name: "failed"
      }      
    ],
    accounts: [
      {
        guid: "0",
        name: "active"
      },
      {
        guid: "1",
        name: "blocked"
      }

    ]
  }

  render() {
    return (
      // <div
      //   id="search-wrapper"
      //   className={this.props.isSearch ? "searchOpen search" : "searchClosed search"}
      // >
      //   <i
      //     id='Success'
      //     className="far fa-check-circle status-icon-success"
      //     style={this.state.id === "Success" 
      //       ? {
      //         color: "green",
      //         WebkitTransform: "scale(1.5)",
      //         msTransform: "scale(1.5)",
      //         transform: "scale(1.5)"
      //       }
      //       : null
      //     }
      //     onClick={this.handleClick}
      //   />
      //   <i
      //     id='Failed'
      //     className="far fa-times-circle status-icon-failed"
      //     style={this.state.id === "Failed" 
      //       ? {
      //         color: "red",
      //         WebkitTransform: "scale(1.5)",
      //         msTransform: "scale(1.5)",
      //         transform: "scale(1.5)"
      //       }
      //       : null
      //     }
      //     onClick={this.handleClick}
      //   />
      // </div>
      <div>
        <Select
          multi={false}
          name={`SELECT ${this.props.type.toUpperCase()} STATUS`}
          options={this.types[this.props.type]}
          onSelect={this.handleSelect}
          isSearchable={false}
          styles={{
            menu: (styles) => {
              return {
                ...styles,
                color: "black !important",
              };
            },
            control: (styles) => {
              return {
                ...styles,
                minHeight: "0px !important",
                height: "35px !important",
                marginTop: "-5px !important",
                boxShadow: "inset 0 0 5px rgba(0,0,0,0.1), inset 0 1px 2px rgba(0,0,0,0.3)",
                borderRadius: "5px",
                border: "none",
                background: "#b3eaff",
                font: "13px Tahoma, Arial sans-serif",
                textAlign: "center !important",
                paddingLeft: "10%"
              };
            }
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    reset: state.search.reset,
    isSearch: state.search.isSearch
  };
};

export default connect(mapStateToProps, {})(StatusSelector);

StatusSelector.propTypes = {
  value: PropTypes.string,
  reset: PropTypes.bool,
  search: PropTypes.func,
  type: PropTypes.string,
  isSearch: PropTypes.bool,
};