import React, { Component } from "react";
import PropTypes from "prop-types";

class Select extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.data[0].name
    };
  }

  handleChange = (event) => {
    const value = event.target.value;
    this.setState(() => ({
      value,
    }), () =>  { 
      this.props.onSelect(value);});
  };

  componentDidMount() {
    this.props.onSelect(this.state.value); // return first option, because this option is default
  }

  render() {
    return (
      <div className="form-group">
        <select 
          size="3" 
          name="" 
          onChange={this.handleChange}
        >
          {
            this.props.data.map(item => (
              <option key={item.Id} value={item.name}>
                {item.name}
              </option>
            )
            )
          }
        </select>
      </div>
    );
  }
}

export default Select;

Select.propTypes = {
  data: PropTypes.array,
  onSelect: PropTypes.func,
};