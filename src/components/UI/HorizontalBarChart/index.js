import React from "react";
import { HorizontalBar } from "react-chartjs-2";
import PropTypes from "prop-types";
import { ExportButton } from "components/ExportButton";

export const HorizontalBarChart = (props) => {
  const data = {
    labels: props.labels,
    datasets: props.datasets
  };
  
  return (
    <>
    <HorizontalBar
      data={data}
      height={props.height}
      options={props.options}
    />
    <ExportButton
      labels={props.labels}
      datasets={props.datasets}
      name={props.name}
    >
      export
    </ExportButton>
    </>
  );
};

HorizontalBarChart.propTypes = {
  name: PropTypes.string,
  options: PropTypes.object,
  height: PropTypes.number,
  labels: PropTypes.array,
  datasets: PropTypes.array,
};