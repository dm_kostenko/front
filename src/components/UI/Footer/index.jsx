import React from "react";
import { Grid } from "react-bootstrap";

export const Footer = (...props) => {
  return (
    <div>
      <footer id="footer" className="footer">
        <Grid fluid>
          <p className="copyright pull-right">
            &copy; {new Date().getFullYear()}{" "}
            <a href="URL">KLTA</a>
          </p>
        </Grid>
      </footer>
    </div>
  );
};
