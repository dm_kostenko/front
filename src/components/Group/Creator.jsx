import React, { Component } from "react";
import { Col, Row, ControlLabel, FormControl, FormGroup } from "react-bootstrap";
import Button from "react-bootstrap/es/Button";
import MultiSelect from "components/UI/MultiSelect/index";
import LoginCreator from "components/Login/Creator";
import Radio from "components/UI/Radio";
import { connect } from "react-redux";
import { addGroupAction, addGroupLoginAction } from "../../actions/groups";
import { getAllMerchants, editMerchantAction } from "../../actions/merchants";
import { getAllRoles } from "../../actions/roles";
import { getFreeLoginsAction } from "../../actions/logins";
import { showModal } from "../../actions/modal";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import { getAuthData } from "../../services/paymentBackendAPI/backendPlatform";
import { getRoles } from "../../services/paymentBackendAPI/backendPlatform";
import ReactLoading from "react-loading";

class GroupCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: "0",
      radioValid: false,
      name: "",
      type: "",
      login_guid: "",
      role_guid: "",
      Partner_guid: "",
      newLoginGuid: "",
      NameIsValid: false,
      TypeFieldValid: false,
      selectedGroups: [],
      selectedMerchants: [],
      isAdmin: false,
      token: {
        partner: true
      },
      role: "",
      isLoading: false
    };
  }

  validate(val) {
    return (val !== "");
  }

  updateState = (newLoginGuid) => {
    this.setState({
      newLoginGuid
    });
  };

  chooseExistingLogin = () => {
    if (this.state.radio === "2") {
      let array = [];
      const logins = this.props.logins;
      for (let i = 0; i < logins.length; i++) {
        let item = {
          guid: logins[i].guid,
          name: logins[i].username
        };
        array = [ ...array, item ];
      }
      return (<div style={{ textAlign: "center" }}>
        <Row>
          <Col md={1} />
          <Col md={10}>
            <MultiSelect
              options={array}
              multi={false}
              onSelect={this.onSelectLogin}
            />
          </Col>
          <Col md={1} />
        </Row>
      </div>);
    }
    return null;
  }

  chooseNewLogin = () => {
    if (this.state.radio === "1" || !this.state.isAdmin)
      return <LoginCreator flag={true} updateState={this.updateState} />;
    return null;
  }

  onNameChange = (e) => {
    const val = e.target.value;
    const valid = this.validate(val);
    this.setState({ name: val, NameIsValid: valid });
  };

  onTypeFieldChange = (e) => {
    const val = e.target.value;
    const valid = this.validate(val);
    this.setState({ type: val, TypeFieldValid: valid });
  };

  async componentDidMount() {    
    const token = getAuthData().userPayload;
    let isAdmin = await getRoles();
    isAdmin = isAdmin[0].role === "admin";
    this.setState({ token, isAdmin });
    await this.props.getAllRoles(undefined, undefined, undefined, "group");
    await this.props.getAllMerchants();
    await this.props.getFreeLoginsAction("group");
  }

  onSelectLogin = (option) => {
    this.setState({
      login_guid: option.guid
    });
  };

  onSelectGroups = (option) => {
    this.setState({
      selectedGroups: option
    });
  };

  onSelectMerchants = (option) => {
    this.setState({
      selectedMerchants: option
    });
  };

  onSelectRole = (option) => {
    this.setState({
      role_guid: option.guid
    });
  };

  handleRadio = event => {
    const target = event.target;
    if (target.value === "1")
      this.setState({
        radio: "1",
        radioValid: true
      });
    if (target.value === "2") {
      this.setState({
        radio: "2",
        radioValid: true
      });
    }
  };

  requestServices = async (userGuid) => {
    let name = this.state.name;
    let type = this.state.type;
    let login_guid = userGuid;
    let role_guid = this.state.role_guid;
    let selectedMerchants = this.state.selectedMerchants;
    const data = {
      name,
      type
    };
    if (this.state.token.partner)
      data.partner_guid = this.state.token.partner.partner_guid;
    try {
      await this.props.addGroupAction(data, this.props.currentPageGroup, this.props.pageSizeGroup, this.props.groupsSearch);
      const group_guid = this.props.group_guid;
      for (let i = 0; i < selectedMerchants.length; i++) {
        const data = {
          guid: selectedMerchants[i].guid,
          group_guid
        };
        this.props.editMerchantAction(data);
      }
      await this.props.addGroupLoginAction(
        group_guid,
        {
          login_guid,
          role_guid
        });
      swal({
        title: "Group is created",
        icon: "success",
        button:false,
        timer: 2000
      });
      await this.props.showModal(false);
    } catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  }

  handleSubmit = (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    if (this.state.name !== ""
      && this.state.type !== ""
      && this.state.role_guid !== "" && (this.state.newLoginGuid !== "" || this.state.login_guid !== "")) {
      if (this.state.newLoginGuid !== "" && this.state.login_guid === "") {
        this.requestServices(this.state.newLoginGuid);
      }
      else {
        this.requestServices(this.state.login_guid);
      }
    }
    else {
      this.setState({ isLoading: false });
      swal({
        title: "Please, fill in all the fields",
        icon: "warning"
      });
    }     
  }

  render() {
    const chooseNewLogin = this.chooseNewLogin();
    const chooseExistingLogin = this.chooseExistingLogin();
    const merchants = this.props.merchants ? this.props.merchants : [];
    const filteredMerchants = merchants.filter(merchant => merchant.group_guid === null);
    let roles = this.props.roles ? this.props.roles : [];
    if(!this.state.isAdmin)
      roles = this.props.roles ? this.props.roles.filter(role => role.name === "groupRole") : [];
    return (
      <div style={{ padding: "20px" }}>
        <Row>
          <Col sm={1} />
          <Col md={12}>

            <div className="card">
              <Row className="rowReg">
                <Col md={3}>
                  <ControlLabel>Name*</ControlLabel>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <FormControl
                      name="name"
                      type="text"
                      value={this.state.name}
                      onChange={this.onNameChange}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row className="rowReg">
                <Col md={3}>
                  <ControlLabel>Type*</ControlLabel>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <FormControl
                      name="type"
                      type="text"
                      value={this.state.type}
                      onChange={this.onTypeFieldChange}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row className="rowReg">
                <Col md={3}>
                  <ControlLabel>Role*</ControlLabel>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <MultiSelect name="Roles" options={roles} multi={false} onSelect={this.onSelectRole} />
                  </FormGroup>
                </Col>
              </Row>
              {!this.state.token.partner &&
              <Row className="rowReg">
                <Col md={3}>
                  <ControlLabel>Merchants</ControlLabel>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <MultiSelect
                      name="Merchants"
                      options={filteredMerchants}
                      multi={true}
                      onSelect={this.onSelectMerchants}
                    />
                  </FormGroup>
                </Col>
              </Row>}
              {this.state.isAdmin &&
              <>
              <Row className="rowReg">
                <Col md={12}>
                  <FormGroup>
                    <Radio
                      number="3"
                      option="1"
                      name="radio"
                      onChange={this.handleRadio}
                      label="Add new login"
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <FormGroup>
                    <Radio
                      number="4"
                      option="2"
                      name="radio"
                      onChange={this.handleRadio}
                      label="Choose existing login"
                    />
                  </FormGroup>
                </Col>
              </Row>
              </>}
              {chooseExistingLogin}
            </div>
          </Col>
        </Row>
        {chooseNewLogin}
        <div align="center">
          {this.state.isLoading 
            ? <ReactLoading type='cylon' color='grey' />
            : <Button type="submit" onClick={this.handleSubmit}>Add</Button>}
        </div>
        <Col sm={1} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    roles: state.roles.rolesList,
    logins: state.logins.freeLoginsList,
    group_guid: state.groups.group_guid,
    partner_guid: state.partners.partner_guid,
    merchants: state.merchants.merchantsList,
    currentPageGroup: state.groups.currentPage,
    pageSizeGroup: state.groups.pageSize,

    groupsSearch: state.search.groupsSearch,
  };
};

export default connect(mapStateToProps,
  {
    addGroupAction,
    addGroupLoginAction,
    showModal,
    getAllMerchants,
    editMerchantAction,
    getAllRoles,
    getFreeLoginsAction
  })(GroupCreator);

GroupCreator.propTypes = {
  roles: PropTypes.array,
  logins: PropTypes.array,
  group_guid: PropTypes.string,
  partner_guid: PropTypes.string,
  merchants: PropTypes.array,
  currentPageGroup: PropTypes.number,
  pageSizeGroup: PropTypes.number,
  groupsSearch: PropTypes.string,

  addGroupAction: PropTypes.func,
  addGroupLoginAction: PropTypes.func,
  getAllRoles: PropTypes.func,
  getFreeLoginsAction: PropTypes.func,
  showModal: PropTypes.func,
  getAllMerchants: PropTypes.func,
  editMerchantAction: PropTypes.func,
};