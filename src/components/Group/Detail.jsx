import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import Table from "components/UI/Table/index";
import RowAddLogin from "components/RowAddLogin";
import Pagination from "components/UI/Pagination/index";
import moment from "moment";
import { connect } from "react-redux";
import {
  getGroupAction,
  getGroupLoginsAction,
  addGroupLoginAction,
  deleteMerchantFromGroupAction,
  getGroupMerchantsAction
} from "actions/groups";
import { getFreeLoginsAction } from "../../actions/logins";
import { getAllRoles } from "../../actions/roles";
import Can from "components/Can";
import ability from "config/ability";
import PropTypes from "prop-types";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse";
import Spinner from "components/UI/Spinner";

class GroupDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLoginsPage: 1,
      currentMerchantsPage: 1,
      pageSize: 10
    };
  }

  componentDidMount = async () => {
    await this.props.getGroupLoginsAction(this.props.match.params.id, 1, this.state.pageSize);
    await this.props.getGroupMerchantsAction(this.props.match.params.id, 1, this.state.pageSize);
    await this.props.getGroupAction(this.props.match.params.id);
    await this.props.getFreeLoginsAction("group");
    await this.props.getAllRoles(undefined, undefined, undefined, "group");
  };


  handleLoginsPageChange = async (page) => {
    await this.props.getGroupLoginsAction(this.props.match.params.id, page, this.state.pageSize);
    this.setState({
      currentLoginsPage: page
    });
  };

  handleMerchantsPageChange = async (page) => {
    await this.props.getGroupMerchantsAction(this.props.match.params.id, page, this.state.pageSize);
    this.setState({
      currentMerchantsPage: page
    });
  };

  handleDelete = async (loginid, roleid) => {
    try {
      await this.props.addGroupLoginAction(this.props.match.params.id, {
        login_guid: loginid,
        role_guid: roleid,
        delete: true
      });
      await this.props.getGroupLoginsAction(this.props.match.params.id);
    }
    catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  handleDeleteMerchant = async (guid) => {
    try {
      await this.props.deleteMerchantFromGroupAction(guid, this.state.currentMerchantsPage, this.state.pageSize, this.props.match.params.id);
    } catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  columnsGroupMerchants = [
    { path: "guid", label: "Guid" },
    { path: "name", label: "Name" },
    ability.can("EXECUTE", "GROUPDETAIL") && {
      key: "delete",
      content: merchant => (
        <i
          className="far fa-trash-alt"
          style={{ cursor: "pointer", color: "red" }}
          onClick={() =>
            swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
              .then((willDelete) => {
                if (willDelete) {
                  this.handleDeleteMerchant(merchant.guid);
                  swal("Deleted", {
                    icon: "success",
                    button:false,
                    timer: 2000
                  });
                }
              })
          }
        />
      ),
      label: "Delete from the group"
    }
  ];

  columnsGroupLogins = [
    { path: "username", label: "Username" },
    { path: "role.name", label: "Role" },
    ability.can("EXECUTE", "GROUPDETAIL") &&
    {
      key: "delete",
      content: groupLogin => (
        <i
          className="far fa-trash-alt"
          style={{ cursor: "pointer", color: "red" }}
          onClick={() =>
            swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
              .then((willDelete) => {
                if (willDelete) {
                  this.handleDelete(groupLogin.guid, groupLogin.role_guid);
                  swal("Deleted", {
                    icon: "success",
                    button:false,
                    timer: 2000
                  });
                }
              })
          }
        />
      ),
      label: "Delete"
    }
  ];

  update = async (id) => {
    await this.props.getGroupAction(id);
    await this.props.getGroupMerchantsAction(id);
    await this.props.getGroupLoginsAction(id);
  };


  getLoginsPagedData = () => {
    const { loginsCount } = this.props;
    let { pageSize } = this.state;
    const loginsPagesCount = loginsCount ? (loginsCount / pageSize) + (1 && !!(loginsCount % pageSize)) : 0;
    return { loginsPagesCount };
  };

  getMerchantsPagedData = () => {
    const { merchantsCount } = this.props;
    let { pageSize } = this.state;
    const merchantsPagesCount = merchantsCount ? (merchantsCount / pageSize) + (1 && !!(merchantsCount % pageSize)) : 0;
    return { merchantsPagesCount };
  };

  render() {
    const { currentLoginsPage, currentMerchantsPage } = this.state;
    const { group } = this.props;
    const { loginsPagesCount } = this.getLoginsPagedData();
    const { merchantsPagesCount } = this.getMerchantsPagedData();
    const { groupLogins, groupMerchants } = this.props;
    if (this.props.groupLoading || 
      this.props.rolesLoading || 
      this.props.loginsLoading ||
      this.props.groupLoginsLoading ||
      this.props.groupMerchantsLoading) return <Spinner/>;
    else return (
      <>
      <Button onClick={this.props.history.goBack} className="detail-back-button"><i className="fas fa-arrow-left"></i></Button>
      <div className="content" style={{ overflow: "auto" }}>
        <Row style={{ marginLeft: "0px" }}>
          <div className="card">
            <div className="title" align="center">{group ? group.name : null}</div>
          </div>
          <div className="card">
            <div className="header">Info</div>
            <div className="content">
              <Row >
                <Col md={6} className="right"><label>GUID:</label></Col>
                <Col md={6}>{this.props.match.params.id}</Col>
              </Row>
              <Row>
                <Col md={6} className="right"><label>Type:</label></Col>
                <Col md={6}><span>{group.type}</span></Col>
              </Row>
              <Row>
                <Col md={6} className="right"><label>Partner:</label></Col>
                <Col md={6}><span>{group.partner_name}</span></Col>
              </Row>
              <Row>
                <Col md={6} className="right"><label>Created at:</label></Col>
                <Col md={6}><span>{group.created_at !== null ? moment(group.created_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
              </Row>
              <Row>
                <Col md={6} className="right"><label>Created by:</label></Col>
                <Col md={6}><span>{group.created_by}</span></Col>
              </Row>
              <Row>
                <Col md={6} className="right"><label>Updated at:</label></Col>
                <Col md={6}><span>{group.updated_at !== null ? moment(group.updated_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
              </Row>
              <Row>
                <Col md={6} className="right"><label>Updated by:</label></Col>
                <Col md={6}><span>{group.updated_by}</span></Col>
              </Row>
            </div>
          </div>
          <div className="card">
            <div className="header">Merchants</div>
            <div className="content">
              <Table
                columns={this.columnsGroupMerchants}
                data={groupMerchants ? groupMerchants : []}
                disableSearch={true}
              />
              <Pagination
                pagesCount={merchantsPagesCount}
                currentPage={currentMerchantsPage}
                onPageChange={this.handleMerchantsPageChange}
              />
            </div>

          </div>
          <div className="card">
            <div className="header">Logins</div>
            <div className="content">
              <Table
                columns={this.columnsGroupLogins}
                data={groupLogins ? groupLogins : []}
                disableSearch={true}
              />
              <Pagination
                pagesCount={loginsPagesCount}
                currentPage={currentLoginsPage}
                onPageChange={this.handleLoginsPageChange}
              />
              <Can do="EXECUTE" on="GROUPDETAIL">
                <Row>
                  <Col md={3} sm={3} xs={3} />
                  <Col md={6} sm={6} xs={6}>
                    <RowAddLogin type="group" id={this.props.match.params.id} update={this.update} />
                  </Col>
                  <Col md={3} sm={3} xs={3} />
                </Row>
              </Can>
            </div>
          </div>
        </Row>
      </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    group: state.groups.group,
    groupLogins: state.groups.groupLogins,
    loginsCount: state.groups.loginsCount,
    groupMerchants: state.groups.groupMerchants,
    merchantsCount: state.groups.merchantsCount,
    groupLoading: state.groups.groupLoading,
    rolesLoading: state.roles.rolesLoading,
    loginsLoading: state.logins.loginsLoading,
    groupLoginsLoading: state.groups.groupLoginsLoading,
    groupMerchantsLoading: state.groups.groupMerchantsLoading
  };
};

export default connect(mapStateToProps, {
  getGroupAction,
  getGroupLoginsAction,
  addGroupLoginAction,
  getGroupMerchantsAction,
  deleteMerchantFromGroupAction,
  getFreeLoginsAction,
  getAllRoles
})(GroupDetail);

GroupDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  group: PropTypes.object,
  groupLogins: PropTypes.array,
  loginsCount: PropTypes.number,
  groupMerchants: PropTypes.array,
  merchantsCount: PropTypes.number,
  groupLoading: PropTypes.bool,
  rolesLoading: PropTypes.bool,
  loginsLoading: PropTypes.bool,
  groupLoginsLoading: PropTypes.bool,
  groupMerchantsLoading: PropTypes.bool,

  getGroupAction: PropTypes.func,
  getGroupLoginsAction: PropTypes.func,
  addGroupLoginAction: PropTypes.func,
  getGroupMerchantsAction: PropTypes.func,
  deleteMerchantFromGroupAction: PropTypes.func,
  getFreeLoginsAction: PropTypes.func,
  getAllRoles: PropTypes.func
};
