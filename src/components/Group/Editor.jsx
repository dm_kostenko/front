import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Row, FormGroup, ControlLabel, FormControl, Button, Form } from "react-bootstrap";
import { getAllMerchants } from "actions/merchants";
import { getGroupAction, editGroupAction, addMerchantToGroupAction, deleteMerchantFromGroupAction, getGroupMerchantsAction } from "actions/groups";
import { showModal } from "actions/modal";
import { getAuthData } from "services/paymentBackendAPI/backendPlatform";
import PropTypes from "prop-types";
import GroupMerchantsManagement from "./MerchantsManagement";
import { parseResponse } from "helpers/parseResponse.js";
import Spinner from "components/UI/Spinner/index";
import swal from "sweetalert";
import { getPartnerAction } from "actions/partners";
import ReactLoading from "react-loading";

class GroupEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      label: "",
      data: {
        name: "",
        type: "",
        description: "",
        selectedValue: "",
        group_guid: "",
        partner_guid: "",
      },
      select: [],
      selectedValue: [],
      token: {},
      isLoading: false
    };
  }

  async componentDidMount() {
    const token = getAuthData().userPayload;
    this.setState({ token });
    await this.props.getGroupAction(this.props.guid);
    await this.props.getAllMerchants();
    await this.props.getGroupMerchantsAction(this.props.guid);
    const { group, merchants } = this.props;
    this.setState({
      label: "Merchants",
      data: group,
      selectedValue: group.merchants,
      select: merchants
    });
  }

  formValidation = (data) => {
    if (data === "") return "error";
    else return "success";
  };

  handleChange = ({ currentTarget: input }) => {
    const data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data });
  };

  generalValidation = () => {
    const { data } = this.state;
    return this.formValidation(data.name) === "success" &&
      this.formValidation(data.type) === "success" &&
      this.formValidation(data.selectedValue) === "success";
  };

  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    try {
      const merchants = this.props.changedMerchants ? this.props.changedMerchants : [];
      merchants.forEach(merchant => {
        if (merchant.delete) {
          this.props.deleteMerchantFromGroupAction(merchant.guid);
        }
        else {
          this.props.addMerchantToGroupAction(merchant.guid, this.props.guid);
        }
      });
      const data = {
        guid: this.props.guid,
        name: this.state.data.name,
        type: this.state.data.type
      };
      if (this.state.data.partner_guid)
        data.partner_guid = this.state.data.partner_guid;
      else
        data.partner_guid = "";
      await this.props.editGroupAction(data);
      swal({
        title: "Group is updated",
        icon: "success",
        button:false,
        timer: 2000
      });
      this.state.token.partner && await this.props.getPartnerAction(this.state.token.partner.partner_guid);
      await this.props.showModal(false);
    }
    catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };


  render() {
    const { data } = this.state;
    if (this.props.merchantsLoading ||
      this.props.groupLoading ||
      this.props.groupMerchantsLoading) {
      return <Spinner />;
    }
    else return (
      <div>
        <div className="card">
          <div className="content"><label>ID: {this.props.guid}</label></div>
        </div>
        <div className="card">
          <Form onSubmit={this.handleSubmit} autoComplete="off">
            <Row className="rowEdit" >
              <Col className='col'>
                <ControlLabel>Name:</ControlLabel>
                <FormGroup validationState={this.formValidation(data.username)}>
                  <FormControl
                    name="name"
                    type="text"
                    value={data.name}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row className="rowEdit">
              <Col className='col'>
                <ControlLabel>Type:</ControlLabel>
                <FormGroup validationState={this.formValidation(this.state.data.username)}>
                  <FormControl
                    name="type"
                    type="text"
                    value={this.state.data.type}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row className="rowEdit">
              <Col className='col'>
                <ControlLabel>Included merchants:</ControlLabel>
                <GroupMerchantsManagement group_guid={this.props.guid} />
              </Col>
            </Row>
            <div align="center">
              {this.state.isLoading 
                ? <ReactLoading type='cylon' color='grey' />
                : <Button type="submit">Save</Button>}
            </div>
          </Form>
        </div>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    merchants: state.merchants.merchantsList,
    merchantsLoading: state.merchants.merchantsLoading,

    group: state.groups.group,
    changedMerchants: state.groups.changedMerchants,
    GroupLoading: state.groups.groupLoading,
    groupMerchantsLoading: state.groups.groupMerchantsLoading,
  };
};

export default connect(mapStateToProps, {
  getAllMerchants,
  getGroupAction,
  getGroupMerchantsAction,
  editGroupAction,
  showModal,
  addMerchantToGroupAction,
  deleteMerchantFromGroupAction,
  getPartnerAction
})(GroupEditor);

GroupEditor.propTypes = {
  getAllMerchants: PropTypes.func,
  getGroupAction: PropTypes.func,
  editGroupAction: PropTypes.func,
  showModal: PropTypes.func,
  addMerchantToGroupAction: PropTypes.func,
  deleteMerchantFromGroupAction: PropTypes.func,
  getGroupMerchantsAction: PropTypes.func,
  getPartnerAction: PropTypes.func,

  merchants: PropTypes.array,
  group: PropTypes.object,
  changedMerchants: PropTypes.array,
  merchantsLoading: PropTypes.bool,
  groupLoading: PropTypes.bool,
  groupMerchantsLoading: PropTypes.bool,

  guid: PropTypes.string,
};