import React, { Component } from "react";
import { connect } from "react-redux";
import { upsertChangedMerchantAction } from "../../actions/groups";
import PropTypes from "prop-types";

class GroupMerchantsManagement extends Component {
  state = {
    groupGuid: "",
    groupMerchants: [],
    notGroupMerchants: [],
    filteredGroupMerchants: [],
    filteredNotGroupMerchants: [],
    changedMerchants: [],
    query: ""
  };

  componentDidMount() {
    const allMerchants = this.props.merchants.filter(merchant => merchant.group_guid === null);
    const groupMerchants = this.props.groupMerchants;
    let notGroupMerchants;
    if (groupMerchants.length === 0) {
      this.setState({
        notGroupMerchants: allMerchants,
        filteredNotGroupMerchants: allMerchants
      });
    }
    else {
      let groupMerchantsGuids = [];
      for (let i = 0; i < groupMerchants.length; i += 1) {
        groupMerchantsGuids = [ ...groupMerchantsGuids, groupMerchants[i].guid ];
      }
      notGroupMerchants = allMerchants.filter(merchant => !groupMerchantsGuids.includes(merchant.guid));
      this.setState({
        groupMerchants,
        notGroupMerchants,
        filteredGroupMerchants: groupMerchants,
        filteredNotGroupMerchants: []
      });
    }
  }

  handleDelete = async (e) => {
    const guid = e.target.id;
    const groupMerchants = this.state.groupMerchants.filter(merchant => merchant.guid !== guid);
    const merchant = this.state.groupMerchants.filter(merchant => merchant.guid === guid);
    const notGroupMerchants = [ merchant[0], ...this.state.notGroupMerchants ];
    await this.setState({
      notGroupMerchants,
      groupMerchants
    });
    this.filterMerchants(this.state.query);
    const newDel = {
      guid,
      delete: true
    };
    this.setState({ changedMerchants: [ ...this.state.changedMerchants, newDel ] });
    await this.props.upsertChangedMerchantAction(newDel);
  };

  handleAdd = async (e) => {
    const guid = e.target.id;
    const notGroupMerchants = this.state.notGroupMerchants.filter(merchant => merchant.guid !== guid);
    const merchant = this.state.notGroupMerchants.filter(merchant => merchant.guid === guid);
    const groupMerchants = [ ...this.state.groupMerchants, merchant[0] ];
    await this.setState({
      notGroupMerchants,
      groupMerchants
    });
    this.filterMerchants(this.state.query);
    const newAdd = {
      guid
    };
    this.setState({ changedMerchants: [ ...this.state.changedMerchants, newAdd ] });
    await this.props.upsertChangedMerchantAction(newAdd);
  };

  filterMerchants = (query) =>{
    const filteredGroupMerchants = this.state.groupMerchants.filter(merchant => {
      return merchant.name.toLowerCase().includes(query.toLowerCase());
    });
    const filteredNotGroupMerchants = this.state.notGroupMerchants.filter(merchant => {
      return merchant.name.toLowerCase().includes(query.toLowerCase());
    });
    this.setState({
      query,
      filteredGroupMerchants,
      filteredNotGroupMerchants
    });
  }

  handleInputChange = (event) => {
    const query = event.target.value;
    if (!query && this.state.groupMerchants.length === 0) {
      this.setState({
        query,
        filteredGroupMerchants: this.state.groupMerchants,
        filteredNotGroupMerchants: this.state.notGroupMerchants
      });
      return null;
    }
    else if (!query) {
      this.setState({
        query,
        filteredGroupMerchants: this.state.groupMerchants,
        filteredNotGroupMerchants: []
      });
      return null;
    }
    this.filterMerchants(query);
  }

  render() {
    const { filteredGroupMerchants, filteredNotGroupMerchants } = this.state;
    let row = [];
    filteredGroupMerchants.forEach(item => {
      row = [ ...row,
        <li className="list-group-item" key={item.name} style={{ textAlign: "right", backgroundColor: "#87cefa" }}>
          {item.name + " "}
          <i
            id={item.guid}
            className="far fa-trash-alt"
            style={{ cursor: "pointer", color: "red", marginLeft: "10px" }}
            onClick={this.handleDelete}
          />
        </li> ];
    });
    let newRow = [];
    filteredNotGroupMerchants.forEach(item => {
      newRow = [ ...newRow,
        <li className="list-group-item" key={item.name} style={{ textAlign: "right" }}>
          {item.name + " "}
          <i
            id={item.guid}
            className="fas fa-plus"
            style={{ cursor: "pointer", color: "#1e90ff", marginLeft: "10px" }}
            onClick={this.handleAdd}
          />
        </li> ];
    });
    row = [ ...row, newRow ];
    return (
      <div>
        <div className="search-box">
          <form className="form-inline md-form form-sm mt-0">
            <input
              className="form-control form-control-sm ml-3 w-75"
              type="text"
              placeholder="Search"
              aria-label="Search"
              value={this.state.query}
              onChange={e => this.handleInputChange(e)}
            />{"    "}
            <i className="fas fa-search" aria-hidden="true"></i>
          </form>
        </div>
        <ul className="list-group">{row}</ul>
        {(filteredGroupMerchants.length === 0 && filteredNotGroupMerchants === 0) ?
          <div>
            <n />
            <label htmlFor="">There are no available merchants</label>
          </div>
          : null
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    group: state.groups.group,
    groupMerchants: state.groups.groupMerchants,
    merchants: state.merchants.merchantsList
  };
};

export default connect(mapStateToProps, {
  upsertChangedMerchantAction,
})(GroupMerchantsManagement);

GroupMerchantsManagement.propTypes = {
  getAllMerchants: PropTypes.func,
  getGroupAction: PropTypes.func,
  upsertChangedMerchantAction: PropTypes.func,
  getGroupMerchantsAction: PropTypes.func,

  groupMerchants: PropTypes.array,
  merchants: PropTypes.array,
};