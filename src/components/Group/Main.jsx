import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../factories/Main";
import GroupCreator from "./Creator";
import ability from "config/ability";
import { getAllGroups, deleteGroupAction } from "../../actions/groups";
import { searchInGroups, reset } from "../../actions/search";
import GroupEditor from "./Editor";
import Modal from "components/UI/Modal";
import { Link } from "react-router-dom";
import { getAuthData } from "../../services/paymentBackendAPI/backendPlatform";

let columns = [
  {
    path: "guid",
    label: "ID",
    key: "guid",
    content: group => (
      <Link className="link" to={`/about/group/${group.guid}`} >{group.guid}</Link>
    ),
  },
  { path: "name", label: "Name" },
  { path: "partner_name", label: "Partner" },
  ability.can("EXECUTE", "GROUPS") &&
  {
    key: "edit",
    content: group => (
      <Modal 
        header="Edit group" 
        content={<GroupEditor guid={group.guid} type="gateway" />}
        button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }}/>} 
      />     
    ),
    label: "Edit"
  },
  ability.can("EXECUTE", "GROUPS") &&
  {
    key: "delete",
    content: () => (
      <i
        className="far fa-trash-alt"
        style={{ cursor: "pointer", color: "red" }}
      />
    ),
    label: "Delete"
  }
];

if(getAuthData() && getAuthData().userPayload.partner)
  columns.splice(2,1);

const mapStateToProps = (state) => {
  return {
    data: state.groups.groupsList,
    count: state.groups.count,
    searchData: state.search.groupsSearch,
    isSearch: state.search.isSearch,
    loading: state.groups.groupsLoading,
    name: "groups",
    columns,
    modal: <GroupCreator />
  };
};

export default connect(mapStateToProps, {
  get: getAllGroups,
  delete: deleteGroupAction,
  search: searchInGroups,
  reset
})(AbstractComponent);