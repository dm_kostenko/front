import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import Table from "components/UI/Table";
import RowAddLogin from "components/RowAddLogin";
import moment from "moment";
import { connect } from "react-redux";
import { getFreeLoginsAction } from "actions/logins";
import { getAllRoles } from "actions/roles";
import { getMerchantAction, getMerchantLoginsAction, addMerchantLoginAction } from "../../actions/merchants";
import Can from "components/Can";
import Pagination from "components/UI/Pagination";
import ability from "../../config/ability";
import PropTypes from "prop-types";
import { parseResponse } from "helpers/parseResponse";
import swal from "sweetalert";
import Spinner from "components/UI/Spinner/index";

class MerchantDetail extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      pageSize: 10
    };
  }

  componentDidMount = async () => {
    await this.props.getMerchantLoginsAction(this.props.match.params.id, 1, this.state.pageSize);
    await this.props.getMerchantAction(this.props.match.params.id);
    await this.props.getFreeLoginsAction("merchant");
    await this.props.getAllRoles(undefined, undefined, undefined, "merchant");
  };

  handlePageChange = async (page) => {
    await this.props.getMerchantLoginsAction(this.props.match.params.id, page, this.state.pageSize);
    this.setState({
      currentPage: page
    });
  };

  handleDelete = async (loginid, roleid) => {
    const data = {
      login_guid: loginid,
      role_guid: roleid,
      delete: true
    };
    try {
      await this.props.addMerchantLoginAction(this.props.match.params.id, data);
      await this.props.getMerchantLoginsAction(this.props.match.params.id);
    }
    catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  columnsLogin = [
    { path: "username", label: "Name" },
    { path: "role.name", label: "Role" },
    ability.can("EXECUTE", "MERCHANTDETAIL") &&
    {
      key: "delete",
      content: merchantLogin => (
        <i
          className="far fa-trash-alt"
          style={{ cursor: "pointer", color: "red" }}
          onClick={() =>
            swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
              .then((willDelete) => {
                if (willDelete) {
                  this.handleDelete(merchantLogin.guid, merchantLogin.role_guid);
                  swal("Deleted", {
                    icon: "success",
                    button:false,
                    timer: 2000
                  });
                }
              })
          }
        />
      ),
      label: "Delete"
    }
  ];

  getPagedData = () => {
    const { count } = this.props;
    let { pageSize } = this.state;
    const pagesCount = (count / pageSize) + (1 && !!(count % pageSize));
    return { pagesCount };
  };

  update = (id) => {
    this.props.getMerchantAction(id);
    this.props.getMerchantLoginsAction(id);
  }

  render() {
    const { currentPage } = this.state;
    const { merchant, merchantLogins } = this.props;
    const { pagesCount } = this.getPagedData();
    if (this.props.merchantLoginsLoading || this.props.merchantLoading) return <Spinner/>;
    else return (
      <>
      <Button onClick={this.props.history.goBack} className="detail-back-button"><i className="fas fa-arrow-left"></i></Button>
      <div className="content" style={{ overflow: "auto" }}>
        <Row style={{ marginLeft: "0px" }}>
          <div className="card">
            <div className="title">{merchant.name}</div>
          </div>
          <div className="card-container">
            <div className="card">
              <div className="header">Info</div>
              <div className="content">
                <Row>
                  <Col md={6} sm={6} xs={6} className="right"><label>GUID:</label></Col>
                  <Col md={6} sm={6} xs={6} className="left"><span>{this.props.match.params.id}</span></Col>
                </Row>
                <Row>
                  <Col md={6} sm={6} xs={6} className="right"><label>Type:</label></Col>
                  <Col md={6} sm={6} xs={6} className="left"><span>{merchant.type}</span></Col>
                </Row>
                <Row>
                  <Col md={6} sm={6} xs={6} className="right"><label>Group:</label></Col>
                  <Col md={6} sm={6} xs={6} className="left"><span>{merchant.group_name}</span></Col>
                </Row>
                <Row>
                  <Col md={6} sm={6} xs={6} className="right"><label>Created at:</label></Col>
                  <Col md={6} sm={6} xs={6} className="left"><span>{merchant.created_at !== null ? moment(merchant.created_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
                </Row>
                <Row>
                  <Col md={6} sm={6} xs={6} className="right"><label>Created by:</label></Col>
                  <Col md={6} sm={6} xs={6} className="left"><span>{merchant.created_by}</span></Col>
                </Row>
                <Row>
                  <Col md={6} sm={6} xs={6} className="right"><label>Updated at:</label></Col>
                  <Col md={6} sm={6} xs={6} className="left"><span> {merchant.updated_at !== null ? moment(merchant.updated_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
                </Row>
                <Row>
                  <Col md={6} sm={6} xs={6} className="right"><label>Updated by:</label></Col>
                  <Col md={6} sm={6} xs={6} className="left"><span>{merchant.updated_by}</span></Col>
                </Row>
              </div>
            </div>
            <div className="card">
              <div className="header">Logins</div>
              <div className="content">
                <Row>
                  <Col md={12} sm={12} xs={12}>
                    <Table
                      columns={this.columnsLogin}
                      data={merchantLogins ? merchantLogins : []}
                      disableSearch={true}
                    />
                    <Pagination
                      pagesCount={pagesCount}
                      currentPage={currentPage}
                      onPageChange={this.handlePageChange}
                    />
                    <Can do="EXECUTE" on="MERCHANTDETAIL">
                      <Col md={3} sm={3} xs={3} />
                      <Col md={6} sm={6} xs={6}>
                        <RowAddLogin type="merchant" id={this.props.match.params.id} update={this.update} />
                      </Col>
                      <Col md={3} sm={3} xs={3} />
                    </Can>
                  </Col>
                </Row>
              </div>
            </div>
          </div>
        </Row>
      </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    merchant: state.merchants.merchant,
    merchantLogins: state.merchants.merchantLogins,
    count: state.merchants.loginsCount,
    merchantLoading: state.merchants.merchantLoading,
    merchantLoginsLoading: state.merchants.merchantLoginsLoading
  };
};

export default connect(mapStateToProps, {
  getMerchantAction,
  getMerchantLoginsAction,
  addMerchantLoginAction,
  getAllRoles,
  getFreeLoginsAction
})(MerchantDetail);

MerchantDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  merchant: PropTypes.object,
  merchantLogins: PropTypes.array,
  count: PropTypes.number,
  getMerchantAction: PropTypes.func,
  getMerchantLoginsAction: PropTypes.func,
  addMerchantLoginAction: PropTypes.func,
  getFreeLoginsAction: PropTypes.func,
  getAllRoles: PropTypes.func,
  merchantLoginsLoading: PropTypes.bool,
  merchantLoading: PropTypes.bool,
};