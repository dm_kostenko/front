import React, { Component } from "react";
import { Col, Row, ControlLabel, FormControl, FormGroup } from "react-bootstrap";
import Button from "components/UI/CustomButton/CustomButton";
import MultiSelect from "components/UI/MultiSelect";
import LoginCreator from "components/Login/Creator";
import Radio from "components/UI/Radio";
import { connect } from "react-redux";
import { addMerchantAction, addMerchantLoginAction } from "../../actions/merchants";
import { editGroupAction, getAllGroups } from "actions/groups";
import { getAllRoles } from "../../actions/roles";
import { getFreeLoginsAction } from "../../actions/logins";
import { showModal } from "../../actions/modal";
import { getAuthData } from "../../services/paymentBackendAPI/backendPlatform";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import ReactLoading from "react-loading";
import swal from "sweetalert";
import { getRoles } from "../../services/paymentBackendAPI/backendPlatform";

class MerchantCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: "0",
      radioValid: false,
      name: "",
      type: "",
      login_guid: "",
      role_guid: "",
      group_guid: "",
      newLoginGuid: "",
      typeFieldValid: false,
      token: {},
      isLoading: false,
      isAdmin: false,
      role: ""
    };
  }

  async componentDidMount() {
    const token = getAuthData().userPayload;
    let isAdmin = await getRoles();
    isAdmin = isAdmin[0].role === "admin";
    this.setState({ token, isAdmin });
    await this.props.getAllRoles(undefined, undefined, undefined, "merchant");
    await this.props.getFreeLoginsAction("merchant");
    await this.props.getAllGroups();
  }

  updateState = (newLoginGuid) => {
    this.setState({
      newLoginGuid
    });
  };

  chooseExistingLogin = () => {
    if (this.state.radio === "2") {
      let array = [];
      const logins = this.props.logins;
      for (let i = 0; i < logins.length; i++) {
        let item = {
          guid: logins[i].guid,
          name: logins[i].username
        };
        array = [ ...array, item ];
      }
      return (<div style={{ textAlign: "center" }}>
        <Row>
          <Col md={1} />
          <Col md={10}>
            <MultiSelect
              options={array}
              multi={false}
              onSelect={this.onSelectLogin}
            />
          </Col>
          <Col md={1} />
        </Row>
      </div>);
    }
    return null;
  }

  chooseNewLogin = () => {
    if (this.state.radio === "1" || !this.state.isAdmin)
      return <LoginCreator flag={true} updateState={this.updateState} />;
    return null;
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSelectLogin = (option) => {
    this.setState({
      login_guid: option.guid
    });
  };

  onSelectRole = (option) => {
    this.setState({
      role_guid: option.guid
    });
  };

  onSelectPartnerGroup = (option) => {
    this.setState({
      group_guid: option.guid
    });
  }

  handleRadio = event => {
    const target = event.target;
    if (target.value === "1")
      this.setState({
        radio: "1",
        radioValid: true
      });
    if (target.value === "2") {
      this.setState({
        radio: "2",
        radioValid: true
      });
    }
  };

  requestServices = async (loginGuid) => {
    let { name, type, role_guid, token, group_guid } = this.state;
    let login_guid = loginGuid;
    const data = {
      name,
      type
    };
    if (token.group)
      data.group_guid = token.group.group_guid;
    else if(group_guid)
      data.group_guid = group_guid;
    try {
      await this.props.addMerchantAction(data, this.props.currentPageMerchant, this.props.pageSizeMerchant, this.props.merchantsSearch);
      const merchant_guid = this.props.merchant_guid;
      await this.props.addMerchantLoginAction(
        merchant_guid,
        {
          login_guid,
          role_guid
        });
      swal({
        title: "Merchant is created",
        icon: "success",
        button: false,
        timer: 2000
      });
      await this.props.showModal(false);
    } catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  }

  handleSubmit = (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    if (this.state.name !== "" && this.state.type !== "" && this.state.role_guid !== "" && (this.state.newLoginGuid !== "" || this.state.login_guid !== "")) {
      if (this.state.newLoginGuid !== "" && this.state.login_guid === "") {
        this.requestServices(this.state.newLoginGuid);
      }
      else {
        this.requestServices(this.state.login_guid);
      }    
    }
    else {
      this.setState({ isLoading: false });
      swal({
        title: "Please, fill in all the fields",
        icon: "warning"
      });
    }
  }

  render() {
    const chooseNewLogin = this.chooseNewLogin();
    const chooseExistingLogin = this.chooseExistingLogin();
    let roles = this.props.roles ? this.props.roles : [];
    if(!this.state.isAdmin)
      roles = this.props.roles ? this.props.roles.filter(role => role.name === "merchantRole") : [];
    return (
      <div style={{ padding: "20px" }}>
        <Row>
          <Col sm={1} />
          <Col md={12}>
            <div className="card">
              <Row className="rowReg">
                <Col md={3}>
                  <ControlLabel>Name*</ControlLabel>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <FormControl
                      name="name"
                      type="text"
                      value={this.state.name}
                      onChange={this.handleChange}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row className="rowReg">
                <Col md={3}>
                  <ControlLabel>Type*</ControlLabel>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <FormControl
                      name="type"
                      type="text"
                      value={this.state.TypeField}
                      onChange={this.handleChange}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row className="rowReg">
                <Col md={3}>
                  <ControlLabel>Role*</ControlLabel>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <MultiSelect name="Roles" options={roles} multi={false} onSelect={this.onSelectRole} />
                  </FormGroup>
                </Col>
              </Row>
              {this.state.token.partner &&
              <Row className="rowReg">
                <Col md={3}>
                  <ControlLabel>Group*</ControlLabel>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <MultiSelect name="Groups" options={this.props.groups ? this.props.groups : []} multi={false} onSelect={this.onSelectPartnerGroup} />
                  </FormGroup>
                </Col>
              </Row>}
              {this.state.isAdmin &&
              <>
              <Row className="rowReg">
                <Col md={12}>
                  <FormGroup>
                    <Radio
                      number="3"
                      option="1"
                      name="radio"
                      onChange={this.handleRadio}
                      label="Add new login"
                    />
                  </FormGroup>
                </Col>
              </Row>             
              <Row>
                <Col md={12}>
                  <FormGroup>
                    <Radio
                      number="4"
                      option="2"
                      name="radio"
                      onChange={this.handleRadio}
                      label="Choose existing login"
                    />
                  </FormGroup>
                </Col>
              </Row>
              </>}
              {chooseExistingLogin}
            </div>
          </Col>
        </Row>
        {chooseNewLogin}
        <div align="center">
          {this.state.isLoading
            ? <ReactLoading type='cylon' color='grey' />
            : <Button type="submit" onClick={this.handleSubmit}>Add</Button>}
        </div>
        <Col sm={1} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    roles: state.roles.rolesList,
    logins: state.logins.freeLoginsList,
    groups: state.groups.groupsList,
    merchant_guid: state.merchants.merchant_guid,
    currentPageMerchant: state.merchants.currentPage,
    pageSizeMerchant: state.merchants.pageSize,
    merchantsSearch: state.search.merchantsSearch
  };
};

export default connect(mapStateToProps,
  {
    getAllRoles,
    getFreeLoginsAction,
    addMerchantAction,
    addMerchantLoginAction,
    getAllGroups,
    editGroupAction,
    showModal
  })(MerchantCreator);

MerchantCreator.propTypes = {
  roles: PropTypes.array,
  logins: PropTypes.array,
  merchant_guid: PropTypes.string,
  currentPageMerchant: PropTypes.number,
  pageSizeMerchant: PropTypes.number,

  getGroupAction: PropTypes.func,
  getAllRoles: PropTypes.func,
  getFreeLoginsAction: PropTypes.func,
  getAllGroups: PropTypes.func,
  addMerchantAction: PropTypes.func,
  addMerchantLoginAction: PropTypes.func,
  merchantsSearch: PropTypes.object,
  showModal: PropTypes.func,
  groups: PropTypes.array,
};