import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Row, FormGroup, ControlLabel, FormControl, Button, Form } from "react-bootstrap";
import { editMerchantAction, getMerchantAction } from "../../actions/merchants";
import { getGroupAction, getAllGroups } from "actions/groups";
import { showModal } from "../../actions/modal";
import { getAuthData, getRoles } from "../../services/paymentBackendAPI/backendPlatform";
import PropTypes from "prop-types";
import { parseResponse } from "helpers/parseResponse.js";
import Spinner from "components/UI/Spinner/index";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class MerchantEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      label: "",
      data: {
        name: "",
        type: "",
        monthly_amount_limit: "",
        group_guid: ""
      },
      select: [],
      selectedValue: [],
      token: {},
      role: "",
      isLoading: false
    };
  }

  async componentDidMount() {
    const token = getAuthData().userPayload;
    this.setState({ token });
    const role = getRoles()[0].role;
    this.setState({ role });
    await this.props.getMerchantAction(this.props.guid);
    await this.props.getAllGroups();
    const { merchant } = this.props;
    this.setState({
      data: merchant
    });
  }

  formValidation = (data) => {
    if (data === "") return "error";
    else return "success";
  };

  handleChange = ({ currentTarget: input }) => {
    let { data } = this.state;
    data[input.name] = input.value;
    this.setState({ data });
  };

  handleSelectChange = (e) => {
    let { data } = this.state;
    data.group_guid = e.target.value.split("---")[0];
    data.group_name = e.target.value.split("---")[1];
    this.setState({ data });
  };

  generalValidation = () => {
    const { data } = this.state;
    return this.formValidation(data.name) === "success" &&
      this.formValidation(data.type) === "success" &&
      this.formValidation(data.selectedValue) === "success";
  };

  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    try {
      let data = {
        guid: this.props.guid,
        name: this.state.data.name,
        type: this.state.data.type,
        monthly_amount_limit: this.state.data.monthly_amount_limit
      };
      if (this.state.data.group_guid)
        data.group_guid = this.state.data.group_guid;
      else
        data.group_guid = "";
      await this.props.editMerchantAction(data);
      swal({
        title: "Merchant is updated",
        icon: "success",
        button: false,
        timer: 2000
      });
      // this.state.token.group && await this.props.getGroupAction(this.state.token.group.group_guid);
      await this.props.showModal(false);
    }
    catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };


  render() {
    const { data } = this.state;
    if (this.props.merchantLoading) return <Spinner />;
    else return (
      <div>
        <div className="card">
          <div className="content"><label>ID: {this.props.guid}</label></div>
        </div>
        <div className="card">
          <Form onSubmit={this.handleSubmit} autoComplete="off">
            <Row className="rowEdit" >
              <Col className='col'>
                <ControlLabel>Name:</ControlLabel>
                <FormGroup validationState={this.formValidation(data.username)}>
                  <FormControl
                    name="name"
                    type="text"
                    value={data.name}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row className="rowEdit">
              <Col className='col'>
                <ControlLabel>Type:</ControlLabel>
                <FormGroup validationState={this.formValidation(data.type)}>
                  <FormControl
                    name="type"
                    type="text"
                    value={this.state.data.type}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
            {this.state.token.partner &&
              <Row className="rowEdit">
                <Col className="col">
                  <ControlLabel>Group*</ControlLabel>
                  <FormGroup>
                    <FormControl
                      name="group"
                      componentClass="select"
                      onChange={(e) => this.handleSelectChange(e)}
                      validationstate={this.formValidation(data.group_guid)}
                    >
                      <option value={this.state.data.group_guid + "---" + this.state.data.group_name}>{this.state.data.group_name}</option>
                      {this.props.groups.filter(group => group.guid !== this.state.data.group_guid).map(group => {
                        return (<option key={group.guid} value={group.guid + "---" + group.name}>{group.name}</option>);
                      })
                      }
                    </FormControl>                  
                  </FormGroup>
                </Col>
              </Row>}
            {this.state.role === "admin" &&
              <Row className="rowEdit">
                <Col className="col">
                  <ControlLabel>Month limit</ControlLabel>
                  <FormGroup>
                    <FormControl
                      name="monthly_amount_limit"
                      type="number"
                      value={this.state.data.monthly_amount_limit}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
              </Row>}
            <div align="center">
              {this.state.isLoading
                ? <ReactLoading type='cylon' color='grey' />
                : <Button type="submit">Save</Button>
              }
            </div>
          </Form>
        </div>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    merchant: state.merchants.merchant,
    merchantLoading: state.merchants.merchantLoading,
    groups: state.groups.groupsList
  };
};

export default connect(mapStateToProps, {
  getMerchantAction,
  editMerchantAction,
  showModal,
  getGroupAction,
  getAllGroups
})(MerchantEditor);

MerchantEditor.propTypes = {
  getMerchantAction: PropTypes.func,
  editMerchantAction: PropTypes.func,
  showModal: PropTypes.func,
  getGroupAction: PropTypes.func,
  getAllGroups: PropTypes.func,

  merchant: PropTypes.object,
  merchantLoading: PropTypes.bool,
  guid: PropTypes.string,
  groups: PropTypes.array
};