import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../factories/Main";
import MerchantCreator from "./Creator";
import ability from "config/ability";
import { getAllMerchants, deleteMerchantAction } from "../../actions/merchants";
import { searchInMerchants, reset } from "../../actions/search";
import { Link } from "react-router-dom";
import Modal from "components/UI/Modal";
import MerchantEditor from "./Editor";
import { getAuthData } from "../../services/paymentBackendAPI/backendPlatform";


let columns = [
  {
    path: "guid",
    label: "ID",
    key: "guid",
    content: merchant => (
      <Link className="link" to={`/about/merchant/${merchant.guid}`} >{merchant.guid}</Link>
    ),
  },
  { path: "name", label: "Name" },
  { path: "group_name", label: "Group" },
  ability.can("EXECUTE", "MERCHANTS") &&
  {
    key: "edit",
    content: merchant => (
      <Modal 
        header="Edit merchant" 
        action="Save" 
        content={<MerchantEditor guid={merchant.guid} />}
        button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }}/>} />
    ),
    label: "Edit"
  },
  ability.can("EXECUTE", "MERCHANTS") &&
  {
    key: "delete",
    content: () => (
      <i
        className="far fa-trash-alt"
        style={{ cursor: "pointer", color: "red" }}
      />
    ),
    label: "Delete"
  }
];

if(getAuthData() && getAuthData().userPayload.group)
  columns.splice(2,1);

const mapStateToProps = (state) => {
  return {
    data: state.merchants.merchantsList,
    count: state.merchants.count,
    searchData: state.search.merchantsSearch,
    isSearch: state.search.isSearch,
    loading: state.merchants.merchantsLoading,
    name: "merchants",
    columns,
    modal: <MerchantCreator />
  };
};

export default connect(mapStateToProps, {
  get: getAllMerchants,
  delete: deleteMerchantAction,
  search: searchInMerchants,
  reset
})(AbstractComponent);
