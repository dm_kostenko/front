import React, { Component } from "react";
import { DropdownButton, MenuItem } from "react-bootstrap";
import RoleMain from "../Role/Main";
import RoleCreator from "../Role/Creator";
import PrivilegeMain from "../Privilege/Main";
import PrivilegeCreator from "../Privilege/Creator";
import Modal from "../UI/Modal";
import Can from "../Can";


class RolePrivilegeMain extends Component {
  constructor() {
    super();

    this.state = {
      type: 1.1,
      isUpdate: false
    };

    this.update = this.update.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }

  async handleSelect(e) {
    await this.setState({ type: e });
  }

  update(value) {
    this.setState({ isUpdate: value });
  }

  render() {
    let typeToShow, labelButton, modalButtonName, modalContent, modalHeader;

    switch (this.state.type) {
    case 1.1:
      typeToShow = <RoleMain />;
      labelButton = "Role";
      modalButtonName = "Create role";
      modalContent = <RoleCreator />;
      modalHeader = "Create role";
      break;
    case 1.2:
      typeToShow = <PrivilegeMain />;
      labelButton = "Privilege";
      modalButtonName = "Create privilege";
      modalContent = <PrivilegeCreator />;
      modalHeader = "Create privilege";
      break;
    default:
      typeToShow = <RoleMain />;
      labelButton = "Role";
      modalButtonName = "Create role";
      modalContent = <RoleCreator />;
      modalHeader = "Create role";
    }

    return (
      <div>
        <DropdownButton
          title={labelButton + "s"}
          id="basic-nav-dropdown"
          onSelect={this.handleSelect}
          style={{ marginRight: "5px", marginBottom:"-15px", marginTop:"-15px" }}
        >
          <MenuItem eventKey={1.1}>Roles</MenuItem>
          <MenuItem eventKey={1.2}>Privileges</MenuItem>
        </DropdownButton>
        <Can do="EXECUTE" on={labelButton.toUpperCase() + "S"}>
          <Modal
            button={
              <button
                type="button"
                className="btn btn-primary"
                style={ { marginBottom:"-15px", marginTop:"-15px" } }
              >
                {modalButtonName}
              </button>
            }
            content={modalContent}
            header={modalHeader}
          />
        </Can>
        {typeToShow}
      </div>
    );
  }
}

export default RolePrivilegeMain;
