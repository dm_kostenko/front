import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "factories/Main";
import { getGroupsLogsAction } from "actions/logs";
import { searchInGroupsLogs, reset } from "actions/search";
import Modal from "components/UI/Modal";
import moment from "moment";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse.js";

const tryStringToJson = str => {
  try {
    return JSON.stringify(str, null, "\t");
  } catch (error) {
    const parsedError = parseResponse(error);
    swal({
      title: parsedError.error,
      text: parsedError.message,
      icon: "error",
    });
  }
};

const columns = [
  {
    path: "request_id", label: "Request guid"
  },
  {
    path: "createdAt",
    label: "Datetime",
    content: log => (
      moment(log.createdAt).utcOffset(0).format("DD.MM.YYYY HH:mm:ss")
    )
  },
  {
    path: "ip", label: "IP"
  },
  {
    path: "guid", label: "Group guid"
  },
  {
    path: "url",
    label: "URL",
    content: log => (
      log.url.slice(7)
    )
  },
  {
    path: "headers",
    label: "Headers",
    key: "headers",
    content: log => (log.headers && log.headers !== "{}") ? (
      <Modal
        content={<div className='card-shop-secret' style={{ whiteSpace: "pre-wrap", padding: "30px", maxWidth: "100%" }}>{tryStringToJson(log.headers)}</div>}
        buttonName="Show"
        button={
          <button
            type="button"
            className="btn"
            style={{ marginBottom: "-15px", marginTop: "-15px" }}
          >
            Show
          </button>
        }
      />
    ) : null
  },
  {
    path: "parameters",
    label: "Parameters",
    key: "parameters",
    content: log => (log.parameters && log.parameters !== "{}") ? (
      <Modal
        content={<div className='card-shop-secret' style={{ whiteSpace: "pre-wrap", padding: "30px", maxWidth: "100%" }}>{tryStringToJson(log.parameters)}</div>}
        buttonName="Show"
        button={
          <button
            type="button"
            className="btn"
            style={{ marginBottom: "-15px", marginTop: "-15px" }}
          >
            Show
          </button>
        }
      />
    ) : null
  },
  {
    path: "reqBody",
    label: "Request",
    key: "request",
    content: log => (log.reqBody && log.reqBody !== "{}") ? (
      <Modal
        content={<div className='card-shop-secret' style={{ whiteSpace: "pre-wrap", padding: "30px", maxWidth: "100%" }}>{tryStringToJson(log.reqBody)}</div>}
        buttonName="Show"
        button={
          <button
            type="button"
            className="btn"
            style={{ marginBottom: "-15px", marginTop: "-15px" }}
          >
            Show
          </button>
        }
      />
    ) : null
  },
  {
    path: "resBody",
    label: "Response",
    key: "response",
    content: log => (log.resBody && log.resBody !== "{}") ? (
      <Modal
        content={<div className='card-shop-secret' style={{ whiteSpace: "pre-wrap", padding: "30px", maxWidth: "100%" }}>{tryStringToJson(log.resBody)}</div>}
        button={
          <button
            type="button"
            className="btn"
            style={{ marginBottom: "-15px", marginTop: "-15px" }}
          >
            Show
          </button>
        }
      />
    ) : null
  },
  {
    path: "resStatusCode", label: "Status code"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.logs.usersLogs,
    count: state.logs.usersLogsCount,
    loading: state.logs.usersLogsLoading,
    searchData: state.search.groupsLogsSearch,
    isSearch: state.search.isSearch,
    name: "logs",
    columns,
  };
};

export default connect(mapStateToProps, {
  get: getGroupsLogsAction,
  search: searchInGroupsLogs,
  reset
})(AbstractComponent);