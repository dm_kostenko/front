import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "factories/Main";
import { getTransactionsLogsAction } from "actions/logs";
import { searchInTransactionsLogs, reset } from "actions/search";
import Modal from "components/UI/Modal";
import moment from "moment";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse.js";
import { getAuthData } from "services/paymentBackendAPI/backendPlatform";

const tryStringToJson = str => {
  try {
    return JSON.stringify(str, null, "\t");
  } catch (error) {
    const parsedError = parseResponse(error);
    swal({
      title: parsedError.error,
      text: parsedError.message,
      icon: "error",
    });
  }
};

const token = getAuthData() ? getAuthData().userPayload : {};

const columns = [
  {
    path: "request_id", 
    label: "Request guid"
  },
  {
    path: "createdAt", 
    label: "Created at",
    content: log => (
      moment(log.createdAt).utcOffset(0).format("DD.MM.YYYY HH:mm:ss")
    )
  },
  {
    path: "step_info", 
    label: "Step info"
  },
  {
    path: "transaction_guid", 
    label: "Transaction guid"
  },
  (token.merchant || token.group || token.partner) ? 
    undefined
    : { 
      path: "partner_name", 
      label: "Partner"
    },
  (token.merchant || token.group) ? 
    undefined
    : { 
      path: "group_name", 
      label: "Group"
    },
  (token.merchant) ? undefined
    : { 
      path: "merchant_name", 
      label: "Merchant"
    },
  {
    path: "shop_name", 
    label: "Shop"
  },
  {
    path: "message", 
    label: "Message",
    key: "message",
    content: transactionLog => transactionLog.message ? (
      <Modal
        content={<div className="card-shop-secret" style={{ whiteSpace: "pre-wrap", padding: "30px", maxWidth: "100%" }}>{tryStringToJson(transactionLog.message)}</div>}
        button={
          <button
            type="button"
            className="btn"
            style={{ marginBottom:"-15px", marginTop:"-15px" }}
          >
            Show
          </button>
        }
      />
    ) : null
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.logs.transactionsLogs,
    count: state.logs.transactionsLogsCount,
    loading: state.logs.transactionsLogsLoading,
    searchData: state.search.transactionsLogsSearch,
    isSearch: state.search.isSearch,
    name:"logs",
    columns,
  };
};

export default connect(mapStateToProps, {
  get: getTransactionsLogsAction,
  search: searchInTransactionsLogs,
  reset
})(AbstractComponent);