import React, { Component } from "react";
import { connect } from "react-redux";
import Table from "components/UI/Table";
import { getTransactionsStepsLogsAction } from "actions/logs";
import { StatusIcon } from "components/UI/StatusIcon";
import moment from "moment";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";

class TransactionsStepsLogs extends Component {

  componentDidMount = async() => {
    await this.props.getTransactionsStepsLogsAction(this.props.guid);
  }

  columns = [
    { path: "type", label: "Type" },
    { path: "status", 
      label: "Status",
      content: transactionStepLog => (
        StatusIcon(null, transactionStepLog.status, transactionStepLog.error ? transactionStepLog.error : null)
      )
    },
    {
      path: "date", 
      label: "Datetime",
      content: transactionStepLog => (
        moment(transactionStepLog.date).utcOffset(0).format("DD.MM.YYYY HH:mm")
      )
    },
    {
      path: "params", 
      label: "Params",
      key: "params",
      content: transactionStepLog => (
        transactionStepLog.params 
          ? <div
            className={"card-shop-secret"}
            style={{ 
              textAlign: "left", 
              whiteSpace: "pre-wrap", 
              padding: "30px",
              fontSize: "12px",
              maxWidth: "100%"
            }}
          >
            {JSON.stringify(transactionStepLog.params, null, "\t")}
          </div>
          : null

      )
    }
  ]

  render() {
    const { transactionsStepsLogs } = this.props;
    if (this.props.loading) return <Spinner/>;
    else return (
      <div>
        <Table 
          columns={this.columns} 
          data={transactionsStepsLogs} 
          disableSearch={true}
        />
      </div>
    );
  }


}

const mapStateToProps = (state) => {
  return {
    transactionsStepsLogs: state.logs.transactionsStepsLogs,
    loading: state.logs.loading
  };
};

export default connect(mapStateToProps, { 
  getTransactionsStepsLogsAction
})(TransactionsStepsLogs);

TransactionsStepsLogs.propTypes = {
  guid: PropTypes.string,
  transactionsStepsLogs: PropTypes.array,
  getTransactionsStepsLogsAction: PropTypes.func,
  loading: PropTypes.bool
};