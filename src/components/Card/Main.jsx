import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../factories/Main";
import CardCreator from "./Creator";
import ability from "config/ability";
import { getAllCards, deleteCardAction } from "../../actions/cards";
import { searchInCards, reset } from "../../actions/search";
import CardEditor from "./Editor";
import { Link } from "react-router-dom";
import Modal from "components/UI/Modal";

const columns = [
  {
    path: "guid",
    label: "ID",
    key: "guid",
    content: card => (
      <Link className="link" to={`/about/card/${card.guid}`}>{card.guid}</Link>
    )
  },
  { path: "type", label: "Type" },
  { path: "account_number", label: "Account number" },
  { path: "card_number", label: "Card number" },
  { path: "currency_name", label: "Currency" },
  { path: "name_on_card", label: "Name on card" },
  { path: "balance", label: "Balance" },
  ability.can("EXECUTE", "CARDS") &&
  {
    key: "edit",
    content: card => (
      <Modal 
        header="Edit card" 
        action="Save" 
        content={<CardEditor guid={card.guid} type="card"/>}
        button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }}/>} />
    ),
    label: "Edit"
  },
  ability.can("EXECUTE", "CARDS") &&
  {
    key: "delete",
    content: () => (
      <i
        className="far fa-trash-alt"
        style={{ cursor: "pointer", color: "red" }}
      />
    ),
    label: "Delete"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.cards.cardsList,
    count: state.cards.count,
    loading: state.cards.cardsLoading,
    searchData: state.search.cardsSearch,
    isSearch: state.search.isSearch,
    name: "cards",
    columns,
    modal: <CardCreator />
  };
};

export default connect(mapStateToProps, {
  get:  getAllCards,
  delete: deleteCardAction,
  search: searchInCards,
  reset
})(AbstractComponent);