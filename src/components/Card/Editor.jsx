import React, { Component } from "react";
import { Col, Row, Form, ControlLabel, FormControl, Button, FormGroup } from "react-bootstrap";
import { connect } from "react-redux";
import { getCardAction, updateCardAction } from "actions/cards";
import { showModal } from "actions/modal";
import { getAllCurrencies } from "actions/currencies";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner/index";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class CardEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      card: {
        guid: "",
        type: "",
        account_guid: "",
        account_number: "",
        card_number: "",
        currency_guid: "",
        currency_name: "",
        name_on_card: "",
        expired: "",
        expires_at: "",
        balance: null
      },
      isLoading: false
    };
  }

  componentDidMount = async () => {
    const cardGuid = this.props.guid;
    await this.props.getCardAction(cardGuid);
    this.props.getAllCurrencies();
    this.setState({
      card: this.props.card
    });
  }

  handleChange = ({ currentTarget: input }) => {
    const card = { ...this.state.card };
    card[input.name] = input.value;
    this.setState({ card });
  };

  onSelectCurrency = (e) => {
    const card = this.state.card;
    card.currency_guid = e.target.value;
    this.setState({
      card
    });
  };

  formValidation = data => {
    if (data === "") return "error";
    else return "success";
  };

  generalValidation = () => {
    const { card } = this.state;
    return this.formValidation(card.account_guid) === "success" &&
      this.formValidation(card.account_number) === "success";
  };

  luhnAlgorithm = (digits) => {
    let sum = 0;
    for (let i = 0; i < digits.length; i++) {
      let cardNum = parseInt(digits[i]);

      if ((digits.length - i) % 2 === 0) {
        cardNum = cardNum * 2;

        if (cardNum > 9) {
          cardNum = cardNum - 9;
        }
      }

      sum += cardNum;
    }

    return sum % 10 === 0;
  }

  handleSubmit = async (event) => {
    this.setState({ isLoading: true });
    event.preventDefault();
    const { card } = this.state;
    if (!this.generalValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    }
    else {
      try {
        if (!this.luhnAlgorithm(card.card_number)) {
          this.setState({ isLoading: false });
          swal({
            title: "Incorrect card number",
            icon: "warning"
          });
        }
        else {
          await this.props.updateCardAction({
            guid: card.guid,
            type: card.type,
            account_guid: card.account_guid,
            account_number: card.account_number,
            card_number: card.card_number,
            currency_name: card.currency_name,
            name_on_card: card.name_on_card
          });
          swal({
            title: "Card is updated",
            icon: "success",
            button:false,
            timer: 2000
          });
          this.props.showModal(false);
        }
      }
      catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
    }
  };

  render() {
    if (this.props.isCardInfoLoading) {
      return <Spinner />;
    }
    const { card } = this.state;
    return (
      <div >
        <div className="card">
          <div className="content">
            <label>ID: {this.props.guid}</label>
          </div>
        </div>
        <div className="card">
          <div className="content">
            <Form onSubmit={this.handleSubmit} autoComplete="off">
              <Row className="rowEdit" >
                <Col className='col'>
                  <ControlLabel>Card number:</ControlLabel>
                  <FormGroup validationState={this.formValidation(card.card_number)}>
                    <FormControl
                      name="card_number"
                      type="text"
                      value={card.card_number}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>

              </Row>

              <Row className="rowEdit">
                <Col className='col'>
                  <ControlLabel>Type:</ControlLabel>
                  <FormGroup validationState={this.formValidation(card.type)}>
                    <FormControl
                      name="type"
                      type="text"
                      value={card.type}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row className="rowEdit">
                <Col className='col'>
                  <ControlLabel>Name on card:</ControlLabel>
                  <FormGroup validationState={this.formValidation(card.name_on_card)}>
                    <FormControl
                      name="name_on_card"
                      type="text"
                      value={card.name_on_card}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
              </Row>

              <div align="center">
                {this.state.isLoading 
                  ? <ReactLoading type='cylon' color='grey' />
                  : <Button style={{ "marginTop": "10px" }} type="submit" onClick={this.handleSubmit}>Save</Button>}
              </div>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    card: state.cards.card,
    currencies: state.currencies.currenciesList,
    isCardInfoLoading: state.cards.cardLoading
  };
};

export default connect(mapStateToProps,
  { getCardAction, updateCardAction, showModal, getAllCurrencies })(CardEditor);

CardEditor.propTypes = {
  isCardInfoLoading: PropTypes.bool,
  guid: PropTypes.string,
  card: PropTypes.array,
  currencies: PropTypes.array,
  getCardAction: PropTypes.func,
  updateCardAction: PropTypes.func,
  showModal: PropTypes.func,
  getAllCurrencies: PropTypes.func,
};