import React from "react";
import { Form } from "react-bootstrap";
import Joi from "joi-browser";
import CustomForm from "components/UI/Form/index";
import { connect } from "react-redux";
import { addCardAction } from "actions/cards";
import { getAllCurrencies } from "actions/currencies";
import { getAllAccounts, getAccountAction } from "actions/accounts";
import { showModal } from "actions/modal";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class CardCreator extends CustomForm {
  state = {
    data: {
      account_guid: "",
      type: "",
      card_number: "",
      name_on_card: "",
      expires_at: ""
    },
    expired: 0,
    errors: {},
    isLoading: false
  };

  schema = {
    account_guid: Joi.string()
      .required()
      .label("Account"),
    type: Joi.string()
      .required()
      .min(0)
      .label("Type"),
    card_number: Joi.string()
      .creditCard()
      .required()
      .min(16)
      .label("Card number"),
    name_on_card: Joi.string()
      .required()
      .min(0)
      .label("Name on card"),
    expires_at: Joi.string()
      .required()
      .min(0)
      .label("Expires at")
  };

  componentDidMount() {
    this.props.getAllAccounts();
  }

  doSubmit = async () => {
    this.setState({ isLoading: true });
    const { data } = this.state;
    try {
      await this.props.getAccountAction(data.account_guid);
      await this.props.addCardAction({
        account_guid: data.account_guid,
        type: data.type,
        card_number: data.card_number,
        currency_guid: this.props.account.currency_guid,
        name_on_card: data.name_on_card,
        expired: this.state.expired,
        expires_at: data.expires_at,
        balance: this.props.account.balance
      }, this.props.currentPage, this.props.pageSize);
      swal({
        title: "Card is successfully created",
        icon: "success",
        button: false,
        timer: 2000
      });
      this.props.showModal(false);
    }
    catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  render() {
    return (
      <div className="card">
        <div className="content">
          <Form autoComplete="off" onSubmit={this.handleSubmit}>
            {this.renderSelectNumber("account_guid", "Account* ", this.props.accounts)}
            {this.renderInput("type", "Type* ")}
            {this.renderInput("card_number", "Card Number*", "number")}
            {this.renderInput("name_on_card", "Name on card* ")}
            {this.renderInput("expires_at", "Expires at*", "date")}
            <div align="center">
              {this.state.isLoading
                ? <ReactLoading type='cylon' color='grey' />
                : this.renderButton("Add")}
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    accounts: state.accounts.accountsList,
    account: state.accounts.account,
    cards: state.cards.cardsList,
    currencies: state.currencies.currenciesList,
    pageSize: state.cards.pageSize,
    currentPage: state.cards.currentPage
  };
};

export default connect(mapStateToProps,
  {
    getAllAccounts,
    getAllCurrencies,
    getAccountAction,
    addCardAction,
    showModal
  })(CardCreator);

CardCreator.propTypes = {
  accounts: PropTypes.array,
  currencies: PropTypes.array,
  cards: PropTypes.array,
  account: PropTypes.object,
  addCardAction: PropTypes.func,
  getAllCurrencies: PropTypes.func,
  showModal: PropTypes.func,
  getAllAccounts: PropTypes.func,
  getAccountAction: PropTypes.func
};