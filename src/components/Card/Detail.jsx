import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import moment from "moment";
import { connect } from "react-redux";
import { getCardAction } from "actions/cards";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";

class CardDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      card: {}
    };
  }

  componentDidMount() {
    this.update(this.props.match.params.id);
  }

  update = (id) => {
    this.props.getCardAction(id);
  }

  render() {
    const card = this.props.card;
    if (this.props.cardLoading) return <Spinner/>;
    else return (
      <>
      <Button onClick={this.props.history.goBack} className="detail-back-button"><i className="fas fa-arrow-left"></i></Button>
      <div className="content">
        <div className="card">
          <div className="title">{card.card_number}</div>
        </div>
        <div className="card">
          <div className="content">
            <Row>
              <Col md={12}>
                <div className="column">
                  <Row>
                    <Col md={5} lg={5} xs={5} className="right"><label>GUID:</label></Col>
                    <Col md={5} lg={5} xs={5}><span>{this.props.match.params.id}</span></Col>
                  </Row>
                  <Row>
                    <Col md={5} lg={5} xs={5} className="right"><label>Type:</label></Col>
                    <Col md={5} lg={5} xs={5}><span>{card.type}</span></Col>
                  </Row>
                  <Row>
                    <Col md={5} lg={5} xs={5} className="right"><label>Account number:</label></Col>
                    <Col md={5} lg={5} xs={5}><span>{card.account_number}</span></Col>
                  </Row>
                  <Row>
                    <Col md={5} lg={5} xs={5} className="right"><label>Currency:</label></Col>
                    <Col md={5} lg={5} xs={5}><span>{card.currency_name}</span></Col>
                  </Row>
                  <Row>
                    <Col md={5} lg={5} xs={5} className="right"><label>Name on card:</label></Col>
                    <Col md={5} lg={5} xs={5}><span>{card.name_on_card}</span></Col>
                  </Row>
                  <Row>
                    <Col md={5} sm={5} xs={5} className="right"><label>Expired:</label></Col>
                    <Col md={5} sm={5} xs={5}>{card.expired ? <span>true</span> : <span>false</span> }</Col>
                  </Row>
                  <Row>
                    <Col md={5} lg={5} xs={5} className="right"><label>Expires at:</label></Col>
                    <Col md={5} lg={5} xs={5}><span>{card.expires_at!==null ? moment(card.expires_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
                  </Row>
                  <Row>
                    <Col md={5} lg={5} xs={5} className="right"><label>Name on card:</label></Col>
                    <Col md={5} lg={5} xs={5}><span>{card.name_on_card}</span></Col>
                  </Row>
                  <Row>
                    <Col md={5} lg={5} xs={5} className="right"><label>Created at:</label></Col>
                    <Col md={5} lg={5} xs={5}><span>{card.created_at!==null ? moment(card.created_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
                  </Row>
                  <Row>
                    <Col md={5} lg={5} xs={5} className="right"><label>Created by:</label></Col>
                    <Col md={5} lg={5} xs={5}><span>{card.created_by}</span></Col>
                  </Row>
                  <Row>
                    <Col md={5} lg={5} xs={5} className="right"><label>Updated at:</label></Col>
                    <Col md={5} lg={5} xs={5}><span>{card.updated_at!==null ? moment(card.updated_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
                  </Row>
                  <Row>
                    <Col md={5} lg={5} xs={5} className="right"><label>Updated by:</label></Col>
                    <Col md={5} lg={5} xs={5}><span>{card.updated_by}</span></Col>
                  </Row>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    card: state.cards.card,
    cardLoading: state.cards.cardLoading
  };
};

export default connect(mapStateToProps, { getCardAction })(CardDetail);

CardDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  card: PropTypes.object,
  getCardAction: PropTypes.func,
  cardLoading: PropTypes.bool,
};