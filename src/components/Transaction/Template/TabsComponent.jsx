import React, { Component } from "react";
import Table from "components/UI/Table/index";
import Pagination from "components/UI/Pagination/index";
import Params from "../Step/Param/Main";
import Modal from "components/UI/Modal";
import { getTransactionStepsAction } from "actions/transactions";
import { parseResponse } from "helpers/parseResponse";
import swal from "sweetalert";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";

class TabsComponent extends Component {
  state = {
    currentStepsPage: 1,
    pageSize: 10,
    gateway_guid: ""
  }

  componentDidMount = async () => {
    try {
      await this.props.getTransactionStepsAction(this.props.template_guid, 1, this.state.pageSize, this.props.gateway_guid);
    } catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  }

  columnsTransactionSteps = [
    { path: "guid", label: "ID" },
    { path: "number", label: "Number" },
    { path: "name", label: "Name" },
    {
      label: "Parameters",
      content: step => (
        <Modal
          header={"Params " + step.name}
          content={<Params guid={step.guid} />}
          buttonName="Show"
          button={
            <button
              type="button"
              className="btn"
              style={{ marginBottom: "-15px", marginTop: "-15px" }}
            >
              Show
            </button>
          }
        />)
    },
  ];

  handleStepsPageChange = async (page) => {
    await this.props.getTransactionStepsAction(this.props.template_guid, page, this.state.pageSize, this.props.gateway_guid);
    this.setState({
      currentStepsPage: page
    });
  };

  getStepsPagedData = (count) => {
    const stepsCount = count;
    let { pageSize } = this.state;
    const stepsPagesCount = stepsCount ? (stepsCount / pageSize) + (1 && !!(stepsCount % pageSize)) : 0;
    return { stepsPagesCount };
  };

  render() {
    const { currentStepsPage } = this.state;
    const transactionSteps = this.props.transactionSteps ? this.props.transactionSteps.length ? this.props.transactionSteps.filter(steps => steps.guid === this.props.gateway_guid) : [] : [];
    const { stepsPagesCount } = this.getStepsPagedData(transactionSteps.length ? transactionSteps[0].count : 0);
    return (
      this.props.loading ?
        <Spinner /> :
        <div>
          <Table
            columns={this.columnsTransactionSteps}
            data={transactionSteps.length ? transactionSteps[0].data : []}
            disableSearch={true}
          />
          <Pagination
            pagesCount={stepsPagesCount}
            currentPage={currentStepsPage}
            onPageChange={this.handleStepsPageChange}
          />
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    transactionSteps: state.transactions.transactionSteps,
    stepsCount: state.transactions.stepsCount,
    loading: state.transactions.transactionStepsLoading
  };
};

export default connect(mapStateToProps, {
  getTransactionStepsAction
})(TabsComponent);

TabsComponent.propTypes = {
  transactionSteps: PropTypes.array,
  stepsCount: PropTypes.number,
  getTransactionStepsAction: PropTypes.func,
  template_guid: PropTypes.string,
  gateway_guid: PropTypes.string,
  loading: PropTypes.bool
};