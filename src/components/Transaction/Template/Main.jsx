import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "factories/Main";
import { getTransactionTemplatesAction, deleteTransactionTemplateAction } from "actions/transactions";
import { searchInTransactionsTemplates, reset } from "actions/search";
import { Link } from "react-router-dom";
import moment from "moment";

const columns = [
  {
    path: "guid",
    label: "ID",
    key: "guid",
    content: template => (
      <Link className="link" to={`/about/transactiontemplate/${template.guid}`} >{template.guid}</Link>
    )
  },
  { path: "name", label: "Name" },
  { path: "type", label: "Type" },
  { path: "created_at", label: "Datetime", content: transaction => (moment(transaction.created_at).utcOffset(0).format("DD.MM.YYYY HH:mm")) },
  { path: "created_by", label: "Created by" },
  {
    key: "delete",
    content: () => (
      <i
        className="far fa-trash-alt"
        style={{ cursor: "pointer", color: "red" }}
      />
    ),
    label: "Delete"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.transactions.templatesList,
    count: state.transactions.count,
    searchData: state.search.transactionsTemplatesSearch,
    isSearch: state.search.isSearch,
    loading: state.transactions.templatesLoading,
    name:"transactiontemplates",
    columns
  };
};

export default connect(mapStateToProps, {
  get: getTransactionTemplatesAction,
  delete: deleteTransactionTemplateAction,
  search: searchInTransactionsTemplates,
  reset
})(AbstractComponent);