import React from "react";
import { Row, FormGroup, ControlLabel, FormControl, Col } from "react-bootstrap";
import StepList from "../Step/List";
import StepForm from "../Step/Form";
import { connect } from "react-redux";
import { cleanParams } from "actions/transactions";
import { addTransactionStep } from "services/paymentBackendAPI/management/steps";
import { addTransactionParam } from "services/paymentBackendAPI/management/params";
import { addTransactionTemplate, bindTransactionStep } from "services/paymentBackendAPI/management/transactions";
import { parseResponse } from "helpers/parseResponse";
import swal from "sweetalert";
import PropTypes from "prop-types";

var stepItems = [];

class TransactionConstructor extends React.Component {
  state = {
    name: "",
    type: "",
    stepItems: stepItems
  };

  formValidation = (data) => {
    if (data === "" || typeof (data) === "undefined") return "error";
    else return "success";
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  addItem = stepItem => {
    stepItems.unshift({
      index: stepItems.length + 1,
      number: stepItem.number,
      name: stepItem.name,
      gateway_guid: stepItem.gateway,
      params: this.props.params
    });
    this.setState({ stepItems: stepItems });
    this.props.cleanParams();
  };

  removeItem = itemIndex => {
    stepItems.splice(itemIndex, 1);
    this.setState({ stepItems: stepItems });
  };

  handleSubmit = async () => {
    try {
      const data = { type: this.state.type, name: this.state.name };
      let transaction = await addTransactionTemplate(data);
      this.state.stepItems.map(async (step) => {
        let stepItem = await addTransactionStep(step);
        step.params.map(async (param) => {
          await addTransactionParam(stepItem.data.guid, param);
        });
        await bindTransactionStep({ step_guid: stepItem.data.guid, number: step.number }, transaction.data.guid);
      }
      );
    }
    catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  }

  render() {
    return (
      <div className="content">
        <Row>
          <Col md={15}>
            <div className="card">
              <div style={{ paddingTop: "20px", width: "40%", margin: "auto", textAlign: "center" }}>
                <h2>Transaction creator</h2>
                <Row>
                  <Col md={5}></Col>
                  <Col md={10} sm={9} style={{ marginLeft: "50px" }}>
                    <ControlLabel>Name*:</ControlLabel>
                    <FormGroup>
                      <FormControl
                        name="name"
                        type="text"
                        value={this.state.name}
                        onChange={this.handleChange}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col md={10} sm={9} style={{ marginLeft: "50px" }}>
                    <ControlLabel>Type*:</ControlLabel>
                    <FormGroup>
                      <FormControl
                        name="type"
                        type="text"
                        value={this.state.type}
                        onChange={this.handleChange}
                      />
                    </FormGroup>
                  </Col>
                  <Col md={5}></Col>
                </Row>
                <div className="card">
                  <StepForm addItem={this.addItem} />
                </div>
                {stepItems.length !== 0 ? <div className="card">
                  <StepList items={stepItems} removeItem={this.removeItem} markTodoDone={this.markTodoDone} />
                </div> : <div></div>}
                <button type="submit" onClick={this.handleSubmit} className="btn btn-primary">Add transaction</button>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    params: state.transactions.paramsList
  };
};

export default connect(
  mapStateToProps,
  {
    cleanParams
  }
)(TransactionConstructor);

TransactionConstructor.propTypes = {
  cleanParams: PropTypes.func,
  params: PropTypes.array
};
