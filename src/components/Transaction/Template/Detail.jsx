import React, { Component } from "react";
import { getTransactionTemplateAction, getTransactionStepsAction, getStepAction } from "actions/transactions";
import { connect } from "react-redux";
import { Row, Col, Tabs, Tab, Button } from "react-bootstrap";
import { parseResponse } from "helpers/parseResponse";
import swal from "sweetalert";
import { getAllGateways } from "actions/gateways";
import TabsComponent from "./TabsComponent";
import PropTypes from "prop-types";

class TransactionTemplateDetail extends Component {

  componentDidMount = async () => {
    try {
      await this.props.getTransactionTemplateAction(this.props.match.params.id);
      await this.props.getAllGateways();
    } catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  render() {
    return (
      <>
      <Button onClick={this.props.history.goBack} className="detail-back-button"><i className="fas fa-arrow-left"></i></Button>
      <div className="content" style={{ overflow: "auto" }}>
        <div className="card">
          <div className="title">{this.props.template.name}</div>
        </div>
        <div className="card-container">
          <div className="card">
            <div className="header">General Info</div>
            <div className="content">
              <Row>
                <Col md={12}>
                  <Row>
                    <Col className="right" md={5} lg={5} xs={5}>
                      <label>GUID: </label>
                    </Col>
                    <Col md={7} lg={7} xs={7}>
                      <span>{this.props.match.params.id}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="right" md={5} lg={5} xs={5}>
                      <label>Name:</label>
                    </Col>
                    <Col md={7} lg={7} xs={7}>
                      <span>{this.props.template.name}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="right" md={5} lg={5} xs={5}>
                      <label>Type:</label>
                    </Col>
                    <Col md={7} lg={7} xs={7}>
                      <span>{this.props.template.type}</span>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
          </div>
          <div className="card" >
            <div className="header">Steps</div>
            <div className="content">
              <Tabs defaultActiveKey={0} transition={false} style={{ marginLeft: "23px" }}>
                <br />
                {this.props.gateways.map((gateway, index) =>
                  <Tab eventKey={index} key={gateway.name} title={gateway.name} mountOnEnter={true} style={{ position: "inline-block" }}>
                    <TabsComponent template_guid={this.props.match.params.id} gateway_guid={gateway.guid} />
                  </Tab>
                )}
              </Tabs>
            </div>
          </div>
        </div>
      </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    template: state.transactions.transactionTemplate,
    stepsCount: state.transactions.stepsCount,
    gateways: state.gateways.gatewaysList
  };
};

export default connect(mapStateToProps, {
  getTransactionTemplateAction,
  getTransactionStepsAction,
  getStepAction,
  getAllGateways
})(TransactionTemplateDetail);


TransactionTemplateDetail.propTypes = {
  getTransactionTemplateAction: PropTypes.func,
  getTransactionStepsAction: PropTypes.func,
  getStepAction: PropTypes.func,
  getAllGateways: PropTypes.func,
  template: PropTypes.object,
  stepsCount: PropTypes.number,
  gateways: PropTypes.array, 
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    })
  })
};