import React, { Component } from "react";
import {
  Button,
  Col,
  ControlLabel,
  FormControl,
  FormGroup,
  Row,
  Form
} from "react-bootstrap";
import config from "config/";
import { getAllShops } from "actions/shops";
import { connect } from "react-redux";
import Joi from "joi-browser";
import { addTransactionAction } from "actions/transactions";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";
import { addParamsInUrl } from "helpers/addParamsInUrl";
import queryString from "query-string";

class NewTransactionForm extends Component {
  state = {
    data: {
      shop: "",
      cardNumber: "",
      verificationValue: "",
      nameOnCard: "",
      expMonth: "",
      expYear: "",
      type: "",
      shopSecret: "",
      shopHashkey: "",
      transactionId: ""
    },
    withCard: false,
    showMore: true,
    checkFlag: true,
    errorMessage: "",
    successMessage: "",
    errors: {},
    isLoading: false,
    isNeedTransactionId: false,
    testData: {
      cardNumber :"4242424242424242",
      nameOnCard : "DAN BALAN",
      expMonth : "12",
      expYear :"2020",
      verificationValue : "123",
      amount: 1000,
      currency: "RUB",
      description: "PEWPEW",
      billing_address: {
        first_name: "Ivan",
        last_name: "Vovanov",
        country: "US",
        city: "New York",
        address: "Groove st.",
        zip: "646464"
      },
      customer: {
        email: "mail@mail.com",
        ip: "200.200.200.200"
      }
    }
  };

  withCardSchema = {
    shop: Joi.string()
      .required()
      .min(0)
      .label("Shop"),
    shopSecret: Joi.string()
      .label("Shop Secret"),
    shopHashkey: Joi.string()
      .label("Shop Hash Key"),
    cardNumber: Joi.string()
      .min(12)
      .max(19)
      .creditCard()
      .required()
      .label("Card Number"),
    verificationValue: Joi.number()
      .required()
      .label("Security code"),
    nameOnCard: Joi.string()
      .required()
      .min(0)
      .max(150)
      .label("Name on card"),
    expMonth: Joi.number()
      .positive()
      .min(1)
      .max(12)
      .required()
      .label("Expiry month"),
    expYear: Joi.number()
      .positive()
      .min(2019)
      .required()
      .label("Expiry year"),
    type: Joi.string()
      .required()
      .label("Transaction type"),
    transactionId: Joi.string()
      .label("Transaction ID")
  };

  withoutCardSchema = {
    shop: Joi.string()
      .required()
      .min(0)
      .label("Shop"),
    shopSecret: Joi.string()
      .label("Shop Secret"),
    shopHashkey: Joi.string()
      .label("Shop Hash Key"),
    cardNumber: Joi.string().allow(""),
    verificationValue: Joi.string().allow(""),
    nameOnCard: Joi.string().allow(""),
    expMonth: Joi.string().allow(""),
    expYear: Joi.string().allow(""),
    type: Joi.string()
      .required()
      .label("Transaction type"),
    transactionId: Joi.string()
      .label("Transaction ID")
      .allow("")
      .optional()
  }

  async componentDidMount() {
    await this.props.getAllShops();  
    const values = queryString.parse(window.location.hash.split("?")[1]);
    if(values.status === "succeed")
      swal({
        title: "Transaction submitted",
        text: values.guid ? `Transaction guid: ${values.guid}` : "Success",
        icon: "success"
      });
    else if(values.status === "failed")
      swal({
        title: "Transaction failed",
        text: values.guid ? `Transaction guid: ${values.guid}` : "Failed",
        icon: "error"
      });
    window.location.replace(window.location.href.split("?")[0]);
  }

  validate = () => {
    const options = { abortEarly: false };
    const schema = this.state.withCard ? this.withCardSchema : this.withoutCardSchema;
    const { error } = Joi.validate(this.state.data, schema, options);
    if (!error) return null;

    const errors = {};
    for (let item of error.details) errors[item.path[0]] = item.message;
    return errors;
  };

  formValidation = (errors) => {
    if (errors) return "error";
    else return "success";
  };

  validateProperty = ({ name, value }) => {
    let schema = this.state.withCard ? this.withCardSchema : this.withoutCardSchema;
    const obj = { [name]: value };
    schema = { [name]: schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };



  handleChange = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];

    const data = { ...this.state.data };
    data[input.name] = input.value;
    if([ "Refund", "Capture" ].includes(input.value))
      this.setState({ 
        isNeedTransactionId: true,
        withCard: false
      });
    else if([ "Authorization", "Payment" ].includes(input.value))
      this.setState({ isNeedTransactionId: false });
    this.setState({ data, errors });
  };

  doChange = e => {
    const property = e.target.name;
    this.setState({ [property]: e.target.value });
  };

  handleCheck = () => {
    const { data } = this.state;
    this.setState({
      checkFlag: !this.state.checkFlag
    });
    if (this.state.checkFlag && this.state.withCard)
    {
      this.setState({
        data: {
          cardNumber : this.state.testData.cardNumber,
          nameOnCard :  this.state.testData.nameOnCard,
          expMonth : this.state.testData.expMonth,
          expYear : this.state.testData.expYear,
          verificationValue : this.state.testData.verificationValue,
          shop: data.shop, 
          type: data.type,
          shopSecret: data.shopSecret,
          shopHashkey: data.shopHashkey
        }
      }); 
    }
    else {
      this.setState({
        data: {
          cardNumber :"",
          nameOnCard : "",
          expMonth : "",
          expYear :"",
          verificationValue : "",
          shop: data.shop,
          type: data.type,
          shopSecret: data.shopSecret,
          shopHashkey: data.shopHashkey
        }
      }); 
    }
     
  }

  handleChangeCardData = () => {
    const { data } = this.state;
    this.setState({
      withCard: !this.state.withCard,
      checkFlag: !this.state.withCard,
      data: {
        cardNumber :"",
        nameOnCard : "",
        expMonth : "",
        expYear :"",
        verificationValue : "",
        shop: data.shop,
        type: data.type,
        shopSecret: data.shopSecret,
        shopHashkey: data.shopHashkey
      }
    }); 
  }

  handleShowMore = () => {
    this.setState({
      showMore: !this.state.showMore
    });
  }

  formUrlEncoded = x => Object.keys(x).reduce((p, c) => p + `&${c}=${encodeURIComponent(x[c])}`, "");

  handleSubmit = async e => {
    e.preventDefault();

    const errors = this.validate();
    this.setState({ errors: errors || {} });
    if (errors) return;

    try {
      const credit_card = {
        number: this.state.data.cardNumber,
        verification_value: this.state.data.verificationValue,
        holder: this.state.data.nameOnCard,
        exp_month: this.state.data.expMonth,
        exp_year: this.state.data.expYear
      };
      let data = {
        shopGuid: this.state.data.shop,
        amount: this.state.testData.amount,
        currency: this.state.testData.currency,
        description: this.state.testData.description,
        type: this.state.data.type,
        transactionId: this.state.data.transactionId,
        billing_address: {
          first_name: this.state.testData.billing_address.first_name,
          last_name: this.state.testData.billing_address.last_name,
          country: this.state.testData.billing_address.country,
          city: this.state.testData.billing_address.city,
          address: this.state.testData.billing_address.address,
          zip: this.state.testData.billing_address.zip
        },
        customer: {
          email: this.state.testData.customer.email,
          ip: this.state.testData.customer.ip
        }
      };
      if(this.state.withCard)
        data.credit_card = credit_card;
      else {
        data.succeed_url = `${window.location}?status=succeed`;
        data.failed_url = `${window.location}?status=failed`;
      }

      this.setState({ isLoading: true });
      await this.props.addTransactionAction(data, this.state.data.shop, this.state.data.shopSecret, this.state.data.shopHashkey);
      this.setState({ isLoading: false });
      let transactionGuid = "Transaction guid: " + this.props.transaction.guid;
      if(this.props.transaction.status === "Failed") {
        swal({
          title: "Transaction failed",
          text: transactionGuid,
          icon: "error"
        });
      }
      else if(this.props.transaction.redirect_to && this.props.transaction.redirect_to.url && this.props.transaction.redirect_to.params) {
        const redirectUrlWithoutParams = `${config.node.host}/api/v1${this.props.transaction.redirect_to.url}`;
        const redirectUrl = addParamsInUrl(redirectUrlWithoutParams, this.props.transaction.redirect_to.params);
        window.location.href = redirectUrl;
      }
      else {
        swal({
          title: "Transaction submitted",
          text: transactionGuid,
          icon: "success"
        });
      }
    } catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error"
      });
    }
  };

  render() {
    const shops = this.props.shops ? this.props.shops : [];
    const { errors } = this.state;
    return (
      <React.Fragment>
        <div className="content" style={{ paddingTop:"0px" }}>
          <div className="card card-grid">
            <div className="content">
              <Form onSubmit={this.handleSubmit} autoComplete="off">
                <Row>
                  <Button onClick={this.handleShowMore} style={{ marginLeft: "10px" }}>Show more</Button>
              
                  {!this.state.showMore ?           
                    (<React.Fragment>     
                      <div className="content"> 
                        <h5 style={{ marginTop:"0px" }}>Billing Data</h5>
                        <Row>
                          <Col md={2}>
                            <ControlLabel>First Name</ControlLabel>
                            <FormGroup><span>{this.state.testData.billing_address.first_name}</span></FormGroup>
                          </Col>
                          <Col md={2}>
                            <ControlLabel>Last Name</ControlLabel>
                            <FormGroup><span>{this.state.testData.billing_address.last_name}</span></FormGroup>
                          </Col>
                          <Col md={2}>
                            <ControlLabel>Country</ControlLabel>
                            <FormGroup><span>{this.state.testData.billing_address.country}</span></FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={2}>
                            <ControlLabel>City</ControlLabel>
                            <FormGroup><span>{this.state.testData.billing_address.city}</span></FormGroup>
                          </Col>

                          <Col md={2}>
                            <ControlLabel>Address</ControlLabel>
                            <FormGroup><span>{this.state.testData.billing_address.address}</span></FormGroup>
                          </Col>

                          <Col md={2}>
                            <ControlLabel>Zip</ControlLabel>
                            <FormGroup><span>{this.state.testData.billing_address.zip}</span></FormGroup>
                          </Col>
                        </Row>
                      </div> 
          
                      <div className="content"> 
                        <h5 style={{ marginTop:"0px" }}>Customer Data</h5>
                        <Row>
                          <Col md={2}>
                            <ControlLabel>Email</ControlLabel>
                            <FormGroup><span>{this.state.testData.customer.email}</span></FormGroup>
                          </Col>

                          <Col md={2}>
                            <ControlLabel>IP</ControlLabel>
                            <FormGroup><span>{this.state.testData.customer.ip}</span></FormGroup>
                          </Col>
                        </Row>
                      </div> 

                      <div className="content"> 
                        <h5 style={{ marginTop:"0px" }}>Payment Data</h5>
                        <Row>
                          <Col md={2}>
                            <ControlLabel>Amount</ControlLabel>{" "}
                            <FormGroup><span>{this.state.testData.amount}</span></FormGroup>
                          </Col>

                          <Col md={2}>
                            <ControlLabel>Description</ControlLabel>
                            <FormGroup><span>{this.state.testData.description}</span></FormGroup>
                          </Col>

                          <Col md={2}>
                            <ControlLabel>Currency</ControlLabel>
                            <FormGroup><span>{this.state.testData.currency}</span></FormGroup>
                          </Col>
                        </Row>
                      </div> 
                    </React.Fragment>) : null}



                  <div className="card-container">
                    <Col md={3}>
                      <ControlLabel>Shop *</ControlLabel>
                      <FormGroup validationState={this.formValidation(errors.shop)}>
                        <FormControl
                          name="shop"
                          componentClass="select"
                          placeholder="select"
                          onChange={this.handleChange}
                        >
                          <option value="">Select shop</option>
                          {shops.map(shop => {
                            return (
                              <option key={shop.guid} value={shop.guid}>
                                {shop.guid} / {shop.name}
                              </option>
                            );
                          })}
                        </FormControl>
                        {errors.shop && <span className="validate-error">{errors.shop}</span>}
                      </FormGroup>
                    </Col>
            
                    <Col md={3}>
                      <ControlLabel>Shop secret </ControlLabel>
                      <FormGroup>
                        <FormControl
                          name="shopSecret"
                          value={this.state.shopSecret}
                          onChange={this.handleChange}
                        />
                      </FormGroup>
                    </Col>
                    <Col md={3}>
                      <ControlLabel>Hash key </ControlLabel>
                      <FormGroup>
                        <FormControl
                          name="shopHashkey"
                          value={this.state.shopHashkey}
                          onChange={this.handleChange}
                        />
                      </FormGroup>
                    </Col>
                    <Col md={3}>
                      <ControlLabel>Transaction Type *</ControlLabel>
                      <FormGroup validationState={this.formValidation(errors.type)}>
                        <FormControl
                          name="type"
                          componentClass="select"
                          placeholder="select"
                          onChange={this.handleChange}
                        >
                          <option value="">Select transaction type</option>
                          <option value="Payment">Payment</option>
                          <option value="Authorization">Authorization</option>                          
                          <option value="Capture">Capture</option>
                          <option value="Refund">Refund</option>
                        </FormControl>
                        {errors.type && <span className="validate-error">{errors.type}</span>}
                      </FormGroup>
                    </Col>
                  </div>
                  {this.state.isNeedTransactionId &&
                    <div className="card-container">
                      <Col md={3}>
                        <ControlLabel>Transaction id </ControlLabel>
                        <FormGroup>
                          <FormControl
                            name="transactionId"
                            value={this.state.data.transactionId}
                            onChange={this.handleChange}
                          />
                        </FormGroup>
                      </Col>
                    </div>}
                  {!this.state.isNeedTransactionId &&
                  <>
                  <div className="card-grid" style={{ marginLeft: "20px" }}>
                    <input type="checkbox" id="applyTestAccountCheckbox" onChange={this.handleChangeCardData} />{" "}
                    <label>Enter card data * </label>{" "}
                    <i className="far fa-question-circle"
                      title="You can enter card data here or on redirected page"
                    />
                  </div>
                  {this.state.withCard &&
                  <>
                  <div className="card-grid" style={{ marginLeft: "20px" }}>
                    <input type="checkbox" id="applyTestAccountCheckbox" onChange={this.handleCheck} />{" "}
                    <label>Apply test account settings * </label>{" "}
                    <i className="far fa-question-circle"
                      title="Credit card data will be automatically taken from Test
                      Account Settings section of shop settings."
                    />
                  </div>
                  <div className="card">
                    <div className="content">
                      <h5 style={{ marginTop:"0px" }}>Credit Card Data</h5>
                      <Row>
                        {/* <Col md={5} /> */}
                        <Col md={3}>
                          <ControlLabel>Card Number *</ControlLabel>{" "}
                          <i className="far fa-question-circle help-tip">
                            <p>
                            16-digit number on the front of your credit card.
                            </p>
                          </i>
                          <FormGroup validationState={this.formValidation(errors.cardNumber)}>
                            <FormControl
                              name="cardNumber"
                              type="number"
                              value={this.state.data.cardNumber}
                              placeholder="0000 0000 0000 0000"
                              className="no-spinners"
                              onChange={this.handleChange}
                            />
                            {/* <span style={{color:"lightgrey", marginTop:"10px"}}>
                            <em>16-digit number on the front of your credit card</em>
                          </span> */}
                            {errors.cardNumber && (
                              <span className="validate-error">{errors.cardNumber}</span>
                            )}
                          </FormGroup>
                        </Col>
                        {/* <Col md={5} /> */}
                        <Col md={3}>
                          <ControlLabel>Full name on card * </ControlLabel>
                          <FormGroup validationState={this.formValidation(errors.nameOnCard)}>
                            <FormControl
                              name="nameOnCard"
                              value={this.state.data.nameOnCard}
                              placeholder="ex. John Doe"
                              onChange={this.handleChange}
                            />
                            {errors.nameOnCard && (
                              <span className="validate-error">{errors.nameOnCard}</span>
                            )}
                          </FormGroup>
                        </Col>
                        {/* <Col md={5} /> */}
                        <Col md={2}>
                          <ControlLabel>Expiry month *</ControlLabel>
                          <FormGroup validationState={this.formValidation(errors.expMonth)}>
                            <FormControl
                              name="expMonth"
                              value={this.state.data.expMonth}
                              maxLength="2"
                              onChange={this.handleChange}
                              placeholder="MM"
                            />
                            {errors.expMonth && <span className="validate-error">{errors.expMonth}</span>}
                          </FormGroup>
                        </Col>

                        <Col md={2}>
                          <ControlLabel>Expiry year *</ControlLabel>
                          <FormGroup validationState={this.formValidation(errors.expYear)}>
                            <FormControl
                              name="expYear"
                              value={this.state.data.expYear}
                              maxLength="4"
                              placeholder="YYYY"
                              onChange={this.handleChange}
                            />
                            {errors.expYear && <span className="validate-error">{errors.expYear}</span>}
                          </FormGroup>
                        </Col>
                        <Col md={2}>
                          <ControlLabel>Security code * </ControlLabel>{" "}
                          <i className="far fa-question-circle help-tip">
                            <p>3 digits displayed on the back of your card.</p>
                          </i>
                          <FormGroup validationState={this.formValidation(errors.verificationValue)}>
                            <FormControl
                              name="verificationValue"
                              value={this.state.data.verificationValue}
                              placeholder="***"
                              maxLength="3"
                              onChange={this.handleChange}
                            />
                            {errors.verificationValue && (
                              <span className="validate-error">{errors.verificationValue}</span>
                            )}
                          </FormGroup>
                        </Col>
                        {/* <Col md={3} /> */}
                      </Row>
                    </div>
                  </div>
                  </>}
                  </>}
                  <div align="center">
                    {this.state.isLoading
                      ? <ReactLoading type='cylon' color='grey'/>
                      : <Button className="btn btn-primary" type="submit">
                        Send
                      </Button>}
                  </div>
                </Row>
              </Form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    shops: state.shops.shopsList,
    transaction: state.transactions.transaction,
    status: state.transactions.status
  };
};

export default connect(
  mapStateToProps,
  {
    getAllShops,
    addTransactionAction
  }
)(NewTransactionForm);

NewTransactionForm.propTypes = {
  shops:  PropTypes.array,
  getAllShops: PropTypes.func,
  addTransactionAction: PropTypes.func,
  transaction: PropTypes.object,
};