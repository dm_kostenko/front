import React from "react";
import StepItem from "./Item";
import PropTypes from "prop-types";

export default class StepList extends React.Component {
  render() {

    var items = this.props.items.map((item, index) => {
      return (
        <StepItem key={index} item={item} index={index} removeItem={this.props.removeItem} markTodoDone={this.props.markTodoDone} />
      );
    });
    return (
      <div style={{ paddingTop: "20px", width: "90%", margin: "auto", textAlign: "center" }}>
        <h3>Steps</h3>
        <ul className="list-group"> {items} </ul>
      </div>
    );
  }
}

StepList.propTypes = {
  items: PropTypes.array,
  removeItem: PropTypes.func,
  markTodoDone: PropTypes.func,
};
