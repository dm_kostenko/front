import React from "react";
import { Row, Col, ControlLabel } from "react-bootstrap";
import PropTypes from "prop-types";

export default class StepItem extends React.Component {

  onClickClose = () => {
    var index = parseInt(this.props.index);
    this.props.removeItem(index);
  }
  
  render() {
    return (
      <div className="card">
        <button type="button" className="close" style={{ color: "red" }} onClick={this.onClickClose}>&times;</button><br/>
        <Row>
          <Col>
            <h4 style={{ margin: "auto" }}>{"Step " + this.props.item.number}</h4>
          </Col>
        </Row>
        <Row>
          <h7>Name</h7>
        </Row>
        <Row>
          <ControlLabel>{this.props.item.name}</ControlLabel>
        </Row>
        <Row>
          <h7>Gateway</h7>
        </Row>
        <Row>
          <ControlLabel>{this.props.item.gateway_guid}</ControlLabel>
        </Row>
        <Row>
          <h7>Params</h7>
        </Row>
        {this.props.item.params.map((item, index )=> {
          return (
            <Row key={index} >
              <ControlLabel>{item.name}</ControlLabel>
            </Row>
          );
        })}
      </div>
    );
  }
}

StepItem.propTypes = {
  removeItem: PropTypes.func,
  index: PropTypes.number,
  params: PropTypes.array,
  item: PropTypes.object,
};
