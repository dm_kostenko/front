import React from "react";
import Joi from "joi-browser";
import { connect } from "react-redux";
import CustomForm from "components/UI/Form";
import { showModal } from "actions/modal";
import { Form } from "react-bootstrap";
import { addParamTransaction } from "actions/transactions";
import { parseResponse } from "helpers/parseResponse";
import swal from "sweetalert";

class ParamManager extends CustomForm {
  state = {
    data: {
      name: "",
      value: ""
    },
    errors: {}
  };

  schema = {
    name: Joi.string()
      .required()
      .min(0)
      .max(100)
      .label("Name"),
    value: Joi.string()
      .required()
      .min(0)
      .label("Value")
  };

  doSubmit = async () => {
    try {
      await this.props.addParamTransaction(this.state.data);
      await this.props.showModal(false);
    } catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  render() {
    return (
      <div className="card">
        <div className="content">
          <Form onSubmit={this.handleSubmit}>
            {this.renderInput("name", "Name")}
            {this.renderTextarea("value", "Value")}
            <div align="center">
              {this.renderButton("Add")}
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  {
    showModal,
    addParamTransaction
  }
)(ParamManager);