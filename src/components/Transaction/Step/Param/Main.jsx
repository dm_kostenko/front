import React, { Component } from "react";
import { getParamsForStepAction } from "actions/steps";
import { parseResponse } from "helpers/parseResponse";
import swal from "sweetalert";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Badge } from "react-bootstrap";

class Params extends Component {
  state = {
    isSecretCopied: false
  }

  componentDidMount = async () => {
    try {
      await this.props.getParamsForStepAction(this.props.guid);
    } catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  render() {
    const params = this.props.params;
    return (
      params.length > 0 ?
        <div>{params.map((param, index) =>
          <div className="card" key={index}>
            <div className="header">{param.name}</div>
            <div
              className="param-container"
            >
              <span style={{ wordWrap: "break-word", whiteSpace: "pre-wrap" }}>{JSON.stringify(JSON.parse(param.value), null, "\t")}</span>
            </div>
          </div>
        )}
        </div> :
        <div
          style={{
            width: "100%",
            height: "60px",
            textAlign: "center",
            paddingTop: "13%",
            paddingBottom: "13%",
            marginTop: "0",
            borderTopLeftRadius: "0",
            borderTopRightRadius: "0"
          }}
        >
          <Badge
            pill
            style={{
              position: "absolute",
              left: "calc(50% - 77px)",
              fontSize: "1.2vw"
            }}
          >
            There is no data
          </Badge>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    params: state.steps.paramsList
  };
};

export default connect(mapStateToProps, {
  getParamsForStepAction
})(Params);

Params.propTypes = {
  params: PropTypes.array,
  getParamsForStepAction: PropTypes.array,
  guid: PropTypes.string
};