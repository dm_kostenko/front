import React from "react";
import { connect } from "react-redux";
import { Row, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import Modal from "components/UI/Modal";
import { getAllGateways } from "actions/gateways";
import ParamManager from "./Param/Manager";
import swal from "sweetalert";
import PropTypes from "prop-types";

class StepForm extends React.Component {
  state = {
    number: "",
    name: "",
    gateway: ""
  }

  handleChange = (e) => {
    const property = e.target.name;
    this.setState({ [property]: e.target.value });
  };

  componentDidMount() {
    this.props.getAllGateways();
  }

  onSubmit = event => {
    event.preventDefault();
    const name = this.state.name;
    const number = this.state.number;
    const params = this.props.params;
    const gateway = this.state.gateway;
    let data = {};
    if (name!=="" && number!=="" && params.length!==0 && gateway!=="") {
      data = { number, name, gateway, params };
      this.props.addItem(data);
      this.setState({
        number: "",
        name: ""
      });
    }
    else 
      swal({
        title: "Please, enter all fields in form \"Add step\"",
        icon: "warning"
      });
  }

  handleSelect = e => {
    this.setState({
      gateway: e.target.value
    });
  }

  render() {
    const gateways = this.props.gateways ? this.props.gateways : [];

    return (
      <div style={{ width: "80%", margin: "auto", textAlign: "center" }}>
        <form>
          <Row>
            <h2>Add step</h2>
          </Row>
          <Row>
            <ControlLabel>Number of step*:</ControlLabel>
            <FormGroup>
              <FormControl
                onChange={this.handleChange}
                name="number"
                type="number"
                value={this.state.number}
              />
            </FormGroup>
          </Row>
          <Row>
            <ControlLabel>Name*:</ControlLabel>
            <FormGroup>
              <FormControl
                onChange={this.handleChange}
                name="name"
                type="text"
                value={this.state.name}
              />
            </FormGroup>
          </Row>
          <Row>
            <ControlLabel>Parameters:</ControlLabel>
          </Row>
          <Row>
            {this.props.params.map((item, index) => {
              return (
                <Row key={index}>
                  <ControlLabel style={{ color:"black", marginBottom:"30px" }}>{item.name}</ControlLabel>
                </Row>
              );
            })}
            <Modal
              content={<ParamManager/>}
              button={<i className="fas fa-plus" style={{ cursor: "pointer", color: "green" }}/>} />
          </Row>
          <Row>
            <ControlLabel style={{ marginTop:"10px" }} >Gateway</ControlLabel>
            <FormGroup>
              <FormControl
                name="gateway"
                id="gateway"
                onChange={this.handleSelect}
                componentClass="select"
              >
                <option value="">{gateways.name}</option>
                {gateways.map(gateway => (
                  <option key={gateway.guid} value={gateway.guid}>
                    {gateway.name}
                  </option>
                ))}
              </FormControl>
            </FormGroup>
          </Row>
          <button type="submit" onClick={this.onSubmit} className="btn btn-default">Add</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    gateways: state.gateways.gatewaysList,
    params: state.transactions.paramsList
  };
};

export default connect(
  mapStateToProps,
  {
    getAllGateways
  }
)(StepForm);

StepForm.propTypes = {
  params: PropTypes.array,
  gateways: PropTypes.array,
  getAllGateways: PropTypes.func,
  addItem: PropTypes.func,
};