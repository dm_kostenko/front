import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Button } from "react-bootstrap";
import { getTransactionProcessingParamsAction } from "actions/transactions";
import Table from "components/UI/Table/index";
import Pagination from "components/UI/Pagination/index";
import PropTypes from "prop-types";

class HistoryParamsDetail extends Component {
    state = {
      count: "",
      currentPage: 1,
      pageSize: 10
    };

    columnsParams = [
      { path: "name", label: "Name" },
      { path: "value", label: "Value" }
    ];

    async componentDidMount() {
      await this.props.getTransactionProcessingParamsAction(this.props.match.params.id);
      this.setState({
        count: this.props.count
      });
    }

    handlePageChange = async (page) => {
      await this.props.getTransactionProcessingParamsAction(this.props.match.params.id, page, this.state.pageSize);
      this.setState({
        count: this.props.count,
        currentPage: page
      });
    };
    
    getPagedData = () => {
      const { count } = this.state;
      let { pageSize } = this.state;
      const pagesCount = count ? (count / pageSize) + (1 && !!(count % pageSize)) : 0;
      return { pagesCount };
    };
    
    render() { 
      const params = this.props.params;
      const { currentPage } = this.state;
      const { pagesCount } = this.getPagedData();

        return ( 
            <>
            <Button onClick={this.props.history.goBack} className="detail-back-button"><i className="fas fa-arrow-left"></i></Button>
            <div className="content">
              <Row style={{ marginLeft: "0px" }}>
                <div className="card">
                    <div className="title">Step: {this.props.match.params.id}</div>
                </div>

            <div className="card-container">
              <div className="card">
                <div className="header">Params</div>
                <div className="content">
                  <Table
                    columns={this.columnsParams}
                    data={params}
                    disableSearch={true}
                  />
                  <Pagination
                    pagesCount={pagesCount}
                    currentPage={currentPage}
                    onPageChange={this.handlePageChange}
                  />
                </div>
              </div>
            </div>
          </Row>
        </div>
        </>
      );
    }
}

const mapStateToProps = (state) => {
  return {
    params: state.transactions.transactionProcessingParams,
    count: state.transactions.transactionProcessingParamsCount
  };
};
  
export default connect(mapStateToProps, {
  getTransactionProcessingParamsAction
})(HistoryParamsDetail);

HistoryParamsDetail.propTypes = {
  getTransactionProcessingParamsAction: PropTypes.func,
  count: PropTypes.number,
  params: PropTypes.array,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    })
  })
};