import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "factories/Main";
import { getAllTransactions } from "actions/transactions";
import { searchInTransactions, reset } from "actions/search";
import { Link } from "react-router-dom";
import moment from "moment";

const columns = [
  { path: "guid", 
    label: "ID",
    key: "guid",
    content: transaction => (
      <Link className="link" to={`/about/processing/${transaction.guid}`} >{transaction.guid}</Link>
    ), },
  { path: "transaction_type", label: "Type" },
  { 
    path: "status",
    key: "status",
    label: "Status",
    content: transaction => (
      // StatusIcon(null, transaction.status)
      <i className={transaction.status === "Success" ? "far fa-check-circle green" : "far fa-times-circle red"} />
    )
  },
  {
    path: "amount",
    key: "amount",
    label: "Amount",
    content: transaction => transaction.amount / 100
  },
  {
    path: "currency",
    label: "Currency"
  },
  { path: "account_guid", label: "Account" },
  { path: "created_at", label: "Created at", content: transaction => (moment(transaction.created_at).utcOffset(0).format("DD.MM.YYYY HH:mm")) },
  { path: "created_by", label: "Created by" },
];

const mapStateToProps = (state) => {
  return {
    data: state.transactions.transactionsList,
    count: state.transactions.count,
    loading: state.transactions.loading,
    searchData: state.search.transactionsSearch,
    isSearch: state.search.isSearch,
    name:"transaction history",
    columns,
  };
};

export default connect(mapStateToProps, {
  get: getAllTransactions,
  search: searchInTransactions,
  reset
})(AbstractComponent);