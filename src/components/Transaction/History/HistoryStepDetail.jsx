import React, { Component } from "react";
import { connect } from "react-redux";
import { getTransactionProcessingStepsAction } from "actions/transactions";
import Table from "components/UI/Table/index";
import Pagination from "components/UI/Pagination/index";
import { Link } from "react-router-dom";
import { Row, Button } from "react-bootstrap";
import moment from "moment";
import PropTypes from "prop-types";

class HistoryStepDetail extends Component {
  state = {
    count: "",
    currentPage: 1,
    pageSize: 10
  };

  columnsSteps = [
    {
      path: "guid",
      label: "ID",
      key: "guid",
      content: step => (
        <Link className="link" to={`/about/steps/${step.guid}`}>{step.guid}</Link>
      )
    },
    { path: "name", label: "Name" },
    { path: "type", label: "Type" },
    {
      label: "Created at",
      content: log => (
        moment(log.created_at).utcOffset(0).format("DD.MM.YYYY HH:mm:ss")
      )
    },
    {
      label: "Updated at",
      content: log => (
        moment(log.updated_at).utcOffset(0).format("DD.MM.YYYY HH:mm:ss")
      )
    },
    {
      label: "Duration",
      content: log => (
        log.updated_at ? moment(moment(log.updated_at) - moment(log.created_at)).utcOffset(0).format("mm:ss") : null
      )
    },
    { path: "status", label: "Status" }
  ];

  async componentDidMount() {
    await this.props.getTransactionProcessingStepsAction(this.props.match.params.id, 1, this.state.pageSize);
    this.setState({
      count: this.props.count
    });
  }

  handlePageChange = async (page) => {
    await this.props.getTransactionProcessingStepsAction(this.props.match.params.id, page, this.state.pageSize);
    this.setState({
      count: this.props.count,
      currentPage: page
    });
  };

  getPagedData = () => {
    const { count } = this.state;
    let { pageSize } = this.state;
    const pagesCount = count ? (count / pageSize) + (1 && !!(count % pageSize)) : 0;
    return { pagesCount };
  };

  render() {
    const steps = this.props.steps;
    const { currentPage } = this.state;
    const { pagesCount } = this.getPagedData();

    return (
      <>
      <Button onClick={this.props.history.goBack} className="detail-back-button"><i className="fas fa-arrow-left"></i></Button>
      <div className="content">
        <Row style={{ marginLeft: "0px" }}>
          <div className="card">
            <div className="title">Transaction guid: {this.props.match.params.id}</div>
          </div>

          <div className="card-container">
            <div className="card">
              <div className="header">Steps</div>
              <div className="content">
                <Table
                  columns={this.columnsSteps}
                  data={steps}
                  disableSearch={true}
                />
                <Pagination
                  pagesCount={pagesCount}
                  currentPage={currentPage}
                  onPageChange={this.handlePageChange}
                />
              </div>
            </div>
          </div>
        </Row>
      </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    steps: state.transactions.transactionProcessingSteps,
    count: state.transactions.transactionProcessingStepsCount
  };
};

export default connect(mapStateToProps, {
  getTransactionProcessingStepsAction
})(HistoryStepDetail);

HistoryStepDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  accountInfo: PropTypes.object,
  cards: PropTypes.array,
  count: PropTypes.number,
  getAccountAction: PropTypes.func,
  getCardsAccountAction: PropTypes.func,
  accountLoading: PropTypes.bool,
  getTransactionProcessingStepsAction: PropTypes.func,
  steps: PropTypes.array,
};