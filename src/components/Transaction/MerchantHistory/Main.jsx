import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import { getMerchantTransactionsAction } from "actions/transactions";
import { reset, searchInMerchantTransactions } from "actions/search";
import AbstractComponent from "factories/Main";


const columns = [
  { path: "date", label: "Date", content: transaction => (moment(transaction.date).utcOffset(0).format("DD.MM.YYYY HH:mm")) },
  { path: "type", label: "Type" },
  {
    path: "status",
    key: "status",
    label: "Status",
    content: transaction => (
      // StatusIcon(null, transaction.status)
      <i className={transaction.status === "Success" ? "far fa-check-circle green" : "far fa-times-circle red"} />
    )
  },
  { path: "account_number", label: "Account" },
];


const mapStateToProps = (state) => {
  return {
    data: state.transactions.transactionsList,
    count: state.transactions.count,
    searchData: state.search.merchantTransactionsSearch,
    isSearch: state.search.isSearch,
    name: "merchant transaction history",
    columns
  };
};

export default connect(mapStateToProps, {
  get: getMerchantTransactionsAction,
  search: searchInMerchantTransactions,
  reset
})(AbstractComponent);