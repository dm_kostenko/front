import React from "react";
import { connect } from "react-redux";
import { getNewLoginsCountAction } from "../../actions/logins";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";

class NewLoginsCount extends React.Component {
  state = {
    daysCount: 7
  }

  componentDidMount = () => {
    this.props.getNewLoginsCountAction(this.state.daysCount);
  }

  render() {
    if (this.props.loading) return <Spinner/>;
    else if (this.props.newLoginsCount !== undefined)
      return (
        <div>
          <div className="header">New clients</div>
          <div className="card-large-content">+ {this.props.newLoginsCount}</div>
        </div>
      );
    return null;
  }
}

const mapStateToProps = (state) => {
  return {
    newLoginsCount: state.logins.newLoginsCount,
    loading: state.logins.newLoginsCountLoading
  };
};

export default connect(mapStateToProps, {
  getNewLoginsCountAction
})(NewLoginsCount);

NewLoginsCount.propTypes = {
  newLoginsCount: PropTypes.number,
  getNewLoginsCountAction: PropTypes.func,
  loading: PropTypes.bool,
};