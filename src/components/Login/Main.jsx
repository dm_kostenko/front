import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../factories/Main";
import LoginCreator from "./Creator";
import ability from "config/ability";
import { getAllLogins, deleteLoginAction } from "../../actions/logins";
import { searchInLogins, reset } from "../../actions/search";
import LoginEditor from "./Editor";
import Modal from "components/UI/Modal";
import { Link } from "react-router-dom";

const columns = [
  {
    path: "guid",
    label: "ID",
    key: "guid",
    content: login => (
      <Link className="link" to={`/about/login/${login.guid}`} >{login.guid}</Link>
    ),
  },
  { path: "username", label: "Name" },
  { path: "email", label: "Email" },
  {
    key: "enabled",
    content: login => (
      <i className={login.enabled === 1 ? "far fa-check-circle green" : "far fa-times-circle red"} />
    ),
    label: "Status"
  },
  ability.can("EXECUTE", "LOGINS") &&
  {
    key: "edit",
    content: login => (
      <Modal 
        header="Edit login"
        content={<LoginEditor login_guid={login.guid} />} 
        button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }}/>}
      />
    ),
    label: "Edit"
  },
  ability.can("EXECUTE", "LOGINS") &&
  {
    key: "delete",
    content: () => (
      <i
        className="far fa-trash-alt"
        style={{ cursor: "pointer", color: "red" }}
      />
    ),
    label: "Delete"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.logins.loginsList,
    count: state.logins.count,
    searchData: state.search.loginsSearch,
    isSearch: state.search.isSearch,
    loading: state.logins.loginsLoading,
    name: "logins",
    columns,
    modal: <LoginCreator />
  };
};

export default connect(mapStateToProps, {
  get: getAllLogins,
  delete: deleteLoginAction,
  search: searchInLogins,
  reset
})(AbstractComponent);