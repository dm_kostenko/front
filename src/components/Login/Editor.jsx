import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Row, Form, ControlLabel, FormControl, Button, FormGroup } from "react-bootstrap";
import { getLoginAction, editLoginAction, getLoginUserProfileAction } from "actions/logins";
import { showModal } from "actions/modal";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class LoginEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      enabled: true,
      email: "",
      authType: false,
      phone: "",
      userProfileGuid: "",
      firstName: "",
      lastName: "",
      personalEmail: "",
      personalPhone: "",
      companyName: "",
      note: "",
      isLoading: false
    };
  }

  async componentDidMount() {
    try {
      await this.props.getLoginAction(this.props.login_guid);
      await this.props.getLoginUserProfileAction(this.props.login.user_profile_id);
      return this.setState(
        this.mapToViewModel(this.props.login, this.props.userprofile)
      );
    }
    catch (ex) {
      throw (ex);
    }
  }

  mapToViewModel(login, user_profile) {
    return {
      guid: login.guid,
      username: login.username,
      email: login.email,
      phone: login.phone,
      enabled: login.enabled === 0 ? false : true,
      authType: login.auth_type !== "login-password",
      userProfileGuid: user_profile.guid,
      firstName: user_profile.first_name,
      lastName: user_profile.last_name,
      personalEmail: user_profile.email,
      personalPhone: user_profile.phone,
      companyName: user_profile.company_name,
      note: user_profile.note
    };
  }

  handleChange = ({ currentTarget: input }) => {
    const data = { ...this.state.data };
    data[input.name] = input.value;

    this.setState(data);
  };

  handleChangeCheckbox = () => {
    this.setState({
      authType: !this.state.authType
    });
  }

  handleEnabledCheckbox = () => {
    this.setState({ enabled: !this.state.enabled });
  };

  formValidation = (data) => {
    if (data === "") return "error";
    else return "success";
  };

  generalValidation = () => {
    const data = this.state;
    return this.formValidation(data.username) === "success" &&
      this.formValidation(data.email) === "success" &&
      this.formValidation(data.phone) === "success";
  }


  personalDataValidation = () => {
    if
    (
      this.state.firstName === "" &&
      this.state.lastName === "" &&
      this.state.personalEmail === "" &&
      this.state.personalPhone === "" &&
      this.state.companyName === "" &&
      this.state.note === ""
    ) return "empty";
    else if
    (
      this.state.firstName !== "" &&
      this.state.lastName !== "" &&
      this.state.personalEmail !== "" &&
      this.state.personalPhone !== "" &&
      this.state.companyName !== "" &&
      this.state.note !== ""
    )
      return "ok";
    else return "error";
  }


  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    if (!this.generalValidation())
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    else {
      const data = this.state;
      try {
        switch (this.personalDataValidation()) {
          case "ok": {
            await this.props.editLoginAction({
              guid: this.props.login.guid,
              user_profile_guid: data.userProfileGuid,
              username: data.username,
              auth_type: data.authType ? "login-password-mail" : "login-password",
              username_canonical: data.username,
              email: data.email,
              email_canonical: data.email,
              enabled: data.enabled === true ? 1 : 0,
              phone: data.phone,
              user_profile: {
                first_name: data.firstName,
                last_name: data.lastName,
                email: data.personalEmail,
                phone: data.personalPhone,
                company_name: data.companyName,
                note: data.note
              }
            });
            swal({
              title: "Login is updated",
              icon: "success",
              button: false,
              timer: 2000
            });
            this.props.showModal(false);
            break;
          }
          case "empty": {
            await this.props.editLoginAction({
              guid: this.props.login.guid,
              username: data.username,
              auth_type: data.authType ? "login-password-mail" : "login-password",
              username_canonical: data.username,
              email: data.email,
              email_canonical: data.email,
              enabled: data.enabled === true ? 1 : 0,
              phone: data.phone,
            });
            swal({
              title: "Login is updated",
              icon: "success",
              button: false,
              timer: 2000
            });
            this.props.showModal(false);
            break;
          }
          case "error": {
            this.setState({ isLoading: false });
            swal({
              title: "Please, enter information in every required field of personal Info",
              icon: "warning"
            });
            break;
          }
          default: break;
        }
      }
      catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
    }
  }


  render() {
    const data = this.state;
    if (this.props.loginLoading) {
      return <Spinner />;
    }
    else return (
      <Form onSubmit={this.handleSubmit} autoComplete="off">
        <div className="card">
          <div className="content">
            <label>ID: {this.props.login_guid}</label>
          </div>
        </div>
        <h3 align='center'>General info</h3>
        <div className="card">
          <Col>
            <Row className="rowEdit" >
              <Col className='col-md-2'>
                <ControlLabel>Username:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <FormGroup validationState={this.formValidation(data.username)}>
                  <FormControl
                    name="username"
                    type="text"
                    value={data.username}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row className="rowEdit">
              <Col className='col-md-2'>
                <ControlLabel>2FA:</ControlLabel>
              </Col>
              <Col className='col-md-1'>
                <input type="checkbox" checked={this.state.authType} onChange={this.handleChangeCheckbox} />{" "}
              </Col>
            </Row>
            
            <Row className="rowEdit">
              <Col className='col-md-2'>
                <ControlLabel>Email:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <FormGroup validationState={this.formValidation(data.email)}>
                  <FormControl
                    name="email"
                    type="email"
                    value={data.email}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row className="rowEdit">
              <Col className='col-md-2'>
                <ControlLabel>Phone:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <FormGroup validationState={this.formValidation(data.phone)}>
                  <FormControl
                    name="phone"
                    type="text"
                    value={data.phone}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row className="rowEdit">
              <Col className='col-md-2'>
                <ControlLabel>Enable*:</ControlLabel>
              </Col>
              <Col className='col-md-1'>
                <input type="checkbox" id='lockedCheckbox' checked={data.enabled} onChange={this.handleEnabledCheckbox} />{" "}
              </Col>
            </Row>
          </Col>
        </div>


        <h3 align='center'>Personal data</h3>
        <div className="card">
          <Col>
            <Row className="rowEdit">
              <Col className='col-md-2'>
                <ControlLabel>Name:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <FormGroup validationState={this.formValidation(data.firstName)}>
                  <FormControl
                    name="firstName"
                    type="text"
                    value={data.firstName}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row className="rowEdit">
              <Col className='col-md-2'>
                <ControlLabel>Surname:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <FormGroup validationState={this.formValidation(data.lastName)}>
                  <FormControl
                    name="lastName"
                    type="text"
                    value={data.lastName}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row className="rowEdit">
              <Col className='col-md-2'>
                <ControlLabel>Email:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <FormGroup validationState={this.formValidation(data.personalEmail)}>
                  <FormControl
                    name="personalEmail"
                    type="email"
                    value={data.personalEmail}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row className="rowEdit">
              <Col className='col-md-2'>
                <ControlLabel>Phone:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <FormGroup validationState={this.formValidation(data.personalPhone)}>
                  <FormControl
                    name="personalPhone"
                    type="text"
                    value={data.personalPhone}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row className="rowEdit">
              <Col className='col-md-2'>
                <ControlLabel>Company:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <FormGroup validationState={this.formValidation(data.companyName)}>
                  <FormControl
                    name="companyName"
                    type="text"
                    value={data.companyName}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row className="rowEdit">
              <Col className='col-md-2'>
                <ControlLabel>Note:</ControlLabel>
              </Col>
              <Col className='col-md-9'>
                <FormGroup validationState={this.formValidation(data.note)}>
                  <FormControl
                    name="note"
                    type="textarea"
                    value={data.note}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
          </Col>
        </div>
        <div align="center">
          {this.state.isLoading
            ? <ReactLoading type='cylon' color='grey' />
            : <Button type="submit">Save</Button>}
        </div>
      </Form >
    );
  }
}

const mapStateToProps = (state) => {
  return {
    login: state.logins.login,
    loginLoading: state.logins.loginLoading,
    userprofile: state.logins.userProfile
  };
};

export default connect(mapStateToProps, {
  getLoginAction,
  editLoginAction,
  showModal,
  getLoginUserProfileAction
})(LoginEditor);

LoginEditor.propTypes = {
  loginLoading: PropTypes.bool,
  login_guid: PropTypes.string,
  login: PropTypes.array,
  getLoginAction: PropTypes.func,
  editLoginAction: PropTypes.func,
  showModal: PropTypes.func,
  getLoginUserProfileAction: PropTypes.func,
  userprofile: PropTypes.object,
};