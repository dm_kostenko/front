import React, { Component } from "react";
import { Row, Col, Badge, Button } from "react-bootstrap";
import Table from "components/UI/Table/index";
import { connect } from "react-redux";
import { getLoginAction, addLoginRoleAction, getLoginRolesAction, getLoginUserProfileAction } from "actions/logins";
import { getAllRoles } from "actions/roles";
import RowAddRole from "components/RowAddRole";
import Pagination from "components/UI/Pagination/index";
import moment from "moment";
import Can from "components/Can/index";
import ability from "config/ability";
import PropTypes from "prop-types";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse";
import Spinner from "components/UI/Spinner";

class LoginDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      pageSize: 10
    };
  }

  componentDidMount = async () => {
    await this.props.getLoginAction(this.props.match.params.id);
    await this.props.getAllRoles();
    await this.props.getLoginRolesAction(this.props.match.params.id, 1, this.state.pageSize);
    if (this.props.login.user_profile_id)
      await this.props.getLoginUserProfileAction(this.props.login.user_profile_id);
  };

  handlePageChange = async (page) => {
    await this.props.getLoginRolesAction(this.props.match.params.id, page, this.state.pageSize);
    this.setState({
      currentPage: page
    });
  };

  handleDelete = async (role_guid) => {
    try {
      const data = {
        guid: role_guid,
        delete: "true"
      };
      await this.props.addLoginRoleAction(this.props.match.params.id, data);
      await this.props.getLoginRolesAction(this.props.match.params.id);
    }
    catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };

  columnsRoles = [
    { path: "name", label: "Name" },
    { path: "description", label: "Description" },
    ability.can("EXECUTE", "LOGINDETAIL") &&
    {
      key: "delete",
      content: role => (
        <i
          className="far fa-trash-alt"
          style={{ cursor: "pointer", color: "red" }}
          onClick={() =>
            swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
              .then((willDelete) => {
                if (willDelete) {
                  this.handleDelete(role.guid);
                  swal("Deleted", {
                    icon: "success",
                    button: false,
                    timer: 2000
                  });
                }
              })
          }
        />
      ),
      label: "Delete"
    }
  ];

  getPagedData = () => {
    const { pageSize } = this.state;
    const loginRoles = this.props.loginRoles ? this.props.loginRoles : [];
    const count = this.props.count;
    const pagesCount = (count / pageSize) + (1 && !!(count % pageSize));
    return { pagesCount, data: loginRoles };
  };

  render() {
    if (this.props.loginLoading || this.props.rolesLoading || this.props.loginRolesLoading) return <Spinner />;
    else {
      const { currentPage } = this.state;
      const { login, user_profile } = this.props;
      const { pagesCount, data: loginRoles } = this.getPagedData();
      return (
        <>
        <Button onClick={this.props.history.goBack} className="detail-back-button"><i className="fas fa-arrow-left"></i></Button>
        <div className="content" style={{ overflow: "auto" }}>
          <div className="card" style={{ marginRight: "5px" }}>
            <div xs={12} className="title">{login ? login.username : null}</div>
          </div>
          <Row style={{ marginLeft: "0px" }}>
            <div className="card-container">
              <div className="card">
                <div className="header">Registration info</div>
                <div className="content">
                  <Row>
                    <Col md={6} sm={6} xs={6} className="right"><label>GUID:</label></Col>
                    <Col md={6} sm={6} xs={6}><span> {this.props.match.params.id}</span></Col>
                  </Row>
                  <Row>
                    <Col md={6} sm={6} xs={6} className="right"><label>Enabled:</label></Col>
                    <Col md={6} sm={6} xs={6}>{login.enabled ? <span>true</span> : <span>false</span>}</Col>
                  </Row>
                  <Row>
                    <Col md={6} sm={6} xs={6} className="right"><label>Locked:</label></Col>
                    <Col md={6} sm={6} xs={6}>{login.locked ? <span>true</span> : <span>false</span>}</Col>
                  </Row>
                  <Row>
                    <Col md={6} sm={6} xs={6} className="right"><label>Enabled api:</label></Col>
                    <Col md={6} sm={6} xs={6}>{login.enabled_api ? <span>true</span> : <span>false</span>}</Col>
                  </Row>
                  <Row>
                    <Col md={6} sm={6} xs={6} className="right"><label>Expired:</label></Col>
                    <Col md={6} sm={6} xs={6}>{login.expired ? <span>true</span> : <span>false</span>}</Col>
                  </Row>
                  <Row>
                    <Col md={6} sm={6} xs={6} className="right"><label>Expires at:</label></Col>
                    <Col md={6} sm={6} xs={6}><span> {login.expires_at !== null ? moment(login.expires_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
                  </Row>
                  <Row>
                    <Col md={6} sm={6} xs={6} className="right"><label>Credentials expired:</label></Col>
                    <Col md={6} sm={6} xs={6}>{login.credentials_expired ? <span>true</span> : <span>false</span>}</Col>
                  </Row>
                  <Row>
                    <Col md={6} sm={6} xs={6} className="right"><label>Credentials expire at:</label></Col>
                    <Col md={6} sm={6} xs={6}><span> {login.credentials_expire_at !== null ? moment(login.credentials_expire_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
                  </Row>
                  <Row>
                    <Col md={6} sm={6} xs={6} className="right"><label>Created at:</label></Col>
                    <Col md={6} sm={6} xs={6}><span> {login.created_at !== null ? moment(login.created_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
                  </Row>
                  <Row>
                    <Col md={6} sm={6} xs={6} className="right"><label>Created by:</label></Col>
                    <Col md={6} sm={6} xs={6}><span>{login.created_by}</span></Col>
                  </Row>
                  <Row>
                    <Col md={6} sm={6} xs={6} className="right"><label>Updated at:</label></Col>
                    <Col md={6} sm={6} xs={6}><span>{login.updated_at !== null ? moment(login.updated_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>

                  </Row>
                  <Row>
                    <Col md={6} sm={6} xs={6} className="right"><label>Updated by:</label></Col>
                    <Col md={6} sm={6} xs={6}><span>{login.updated_by}</span></Col>
                  </Row>
                </div>
              </div>

              {login.user_profile_id
                ? <div className="card">
                  <div className="header">Personal info</div>
                  <div className="content">
                    <Row>
                      <Col md={6} sm={6} xs={6} className="right"><label>Name:</label></Col>
                      <Col md={6} sm={6} xs={6}><span>{user_profile.first_name}</span></Col>
                    </Row>
                    <Row>
                      <Col md={6} sm={6} xs={6} className="right"><label>Surname:</label></Col>
                      <Col md={6} sm={6} xs={6}><span>{user_profile.last_name}</span></Col>
                    </Row>
                    <Row>
                      <Col md={6} sm={6} xs={6} className="right"><label>Email:</label></Col>
                      <Col md={6} sm={6} xs={6}><span> {user_profile.email}</span></Col>
                    </Row>
                    <Row>
                      <Col md={6} sm={6} xs={6} className="right"><label>Phone:</label></Col>
                      <Col md={6} sm={6} xs={6}><span> {user_profile.phone}</span></Col>
                    </Row>
                    <Row>
                      <Col md={6} sm={6} xs={6} className="right"><label>Company name:</label></Col>
                      <Col md={6} sm={6} xs={6}><span> {user_profile.company_name}</span></Col>
                    </Row>
                    <Row>
                      <Col md={6} sm={6} xs={6} className="right"><label>Note:</label></Col>
                      <Col md={6} sm={6} xs={6}><span> {user_profile.note}</span></Col>
                    </Row>
                    <Row>
                      <Col md={6} sm={6} xs={6} className="right"><label>Created at:</label></Col>
                      <Col md={6} sm={6} xs={6} className="left"><span>{user_profile.created_at !== null ? moment(user_profile.created_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
                    </Row>
                    <Row>
                      <Col md={6} sm={6} xs={6} className="right"><label>Created by:</label></Col>
                      <Col md={6} sm={6} xs={6} className="left"><span>{user_profile.created_by}</span></Col>
                    </Row>
                    <Row>
                      <Col md={6} sm={6} xs={6} className="right"><label>Updated at:</label></Col>
                      <Col md={6} sm={6} xs={6} className="left"><span> {user_profile.updated_at !== null ? moment(user_profile.updated_at).utcOffset(0).format("D MMM YYYY") : ""}</span></Col>
                    </Row>
                    <Row>
                      <Col md={6} sm={6} xs={6} className="right"><label>Updated by:</label></Col>
                      <Col md={6} sm={6} xs={6} className="left"><span>{user_profile.updated_by}</span></Col>
                    </Row>
                  </div>
                </div>
                : <div className="card">
                  <div className="header">Personal info</div>
                  <div className="content">
                    <Badge
                      pill
                      style={{
                        position: "absolute",
                        left: "calc(50% - 77px)",
                        fontSize: "1.2vw"
                      }}
                    >
                      There is no data
                    </Badge>
                  </div>
                </div>}



            </div>
          </Row>
          <Row style={{ marginLeft: "0px" }}>
            <div className="card">
              <div className="header">Roles</div>
              <div className="content">
                <Row>
                  <Col md={3} sm={3} xs={3}></Col>
                  <Col md={6} sm={6} xs={6}>
                    <Table
                      columns={this.columnsRoles}
                      data={loginRoles ? loginRoles : null}
                      disableSearch={true}
                    />
                    <Pagination
                      pagesCount={pagesCount}
                      currentPage={currentPage}
                      onPageChange={this.handlePageChange}
                    />
                    <Can do="EXECUTE" on="LOGINDETAIL">
                      <RowAddRole loginGuid={this.props.match.params.id} />
                    </Can>
                  </Col>
                  <Col md={3} sm={3} xs={3}></Col>
                </Row>
              </div>
            </div>
          </Row>
        </div>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    login: state.logins.login,
    loginRoles: state.logins.loginRoles,
    count: state.logins.loginRolesCount,
    loginLoading: state.logins.loginLoading,
    rolesLoading: state.roles.rolesLoading,
    loginRolesLoading: state.logins.loginRolesLoading,
    user_profile: state.logins.userProfile
  };
};

export default connect(mapStateToProps, {
  getLoginAction,
  getLoginRolesAction,
  addLoginRoleAction,
  getAllRoles,
  getLoginUserProfileAction
})(LoginDetail);

LoginDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  login: PropTypes.object,
  user_profile: PropTypes.object,
  loginRoles: PropTypes.array,
  count: PropTypes.number,
  loginLoading: PropTypes.bool,
  rolesLoading: PropTypes.bool,
  loginRolesLoading: PropTypes.bool,
  getLoginUserProfileAction: PropTypes.func,
  getLoginAction: PropTypes.func,
  getLoginRolesAction: PropTypes.func,
  addLoginRoleAction: PropTypes.func,
  getAllRoles: PropTypes.func
};