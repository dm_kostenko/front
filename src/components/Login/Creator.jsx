import React, { Component } from "react";
import { Button, Col, ControlLabel, Form, FormControl, FormGroup, Row } from "react-bootstrap";
import { addLoginAction } from "../../actions/logins";
import { showModal } from "../../actions/modal";
import { generate } from "generate-password";
import { connect } from "react-redux";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class LoginCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      usernameCanonical: "",
      password: "",
      confirmPassword: "",
      showPassword: false,
      isPasswordConfirmed: false,
      email: "",
      personalEmail: "",
      emailCanonical: "",
      enabled: true,
      phone: "",
      personalPhone: "",
      firstName: "",
      lastName: "",
      companyName: "",
      note: "",
      isLocked: false,
      isLoading: false
    };
  }


  onChangeHandler = (e) => {
    const property = e.target.name;
    const value = e.target.value;
    this.setState({
      [property]: value
    });
  };

  handleExpiresDate = (date) => {
    this.setState({
      expiresAt: date,
    });
  };

  isPasswordConfirmed = () => {
    if (this.state.password === this.state.confirmPassword) {
      this.setState({
        isPasswordConfirmed: true
      });

    }
    else {
      this.setState({
        isPasswordConfirmed: false
      });
    }
  };

  handleEnabledCheckbox = () => {
    this.setState({
      enabled: !(this.state.enabled)
    });
  };

  handleShowPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword
    });
  }

  handleGenerate = () => {
    const password = generate({
      length: 10,
      numbers: true
    });
    this.setState({
      password,
      confirmPassword: password
    });
  };

  formValidation = (data) => {
    if (data === "") return "error";
    else return "success";
  };

  generalValidation = () => {
    return this.formValidation(this.state.username) === "success" &&
      this.formValidation(this.state.password) === "success" &&
      this.formValidation(this.state.email) === "success" &&
      this.formValidation(this.state.phone) === "success";
  };

  getControlForm = (label, name) => {
    return (
      <Form inline>
        <ControlLabel>{label}:</ControlLabel>
        <FormControl
          name={name}
          type="text"
          value={this.state[name]}
          onChange={(e) => this.onChangeHandler(e)}
        />
      </Form>);
  };

  personalDataValidation = () => {
    if
    (
      this.state.firstName === "" &&
      this.state.lastName === "" &&
      this.state.personalEmail === "" &&
      this.state.personalPhone === "" &&
      this.state.companyName === "" &&
      this.state.note === ""
    ) return "empty";
    else if
    (
      this.state.firstName !== "" &&
      this.state.lastName !== "" &&
      this.state.personalEmail !== "" &&
      this.state.personalPhone !== "" &&
      this.state.companyName !== "" &&
      this.state.note !== ""
    )
      return "ok";
    else return "error";
  }

  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    if (!this.generalValidation()) {
      this.setState({ isLoading: false });
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    }
    else {
      const login = this.state;
      try {
        switch (this.personalDataValidation()) {
        case "ok": {
          await this.props.addLoginAction({
            username: login.username,
            username_canonical: login.username,
            email: login.email,
            email_canonical: login.email,
            password: login.password,
            enabled: login.enabled === true ? 1 : 0,
            phone: login.phone,
            user_profile: {
              first_name: login.firstName,
              last_name: login.lastName,
              email: login.personalEmail,
              phone: login.personalPhone,
              company_name: login.companyName,
              note: login.note,
            }
          }, this.props.currentPage, this.props.pageSize, this.props.loginsSearch);
          swal({
            title: "Login is created",
            icon: "success",
            button:false,
            timer: 2000
          });
          this.setState({ isLoading: false });
          if (this.props.flag)
            this.props.updateState(this.props.loginGuid);
          else
            await this.props.showModal(false);
          break;
        }
        case "empty": {
          await this.props.addLoginAction({
            username: login.username,
            username_canonical: login.username,
            email: login.email,
            email_canonical: login.email,
            password: login.password,
            enabled: login.enabled === true ? 1 : 0,
            phone: login.phone,
          }, this.props.currentPage, this.props.pageSize, this.props.loginsSearch);
          swal({
            title: "Login is created",
            icon: "success",
            button:false,
            timer: 2000
          });
          this.setState({ isLoading: false });
          if (this.props.flag)
            this.props.updateState(this.props.loginGuid);
          else
            await this.props.showModal(false);
          break;
        }
        case "error": {
          this.setState({ isLoading: false });
          swal({
            title: "Please, enter information in every required field of personal Info",
            icon: "warning"
          });
          break;
        }
        default: break;
        }
      }
      catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
    }
  };

  render() {
    return (
      <Form autoComplete="off">
        <Row>
          <Col>
            <Row>
              <Col md={12}>
                <h3 align='center'>General info</h3>
              </Col>
            </Row>
            <div className="card">
              <Col >
                <Row className="rowReg">
                  <Col md={3}>
                    <ControlLabel >Username*</ControlLabel>
                  </Col>
                  <Col md={8}>
                    <FormGroup validationState={this.formValidation(this.state.username)}>
                      <FormControl
                        name="username"
                        type="text"
                        value={this.state.username}
                        onChange={(e) => this.onChangeHandler(e)}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="rowReg">
                  <Col md={3}>
                    <ControlLabel >Password*</ControlLabel>
                  </Col>
                  <Col md={8}>
                    <FormGroup validationState={this.formValidation(this.state.password)}>
                      <FormControl
                        name="password"
                        type={this.state.showPassword ? "text" : "password"}
                        value={this.state.password}
                        onChange={(e) => this.onChangeHandler(e)}
                      />
                      <Button style={{ marginTop: "10px", marginRight: "10px" }} type="button" onClick={this.handleGenerate}>Generate</Button>
                    </FormGroup>
                  </Col>
                  <Button className="eye" onClick={this.handleShowPassword}>
                    {this.state.showPassword
                      ? <span className="fa fa-eye-slash" />
                      : <span className="fa fa-eye" />
                    }
                  </Button>
                </Row>
                <Row className="rowReg" >
                  <Col md={3}>
                    <ControlLabel>Email*</ControlLabel>
                  </Col>
                  <Col md={8}>
                    <FormGroup validationState={this.formValidation(this.state.email)}>
                      <FormControl
                        name="email"
                        type="email"
                        value={this.state.email}
                        onChange={(e) => this.onChangeHandler(e)}
                      />
                    </FormGroup>
                  </Col>

                </Row>
                <Row className="rowReg" >
                  <Col md={3}>
                    <ControlLabel>Phone*</ControlLabel>
                  </Col>
                  <Col md={8}>
                    <FormGroup validationState={this.formValidation(this.state.phone)}>
                      <FormControl
                        name="phone"
                        type="number"
                        className="no-spinners"
                        value={this.state.phone}
                        onChange={(e) => this.onChangeHandler(e)}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="rowReg" >
                  <Col md={3}>
                    <label>Enable*</label>
                  </Col>
                  <Col md={1}>
                    <input type="checkbox" id='enabledCheckbox' checked={this.state.enabled} onChange={this.handleEnabledCheckbox} />{" "}
                  </Col>
                </Row>
              </Col>
            </div>
            <Col md={1} />
            <Row>
              <Col md={12}>
                <h3 align='center'>Personal data</h3>
              </Col>
            </Row>
            <div className="card">
              <Col>
                <Row className="rowReg" >
                  <Col md={3}>
                    <ControlLabel>Name</ControlLabel>
                  </Col>
                  <Col md={8}>
                    <FormGroup validationState={this.formValidation(this.state.firstName)}>
                      <FormControl
                        name="firstName"
                        type="text"
                        value={this.state.firstName}
                        onChange={(e) => this.onChangeHandler(e)}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="rowReg" >
                  <Col md={3}>
                    <ControlLabel>Surname</ControlLabel>
                  </Col>
                  <Col md={8}>
                    <FormGroup validationState={this.formValidation(this.state.lastName)}>
                      <FormControl
                        name="lastName"
                        type="text"
                        value={this.state.lastName}
                        onChange={(e) => this.onChangeHandler(e)}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="rowReg">
                  <Col md={3}>
                    <ControlLabel>Email</ControlLabel>
                  </Col>
                  <Col md={8}>
                    <FormGroup validationState={this.formValidation(this.state.personalEmail)}>
                      <FormControl
                        name="personalEmail"
                        type="email"
                        value={this.state.personalEmail}
                        onChange={(e) => this.onChangeHandler(e)}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="rowReg">
                  <Col md={3}>
                    <ControlLabel>Phone</ControlLabel>
                  </Col>
                  <Col md={8}>
                    <FormGroup validationState={this.formValidation(this.state.personalPhone)}>
                      <FormControl
                        name="personalPhone"
                        type="number"
                        className="no-spinners"
                        value={this.state.personalPhone}
                        onChange={(e) => this.onChangeHandler(e)}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="rowReg" >
                  <Col md={3}>
                    <ControlLabel>Company</ControlLabel>
                  </Col>
                  <Col md={8}>
                    <FormGroup validationState={this.formValidation(this.state.companyName)}>
                      <FormControl
                        name="companyName"
                        type="text"
                        value={this.state.companyName}
                        onChange={(e) => this.onChangeHandler(e)}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="rowReg" >
                  <Col md={3}>
                    <ControlLabel>Note</ControlLabel>
                  </Col>
                  <Col md={8}>
                    <FormControl
                      name="note"
                      type="textarea"
                      value={this.state.note}
                      onChange={(e) => this.onChangeHandler(e)}
                    />
                  </Col>
                </Row>

              </Col>
            </div>
            <div align="center">
              {this.state.isLoading 
                ? <ReactLoading type='cylon' color='grey' /> 
                : <Button type="submit" onClick={this.handleSubmit}>Create Login</Button>}
            </div>
          </Col>
        </Row>
      </Form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loginGuid: state.logins.loginGuid,
    pageSize: state.logins.pageSize,
    currentPage: state.logins.currentPage,

    loginsSearch: state.search.loginsSearch
  };
};

export default connect(mapStateToProps, {
  addLoginAction,
  showModal,
})(LoginCreator);

LoginCreator.propTypes = {
  loginGuid: PropTypes.string,
  currentPage: PropTypes.number,
  pageSize: PropTypes.number,
  flag: PropTypes.bool,
  updateState: PropTypes.bool,

  addPrivilegeAction: PropTypes.func,
  showModal: PropTypes.func,
  addLoginAction: PropTypes.func,
  loginsSearch: PropTypes.object,
};