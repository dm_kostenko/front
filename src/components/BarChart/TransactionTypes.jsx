import React from "react";
import { connect } from "react-redux";
import { BarChart } from "../UI/BarChart";
import { getTransactionTypesAction } from "../../actions/transactions";
import PropTypes from "prop-types";
import MultiSelect from "../UI/MultiSelect";
import Spinner from "components/UI/Spinner";


class BarChartTransactionTypes extends React.Component {
  state = {
    daysCount: 7,
    selectedType: ""
  }

  componentDidMount = async () => {
    await this.props.getTransactionTypesAction({ days:this.state.daysCount });
    if(this.props.types)
      this.setState({
        selectedType: Object.keys(this.props.types)[0]
      });
  }

  handleSelect = (option) => {
    this.setState({
      selectedType: option.name
    });
  }

  render() {
    if (this.props.loading) return <Spinner />;
    else {
      let types = Object.keys(this.props.types).map((type, index) => {
        return {
          guid: index,
          name: type
        };
      });

      let labelsPie = [ this.state.selectedType ];
      const data = this.state.selectedType ? this.props.types[this.state.selectedType] : [];

      let success = 0;
      let failed = 0;
      data.map(item => {
        success += item.success;
        failed += item.failed;
      });     

      const datasetsPie = [
        {
          label: "Success",
          backgroundColor: "rgba(75,192,192,0.4)",
          borderColor: "rgba(75,192,192,1)",
          borderWidth: 1,
          hoverBackgroundColor: "rgba(75,192,192,1)",
          hoverBorderColor: "rgba(75,192,192,1)",
          data: [success]
        },
        {
          label: "Failed",
          backgroundColor: "rgba(255,99,132,0.4)",
          borderColor: "rgba(255,99,132,1)",
          borderWidth: 1,
          hoverBackgroundColor: "rgba(255,99,132,1)",
          hoverBorderColor: "rgba(255,99,132,1)",
          data: [failed]
        }
      ];

      return (
        <div>
          <div className="header">Transactions types</div>
          <BarChart
            labels={labelsPie}
            datasets={datasetsPie}
            height={200}
            name={"Transactions types"}
            data={this.props.types}
            options={{
              scales: {
                yAxes: [ {
                  ticks: {
                    beginAtZero: true
                  }
                } ]
              }
            }}
          />
          <MultiSelect
            options={types || []}
            multi={false}
            onSelect={this.handleSelect}
            value={this.state.selectedType}
          />
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    types: state.transactions.types,
    loading: state.transactions.transactionTypesLoading
  };
};

export default connect(mapStateToProps, {
  getTransactionTypesAction
})(BarChartTransactionTypes);

BarChartTransactionTypes.propTypes = {
  types: PropTypes.object,
  getTransactionTypesAction: PropTypes.func,
  loading: PropTypes.bool,
};