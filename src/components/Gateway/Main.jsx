import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../factories/Main";
import GatewayCreator from "./Creator";
import ability from "config/ability";
import { getAllGateways, deleteGatewayAction } from "../../actions/gateways";
import { searchInGateways, reset } from "../../actions/search";
import GatewayEditor from "./Editor";
import Modal from "components/UI/Modal";
import { Link } from "react-router-dom";

const columns = [
  {
    path: "guid",
    label: "ID",
    key: "guid",
    content: gateway => (
      <Link className="link" to={`/about/gateway/${gateway.guid}`} >{gateway.guid}</Link>
    ),
  },
  { path: "name", label: "Name" },
  { path: "description", label: "Description" },
  ability.can("EXECUTE", "GATEWAYS") &&
  {
    key: "edit",
    content: gateway => (
      <Modal 
        header="Edit gateway" 
        action="Save" 
        content={<GatewayEditor guid={gateway.guid} type="gateway" />}
        button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }}/>} 
      />
    ),
    label: "Edit"
  },
  ability.can("EXECUTE", "GATEWAYS") &&
  {
    key: "delete",
    content: () => (
      <i
        className="far fa-trash-alt"
        style={{ cursor: "pointer", color: "red" }}
      />
    ),
    label: "Delete"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.gateways.gatewaysList,
    count: state.gateways.count,
    loading: state.gateways.gatewaysLoading,
    searchData: state.search.gatewaysSearch,
    isSearch: state.search.isSearch,
    name:"gateways",
    columns,
    modal: <GatewayCreator/>
  };
};

export default connect(mapStateToProps, {
  get: getAllGateways,
  delete: deleteGatewayAction,
  search: searchInGateways,
  reset
})(AbstractComponent);
