import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Row, FormGroup, ControlLabel, FormControl, Button, Form } from "react-bootstrap";
import { getGatewayAction, editGatewayAction } from "../../actions/gateways";
import { showModal } from "../../actions/modal";
import PropTypes from "prop-types";
import { parseResponse } from "helpers/parseResponse.js";
import Spinner from "components/UI/Spinner/index";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class GatewayEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      label: "",
      data: {},
      select: [],
      selectedValue: [],
      isLoading: false
    };
  }

  async componentDidMount() {
    await this.props.getGatewayAction(this.props.guid);
    const { gateway } = this.props;
    this.setState({
      data: gateway
    });
  }

  formValidation = (data) => {
    if (data === "") return "error";
    else return "success";
  };

  handleChange = ({ currentTarget: input }) => {
    const data = { ...this.state.data };
    data[input.name] = input.value;

    this.setState({ data });
  };

  generalValidation = () => {
    const { data } = this.state;
    return this.formValidation(data.name) === "success" &&
      this.formValidation(data.type) === "success" &&
      this.formValidation(data.selectedValue) === "success";
  };

  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    try {
      const data = {
        guid: this.props.guid,
        name: this.state.data.name,
        description: this.state.data.description
      };
      try {
        await this.props.editGatewayAction(data);
        swal({
          title: "Gateway is updated",
          icon: "success",
          button:false,
          timer: 2000
        });
      } catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
      await this.props.showModal(false);
    }
    catch (err) {
      this.setState({ isLoading: false });
      throw (err);
    }
  };

  render() {
    if (this.props.gatewayLoading) {
      return <Spinner />;
    }
    else return (
      <div>
        <div className="card">
          <div className="content"><label>ID: {this.props.guid}</label></div>
        </div>
        <div className="card">
          <Form onSubmit={this.handleSubmit} autoComplete="off">
            <Row className="rowEdit" >
              <Col className='col'>
                <ControlLabel>Name:</ControlLabel>
                <FormGroup validationState={this.formValidation(this.state.data.username)}>
                  <FormControl
                    name="name"
                    type="text"
                    value={this.state.data.name}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row className="rowEdit">
              <Col className='col'>
                <ControlLabel>Description:</ControlLabel>
                <FormGroup validationState={this.formValidation(this.state.data.username)}>
                  <FormControl
                    name="description"
                    type="text"
                    value={this.state.data.description}
                    onChange={(e) => this.handleChange(e)}
                  />
                </FormGroup>
              </Col>
            </Row>
            <div align="center">
              {this.state.isLoading 
                ? <ReactLoading type='cylon' color='grey' />
                : <Button type="submit">Save</Button>}
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    gateway: state.gateways.gateway,
    gatewayLoading: state.gateways.gatewayLoading
  };
};

export default connect(mapStateToProps, {
  getGatewayAction,
  editGatewayAction,
  showModal,
})(GatewayEditor);

GatewayEditor.propTypes = {
  getGatewayAction: PropTypes.func,
  editGatewayAction: PropTypes.func,
  showModal: PropTypes.func,

  gateway: PropTypes.object,
  gatewayLoading: PropTypes.bool,
  guid: PropTypes.string,
};