import React from "react";
import { Form } from "react-bootstrap";
import Joi from "joi-browser";
import CustomForm from "../UI/Form";
import { connect } from "react-redux";
import { getAllGateways, addGatewayAction } from "../../actions/gateways";
import { showModal } from "../../actions/modal";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class GatewayCreator extends CustomForm {
  state = {
    data: {
      name: "",
      description: ""
    },
    errors: {},
    isLoading: false
  };

  schema = {
    name: Joi.string()
      .required()
      .min(0)
      .label("Name"),
    description: Joi.string()
      .required()
      .min(0)
      .label("Description")
  };

  doSubmit = async () => {
    this.setState({ isLoading: true });
    const { name, description } = this.state.data;
    try {
      await this.props.addGatewayAction({
        name,
        description
      }, this.props.currentPage, this.props.pageSize);
      swal({
        title: "Gateway is created",
        icon: "success",
        button: false,
        timer: 2000
      });
      //await this.props.getAllGateways(this.props.currentPage, this.props.pageSize);
      await this.props.showModal(false);
    } catch (error) {
      this.setState({ isLoading: false });
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  }

  render() {
    return (
      <div className="card">
        <div className="content">
          <Form autoComplete="off" onSubmit={this.handleSubmit}>
            {this.renderInput("name", "Name")}
            {this.renderInput("description", "Description")}
            <div align="center">
              {this.state.isLoading
                ? <ReactLoading type='cylon' color='grey' />
                : this.renderButton("Add gateway")}
            </div>
          </Form>
        </div>
      </div >
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pageSize: state.gateways.pageSize,
    currentPage: state.gateways.currentPage
  };
};

export default connect(mapStateToProps,
  {
    getAllGateways,
    addGatewayAction,
    showModal
  })(GatewayCreator);

GatewayCreator.propTypes = {
  addGatewayAction: PropTypes.func,
  showModal: PropTypes.func
};