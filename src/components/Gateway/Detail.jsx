import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import Table from "../UI/Table/index";
import Pagination from "../UI/Pagination/index";
import { connect } from "react-redux";
import Modal from "components/UI/Modal";
import PropertyCreator from "./Property/Creator";
import { getShopsByGatewayAction } from "actions/shops";
import { getGatewayAction, getGatewayPropsAction, deleteGatewayPropsAction } from "actions/gateways";
import PropTypes from "prop-types";
import moment from "moment";
import ability from "config/ability";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse";
import Spinner from "components/UI/Spinner/index";

export class GatewayDetail extends Component {
  state = {
    gatewayInfo: {},
    shops: [],
    count: "",
    currentPage: 1,
    pageSize: 10
  };

  columnsShops = [
    { path: "name", label: "Name" },
    { path: "email", label: "Email" },
    { path: "url", label: "URL" }
  ];

  columnsProperties = [
    { path: "number", label: "№" },
    { path: "label", label: "Name" },
    ability.can("EXECUTE", "GATEWAYS") &&
    {
      key: "delete",
      content: (property) => (
        <i
          className="far fa-trash-alt"
          style={{ cursor: "pointer", color: "red" }}
          onClick={() =>
            swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
              .then((willDelete) => {
                if (willDelete) {
                  this.handleDelete(property.name);
                  swal("Deleted", {
                    icon: "success",
                    button:false,
                    timer: 2000
                  });
                }
              })
          }
        />
      ),
      label: "Delete"
    }
  ]

  async componentDidMount() {
    await this.props.getGatewayAction(this.props.match.params.id);
    await this.props.getShopsByGatewayAction(this.props.match.params.id, 1, this.state.pageSize);
    await this.props.getGatewayPropsAction(this.props.match.params.id);
    this.setState({
      shops: this.props.shops,
      count: this.props.count
    });
  }

  handlePageChange = async (page) => {
    await this.props.getShopsByGatewayAction(this.props.match.params.id, page, this.state.pageSize);
    this.setState({
      shops: this.props.shops,
      count: this.props.count,
      currentPage: page
    });
  };

  getPagedData = () => {
    const { count } = this.state;
    let { pageSize } = this.state;
    const pagesCount = count ? (count / pageSize) + (1 && !!(count % pageSize)) : 0;
    return { pagesCount };
  };

  handleDelete = async (propName) => {
    try {
      const data = {
        name: propName,
        delete: "true"
      };
      await this.props.deleteGatewayPropsAction(this.props.match.params.id, data);
      swal({
        title: "Property successfully deleted",
        icon: "success",
        button:false,
        timer: 2000
      });
    }
    catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  };


  render() {
    if (this.props.gatewayPropsLoading || this.props.gatewayLoading || this.props.gatewayShopsLoading) return <Spinner />;
    else {
      const gateway = this.props.gatewayInfo;
      const { currentPage } = this.state;
      const { pagesCount } = this.getPagedData();
      const { shops } = this.state;
      return (
        <>
        <Button onClick={this.props.history.goBack} className="detail-back-button"><i className="fas fa-arrow-left"></i></Button>
        <div className="content" style={{ overflow: "auto" }}>
          <Row style={{ marginLeft: "0px" }}>
            <div className="card">
              <div className="title">{gateway.name}</div>
            </div>
            <div className="card-container">
              <div className="card">
                <div className="header">Detailed info</div>
                <div className="content">
                  <Row>
                    <Col className="right" md={5} lg={5} xs={5}>
                      <label>GUID: </label>
                    </Col>
                    <Col md={7} lg={7} xs={7}>
                      <span>{gateway.guid}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="right" md={5} lg={5} xs={5}>
                      <label>Description: </label>
                    </Col>
                    <Col md={7} lg={7} xs={7}>
                      <span>{gateway.description}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="right" md={5} lg={5} xs={5}>
                      <label>Created at: </label>
                    </Col>
                    <Col md={7} lg={7} xs={7}>
                      <span>{gateway.created_at !== null ? moment(gateway.created_at).utcOffset(0).format("D MMM YYYY") : ""}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="right" md={5} lg={5} xs={5}>
                      <label>Created by: </label>
                    </Col>
                    <Col md={7} lg={7} xs={7}>
                      <span>{gateway.created_by}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="right" md={5} lg={5} xs={5}>
                      <label>Updated at: </label>
                    </Col>
                    <Col md={7} lg={7} xs={7}>
                      <span>{gateway.updated_at !== null ? moment(gateway.updated_at).utcOffset(0).format("D MMM YYYY") : ""}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="right" md={5} lg={5} xs={5}>
                      <label>Updated by: </label>
                    </Col>
                    <Col md={7} lg={7} xs={7}>
                      <span>{gateway.updated_by}</span>
                    </Col>
                  </Row>
                </div>
              </div>
              <div className="card">
                <div className="header">Connected shops</div>
                <div className="content">
                  <Row>
                    <Col md={12}>
                      <div className="table">
                        <Table
                          columns={this.columnsShops}
                          data={shops}
                          disableSearch={true}
                        />
                        <Pagination
                          pagesCount={pagesCount}
                          currentPage={currentPage}
                          onPageChange={this.handlePageChange}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
            </div>
          </Row>
          <Row style={{ marginLeft: "0px" }}>
            <div className="card">
              <div className="header">Gateway properties</div>
              <div className="content">
                <Row>
                  <Col md={12}>
                    <div className="table">
                      <Table
                        columns={this.columnsProperties}
                        data={this.props.gatewayProps}
                        disableSearch={true}
                      />
                      <Pagination
                        pagesCount={pagesCount}
                        currentPage={currentPage}
                        onPageChange={this.handlePageChange}
                      />
                    </div>
                  </Col>
                </Row>
                {ability.can("EXECUTE", "GATEWAYDETAIL") &&
                <div align="center">
                  <Modal
                    header="Add gateway property"
                    action="Save"
                    content={<PropertyCreator guid={gateway.guid} type="gateway" />}
                    button={<Button>Add property</Button>}
                  />
                </div>}
              </div>
            </div>
          </Row>
        </div >
        </>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    gatewayInfo: state.gateways.gateway,
    shops: state.shops.gatewayShops,
    count: state.shops.shopsCount,
    gatewayProps: state.gateways.gatewayProps,
    gatewayPropsLoading: state.gateways.propsLoading,
    gatewayLoading: state.gateways.gatewayLoading,
    gatewayShopsLoading: state.shops.gatewayShopsLoading
  };
};

export default connect(mapStateToProps, {
  getShopsByGatewayAction,
  getGatewayPropsAction,
  getGatewayAction,
  deleteGatewayPropsAction
})(GatewayDetail);

GatewayDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  gatewayProps: PropTypes.array,
  gatewayInfo: PropTypes.object,
  shops: PropTypes.array,
  count: PropTypes.number,
  gatewayPropsLoading: PropTypes.bool,
  gatewayShopsLoading: PropTypes.bool,
  gatewayLoading:PropTypes.bool,
  getShopsByGatewayAction: PropTypes.func,
  getGatewayAction: PropTypes.func,
  getGatewayPropsAction: PropTypes.func,
  deleteGatewayPropsAction: PropTypes.func
};