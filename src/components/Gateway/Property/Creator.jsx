import React from "react";
import { Col, Form, Button } from "react-bootstrap";
import Joi from "joi-browser";
import CustomForm from "components/UI/Form";
import { connect } from "react-redux";
import { addGatewayPropsAction, getGatewayPropsAction } from "actions/gateways";
import { showModal } from "actions/modal";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";

class PropertyCreator extends CustomForm {
  state = {
    data: {
      name: "",
      label: ""
    },
    errors: {}
  };

  schema = {
    name: Joi.string()
      .required()
      .min(0)
      .label("Name"),
    label: Joi.string()
      .required()
      .min(0)
      .label("Label")
  };

  doSubmit = async () => {
    const { data } = this.state;
    try {
      await this.props.addGatewayPropsAction(this.props.guid, data);
      swal({
        title: "Property is created",
        icon: "success",
        button:false,
        timer: 2000
      });
      await this.props.showModal(false);
    } catch (error) {
      const parsedError = parseResponse(error);
      swal({
        title: parsedError.error,
        text: parsedError.message,
        icon: "error",
      });
    }
  }

  render() {
    return (
      <div className="card">
        <div className="content">
          <Form autoComplete="off">
            <Col sm={1} />
            <Col sm={10}>
              {this.renderInput("name", "Name")}
              {this.renderInput("label", "Label")}
            </Col>
            <Col sm={1} />
            <div align="center">
              <Button type="submit" onClick={this.handleSubmit}> Add property</Button>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pageSize: state.gateways.pageSize,
    currentPage: state.gateways.currentPage
  };
};

export default connect(mapStateToProps,
  {
    addGatewayPropsAction,
    getGatewayPropsAction,
    showModal
  })(PropertyCreator);

PropertyCreator.propTypes = {
  addGatewayPropsAction: PropTypes.func,
  getGatewayPropsAction: PropTypes.func,
  showModal: PropTypes.func
};