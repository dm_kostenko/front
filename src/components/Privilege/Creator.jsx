import React, { Component } from "react";
import { Form, Row, Col, FormControl, ControlLabel, FormGroup } from "react-bootstrap";
import Button from "react-bootstrap/es/Button";
import { connect } from "react-redux";
import { addPrivilegeAction } from "actions/privileges";
import { showModal } from "actions/modal";
import MultiSelect from "components/UI/MultiSelect";
import { parseResponse } from "helpers/parseResponse";
import PropTypes from "prop-types";
import swal from "sweetalert";
import ReactLoading from "react-loading";

class PrivilegeCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      actionName: "",
      viewName: "",
      name: "",
      type: "merchant",
      description: "",
      isLoading: false
    };
  }

  validate(val) {
    return (val !== "");
  }

  privilegeValidate(val) {
    return this.validate(val.split("_")[0]) && this.validate(val.split("_")[1]) && val.split("_").length === 2;
  }

  generalValidation = () => {
    if (this.formValidation(this.state.name) === "success" &&
      this.formValidation(this.state.description) === "success" &&
      this.formValidation(this.state.type) === "success"
    ) return true;
    else return false;
  }

  formValidation = (data) => {
    if (data === "" || typeof (data) === "undefined") return "error";
    else return "success";
  }

  onActionSelect = (option) => {
    this.setState({
      actionName: option.name,
      name: option.name + "_" + this.state.viewName
    });
  };


  onNameChange = (e) => {
    this.setState({
      viewName: e.target.value.toUpperCase(),
      name: this.state.actionName + "_" + e.target.value.toUpperCase()
    });
  };

  onDescriptionChange = (e) => {
    this.setState({
      description: e.target.value
    });
  };

  onSelectType = (e) => {  
    this.setState({
      type: e.target.value
    });
  };


  handleSubmit = async (e) => {
    this.setState({ isLoading: true });
    e.preventDefault();
    const { name, type, description } = this.state;
    if (this.generalValidation() && this.privilegeValidate(name))  {
      try {
        await this.props.addPrivilegeAction({
          name,
          type,
          description
        }, this.props.currentPage, this.props.pageSize, this.props.privilegesSearch);
        swal({
          title: "Privelege is created",
          icon: "success",
          button:false,
          timer: 2000
        });
        await this.props.showModal(false);
      } catch (error) {
        this.setState({ isLoading: false });
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }

    } else {
      this.setState({ isLoading: false });
      swal({
        title: "Please, fill in all the fields",
        icon: "warning"
      });
    }    
  }

  render() {

    const actions = [
      {
        guid: 0,
        name: "READ"
      },
      {
        guid: 1,
        name: "EXECUTE"
      }
    ];

    const types = [
      {
        guid: "0",
        name: "merchant"
      },
      {
        guid: "1",
        name: "group"
      },
      {
        guid: "2",
        name: "partner"
      },      
      {
        guid: "3",
        name: "system"
      }      
    ];

    return (
      <div className="text-center">
        <div className="row justify-content-center">
          <Form autoComplete="off">
            <Col sm="1" />
            <Col sm="10">
              <Row>
                <Col className="col-md-3">
                  <ControlLabel>Name*</ControlLabel>
                </Col>
                <Col className="col-md-4">
                  <FormGroup>
                    <MultiSelect
                      name="Name"
                      options={actions}
                      multi={false}
                      onSelect={this.onActionSelect}
                    />
                  </FormGroup>
                </Col>
                <Col className="col-md-5">
                  <FormGroup validationState={this.formValidation(this.state.name)}>
                    <FormControl
                      name="name"
                      value={this.state.viewName}
                      onChange={this.onNameChange}
                      style={{ height: "38px" }}
                    />
                  </FormGroup>
                </Col>
              </Row>              
              <br />
              <Row>
                <Col className="col-md-3">
                  <ControlLabel>Type*</ControlLabel>
                </Col>
                <Col className="col-md-9">
                  <FormGroup validationState={this.formValidation(this.state.type)}>
                    <FormControl
                      name="type"
                      componentClass="select"
                      onChange={this.onSelectType}
                    >
                      {types.map(type => {
                        return (<option key={type.guid} value={type.name}>{type.name}</option>);
                      })}
                    </FormControl>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col className="col-md-3">
                  <ControlLabel>Description*</ControlLabel>
                </Col>
                <FormGroup validationState={this.formValidation(this.state.description)}>
                  <Col className="col-md-9">
                    <FormControl
                      name="description"
                      value={this.state.description}
                      onChange={this.onDescriptionChange}
                    />
                  </Col>
                </FormGroup>
              </Row>
              <br />
              <div align="center">
                {this.state.isLoading 
                  ? <ReactLoading type='cylon' color='grey' />
                  : <Button type="submit" onClick={this.handleSubmit}>Add</Button>}
              </div>
            </Col>
            <Col sm="1" />
          </Form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pageSize: state.privileges.pageSize,
    currentPage: state.privileges.currentPage,

    privilegesSearch: state.search.privilegesSearch
  };
};

export default connect(mapStateToProps,
  {
    addPrivilegeAction,
    showModal
  })(PrivilegeCreator);

PrivilegeCreator.propTypes = {
  addPrivilegeAction: PropTypes.func,
  showModal: PropTypes.func,
  currentPage: PropTypes.number,
  pageSize: PropTypes.number,
  privilegesSearch: PropTypes.array,
};