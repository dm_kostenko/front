import React from "react";
import { connect } from "react-redux";
import AbstractComponent from "../../factories/Main";
import PrivilegeCreator from "./Creator";
import ability from "config/ability";
import { getAllPrivileges, deletePrivilegeAction } from "../../actions/privileges";
import { searchInPrivileges, reset } from "../../actions/search";
import Modal from "components/UI/Modal";
import PrivilegeEditor from "./Editor";

const columns = [
  { path: "guid", label: "ID" },
  { path: "name", label: "Name" },
  { path: "type", label: "Type" },
  { path: "description", label: "Description" },
  ability.can("EXECUTE", "PRIVILEGES") &&
  {
    key: "edit",
    content: privilege => (
      <Modal 
        header="Edit privilege" 
        button={<i className="far fa-edit" style={{ cursor: "pointer", color: "green" }}/>} 
        content={<PrivilegeEditor privilege_guid={privilege.guid} />} />
    ),
    label: "Edit"
  },
  ability.can("EXECUTE", "PRIVILEGES") &&
  {
    key: "delete",
    content: () => (
      <i
        className="far fa-trash-alt"
        style={{ cursor: "pointer", color: "red" }}
      />
    ),
    label: "Delete"
  }
];

const mapStateToProps = (state) => {
  return {
    data: state.privileges.privilegesList,
    count: state.privileges.count,
    loading: state.privileges.privilegesLoading,
    searchData: state.search.privilegesSearch,
    isSearch: state.search.isSearch,
    name:"privileges",
    columns,
    modal: <PrivilegeCreator/>
  };
};

export default connect(mapStateToProps, {
  get: getAllPrivileges,
  delete: deletePrivilegeAction,
  search: searchInPrivileges,
  reset
})(AbstractComponent);
