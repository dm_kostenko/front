import React, { Component } from "react";
import { connect } from "react-redux";
import { getTotalProcessedAction } from "../../actions/transactions";
import PropTypes from "prop-types";
import Spinner from "components/UI/Spinner";

class TotalProcessed extends Component {
  state = {
    daysCount: 7
  }

  componentDidMount = () => {
    this.props.getTotalProcessedAction({ days:this.state.daysCount });
  }

  render() {
    const { loading, totalProcessed, classname } = this.props;

    if (loading) return <Spinner/>;
    else if (totalProcessed !== undefined)
      return (
        <div>
          <div className="header">Amount of money processed</div>
          <div className={classname}>$ {totalProcessed ? totalProcessed : 0}</div>
        </div>
      );
    return null;
  }
}

const mapStateToProps = (state) => {
  return {
    totalProcessed: state.transactions.totalProcessed,
    loading: state.transactions.totalProcessedLoading
  };
};

export default connect(mapStateToProps, {
  getTotalProcessedAction
})(TotalProcessed);

TotalProcessed.propTypes = {
  totalProcessed: PropTypes.number,
  getTotalProcessedAction: PropTypes.func,
  loading: PropTypes.bool,
  classname: PropTypes.string
};