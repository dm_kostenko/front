import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { connect } from "react-redux";
import { addLoginRoleAction, getLoginRolesAction } from "actions/logins";
import MultiSelect from "components/UI/MultiSelect";
import PropTypes from "prop-types";
import swal from "sweetalert";
import { parseResponse } from "helpers/parseResponse";

export class RowAddRole extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roles: [],
      selectedRole: ""
    };
  }

  handleRoleSelect = (option) => {
    this.setState({ selectedRole: option.guid });
  };

  componentDidMount = async () => {
    let { roles } = this.props;
    const { loginRoles } = this.props;
    loginRoles && loginRoles.forEach(loginRole => {
      roles = roles.filter(role => role.guid !== loginRole.guid);
    });
    this.setState({
      roles
    });
  };

  handleSubmit = async () => {
    if (this.state.selectedRole === "")
      swal({
        title: "Please, enter information in every required field",
        icon: "warning"
      });
    else {
      try {
        const data = {
          guid: this.state.selectedRole
        };
        await this.props.addLoginRoleAction(this.props.loginGuid, data);
        swal({
          title: "Role is added",
          icon: "success",
          button:false,
          timer: 2000
        });
        let { roles, loginRoles } = this.props;
        loginRoles.forEach(loginRole => {
          roles = roles.filter(role => role.guid !== loginRole.guid);
        });
        await this.setState({
          selectedRole: "",
          roles
        });

      }
      catch (error) {
        const parsedError = parseResponse(error);
        swal({
          title: parsedError.error,
          text: parsedError.message,
          icon: "error",
        });
      }
    }
  };

  render() {
    return (
      <div style={{ textAlign: "center" }}>
        <label>Role*</label>
        <MultiSelect options={this.state.roles} multi={false} onSelect={this.handleRoleSelect} />
        <Button onClick={this.handleSubmit} style={{ margin: "15px" }}>Add role</Button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    roles: state.roles.rolesList,
    loginRoles: state.logins.loginRoles
  };
};

export default connect(mapStateToProps, {
  addLoginRoleAction,
  getLoginRolesAction
})(RowAddRole);

RowAddRole.propTypes = {
  loginGuid: PropTypes.string,
  roles: PropTypes.array,
  loginRoles: PropTypes.array,

  addLoginRoleAction: PropTypes.func,
  getLoginRolesAction: PropTypes.func,
};