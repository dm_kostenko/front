import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import Header from "ibankComponents/UI/Header/";
import { Footer } from "ibankComponents/UI/Footer/index";
import Sidebar from "ibankComponents/UI/Sidebar";

import {
  navRoutes,
  nonNavRoutes
} from "routes/dashboard";

class Routes extends Component {

  componentDidUpdate(e) {
    if (
      window.innerWidth < 993 &&
      e.history.location.pathname !== e.location.pathname &&
      document.documentElement.className.indexOf("nav-open") !== -1
    ) {
      // document.documentElement.classList.toggle("nav-open");
    }
    // if (e.history.action === "PUSH") {
    //   document.documentElement.scrollTop = 0;
    //   document.scrollingElement.scrollTop = 0;
    //   // this.mainPanel.scrollTop = 0;
    //   this.mainPanel = React.createRef();
    // }
  }
  constructRoute(prop) {
    if (prop.notifications)
      return (
        <Route
          path={prop.path}
          key={prop.id}
          render={routeProps => (
            <prop.component
              {...routeProps}
            />
          )}
        />
      );
    if (prop.redirect)
      return <Redirect from={prop.path} to={prop.to} key={prop.id} />;
    if(!prop.logout)
      return <Route path={prop.path} component={prop.component} key={prop.id} />;
  }

  render() {
    return (
      <div className="wrapper">
        <Sidebar {...this.props} />
        <div 
          id="main-panel" 
          className={this.props.isHide ? "main-panel slideOut" : "main-panel slideIn"}
          ref={this.mainPanel} 
        >
          <Header {...this.props} />
          <Switch>
            {nonNavRoutes.map((prop, key) =>
              this.constructRoute(prop, key)
            )}
            {navRoutes.map((prop, key) =>
              prop.routes ? prop.routes.map((item, key)=> this.constructRoute(item, key)) :this.constructRoute(prop, key)
            )}
          </Switch>
          <Footer />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isHide: state.sidebar.isHide
  };
};

export default connect(mapStateToProps, null)(Routes);

Routes.propTypes = {
  isHide: PropTypes.bool,
};