import { backendAuth, setTokens, flushTokens } from "../backendPlatform";
import axios from "axios";
import config from "config/";

const login = async data => {
  const response = await backendAuth.post("/login", data);
  if(response.data.accessToken && response.data.refreshToken)
    await setTokens(response.data.accessToken, response.data.refreshToken);
  return response;
};

const logout = async () => {
  await backendAuth.post("/logout");
  flushTokens();
};

const validate = () => backendAuth.get("/validate");

const change = async data => {
  const response = await backendAuth.post("/changeLogin", data);
  return response;
};

const getQRCode = () => {
  const tokens = JSON.parse(localStorage.getItem("changeAuth"));
  if(tokens && tokens.accessToken) {
    const backendAuthRelogin = axios.create({ 
      baseURL: `${config.node.host}/api/bank/auth`,
      headers: {
        common: {
          "Authorization": `Bearer ${tokens.accessToken}`
        }
      }
    });
    return backendAuthRelogin.get("/otp");
  }
  return backendAuth.get("/otp");
};

const sendOtp = (data) => {  
  const tokens = JSON.parse(localStorage.getItem("changeAuth"));
  if(tokens && tokens.accessToken) {
    const backendAuthRelogin = axios.create({ 
      baseURL: `${config.node.host}/api/bank/auth`,
      headers: {
        common: {
          "Authorization": `Bearer ${tokens.accessToken}`
        }
      }
    });
    return backendAuthRelogin.post("/otp", { ...data });
  }
  return backendAuth.post("/otp", { ...data });
};

const getEmailMessage = async (data) => {
  return await backendAuth.post("/forgot", data);
};

const checkRecoveryToken = async (data) => {
  return await backendAuth.post("/check/recovery", data);
};

const setNewPassword = async (data) => {
  return await backendAuth.post("/recovery", data);
};

export default { 
  login, 
  logout,
  validate, 
  change, 
  getQRCode, 
  sendOtp, 
  getEmailMessage, 
  checkRecoveryToken,
  setNewPassword 
};