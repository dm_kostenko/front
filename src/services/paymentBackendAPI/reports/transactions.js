import { backendReports } from "../backendPlatform";

/** 
 * * Get transactions currencies
 * * Inputs (partner_guid, group_guid, merchant_guid, shop_guid, from_date, to_date, days) 
 * * Outputs ([CURRENCY_NAME: [{date, success, failed},...],...])
 * */
export const getTransactionsCurrencies = (params) => {
  return backendReports.get("/currencies", { params });
};

/** 
 * * Get transactions currencies
 * * Inputs (partner_guid, group_guid, merchant_guid, shop_guid, from_date, to_date, days) 
 * * Outputs ([CURRENCY_NAME: [{date, success, failed},...],...])
 * */
export const getCurrenciesOfTransactions = (params) => {
  return backendReports.get("/currencies_of_transactions", { params });
};

/** 
 * * Get daily Transaction info
 * * Inputs (partner_guid, group_guid, merchant_guid, shop_guid)
 * * Outputs ([ {time, type, success, failed},...])
 * */
export const getDailyTransaction = (params) => {
  return backendReports.get("/daily_transactions", { params } );
};

/** 
 * * Get orders
 * * Inputs (status, from_date, to_date, days, page, items)
 * * Outputs ({ count:  data: [ { guid, type, status, date: timestamp }, ... ] }
 * */
export const getOrders = ( params ) => {
  return backendReports.get("/orders", { params } );
};

/** 
 * * Get shop totals
 * * Inputs ( partner_guid, group_guid, merchant_guid, shop_guid, from_date, to_date, days, page, items)
 * * Outputs ({ count: data: [ { shop_guid, shop_name, currency, transaction_type, status, number:  }, ... ] })
 * */
export const getShopTotals = ( params ) => {
  return backendReports.get("/shop_totals", { params });
};


/** 
 * * Get totals
 * * Inputs (partner_guid, group_guid, merchant_guid, shop_guid, from_date, to_date, days, page, items)
 * * Outputs ({ count:  data: [ { currency, type, status, number:  }, ... ] })
 * */
export const getTotals = ( params ) => {
  return backendReports.get("/totals", { params });
};

/** 
 * * Get totals processed
 * * Inputs (partner_guid, group_guid, merchant_guid, shop_guid, from_date, to_date, days)
 * * Outputs (amount)
 * */
export const getTotalProcessed = (params) => {
  return backendReports.get("/total_processed", { params } );
};

/** 
 * * Get totals to client
 * * Inputs (partner_guid, group_guid, merchant_guid, shop_guid, from_date, to_date, days)
 * * Outputs (amount)
 * */
export const getTotalToClient = ( params ) => {
  return backendReports.get("/total_to_client", { params } );
};

/** 
 * * Get totals to processor
 * * Inputs (partner_guid, group_guid, merchant_guid, shop_guid, from_date, to_date, days)
 * * Outputs (amount)
 * */
export const getTotalToProcessor = (params) => {
  return backendReports.get("/total_to_processor", { params } );
};


/** 
 * * Get transaction history
 * * Inputs (guid, status, type, partner_guid, partner_name, group_guid, group_name, merchant_guid, merchant_name, shop_guid, shop_name, account_guid, account_number, from_date, to_date, days, page, items)
 * * Outputs (count, data: [ { guid, type, status, partner_guid, partner_name, group_guid, group_name, merchant_guid, merchant_name, shop_guid, shop_name, account_guid, account_number, date, }, ... ])
 * */
export const getTransactionHistory = (params) => {
  return backendReports.get("/transaction_history",  { params });
};

/** 
 * * Get transaction types
 * * Inputs (partner_guid, group_guid, merchant_guid, shop_guid, from_date, to_date, days)
 * * Outputs ([ TRANSACTION_TYPE: [ { date, success, failed }, ... ], ... ])
 * */
export const getTransactionTypes = (params) => {
  return backendReports.get("/transaction_types", { params });
};

/** 
 * * Get balance
 * * Inputs (partner_guid, group_guid, merchant_guid, shop_guid)
 * * Outputs ([ { name, sum}, ... ])
 * */
export const getBalance = (params) => {
  return backendReports.get("/balance", { params });
};