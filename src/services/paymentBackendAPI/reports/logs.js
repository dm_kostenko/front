import { backendReports } from "../backendPlatform";
import { getAuthData } from "services/paymentBackendAPI/backendPlatform";

export const getUsersLogs = (params) => {
  const token = getAuthData().userPayload;
  if (token.group)
    return backendReports.get("/group/logs/users", { params }); 
  else if (token.partner)
    return backendReports.get("/partner/logs/users", { params });
  else
    return backendReports.get("/admin/logs/users", { params });
};

// (request_id, partner_guid, partner_name, group_guid, group_name, merchant_guid, merchant_name, transaction_guid, step_info, shop_guid, shop_name, from_date, to_date, items, page)
export const getTransactionsLogsAdmin = (page, items, search) => {
  return backendReports.get("/admin/logs/transactions", { params: { ...search, items, page } });
};

// (request_id, transaction_guid, step_info, shop_guid, from_date, to_date, items, page)
export const getMerchantTransactionsLogs = (page, items, search) => {
  return backendReports.get("/merchant/logs/transactions", { params: { ...search, items, page } });
};

// (request_id, merchant_guid, merchant_name, transaction_guid, step_info, shop_guid, from_date, to_date, items, page)
export const getGroupTransactionsLogs = ( page, items, search ) => {
  return backendReports.get("/group/logs/transactions", { params: { ...search, items, page } });
};

// (request_id, group_guid, group_name, merchant_guid, merchant_name, transaction_guid, step_info, shop_guid, from_date, to_date, items, page)
export const getPartnerTransactionsLogs = ( page, items, search ) => {
  return backendReports.get("/partner/logs/transactions", { params: { ...search, items, page } });
};

// (guid, status, type, transaction_processing_guid, transaction_type, partner_guid, partner_name, group_guid, group_name, merchant_guid, merchant_name, shop_guid, shop_name, account_guid, account_number, from_date, to_date, days, page, items)
export const getTransactionsStepsLogs = ( page, items, search ) => {
  return backendReports.get("/step_logs", { params: { ...search, items, page } });
};

// don't use
// (guid, status, type, partner_guid, partner_name, group_guid, group_name, merchant_guid, merchant_name, shop_guid, shop_name, account_guid, account_number, from_date, to_date, days, page, items)
export const getTransactionsLogs = ( page, items, search ) => {
  return backendReports.get("/transaction_logs", { params: { ...search, items, page } });
};
