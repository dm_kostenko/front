import { backendManagement, getRoles } from "../backendPlatform";

const roles = getRoles()[0];
const role = (!roles.role || roles.role === "admin") ? "" : "/" + roles.role + "s";
const roleGuid = roles.guid ? "/" + roles.guid : "";
const urlPrefix = role + roleGuid;

export const getGroups = (page, items, search) => {
  return backendManagement.get(`${urlPrefix}/groups`, { params: { ...search, items, page } });
};

export const createGroup = data => {
  return backendManagement.post("/groups", data);
};

export const getGroup = guid => {
  return backendManagement.get(`/groups/${guid}`);
};
// with guid
export const updateGroup = data => {
  return backendManagement.post("/groups", data);
};

export const deleteGroup = guid => {
  return urlPrefix 
    ? backendManagement.post("/groups", { guid, partner_guid: "" })
    : backendManagement.post("/groups", { guid, delete: true });
};

export const addGroupToPartner = (guid, partner_guid) => {
  return backendManagement.post("/groups", { guid, partner_guid });
};

export const deleteGroupFromPartner = guid => {
  return backendManagement.post("/groups", { guid, partner_guid: "" });
};

export const getGroupLogins = (guid, page, items ) => {
  return backendManagement.get(`/groups/${guid}/logins`, { params: { items, page } });
};

export const getGroupMerchants = (guid, page, items) => {
  return backendManagement.get(`/groups/${guid}/merchants`, { params: { items, page } });
};

export const getGroupShops = (guid, page, items) => {
  return backendManagement.get(`/groups/${guid}/shops`, { params: { items, page } });
};

export const addGroupLogin = (guid, data) => {
  return backendManagement.post(`/groups/${guid}/logins`, data);
};
// data = {guid, delete}
export const deleteGroupLogin = (guid, data) => {
  return backendManagement.post(`/groups/${guid}`, data);
};