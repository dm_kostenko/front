import { backendManagement } from "../backendPlatform";

export const getAccounts = (page, items, search) => {
  return backendManagement.get("/accounts", { params: { ...search, items, page } });
};

export const createAccount = data => {
  return backendManagement.post("/accounts", data);
};

export const getAccount = guid => {
  return backendManagement.get(`/accounts/${guid}`);
};

export const updateAccount = data => {
  return backendManagement.post("/accounts", data);
};

export const deleteAccount = guid => {
  return backendManagement.post("/accounts", { guid, delete: true });
};