import { backendManagement } from "../backendPlatform";

export const getPrivileges = (page, items, search, type = false) => {
  let newSearch = { 
    ...search, 
  };
  if(type)
    newSearch.type = type;
  return backendManagement.get("/privileges", { params: { ...newSearch, items, page } });
};

export const createPrivilege = data => {
  return backendManagement.post("/privileges", data);
};

export const updatePrivilege = data => {
  return backendManagement.post("/privileges", data);
};

export const getPrivilege = guid => {
  return backendManagement.get(`/privileges/${guid}`);
};

export const deletePrivilege = guid => {
  return backendManagement.post("/privileges", { guid, delete: true });
};