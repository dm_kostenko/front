import { backendManagement } from "../backendPlatform";

export const getCurrencies = (page, items, search) => {
  return backendManagement.get("/currencies", { params: { ...search, items, page } });
};

export const createCurrency = data => {
  return backendManagement.post("/currencies", data);
};

export const getCurrency = guid => {
  return backendManagement.get(`/currencies/${guid}`);
};

export const updateCurrency = data => {
  return backendManagement.post("/currencies", data);
};

export const deleteCurrency = guid => {
  return backendManagement.post("/currencies", { guid, delete: true });
};