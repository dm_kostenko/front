import { getAuthData } from "services/paymentBackendAPI/backendPlatform";
import { backendManagement } from "../backendPlatform";

export const getUserAccount = () => {
  const userGuid = getAuthData().userPayload.loginGuid;
  return backendManagement.get(`/logins/${userGuid}`);
};

export const updateUserAccount = data => {
  return backendManagement.post("/logins", data);
};