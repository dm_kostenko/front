import { backendManagement } from "../backendPlatform";

export const getCards = (page, items, search) => {
  return backendManagement.get("/cards", { params: { ...search, items, page } });
};

export const createCard = data => {
  return backendManagement.post("/cards", data);
};
// with guid
export const updateCard = data => {
  return backendManagement.post("/cards", data);
};

export const getCard = guid => {
  return backendManagement.get(`/cards/${guid}`);
};

export const getCardsAccount = (number, page, items) => {
  return backendManagement.get("/cards", { params: { account_number: number, items, page } });
};