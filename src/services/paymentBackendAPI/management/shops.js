import { backendManagement, getRoles } from "../backendPlatform";

const roles = getRoles()[0];
const role = (!roles.role || roles.role === "admin") ? "" : "/" + roles.role + "s";
const roleGuid = roles.guid ? "/" + roles.guid : "";
const urlPrefix = role + roleGuid;

export const getShops = (page, items, search) => {
  return backendManagement.get(`${urlPrefix}/shops`, { params: { ...search, items, page } });
};

export const createShop = data => {
  return backendManagement.post("/shops", data);
};
//With guid field it will update shop info
export const updateShop = data => {
  return backendManagement.post("/shops", data);
};
//With guid and delete fields it will delete shop 
export const deleteShop = guid => {
  return backendManagement.post("/shops", { guid, delete: true });
};

export const getShop = guid => {
  return backendManagement.get(`/shops/${guid}`);
};

export const getAccountsFromShop = (guid, page, items) => {
  return backendManagement.get(`/shops/${guid}/accounts`, { params: { items, page } });
};

export const addAccountToShop = (guid, data) => {
  return backendManagement.post(`/shops/${guid}/accounts`, data);
};

export const deleteAccountFromShop = (guid, data) => {
  return backendManagement.post(`/shops/${guid}/accounts`, data);
};

export const upsertAccountToShop = (guid, data) => {
  return backendManagement.post(`/shops/${guid}/accounts`, data);
};

export const getShopGateways = (guid, page, items) => {
  return backendManagement.get(`/shops/${guid}/gateways`, { params: { items, page } });
};

export const deleteGatewayFromShop = (guid, data) => {
  return backendManagement.post(`/shops/${guid}/gateways`, data);
};

export const upsertGatewayToShop = (guid, data) => {
  return backendManagement.post(`/shops/${guid}/gateways`, data);
};

export const getShopsByGateway = (guid, page, items) => {
  return backendManagement.get("/shops", { params: { gateway_guid: guid, items, page } });
};

export const getShopGatewayProps = (guid, gatewayGuid) => {
  return backendManagement.get(`/shops/${guid}/gateways/${gatewayGuid}/props`);
};

export const addShopGatewayProps = (guid, gatewayGuid, data) => {
  return backendManagement.post(`/shops/${guid}/gateways/${gatewayGuid}/props`, data);
};

export const deleteShopGatewayProps = (guid, gatewayGuid, data) => {
  return backendManagement.post(`/shops/${guid}/gateways/${gatewayGuid}/props`, data);
};

export const editShopGatewayProps = (guid, gatewayGuid, data) => {
  return backendManagement.post(`/shops/${guid}/gateways/${gatewayGuid}/props`, data);
};