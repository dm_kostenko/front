import { backendManagement } from "../backendPlatform";

export const getGateways = (page, items, search) => {
  return backendManagement.get("/gateways", { params: { ...search, items, page } });
};

export const createGateway = data => {
  return backendManagement.post("/gateways", data);
};

export const getGateway = guid => {
  return backendManagement.get(`/gateways/${guid}`);
};

export const updateGateway = data => {
  return backendManagement.post("/gateways", data);
};

export const deleteGateway = guid => {
  return backendManagement.post("/gateways", { guid, delete: true });
};

export const getGatewayProps = guid => {
  return backendManagement.get(`/gateways/${guid}/props`);
};

export const addGatewayProps = (guid, data) => {
  return backendManagement.post(`/gateways/${guid}/props`,data);
};

export const deleteGatewayProps = (guid, data) => {
  return backendManagement.post(`/gateways/${guid}/props`,data);
};

export const getGatewayPropsByName = (guid, name) => {
  return backendManagement.get(`/gateways/${guid}/props/${name}`);
};