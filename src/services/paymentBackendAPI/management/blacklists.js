import { backendManagement } from "../backendPlatform";
import { getAuthData } from "services/paymentBackendAPI/backendPlatform";

const token = getAuthData();

export const getBlackListItems = (page, items, search) => {
  return backendManagement.get("/blacklist", { params: { ...search, items, page } });
};

export const getBlackListItem = (guid) => {
  return backendManagement.get(`/blacklist/${guid}`);
};

export const createBlackListRule = (data) => {
  return backendManagement.post("/blacklist/values", data);
};

export const createBlackListItem = data => {
  return backendManagement.post("/blacklist", data);
};

export const updateBlackListItem = data => {
  return backendManagement.post("/blacklist", data);
};

export const deleteBlackListItem = guid => {
  return backendManagement.post("/blacklist", { guid, delete: true });
};

export const getBlackListGlobal = (page, items, search) => {
  return backendManagement.get("/blacklist/global", { params: { ...search, items, page } });
};

export const getMerchantsBlackList = (page, items, search) => {
  if (token && token.userPayload && token.userPayload.merchant)
    return backendManagement.get(`merchants/${token.userPayload.merchant.merchant_guid}/blacklists`, { params: { ...search, items, page } });
  else
    return backendManagement.get("/blacklist/merchants", { params: { ...search, items, page } });
};

export const createItemGlobalBlackList = data => {
  return backendManagement.post("/blacklist/global", data);
};

export const deleteItemGlobalBlackList = blacklist_rule_guid => {
  return backendManagement.post("/blacklist/global", { blacklist_rule_guid, delete: true });
};

export const createItemMerchantsBlackList = data => {
  if (token && token.userPayload && token.userPayload.merchant) {
    return backendManagement.post(`merchants/${token.userPayload.merchant.merchant_guid}/blacklists`, data);
  }   
  else
    return backendManagement.post("/blacklist/merchants", data);
};

export const deleteItemMerchantsBlackList = (blacklist_rule_guid, merchant_guid) => {
  if (token && token.userPayload && token.userPayload.merchant)
    return backendManagement.post(`merchants/${token.userPayload.merchant.merchant_guid}/blacklists`, { blacklist_rule_guid, delete: true });
  else
    return backendManagement.post("/blacklist/merchants", { blacklist_rule_guid, merchant_guid, delete: true });
};