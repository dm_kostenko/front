import { backendManagement } from "../backendPlatform";

export const addTransactionParam = (guid, data) => {
  return backendManagement.post(`/steps/${guid}/params`, data);
};