import { backendManagement } from "../backendPlatform";

export const getRates = (page, items, search) => {
  return backendManagement.get("/rates", { params: { ...search, items, page } });
};

export const createRate = data => {
  return backendManagement.post("/rates", data);
};

export const getRate = (shop_id, gateway_id, currency_id ) => {
  return backendManagement.get(`/rates/shop/${shop_id}/gateway/${gateway_id}/currency/${currency_id}`);
};

export const getPaymentStatusRate = (shop_id, gateway_id, currency_id ) => {
  return backendManagement.get(`/rates/transactions/shop/${shop_id}/gateway/${gateway_id}/currency/${currency_id}`);
};

export const updateRate = data => {
  return backendManagement.post("/rates", data);
};

export const deleteRate = (shop_guid, gateway_guid, currency_guid) => {
  return backendManagement.post("/rates", { shop_guid, gateway_guid, currency_guid, delete: true });
};

export const getOneTimeCharges = (shop_guid, gateway_guid, currency_guid,  page, items) => {
  return backendManagement.get(`/charges/one_time/shop/${shop_guid}/gateway/${gateway_guid}/currency/${currency_guid}`, { params: { items, page } });
};

export const getPeriodicCharges = (shop_guid, gateway_guid, currency_guid,  page, items) => {
  return backendManagement.get(`/charges/periodic/shop/${shop_guid}/gateway/${gateway_guid}/currency/${currency_guid}`, { params: { items, page } });
};

export const getConditionalCharges = (shop_guid, gateway_guid, currency_guid,  page, items) => {
  return backendManagement.get(`/charges/conditional/shop/${shop_guid}/gateway/${gateway_guid}/currency/${currency_guid}`, { params: { items, page } });
};

export const createOneTimeCharge = data => {
  return backendManagement.post("/charges/one_time", data);
};

export const createPeriodicCharge = data => {
  return backendManagement.post("/charges/periodic", data);
};

export const createConditionalCharge = data => {
  return backendManagement.post("/charges/conditional", data);
};