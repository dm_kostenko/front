import { backendManagement } from "../backendPlatform";

export const getTransactions = (page, items, search) => {
  return backendManagement.get("/transactions/processing", { params: { ...search, page, items } });
};

export const getTransactionTemplates = (page, items, search) => {
  return backendManagement.get("/transactions", { params: { ...search, page, items } });
};

export const getTransactionTemplate = guid => {
  return backendManagement.get(`/transactions/${guid}`);
};

export const addTransactionTemplate = data => {
  return backendManagement.post("/transactions", data);
};

export const bindTransactionStep = (data, guid) => {
  return backendManagement.post(`/transactions/${guid}/steps`, data);
};

export const deleteTransactionTemplate = guid => {
  return backendManagement.post("/transactions", { guid, delete: true });
};

export const getTransactionSteps = (guid, page, items, gateway_guid) => {
  return backendManagement.get(`/transactions/${guid}/steps`, { params: { page, items, gateway_guid } });
};

export const getTransactionProcessingSteps = (guid, page, items) => {
  return backendManagement.get("/steps/processing", { params: { transaction_processing_guid: guid, page, items } });
};

export const getTransactionProcessingParams = (guid, page, items) => {
  return backendManagement.get(`/steps/processing/${guid}/params`, { params: { page, items } });
};

export const getTransactionsForGateway = (guid) => {
  return backendManagement.get(`/transactions/gateways/${guid}`);
};