import { backendManagement, getRoles } from "../backendPlatform";

const roles = getRoles()[0];
const role = (!roles.role || roles.role === "admin") ? "" : "/" + roles.role + "s";
const roleGuid = roles.guid ? "/" + roles.guid : "";
const urlPrefix = role + roleGuid;

export const getMerchants = (page, items, search) => {
  return backendManagement.get(`${urlPrefix}/merchants`, { params: { ...search, items, page } });
};

export const createMerchant = data => {
  return backendManagement.post("/merchants", data);
};

export const getMerchant = guid => {
  return backendManagement.get(`/merchants/${guid}`);
};
// with guid
export const updateMerchant = data => {
  return backendManagement.post("/merchants", data);
};

export const deleteMerchant = guid => {
  return urlPrefix 
    ? backendManagement.post("/merchants", { guid, group_guid: "" })
    : backendManagement.post("/merchants", { guid, delete: true });
};

export const addMerchantToGroup = (guid, group_guid) => {
  return backendManagement.post("/merchants", { guid, group_guid });
};

export const deleteMerchantFromGroup = guid => {
  return backendManagement.post("/merchants", { guid, group_guid: "" });
};

export const getMerchantLogins = (guid, page, items) => {
  return backendManagement.get(`/merchants/${guid}/logins`, { params: { items, page } });
};

export const addMerchantLogin = (guid, data) => {
  return backendManagement.post(`/merchants/${guid}/logins`, data);
};
// data = {guid, delete}
export const deleteMerchantLogin = (guid, data) => {
  return backendManagement.post(`/merchants/${guid}/logins`, data);
};

export const getMerchantShops = (guid, page, items) => {
  return backendManagement.get(`/merchants/${guid}/shops`, { params: { items, page } });
};
