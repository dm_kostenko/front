import { backendManagement } from "../backendPlatform";

export const getPartners = (page, items, search) => {
  return backendManagement.get("/partners", { params: { ...search, items, page } });
};

export const createPartner = (data) => {
  return backendManagement.post("/partners", data);
};
// with guid
export const updatePartner = data => {
  return backendManagement.post("/partners", data);
};

export const getPartner = guid => {
  return backendManagement.get(`/partners/${guid}`);
};
// data = {guid, delete}
export const deletePartner = guid => {
  return backendManagement.post("/partners", { guid, delete: true });
};

export const getPartnerLogins = (guid, page, items) => {
  return backendManagement.get(`/partners/${guid}/logins`, { params: { items, page } });
};

export const getPartnerGroups = (guid, page, items) => {
  return backendManagement.get(`/partners/${guid}/groups`, { params: { items, page } });
};

export const getPartnerShops = (guid, page, items) => {
  return backendManagement.get(`/partners/${guid}/shops`, { params: { items, page } });
};

export const getPartnerMerchants = (guid, page, items) => {
  return backendManagement.get(`/partners/${guid}/merchants`, { params: { items, page } });
};

export const addPartnerLogin = (guid, data) => {
  return backendManagement.post(`/partners/${guid}/logins`, data);
};
// data = {guid, delete}
export const deletePartnerLogin = (guid, data) => {
  return backendManagement.post(`/partners/${guid}`, data);
};