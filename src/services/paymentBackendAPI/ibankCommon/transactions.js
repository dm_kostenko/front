import { backendCommon, getRoutePrefix } from "../backendPlatform";

const role = getRoutePrefix();

export const getTransactions = (params) => backendCommon.get(`${role}/transactions/history`, { params });

export const getWaitPayments = (params) => backendCommon.get(`${role}/transactions/ffcctrns`, { params })

export const getTransactionsFees = (data) => backendCommon.post(`${role}/transactions/ffcctrns/fee`, data)

export const getTransactionRules = (params) => backendCommon.get(`${role}/transactions/rules`, { params });

export const createTransactionRule = (data) => backendCommon.post(`${role}/transactions/rules`, data);

export const getAllTransactions = (type) => backendCommon.get(`${role}/transactions/history`, { params: { type } });

export const getTransaction = (guid, type) => backendCommon.get(`${role}/transactions/${type}/${guid}`);

export const createTransaction = (data, type) => backendCommon.post(`${role}/transactions/${type}`, data);

export const createTransactions = (data) => backendCommon.post(`${role}/transactions/ffcctrns/array`, data);

export const editTransactionStatus = (data, type, guid) => backendCommon.post(`${role}/transactions/${type}/${guid}/status`, data);

export const requestOtp = (guid) => backendCommon.get(`${role}/transactions/history/${guid}/otp`);

export const getTransactionsTemplates = (params) => backendCommon.get(`${role}/transactions/templates`, { params });

export const upsertTransactionsTemplates = (data) => backendCommon.post(`${role}/transactions/templates`, data);

export const getExternalTransactionsTemplates = (params) => backendCommon.get(`${role}/transactions/externalTemplates`, { params });

export const createExternalTransactionTemplate = (data) => backendCommon.post(`${role}/transactions/externalTemplates`, data);

export const getInternalTransactionsTemplates = (params) => backendCommon.get(`${role}/transactions/internalTemplates`, { params });

export const createInternalTransactionTemplate = (data) => backendCommon.post(`${role}/transactions/internalTemplates`, data);