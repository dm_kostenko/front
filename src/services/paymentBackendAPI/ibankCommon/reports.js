import { backendCommon, getRoutePrefix } from "../backendPlatform";

const role = getRoutePrefix();

export const getTransactionHistory = (params) => {
  return backendCommon.get(`${role}/reports/transactionHistory`, { params });
};

export const getAmountOfTransactions = ({ search, ...rest }) => {
  return backendCommon.get(`${role}/reports/amountOfTransactions`, { params: { ...search, ...rest } });
};

export const getTransactionTypes = (params) => {
  return backendCommon.get(`${role}/reports/transactionTypes`, { params });
};