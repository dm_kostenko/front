import { backendCommon, getRoutePrefix } from "../backendPlatform";

const role = getRoutePrefix();

export const getAmlHistory = args => backendCommon.get(`${role}/aml`, { params: { ...args } });

export const getAmlHistoryDetail = id => backendCommon.get(`${role}/aml/${id}`);

export const updateAmlDetail = ({ id, ...data }) => backendCommon.post(`${role}/aml/${id}`, data);

export const checkInAml = (data) => backendCommon.post(`${role}/aml`, data);

export const getCaseComments = ({ id, ...data }) => backendCommon.get(`${role}/aml/comments/${id}`, { params: { ...data } });

export const addCaseComment = ({ id, ...data }) => backendCommon.post(`${role}/aml/comments/${id}`, data);

export const getEntityComments = ({ caseId, entityId, ...data }) => backendCommon.get(`${role}/aml/comments/${caseId}/entity/${entityId}`, { params: { ...data } });

export const addEntityComment = ({ caseId, entityId, ...data }) => backendCommon.post(`${role}/aml/comments/${caseId}/entity/${entityId}`, data);

export const getTags = args => backendCommon.get(`${role}/aml/tags`, { params: { ...args } });

export const upsertTag = data => backendCommon.post(`${role}/aml/tags`, data);

export const getCaseTags = ({ caseId, ...data }) => backendCommon.get(`${role}/aml/tags/${caseId}/case`, { params: { ...data } });

export const attachCaseTags = ({ caseId, ...data }) => backendCommon.post(`${role}/aml/tags/${caseId}/case`, data);

export const getCaseFiles = ({ caseId, ...data }) => backendCommon.get(`${role}/aml/${caseId}/files`, { params: { ...data } });

export const addCaseFiles = ({ caseId, data }) => backendCommon.post(`${role}/aml/${caseId}/files`, data, data.del ? {} : { headers: { 'Content-Type': 'multipart/form-data' } });

export const getAmlCertificate = id => backendCommon.get(`${role}/aml/${id}/certificate`);

