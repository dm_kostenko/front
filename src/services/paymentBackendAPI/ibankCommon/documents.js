import { backendCommon, getRoutePrefix } from "../backendPlatform";

const role = getRoutePrefix();

// export const getTransactionsDocs = (page, items, search, type) => {
//   return backendCommon.get(`${role}/documents/${type}`, { params: { ...search, items, page } });
// };

// export const getTransactionDoc = (guid, type) => {
//   return backendCommon.get(`${role}/documents/${type}/${guid}`);
// };

// export const generateDoc = (data, type) => {
//   return backendCommon.post(`${role}/documents/${type}`, data);
// };


export const getInbox = (params) => backendCommon.get(`${role}/inbox`, { params });

export const getInboxDetail = (guid) => backendCommon.get(`${role}/inbox/${guid}`);

export const editInboxStatus = (params, guid) => backendCommon.post(`${role}/inbox/${guid}/status`, params);