import { backendCommon, getRoutePrefix } from "../backendPlatform";

const role = getRoutePrefix();

export const getRates = args => backendCommon.get(`${role}/rates`, { params: { ...args } });

export const getRateRules = args => backendCommon.get(`${role}/rates/${args.rate_guid}/rules`, { params: { ...args } });

export const getRateRuleParams = args => backendCommon.get(`${role}/rates/${args.rate_guid}/params`, { params: { ...args } });

export const createRate = (data) => backendCommon.post(`${role}/rates`, data);

export const changeRateRuleParams = data => backendCommon.post(`${role}/rateRuleParams`, data);