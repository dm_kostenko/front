import { backendCommon } from "../backendPlatform";

export const getPrivileges = (params) => backendCommon.get("admin/privileges", { params });