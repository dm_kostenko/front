import { backendCommon, getRoutePrefix } from "../backendPlatform";

const role = getRoutePrefix();

export const getCurrencies = (params) => backendCommon.get(`${role}/currencies`, { params });

export const getCurrenciesRates = (params) => backendCommon.get(`${role}/currencies/rates`, { params });