import { backendCommon, getRoutePrefix } from "../backendPlatform";

const role = getRoutePrefix();

export const getUsers = (params) => backendCommon.get(`${role}/logins`, { params });

export const getEntities = () => backendCommon.get(`${role}/logins`, { params: { type: "entity" } });

export const getUser = (guid) => backendCommon.get(`${role}/logins/${guid}`);

export const getUserInfo = () => backendCommon.get(`${role}/info`);

export const upsertUser = (data) => backendCommon.post(`${role}/logins`, data);