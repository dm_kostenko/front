import { backendCommon, getRoutePrefix } from "../backendPlatform";

const role = getRoutePrefix();

export const getAccounts = (args) => backendCommon.get(`${role}/accounts`, { params: { ...args } });

export const addCurrencyToAccount = (number, data) => backendCommon.post(`${role}/accounts/${number}/currencies`, data);

export const getAccount = (number) => backendCommon.get(`${role}/accounts/${number}`);

export const createAccount = (data) => backendCommon.post(`${role}/accounts`, data);

export const getAccountsStatements = (accountNumber, args) => backendCommon.get(`${role}/accounts/${accountNumber}/statement`, { params: { ...args } });

export const getAccountsStatementsInXml = (accountNumber, args) => backendCommon.get(`${role}/accounts/${accountNumber}/statement/xml`, { params: { ...args } });

export const getAccountsBalances = (accountNumber, args) => backendCommon.get(`${role}/accounts/${accountNumber}/balances`, { params: { ...args } });

export const getAccountsPaysDocs = (args) => backendCommon.get(`${role}/accounts/123/pays`, { params: { ...args } });