# Backend API service
## Management
Tested:
- [ ] Roles
- [ ] Users
- [ ] Merchants
- [ ] Groups
- [ ] Partners
<br><br>
* ### Roles<br>
    
    | Methods      |  
    | :-------------- | 
    | **getRoles()**|
    | **createRole(data)**| 
    | **getRole(guid)**  |
    | **updateRole(data)** |
    | **deleteRole(data)**|
* ### Users<br>
    
    | Methods      |  
    | :-------------- | 
    | **getUsers()**|
    | **createUser(data)**| 
    | **getUser(guid)**  |
    | **updateUser(data)** |
    | **deleteUser(data)**|
* ### Merchants<br>

    | Methods      |  
    | :-------------- | 
    | **getMerchants()**|
    | **getMerchants(group_guid)**|
    | **createMerchant(data)**| 
    | **getMerchant(guid)**  |
    | **updateMerchant(data)** |
    | **deleteMerchant(data)**|
    | **getUsersFromMerchant(guid)**|
    | **addUserToMerchant(guid, data)**|
    | **deleteUserFromMerchant(guid, data)**|
* ### Groups<br>

    | Methods         |  
    | :-------------- | 
    | **getGroups()**|
    | **getGroups(partner_guid)**|
    | **createGroup(data)**| 
    | **getGroup(guid)**  |
    | **updateGroup(data)** |
    | **deleteGroup(data)**|
    | **getUsersFromGroup(guid)**|
    | **addUserInGroup(guid, data)**|
    | **deleteUserFromGroup(guid, data)**|
        
* ### Partners<br>
    
    | Methods         |  
    | :-------------- | 
    | **getPartners()**|
    | **createPartner(data)**| 
    | **getPartner(guid)**  |
    | **updatePartner(data)** |
    | **deletePartner(data)**|
    | **getUsersFromPartner(guid)**|
    | **addUserToPartner(guid, data)**|
    | **deleteUserFromPartner(guid, data)**|
    
