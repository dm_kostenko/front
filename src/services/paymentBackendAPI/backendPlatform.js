import { getPrivilegesFromRole } from "./management/roles";
import axios from "axios";
import config from "../../config/";
import publicKey from "../../config/publicKey";
import jwt from "jsonwebtoken";
import Joi from "@hapi/joi";
import validate from "helpers/validation";
import { showConfirmationModal } from "ibankActions/modal";
import { saveConfig } from "ibankActions/confirmation";
import store from "ibankStore/store";
const { dispatch } = store;

const reportApiUrl = `${config.node.host}/api/v1/reports`;
const managementApiUrl = `${config.node.host}/api/bank/management`;
const apiUrl = `${config.node.host}/api/bank`;
const authApiUrl = `${apiUrl}/auth`;


//non-uses tokens
export const backendAuth = axios.create({ baseURL: authApiUrl }); 
export const backendCommon = axios.create({ baseURL: apiUrl }); 
//requires token 
export const backendReports = axios.create({ baseURL: reportApiUrl });
export const backendManagement = axios.create({ baseURL: managementApiUrl });

if(!config.tfa) {
  backendCommon.defaults.headers.common["one-time-password-checking-ignore"] = "true";
  backendAuth.defaults.headers.common["one-time-password-checking-ignore"] = "true";
  backendManagement.defaults.headers.common["one-time-password-checking-ignore"] = "true";
}

export const setTokens = async (accessToken, refreshToken) => {
  jwt.verify(accessToken, publicKey, { algorithm: "RS256" });
  jwt.verify(refreshToken, publicKey, { algorithm: "RS256" });
  const auth = { accessToken, refreshToken };
  backendCommon.defaults.headers.common["Authorization"] = `Bearer ${accessToken}`;

  setLoginInfo(accessToken);
  // await isAdmin(accessToken);
  await setPermissions(accessToken);
  localStorage.setItem("auth", JSON.stringify(auth));
  // localStorage.setItem("isLoggedIn", true);
};

export const getTokens = () => JSON.parse(localStorage.getItem("auth"));

export const getUserType = () => localStorage.getItem("userType");

export const getUsername = () => localStorage.getItem("username");

export const getRoutePrefix = () => ["admin", "aml", "finance"].includes(localStorage.getItem("userType")) ? "admin" : "user";

export const getUserLoginsData = () => {
  const decoded = getToken();
  if(decoded && decoded.userPayload.logins && decoded.userPayload.logins.length > 1)
    return {
      currentLogin: decoded.userPayload.loginGuid,
      logins: decoded.userPayload.logins
    };
  return false;
};

export const isIndividualWithEntities = () => {
  const decoded = getToken();
  return decoded && decoded.userPayload.type === "individual" && decoded.userPayload.logins && decoded.userPayload.logins.length > 1;
};

export const getUserData = () => {
  try {
    if (!localStorage.getItem("isLoggedIn")) return;
    let accessToken;
    let tokens = JSON.parse(localStorage.getItem("changeAuth"));
    if(tokens && tokens.accessToken)
      accessToken = tokens.accessToken;
    else
      accessToken = getTokens().accessToken;
    return jwt.decode(accessToken, publicKey, { algorithm: "RS256" }).userPayload;
  } catch (error) {
    flushTokens();
  }
};

export const getToken = () => {
  try {
    if (!localStorage.getItem("isLoggedIn")) return;
    const { accessToken } = getTokens();
    return jwt.decode(accessToken, publicKey, { algorithm: "RS256" });
  } catch (error) {
    flushTokens();
  }
};


const setLoginInfo = (accessToken) => {
  const decoded = jwt.decode(accessToken);
  localStorage.setItem("userType", decoded.userPayload.type);
  localStorage.setItem("username", decoded.userPayload.username);
};

export const setPermissions = (accessToken) => {
  const token = jwt.decode(accessToken, publicKey, { algorithm: "RS256" });
  if(token && token.userPayload && token.userPayload.privileges) {
    localStorage.setItem("permissions", JSON.stringify(token.userPayload.privileges.map(privilege => { return privilege.name; })));
  }
};

export const getPermissions = () => {
  const permissions = localStorage.getItem("permissions");
  return permissions ? JSON.parse(permissions) : [];
};

export const issueToken = () => {
  const tokens = JSON.parse(localStorage.getItem("changeAuth"));
  if(tokens && tokens.refreshToken) {
    localStorage.removeItem("changeAuth");
    return backendAuth.post("/token", { refreshToken: tokens.refreshToken });
  }
  const refreshToken = getTokens().refreshToken;
  return backendAuth.post("/token", { refreshToken });
};

export const flushTokens = () => {
  // backendManagement.defaults.headers.common["Authorization"] = undefined;
  backendCommon.defaults.headers.common["Authorization"] = undefined;
  // localStorage.removeItem("auth");
  // localStorage.removeItem("isLoggedIn");
  localStorage.clear();
  
  window.location.reload();
};

const initAccessToken = () => {
  const tokens = getTokens();
  if (tokens && tokens.accessToken){
    backendCommon.defaults.headers.common["Authorization"] = `Bearer ${tokens.accessToken}`;
  }
};

export const checkToken = () => {
  const isAdmin = getUserType() === "admin";
  const schema = isAdmin
    ? {
      payload: Joi.object().keys({
        id: Joi.string().guid().required(),
        ip: Joi.string().ip().required(),
        user_agent: Joi.string().required()
      }),
      userPayload: Joi.object().keys({
        type: Joi.string().equal("admin", "individual", "entity", "aml", "finance").required(),
        email: Joi.string().email().required(),
        username: Joi.string().required(),
        logins: Joi.array().items(Joi.string().guid()).required(),
        loginGuid: Joi.string().guid().required()
      }),
      iat: Joi.date().timestamp("unix").required(),
      exp: Joi.date().timestamp("unix").required()
    }
    : {
      payload: Joi.object().keys({
        id: Joi.string().guid().required(),
        ip: Joi.string().ip().required(),
        user_agent: Joi.string().required()
      }),
      userPayload: Joi.object().keys({
        type: Joi.string().equal("admin", "individual", "entity", "aml", "finance").required(),
        email: Joi.string().email().required(),
        username: Joi.string().required(),
        logins: Joi.array().items(Joi.string().guid()).required(),
        loginGuid: Joi.string().guid().required(),
        privileges: Joi.array().items(Joi.object().keys({
          guid: Joi.string().guid().required(),
          name: Joi.string().required()
        }))      
      }),
      iat: Joi.date().timestamp("unix").required(),
      exp: Joi.date().timestamp("unix").required()
    };
  try {
    const tokens = getTokens();
    if (tokens && tokens.accessToken) {
      if(jwt.verify(tokens.accessToken, publicKey, { algorithm: "RS256" })) {
        const accessToken = jwt.decode(tokens.accessToken, publicKey, { algorithm: "RS256" });
        validate(accessToken, schema);  
      }
      else
        flushTokens();
    }
  }
  catch(error) {
    flushTokens();
  }
};

let isAlreadyFetchingAccessToken = false;
export const refreshTokens = async() => {
  if (localStorage.getItem("isLoggedIn")) {
    const token = getToken();
    const exp = new Date((token.exp - Math.round(+new Date() / 1000)) * 1000);
    if (exp.getMinutes() < 5)
      if (!isAlreadyFetchingAccessToken) {
        isAlreadyFetchingAccessToken = true;
        try {
          const response = await issueToken();
          isAlreadyFetchingAccessToken = false;
          setTokens(response.data.accessToken, response.data.refreshToken);
        } 
        catch (err) { 
          flushTokens();
        }
      } 
  }
}

// let isAlreadyFetchingAccessToken = false;
// const successResponseInterceptor = async originResponse => {
//   if (localStorage.getItem("isLoggedIn")) {
//     const token = getToken();
//     const exp = new Date((token.exp - Math.round(+new Date() / 1000)) * 1000);
//     if (exp.getMinutes() < 5)
//       return new Promise(async resolve => {
//         if (!isAlreadyFetchingAccessToken) {
//           isAlreadyFetchingAccessToken = true;
//           try {
//             const response = await issueToken();
//             isAlreadyFetchingAccessToken = false;
//             setTokens(response.data.accessToken, response.data.refreshToken);
//             return resolve(originResponse);
//           } catch (err) { 
//             flushTokens();
//           }
//         } else return resolve(originResponse);
//       });
//   }
//   return originResponse;
// };

const errorResponseInterceptor = async error => {
  if (!error.response && localStorage.getItem("isLoggedIn")){
    flushTokens();
  }
  
  if (error.response) {
    if (error.config.__isRetryRequest)
      flushTokens();
    else if (localStorage.getItem("isLoggedIn") && error.response.status === 401) {
      flushTokens(); 
    } 
    else if(localStorage.getItem("isLoggedIn") && error.response.status === 409 && config.tfa) {  // TFA
      const { config } = error;
      dispatch(saveConfig(config));
      dispatch(showConfirmationModal(true));
    }
    /*
    else if (localStorage.getItem("isLoggedIn") && error.response.status === 401) {
      return new Promise(async resolve => {
        if (!isAlreadyFetchingAccessToken) {
          isAlreadyFetchingAccessToken = true;
          try {
            const { config } = error;
            const response = await issueToken();
            isAlreadyFetchingAccessToken = false;
            setTokens(response.data.accessToken, response.data.refreshToken);
            config.headers.Authorization = "Bearer " + response.data.accessToken;
            config.baseURL = undefined;
            config.__isRetryRequest = true;
            return resolve(axios.request(error.config));
          } catch (err) { 
            flushTokens();
          }
        } else {
          error.config.baseURL = undefined;
          return await setTimeout(() => {
            error.config.headers.Authorization = `Bearer ${getTokens().accessToken}`;
            return resolve(axios.request(error.config));
          }, 2500);
        }
      });
    } */
    else if (localStorage.getItem("isLoggedIn") && error.response.status === 403)
      flushTokens();
  }
  return Promise.reject(error);
};

backendAuth.interceptors.response.use(response => response, errorResponseInterceptor);
backendManagement.interceptors.response.use(response => response, errorResponseInterceptor);
backendReports.interceptors.response.use(response => response, errorResponseInterceptor);
backendCommon.interceptors.response.use(response => response, errorResponseInterceptor);
initAccessToken();
