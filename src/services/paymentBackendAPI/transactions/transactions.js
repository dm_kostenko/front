import { backendTransactions, backendManagement } from "../backendPlatform";
import crypto from "crypto";

export const createTransaction = (data, shopGuid, shopSecret, shopHashkey) => {
  const signature = crypto.createHash("sha256").update(JSON.stringify(data) + shopHashkey).digest("base64");
  data.signature = signature;
  return backendTransactions.post("/create", data, { auth: { "username": shopGuid, "password": shopSecret } } );
};

export const getTransactions = (page, items, search) => {
  return backendManagement.get("/transactions/processing", { params: { ...search, page, items } });
};

export const addTransactionTemplate = data => {
  return backendManagement.post("/transactions", data);
};

