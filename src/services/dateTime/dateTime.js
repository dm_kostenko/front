import moment from "moment";


export const formatDate = (date) => {
  return moment(date).utcOffset(0).format("DD.MM");
};

const daysBetweenTwoDates = (date1, date2) => {
  return Math.abs((date2.getTime() - date1.getTime()) / (1000 * 60 * 60 * 24)); 
};

export const lastNDays = (N, type) => {
  let result = [];
  switch(type) {
  case "day":
    for (let i = 0; i < N; i++) {
      let d = new Date();
      d.setDate(d.getDate() - i);
      result.unshift(moment(d).utcOffset(0).format("DD.MM"));
    }
    break;
  case "month":
    for (let i = 0; i < N; i++) {
      let d = new Date();
      d.setMonth(d.getMonth() - i);
      result = [ moment(d).format("MMM"), ...result ];
    }
    break;    
  default: break;
  }  
  return result;
};



/*
  EXAMPLE: 
    const since = new Date('7/13/2010'); (MM/DD/YYYY)
    const to = new Date('12/15/2010');
*/
export const daysSinceTo = (since, to) => {
  let result = [ formatDate(to) ];
  let N = daysBetweenTwoDates(since, to);
  for (let i = 0; i < N; i++) {
    let d = to;
    d.setDate(d.getDate() - 1 );
    result.unshift(formatDate(d));
  }
  return result;
};



export const JSDateToDateTime = (inDate) => {
  const GMTPlusSeven = 7;   // Tomsk timezone
  let date = new Date(inDate.valueOf() + GMTPlusSeven * 60 * 60000);     // GMT+7
  return moment(date).format("YYYY-MM-DD HH:mm:ss");
  // date = date.getUTCFullYear() + "-" +
  //     ("00" + (date.getUTCMonth()+1)).slice(-2) + "-" +
  //     ("00" + date.getUTCDate()).slice(-2) + " " +
  //     ("00" + date.getUTCHours()).slice(-2) + ":" +
  //     ("00" + date.getUTCMinutes()).slice(-2) + ":" +
  //     ("00" + date.getUTCSeconds()).slice(-2);
  // return date;
};