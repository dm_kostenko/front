# pull official base image
FROM node:10.16

# set working directory
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

ARG REACT_APP_BACKEND_HOST
ENV REACT_APP_BACKEND_HOST=${REACT_APP_BACKEND_HOST}

ARG REACT_APP_STAGE
ENV REACT_APP_STAGE=${REACT_APP_STAGE}

ARG REACT_APP_TFA
ENV REACT_APP_TFA=${REACT_APP_TFA}

# install app dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm install

# add app
COPY . ./

# start app
CMD ["npm", "start"]