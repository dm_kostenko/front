module.exports = {
  "env": {
    "browser": true,
    "es6": true,
    "node": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended"
  ],
  "globals": {
    "Atomics": "readonly",
    "SharedArrayBuffer": "readonly"
  },
  "parserOptions": {
    "ecmaVersion": 2018,
    "sourceType": "module"
  },
  "parser": "babel-eslint",
  "rules": {
    "indent": [
      "error",
      2
    ],
    "quotes": [
      "error",
      "double"
    ],
    "semi": [
      "error",
      "always"
    ],
    "no-console": [
      0
    ],
    "array-bracket-spacing": ["error", "always", { "arraysInArrays": false }],
    "object-curly-spacing": ["error", "always"],
    "react/display-name": ["false"],
    "no-case-declarations": [
      1
    ],
    "jsx-a11y/anchor-is-valid": [0]
  }
};